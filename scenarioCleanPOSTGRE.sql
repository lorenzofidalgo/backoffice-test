CREATE OR REPLACE FUNCTION scenarioClean()
  RETURNS void AS
 $BODY$ DECLARE 
 v_Seed VARCHAR(32);
 
 BEGIN
 
 SELECT replace(get_uuid()::varchar,'-','') INTO v_Seed;
 
 --AD_Client.name, AD_Client.value, AD_Client.description
 update ad_client set name=substr(name || v_Seed,1,60),
   value=substr(value || v_Seed,1,40), 
   description=description || v_Seed
 where name='SampleClient';

 --AD_User.name, AD_User.description, AD_User.username
 update ad_user set name=substr(name || v_Seed,1,40), 
   description=substr(description || v_Seed,1,40),
   username=substr(username || v_Seed,1,40)
 where username='SampleClientAdmin' or username='userA' or username='userB' or username='SampleClientUser';

 --AD_Role.name, AD_Role.description
 update ad_role set name=substr(name || v_Seed,1,40), 
   description=substr(description || v_Seed,1,40)
 where name='SampleClient Admin' or name='SampleClient User';

 END ; $BODY$
 LANGUAGE 'plpgsql' VOLATILE;
 
 SELECT scenarioClean();
 
 DROP FUNCTION scenarioClean();
 
