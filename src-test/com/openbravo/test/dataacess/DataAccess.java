/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.dataacess;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Class that provides data access from a database.
 *
 * @author elopio
 *
 */
public class DataAccess {

  /**
   * Connect to the data provider.
   *
   * @return the opened connection.
   */
  public static Connection connect()
      throws ClassNotFoundException, SQLException, FileNotFoundException, IOException {
    Properties properties = new Properties();
    Connection connection;
    properties.load(new FileInputStream("config" + File.separator + "OpenbravoERPTest.properties"));
    String driver = properties.getProperty("bbdd.driver");
    String URL = properties.getProperty("bbdd.url");
    String user = properties.getProperty("bbdd.user");
    String password = properties.getProperty("bbdd.password");
    if (properties.getProperty("bbdd.rdbms").equalsIgnoreCase("POSTGRE")) {
      URL += "/" + properties.getProperty("bbdd.sid");
    }
    System.out.println("Loading driver: " + driver);
    Class.forName(driver);
    System.out.println("Driver loaded");
    if (user == null || user.equals("")) {
      connection = DriverManager.getConnection(URL);
    } else {
      connection = DriverManager.getConnection(URL, user, password);
    }
    System.out.println("connect made");
    return connection;
  }
}
