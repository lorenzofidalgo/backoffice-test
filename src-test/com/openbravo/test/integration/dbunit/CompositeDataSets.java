/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.dbunit;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;

/**
 * Noninstantiable utility class to handle DBUnit composite data sets.
 *
 * @author elopio
 *
 */
public class CompositeDataSets {

  // Supress default constructor for noninstantiability.
  private CompositeDataSets() {
    throw new AssertionError();
  }

  /**
   * Create a composite data set made from multiple flat XML data sets.
   *
   * @param xmlDataSetsPaths
   *          a list of paths to XML data set files.
   * @return a composite data set.
   */
  public static IDataSet compositeFromFlatXmlDataSets(String... xmlDataSetsPaths)
      throws DataSetException, FileNotFoundException {
    ArrayList<IDataSet> flatXmlDataSets = new ArrayList<IDataSet>();
    FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
    flatXmlDataSetBuilder.setColumnSensing(true);
    for (String xmlDataSetPath : xmlDataSetsPaths) {
      IDataSet flatXmlDataSet = flatXmlDataSetBuilder.build(new FileInputStream(xmlDataSetPath));
      flatXmlDataSets.add(flatXmlDataSet);
    }
    IDataSet compositeDataSet = new CompositeDataSet(
        flatXmlDataSets.toArray(new IDataSet[flatXmlDataSets.size()]));
    return compositeDataSet;
  }
}
