/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.dbunit;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.FilteredDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.filter.DefaultTableFilter;
import org.dbunit.dataset.filter.ITableFilter;
import org.dbunit.dataset.xml.FlatXmlDataSet;

import com.openbravo.test.dataacess.DataAccess;

/**
 * Noninstantiable utility class to handle DBUnit Flat XML data sets.
 *
 * @author elopio
 *
 */
public class FlatXmlDataSets {

  // Suppress default constructor for noninstantiability.
  private FlatXmlDataSets() {
    throw new AssertionError();
  }

  /**
   * Generate a Flat XML Data set file.
   *
   * @param dbUnitConnection
   *          The DBUnit connection.
   * @param table
   *          The table.
   * @param query
   *          The query that will filter table records.
   * @param path
   *          The path where the file will be stored.
   */
  public static void generateFile(IDatabaseConnection dbUnitConnection, String table, String query,
      String path) throws DataSetException, SQLException, FileNotFoundException, IOException {
    final ITableFilter filter = new DefaultTableFilter();
    final QueryDataSet partialDataSet = new QueryDataSet(dbUnitConnection);
    partialDataSet.addTable(table, query);
    final IDataSet dataset = new FilteredDataSet(filter, partialDataSet);

    FlatXmlDataSet.write(dataset, new FileOutputStream(path));
  }

  /**
   * A main class for standalone XML generation.
   *
   * @param args
   *          args
   */
  public static void main(String[] args) throws DatabaseUnitException, SQLException,
      FileNotFoundException, ClassNotFoundException, IOException {
    final Connection connection = DataAccess.connect();
    final IDatabaseConnection dbUnitConnection = Connections.getDbUnitConnection(connection);
    FlatXmlDataSets.generateFile(dbUnitConnection, "m_inoutline",
        "select * from m_inoutline where m_inout_id = 'E213C5723B344DAFA939FE285AEDFB9C';",
        "/tmp/shipmentline.xml");
  }
}
