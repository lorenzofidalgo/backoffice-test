/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *      Leo Arias <leo.arias@openbravo.com>
 ************************************************************************
 */
package com.openbravo.test.integration.erp.common;

import java.io.File;

/**
 * Class that handles ERP Test Files
 *
 * @author elopio
 *
 */
public class TestFiles {

  /** Name of the test files folder. */
  private static final String TestFilesFolder = "testfiles";
  /** Name of the datasets folder. */
  private static final String DatasetFilesFolder = "datasets";

  /**
   * Get the path of a test file, relative to the code base path
   *
   * @param filePath
   *          the path to the file, from the testfiles folder.
   * @return the relative path to the file
   */
  public static String getRelativeFilePath(String... filePath) {
    StringBuilder path = new StringBuilder();
    path.append(TestFilesFolder);
    for (String s : filePath) {
      path.append(File.separator);
      path.append(s);
    }
    return path.toString();
  }

  /**
   * Get the full path of a test file.
   *
   * @param filePath
   *          the path to the file, from the testfiles folder.
   * @return the full path to the file
   */
  public static String getFullFilePath(String... filePath) {
    File file = new File(getRelativeFilePath(filePath));
    return file.getAbsolutePath();
  }

  /**
   * Get the path of a test file, relative to the code base path
   *
   * @param path
   *          relative to the code base path
   * @return the relative path to the file.
   */
  public static String getRelativeDataSetFilePath(String... path) {
    String[] datasetPath = new String[path.length + 1];
    datasetPath[0] = DatasetFilesFolder;
    for (int index = 0; index < path.length; index++) {
      datasetPath[index + 1] = path[index];
    }
    return getRelativeFilePath(datasetPath);
  }
}
