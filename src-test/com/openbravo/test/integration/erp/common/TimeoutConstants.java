/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.common;

/**
 * Constants used to wait for elements or conditions.
 *
 * @author elopio
 *
 */
public class TimeoutConstants {

  /**
   * Timeout in milliseconds, after which an open command will return an error.
   */
  public static final String OPEN_PAGE_TIMEOUT = "120000";

  /** Timeout in seconds, after which a page load will return an error. */
  public static final long PAGE_LOAD_TIMEOUT = 60;

  /** Timeout in milliseconds, after which the page that downloads modules will return an error. */
  // Five minutes
  public static final String MODULE_LOAD_TIMEOUT = "300000";

  /** Timeout in seconds, after which a page load will return an error. */
  public static final long LOGGING_TIMEOUT = 120;

  /**
   * Timeout in milliseconds, after which a client deletion will return an error.
   */
  public static final String DELETE_CLIENT_TIMEOUT = "2400000";

  /**
   * Timeout in milliseconds, after which a GL Posting by DB Tables will return an error.
   */
  public static final String GL_POSTING_BY_DB_TABLES_TIMEOUT = "2400000";

  /**
   * Timeout in milliseconds, after which a Initial Organization Setup will return an error.
   */
  public static final String INITIAL_ORGANIZATION_SETUP_TIMEOUT = "240000";

  /**
   * Timeout in seconds, after which a system rebuild will return an error.
   */
  // One hour.
  public static final long REBUILD_TIMEOUT = 3600;

  /**
   * Timeout in seconds, after which a servlet container restart will return an error.
   */
  // Fifteen minutes.
  public static final long RESTART_SERVLET_CONTAINER_TIMEOUT = 900;
}
