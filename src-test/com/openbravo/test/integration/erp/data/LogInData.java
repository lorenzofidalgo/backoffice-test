/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data;

/**
 * Class for Log In data.
 *
 * @author elopio
 *
 */
public class LogInData {

  /** Formatting string used to get a string representation of this object. */
  private static final String FORMAT_STRING_REPRESENTATION = "[Log In data: user name=%s, password=%s]";

  /* Data fields. */
  /** The user name. */
  private String userName;
  /** The user password. */
  private String password;

  /**
   * Class builder.
   *
   * @author elopio
   *
   */
  public static class Builder {
    /** The user name. */
    private String userName = null;
    /** The user password. */
    private String password = null;

    /**
     * Set the user name.
     *
     * @param value
     *          The user name.
     * @return the builder for this class.
     */
    public Builder userName(String value) {
      this.userName = value;
      return this;
    }

    /**
     * Set the user password.
     *
     * @param value
     *          The password.
     * @return the builder for this class.
     */
    public Builder password(String value) {
      this.password = value;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return the data object.
     */
    public LogInData build() {
      return new LogInData(this);
    }
  }

  /**
   * Class constructor.
   *
   * @param builder
   *          The object builder.
   */
  private LogInData(Builder builder) {
    userName = builder.userName;
    password = builder.password;
  }

  /**
   * Get the user name.
   *
   * @return the user name.
   */
  public String getUserName() {
    return userName;
  }

  /**
   * Get the user password.
   *
   * @return the user password.
   */
  public String getPassword() {
    return password;
  }

  /**
   * Get the string representation of this data object.
   *
   * @return the string representation.
   */
  @Override
  public String toString() {
    return String.format(FORMAT_STRING_REPRESENTATION, userName, password);
  }
}
