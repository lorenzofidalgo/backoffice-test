/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data;

import java.util.LinkedHashMap;

/**
 *
 * Class for ProcessSchedulingData
 *
 * @author plujan
 *
 */
public class ProcessSchedulingData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the fullTime value
     *
     * Description: Full Time
     *
     * @param value
     *          The fullTime value.
     * @return The builder for this class.
     */
    public Builder fullTime(Boolean value) {
      this.dataFields.put("fullTime", value);
      return this;
    }

    /**
     * Set the weekday value
     *
     * Description: Any day of the week excluding Saturday and Sunday.
     *
     * @param value
     *          The weekday value.
     * @return The builder for this class.
     */
    public Builder weekday(String value) {
      this.dataFields.put("weekday", value);
      return this;
    }

    /**
     * Set the endingTime value
     *
     * Description: Ending Time
     *
     * @param value
     *          The endingTime value.
     * @return The builder for this class.
     */
    public Builder endingTime(String value) {
      this.dataFields.put("endingTime", value);
      return this;
    }

    /**
     * Set the startingTime value
     *
     * Description: Starting Time
     *
     * @param value
     *          The startingTime value.
     * @return The builder for this class.
     */
    public Builder startingTime(String value) {
      this.dataFields.put("startingTime", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the process value
     *
     * Description: A series of actions carried out in sequential order.
     *
     * @param value
     *          The process value.
     * @return The builder for this class.
     */
    public Builder process(String value) {
      this.dataFields.put("process", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ProcessSchedulingData build() {
      return new ProcessSchedulingData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ProcessSchedulingData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
