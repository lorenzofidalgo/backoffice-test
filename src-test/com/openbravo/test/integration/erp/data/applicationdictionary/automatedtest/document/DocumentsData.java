/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:10
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.automatedtest.document;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for DocumentsData
 *
 * @author plujan
 *
 */
public class DocumentsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the introduction value
     *
     * Description: Introduction
     *
     * @param value
     *          The introduction value.
     * @return The builder for this class.
     */
    public Builder introduction(Boolean value) {
      this.dataFields.put("introduction", value);
      return this;
    }

    /**
     * Set the prefix value
     *
     * Description: Characters which are added at the beginning of a statement or number.
     *
     * @param value
     *          The prefix value.
     * @return The builder for this class.
     */
    public Builder prefix(Boolean value) {
      this.dataFields.put("prefix", value);
      return this;
    }

    /**
     * Set the lineInterchange value
     *
     * Description: Line Interchange
     *
     * @param value
     *          The lineInterchange value.
     * @return The builder for this class.
     */
    public Builder lineInterchange(Boolean value) {
      this.dataFields.put("lineInterchange", value);
      return this;
    }

    /**
     * Set the language value
     *
     * Description: A method of communication being used.
     *
     * @param value
     *          The language value.
     * @return The builder for this class.
     */
    public Builder language(String value) {
      this.dataFields.put("language", value);
      return this;
    }

    /**
     * Set the header4 value
     *
     * Description: Header 4
     *
     * @param value
     *          The header4 value.
     * @return The builder for this class.
     */
    public Builder header4(String value) {
      this.dataFields.put("header4", value);
      return this;
    }

    /**
     * Set the header3 value
     *
     * Description: Header 3
     *
     * @param value
     *          The header3 value.
     * @return The builder for this class.
     */
    public Builder header3(String value) {
      this.dataFields.put("header3", value);
      return this;
    }

    /**
     * Set the header2 value
     *
     * Description: Header 2
     *
     * @param value
     *          The header2 value.
     * @return The builder for this class.
     */
    public Builder header2(String value) {
      this.dataFields.put("header2", value);
      return this;
    }

    /**
     * Set the contentType value
     *
     * Description: Content Type
     *
     * @param value
     *          The contentType value.
     * @return The builder for this class.
     */
    public Builder contentType(String value) {
      this.dataFields.put("contentType", value);
      return this;
    }

    /**
     * Set the footer value
     *
     * Description: Footer
     *
     * @param value
     *          The footer value.
     * @return The builder for this class.
     */
    public Builder footer(String value) {
      this.dataFields.put("footer", value);
      return this;
    }

    /**
     * Set the suffix value
     *
     * Description: One or many characters which are added at the end of a statement or number.
     *
     * @param value
     *          The suffix value.
     * @return The builder for this class.
     */
    public Builder suffix(String value) {
      this.dataFields.put("suffix", value);
      return this;
    }

    /**
     * Set the header1 value
     *
     * Description: Header 1
     *
     * @param value
     *          The header1 value.
     * @return The builder for this class.
     */
    public Builder header1(String value) {
      this.dataFields.put("header1", value);
      return this;
    }

    /**
     * Set the extension value
     *
     * Description: Extension
     *
     * @param value
     *          The extension value.
     * @return The builder for this class.
     */
    public Builder extension(String value) {
      this.dataFields.put("extension", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public DocumentsData build() {
      return new DocumentsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private DocumentsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
