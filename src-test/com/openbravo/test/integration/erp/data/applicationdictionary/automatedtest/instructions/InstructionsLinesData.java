/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:10
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.automatedtest.instructions;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for InstructionsLinesData
 *
 * @author plujan
 *
 */
public class InstructionsLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the help3 value
     *
     * Description: A step by step help description to guide the user through processes and tasks.
     *
     * @param value
     *          The help3 value.
     * @return The builder for this class.
     */
    public Builder help3(String value) {
      this.dataFields.put("help3", value);
      return this;
    }

    /**
     * Set the help2 value
     *
     * Description: A step by step help description to guide the user through processes and tasks.
     *
     * @param value
     *          The help2 value.
     * @return The builder for this class.
     */
    public Builder help2(String value) {
      this.dataFields.put("help2", value);
      return this;
    }

    /**
     * Set the help1 value
     *
     * Description: A step by step help description to guide the user through processes and tasks.
     *
     * @param value
     *          The help1 value.
     * @return The builder for this class.
     */
    public Builder help1(String value) {
      this.dataFields.put("help1", value);
      return this;
    }

    /**
     * Set the childTest value
     *
     * Description: Child Test
     *
     * @param value
     *          The childTest value.
     * @return The builder for this class.
     */
    public Builder childTest(String value) {
      this.dataFields.put("childTest", value);
      return this;
    }

    /**
     * Set the type value
     *
     * Description: A distinct item characteristic used for processes and sometimes grouped within a
     * category.
     *
     * @param value
     *          The type value.
     * @return The builder for this class.
     */
    public Builder type(String value) {
      this.dataFields.put("type", value);
      return this;
    }

    /**
     * Set the arg3 value
     *
     * Description: Arg. 3
     *
     * @param value
     *          The arg3 value.
     * @return The builder for this class.
     */
    public Builder arg3(String value) {
      this.dataFields.put("arg3", value);
      return this;
    }

    /**
     * Set the arg2 value
     *
     * Description: Arg. 2
     *
     * @param value
     *          The arg2 value.
     * @return The builder for this class.
     */
    public Builder arg2(String value) {
      this.dataFields.put("arg2", value);
      return this;
    }

    /**
     * Set the arg1 value
     *
     * Description: Arg. 1
     *
     * @param value
     *          The arg1 value.
     * @return The builder for this class.
     */
    public Builder arg1(String value) {
      this.dataFields.put("arg1", value);
      return this;
    }

    /**
     * Set the sequenceNumber value
     *
     * Description: The order of records in a specified document.
     *
     * @param value
     *          The sequenceNumber value.
     * @return The builder for this class.
     */
    public Builder sequenceNumber(String value) {
      this.dataFields.put("sequenceNumber", value);
      return this;
    }

    /**
     * Set the command value
     *
     * Description: The identifier of a table.
     *
     * @param value
     *          The command value.
     * @return The builder for this class.
     */
    public Builder command(String value) {
      this.dataFields.put("command", value);
      return this;
    }

    /**
     * Set the test value
     *
     * Description: Test
     *
     * @param value
     *          The test value.
     * @return The builder for this class.
     */
    public Builder test(String value) {
      this.dataFields.put("test", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public InstructionsLinesData build() {
      return new InstructionsLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private InstructionsLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
