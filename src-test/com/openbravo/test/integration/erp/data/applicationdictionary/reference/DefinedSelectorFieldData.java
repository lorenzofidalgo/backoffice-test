/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.reference;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for DefinedSelectorFieldData
 *
 * @author plujan
 *
 */
public class DefinedSelectorFieldData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the property value
     *
     * Description: Path/name of the property to show in the selector
     *
     * @param value
     *          The property value.
     * @return The builder for this class.
     */
    public Builder property(String value) {
      this.dataFields.put("property", value);
      return this;
    }

    /**
     * Set the sortno value
     *
     * Description: Defines the sorting of columns in a grid
     *
     * @param value
     *          The sortno value.
     * @return The builder for this class.
     */
    public Builder sortno(String value) {
      this.dataFields.put("sortno", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the obserdsDatasourceFieldID value
     *
     * Description: The datasource field is part of the datasource definition.
     *
     * @param value
     *          The obserdsDatasourceFieldID value.
     * @return The builder for this class.
     */
    public Builder obserdsDatasourceFieldID(String value) {
      this.dataFields.put("obserdsDatasourceFieldID", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the searchinsuggestionbox value
     *
     * Description: Defines if the field is used to search when retrieving values in the suggestion
     * box.
     *
     * @param value
     *          The searchinsuggestionbox value.
     * @return The builder for this class.
     */
    public Builder searchinsuggestionbox(Boolean value) {
      this.dataFields.put("searchinsuggestionbox", value);
      return this;
    }

    /**
     * Set the obuiselSelectorID value
     *
     * Description: The unique identifying key for the selector.
     *
     * @param value
     *          The obuiselSelectorID value.
     * @return The builder for this class.
     */
    public Builder obuiselSelectorID(String value) {
      this.dataFields.put("obuiselSelectorID", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the sortable value
     *
     * Description: Allow sorting by this field in the popup grid.
     *
     * @param value
     *          The sortable value.
     * @return The builder for this class.
     */
    public Builder sortable(Boolean value) {
      this.dataFields.put("sortable", value);
      return this;
    }

    /**
     * Set the filterable value
     *
     * Description: Allow filtering on this field.
     *
     * @param value
     *          The filterable value.
     * @return The builder for this class.
     */
    public Builder filterable(Boolean value) {
      this.dataFields.put("filterable", value);
      return this;
    }

    /**
     * Set the module value
     *
     * Description: Module
     *
     * @param value
     *          The module value.
     * @return The builder for this class.
     */
    public Builder module(String value) {
      this.dataFields.put("module", value);
      return this;
    }

    /**
     * Set the showingrid value
     *
     * Description: Defines if the field is shown in the popup grid.
     *
     * @param value
     *          The showingrid value.
     * @return The builder for this class.
     */
    public Builder showingrid(Boolean value) {
      this.dataFields.put("showingrid", value);
      return this;
    }

    /**
     * Set the defaultExpression value
     *
     * Description: Defines a expression used to filter the property
     *
     * @param value
     *          The defaultExpression value.
     * @return The builder for this class.
     */
    public Builder defaultExpression(String value) {
      this.dataFields.put("defaultExpression", value);
      return this;
    }

    /**
     * Set the isoutfield value
     *
     * Description: The value of this field should be returned to the opener window
     *
     * @param value
     *          The isoutfield value.
     * @return The builder for this class.
     */
    public Builder isoutfield(Boolean value) {
      this.dataFields.put("isoutfield", value);
      return this;
    }

    /**
     * Set the helpComment value
     *
     * Description: A comment that adds additional information to help users work with fields.
     *
     * @param value
     *          The helpComment value.
     * @return The builder for this class.
     */
    public Builder helpComment(String value) {
      this.dataFields.put("helpComment", value);
      return this;
    }

    /**
     * Set the centralMaintenance value
     *
     * Description: A flag indicating that this label is managed in a central repository.
     *
     * @param value
     *          The centralMaintenance value.
     * @return The builder for this class.
     */
    public Builder centralMaintenance(Boolean value) {
      this.dataFields.put("centralMaintenance", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public DefinedSelectorFieldData build() {
      return new DefinedSelectorFieldData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private DefinedSelectorFieldData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
