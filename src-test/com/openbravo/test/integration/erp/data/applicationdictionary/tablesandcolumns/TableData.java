/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.tablesandcolumns;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for TableData
 *
 * @author plujan
 *
 */
public class TableData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the dataPackage value
     *
     * Description: Links a table to a package
     *
     * @param value
     *          The dataPackage value.
     * @return The builder for this class.
     */
    public Builder dataPackage(String value) {
      this.dataFields.put("dataPackage", value);
      return this;
    }

    /**
     * Set the treeType value
     *
     * Description: Element this tree is built on (i.e Product Business Partner)
     *
     * @param value
     *          The treeType value.
     * @return The builder for this class.
     */
    public Builder treeType(String value) {
      this.dataFields.put("treeType", value);
      return this;
    }

    /**
     * Set the isFullyAudited value
     *
     * Description: Maitain Audit Trail for this table
     *
     * @param value
     *          The isFullyAudited value.
     * @return The builder for this class.
     */
    public Builder isFullyAudited(Boolean value) {
      this.dataFields.put("isFullyAudited", value);
      return this;
    }

    /**
     * Set the acctdateColumnID value
     *
     * Description: Acctdate_Column_ID
     *
     * @param value
     *          The acctdateColumnID value.
     * @return The builder for this class.
     */
    public Builder acctdateColumnID(String value) {
      this.dataFields.put("acctdateColumnID", value);
      return this;
    }

    /**
     * Set the acctclassname value
     *
     * Description: Acctclassname
     *
     * @param value
     *          The acctclassname value.
     * @return The builder for this class.
     */
    public Builder acctclassname(String value) {
      this.dataFields.put("acctclassname", value);
      return this;
    }

    /**
     * Set the javaClassName value
     *
     * Description: x not implemented
     *
     * @param value
     *          The javaClassName value.
     * @return The builder for this class.
     */
    public Builder javaClassName(String value) {
      this.dataFields.put("javaClassName", value);
      return this;
    }

    /**
     * Set the developmentStatus value
     *
     * Description: Development Status
     *
     * @param value
     *          The developmentStatus value.
     * @return The builder for this class.
     */
    public Builder developmentStatus(String value) {
      this.dataFields.put("developmentStatus", value);
      return this;
    }

    /**
     * Set the pOWindow value
     *
     * Description: Purchase Order Window
     *
     * @param value
     *          The pOWindow value.
     * @return The builder for this class.
     */
    public Builder pOWindow(String value) {
      this.dataFields.put("pOWindow", value);
      return this;
    }

    /**
     * Set the view value
     *
     * Description: This is a view
     *
     * @param value
     *          The view value.
     * @return The builder for this class.
     */
    public Builder view(Boolean value) {
      this.dataFields.put("view", value);
      return this;
    }

    /**
     * Set the highVolume value
     *
     * Description: Use Search instead of Pick list
     *
     * @param value
     *          The highVolume value.
     * @return The builder for this class.
     */
    public Builder highVolume(Boolean value) {
      this.dataFields.put("highVolume", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the deletableRecords value
     *
     * Description: Indicates if records can be deleted from the database
     *
     * @param value
     *          The deletableRecords value.
     * @return The builder for this class.
     */
    public Builder deletableRecords(Boolean value) {
      this.dataFields.put("deletableRecords", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the dataAccessLevel value
     *
     * Description: Indicates what type of data (in terms of AD_CLIENT and AD_ORG data columns) can
     * be entered or viewed.
     *
     * @param value
     *          The dataAccessLevel value.
     * @return The builder for this class.
     */
    public Builder dataAccessLevel(String value) {
      this.dataFields.put("dataAccessLevel", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the dBTableName value
     *
     * Description: Name of the table in the database
     *
     * @param value
     *          The dBTableName value.
     * @return The builder for this class.
     */
    public Builder dBTableName(String value) {
      this.dataFields.put("dBTableName", value);
      return this;
    }

    /**
     * Set the window value
     *
     * Description: A work area which can be used to create view edit and process a record.
     *
     * @param value
     *          The window value.
     * @return The builder for this class.
     */
    public Builder window(String value) {
      this.dataFields.put("window", value);
      return this;
    }

    /**
     * Set the helpComment value
     *
     * Description: A comment that adds additional information to help users work with fields.
     *
     * @param value
     *          The helpComment value.
     * @return The builder for this class.
     */
    public Builder helpComment(String value) {
      this.dataFields.put("helpComment", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public TableData build() {
      return new TableData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private TableData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
