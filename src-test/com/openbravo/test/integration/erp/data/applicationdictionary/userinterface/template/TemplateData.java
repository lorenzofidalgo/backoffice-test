/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.userinterface.template;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for TemplateData
 *
 * @author plujan
 *
 */
public class TemplateData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the template value
     *
     * Description: The template source code.
     *
     * @param value
     *          The template value.
     * @return The builder for this class.
     */
    public Builder template(String value) {
      this.dataFields.put("template", value);
      return this;
    }

    /**
     * Set the templateClasspathLocation value
     *
     * Description: Defines the location in the classpath of the template file
     *
     * @param value
     *          The templateClasspathLocation value.
     * @return The builder for this class.
     */
    public Builder templateClasspathLocation(String value) {
      this.dataFields.put("templateClasspathLocation", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the overridesTemplateID value
     *
     * Description: Is used to define that this template overrides another template.
     *
     * @param value
     *          The overridesTemplateID value.
     * @return The builder for this class.
     */
    public Builder overridesTemplateID(String value) {
      this.dataFields.put("overridesTemplateID", value);
      return this;
    }

    /**
     * Set the module value
     *
     * Description: Module
     *
     * @param value
     *          The module value.
     * @return The builder for this class.
     */
    public Builder module(String value) {
      this.dataFields.put("module", value);
      return this;
    }

    /**
     * Set the templateLanguage value
     *
     * Description: Defines the template language used to process this template.
     *
     * @param value
     *          The templateLanguage value.
     * @return The builder for this class.
     */
    public Builder templateLanguage(String value) {
      this.dataFields.put("templateLanguage", value);
      return this;
    }

    /**
     * Set the componentType value
     *
     * Description: A type of component for which the template operates.
     *
     * @param value
     *          The componentType value.
     * @return The builder for this class.
     */
    public Builder componentType(String value) {
      this.dataFields.put("componentType", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public TemplateData build() {
      return new TemplateData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private TemplateData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
