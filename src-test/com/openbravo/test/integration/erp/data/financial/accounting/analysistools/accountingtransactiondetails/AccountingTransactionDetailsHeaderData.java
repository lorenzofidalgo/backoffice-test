/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.analysistools.accountingtransactiondetails;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;

/**
 *
 * Class for AccountingTransactionDetailsHeaderData
 *
 * @author plujan
 *
 */
public class AccountingTransactionDetailsHeaderData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the storageBin value
     *
     * Description: A set of coordinates (x y z) which help locate an item in a warehouse.
     *
     * @param value
     *          The storageBin value.
     * @return The builder for this class.
     */
    public Builder storageBin(String value) {
      this.dataFields.put("storageBin", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the tax value
     *
     * Description: The percentage of money requested by the government for this specified product
     * or transaction.
     *
     * @param value
     *          The tax value.
     * @return The builder for this class.
     */
    public Builder tax(String value) {
      this.dataFields.put("tax", value);
      return this;
    }

    /**
     * Set the table value
     *
     * Description: A dictionary table used for this tab that points to the database table.
     *
     * @param value
     *          The table value.
     * @return The builder for this class.
     */
    public Builder table(String value) {
      this.dataFields.put("table", value);
      return this;
    }

    /**
     * Set the accountingDate value
     *
     * Description: The date this transaction is recorded for in the general ledger.
     *
     * @param value
     *          The accountingDate value.
     * @return The builder for this class.
     */
    public Builder accountingDate(String value) {
      this.dataFields.put("accountingDate", value);
      return this;
    }

    /**
     * Set the transactionDate value
     *
     * Description: The date that a specified transaction is entered into the application.
     *
     * @param value
     *          The transactionDate value.
     * @return The builder for this class.
     */
    public Builder transactionDate(String value) {
      this.dataFields.put("transactionDate", value);
      return this;
    }

    /**
     * Set the activity value
     *
     * Description: A distinct activity defined and used in activity based management.
     *
     * @param value
     *          The activity value.
     * @return The builder for this class.
     */
    public Builder activity(String value) {
      this.dataFields.put("activity", value);
      return this;
    }

    /**
     * Set the 2ndDimension value
     *
     * Description: A display of optional elements that are previously defined for this account
     * combination.
     *
     * @param value
     *          The 2ndDimension value.
     * @return The builder for this class.
     */
    public Builder secondDimension(String value) {
      this.dataFields.put("2ndDimension", value);
      return this;
    }

    /**
     * Set the 1stDimension value
     *
     * Description: A display of optional elements that are previously defined for this account
     * combination.
     *
     * @param value
     *          The 1stDimension value.
     * @return The builder for this class.
     */
    public Builder firstDimension(String value) {
      this.dataFields.put("1stDimension", value);
      return this;
    }

    /**
     * Set the salesCampaign value
     *
     * Description: An advertising effort aimed at increasing sales.
     *
     * @param value
     *          The salesCampaign value.
     * @return The builder for this class.
     */
    public Builder salesCampaign(String value) {
      this.dataFields.put("salesCampaign", value);
      return this;
    }

    /**
     * Set the quantity value
     *
     * Description: The number of a certain item.
     *
     * @param value
     *          The quantity value.
     * @return The builder for this class.
     */
    public Builder quantity(String value) {
      this.dataFields.put("quantity", value);
      return this;
    }

    /**
     * Set the postingType value
     *
     * Description: A distinct posting amount characteristic used for processes and sometimes
     * grouped within a category.
     *
     * @param value
     *          The postingType value.
     * @return The builder for this class.
     */
    public Builder postingType(String value) {
      this.dataFields.put("postingType", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the gLCategory value
     *
     * Description: A classification used to group lines in the general ledger.
     *
     * @param value
     *          The gLCategory value.
     * @return The builder for this class.
     */
    public Builder gLCategory(String value) {
      this.dataFields.put("gLCategory", value);
      return this;
    }

    /**
     * Set the uOM value
     *
     * Description: A non monetary unit of measure.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the salesRegion value
     *
     * Description: A defined section of the world where sales efforts will be focused.
     *
     * @param value
     *          The salesRegion value.
     * @return The builder for this class.
     */
    public Builder salesRegion(String value) {
      this.dataFields.put("salesRegion", value);
      return this;
    }

    /**
     * Set the project value
     *
     * Description: Identifier of a project defined within the Project Service Management module.
     *
     * @param value
     *          The project value.
     * @return The builder for this class.
     */
    public Builder project(String value) {
      this.dataFields.put("project", value);
      return this;
    }

    /**
     * Set the period value
     *
     * Description: A specified time period.
     *
     * @param value
     *          The period value.
     * @return The builder for this class.
     */
    public Builder period(String value) {
      this.dataFields.put("period", value);
      return this;
    }

    /**
     * Set the locationToAddress value
     *
     * Description: The location where items are sent to.
     *
     * @param value
     *          The locationToAddress value.
     * @return The builder for this class.
     */
    public Builder locationToAddress(String value) {
      this.dataFields.put("locationToAddress", value);
      return this;
    }

    /**
     * Set the locationFromAddress value
     *
     * Description: The location where items are sent from.
     *
     * @param value
     *          The locationFromAddress value.
     * @return The builder for this class.
     */
    public Builder locationFromAddress(String value) {
      this.dataFields.put("locationFromAddress", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the accountingSchema value
     *
     * Description: The structure used in accounting including costing methods currencies and the
     * calendar.
     *
     * @param value
     *          The accountingSchema value.
     * @return The builder for this class.
     */
    public Builder accountingSchema(String value) {
      this.dataFields.put("accountingSchema", value);
      return this;
    }

    /**
     * Set the foreignCurrencyDebit value
     *
     * Description: The amount debited from the account given in provider currency.
     *
     * @param value
     *          The foreignCurrencyDebit value.
     * @return The builder for this class.
     */
    public Builder foreignCurrencyDebit(String value) {
      this.dataFields.put("foreignCurrencyDebit", value);
      return this;
    }

    /**
     * Set the foreignCurrencyCredit value
     *
     * Description: The amount credited from the account given in provider currency.
     *
     * @param value
     *          The foreignCurrencyCredit value.
     * @return The builder for this class.
     */
    public Builder foreignCurrencyCredit(String value) {
      this.dataFields.put("foreignCurrencyCredit", value);
      return this;
    }

    /**
     * Set the debit value
     *
     * Description: The amount debited to an account converted to the organization default currency.
     *
     * @param value
     *          The debit value.
     * @return The builder for this class.
     */
    public Builder debit(String value) {
      this.dataFields.put("debit", value);
      return this;
    }

    /**
     * Set the credit value
     *
     * Description: The amount credited to an account converted to the organization default
     * currency.
     *
     * @param value
     *          The credit value.
     * @return The builder for this class.
     */
    public Builder credit(String value) {
      this.dataFields.put("credit", value);
      return this;
    }

    /**
     * Set the account value
     *
     * Description: The identification code used for accounting.
     *
     * @param value
     *          The account value.
     * @return The builder for this class.
     */
    public Builder account(String value) {
      this.dataFields.put("account", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AccountingTransactionDetailsHeaderData build() {
      return new AccountingTransactionDetailsHeaderData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AccountingTransactionDetailsHeaderData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
