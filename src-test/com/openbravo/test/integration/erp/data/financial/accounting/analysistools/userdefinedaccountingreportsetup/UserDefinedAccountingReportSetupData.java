/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.analysistools.userdefinedaccountingreportsetup;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for UserDefinedAccountingReportSetupData
 *
 * @author plujan
 *
 */
public class UserDefinedAccountingReportSetupData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the yearInitialBalance value
     *
     * Description: If checked the system will automatically calculate the opening balance for the
     * fiscal year (regardless of whether the period is monthly or quarterly).
     *
     * @param value
     *          The yearInitialBalance value.
     * @return The builder for this class.
     */
    public Builder yearInitialBalance(Boolean value) {
      this.dataFields.put("yearInitialBalance", value);
      return this;
    }

    /**
     * Set the report value
     *
     * Description: An indication whether something is a document or a report which summarizes
     * information.
     *
     * @param value
     *          The report value.
     * @return The builder for this class.
     */
    public Builder report(Boolean value) {
      this.dataFields.put("report", value);
      return this;
    }

    /**
     * Set the reportType value
     *
     * Description: Report Type
     *
     * @param value
     *          The reportType value.
     * @return The builder for this class.
     */
    public Builder reportType(String value) {
      this.dataFields.put("reportType", value);
      return this;
    }

    /**
     * Set the accountingSchema value
     *
     * Description: The structure used in accounting including costing methods currencies and the
     * calendar.
     *
     * @param value
     *          The accountingSchema value.
     * @return The builder for this class.
     */
    public Builder accountingSchema(String value) {
      this.dataFields.put("accountingSchema", value);
      return this;
    }

    /**
     * Set the account value
     *
     * Description: The identification code used for accounting.
     *
     * @param value
     *          The account value.
     * @return The builder for this class.
     */
    public Builder account(String value) {
      this.dataFields.put("account", value);
      return this;
    }

    /**
     * Set the reportingInterval value
     *
     * Description: Reporting Interval
     *
     * @param value
     *          The reportingInterval value.
     * @return The builder for this class.
     */
    public Builder reportingInterval(String value) {
      this.dataFields.put("reportingInterval", value);
      return this;
    }

    /**
     * Set the filteredByOrganization value
     *
     * Description: Filtered by Organization
     *
     * @param value
     *          The filteredByOrganization value.
     * @return The builder for this class.
     */
    public Builder filteredByOrganization(Boolean value) {
      this.dataFields.put("filteredByOrganization", value);
      return this;
    }

    /**
     * Set the shown value
     *
     * Description: Shown.
     *
     * @param value
     *          The shown value.
     * @return The builder for this class.
     */
    public Builder shown(Boolean value) {
      this.dataFields.put("shown", value);
      return this;
    }

    /**
     * Set the summaryLevel value
     *
     * Description: A means of grouping fields in order to view or hide additional information.
     *
     * @param value
     *          The summaryLevel value.
     * @return The builder for this class.
     */
    public Builder summaryLevel(Boolean value) {
      this.dataFields.put("summaryLevel", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public UserDefinedAccountingReportSetupData build() {
      return new UserDefinedAccountingReportSetupData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private UserDefinedAccountingReportSetupData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
