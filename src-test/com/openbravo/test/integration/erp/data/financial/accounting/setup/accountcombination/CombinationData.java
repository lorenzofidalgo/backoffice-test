/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.setup.accountcombination;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;

/**
 *
 * Class for CombinationData
 *
 * @author plujan
 *
 */
public class CombinationData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the activity value
     *
     * Description: A distinct activity defined and used in activity based management.
     *
     * @param value
     *          The activity value.
     * @return The builder for this class.
     */
    public Builder activity(String value) {
      this.dataFields.put("activity", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the salesCampaign value
     *
     * Description: An advertising effort aimed at increasing sales.
     *
     * @param value
     *          The salesCampaign value.
     * @return The builder for this class.
     */
    public Builder salesCampaign(String value) {
      this.dataFields.put("salesCampaign", value);
      return this;
    }

    /**
     * Set the project value
     *
     * Description: Identifier of a project defined within the Project Service Management module.
     *
     * @param value
     *          The project value.
     * @return The builder for this class.
     */
    public Builder project(String value) {
      this.dataFields.put("project", value);
      return this;
    }

    /**
     * Set the salesRegion value
     *
     * Description: A defined section of the world where sales efforts will be focused.
     *
     * @param value
     *          The salesRegion value.
     * @return The builder for this class.
     */
    public Builder salesRegion(String value) {
      this.dataFields.put("salesRegion", value);
      return this;
    }

    /**
     * Set the locationToAddress value
     *
     * Description: The location where items are sent to.
     *
     * @param value
     *          The locationToAddress value.
     * @return The builder for this class.
     */
    public Builder locationToAddress(String value) {
      this.dataFields.put("locationToAddress", value);
      return this;
    }

    /**
     * Set the locationFromAddress value
     *
     * Description: The location where items are sent from.
     *
     * @param value
     *          The locationFromAddress value.
     * @return The builder for this class.
     */
    public Builder locationFromAddress(String value) {
      this.dataFields.put("locationFromAddress", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductCompleteSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the fullyQualified value
     *
     * Description: This account is fully qualified
     *
     * @param value
     *          The fullyQualified value.
     * @return The builder for this class.
     */
    public Builder fullyQualified(Boolean value) {
      this.dataFields.put("fullyQualified", value);
      return this;
    }

    /**
     * Set the combination value
     *
     * Description: Unique combination of account elements
     *
     * @param value
     *          The combination value.
     * @return The builder for this class.
     */
    public Builder combination(String value) {
      this.dataFields.put("combination", value);
      return this;
    }

    /**
     * Set the alias value
     *
     * Description: Defines an alternate method of indicating an account combination.
     *
     * @param value
     *          The alias value.
     * @return The builder for this class.
     */
    public Builder alias(String value) {
      this.dataFields.put("alias", value);
      return this;
    }

    /**
     * Set the 1stDimension value
     *
     * Description: A display of optional elements that are previously defined for this account
     * combination.
     *
     * @param value
     *          The 1stDimension value.
     * @return The builder for this class.
     */
    public Builder firstDimension(String value) {
      this.dataFields.put("1stDimension", value);
      return this;
    }

    /**
     * Set the account value
     *
     * Description: The identification code used for accounting.
     *
     * @param value
     *          The account value.
     * @return The builder for this class.
     */
    public Builder account(String value) {
      this.dataFields.put("account", value);
      return this;
    }

    /**
     * Set the accountingSchema value
     *
     * Description: The structure used in accounting including costing methods currencies and the
     * calendar.
     *
     * @param value
     *          The accountingSchema value.
     * @return The builder for this class.
     */
    public Builder accountingSchema(String value) {
      this.dataFields.put("accountingSchema", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the 2ndDimension value
     *
     * Description: A display of optional elements that are previously defined for this account
     * combination.
     *
     * @param value
     *          The 2ndDimension value.
     * @return The builder for this class.
     */
    public Builder secondDimension(String value) {
      this.dataFields.put("2ndDimension", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public CombinationData build() {
      return new CombinationData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private CombinationData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
