/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.setup.accountingschema;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for DocumentsData
 *
 * @author plujan
 *
 */
public class DocumentsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the aDCreatefactTemplateID value
     *
     * Description: Accounting Template
     *
     * @param value
     *          The aDCreatefactTemplateID value.
     * @return The builder for this class.
     */
    public Builder aDCreatefactTemplateID(String value) {
      this.dataFields.put("aDCreatefactTemplateID", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the documentCategory value
     *
     * Description: A classification of document types that are shown and processed in the same
     * window.
     *
     * @param value
     *          The documentCategory value.
     * @return The builder for this class.
     */
    public Builder documentCategory(String value) {
      this.dataFields.put("documentCategory", value);
      return this;
    }

    /**
     * Set the allowNegative value
     *
     * Description: Allow negative entries in credit and debit
     *
     * @param value
     *          The allowNegative value.
     * @return The builder for this class.
     */
    public Builder allowNegative(Boolean value) {
      this.dataFields.put("allowNegative", value);
      return this;
    }

    /**
     * Set the acctschemaTable value
     *
     * Description: Acctschema table
     *
     * @param value
     *          The acctschemaTable value.
     * @return The builder for this class.
     */
    public Builder acctschemaTable(String value) {
      this.dataFields.put("acctschemaTable", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public DocumentsData build() {
      return new DocumentsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private DocumentsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
