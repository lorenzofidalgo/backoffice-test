/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.setup.accountingschema;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.AccountSelectorData;

/**
 *
 * Class for GeneralLedgerData
 *
 * @author plujan
 *
 */
public class GeneralLedgerData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the cFSOrderAccount value
     *
     * Description: Cash Flow Statement Order Account
     *
     * @param value
     *          The cFSOrderAccount value.
     * @return The builder for this class.
     */
    public Builder cFSOrderAccount(AccountSelectorData value) {
      this.dataFields.put("cFSOrderAccount", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the incomeSummary value
     *
     * Description: Income Summary Account
     *
     * @param value
     *          The incomeSummary value.
     * @return The builder for this class.
     */
    public Builder incomeSummary(AccountSelectorData value) {
      this.dataFields.put("incomeSummary", value);
      return this;
    }

    /**
     * Set the retainedEarning value
     *
     * Description: Retained Earning Account
     *
     * @param value
     *          The retainedEarning value.
     * @return The builder for this class.
     */
    public Builder retainedEarning(AccountSelectorData value) {
      this.dataFields.put("retainedEarning", value);
      return this;
    }

    /**
     * Set the currencyBalancingUse value
     *
     * Description: Use Currency Balancing
     *
     * @param value
     *          The currencyBalancingUse value.
     * @return The builder for this class.
     */
    public Builder currencyBalancingUse(Boolean value) {
      this.dataFields.put("currencyBalancingUse", value);
      return this;
    }

    /**
     * Set the suspenseError value
     *
     * Description: Suspense Error Account
     *
     * @param value
     *          The suspenseError value.
     * @return The builder for this class.
     */
    public Builder suspenseError(AccountSelectorData value) {
      this.dataFields.put("suspenseError", value);
      return this;
    }

    /**
     * Set the suspenseErrorUse value
     *
     * Description: Use Suspense Error
     *
     * @param value
     *          The suspenseErrorUse value.
     * @return The builder for this class.
     */
    public Builder suspenseErrorUse(Boolean value) {
      this.dataFields.put("suspenseErrorUse", value);
      return this;
    }

    /**
     * Set the suspenseBalancing value
     *
     * Description: Suspense Balancing Account
     *
     * @param value
     *          The suspenseBalancing value.
     * @return The builder for this class.
     */
    public Builder suspenseBalancing(AccountSelectorData value) {
      this.dataFields.put("suspenseBalancing", value);
      return this;
    }

    /**
     * Set the suspenseBalancingUse value
     *
     * Description: Use Suspense Balancing
     *
     * @param value
     *          The suspenseBalancingUse value.
     * @return The builder for this class.
     */
    public Builder suspenseBalancingUse(Boolean value) {
      this.dataFields.put("suspenseBalancingUse", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the accountingSchema value
     *
     * Description: The structure used in accounting including costing methods currencies and the
     * calendar.
     *
     * @param value
     *          The accountingSchema value.
     * @return The builder for this class.
     */
    public Builder accountingSchema(String value) {
      this.dataFields.put("accountingSchema", value);
      return this;
    }

    /**
     * Set the currencyBalancingAcct value
     *
     * Description: Account used when a currency is out of balance
     *
     * @param value
     *          The currencyBalancingAcct value.
     * @return The builder for this class.
     */
    public Builder currencyBalancingAcct(AccountSelectorData value) {
      this.dataFields.put("currencyBalancingAcct", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public GeneralLedgerData build() {
      return new GeneralLedgerData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private GeneralLedgerData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
