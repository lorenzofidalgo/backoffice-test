/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.setup.accounttree;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for CustomizedElementsData
 *
 * @author plujan
 *
 */
public class CustomizedElementsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the sequenceNumber value
     *
     * Description: The order of records in a specified document.
     *
     * @param value
     *          The sequenceNumber value.
     * @return The builder for this class.
     */
    public Builder sequenceNumber(String value) {
      this.dataFields.put("sequenceNumber", value);
      return this;
    }

    /**
     * Set the account value
     *
     * Description: The identification code used for accounting.
     *
     * @param value
     *          The account value.
     * @return The builder for this class.
     */
    public Builder account(String value) {
      this.dataFields.put("account", value);
      return this;
    }

    /**
     * Set the accountElement value
     *
     * Description: A identification code for an account type.
     *
     * @param value
     *          The accountElement value.
     * @return The builder for this class.
     */
    public Builder accountElement(String value) {
      this.dataFields.put("accountElement", value);
      return this;
    }

    /**
     * Set the sign value
     *
     * Description: Sign
     *
     * @param value
     *          The sign value.
     * @return The builder for this class.
     */
    public Builder sign(String value) {
      this.dataFields.put("sign", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public CustomizedElementsData build() {
      return new CustomizedElementsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private CustomizedElementsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
