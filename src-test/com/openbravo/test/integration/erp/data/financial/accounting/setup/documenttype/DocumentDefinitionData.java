/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.setup.documenttype;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for DocumentDefinitionData
 *
 * @author plujan
 *
 */
public class DocumentDefinitionData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the documentTypeForShipment value
     *
     * Description: Document type used for shipments generated from this sales document
     *
     * @param value
     *          The documentTypeForShipment value.
     * @return The builder for this class.
     */
    public Builder documentTypeForShipment(String value) {
      this.dataFields.put("documentTypeForShipment", value);
      return this;
    }

    /**
     * Set the documentSequence value
     *
     * Description: Document sequence determines the numbering of documents
     *
     * @param value
     *          The documentSequence value.
     * @return The builder for this class.
     */
    public Builder documentSequence(String value) {
      this.dataFields.put("documentSequence", value);
      return this;
    }

    /**
     * Set the documentCancelled value
     *
     * Description: Document Cancelled
     *
     * @param value
     *          The documentCancelled value.
     * @return The builder for this class.
     */
    public Builder documentCancelled(String value) {
      this.dataFields.put("documentCancelled", value);
      return this;
    }

    /**
     * Set the table value
     *
     * Description: A dictionary table used for this tab that points to the database table.
     *
     * @param value
     *          The table value.
     * @return The builder for this class.
     */
    public Builder table(String value) {
      this.dataFields.put("table", value);
      return this;
    }

    /**
     * Set the salesTransaction value
     *
     * Description: An indication that a transfer of goods and money between business partners is
     * occurring.
     *
     * @param value
     *          The salesTransaction value.
     * @return The builder for this class.
     */
    public Builder salesTransaction(Boolean value) {
      this.dataFields.put("salesTransaction", value);
      return this;
    }

    /**
     * Set the defaultValue value
     *
     * Description: A value that is shown whenever a record is created.
     *
     * @param value
     *          The defaultValue value.
     * @return The builder for this class.
     */
    public Builder defaultValue(Boolean value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Set the documentTypeForInvoice value
     *
     * Description: Document type used for invoices generated from this sales document
     *
     * @param value
     *          The documentTypeForInvoice value.
     * @return The builder for this class.
     */
    public Builder documentTypeForInvoice(String value) {
      this.dataFields.put("documentTypeForInvoice", value);
      return this;
    }

    /**
     * Set the sOSubType value
     *
     * Description: Sales Order Sub Type
     *
     * @param value
     *          The sOSubType value.
     * @return The builder for this class.
     */
    public Builder sOSubType(String value) {
      this.dataFields.put("sOSubType", value);
      return this;
    }

    /**
     * Set the printText value
     *
     * Description: The displayed text of an element.
     *
     * @param value
     *          The printText value.
     * @return The builder for this class.
     */
    public Builder printText(String value) {
      this.dataFields.put("printText", value);
      return this;
    }

    /**
     * Set the documentCategory value
     *
     * Description: A classification of document types that are shown and processed in the same
     * window.
     *
     * @param value
     *          The documentCategory value.
     * @return The builder for this class.
     */
    public Builder documentCategory(String value) {
      this.dataFields.put("documentCategory", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the gLCategory value
     *
     * Description: A classification used to group lines in the general ledger.
     *
     * @param value
     *          The gLCategory value.
     * @return The builder for this class.
     */
    public Builder gLCategory(String value) {
      this.dataFields.put("gLCategory", value);
      return this;
    }

    /**
     * Set the sequencedDocument value
     *
     * Description: The document has a document sequence
     *
     * @param value
     *          The sequencedDocument value.
     * @return The builder for this class.
     */
    public Builder sequencedDocument(Boolean value) {
      this.dataFields.put("sequencedDocument", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public DocumentDefinitionData build() {
      return new DocumentDefinitionData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private DocumentDefinitionData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
