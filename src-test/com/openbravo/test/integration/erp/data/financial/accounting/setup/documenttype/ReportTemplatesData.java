/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.setup.documenttype;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ReportTemplatesData
 *
 * @author plujan
 *
 */
public class ReportTemplatesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the showcompanydata value
     *
     * Description: Showcompanydata
     *
     * @param value
     *          The showcompanydata value.
     * @return The builder for this class.
     */
    public Builder showcompanydata(Boolean value) {
      this.dataFields.put("showcompanydata", value);
      return this;
    }

    /**
     * Set the headermargin value
     *
     * Description: Headermargin
     *
     * @param value
     *          The headermargin value.
     * @return The builder for this class.
     */
    public Builder headermargin(String value) {
      this.dataFields.put("headermargin", value);
      return this;
    }

    /**
     * Set the showlogo value
     *
     * Description: Showlogo
     *
     * @param value
     *          The showlogo value.
     * @return The builder for this class.
     */
    public Builder showlogo(Boolean value) {
      this.dataFields.put("showlogo", value);
      return this;
    }

    /**
     * Set the templateLocation value
     *
     * Description: The location of the template
     *
     * @param value
     *          The templateLocation value.
     * @return The builder for this class.
     */
    public Builder templateLocation(String value) {
      this.dataFields.put("templateLocation", value);
      return this;
    }

    /**
     * Set the templateFilename value
     *
     * Description: Name of the template
     *
     * @param value
     *          The templateFilename value.
     * @return The builder for this class.
     */
    public Builder templateFilename(String value) {
      this.dataFields.put("templateFilename", value);
      return this;
    }

    /**
     * Set the reportFilename value
     *
     * Description: The name of the generated file
     *
     * @param value
     *          The reportFilename value.
     * @return The builder for this class.
     */
    public Builder reportFilename(String value) {
      this.dataFields.put("reportFilename", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ReportTemplatesData build() {
      return new ReportTemplatesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ReportTemplatesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
