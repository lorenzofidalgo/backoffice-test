/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.setup.taxrate;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for TaxData
 *
 * @author plujan
 *
 */
public class TaxData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the docTaxAmount value
     *
     * Description: Document tax amount calculation criteria
     *
     * @param value
     *          The docTaxAmount value.
     * @return The builder for this class.
     */
    public Builder docTaxAmount(String value) {
      this.dataFields.put("docTaxAmount", value);
      return this;
    }

    /**
     * Set the istaxdeductable value
     *
     * Description: If active this rate will behave as a tax deductible rate no matter if the
     * organization where used is tax deductible or not.
     *
     * @param value
     *          The istaxdeductable value.
     * @return The builder for this class.
     */
    public Builder istaxdeductable(Boolean value) {
      this.dataFields.put("istaxdeductable", value);
      return this;
    }

    /**
     * Set the taxBase value
     *
     * Description: Tax rate used as tax base amount for tax calculation
     *
     * @param value
     *          The taxBase value.
     * @return The builder for this class.
     */
    public Builder taxBase(String value) {
      this.dataFields.put("taxBase", value);
      return this;
    }

    /**
     * Set the baseAmount value
     *
     * Description: Base Amount to be used for the calculation of the tax amount.
     *
     * @param value
     *          The baseAmount value.
     * @return The builder for this class.
     */
    public Builder baseAmount(String value) {
      this.dataFields.put("baseAmount", value);
      return this;
    }

    /**
     * Set the notTaxable value
     *
     * Description: Check if it is not taxable
     *
     * @param value
     *          The notTaxable value.
     * @return The builder for this class.
     */
    public Builder notTaxable(Boolean value) {
      this.dataFields.put("notTaxable", value);
      return this;
    }

    /**
     * Set the notTaxDeductable value
     *
     * Description: Tax Not Deductible
     *
     * @param value
     *          The notTaxDeductable value.
     * @return The builder for this class.
     */
    public Builder notTaxDeductable(Boolean value) {
      this.dataFields.put("notTaxDeductable", value);
      return this;
    }

    /**
     * Set the deductableRate value
     *
     * Description: Deductable Rate
     *
     * @param value
     *          The deductableRate value.
     * @return The builder for this class.
     */
    public Builder deductableRate(String value) {
      this.dataFields.put("deductableRate", value);
      return this;
    }

    /**
     * Set the withholdingTax value
     *
     * Description: Is withholding tax
     *
     * @param value
     *          The withholdingTax value.
     * @return The builder for this class.
     */
    public Builder withholdingTax(Boolean value) {
      this.dataFields.put("withholdingTax", value);
      return this;
    }

    /**
     * Set the cascade value
     *
     * Description: Any additional discount built upon the remaining total after applying previous
     * discounts.
     *
     * @param value
     *          The cascade value.
     * @return The builder for this class.
     */
    public Builder cascade(Boolean value) {
      this.dataFields.put("cascade", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the businessPartnerTaxCategory value
     *
     * Description: Bussines Partner Tax Category as a Vendor
     *
     * @param value
     *          The businessPartnerTaxCategory value.
     * @return The builder for this class.
     */
    public Builder businessPartnerTaxCategory(String value) {
      this.dataFields.put("businessPartnerTaxCategory", value);
      return this;
    }

    /**
     * Set the salesPurchaseType value
     *
     * Description: Sales Tax applies to sales situations Purchase Tax to purchase situations
     *
     * @param value
     *          The salesPurchaseType value.
     * @return The builder for this class.
     */
    public Builder salesPurchaseType(String value) {
      this.dataFields.put("salesPurchaseType", value);
      return this;
    }

    /**
     * Set the defaultValue value
     *
     * Description: A value that is shown whenever a record is created.
     *
     * @param value
     *          The defaultValue value.
     * @return The builder for this class.
     */
    public Builder defaultValue(Boolean value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Set the taxSearchKey value
     *
     * Description: A fast method for finding a specific tax.
     *
     * @param value
     *          The taxSearchKey value.
     * @return The builder for this class.
     */
    public Builder taxSearchKey(String value) {
      this.dataFields.put("taxSearchKey", value);
      return this;
    }

    /**
     * Set the rate value
     *
     * Description: The percentage to be multiplied by the source to arrive at the tax or exchange
     * amount.
     *
     * @param value
     *          The rate value.
     * @return The builder for this class.
     */
    public Builder rate(String value) {
      this.dataFields.put("rate", value);
      return this;
    }

    /**
     * Set the validFromDate value
     *
     * Description: A parameter stating the starting time of a specified request.
     *
     * @param value
     *          The validFromDate value.
     * @return The builder for this class.
     */
    public Builder validFromDate(String value) {
      this.dataFields.put("validFromDate", value);
      return this;
    }

    /**
     * Set the summaryLevel value
     *
     * Description: A means of grouping fields in order to view or hide additional information.
     *
     * @param value
     *          The summaryLevel value.
     * @return The builder for this class.
     */
    public Builder summaryLevel(Boolean value) {
      this.dataFields.put("summaryLevel", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the taxCategory value
     *
     * Description: A classification of tax options based on similar characteristics or attributes.
     *
     * @param value
     *          The taxCategory value.
     * @return The builder for this class.
     */
    public Builder taxCategory(String value) {
      this.dataFields.put("taxCategory", value);
      return this;
    }

    /**
     * Set the destinationRegion value
     *
     * Description: The state/province inside of a country receiving the shipment.
     *
     * @param value
     *          The destinationRegion value.
     * @return The builder for this class.
     */
    public Builder destinationRegion(String value) {
      this.dataFields.put("destinationRegion", value);
      return this;
    }

    /**
     * Set the destinationCountry value
     *
     * Description: The country receiving a shipment
     *
     * @param value
     *          The destinationCountry value.
     * @return The builder for this class.
     */
    public Builder destinationCountry(String value) {
      this.dataFields.put("destinationCountry", value);
      return this;
    }

    /**
     * Set the region value
     *
     * Description: An area of a specific country.
     *
     * @param value
     *          The region value.
     * @return The builder for this class.
     */
    public Builder region(String value) {
      this.dataFields.put("region", value);
      return this;
    }

    /**
     * Set the country value
     *
     * Description: A state or a nation.
     *
     * @param value
     *          The country value.
     * @return The builder for this class.
     */
    public Builder country(String value) {
      this.dataFields.put("country", value);
      return this;
    }

    /**
     * Set the parentTaxRate value
     *
     * Description: Parent Tax indicates a tax that is made up of multiple taxes
     *
     * @param value
     *          The parentTaxRate value.
     * @return The builder for this class.
     */
    public Builder parentTaxRate(String value) {
      this.dataFields.put("parentTaxRate", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public TaxData build() {
      return new TaxData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private TaxData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
