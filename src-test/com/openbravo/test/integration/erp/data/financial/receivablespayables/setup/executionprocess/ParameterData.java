/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.setup.executionprocess;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ParameterData
 *
 * @author plujan
 *
 */
public class ParameterData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the searchKey value
     *
     * Description: Search Key
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the sequenceNumber value
     *
     * Description: The order of records in a specified document.
     *
     * @param value
     *          The sequenceNumber value.
     * @return The builder for this class.
     */
    public Builder sequenceNumber(String value) {
      this.dataFields.put("sequenceNumber", value);
      return this;
    }

    /**
     * Set the parameterType value
     *
     * Description: Parameter Type
     *
     * @param value
     *          The parameterType value.
     * @return The builder for this class.
     */
    public Builder parameterType(String value) {
      this.dataFields.put("parameterType", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the mandatory value
     *
     * Description: An indication noting that completing in a field is required to proceed.
     *
     * @param value
     *          The mandatory value.
     * @return The builder for this class.
     */
    public Builder mandatory(Boolean value) {
      this.dataFields.put("mandatory", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the inputType value
     *
     * Description: Input Type
     *
     * @param value
     *          The inputType value.
     * @return The builder for this class.
     */
    public Builder inputType(String value) {
      this.dataFields.put("inputType", value);
      return this;
    }

    /**
     * Set the paymentExecutionProcess value
     *
     * Description: Payment Execution Process
     *
     * @param value
     *          The paymentExecutionProcess value.
     * @return The builder for this class.
     */
    public Builder paymentExecutionProcess(String value) {
      this.dataFields.put("paymentExecutionProcess", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the defaultTextValue value
     *
     * Description: Default Text Value
     *
     * @param value
     *          The defaultTextValue value.
     * @return The builder for this class.
     */
    public Builder defaultTextValue(String value) {
      this.dataFields.put("defaultTextValue", value);
      return this;
    }

    /**
     * Set the defaultValueForFlag value
     *
     * Description: Default Value for Flag
     *
     * @param value
     *          The defaultValueForFlag value.
     * @return The builder for this class.
     */
    public Builder defaultValueForFlag(String value) {
      this.dataFields.put("defaultValueForFlag", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ParameterData build() {
      return new ParameterData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ParameterData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
