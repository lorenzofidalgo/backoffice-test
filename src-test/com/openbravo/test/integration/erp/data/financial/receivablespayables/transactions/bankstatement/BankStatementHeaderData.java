/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for BankStatementHeaderData
 *
 * @author plujan
 *
 */
public class BankStatementHeaderData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the documentNo value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(String value) {
      this.dataFields.put("documentNo", value);
      return this;
    }

    /**
     * Set the document type value
     *
     * Description: Document type
     *
     * @param value
     *          The document type value.
     * @return The builder for this class.
     */
    public Builder documentType(String value) {
      this.dataFields.put("documentType", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: Active
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the importDate value
     *
     * Description: The date the import is carried out and registered in the cash journal.
     *
     * @param value
     *          The importDate value.
     * @return The builder for this class.
     */
    public Builder importDate(String value) {
      this.dataFields.put("importdate", value);
      return this;
    }

    /**
     * Set the transactionDate value
     *
     * Description: The date the transaction is carried out and registered in the cash journal.
     *
     * @param value
     *          The transactionDate value.
     * @return The builder for this class.
     */
    public Builder transactionDate(String value) {
      this.dataFields.put("transactionDate", value);
      return this;
    }

    /**
     * Set the file name value
     *
     * Description: File name
     *
     * @param value
     *          The file name value.
     * @return The builder for this class.
     */
    public Builder fileName(String value) {
      this.dataFields.put("fileName", value);
      return this;
    }

    /**
     * Set the notes value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The notes value.
     * @return The builder for this class.
     */
    public Builder notes(String value) {
      this.dataFields.put("notes", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public BankStatementHeaderData build() {
      return new BankStatementHeaderData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private BankStatementHeaderData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
