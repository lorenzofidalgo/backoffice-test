/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for BankStatementLinesData
 *
 * @author plujan
 *
 */
public class BankStatementLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the gLItem value
     *
     * Description: An alias for the Account Combination which can be commonly used in daily
     * operations.
     *
     * @param value
     *          The gLItem value.
     * @return The builder for this class.
     */
    public Builder gLItem(String value) {
      this.dataFields.put("gLItem", value);
      return this;
    }

    /**
     * Set the payment value
     *
     * Description: A obligation to pay or a right to collect for a specified item or service.
     *
     * @param value
     *          The payment value.
     * @return The builder for this class.
     */
    public Builder payment(String value) {
      this.dataFields.put("payment", value);
      return this;
    }

    /**
     * Set the effectiveDate value
     *
     * Description: Date when money is available
     *
     * @param value
     *          The effectiveDate value.
     * @return The builder for this class.
     */
    public Builder effectiveDate(String value) {
      this.dataFields.put("effectiveDate", value);
      return this;
    }

    /**
     * Set the transactionAmount value
     *
     * Description: Amount of a transaction
     *
     * @param value
     *          The transactionAmount value.
     * @return The builder for this class.
     */
    public Builder transactionAmount(String value) {
      this.dataFields.put("transactionAmount", value);
      return this;
    }

    /**
     * Set the statementAmount value
     *
     * Description: Statement Amount
     *
     * @param value
     *          The statementAmount value.
     * @return The builder for this class.
     */
    public Builder statementAmount(String value) {
      this.dataFields.put("statementAmount", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the accountingDate value
     *
     * Description: The date this transaction is recorded for in the general ledger.
     *
     * @param value
     *          The accountingDate value.
     * @return The builder for this class.
     */
    public Builder accountingDate(String value) {
      this.dataFields.put("accountingDate", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the chargeAmount value
     *
     * Description: The amount of a cost or expense incurred during business activity.
     *
     * @param value
     *          The chargeAmount value.
     * @return The builder for this class.
     */
    public Builder chargeAmount(String value) {
      this.dataFields.put("chargeAmount", value);
      return this;
    }

    /**
     * Set the bankStatement value
     *
     * Description: Bank Statement of account
     *
     * @param value
     *          The bankStatement value.
     * @return The builder for this class.
     */
    public Builder bankStatement(String value) {
      this.dataFields.put("bankStatement", value);
      return this;
    }

    /**
     * Set the bankStatement value
     *
     * Description: Bank Statement of account
     *
     * @param value
     *          The bankStatement value.
     * @return The builder for this class.
     */
    public Builder referenceNo(String value) {
      this.dataFields.put("referenceNo", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public BankStatementLinesData build() {
      return new BankStatementLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private BankStatementLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
