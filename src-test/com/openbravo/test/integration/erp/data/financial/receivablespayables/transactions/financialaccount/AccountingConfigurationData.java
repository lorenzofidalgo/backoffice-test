/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.AccountSelectorData;

/**
 *
 * Class for AccountingConfigurationData
 *
 * @author plujan
 *
 */
public class AccountingConfigurationData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the clearedPaymentAccountOUT value
     *
     * Description: Account used for status cleared of outgoing payments
     *
     * @param value
     *          The clearedPaymentAccountOUT value.
     * @return The builder for this class.
     */
    public Builder clearedPaymentAccountOUT(AccountSelectorData value) {
      this.dataFields.put("clearedPaymentAccountOUT", value);
      return this;
    }

    /**
     * Set the fINBankrevaluationlossAcct value
     *
     * Description: FIN_Bankrevaluationloss_Acct
     *
     * @param value
     *          The fINBankrevaluationlossAcct value.
     * @return The builder for this class.
     */
    public Builder fINBankrevaluationlossAcct(AccountSelectorData value) {
      this.dataFields.put("fINBankrevaluationlossAcct", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the fINAssetAcct value
     *
     * Description: Bank Asset Account used for bank statement accounting
     *
     * @param value
     *          The fINAssetAcct value.
     * @return The builder for this class.
     */
    public Builder fINAssetAcct(AccountSelectorData value) {
      this.dataFields.put("fINAssetAcct", value);
      return this;
    }

    /**
     * Set the fINOutIntransitAcct value
     *
     * Description: Account used for in transit status of outgoing payments
     *
     * @param value
     *          The fINOutIntransitAcct value.
     * @return The builder for this class.
     */
    public Builder fINOutIntransitAcct(AccountSelectorData value) {
      this.dataFields.put("fINOutIntransitAcct", value);
      return this;
    }

    /**
     * Set the fINTransitoryAcct value
     *
     * Description: Bank Transitory Account used for bank statement accounting
     *
     * @param value
     *          The fINTransitoryAcct value.
     * @return The builder for this class.
     */
    public Builder fINTransitoryAcct(AccountSelectorData value) {
      this.dataFields.put("fINTransitoryAcct", value);
      return this;
    }

    /**
     * Set the account value
     *
     * Description: Financial account used to deposit / withdrawal money such as bank accounts or
     * petty cash
     *
     * @param value
     *          The account value.
     * @return The builder for this class.
     */
    public Builder account(String value) {
      this.dataFields.put("account", value);
      return this;
    }

    /**
     * Set the depositAccount value
     *
     * Description: Deposit Account used by the financial account
     *
     * @param value
     *          The depositAccount value.
     * @return The builder for this class.
     */
    public Builder depositAccount(AccountSelectorData value) {
      this.dataFields.put("depositAccount", value);
      return this;
    }

    /**
     * Set the clearedPaymentAccount value
     *
     * Description: Account used for payment IN when clearing
     *
     * @param value
     *          The clearedPaymentAccount value.
     * @return The builder for this class.
     */
    public Builder clearedPaymentAccount(AccountSelectorData value) {
      this.dataFields.put("clearedPaymentAccount", value);
      return this;
    }

    /**
     * Set the fINBankrevaluationgainAcct value
     *
     * Description: FIN_Bankrevaluationgain_Acct
     *
     * @param value
     *          The fINBankrevaluationgainAcct value.
     * @return The builder for this class.
     */
    public Builder fINBankrevaluationgainAcct(AccountSelectorData value) {
      this.dataFields.put("fINBankrevaluationgainAcct", value);
      return this;
    }

    /**
     * Set the accountingSchema value
     *
     * Description: The structure used in accounting including costing methods currencies and the
     * calendar.
     *
     * @param value
     *          The accountingSchema value.
     * @return The builder for this class.
     */
    public Builder accountingSchema(String value) {
      this.dataFields.put("accountingSchema", value);
      return this;
    }

    /**
     * Set the inTransitPaymentAccountIN value
     *
     * Description: Account used for in transit status of incoming payments
     *
     * @param value
     *          The inTransitPaymentAccountIN value.
     * @return The builder for this class.
     */
    public Builder inTransitPaymentAccountIN(AccountSelectorData value) {
      this.dataFields.put("inTransitPaymentAccountIN", value);
      return this;
    }

    /**
     * Set the withdrawalAccount value
     *
     * Description: Withdrawal Account used by the financial account
     *
     * @param value
     *          The withdrawalAccount value.
     * @return The builder for this class.
     */
    public Builder withdrawalAccount(AccountSelectorData value) {
      this.dataFields.put("withdrawalAccount", value);
      return this;
    }

    /**
     * Set the fINBankfeeAcct value
     *
     * Description: FIN_Bankfee_Acct
     *
     * @param value
     *          The fINBankfeeAcct value.
     * @return The builder for this class.
     */
    public Builder fINBankfeeAcct(AccountSelectorData value) {
      this.dataFields.put("fINBankfeeAcct", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AccountingConfigurationData build() {
      return new AccountingConfigurationData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AccountingConfigurationData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
