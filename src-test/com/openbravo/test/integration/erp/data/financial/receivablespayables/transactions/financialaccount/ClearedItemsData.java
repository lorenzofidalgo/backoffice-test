/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ClearedItemsData
 *
 * @author plujan
 *
 */
public class ClearedItemsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the transactionType value
     *
     * Description: Type of credit card transaction
     *
     * @param value
     *          The transactionType value.
     * @return The builder for this class.
     */
    public Builder transactionType(String value) {
      this.dataFields.put("transactionType", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the project value
     *
     * Description: Identifier of a project defined within the Project Service Management module.
     *
     * @param value
     *          The project value.
     * @return The builder for this class.
     */
    public Builder project(String value) {
      this.dataFields.put("project", value);
      return this;
    }

    /**
     * Set the accountingDate value
     *
     * Description: The date this transaction is recorded for in the general ledger.
     *
     * @param value
     *          The accountingDate value.
     * @return The builder for this class.
     */
    public Builder accountingDate(String value) {
      this.dataFields.put("accountingDate", value);
      return this;
    }

    /**
     * Set the depositAmount value
     *
     * Description: Deposit Amount
     *
     * @param value
     *          The depositAmount value.
     * @return The builder for this class.
     */
    public Builder depositAmount(String value) {
      this.dataFields.put("depositAmount", value);
      return this;
    }

    /**
     * Set the reconciliation value
     *
     * Description: Reconciliation events related to the financial account
     *
     * @param value
     *          The reconciliation value.
     * @return The builder for this class.
     */
    public Builder reconciliation(String value) {
      this.dataFields.put("reconciliation", value);
      return this;
    }

    /**
     * Set the financialAccountTransaction value
     *
     * Description: Financial account transaction
     *
     * @param value
     *          The financialAccountTransaction value.
     * @return The builder for this class.
     */
    public Builder financialAccountTransaction(String value) {
      this.dataFields.put("financialAccountTransaction", value);
      return this;
    }

    /**
     * Set the paymentAmount value
     *
     * Description: Payment Amount
     *
     * @param value
     *          The paymentAmount value.
     * @return The builder for this class.
     */
    public Builder paymentAmount(String value) {
      this.dataFields.put("paymentAmount", value);
      return this;
    }

    /**
     * Set the bankStatementLine value
     *
     * Description: Line related to the bank statement
     *
     * @param value
     *          The bankStatementLine value.
     * @return The builder for this class.
     */
    public Builder bankStatementLine(String value) {
      this.dataFields.put("bankStatementLine", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the salesCampaign value
     *
     * Description: An advertising effort aimed at increasing sales.
     *
     * @param value
     *          The salesCampaign value.
     * @return The builder for this class.
     */
    public Builder salesCampaign(String value) {
      this.dataFields.put("salesCampaign", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the payment value
     *
     * Description: Payment event
     *
     * @param value
     *          The payment value.
     * @return The builder for this class.
     */
    public Builder payment(String value) {
      this.dataFields.put("payment", value);
      return this;
    }

    /**
     * Set the activity value
     *
     * Description: A distinct activity defined and used in activity based management.
     *
     * @param value
     *          The activity value.
     * @return The builder for this class.
     */
    public Builder activity(String value) {
      this.dataFields.put("activity", value);
      return this;
    }

    /**
     * Set the transactionDate value
     *
     * Description: The date the transaction is carried out and registered in the cash journal.
     *
     * @param value
     *          The transactionDate value.
     * @return The builder for this class.
     */
    public Builder transactionDate(String value) {
      this.dataFields.put("transactionDate", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the gLItem value
     *
     * Description: An alias for the Account Combination which can be commonly used in daily
     * operations.
     *
     * @param value
     *          The gLItem value.
     * @return The builder for this class.
     */
    public Builder gLItem(String value) {
      this.dataFields.put("gLItem", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ClearedItemsData build() {
      return new ClearedItemsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ClearedItemsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
