/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ImportedBankStatementsData
 *
 * @author plujan
 *
 */
public class ImportedBankStatementsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the transactionDate value
     *
     * Description: The date the transaction is carried out and registered in the cash journal.
     *
     * @param value
     *          The transactionDate value.
     * @return The builder for this class.
     */
    public Builder transactionDate(String value) {
      this.dataFields.put("transactionDate", value);
      return this;
    }

    /**
     * Set the notes value
     *
     * Description: Notes
     *
     * @param value
     *          The notes value.
     * @return The builder for this class.
     */
    public Builder notes(String value) {
      this.dataFields.put("notes", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the importdate value
     *
     * Description: Import date of the bank file
     *
     * @param value
     *          The importdate value.
     * @return The builder for this class.
     */
    public Builder importdate(String value) {
      this.dataFields.put("importdate", value);
      return this;
    }

    /**
     * Set the fileName value
     *
     * Description: File Name
     *
     * @param value
     *          The fileName value.
     * @return The builder for this class.
     */
    public Builder fileName(String value) {
      this.dataFields.put("fileName", value);
      return this;
    }

    /**
     * Set the fINReconciliationID value
     *
     * Description: Reconciliation events related to the financial account
     *
     * @param value
     *          The fINReconciliationID value.
     * @return The builder for this class.
     */
    public Builder fINReconciliationID(String value) {
      this.dataFields.put("fINReconciliationID", value);
      return this;
    }

    /**
     * Set the account value
     *
     * Description: Financial account used to deposit / withdrawal money such as bank accounts or
     * petty cash
     *
     * @param value
     *          The account value.
     * @return The builder for this class.
     */
    public Builder account(String value) {
      this.dataFields.put("account", value);
      return this;
    }

    /**
     * Set the documentNo value
     *
     * Description: An often automatically generated identifier for all documents.
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(String value) {
      this.dataFields.put("documentNo", value);
      return this;
    }

    /**
     * Set the documentType value
     *
     * Description: A value defining what sequence and process setup are used to handle this
     * document.
     *
     * @param value
     *          The documentType value.
     * @return The builder for this class.
     */
    public Builder documentType(String value) {
      this.dataFields.put("documentType", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ImportedBankStatementsData build() {
      return new ImportedBankStatementsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ImportedBankStatementsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
