/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *  David Miguélez <david.miguelez@openbravo.com>
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ReconciliationsLinesData
 *
 * @author David Miguélez
 *
 */

public class ReconciliationsLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author David Miguélez
   *
   */

  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the Document Number value
     *
     * Description: An often automatically generated identifier for all documents.
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(String value) {
      this.dataFields.put("documentNo", value);
      return this;
    }

    /**
     * Set the Document Type value
     *
     * Description: The document type associated to this document.
     *
     * @param value
     *          The documentType value.
     * @return The builder for this class.
     */
    public Builder documentType(String value) {
      this.dataFields.put("documentType", value);
      return this;
    }

    /**
     * Set the Transaction Date value
     *
     * Description: The time listed on the transaction.
     *
     * @param value
     *          The transactionDate value.
     * @return The builder for this class.
     */
    public Builder transactionDate(String value) {
      this.dataFields.put("transactionDate", value);
      return this;
    }

    /**
     * Set the Ending Date value
     *
     * Description: The end date of the range.
     *
     * @param value
     *          The endingDate value.
     * @return The builder for this class.
     */
    public Builder endingDate(String value) {
      this.dataFields.put("endingDate", value);
      return this;
    }

    /**
     * Set the Item Number value
     *
     * Description: Item number for the financial account.
     *
     * @param value
     *          The itemNo value.
     * @return The builder for this class.
     */
    public Builder itemNo(String value) {
      this.dataFields.put("itemNo", value);
      return this;
    }

    /**
     * Set the Item amount value
     *
     * Description: Item amount for the financial account.
     *
     * @param value
     *          The itemAmt value.
     * @return The builder for this class.
     */
    public Builder itemAmt(String value) {
      this.dataFields.put("itemAmt", value);
      return this;
    }

    /**
     * Set the Unreconcilied Number value
     *
     * Description: Unreconcilied bank statement lines.
     *
     * @param value
     *          The unrecNo value.
     * @return The builder for this class.
     */
    public Builder unrecNo(String value) {
      this.dataFields.put("unrecNo", value);
      return this;
    }

    /**
     * Set the Item Unreconcilied amount value
     *
     * Description: Unreconcilied bank statement lines amount.
     *
     * @param value
     *          The unrecAmt value.
     * @return The builder for this class.
     */
    public Builder unrecAmt(String value) {
      this.dataFields.put("unrecAmt", value);
      return this;
    }

    /**
     * Set the Payment Number value
     *
     * Description: Indicates the outstanding payments.
     *
     * @param value
     *          The paymentNo value.
     * @return The builder for this class.
     */
    public Builder paymentNo(String value) {
      this.dataFields.put("paymentNo", value);
      return this;
    }

    /**
     * Set the Item Payment amount value
     *
     * Description: Indicates the outstanding payments amount.
     *
     * @param value
     *          The paymentAmt value.
     * @return The builder for this class.
     */
    public Builder paymentAmt(String value) {
      this.dataFields.put("paymentAmt", value);
      return this;
    }

    /**
     * Set the Deposit Number value
     *
     * Description: Indicates the outstanding deposits.
     *
     * @param value
     *          The depositNo value.
     * @return The builder for this class.
     */
    public Builder depositNo(String value) {
      this.dataFields.put("depositNo", value);
      return this;
    }

    /**
     * Set the Item Deposit amount value
     *
     * Description: Indicates the outstanding deposits amount.
     *
     * @param value
     *          The depositAmt value.
     * @return The builder for this class.
     */
    public Builder depositAmt(String value) {
      this.dataFields.put("depositAmt", value);
      return this;
    }

    /**
     * Set the Starting Balance value
     *
     * Description: Starting Balance of the financial account.
     *
     * @param value
     *          The startingBalance value.
     * @return The builder for this class.
     */
    public Builder startingBalance(String value) {
      this.dataFields.put("startingBalance", value);
      return this;
    }

    /**
     * Set the Ending Balance value
     *
     * Description: The Ending Balance is the result of adjusting the Beginning Balance by any
     * payments or disbursements.
     *
     * @param value
     *          The endingBalance value.
     * @return The builder for this class.
     */
    public Builder endingBalance(String value) {
      this.dataFields.put("endingBalance", value);
      return this;
    }

    /**
     * Set the Document Status value
     *
     * Description: The Document Status indicates the status of a document at this time.
     *
     * @param value
     *          The documentStatus value.
     * @return The builder for this class.
     */
    public Builder documentStatus(String value) {
      this.dataFields.put("documentStatus", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ReconciliationsLinesData build() {
      return new ReconciliationsLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ReconciliationsLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
