/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  David Miguélez <david.miguelez@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for TransactionLinesData
 *
 * @author David Miguélez
 *
 */

public class TransactionLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author David Miguélez
   *
   */

  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the lineNo value
     *
     * Description: The number of the line.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the transactionDate value
     *
     * Description: The date when the transaction was made.
     *
     * @param value
     *          The transactionDate value.
     * @return The builder for this class.
     */
    public Builder transactionDate(String value) {
      this.dataFields.put("transactionDate", value);
      return this;
    }

    /**
     * Set the payment value
     *
     * Description: The information of the payment document associated to the transaction.
     *
     * @param value
     *          The payment value.
     * @return The builder for this class.
     */
    public Builder payment(String value) {
      this.dataFields.put("payment", value);
      return this;
    }

    /**
     * Set the paymentDocNo value
     *
     * Description: The number of the payment document associated to the transaction.
     *
     * @param value
     *          The paymentDocNo value.
     * @return The builder for this class.
     */
    public Builder paymentDocNo(String value) {
      this.dataFields.put("paymentDocNo", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(String value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the status value
     *
     * Description: The current status of the transaction.
     *
     * @param value
     *          The status value.
     * @return The builder for this class.
     */
    public Builder status(String value) {
      this.dataFields.put("status", value);
      return this;
    }

    /**
     * Set the depostiAmout value
     *
     * Description: The amount of money deposited.
     *
     * @param value
     *          The depostiAmout value.
     * @return The builder for this class.
     */
    public Builder depostiAmout(String value) {
      this.dataFields.put("depostiAmout", value);
      return this;
    }

    /**
     * Set the withdrawalAmount value
     *
     * Description: The amount of money withdrawn.
     *
     * @param value
     *          The withdrawalAmount value.
     * @return The builder for this class.
     */
    public Builder withdrawalAmount(String value) {
      this.dataFields.put("withdrawalAmount", value);
      return this;
    }

    /**
     * Set the cleared value
     *
     * @param value
     *          The cleared value.
     * @return The builder for this class.
     */
    public Builder cleared(Boolean value) {
      this.dataFields.put("cleared", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the transactionType value
     *
     * Description: A distinct item characteristic used for processes and sometimes grouped within a
     * category.
     *
     * @param value
     *          The transactionType value.
     * @return The builder for this class.
     */
    public Builder transactionType(String value) {
      this.dataFields.put("transactionType", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the gLItem value
     *
     * Description: The General Ledger item related to this transaction.
     *
     * @param value
     *          The gLItem value.
     * @return The builder for this class.
     */
    public Builder gLItem(String value) {
      this.dataFields.put("gLItem", value);
      return this;
    }

    /**
     * Set the accountingDate value
     *
     * Description: The date when the transaction was recorded in the accounting registry.
     *
     * @param value
     *          The accountingDate value.
     * @return The builder for this class.
     */
    public Builder accountingDate(String value) {
      this.dataFields.put("accountingDate", value);
      return this;
    }

    /**
     * Set the reconciliation value
     *
     * Description: The reconciliation with the bank account information.
     *
     * @param value
     *          The reconciliation value.
     * @return The builder for this class.
     */
    public Builder reconciliation(String value) {
      this.dataFields.put("reconciliation", value);
      return this;
    }

    /**
     * Set the createdByAlgorithm value
     *
     * @param value
     *          The createdByAlgorithm value.
     * @return The builder for this class.
     */
    public Builder createdByAlgorithm(Boolean value) {
      this.dataFields.put("createdByAlgorithm", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public TransactionLinesData build() {
      return new TransactionLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private TransactionLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
