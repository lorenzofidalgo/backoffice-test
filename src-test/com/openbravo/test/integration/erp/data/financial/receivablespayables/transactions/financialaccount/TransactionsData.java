/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.LocationSelectorData;
import com.openbravo.test.integration.erp.data.selectors.PaymentSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;

/**
 *
 * Class for TransactionsData
 *
 * @author plujan
 *
 */
public class TransactionsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the type value
     *
     * Description: A distinct item characteristic used for processes and sometimes grouped within a
     * category.
     *
     * @param value
     *          The type value.
     * @return The builder for this class.
     */
    public Builder type(String value) {
      this.dataFields.put("type", value);
      return this;
    }

    /**
     * Set the swiftCode value
     *
     * Description: Swift Code (Society of Worldwide Interbank Financial Telecommunications)
     *
     * @param value
     *          The swiftCode value.
     * @return The builder for this class.
     */
    public Builder swiftCode(String value) {
      this.dataFields.put("swiftCode", value);
      return this;
    }

    /**
     * Set the routingNo value
     *
     * Description: Bank Routing Number
     *
     * @param value
     *          The routingNo value.
     * @return The builder for this class.
     */
    public Builder routingNo(String value) {
      this.dataFields.put("routingNo", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the defaultValue value
     *
     * Description: A value that is shown whenever a record is created.
     *
     * @param value
     *          The defaultValue value.
     * @return The builder for this class.
     */
    public Builder defaultValue(Boolean value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the iBAN value
     *
     * Description: International standard for identifying bank accounts across national borders
     *
     * @param value
     *          The iBAN value.
     * @return The builder for this class.
     */
    public Builder iBAN(String value) {
      this.dataFields.put("iBAN", value);
      return this;
    }

    /**
     * Set the iNENo value
     *
     * Description: INE Number.
     *
     * @param value
     *          The iNENo value.
     * @return The builder for this class.
     */
    public Builder iNENo(String value) {
      this.dataFields.put("iNENo", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the currentBalance value
     *
     * Description: Current Balance
     *
     * @param value
     *          The currentBalance value.
     * @return The builder for this class.
     */
    public Builder currentBalance(String value) {
      this.dataFields.put("currentBalance", value);
      return this;
    }

    /**
     * Set the creditLimit value
     *
     * Description: Amount of Credit allowed
     *
     * @param value
     *          The creditLimit value.
     * @return The builder for this class.
     */
    public Builder creditLimit(String value) {
      this.dataFields.put("creditLimit", value);
      return this;
    }

    /**
     * Set the branchCode value
     *
     * Description: Code of the branch
     *
     * @param value
     *          The branchCode value.
     * @return The builder for this class.
     */
    public Builder branchCode(String value) {
      this.dataFields.put("branchCode", value);
      return this;
    }

    /**
     * Set the bankCode value
     *
     * Description: Code of the bank
     *
     * @param value
     *          The bankCode value.
     * @return The builder for this class.
     */
    public Builder bankCode(String value) {
      this.dataFields.put("bankCode", value);
      return this;
    }

    /**
     * Set the partialAccountNo value
     *
     * Description: Code of the account
     *
     * @param value
     *          The partialAccountNo value.
     * @return The builder for this class.
     */
    public Builder partialAccountNo(String value) {
      this.dataFields.put("partialAccountNo", value);
      return this;
    }

    /**
     * Set the locationAddress value
     *
     * Description: A specific place or residence.
     *
     * @param value
     *          The locationAddress value.
     * @return The builder for this class.
     */
    public Builder locationAddress(LocationSelectorData value) {
      this.dataFields.put("locationAddress", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the finPayment value
     *
     * Description: The specific chosen payment related to some transaction
     *
     * @param value
     *          The finPayment value.
     * @return The builder for this class.
     */
    public Builder finPayment(PaymentSelectorData value) {
      this.dataFields.put("finPayment", value);
      return this;
    }

    /**
     * Set the bankDigitcontrol value
     *
     * Description: Bank_Digitcontrol
     *
     * @param value
     *          The bankDigitcontrol value.
     * @return The builder for this class.
     */
    public Builder bankDigitcontrol(String value) {
      this.dataFields.put("bankDigitcontrol", value);
      return this;
    }

    /**
     * Set the accountNo value
     *
     * Description: Account Number
     *
     * @param value
     *          The accountNo value.
     * @return The builder for this class.
     */
    public Builder accountNo(String value) {
      this.dataFields.put("accountNo", value);
      return this;
    }

    /**
     * Set the accountDigitcontrol value
     *
     * Description: Account_Digitcontrol
     *
     * @param value
     *          The accountDigitcontrol value.
     * @return The builder for this class.
     */
    public Builder accountDigitcontrol(String value) {
      this.dataFields.put("accountDigitcontrol", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the transaction type value
     *
     * @param value
     *          The transaction type value.
     * @return The builder for this class.
     */
    public Builder transactionType(String value) {
      this.dataFields.put("transactionType", value);
      return this;
    }

    /**
     * Set the transaction status value
     *
     * @param value
     *          The transaction status value.
     * @return The builder for this class.
     */
    public Builder status(String value) {
      this.dataFields.put("status", value);
      return this;
    }

    /**
     * Set the withdrawal amount value
     *
     * @param value
     *          The withdrawal amount value.
     * @return The builder for this class.
     */
    public Builder paymentAmount(String value) {
      this.dataFields.put("paymentAmount", value);
      return this;
    }

    /**
     * Set the deposit amount value
     *
     * @param value
     *          The deposit amount value.
     * @return The builder for this class.
     */
    public Builder depositAmount(String value) {
      this.dataFields.put("depositAmount", value);
      return this;
    }

    /**
     * Set the Transaction Date value
     *
     * @param value
     *          The Transaction Date value.
     * @return The builder for this class.
     */
    public Builder transactionDate(String value) {
      this.dataFields.put("transactionDate", value);
      return this;
    }

    /**
     * Set the Accounting Date value
     *
     * @param value
     *          The Accounting Date value.
     * @return The builder for this class.
     */
    public Builder accountingDate(String value) {
      this.dataFields.put("dateAcct", value);
      return this;
    }

    /**
     * Set the GL Item value
     *
     * @param value
     *          The GL Item value.
     * @return The builder for this class.
     */
    public Builder gLItem(String value) {
      this.dataFields.put("gLItem", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the project value
     *
     * @param value
     *          The project value.
     * @return The builder for this class.
     */
    public Builder project(String value) {
      this.dataFields.put("project", value);
      return this;
    }

    /**
     * Set the documentNo value
     *
     * Description: The number of the payment document associated to the transaction.
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(String value) {
      this.dataFields.put("finPayment$documentNo", value);
      return this;
    }

    /**
     * Set the foreign currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The foreign currency value.
     * @return The builder for this class.
     */
    public Builder foreignCurrency(String value) {
      this.dataFields.put("foreignCurrency", value);
      return this;
    }

    /**
     * Set the foreign amount value
     *
     * @param value
     *          The foreign amount value.
     * @return The builder for this class.
     */
    public Builder foreignAmount(String value) {
      this.dataFields.put("foreignAmount", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public TransactionsData build() {
      return new TransactionsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private TransactionsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
