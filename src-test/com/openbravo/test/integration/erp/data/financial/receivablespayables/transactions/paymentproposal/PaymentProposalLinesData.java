/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentproposal;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;

/**
 *
 * Class for PaymentProposalLinesData
 *
 * @author plujan
 *
 */
public class PaymentProposalLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the writeOffAmount value
     *
     * Description: A monetary sum that can be deducted from tax obligations.
     *
     * @param value
     *          The writeOffAmount value.
     * @return The builder for this class.
     */
    public Builder writeOffAmount(String value) {
      this.dataFields.put("writeOffAmount", value);
      return this;
    }

    /**
     * Set the paymentNo value
     *
     * Description: Payment Number
     *
     * @param value
     *          The paymentNo value.
     * @return The builder for this class.
     */
    public Builder paymentNo(String value) {
      this.dataFields.put("paymentNo", value);
      return this;
    }

    /**
     * Set the paidAmount value
     *
     * Description: Paid Amount
     *
     * @param value
     *          The paidAmount value.
     * @return The builder for this class.
     */
    public Builder paidAmount(String value) {
      this.dataFields.put("paidAmount", value);
      return this;
    }

    /**
     * Set the orderNo value
     *
     * Description: Order Number
     *
     * @param value
     *          The orderNo value.
     * @return The builder for this class.
     */
    public Builder orderNo(String value) {
      this.dataFields.put("orderNo", value);
      return this;
    }

    /**
     * Set the invoiceNo value
     *
     * Description: Invoice No.
     *
     * @param value
     *          The invoiceNo value.
     * @return The builder for this class.
     */
    public Builder invoiceNo(String value) {
      this.dataFields.put("invoiceNo", value);
      return this;
    }

    /**
     * Set the invoiceAmount value
     *
     * Description: The monetary sum that is invoiced for a specified item or service.
     *
     * @param value
     *          The invoiceAmount value.
     * @return The builder for this class.
     */
    public Builder invoiceAmount(String value) {
      this.dataFields.put("invoiceAmount", value);
      return this;
    }

    /**
     * Set the paymentScheduleDetail value
     *
     * Description: Payment Schedule Detail
     *
     * @param value
     *          The paymentScheduleDetail value.
     * @return The builder for this class.
     */
    public Builder paymentScheduleDetail(String value) {
      this.dataFields.put("paymentScheduleDetail", value);
      return this;
    }

    /**
     * Set the paymentProposal value
     *
     * Description: Payment Proposal
     *
     * @param value
     *          The paymentProposal value.
     * @return The builder for this class.
     */
    public Builder paymentProposal(String value) {
      this.dataFields.put("paymentProposal", value);
      return this;
    }

    /**
     * Set the payment value
     *
     * Description: Payment event
     *
     * @param value
     *          The payment value.
     * @return The builder for this class.
     */
    public Builder payment(String value) {
      this.dataFields.put("payment", value);
      return this;
    }

    /**
     * Set the expected value
     *
     * Description: Expected Amount
     *
     * @param value
     *          The expected value.
     * @return The builder for this class.
     */
    public Builder expected(String value) {
      this.dataFields.put("expected", value);
      return this;
    }

    /**
     * Set the dueDate value
     *
     * Description: The date when a specified request must be carried out by.
     *
     * @param value
     *          The dueDate value.
     * @return The builder for this class.
     */
    public Builder dueDate(String value) {
      this.dataFields.put("dueDate", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PaymentProposalLinesData build() {
      return new PaymentProposalLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PaymentProposalLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
