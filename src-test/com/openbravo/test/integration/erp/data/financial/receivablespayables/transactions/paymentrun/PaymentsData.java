/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentrun;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PaymentsData
 *
 * @author plujan
 *
 */
public class PaymentsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the result value
     *
     * Description: x not implemented
     *
     * @param value
     *          The result value.
     * @return The builder for this class.
     */
    public Builder result(String value) {
      this.dataFields.put("result", value);
      return this;
    }

    /**
     * Set the message value
     *
     * Description: Message
     *
     * @param value
     *          The message value.
     * @return The builder for this class.
     */
    public Builder message(String value) {
      this.dataFields.put("message", value);
      return this;
    }

    /**
     * Set the paymentRun value
     *
     * Description: Payment Run
     *
     * @param value
     *          The paymentRun value.
     * @return The builder for this class.
     */
    public Builder paymentRun(String value) {
      this.dataFields.put("paymentRun", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the payment value
     *
     * Description: Payment event
     *
     * @param value
     *          The payment value.
     * @return The builder for this class.
     */
    public Builder payment(String value) {
      this.dataFields.put("payment", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PaymentsData build() {
      return new PaymentsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PaymentsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
