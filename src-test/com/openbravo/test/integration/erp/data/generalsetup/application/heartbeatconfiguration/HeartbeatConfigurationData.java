/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.application.heartbeatconfiguration;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for HeartbeatConfigurationData
 *
 * @author plujan
 *
 */
public class HeartbeatConfigurationData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the enableCustomQueries value
     *
     * Description: Send Custom Queries to Heartbeat
     *
     * @param value
     *          The enableCustomQueries value.
     * @return The builder for this class.
     */
    public Builder enableCustomQueries(Boolean value) {
      this.dataFields.put("enableCustomQueries", value);
      return this;
    }

    /**
     * Set the proxyUser value
     *
     * Description: User for the authenticated proxy
     *
     * @param value
     *          The proxyUser value.
     * @return The builder for this class.
     */
    public Builder proxyUser(String value) {
      this.dataFields.put("proxyUser", value);
      return this;
    }

    /**
     * Set the proxyPassword value
     *
     * Description: Password for the authenticated proxy
     *
     * @param value
     *          The proxyPassword value.
     * @return The builder for this class.
     */
    public Builder proxyPassword(String value) {
      this.dataFields.put("proxyPassword", value);
      return this;
    }

    /**
     * Set the requiresProxyAuthentication value
     *
     * Description: Is authentication requiered to use the proxy
     *
     * @param value
     *          The requiresProxyAuthentication value.
     * @return The builder for this class.
     */
    public Builder requiresProxyAuthentication(Boolean value) {
      this.dataFields.put("requiresProxyAuthentication", value);
      return this;
    }

    /**
     * Set the proxyServer value
     *
     * Description: Proxy server used to access the internet.
     *
     * @param value
     *          The proxyServer value.
     * @return The builder for this class.
     */
    public Builder proxyServer(String value) {
      this.dataFields.put("proxyServer", value);
      return this;
    }

    /**
     * Set the proxyPort value
     *
     * Description: Proxy port on the proxy server used to access the internet.
     *
     * @param value
     *          The proxyPort value.
     * @return The builder for this class.
     */
    public Builder proxyPort(String value) {
      this.dataFields.put("proxyPort", value);
      return this;
    }

    /**
     * Set the proxyRequired value
     *
     * Description: Proxy configuration required to access internet.
     *
     * @param value
     *          The proxyRequired value.
     * @return The builder for this class.
     */
    public Builder proxyRequired(Boolean value) {
      this.dataFields.put("proxyRequired", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public HeartbeatConfigurationData build() {
      return new HeartbeatConfigurationData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private HeartbeatConfigurationData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
