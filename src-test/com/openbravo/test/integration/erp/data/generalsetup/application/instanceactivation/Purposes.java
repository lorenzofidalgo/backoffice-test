package com.openbravo.test.integration.erp.data.generalsetup.application.instanceactivation;

/**
 * Enumerator with the purpose values.
 *
 * @author elopio
 *
 */
public enum Purposes {

  /** Development purpose. */
  DEVELOPMENT("Development"),
  /** Production purpose. */
  PRODUCTION("Production"),
  /** Testing purpose. */
  TESTING("Testing");

  /** The purpose value. */
  private final String purpose;

  /**
   * Enumerator constructor.
   *
   * @param purpose
   *          The purpose value.
   */
  private Purposes(String purpose) {
    this.purpose = purpose;
  }

  /**
   * Get the purpose value.
   *
   * @return the purpose value.
   */
  public String purpose() {
    return purpose;
  }
}
