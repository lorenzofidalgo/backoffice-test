/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.application.preference;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PreferenceData
 *
 * @author plujan
 *
 */
public class PreferenceData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the propertyList value
     *
     * Description: Determines how the preference is defined: by Property or by Attribute.
     *
     * @param value
     *          The propertyList value.
     * @return The builder for this class.
     */
    public Builder propertyList(Boolean value) {
      this.dataFields.put("propertyList", value);
      return this;
    }

    /**
     * Set the selected value
     *
     * Description: Selected property in case of conflict
     *
     * @param value
     *          The selected value.
     * @return The builder for this class.
     */
    public Builder selected(Boolean value) {
      this.dataFields.put("selected", value);
      return this;
    }

    /**
     * Set the property value
     *
     * Description: Configuration Property to assign value
     *
     * @param value
     *          The property value.
     * @return The builder for this class.
     */
    public Builder property(String value) {
      this.dataFields.put("property", value);
      return this;
    }

    /**
     * Set the attribute value
     *
     * Description: Name of the preference.
     *
     * @param value
     *          The attribute value.
     * @return The builder for this class.
     */
    public Builder attribute(String value) {
      this.dataFields.put("attribute", value);
      return this;
    }

    /**
     * Set the value value
     *
     * Description: value
     *
     * @param value
     *          The value value.
     * @return The builder for this class.
     */
    public Builder value(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the inheritedFrom value
     *
     * Description: inheritedFrom
     *
     * @param value
     *          The inheritedFrom value.
     * @return The builder for this class.
     */
    public Builder inheritedFrom(String value) {
      this.dataFields.put("inheritedFrom", value);
      return this;
    }

    /**
     * Set the visibleAtClient value
     *
     * Description: Defines Client visibility.
     *
     * @param value
     *          The visibleAtClient value.
     * @return The builder for this class.
     */
    public Builder visibleAtClient(String value) {
      this.dataFields.put("visibleAtClient", value);
      return this;
    }

    /**
     * Set the visibleAtOrganization value
     *
     * Description: Defines Organization visibility.
     *
     * @param value
     *          The visibleAtOrganization value.
     * @return The builder for this class.
     */
    public Builder visibleAtOrganization(String value) {
      this.dataFields.put("visibleAtOrganization", value);
      return this;
    }

    /**
     * Set the visibleAtUser value
     *
     * Description: Defines User visibility.
     *
     * @param value
     *          The visibleAtUser value.
     * @return The builder for this class.
     */
    public Builder visibleAtUser(String value) {
      this.dataFields.put("userContact", value);
      return this;
    }

    /**
     * Set the visibleAtUser value
     *
     * Description: Defines User visibility.
     *
     * @param value
     *          The visibleAtUser value.
     * @return The builder for this class.
     */
    public Builder userContact(String value) {
      this.dataFields.put("userContact", value);
      return this;
    }

    /**
     * Set the visibleAtRole value
     *
     * Description: Defines Role visibility.
     *
     * @param value
     *          The visibleAtRole value.
     * @return The builder for this class.
     */
    public Builder visibleAtRole(String value) {
      this.dataFields.put("visibleAtRole", value);
      return this;
    }

    /**
     * Set the window value
     *
     * Description: A work area which can be used to create view edit and process a record.
     *
     * @param value
     *          The window value.
     * @return The builder for this class.
     */
    public Builder window(String value) {
      this.dataFields.put("window", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PreferenceData build() {
      return new PreferenceData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PreferenceData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
