/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.client.initialclientsetup;

import com.openbravo.test.integration.erp.data.generalsetup.InitialSetupData;

/**
 * Class for Initial Client Setup data.
 *
 * @author elopio
 *
 */
public class InitialClientSetupData {

  /** Formatting string used to get a string representation of this object. */
  private static final String FORMAT_STRING_REPRESENTATION = "[Initial Client Setup data:\n"
      + "client=%s,\n" + "client user name=%s\n" + "password=%s,\n" + "password confirmation=%s,\n"
      + "Initial Setup data=%s]";

  /* Data fields. */
  /** The client name. */
  private String client;
  /** The client user name. */
  private String clientUserName;
  /** The client password. */
  private String password;
  /** The password confirmation. */
  private String passwordConfirmation;
  /** The Initial Setup data. */
  private InitialSetupData initialSetupData;

  /**
   * Class builder.
   *
   * @author elopio
   *
   */
  public static class Builder {

    /** The client name. */
    private String client = null;
    /** The client user name. */
    private String clientUserName = null;
    /** The client password. */
    private String password = null;
    /** The password confirmation. */
    private String passwordConfirmation = null;
    /** The Initial Setup data. */
    private InitialSetupData initialSetupData = null;

    /**
     * Set the client name value.
     *
     * @param value
     *          The client name.
     * @return the builder for this class.
     */
    public Builder client(String value) {
      this.client = value;
      return this;
    }

    /**
     * Set the client user name value.
     *
     * @param value
     *          The client user name.
     * @return the builder for this class.
     */
    public Builder clientUserName(String value) {
      this.clientUserName = value;
      return this;
    }

    /**
     * Set the password value.
     *
     * @param value
     *          The client password.
     * @return the builder for this class.
     */
    public Builder password(String value) {
      this.password = value;
      return this;
    }

    /**
     * Set the password confirmation value.
     *
     * @param value
     *          The password confirmation.
     * @return the builder for this class.
     */
    public Builder passwordConfirmation(String value) {
      this.passwordConfirmation = value;
      return this;
    }

    /**
     * Set the Initial Setup data.
     *
     * @param value
     *          The Initial Setup data.
     * @return the builder for this class.
     */
    public Builder initialSetupData(InitialSetupData value) {
      this.initialSetupData = value;
      return this;
    }

    /**
     * Set the fields for this object that are required on the ERP.
     *
     * @param clientValue
     *          The client name.
     * @param clientUserNameValue
     *          The client user name.
     * @param passwordValue
     *          The client password.
     * @param passwordConfirmationValue
     *          The password confirmation.
     * @return the builder for this class.
     *
     */
    public Builder requiredFields(String clientValue, String clientUserNameValue,
        String passwordValue, String passwordConfirmationValue) {
      this.client = clientValue;
      this.clientUserName = clientUserNameValue;
      this.password = passwordValue;
      this.passwordConfirmation = passwordConfirmationValue;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return the data object.
     */
    public InitialClientSetupData build() {
      return new InitialClientSetupData(this);
    }
  }

  /**
   * Class constructor.
   *
   * @param builder
   *          The object builder.
   */
  private InitialClientSetupData(Builder builder) {
    client = builder.client;
    clientUserName = builder.clientUserName;
    password = builder.password;
    passwordConfirmation = builder.passwordConfirmation;
    initialSetupData = builder.initialSetupData;
  }

  /**
   * Get the client name.
   *
   * @return the client name.
   */
  public String getClient() {
    return client;
  }

  /**
   * Get the client user name.
   *
   * @return the client user name.
   */
  public String getClientUserName() {
    return clientUserName;
  }

  /**
   * Get the client password.
   *
   * @return the client password.
   */
  public String getPassword() {
    return password;
  }

  /**
   * Get the password confirmation.
   *
   * @return the password confirmation.
   */
  public String getPasswordConfirmation() {
    return passwordConfirmation;
  }

  /**
   * Get the Initial Setup data.
   *
   * @return the Initial Setup data.
   */
  public InitialSetupData getInitialSetupData() {
    return initialSetupData;
  }

  /**
   * Get the string representation of this data object.
   *
   * @return the string representation.
   */
  @Override
  public String toString() {
    return String.format(FORMAT_STRING_REPRESENTATION, client, clientUserName, password,
        passwordConfirmation, initialSetupData.toString());
  }
}
