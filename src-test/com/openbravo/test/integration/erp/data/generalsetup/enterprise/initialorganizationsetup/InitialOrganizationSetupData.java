/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.enterprise.initialorganizationsetup;

import com.openbravo.test.integration.erp.data.generalsetup.InitialSetupData;
import com.openbravo.test.integration.erp.data.selectors.LocationSelectorData;

/**
 * Class for Initial Organization Setup data.
 *
 * @author elopio
 *
 */
public class InitialOrganizationSetupData {

  /* Data fields. */
  /** The organization name. */
  private String organization;
  /** The organization user name. */
  private String organizationUserName;
  /** The organization password. */
  private String password;
  /** The confirmation of the organization password. */
  private String passwordConfirmation;
  /** The organization type. */
  private String organizationType;
  /** The parent organization. */
  private String parentOrganization;
  /** The organization location. */
  private LocationSelectorData location;
  /** The Initial Setup data. */
  private InitialSetupData initialSetupData;

  /**
   * Class builder.
   *
   * @author elopio
   *
   */
  public static class Builder {

    /** The organization name. */
    private String organization = null;
    /** The organization user name. */
    private String organizationUserName = null;
    /** The organization password. */
    private String password = null;
    /** The confirmation of the organization password. */
    private String passwordConfirmation = null;
    /** The organization type. */
    private String organizationType = null;
    /** The parent organization. */
    private String parentOrganization = null;
    /** The organization location. */
    private LocationSelectorData location = null;
    /** The Initial Setup data. */
    private InitialSetupData initialSetupData = null;

    /**
     * Set the organization name.
     *
     * @param value
     *          The organization name.
     * @return the builder for this class.
     */
    public Builder organization(String value) {
      this.organization = value;
      return this;
    }

    /**
     * Set the organization user name.
     *
     * @param value
     *          The organization user name.
     * @return the builder for this class.
     */
    public Builder organizationUserName(String value) {
      this.organizationUserName = value;
      return this;
    }

    /**
     * Set the password value.
     *
     * @param value
     *          The organization password.
     * @return the builder for this class.
     */
    public Builder password(String value) {
      this.password = value;
      return this;
    }

    /**
     * Set the value for the password confirmation.
     *
     * @param value
     *          The confirmation of the password.
     * @return the builder for this class.
     */
    public Builder passwordConfirmation(String value) {
      this.passwordConfirmation = value;
      return this;
    }

    /**
     * Set the organization type.
     *
     * @param value
     *          The organization type.
     * @return the builder for this class.
     */
    public Builder organizationType(String value) {
      this.organizationType = value;
      return this;
    }

    /**
     * Set the parent organization.
     *
     * @param value
     *          The parent organization.
     * @return the builder for this class.
     */
    public Builder parentOrganization(String value) {
      this.parentOrganization = value;
      return this;
    }

    /**
     * Set the organization location.
     *
     * @param value
     *          The organization location.
     * @return the builder for this class.
     */
    public Builder location(LocationSelectorData value) {
      this.location = value;
      return this;
    }

    /**
     * Set the Initial Setup data.
     *
     * @param value
     *          The Initial Setup data.
     * @return the builder for this class.
     */
    public Builder initialSetupData(InitialSetupData value) {
      this.initialSetupData = value;
      return this;
    }

    /**
     * Set the fields for this object that are required on the ERP.
     *
     * @param organizationValue
     *          The organization name.
     * @param organizationUserNameValue
     *          The organization user name;
     * @param passwordValue
     *          The organization password.
     * @param passwordConfirmationValue
     *          The confirmation of the password.
     * @param organizationTypeValue
     *          The organization type.
     * @param parentOrganizationValue
     *          The parent organization.
     * @return the builder for this class.
     */
    public Builder requiredFields(String organizationValue, String organizationUserNameValue,
        String passwordValue, String passwordConfirmationValue, String organizationTypeValue,
        String parentOrganizationValue) {
      this.organization = organizationValue;
      this.organizationUserName = organizationUserNameValue;
      this.password = passwordValue;
      this.passwordConfirmation = passwordConfirmationValue;
      this.organizationType = organizationTypeValue;
      this.parentOrganization = parentOrganizationValue;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return the data object.
     */
    public InitialOrganizationSetupData build() {
      return new InitialOrganizationSetupData(this);
    }
  }

  /**
   * Class constructor.
   *
   * @param builder
   *          The object builder.
   */
  private InitialOrganizationSetupData(Builder builder) {
    organization = builder.organization;
    organizationUserName = builder.organizationUserName;
    password = builder.password;
    passwordConfirmation = builder.passwordConfirmation;
    organizationType = builder.organizationType;
    parentOrganization = builder.parentOrganization;
    location = builder.location;
    initialSetupData = builder.initialSetupData;
  }

  /**
   * Get the organization name.
   *
   * @return the organization name.
   */
  public String getOrganization() {
    return organization;
  }

  /**
   * Get the organization user name.
   *
   * @return the organization user name.
   */
  public String getOrganizationUserName() {
    return organizationUserName;
  }

  /**
   * Get the organization password.
   *
   * @return the organization password.
   */
  public String getPassword() {
    return password;
  }

  /**
   * Get the confirmation of the password.
   *
   * @return the confirmation of the password.
   */
  public String getPasswordConfirmation() {
    return passwordConfirmation;
  }

  /**
   * Get the organization type.
   *
   * @return the organization type.
   */
  public String getOrganizationType() {
    return organizationType;
  }

  /**
   * Get the parent organization.
   *
   * @return the parent organization.
   */
  public String getParentOrganization() {
    return parentOrganization;
  }

  /**
   * Get the organization location.
   *
   * @return the organization location.
   */
  public LocationSelectorData getLocation() {
    return location;
  }

  /**
   * Get the Initial Setup data.
   *
   * @return the Initial Setup data.
   */
  public InitialSetupData getInitialSetupData() {
    return initialSetupData;
  }

  @Override
  public String toString() {
    return "InitialOrganizationSetupData [organization=" + organization + ", organizationUserName="
        + organizationUserName + ", password=" + password + ", passwordConfirmation="
        + passwordConfirmation + ", organizationType=" + organizationType + ", parentOrganization="
        + parentOrganization + ", location=" + location + ", initialSetupData=" + initialSetupData
        + "]";
  }
}
