/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.enterprise.organization;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PeriodControlData
 *
 * @author plujan
 *
 */
public class PeriodControlData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the endingDate value
     *
     * Description: A parameter stating when a specified request will end.
     *
     * @param value
     *          The endingDate value.
     * @return The builder for this class.
     */
    public Builder endingDate(String value) {
      this.dataFields.put("endingDate", value);
      return this;
    }

    /**
     * Set the fiscalYear value
     *
     * Description: Calendar Year
     *
     * @param value
     *          The fiscalYear value.
     * @return The builder for this class.
     */
    public Builder fiscalYear(String value) {
      this.dataFields.put("fiscalYear", value);
      return this;
    }

    /**
     * Set the startingDate value
     *
     * Description: A parameter stating when a specified request will begin.
     *
     * @param value
     *          The startingDate value.
     * @return The builder for this class.
     */
    public Builder startingDate(String value) {
      this.dataFields.put("startingDate", value);
      return this;
    }

    /**
     * Set the periodStatus value
     *
     * Description: Current state of this period
     *
     * @param value
     *          The periodStatus value.
     * @return The builder for this class.
     */
    public Builder periodStatus(String value) {
      this.dataFields.put("periodStatus", value);
      return this;
    }

    /**
     * Set the documentCategory value
     *
     * Description: A classification of document types that are shown and processed in the same
     * window.
     *
     * @param value
     *          The documentCategory value.
     * @return The builder for this class.
     */
    public Builder documentCategory(String value) {
      this.dataFields.put("documentCategory", value);
      return this;
    }

    /**
     * Set the period value
     *
     * Description: A specified time period.
     *
     * @param value
     *          The period value.
     * @return The builder for this class.
     */
    public Builder period(String value) {
      this.dataFields.put("period", value);
      return this;
    }

    /**
     * Set the calendar value
     *
     * Description: A table showing the days of the week for each month of the year.
     *
     * @param value
     *          The calendar value.
     * @return The builder for this class.
     */
    public Builder calendar(String value) {
      this.dataFields.put("calendar", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PeriodControlData build() {
      return new PeriodControlData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PeriodControlData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
