/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.security.session;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for SessionData
 *
 * @author plujan
 *
 */
public class SessionData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the username value
     *
     * Description: User Name
     *
     * @param value
     *          The username value.
     * @return The builder for this class.
     */
    public Builder username(String value) {
      this.dataFields.put("username", value);
      return this;
    }

    /**
     * Set the loginStatus value
     *
     * Description: Login Status
     *
     * @param value
     *          The loginStatus value.
     * @return The builder for this class.
     */
    public Builder loginStatus(String value) {
      this.dataFields.put("loginStatus", value);
      return this;
    }

    /**
     * Set the lastPing value
     *
     * Description: Time for the last time the session sent ping
     *
     * @param value
     *          The lastPing value.
     * @return The builder for this class.
     */
    public Builder lastPing(String value) {
      this.dataFields.put("lastPing", value);
      return this;
    }

    /**
     * Set the serverUrl value
     *
     * Description: URL for the context the session is in
     *
     * @param value
     *          The serverUrl value.
     * @return The builder for this class.
     */
    public Builder serverUrl(String value) {
      this.dataFields.put("serverUrl", value);
      return this;
    }

    /**
     * Set the creationDate value
     *
     * Description: Time when the session started
     *
     * @param value
     *          The creationDate value.
     * @return The builder for this class.
     */
    public Builder creationDate(String value) {
      this.dataFields.put("creationDate", value);
      return this;
    }

    /**
     * Set the remoteHost value
     *
     * Description: not part of task
     *
     * @param value
     *          The remoteHost value.
     * @return The builder for this class.
     */
    public Builder remoteHost(String value) {
      this.dataFields.put("remoteHost", value);
      return this;
    }

    /**
     * Set the remoteAddress value
     *
     * Description: not part of task
     *
     * @param value
     *          The remoteAddress value.
     * @return The builder for this class.
     */
    public Builder remoteAddress(String value) {
      this.dataFields.put("remoteAddress", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public SessionData build() {
      return new SessionData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private SessionData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
