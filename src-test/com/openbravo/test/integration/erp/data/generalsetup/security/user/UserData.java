/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.security.user;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;

/**
 *
 * Class for UserData
 *
 * @author plujan
 *
 */
public class UserData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the locked value
     *
     * Description: The user is locked and cannot log in the application
     *
     * @param value
     *          The locked value.
     * @return The builder for this class.
     */
    public Builder locked(Boolean value) {
      this.dataFields.put("locked", value);
      return this;
    }

    /**
     * Set the defaultWarehouse value
     *
     * Description: Default Warehouse
     *
     * @param value
     *          The defaultWarehouse value.
     * @return The builder for this class.
     */
    public Builder defaultWarehouse(String value) {
      this.dataFields.put("defaultWarehouse", value);
      return this;
    }

    /**
     * Set the defaultRole value
     *
     * Description: Defautl Role
     *
     * @param value
     *          The defaultRole value.
     * @return The builder for this class.
     */
    public Builder defaultRole(String value) {
      this.dataFields.put("defaultRole", value);
      return this;
    }

    /**
     * Set the defaultLanguage value
     *
     * Description: Default Language
     *
     * @param value
     *          The defaultLanguage value.
     * @return The builder for this class.
     */
    public Builder defaultLanguage(String value) {
      this.dataFields.put("defaultLanguage", value);
      return this;
    }

    /**
     * Set the defaultOrganization value
     *
     * Description: Default Organization
     *
     * @param value
     *          The defaultOrganization value.
     * @return The builder for this class.
     */
    public Builder defaultOrganization(String value) {
      this.dataFields.put("defaultOrganization", value);
      return this;
    }

    /**
     * Set the defaultClient value
     *
     * Description: Default Session Client
     *
     * @param value
     *          The defaultClient value.
     * @return The builder for this class.
     */
    public Builder defaultClient(String value) {
      this.dataFields.put("defaultClient", value);
      return this;
    }

    /**
     * Set the username value
     *
     * Description: User Name
     *
     * @param value
     *          The username value.
     * @return The builder for this class.
     */
    public Builder username(String value) {
      this.dataFields.put("username", value);
      return this;
    }

    /**
     * Set the lastName value
     *
     * Description: Last name of the contact
     *
     * @param value
     *          The lastName value.
     * @return The builder for this class.
     */
    public Builder lastName(String value) {
      this.dataFields.put("lastName", value);
      return this;
    }

    /**
     * Set the firstName value
     *
     * Description: Name of the contact
     *
     * @param value
     *          The firstName value.
     * @return The builder for this class.
     */
    public Builder firstName(String value) {
      this.dataFields.put("firstName", value);
      return this;
    }

    /**
     * Set the comments value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The comments value.
     * @return The builder for this class.
     */
    public Builder comments(String value) {
      this.dataFields.put("comments", value);
      return this;
    }

    /**
     * Set the position value
     *
     * Description: A defined job title or ranking within a company.
     *
     * @param value
     *          The position value.
     * @return The builder for this class.
     */
    public Builder position(String value) {
      this.dataFields.put("position", value);
      return this;
    }

    /**
     * Set the lastContactDate value
     *
     * Description: Date this individual was last contacted
     *
     * @param value
     *          The lastContactDate value.
     * @return The builder for this class.
     */
    public Builder lastContactDate(String value) {
      this.dataFields.put("lastContactDate", value);
      return this;
    }

    /**
     * Set the lastContactResult value
     *
     * Description: A statement related to the result of the last interaction with a business
     * partner.
     *
     * @param value
     *          The lastContactResult value.
     * @return The builder for this class.
     */
    public Builder lastContactResult(String value) {
      this.dataFields.put("lastContactResult", value);
      return this;
    }

    /**
     * Set the fax value
     *
     * Description: A fax number for a specified business partner.
     *
     * @param value
     *          The fax value.
     * @return The builder for this class.
     */
    public Builder fax(String value) {
      this.dataFields.put("fax", value);
      return this;
    }

    /**
     * Set the phone value
     *
     * Description: A telephone number for a specified business partner.
     *
     * @param value
     *          The phone value.
     * @return The builder for this class.
     */
    public Builder phone(String value) {
      this.dataFields.put("phone", value);
      return this;
    }

    /**
     * Set the partnerAddress value
     *
     * Description: The location of the selected business partner.
     *
     * @param value
     *          The partnerAddress value.
     * @return The builder for this class.
     */
    public Builder partnerAddress(String value) {
      this.dataFields.put("partnerAddress", value);
      return this;
    }

    /**
     * Set the birthday value
     *
     * Description: An anniversary of birth for a business partner.
     *
     * @param value
     *          The birthday value.
     * @return The builder for this class.
     */
    public Builder birthday(String value) {
      this.dataFields.put("birthday", value);
      return this;
    }

    /**
     * Set the alternativePhone value
     *
     * Description: A second contact telephone number for a business partner.
     *
     * @param value
     *          The alternativePhone value.
     * @return The builder for this class.
     */
    public Builder alternativePhone(String value) {
      this.dataFields.put("alternativePhone", value);
      return this;
    }

    /**
     * Set the greeting value
     *
     * Description: A description often abbreviated of how to address a business partner.
     *
     * @param value
     *          The greeting value.
     * @return The builder for this class.
     */
    public Builder greeting(String value) {
      this.dataFields.put("greeting", value);
      return this;
    }

    /**
     * Set the emailServerPassword value
     *
     * Description: Password of your email user id
     *
     * @param value
     *          The emailServerPassword value.
     * @return The builder for this class.
     */
    public Builder emailServerPassword(String value) {
      this.dataFields.put("emailServerPassword", value);
      return this;
    }

    /**
     * Set the emailServerUsername value
     *
     * Description: User Name (ID) in the Mail System
     *
     * @param value
     *          The emailServerUsername value.
     * @return The builder for this class.
     */
    public Builder emailServerUsername(String value) {
      this.dataFields.put("emailServerUsername", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the supervisor value
     *
     * Description: Supervisor for this user - used for escalation
     *
     * @param value
     *          The supervisor value.
     * @return The builder for this class.
     */
    public Builder supervisor(String value) {
      this.dataFields.put("supervisor", value);
      return this;
    }

    /**
     * Set the email value
     *
     * Description: An email address for a specified business partner.
     *
     * @param value
     *          The email value.
     * @return The builder for this class.
     */
    public Builder email(String value) {
      this.dataFields.put("email", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the password value
     *
     * Description: A secret code used to allow access to a specified window or tab.
     *
     * @param value
     *          The password value.
     * @return The builder for this class.
     */
    public Builder password(String value) {
      this.dataFields.put("password", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: Alphanumeric identifier of the entity
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the expired password value
     *
     * Description: Mark password as expired so user must change it in their next login
     *
     * @param value
     *          The expired password value
     * @return The builder for this class
     */
    public Builder isPasswordExpired(Boolean value) {
      this.dataFields.put("isPasswordExpired", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public UserData build() {
      return new UserData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private UserData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
