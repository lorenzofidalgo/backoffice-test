/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.workspace.widget;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ColumnData
 *
 * @author plujan
 *
 */
public class ColumnData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the whereClauseLeftPart value
     *
     * Description: Where Clause Left Part
     *
     * @param value
     *          The whereClauseLeftPart value.
     * @return The builder for this class.
     */
    public Builder whereClauseLeftPart(String value) {
      this.dataFields.put("whereClauseLeftPart", value);
      return this;
    }

    /**
     * Set the canBeFiltered value
     *
     * Description: Can be filtered
     *
     * @param value
     *          The canBeFiltered value.
     * @return The builder for this class.
     */
    public Builder canBeFiltered(Boolean value) {
      this.dataFields.put("canBeFiltered", value);
      return this;
    }

    /**
     * Set the hasLink value
     *
     * Description: Has Link
     *
     * @param value
     *          The hasLink value.
     * @return The builder for this class.
     */
    public Builder hasLink(Boolean value) {
      this.dataFields.put("hasLink", value);
      return this;
    }

    /**
     * Set the tab value
     *
     * Description: An indication that a tab is displayed within a window.
     *
     * @param value
     *          The tab value.
     * @return The builder for this class.
     */
    public Builder tab(String value) {
      this.dataFields.put("tab", value);
      return this;
    }

    /**
     * Set the includeIn value
     *
     * Description: Include In
     *
     * @param value
     *          The includeIn value.
     * @return The builder for this class.
     */
    public Builder includeIn(String value) {
      this.dataFields.put("includeIn", value);
      return this;
    }

    /**
     * Set the referenceSearchKey value
     *
     * Description: The exact reference specification for a list or a table.
     *
     * @param value
     *          The referenceSearchKey value.
     * @return The builder for this class.
     */
    public Builder referenceSearchKey(String value) {
      this.dataFields.put("referenceSearchKey", value);
      return this;
    }

    /**
     * Set the reference value
     *
     * Description: The data type of this field.
     *
     * @param value
     *          The reference value.
     * @return The builder for this class.
     */
    public Builder reference(String value) {
      this.dataFields.put("reference", value);
      return this;
    }

    /**
     * Set the width value
     *
     * Description: Width
     *
     * @param value
     *          The width value.
     * @return The builder for this class.
     */
    public Builder width(String value) {
      this.dataFields.put("width", value);
      return this;
    }

    /**
     * Set the summarizeType value
     *
     * Description: Summarize Type
     *
     * @param value
     *          The summarizeType value.
     * @return The builder for this class.
     */
    public Builder summarizeType(String value) {
      this.dataFields.put("summarizeType", value);
      return this;
    }

    /**
     * Set the sequenceNumber value
     *
     * Description: The order of records in a specified document.
     *
     * @param value
     *          The sequenceNumber value.
     * @return The builder for this class.
     */
    public Builder sequenceNumber(String value) {
      this.dataFields.put("sequenceNumber", value);
      return this;
    }

    /**
     * Set the widgetQuery value
     *
     * Description: Identifier of a HQL Query definition of a widget class implementing the
     * Query/List Superclass
     *
     * @param value
     *          The widgetQuery value.
     * @return The builder for this class.
     */
    public Builder widgetQuery(String value) {
      this.dataFields.put("widgetQuery", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the linkExpression value
     *
     * Description: Alias of the HQL Query column with the id required to built the link
     *
     * @param value
     *          The linkExpression value.
     * @return The builder for this class.
     */
    public Builder linkExpression(String value) {
      this.dataFields.put("linkExpression", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the displayExpression value
     *
     * Description: Alias of HQL query column to be shown
     *
     * @param value
     *          The displayExpression value.
     * @return The builder for this class.
     */
    public Builder displayExpression(String value) {
      this.dataFields.put("displayExpression", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ColumnData build() {
      return new ColumnData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ColumnData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
