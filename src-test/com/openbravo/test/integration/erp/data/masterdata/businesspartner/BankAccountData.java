/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.businesspartner;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for BankAccountData
 *
 * @author plujan
 *
 */
public class BankAccountData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the country value
     *
     * Description: A state or a nation.
     *
     * @param value
     *          The country value.
     * @return The builder for this class.
     */
    public Builder country(String value) {
      this.dataFields.put("country", value);
      return this;
    }

    /**
     * Set the displayedAccount value
     *
     * Description: Text that will identify this bank account
     *
     * @param value
     *          The displayedAccount value.
     * @return The builder for this class.
     */
    public Builder displayedAccount(String value) {
      this.dataFields.put("displayedAccount", value);
      return this;
    }

    /**
     * Set the showIBAN value
     *
     * Description: Show IBAN
     *
     * @param value
     *          The showIBAN value.
     * @return The builder for this class.
     */
    public Builder showIBAN(Boolean value) {
      this.dataFields.put("showIBAN", value);
      return this;
    }

    /**
     * Set the showGeneric value
     *
     * Description: Show Generic
     *
     * @param value
     *          The showGeneric value.
     * @return The builder for this class.
     */
    public Builder showGeneric(Boolean value) {
      this.dataFields.put("showGeneric", value);
      return this;
    }

    /**
     * Set the iBAN value
     *
     * Description: International standard for identifying bank accounts across national borders
     *
     * @param value
     *          The iBAN value.
     * @return The builder for this class.
     */
    public Builder iBAN(String value) {
      this.dataFields.put("iBAN", value);
      return this;
    }

    /**
     * Set the bankName value
     *
     * Description: Name of the bank
     *
     * @param value
     *          The bankName value.
     * @return The builder for this class.
     */
    public Builder bankName(String value) {
      this.dataFields.put("bankName", value);
      return this;
    }

    /**
     * Set the userContact value
     *
     * Description: An acquaintance to reach for information related to the business partner.
     *
     * @param value
     *          The userContact value.
     * @return The builder for this class.
     */
    public Builder userContact(String value) {
      this.dataFields.put("userContact", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(String value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the accountNo value
     *
     * Description: Account Number
     *
     * @param value
     *          The accountNo value.
     * @return The builder for this class.
     */
    public Builder accountNo(String value) {
      this.dataFields.put("accountNo", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public BankAccountData build() {
      return new BankAccountData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private BankAccountData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
