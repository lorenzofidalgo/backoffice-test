/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.businesspartner;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for CustomerData
 *
 * @author plujan
 *
 */
public class CustomerData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the maturityDate2 value
     *
     * Description: The day of the month that invoices are due. 3 maturity dates can be defined.
     *
     * @param value
     *          The maturityDate2 value.
     * @return The builder for this class.
     */
    public Builder maturityDate2(String value) {
      this.dataFields.put("maturityDate2", value);
      return this;
    }

    /**
     * Set the paymentMethod value
     *
     * Description: It is the method by which payment is expected to be made or received.
     *
     * @param value
     *          The paymentMethod value.
     * @return The builder for this class.
     */
    public Builder paymentMethod(String value) {
      this.dataFields.put("paymentMethod", value);
      return this;
    }

    /**
     * Set the account value
     *
     * Description: Financial account used to deposit / withdrawal money such as bank accounts or
     * petty cash
     *
     * @param value
     *          The account value.
     * @return The builder for this class.
     */
    public Builder account(String value) {
      this.dataFields.put("account", value);
      return this;
    }

    /**
     * Set the sOBPTaxCategory value
     *
     * Description: SO BP Tax Category
     *
     * @param value
     *          The sOBPTaxCategory value.
     * @return The builder for this class.
     */
    public Builder sOBPTaxCategory(String value) {
      this.dataFields.put("sOBPTaxCategory", value);
      return this;
    }

    /**
     * Set the transactionalBankAccount value
     *
     * Description: Default Customer Bankaccount
     *
     * @param value
     *          The transactionalBankAccount value.
     * @return The builder for this class.
     */
    public Builder transactionalBankAccount(String value) {
      this.dataFields.put("transactionalBankAccount", value);
      return this;
    }

    /**
     * Set the maturityDate3 value
     *
     * Description: The day of the month that invoices are due. 3 maturity dates can be defined.
     *
     * @param value
     *          The maturityDate3 value.
     * @return The builder for this class.
     */
    public Builder maturityDate3(String value) {
      this.dataFields.put("maturityDate3", value);
      return this;
    }

    /**
     * Set the maturityDate1 value
     *
     * Description: The day of the month that invoices are due. 3 maturity dates can be defined.
     *
     * @param value
     *          The maturityDate1 value.
     * @return The builder for this class.
     */
    public Builder maturityDate1(String value) {
      this.dataFields.put("maturityDate1", value);
      return this;
    }

    /**
     * Set the salesRepresentative value
     *
     * Description: The person in charge of a document.
     *
     * @param value
     *          The salesRepresentative value.
     * @return The builder for this class.
     */
    public Builder salesRepresentative(String value) {
      this.dataFields.put("salesRepresentative", value);
      return this;
    }

    /**
     * Set the invoiceTerms value
     *
     * Description: Frequency and method of invoicing
     *
     * @param value
     *          The invoiceTerms value.
     * @return The builder for this class.
     */
    public Builder invoiceTerms(String value) {
      this.dataFields.put("invoiceTerms", value);
      return this;
    }

    /**
     * Set the customer value
     *
     * Description: Indicates a business partner that is your customer ie. that will be making
     * purchases from you.
     *
     * @param value
     *          The customer value.
     * @return The builder for this class.
     */
    public Builder customer(Boolean value) {
      this.dataFields.put("customer", value);
      return this;
    }

    /**
     * Set the invoiceSchedule value
     *
     * Description: Schedule for generating Invoices
     *
     * @param value
     *          The invoiceSchedule value.
     * @return The builder for this class.
     */
    public Builder invoiceSchedule(String value) {
      this.dataFields.put("invoiceSchedule", value);
      return this;
    }

    /**
     * Set the creditUsed value
     *
     * Description: Current open balance
     *
     * @param value
     *          The creditUsed value.
     * @return The builder for this class.
     */
    public Builder creditUsed(String value) {
      this.dataFields.put("creditUsed", value);
      return this;
    }

    /**
     * Set the creditLimit value
     *
     * Description: Total outstanding invoice amounts allowed
     *
     * @param value
     *          The creditLimit value.
     * @return The builder for this class.
     */
    public Builder creditLimit(String value) {
      this.dataFields.put("creditLimit", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the priceList value
     *
     * Description: A catalog of selected items with prices defined generally or for a specific
     * partner.
     *
     * @param value
     *          The priceList value.
     * @return The builder for this class.
     */
    public Builder priceList(String value) {
      this.dataFields.put("priceList", value);
      return this;
    }

    /**
     * Set the paymentTerms value
     *
     * Description: The setup and timing defined to complete a specified payment.
     *
     * @param value
     *          The paymentTerms value.
     * @return The builder for this class.
     */
    public Builder paymentTerms(String value) {
      this.dataFields.put("paymentTerms", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public CustomerData build() {
      return new CustomerData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private CustomerData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
