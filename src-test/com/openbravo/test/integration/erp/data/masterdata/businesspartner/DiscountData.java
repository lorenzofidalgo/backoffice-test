/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.businesspartner;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;

/**
 *
 * Class for DiscountData
 *
 * @author plujan
 *
 */
public class DiscountData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the applyInOrder value
     *
     * Description: Apply the discount also to the Order
     *
     * @param value
     *          The applyInOrder value.
     * @return The builder for this class.
     */
    public Builder applyInOrder(Boolean value) {
      this.dataFields.put("applyInOrder", value);
      return this;
    }

    /**
     * Set the vendor value
     *
     * Description: A business partner who sells products or services.
     *
     * @param value
     *          The vendor value.
     * @return The builder for this class.
     */
    public Builder vendor(Boolean value) {
      this.dataFields.put("vendor", value);
      return this;
    }

    /**
     * Set the customer value
     *
     * Description: Indicates a business partner that is your customer ie. that will be making
     * purchases from you.
     *
     * @param value
     *          The customer value.
     * @return The builder for this class.
     */
    public Builder customer(Boolean value) {
      this.dataFields.put("customer", value);
      return this;
    }

    /**
     * Set the cascade value
     *
     * Description: Any additional discount built upon the remaining total after applying previous
     * discounts.
     *
     * @param value
     *          The cascade value.
     * @return The builder for this class.
     */
    public Builder cascade(Boolean value) {
      this.dataFields.put("cascade", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the discount value
     *
     * Description: The percentage price reduction based on the list price.
     *
     * @param value
     *          The discount value.
     * @return The builder for this class.
     */
    public Builder discount(String value) {
      this.dataFields.put("discount", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public DiscountData build() {
      return new DiscountData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private DiscountData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
