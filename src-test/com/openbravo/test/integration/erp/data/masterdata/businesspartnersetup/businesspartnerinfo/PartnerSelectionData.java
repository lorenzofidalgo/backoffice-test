/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.businesspartnersetup.businesspartnerinfo;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PartnerSelectionData
 *
 * @author plujan
 *
 */
public class PartnerSelectionData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the purchasePricelist value
     *
     * Description: Price List used by this Business Partner
     *
     * @param value
     *          The purchasePricelist value.
     * @return The builder for this class.
     */
    public Builder purchasePricelist(String value) {
      this.dataFields.put("purchasePricelist", value);
      return this;
    }

    /**
     * Set the pOPaymentTerms value
     *
     * Description: Payment rules for a purchase order
     *
     * @param value
     *          The pOPaymentTerms value.
     * @return The builder for this class.
     */
    public Builder pOPaymentTerms(String value) {
      this.dataFields.put("pOPaymentTerms", value);
      return this;
    }

    /**
     * Set the pOFormOfPayment value
     *
     * Description: Purchase payment option
     *
     * @param value
     *          The pOFormOfPayment value.
     * @return The builder for this class.
     */
    public Builder pOFormOfPayment(String value) {
      this.dataFields.put("pOFormOfPayment", value);
      return this;
    }

    /**
     * Set the priceList value
     *
     * Description: A catalog of selected items with prices defined generally or for a specific
     * partner.
     *
     * @param value
     *          The priceList value.
     * @return The builder for this class.
     */
    public Builder priceList(String value) {
      this.dataFields.put("priceList", value);
      return this;
    }

    /**
     * Set the paymentTerms value
     *
     * Description: The setup and timing defined to complete a specified payment.
     *
     * @param value
     *          The paymentTerms value.
     * @return The builder for this class.
     */
    public Builder paymentTerms(String value) {
      this.dataFields.put("paymentTerms", value);
      return this;
    }

    /**
     * Set the formOfPayment value
     *
     * Description: The method used for payment of this transaction.
     *
     * @param value
     *          The formOfPayment value.
     * @return The builder for this class.
     */
    public Builder formOfPayment(String value) {
      this.dataFields.put("formOfPayment", value);
      return this;
    }

    /**
     * Set the salesRepresentative value
     *
     * Description: The person in charge of a document.
     *
     * @param value
     *          The salesRepresentative value.
     * @return The builder for this class.
     */
    public Builder salesRepresentative(String value) {
      this.dataFields.put("salesRepresentative", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the employee value
     *
     * Description: A business partner who will be working for an organization.
     *
     * @param value
     *          The employee value.
     * @return The builder for this class.
     */
    public Builder employee(Boolean value) {
      this.dataFields.put("employee", value);
      return this;
    }

    /**
     * Set the isSalesRepresentative value
     *
     * Description: The person in charge of carry out an order.
     *
     * @param value
     *          The isSalesRepresentative value.
     * @return The builder for this class.
     */
    public Builder isSalesRepresentative(Boolean value) {
      this.dataFields.put("isSalesRepresentative", value);
      return this;
    }

    /**
     * Set the vendor value
     *
     * Description: A business partner who sells products or services.
     *
     * @param value
     *          The vendor value.
     * @return The builder for this class.
     */
    public Builder vendor(Boolean value) {
      this.dataFields.put("vendor", value);
      return this;
    }

    /**
     * Set the customer value
     *
     * Description: Indicates a business partner that is your customer ie. that will be making
     * purchases from you.
     *
     * @param value
     *          The customer value.
     * @return The builder for this class.
     */
    public Builder customer(Boolean value) {
      this.dataFields.put("customer", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the businessPartnerCategory value
     *
     * Description: A classification of business partners based on defined similarities.
     *
     * @param value
     *          The businessPartnerCategory value.
     * @return The builder for this class.
     */
    public Builder businessPartnerCategory(String value) {
      this.dataFields.put("businessPartnerCategory", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PartnerSelectionData build() {
      return new PartnerSelectionData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PartnerSelectionData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
