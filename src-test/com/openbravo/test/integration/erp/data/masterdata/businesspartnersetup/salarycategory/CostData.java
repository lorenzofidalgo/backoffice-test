/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.businesspartnersetup.salarycategory;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for CostData
 *
 * @author plujan
 *
 */
public class CostData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the costUOM value
     *
     * Description: The unit of measure associated with the cost located adjacent to this field.
     *
     * @param value
     *          The costUOM value.
     * @return The builder for this class.
     */
    public Builder costUOM(String value) {
      this.dataFields.put("costUOM", value);
      return this;
    }

    /**
     * Set the cost value
     *
     * Description: A charge related to conducting business.
     *
     * @param value
     *          The cost value.
     * @return The builder for this class.
     */
    public Builder cost(String value) {
      this.dataFields.put("cost", value);
      return this;
    }

    /**
     * Set the startingDate value
     *
     * Description: A parameter stating the starting time range related to a specified request.
     *
     * @param value
     *          The startingDate value.
     * @return The builder for this class.
     */
    public Builder startingDate(String value) {
      this.dataFields.put("startingDate", value);
      return this;
    }

    /**
     * Set the salaryCategory value
     *
     * Description: A classification of salaries based on similar characteristics or attributes.
     *
     * @param value
     *          The salaryCategory value.
     * @return The builder for this class.
     */
    public Builder salaryCategory(String value) {
      this.dataFields.put("salaryCategory", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public CostData build() {
      return new CostData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private CostData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
