/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.businesspartnersetup.vendordetails;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;

/**
 *
 * Class for ProductDetailsData
 *
 * @author plujan
 *
 */
public class ProductDetailsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the manufacturer value
     *
     * Description: The business partner that makes a specific product.
     *
     * @param value
     *          The manufacturer value.
     * @return The builder for this class.
     */
    public Builder manufacturer(String value) {
      this.dataFields.put("manufacturer", value);
      return this;
    }

    /**
     * Set the lastInvoicePrice value
     *
     * Description: The last price paid for this product as noted in a completed invoice.
     *
     * @param value
     *          The lastInvoicePrice value.
     * @return The builder for this class.
     */
    public Builder lastInvoicePrice(String value) {
      this.dataFields.put("lastInvoicePrice", value);
      return this;
    }

    /**
     * Set the uPCEAN value
     *
     * Description: A bar code with a number to identify a product.
     *
     * @param value
     *          The uPCEAN value.
     * @return The builder for this class.
     */
    public Builder uPCEAN(String value) {
      this.dataFields.put("uPCEAN", value);
      return this;
    }

    /**
     * Set the qualityRating value
     *
     * Description: Method for rating vendors
     *
     * @param value
     *          The qualityRating value.
     * @return The builder for this class.
     */
    public Builder qualityRating(String value) {
      this.dataFields.put("qualityRating", value);
      return this;
    }

    /**
     * Set the purchasingLeadTime value
     *
     * Description: Number of days between placing an order and the actual delivery as promised by
     * the vendor.
     *
     * @param value
     *          The purchasingLeadTime value.
     * @return The builder for this class.
     */
    public Builder purchasingLeadTime(String value) {
      this.dataFields.put("purchasingLeadTime", value);
      return this;
    }

    /**
     * Set the actualDeliveryDays value
     *
     * Description: Actual days between order and delivery
     *
     * @param value
     *          The actualDeliveryDays value.
     * @return The builder for this class.
     */
    public Builder actualDeliveryDays(String value) {
      this.dataFields.put("actualDeliveryDays", value);
      return this;
    }

    /**
     * Set the fixedCostPerOrder value
     *
     * Description: The additional added fixed cost for a purchased product.
     *
     * @param value
     *          The fixedCostPerOrder value.
     * @return The builder for this class.
     */
    public Builder fixedCostPerOrder(String value) {
      this.dataFields.put("fixedCostPerOrder", value);
      return this;
    }

    /**
     * Set the currentVendor value
     *
     * Description: Use this Vendor for pricing and stock replenishment
     *
     * @param value
     *          The currentVendor value.
     * @return The builder for this class.
     */
    public Builder currentVendor(Boolean value) {
      this.dataFields.put("currentVendor", value);
      return this;
    }

    /**
     * Set the vendorProductNo value
     *
     * Description: The identifier used by a vendor to indentify a product being purchased by their
     * business partners.
     *
     * @param value
     *          The vendorProductNo value.
     * @return The builder for this class.
     */
    public Builder vendorProductNo(String value) {
      this.dataFields.put("vendorProductNo", value);
      return this;
    }

    /**
     * Set the vendorCategory value
     *
     * Description: A classification of vendors based on similar characteristics or attributes.
     *
     * @param value
     *          The vendorCategory value.
     * @return The builder for this class.
     */
    public Builder vendorCategory(String value) {
      this.dataFields.put("vendorCategory", value);
      return this;
    }

    /**
     * Set the purchaseOrderPrice value
     *
     * Description: The price charged to purchase a specific item.
     *
     * @param value
     *          The purchaseOrderPrice value.
     * @return The builder for this class.
     */
    public Builder purchaseOrderPrice(String value) {
      this.dataFields.put("purchaseOrderPrice", value);
      return this;
    }

    /**
     * Set the priceEffectiveFrom value
     *
     * Description: The time from which a price is valid.
     *
     * @param value
     *          The priceEffectiveFrom value.
     * @return The builder for this class.
     */
    public Builder priceEffectiveFrom(String value) {
      this.dataFields.put("priceEffectiveFrom", value);
      return this;
    }

    /**
     * Set the listPrice value
     *
     * Description: The official price of a product in a specified currency.
     *
     * @param value
     *          The listPrice value.
     * @return The builder for this class.
     */
    public Builder listPrice(String value) {
      this.dataFields.put("listPrice", value);
      return this;
    }

    /**
     * Set the lastPurchasePrice value
     *
     * Description: The last price pad for this product as noted in a completed purchase order.
     *
     * @param value
     *          The lastPurchasePrice value.
     * @return The builder for this class.
     */
    public Builder lastPurchasePrice(String value) {
      this.dataFields.put("lastPurchasePrice", value);
      return this;
    }

    /**
     * Set the quantityPerPackage value
     *
     * Description: The number of a specific item that comes in one package.
     *
     * @param value
     *          The quantityPerPackage value.
     * @return The builder for this class.
     */
    public Builder quantityPerPackage(String value) {
      this.dataFields.put("quantityPerPackage", value);
      return this;
    }

    /**
     * Set the minimumOrderQty value
     *
     * Description: The minimum number of an item that must be purchased at one time from a vendor.
     *
     * @param value
     *          The minimumOrderQty value.
     * @return The builder for this class.
     */
    public Builder minimumOrderQty(String value) {
      this.dataFields.put("minimumOrderQty", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the discontinuedBy value
     *
     * Description: The name of the person who discontinues an item.
     *
     * @param value
     *          The discontinuedBy value.
     * @return The builder for this class.
     */
    public Builder discontinuedBy(String value) {
      this.dataFields.put("discontinuedBy", value);
      return this;
    }

    /**
     * Set the discontinued value
     *
     * Description: A statement mentioning that this product will no longer be available on the
     * market.
     *
     * @param value
     *          The discontinued value.
     * @return The builder for this class.
     */
    public Builder discontinued(Boolean value) {
      this.dataFields.put("discontinued", value);
      return this;
    }

    /**
     * Set the uOM value
     *
     * Description: A non monetary unit of measure.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ProductDetailsData build() {
      return new ProductDetailsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ProductDetailsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
