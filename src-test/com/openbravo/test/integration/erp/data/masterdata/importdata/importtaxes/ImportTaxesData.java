/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.importdata.importtaxes;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ImportTaxesData
 *
 * @author plujan
 *
 */
public class ImportTaxesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the regionToCode value
     *
     * Description: Region to Code
     *
     * @param value
     *          The regionToCode value.
     * @return The builder for this class.
     */
    public Builder regionToCode(String value) {
      this.dataFields.put("regionToCode", value);
      return this;
    }

    /**
     * Set the regionFromCode value
     *
     * Description: Region from Code
     *
     * @param value
     *          The regionFromCode value.
     * @return The builder for this class.
     */
    public Builder regionFromCode(String value) {
      this.dataFields.put("regionFromCode", value);
      return this;
    }

    /**
     * Set the countryFromCode value
     *
     * Description: Country from Code
     *
     * @param value
     *          The countryFromCode value.
     * @return The builder for this class.
     */
    public Builder countryFromCode(String value) {
      this.dataFields.put("countryFromCode", value);
      return this;
    }

    /**
     * Set the countryToCode value
     *
     * Description: Country to Code
     *
     * @param value
     *          The countryToCode value.
     * @return The builder for this class.
     */
    public Builder countryToCode(String value) {
      this.dataFields.put("countryToCode", value);
      return this;
    }

    /**
     * Set the parentTaxName value
     *
     * Description: Parent Tax Name
     *
     * @param value
     *          The parentTaxName value.
     * @return The builder for this class.
     */
    public Builder parentTaxName(String value) {
      this.dataFields.put("parentTaxName", value);
      return this;
    }

    /**
     * Set the importErrorMessage value
     *
     * Description: A message that appears when errors occur during the importing process.
     *
     * @param value
     *          The importErrorMessage value.
     * @return The builder for this class.
     */
    public Builder importErrorMessage(String value) {
      this.dataFields.put("importErrorMessage", value);
      return this;
    }

    /**
     * Set the bPTaxCategoryDescription value
     *
     * Description: BP Tax Category Description
     *
     * @param value
     *          The bPTaxCategoryDescription value.
     * @return The builder for this class.
     */
    public Builder bPTaxCategoryDescription(String value) {
      this.dataFields.put("bPTaxCategoryDescription", value);
      return this;
    }

    /**
     * Set the bPTaxCategoryName value
     *
     * Description: BP Tax Category Name
     *
     * @param value
     *          The bPTaxCategoryName value.
     * @return The builder for this class.
     */
    public Builder bPTaxCategoryName(String value) {
      this.dataFields.put("bPTaxCategoryName", value);
      return this;
    }

    /**
     * Set the businessPartnerTaxCategory value
     *
     * Description: Bussines Partner Tax Category as a Vendor
     *
     * @param value
     *          The businessPartnerTaxCategory value.
     * @return The builder for this class.
     */
    public Builder businessPartnerTaxCategory(String value) {
      this.dataFields.put("businessPartnerTaxCategory", value);
      return this;
    }

    /**
     * Set the commodityCode value
     *
     * Description: Commodity Code
     *
     * @param value
     *          The commodityCode value.
     * @return The builder for this class.
     */
    public Builder commodityCode(String value) {
      this.dataFields.put("commodityCode", value);
      return this;
    }

    /**
     * Set the taxCatDescription value
     *
     * Description: Tax Cat. Description
     *
     * @param value
     *          The taxCatDescription value.
     * @return The builder for this class.
     */
    public Builder taxCatDescription(String value) {
      this.dataFields.put("taxCatDescription", value);
      return this;
    }

    /**
     * Set the taxCategoryName value
     *
     * Description: Tax Category Name
     *
     * @param value
     *          The taxCategoryName value.
     * @return The builder for this class.
     */
    public Builder taxCategoryName(String value) {
      this.dataFields.put("taxCategoryName", value);
      return this;
    }

    /**
     * Set the taxCategory value
     *
     * Description: A classification of tax options based on similar characteristics or attributes.
     *
     * @param value
     *          The taxCategory value.
     * @return The builder for this class.
     */
    public Builder taxCategory(String value) {
      this.dataFields.put("taxCategory", value);
      return this;
    }

    /**
     * Set the cascade value
     *
     * Description: Any additional discount built upon the remaining total after applying previous
     * discounts.
     *
     * @param value
     *          The cascade value.
     * @return The builder for this class.
     */
    public Builder cascade(Boolean value) {
      this.dataFields.put("cascade", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the salesPurchaseType value
     *
     * Description: Sales Tax applies to sales situations Purchase Tax to purchase situations
     *
     * @param value
     *          The salesPurchaseType value.
     * @return The builder for this class.
     */
    public Builder salesPurchaseType(String value) {
      this.dataFields.put("salesPurchaseType", value);
      return this;
    }

    /**
     * Set the taxExempt value
     *
     * Description: A condition stating that for a specific case taxes must not be applied.
     *
     * @param value
     *          The taxExempt value.
     * @return The builder for this class.
     */
    public Builder taxExempt(Boolean value) {
      this.dataFields.put("taxExempt", value);
      return this;
    }

    /**
     * Set the destinationRegion value
     *
     * Description: The state/province inside of a country receiving the shipment.
     *
     * @param value
     *          The destinationRegion value.
     * @return The builder for this class.
     */
    public Builder destinationRegion(String value) {
      this.dataFields.put("destinationRegion", value);
      return this;
    }

    /**
     * Set the destinationCountry value
     *
     * Description: The country receiving a shipment
     *
     * @param value
     *          The destinationCountry value.
     * @return The builder for this class.
     */
    public Builder destinationCountry(String value) {
      this.dataFields.put("destinationCountry", value);
      return this;
    }

    /**
     * Set the region value
     *
     * Description: An area of a specific country.
     *
     * @param value
     *          The region value.
     * @return The builder for this class.
     */
    public Builder region(String value) {
      this.dataFields.put("region", value);
      return this;
    }

    /**
     * Set the country value
     *
     * Description: A state or a nation.
     *
     * @param value
     *          The country value.
     * @return The builder for this class.
     */
    public Builder country(String value) {
      this.dataFields.put("country", value);
      return this;
    }

    /**
     * Set the parentTaxRate value
     *
     * Description: Parent Tax indicates a tax that is made up of multiple taxes
     *
     * @param value
     *          The parentTaxRate value.
     * @return The builder for this class.
     */
    public Builder parentTaxRate(String value) {
      this.dataFields.put("parentTaxRate", value);
      return this;
    }

    /**
     * Set the rate value
     *
     * Description: The percentage to be multiplied by the source to arrive at the tax or exchange
     * amount.
     *
     * @param value
     *          The rate value.
     * @return The builder for this class.
     */
    public Builder rate(String value) {
      this.dataFields.put("rate", value);
      return this;
    }

    /**
     * Set the summaryLevel value
     *
     * Description: A means of grouping fields in order to view or hide additional information.
     *
     * @param value
     *          The summaryLevel value.
     * @return The builder for this class.
     */
    public Builder summaryLevel(Boolean value) {
      this.dataFields.put("summaryLevel", value);
      return this;
    }

    /**
     * Set the validFromDate value
     *
     * Description: A parameter stating the starting time of a specified request.
     *
     * @param value
     *          The validFromDate value.
     * @return The builder for this class.
     */
    public Builder validFromDate(String value) {
      this.dataFields.put("validFromDate", value);
      return this;
    }

    /**
     * Set the taxSearchKey value
     *
     * Description: A fast method for finding a specific tax.
     *
     * @param value
     *          The taxSearchKey value.
     * @return The builder for this class.
     */
    public Builder taxSearchKey(String value) {
      this.dataFields.put("taxSearchKey", value);
      return this;
    }

    /**
     * Set the taxDescription value
     *
     * Description: Tax Description
     *
     * @param value
     *          The taxDescription value.
     * @return The builder for this class.
     */
    public Builder taxDescription(String value) {
      this.dataFields.put("taxDescription", value);
      return this;
    }

    /**
     * Set the taxName value
     *
     * Description: Tax Name
     *
     * @param value
     *          The taxName value.
     * @return The builder for this class.
     */
    public Builder taxName(String value) {
      this.dataFields.put("taxName", value);
      return this;
    }

    /**
     * Set the tax value
     *
     * Description: The percentage of money requested by the government for this specified product
     * or transaction.
     *
     * @param value
     *          The tax value.
     * @return The builder for this class.
     */
    public Builder tax(String value) {
      this.dataFields.put("tax", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ImportTaxesData build() {
      return new ImportTaxesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ImportTaxesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
