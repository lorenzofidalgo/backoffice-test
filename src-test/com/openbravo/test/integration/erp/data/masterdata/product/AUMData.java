/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.product;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PriceData
 *
 * @author plujan
 *
 */
public class AUMData extends DataObject {

  /**
   * Class builder
   *
   * @author Rafael Queralta Pozo
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the uOM value
     *
     * Description: The alternate UOM.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the conversionRate value
     *
     * Description: The conversion rate between Operative UOM and Base UOM.
     *
     * @param value
     *          The conversionRate value.
     * @return The builder for this class.
     */
    public Builder conversionRate(String value) {
      this.dataFields.put("conversionRate", value);
      return this;
    }

    /**
     * Set the sales value
     *
     * Description: Sales priority
     *
     * @param value
     *          The sales value.
     * @return The builder for this class.
     */
    public Builder sales(String value) {
      this.dataFields.put("sales", value);
      return this;
    }

    /**
     * Set the purchase value
     *
     * Description: Purchase priority
     *
     * @param value
     *          The purchase value.
     * @return The builder for this class.
     */
    public Builder purchase(String value) {
      this.dataFields.put("purchase", value);
      return this;
    }

    /**
     * Set the logistics value
     *
     * Description: Logistics priority.
     *
     * @param value
     *          The logistics value.
     * @return The builder for this class.
     */
    public Builder logistics(String value) {
      this.dataFields.put("logistics", value);
      return this;
    }

    /**
     * Set the gtin value
     *
     * Description: Product Gtin.
     *
     * @param value
     *          The gtin value.
     * @return The builder for this class.
     */
    public Builder gtin(String value) {
      this.dataFields.put("gtin", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AUMData build() {
      return new AUMData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AUMData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
