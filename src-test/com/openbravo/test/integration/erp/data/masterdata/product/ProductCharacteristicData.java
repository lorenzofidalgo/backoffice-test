package com.openbravo.test.integration.erp.data.masterdata.product;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

public class ProductCharacteristicData extends DataObject {
  /**
   * Class builder
   *
   * @author jbueno
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the variant value
     *
     * Description: Checked if it is a variant characteristic.
     *
     * @param value
     *          The is variant value.
     * @return The builder for this class.
     */
    public Builder variant(Boolean value) {
      this.dataFields.put("variant", value);
      return this;
    }

    /**
     * Set the Explode configuration
     *
     * Description: Checked if the characteristic will explode with all its values
     *
     * @param value
     *          The explode configuration tab value.
     * @return The builder for this class.
     */
    public Builder explode(Boolean value) {
      this.dataFields.put("explodeConfigurationTab", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ProductCharacteristicData build() {
      return new ProductCharacteristicData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ProductCharacteristicData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
