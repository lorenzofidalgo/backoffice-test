/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.product;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ProductData
 *
 * @author plujan
 *
 */
public class ProductData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the uOM value
     *
     * Description: A non monetary unit of measure.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the stocked value
     *
     * Description: Organization stocks this product
     *
     * @param value
     *          The stocked value.
     * @return The builder for this class.
     */
    public Builder stocked(Boolean value) {
      this.dataFields.put("stocked", value);
      return this;
    }

    /**
     * Set the purchase value
     *
     * Description: An indication that an item may be purchased by a business partner.
     *
     * @param value
     *          The purchase value.
     * @return The builder for this class.
     */
    public Builder purchase(Boolean value) {
      this.dataFields.put("purchase", value);
      return this;
    }

    /**
     * Set the sale value
     *
     * Description: An indication that an item may be sold by a business partner.
     *
     * @param value
     *          The sale value.
     * @return The builder for this class.
     */
    public Builder sale(Boolean value) {
      this.dataFields.put("sale", value);
      return this;
    }

    /**
     * Set the weight value
     *
     * Description: Weight of a product
     *
     * @param value
     *          The weight value.
     * @return The builder for this class.
     */
    public Builder weight(String value) {
      this.dataFields.put("weight", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the productCategory value
     *
     * Description: A classification of items based on similar characteristics or attributes.
     *
     * @param value
     *          The productCategory value.
     * @return The builder for this class.
     */
    public Builder productCategory(String value) {
      this.dataFields.put("productCategory", value);
      return this;
    }

    /**
     * Set the taxCategory value
     *
     * Description: A classification of tax options based on similar characteristics or attributes.
     *
     * @param value
     *          The taxCategory value.
     * @return The builder for this class.
     */
    public Builder taxCategory(String value) {
      this.dataFields.put("taxCategory", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the productType value
     *
     * Description: An important classification used to determine the accounting and management of a
     * product.
     *
     * @param value
     *          The productType value.
     * @return The builder for this class.
     */
    public Builder productType(String value) {
      this.dataFields.put("productType", value);
      return this;
    }

    /**
     * Set the attributeSet value
     *
     * Description: A group of attributes which are assigned to a selected product.
     *
     * @param value
     *          The attributeSet value.
     * @return The builder for this class.
     */
    public Builder attributeSet(String value) {
      this.dataFields.put("attributeSet", value);
      return this;
    }

    /**
     * Set the costType value
     *
     * Description: A distinct cost characteristic used for processes.
     *
     * @param value
     *          The costType value.
     * @return The builder for this class.
     */
    public Builder costType(String value) {
      this.dataFields.put("costType", value);
      return this;
    }

    /**
     * Set the standardCost value
     *
     * Description: Standard Cost
     *
     * @param value
     *          The standardCost value.
     * @return The builder for this class.
     */
    public Builder standardCost(String value) {
      this.dataFields.put("standardCost", value);
      return this;
    }

    /**
     * Set the useAttributeSetValueAs value
     *
     * Description: A distinct Attribute Set Value characteristic.
     *
     * @param value
     *          The useAttributeSetValueAs value.
     * @return The builder for this class.
     */
    public Builder useAttributeSetValueAs(String value) {
      this.dataFields.put("useAttributeSetValueAs", value);
      return this;
    }

    /**
     * Set the is generic value
     *
     * Description: An indication that the product is a generic product.
     *
     * @param value
     *          True if it is a generic product, false in other case.
     * @return The builder for this class.
     */
    public Builder generic(Boolean value) {
      this.dataFields.put("isGeneric", value);
      return this;
    }

    /**
     * Set the is deferred revenue value
     *
     * @param value
     *          True if it is a deferred revenue, false in other case.
     * @return The builder for this class.
     */
    public Builder isDeferredRevenue(Boolean value) {
      this.dataFields.put("deferredRevenue", value);
      return this;
    }

    /**
     * Set the revenue plan type value
     *
     * @param value
     *          The Revenue Plan Type value
     * @return The builder for this class.
     */
    public Builder revenuePlanType(String value) {
      this.dataFields.put("revenuePlanType", value);
      return this;
    }

    /**
     * Set the period number value
     *
     * @param value
     *          The Period Number value
     * @return The builder for this class.
     */
    public Builder revenuePeriodNumber(String value) {
      this.dataFields.put("periodNumber", value);
      return this;
    }

    /**
     * Set the default period value
     *
     * @param value
     *          The Default Period value
     * @return The builder for this class.
     */
    public Builder revenueDefaultPeriod(String value) {
      this.dataFields.put("defaultPeriod", value);
      return this;
    }

    /**
     * Set the is deferred expense value
     *
     * @param value
     *          True if it is a deferred expense, false in other case.
     * @return The builder for this class.
     */
    public Builder isDeferredExpense(Boolean value) {
      this.dataFields.put("isdeferredexpense", value);
      return this;
    }

    /**
     * Set the expense plan type value
     *
     * @param value
     *          The Expense Plan Type value
     * @return The builder for this class.
     */
    public Builder expensePlanType(String value) {
      this.dataFields.put("expplantype", value);
      return this;
    }

    /**
     * Set the expense period number value
     *
     * @param value
     *          The Expense Period Number value
     * @return The builder for this class.
     */
    public Builder expensePeriodNumber(String value) {
      this.dataFields.put("periodnumberExp", value);
      return this;
    }

    /**
     * Set the expense default period value
     *
     * @param value
     *          The Expense Default Period value
     * @return The builder for this class.
     */
    public Builder expenseDefaultPeriod(String value) {
      this.dataFields.put("defaultPeriodExpense", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ProductData build() {
      return new ProductData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ProductData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
