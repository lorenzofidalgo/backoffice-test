/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2019 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.product;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;

/**
 *
 * Class for ProductStockData
 *
 *
 */
public class ProductStockData extends DataObject {

  /**
   * Class builder
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the storageBin value
     *
     * Description: The locator.
     *
     * @param value
     *          The storageBin value.
     * @return The builder for this class.
     */
    public Builder storageBin(String value) {
      this.dataFields.put("storageBin", value);
      return this;
    }

    /**
     * Set the quantityOnHand value
     *
     * Description: quantityOnHand
     *
     * @param value
     *          The quantityOnHand value.
     * @return The builder for this class.
     */
    public Builder quantityOnHand(String value) {
      this.dataFields.put("quantityOnHand", value);
      return this;
    }

    /**
     * Set the reservedQty value
     *
     * Description: reservedQty
     *
     * @param value
     *          The reservedQty value.
     * @return The builder for this class.
     */
    public Builder reservedQty(String value) {
      this.dataFields.put("reservedQty", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductCompleteSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ProductStockData build() {
      return new ProductStockData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ProductStockData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
