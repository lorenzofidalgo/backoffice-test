/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.materialrequirementmrp.setup.planningmethod;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PlanningMethodLinesData
 *
 * @author plujan
 *
 */
public class PlanningMethodLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the daysToEnd value
     *
     * Description: Days to End
     *
     * @param value
     *          The daysToEnd value.
     * @return The builder for this class.
     */
    public Builder daysToEnd(String value) {
      this.dataFields.put("daysToEnd", value);
      return this;
    }

    /**
     * Set the daysFromStart value
     *
     * Description: Days from Start
     *
     * @param value
     *          The daysFromStart value.
     * @return The builder for this class.
     */
    public Builder daysFromStart(String value) {
      this.dataFields.put("daysFromStart", value);
      return this;
    }

    /**
     * Set the weighting value
     *
     * Description: Weighting
     *
     * @param value
     *          The weighting value.
     * @return The builder for this class.
     */
    public Builder weighting(String value) {
      this.dataFields.put("weighting", value);
      return this;
    }

    /**
     * Set the transactionType value
     *
     * Description: A distinct set of characteristics or processes.
     *
     * @param value
     *          The transactionType value.
     * @return The builder for this class.
     */
    public Builder transactionType(String value) {
      this.dataFields.put("transactionType", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the planningMethod value
     *
     * Description: Planning Method
     *
     * @param value
     *          The planningMethod value.
     * @return The builder for this class.
     */
    public Builder planningMethod(String value) {
      this.dataFields.put("planningMethod", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PlanningMethodLinesData build() {
      return new PlanningMethodLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PlanningMethodLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
