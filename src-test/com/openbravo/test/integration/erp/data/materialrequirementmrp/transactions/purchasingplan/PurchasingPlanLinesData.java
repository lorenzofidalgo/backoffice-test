/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.materialrequirementmrp.transactions.purchasingplan;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;

/**
 *
 * Class for PurchasingPlanLinesData
 *
 * @author plujan
 *
 */
public class PurchasingPlanLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the totalMovementQuantity value
     *
     * Description: The final quantity to be moved based on the related business process.
     *
     * @param value
     *          The totalMovementQuantity value.
     * @return The builder for this class.
     */
    public Builder totalMovementQuantity(String value) {
      this.dataFields.put("totalMovementQuantity", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the plannedOrderDate value
     *
     * Description: Planned Order Date
     *
     * @param value
     *          The plannedOrderDate value.
     * @return The builder for this class.
     */
    public Builder plannedOrderDate(String value) {
      this.dataFields.put("plannedOrderDate", value);
      return this;
    }

    /**
     * Set the workRequirement value
     *
     * Description: An order authorizing the production of a specific product and product quantity.
     *
     * @param value
     *          The workRequirement value.
     * @return The builder for this class.
     */
    public Builder workRequirement(String value) {
      this.dataFields.put("workRequirement", value);
      return this;
    }

    /**
     * Set the requisitionLine value
     *
     * Description: Requisition Line
     *
     * @param value
     *          The requisitionLine value.
     * @return The builder for this class.
     */
    public Builder requisitionLine(String value) {
      this.dataFields.put("requisitionLine", value);
      return this;
    }

    /**
     * Set the salesForecastLine value
     *
     * Description: Sales Forecast Line
     *
     * @param value
     *          The salesForecastLine value.
     * @return The builder for this class.
     */
    public Builder salesForecastLine(String value) {
      this.dataFields.put("salesForecastLine", value);
      return this;
    }

    /**
     * Set the salesOrderLine value
     *
     * Description: A unique and often automatically generated identifier for an order line.
     *
     * @param value
     *          The salesOrderLine value.
     * @return The builder for this class.
     */
    public Builder salesOrderLine(String value) {
      this.dataFields.put("salesOrderLine", value);
      return this;
    }

    /**
     * Set the fixed value
     *
     * Description: A means of locking the header tab so it will not be affected if a specified
     * process is run again.
     *
     * @param value
     *          The fixed value.
     * @return The builder for this class.
     */
    public Builder fixed(Boolean value) {
      this.dataFields.put("fixed", value);
      return this;
    }

    /**
     * Set the transactionType value
     *
     * Description: A distinct set of characteristics or processes.
     *
     * @param value
     *          The transactionType value.
     * @return The builder for this class.
     */
    public Builder transactionType(String value) {
      this.dataFields.put("transactionType", value);
      return this;
    }

    /**
     * Set the plannedDate value
     *
     * Description: The date when a transaction will occur.
     *
     * @param value
     *          The plannedDate value.
     * @return The builder for this class.
     */
    public Builder plannedDate(String value) {
      this.dataFields.put("plannedDate", value);
      return this;
    }

    /**
     * Set the requiredQuantity value
     *
     * Description: Required Quantity
     *
     * @param value
     *          The requiredQuantity value.
     * @return The builder for this class.
     */
    public Builder requiredQuantity(String value) {
      this.dataFields.put("requiredQuantity", value);
      return this;
    }

    /**
     * Set the quantity value
     *
     * Description: The number of a certain item.
     *
     * @param value
     *          The quantity value.
     * @return The builder for this class.
     */
    public Builder quantity(String value) {
      this.dataFields.put("quantity", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the purchasingPlan value
     *
     * Description: Purchasing Plan
     *
     * @param value
     *          The purchasingPlan value.
     * @return The builder for this class.
     */
    public Builder purchasingPlan(String value) {
      this.dataFields.put("purchasingPlan", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PurchasingPlanLinesData build() {
      return new PurchasingPlanLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PurchasingPlanLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
