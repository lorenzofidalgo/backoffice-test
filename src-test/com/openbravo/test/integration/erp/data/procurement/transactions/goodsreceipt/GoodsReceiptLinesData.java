/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *  Nono Carballo <nonofce@gmail.com>
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.LocatorSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;

/**
 *
 * Class for GoodsReceiptLinesData
 *
 * @author plujan
 *
 */
public class GoodsReceiptLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the attributeSetValue value
     *
     * Description: An attribute associated with a product as part of an attribute set.
     *
     * @param value
     *          The attributeSetValue value.
     * @return The builder for this class.
     */
    public Builder attributeSetValue(String value) {
      this.dataFields.put("attributeSetValue", value);
      return this;
    }

    /**
     * Set the salesOrderLine value
     *
     * Description: Purchase Order Line
     *
     * @param value
     *          The salesOrderLine value.
     * @return The builder for this class.
     */
    public Builder salesOrderLine(String value) {
      this.dataFields.put("salesOrderLine", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the uOM value
     *
     * Description: A non monetary unit of measure.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the movementQuantity value
     *
     * Description: The number of items being moved from one location to another.
     *
     * @param value
     *          The movementQuantity value.
     * @return The builder for this class.
     */
    public Builder movementQuantity(String value) {
      this.dataFields.put("movementQuantity", value);
      return this;
    }

    /**
     * Set the operative quantity value
     *
     * @param value
     *          The Operative Quantity
     * @return The builder for this class.
     */
    public Builder operativeQuantity(String value) {
      this.dataFields.put("operativeQuantity", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductCompleteSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the storageBin value
     *
     * Description: A set of coordinates (x y z) which help locate an item in a warehouse.
     *
     * @param value
     *          The storageBin value.
     * @return The builder for this class.
     */
    public Builder storageBin(LocatorSelectorData value) {
      this.dataFields.put("storageBin", value);
      return this;
    }

    /**
     * Set the storageBin value
     *
     * Description: A set of coordinates (x y z) which help locate an item in a warehouse.
     *
     * @param value
     *          The storageBin value.
     * @return The builder for this class.
     */
    public Builder storageBin(String value) {
      this.dataFields.put("storageBin", value);
      return this;
    }

    /**
     * Set the shipmentReceipt value
     *
     * Description: The document number for identifying an act of receiving goods.
     *
     * @param value
     *          The shipmentReceipt value.
     * @return The builder for this class.
     */
    public Builder shipmentReceipt(String value) {
      this.dataFields.put("shipmentReceipt", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the business partner value
     *
     * Description: Business partner entity within client
     *
     * @param value
     *          The business partner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(String value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the project value
     *
     * Description: Project entity within client
     *
     * @param value
     *          The project value.
     * @return The builder for this class.
     */
    public Builder project(String value) {
      this.dataFields.put("project", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public GoodsReceiptLinesData build() {
      return new GoodsReceiptLinesData(this);
    }

    /**
     * Set the operative UOM
     *
     * @param value
     *          The Alternative UOM
     * @return The builder for this class.
     */
    public Builder operativeUOM(String value) {
      this.dataFields.put("operativeUOM", value);
      return this;
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private GoodsReceiptLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
