/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.procurement.transactions.landedcost;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.ShipmentReceiptLineSelectorData;

/**
 *
 * Class for LandedCostReceiptData
 *
 * @author plujan
 *
 */
public class LandedCostReceiptData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the goodsShipmentLine value
     *
     * @param value
     *          The goodsShipmentLine value.
     * @return The builder for this class.
     */
    public Builder goodsShipmentLine(ShipmentReceiptLineSelectorData value) {
      this.dataFields.put("goodsShipmentLine", value);
      return this;
    }

    /**
     * Set the goodsShipment value
     *
     * @param value
     *          The goodsShipment value.
     * @return The builder for this class.
     */
    public Builder goodsShipment(String value) {
      this.dataFields.put("goodsShipment", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public LandedCostReceiptData build() {
      return new LandedCostReceiptData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private LandedCostReceiptData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
