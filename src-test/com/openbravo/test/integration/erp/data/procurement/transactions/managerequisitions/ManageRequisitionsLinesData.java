/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.procurement.transactions.managerequisitions;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;

/**
 *
 * Class for ManageRequisitionsLinesData
 *
 * @author plujan
 *
 */
public class ManageRequisitionsLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the lockCause value
     *
     * Description: Lock cause
     *
     * @param value
     *          The lockCause value.
     * @return The builder for this class.
     */
    public Builder lockCause(String value) {
      this.dataFields.put("lockCause", value);
      return this;
    }

    /**
     * Set the lockDate value
     *
     * Description: Lock date
     *
     * @param value
     *          The lockDate value.
     * @return The builder for this class.
     */
    public Builder lockDate(String value) {
      this.dataFields.put("lockDate", value);
      return this;
    }

    /**
     * Set the priceList value
     *
     * Description: A catalog of selected items with prices defined generally or for a specific
     * partner.
     *
     * @param value
     *          The priceList value.
     * @return The builder for this class.
     */
    public Builder priceList(String value) {
      this.dataFields.put("priceList", value);
      return this;
    }

    /**
     * Set the lockPrice value
     *
     * Description: Lock price
     *
     * @param value
     *          The lockPrice value.
     * @return The builder for this class.
     */
    public Builder lockPrice(String value) {
      this.dataFields.put("lockPrice", value);
      return this;
    }

    /**
     * Set the lockQty value
     *
     * Description: Lock qty
     *
     * @param value
     *          The lockQty value.
     * @return The builder for this class.
     */
    public Builder lockQty(String value) {
      this.dataFields.put("lockQty", value);
      return this;
    }

    /**
     * Set the lockedBy value
     *
     * Description: Locked by
     *
     * @param value
     *          The lockedBy value.
     * @return The builder for this class.
     */
    public Builder lockedBy(String value) {
      this.dataFields.put("lockedBy", value);
      return this;
    }

    /**
     * Set the unitPrice value
     *
     * Description: The price that will be paid for a specified item.
     *
     * @param value
     *          The unitPrice value.
     * @return The builder for this class.
     */
    public Builder unitPrice(String value) {
      this.dataFields.put("unitPrice", value);
      return this;
    }

    /**
     * Set the discount value
     *
     * Description: The proportional discount given to an item without respect to any previously
     * defined discounts.
     *
     * @param value
     *          The discount value.
     * @return The builder for this class.
     */
    public Builder discount(String value) {
      this.dataFields.put("discount", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the matchedPOQty value
     *
     * Description: Matched PO Qty
     *
     * @param value
     *          The matchedPOQty value.
     * @return The builder for this class.
     */
    public Builder matchedPOQty(String value) {
      this.dataFields.put("matchedPOQty", value);
      return this;
    }

    /**
     * Set the requisitionLineStatus value
     *
     * Description: Requisition line status
     *
     * @param value
     *          The requisitionLineStatus value.
     * @return The builder for this class.
     */
    public Builder requisitionLineStatus(String value) {
      this.dataFields.put("requisitionLineStatus", value);
      return this;
    }

    /**
     * Set the attributeSetValue value
     *
     * Description: An attribute associated with a product as part of an attribute set.
     *
     * @param value
     *          The attributeSetValue value.
     * @return The builder for this class.
     */
    public Builder attributeSetValue(String value) {
      this.dataFields.put("attributeSetValue", value);
      return this;
    }

    /**
     * Set the orderQuantity value
     *
     * Description: The number of a certain item involved in the transaction shown in units which
     * differ from the standard UOM.
     *
     * @param value
     *          The orderQuantity value.
     * @return The builder for this class.
     */
    public Builder orderQuantity(String value) {
      this.dataFields.put("orderQuantity", value);
      return this;
    }

    /**
     * Set the operative quantity value
     *
     * @param value
     *          The Operative Quantity
     * @return The builder for this class.
     */
    public Builder operativeQuantity(String value) {
      this.dataFields.put("operativeQuantity", value);
      return this;
    }

    /**
     * Set the orderUOM value
     *
     * Description: The unit of measure being used for the request.
     *
     * @param value
     *          The orderUOM value.
     * @return The builder for this class.
     */
    public Builder orderUOM(String value) {
      this.dataFields.put("orderUOM", value);
      return this;
    }

    /**
     * Set the uOM value
     *
     * Description: A non monetary unit of measure.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the notesForSupplier value
     *
     * Description: Notes for supplier
     *
     * @param value
     *          The notesForSupplier value.
     * @return The builder for this class.
     */
    public Builder notesForSupplier(String value) {
      this.dataFields.put("notesForSupplier", value);
      return this;
    }

    /**
     * Set the internalNotes value
     *
     * Description: Internal notes
     *
     * @param value
     *          The internalNotes value.
     * @return The builder for this class.
     */
    public Builder internalNotes(String value) {
      this.dataFields.put("internalNotes", value);
      return this;
    }

    /**
     * Set the lineNetAmount value
     *
     * Description: The final amount of a specified line based only on quantities and prices.
     *
     * @param value
     *          The lineNetAmount value.
     * @return The builder for this class.
     */
    public Builder lineNetAmount(String value) {
      this.dataFields.put("lineNetAmount", value);
      return this;
    }

    /**
     * Set the listPrice value
     *
     * Description: The official price of a product in a specified currency.
     *
     * @param value
     *          The listPrice value.
     * @return The builder for this class.
     */
    public Builder listPrice(String value) {
      this.dataFields.put("listPrice", value);
      return this;
    }

    /**
     * Set the quantity value
     *
     * Description: The number of a certain item.
     *
     * @param value
     *          The quantity value.
     * @return The builder for this class.
     */
    public Builder quantity(String value) {
      this.dataFields.put("quantity", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductCompleteSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the needByDate value
     *
     * Description: Need by date
     *
     * @param value
     *          The needByDate value.
     * @return The builder for this class.
     */
    public Builder needByDate(String value) {
      this.dataFields.put("needByDate", value);
      return this;
    }

    /**
     * Set the requisition value
     *
     * Description: Requisition
     *
     * @param value
     *          The requisition value.
     * @return The builder for this class.
     */
    public Builder requisition(String value) {
      this.dataFields.put("requisition", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ManageRequisitionsLinesData build() {
      return new ManageRequisitionsLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ManageRequisitionsLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
