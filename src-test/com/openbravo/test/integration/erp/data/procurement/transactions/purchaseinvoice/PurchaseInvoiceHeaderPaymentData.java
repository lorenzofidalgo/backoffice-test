/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2015 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2010-12-28 18:05:34
 * Contributor(s):
 *      Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice;

/**
 *
 * @author plujan
 *
 */
public class PurchaseInvoiceHeaderPaymentData {

  /* Data fields */
  /** To Be Paid To */
  private String payingTo;
  /** Payment Method */
  private String paymentMethod;
  /** Take From */
  private String takeFrom;
  /** Currency */
  private String currency;
  /** Expected Payment */
  private String expectedPayment;
  /** Actual Payment */
  private String actualPayment;
  /** Payment Date */
  private String paymentDate;
  /** Transaction Type */
  private String transactionType;

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /* Builder fields */
    /** To Be Paid To */
    private String payingTo = null;
    /** Payment Method */
    private String paymentMethod = null;
    /** Take From */
    private String takeFrom = null;
    /** Currency */
    private String currency = null;
    /** Expected Payment */
    private String expectedPayment = null;
    /** Actual Payment */
    private String actualPayment = null;
    /** Payment Date */
    private String paymentDate = null;
    /** Transaction Type */
    private String transactionType = null;

    /**
     * Set the To Be Paid To value.
     *
     * @param value
     *          The To Be Paid To value.
     * @return The builder for this class.
     */
    public Builder payingTo(String value) {
      this.payingTo = value;
      return this;
    }

    /**
     * Set the Payment Method value.
     *
     * @param value
     *          The Payment Method value.
     * @return The builder for this class.
     */
    public Builder paymentMethod(String value) {
      this.paymentMethod = value;
      return this;
    }

    /**
     * Set the Take From value.
     *
     * @param value
     *          The Take From value.
     * @return The builder for this class.
     */
    public Builder takeFrom(String value) {
      this.takeFrom = value;
      return this;
    }

    /**
     * Set the Currency value.
     *
     * @param value
     *          The Currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.currency = value;
      return this;
    }

    /**
     * Set the Expected Payment value.
     *
     * @param value
     *          The Expected Payment value.
     * @return The builder for this class.
     */
    public Builder expectedPayment(String value) {
      this.expectedPayment = value;
      return this;
    }

    /**
     * Set the Actual Payment value.
     *
     * @param value
     *          The Actual Payment value.
     * @return The builder for this class.
     */
    public Builder actualPayment(String value) {
      this.actualPayment = value;
      return this;
    }

    /**
     * Set the Payment Date value.
     *
     * @param value
     *          The Payment Date value.
     * @return The builder for this class.
     */
    public Builder paymentDate(String value) {
      this.paymentDate = value;
      return this;
    }

    /**
     * Set the Transaction Type value.
     *
     * @param value
     *          The Transaction Type value.
     * @return The builder for this class.
     */
    public Builder transactionType(String value) {
      this.transactionType = value;
      return this;
    }

    /**
     * Set the fields for this object that are required on the ERP.
     *
     * @param payingToField
     *          The To Be Paid To value.
     * @param paymentMethodField
     *          The Payment Method value.
     * @param takeFromField
     *          The Take From value.
     * @param currencyField
     *          The Currency value.
     * @param expectedPaymentField
     *          The Expected Payment value.
     * @param actualPaymentField
     *          The Actual Payment value.
     * @param paymentDateField
     *          The Payment Date value.
     * @param transactionTypeField
     *          The Transaction Type value.
     * @return The builder for this class
     */
    public Builder requiredFields(String payingToField, String paymentMethodField,
        String takeFromField, String currencyField, String expectedPaymentField,
        String actualPaymentField, String paymentDateField, String transactionTypeField) {
      this.payingTo = payingToField;
      this.paymentMethod = payingToField;
      this.takeFrom = payingToField;
      this.currency = currencyField;
      this.expectedPayment = expectedPaymentField;
      this.actualPayment = actualPaymentField;
      this.paymentDate = paymentDateField;
      this.transactionType = transactionTypeField;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PurchaseInvoiceHeaderPaymentData build() {
      return new PurchaseInvoiceHeaderPaymentData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PurchaseInvoiceHeaderPaymentData(Builder builder) {
    payingTo = builder.payingTo;
    paymentMethod = builder.paymentMethod;
    takeFrom = builder.takeFrom;
    currency = builder.currency;
    expectedPayment = builder.expectedPayment;
    actualPayment = builder.actualPayment;
    paymentDate = builder.paymentDate;
    transactionType = builder.transactionType;

  }

  public String getPayingTo() {
    return payingTo;
  }

  public String getPaymentMethod() {
    return paymentMethod;
  }

  public String getTakeFrom() {
    return takeFrom;
  }

  public String getCurrency() {
    return currency;
  }

  public String getExpectedPayment() {
    return expectedPayment;
  }

  public String getActualPayment() {
    return actualPayment;
  }

  public String getPaymentDate() {
    return paymentDate;
  }

  public String getTransactionType() {
    return transactionType;
  }

  @Override
  public String toString() {
    return "PurchaseInvoiceHeaderAddPaymentPopUpData [payingTo=" + payingTo + ", paymentMethod="
        + paymentMethod + ", takeFrom=" + takeFrom + ", currency=" + currency + ", expectedPayment="
        + expectedPayment + ", actualPayment=" + actualPayment + ", paymentDate=" + paymentDate
        + ", transactionType=" + transactionType + "]";
  }

}
