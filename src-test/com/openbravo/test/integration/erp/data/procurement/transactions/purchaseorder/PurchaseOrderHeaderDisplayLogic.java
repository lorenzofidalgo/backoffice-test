/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PurchaseOrderHeaderData
 *
 * @author Lorenzo Fidalgo
 *
 */
public class PurchaseOrderHeaderDisplayLogic extends DataObject {

  /**
   * Class builder
   *
   * @author Lorenzo Fidalgo
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(boolean value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the priceList value
     *
     * Description: A catalog of selected items with prices defined generally or for a specific
     * partner.
     *
     * @param value
     *          The priceList value.
     * @return The builder for this class.
     */
    public Builder priceList(boolean value) {
      this.dataFields.put("priceList", value);
      return this;
    }

    /**
     * Set the documentNo value
     *
     * Description: An often automatically generated identifier for all documents.
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(boolean value) {
      this.dataFields.put("documentNo", value);
      return this;
    }

    /**
     * Set the documentStatus value
     *
     * Description: The Document Status indicates the status of a document at this time.
     *
     * @param value
     *          The documentStatus value.
     * @return The builder for this class.
     */
    public Builder documentStatus(boolean value) {
      this.dataFields.put("documentStatus", value);
      return this;
    }

    /**
     * Set the transactionDocument value
     *
     * Description: The specific document type which should be used for the document.
     *
     * @param value
     *          The transactionDocument value.
     * @return The builder for this class.
     */
    public Builder transactionDocument(boolean value) {
      this.dataFields.put("transactionDocument", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(boolean value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the orderDate value
     *
     * Description: The time listed on the order.
     *
     * @param value
     *          The orderDate value.
     * @return The builder for this class.
     */
    public Builder orderDate(boolean value) {
      this.dataFields.put("orderDate", value);
      return this;
    }

    /**
     * Set the scheduledDeliveryDate value
     *
     * Description: The date that a task process or action is to be completed or delivered by.
     *
     * @param value
     *          The scheduledDeliveryDate value.
     * @return The builder for this class.
     */
    public Builder scheduledDeliveryDate(boolean value) {
      this.dataFields.put("scheduledDeliveryDate", value);
      return this;
    }

    /**
     * Set the paymentTerms value
     *
     * Description: The setup and timing defined to complete a specified payment.
     *
     * @param value
     *          The paymentTerms value.
     * @return The builder for this class.
     */
    public Builder paymentTerms(boolean value) {
      this.dataFields.put("paymentTerms", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(boolean value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the summedLineAmount value
     *
     * Description: The final sum of all line totals made to a specified document or transaction
     * (not including taxes).
     *
     * @param value
     *          The summedLineAmount value.
     * @return The builder for this class.
     */
    public Builder summedLineAmount(boolean value) {
      this.dataFields.put("summedLineAmount", value);
      return this;
    }

    /**
     * Set the grandTotalAmount value
     *
     * Description: The final monetary amount (including taxes) charge listed in a document.
     *
     * @param value
     *          The grandTotalAmount value.
     * @return The builder for this class.
     */
    public Builder grandTotalAmount(boolean value) {
      this.dataFields.put("grandTotalAmount", value);
      return this;
    }

    /**
     * Set the warehouse value
     *
     * Description: The location where products arrive to or are sent from.
     *
     * @param value
     *          The warehouse value.
     * @return The builder for this class.
     */
    public Builder warehouse(boolean value) {
      this.dataFields.put("warehouse", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(boolean value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the orderReference value
     *
     * Description: A reference or document order number as listed in business partner application.
     *
     * @param value
     *          The orderReference value.
     * @return The builder for this class.
     */
    public Builder orderReference(boolean value) {
      this.dataFields.put("orderReference", value);
      return this;
    }

    /**
     * Set the partnerAddress value
     *
     * Description: Identifies the (ship from) address for this Business Partner
     *
     * @param value
     *          The partnerAddress value.
     * @return The builder for this class.
     */
    public Builder partnerAddress(boolean value) {
      this.dataFields.put("partnerAddress", value);
      return this;
    }

    /**
     * Set the formOfPayment value
     *
     * Description: The method used for payment of this transaction.
     *
     * @param value
     *          The formOfPayment value.
     * @return The builder for this class.
     */
    public Builder formOfPayment(boolean value) {
      this.dataFields.put("formOfPayment", value);
      return this;
    }

    /**
     * Set the deliveryNotes value
     *
     * Description: Delivery notes
     *
     * @param value
     *          The deliveryNotes value.
     * @return The builder for this class.
     */
    public Builder deliveryNotes(boolean value) {
      this.dataFields.put("deliveryNotes", value);
      return this;
    }

    /**
     * Set the fINPaymentPriorityID value
     *
     * Description: Sets the priority of the payment plans generated when processing the invoice or
     * order.
     *
     * @param value
     *          The fINPaymentPriorityID value.
     * @return The builder for this class.
     */
    public Builder fINPaymentPriorityID(boolean value) {
      this.dataFields.put("fINPaymentPriorityID", value);
      return this;
    }

    /**
     * Set the paymentMethod value
     *
     * Description: It is the method by which payment is expected to be made or received.
     *
     * @param value
     *          The paymentMethod value.
     * @return The builder for this class.
     */
    public Builder paymentMethod(boolean value) {
      this.dataFields.put("paymentMethod", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PurchaseOrderHeaderDisplayLogic build() {
      return new PurchaseOrderHeaderDisplayLogic(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PurchaseOrderHeaderDisplayLogic(Builder builder) {
    dataFields = builder.dataFields;
  }

}
