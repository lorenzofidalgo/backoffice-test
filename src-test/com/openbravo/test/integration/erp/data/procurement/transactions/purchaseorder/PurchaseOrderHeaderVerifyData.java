/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2010-12-28 18:06:50
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder;

/**
 *
 * @author plujan
 *
 */
public class PurchaseOrderHeaderVerifyData {

  /* Data fields */
  /** Summed Line Amount */
  @SuppressWarnings("unused")
  private String summedLineAmount;
  /** Grand Total Amount */
  @SuppressWarnings("unused")
  private String grandTotalAmount;
  /** Document Status */
  @SuppressWarnings("unused")
  private String documentStatus;

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /* Builder fields */
    /** Summed Line Amount */
    private String summedLineAmount = null;
    /** Grand Total Amount */
    private String grandTotalAmount = null;
    /** Document Status */
    private String documentStatus = null;

    /**
     * Set the Summed Line Amount value.
     *
     * @param value
     *          The Summed Line Amount value.
     * @return The builder for this class.
     */
    public Builder summedLineAmount(String value) {
      this.summedLineAmount = value;
      return this;
    }

    /**
     * Set the Grand Total Amount value.
     *
     * @param value
     *          The Grand Total Amount value.
     * @return The builder for this class.
     */
    public Builder grandTotalAmount(String value) {
      this.grandTotalAmount = value;
      return this;
    }

    /**
     * Set the Document Status value.
     *
     * @param value
     *          The Document Status value.
     * @return The builder for this class.
     */
    public Builder documentStatus(String value) {
      this.documentStatus = value;
      return this;
    }

    /**
     * Set the fields for this object that are required on the ERP.
     *
     * @param summedLineAmountField
     *          The Summed Line Amount value.
     * @param grandTotalAmountField
     *          The Grand Total Amount value.
     * @param documentStatusField
     *          The Document Status value.
     * @return The builder for this class
     */
    public Builder requiredFields(String summedLineAmountField, String grandTotalAmountField,
        String documentStatusField) {
      this.summedLineAmount = summedLineAmountField;
      this.grandTotalAmount = grandTotalAmountField;
      this.documentStatus = documentStatusField;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PurchaseOrderHeaderVerifyData build() {
      return new PurchaseOrderHeaderVerifyData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PurchaseOrderHeaderVerifyData(Builder builder) {
    summedLineAmount = builder.summedLineAmount;
    grandTotalAmount = builder.grandTotalAmount;
    documentStatus = builder.documentStatus;

  }

}
