/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * Contributor(s):
 *************************************************************************
 */
package com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 * Payment Out Detail Data
 *
 * @author nonofce
 *
 */
public class PaymentOutDetailsData extends DataObject {

  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the Paid Amount value
     *
     * Description: Paid Amount value
     *
     * @param value
     *          The paid amount value.
     * @return The builder for this class.
     */
    public Builder paidAmount(String value) {
      this.dataFields.put("paidAmount", value);
      return this;
    }

    /**
     * Set the Payment
     *
     * Description: Payment Out
     *
     * @param value
     *          The payment out value.
     * @return The builder for this class.
     */
    public Builder payment(String value) {
      this.dataFields.put("payment", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PaymentOutDetailsData build() {
      return new PaymentOutDetailsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PaymentOutDetailsData(Builder builder) {
    dataFields = builder.dataFields;
  }
}
