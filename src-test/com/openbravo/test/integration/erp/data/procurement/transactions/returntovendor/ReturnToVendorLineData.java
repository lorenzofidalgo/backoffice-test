/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <f.carballo@nectus.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for Return to Vendor Line Data
 *
 * @author Nono Carballo
 *
 */
public class ReturnToVendorLineData extends DataObject {

  /**
   * Class builder
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the lineNo value
     *
     * Description: Line number
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: The product for the line.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(String value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the attributeSetValue value
     *
     * Description: The value of the atribute set.
     *
     * @param value
     *          The attributeSetValue value.
     * @return The builder for this class.
     */
    public Builder attributeSetValue(String value) {
      this.dataFields.put("attributeSetValue", value);
      return this;
    }

    /**
     * Set the unitPrice value
     *
     * Description: The unit price.
     *
     * @param value
     *          The unitPrice value.
     * @return The builder for this class.
     */
    public Builder unitPrice(String value) {
      this.dataFields.put("unitPrice", value);
      return this;
    }

    /**
     * Set the uOM value
     *
     * Description: The unit of Measure.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * @return this ReturnToVendorLineData
     */
    public Builder operativeUOM(String value) {
      this.dataFields.put("operativeUOM", value);
      return this;
    }

    /**
     * @return ReturnToVendorLineData
     */
    public Builder operativeQuantity(String value) {
      this.dataFields.put("operativeQuantity", value);
      return this;
    }

    /**
     * Set the lineNetAmount value
     *
     * Description: The Line net amount.
     *
     * @param value
     *          The lineNetAmount value.
     * @return The builder for this class.
     */
    public Builder lineNetAmount(String value) {
      this.dataFields.put("lineNetAmount", value);
      return this;
    }

    /**
     * Set the goodsShipmentLine value
     *
     * Description: The goods shipment reference.
     *
     * @param value
     *          The goodsShipmentLine value.
     * @return The builder for this class.
     */
    public Builder goodsShipmentLine(String value) {
      this.dataFields.put("goodsShipmentLine", value);
      return this;
    }

    /**
     * Set the tax value
     *
     * Description: The tax value.
     *
     * @param value
     *          The tax value.
     * @return The builder for this class.
     */
    public Builder tax(String value) {
      this.dataFields.put("tax", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: The description.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the returnReason value
     *
     * Description: The return reason
     *
     * @param value
     *          The return reason
     * @return The builder for this class.
     */
    public Builder returnReason(String value) {
      this.dataFields.put("returnReason", value);
      return this;
    }

    /**
     * Set the orderedQuantity value
     *
     * Description: The quantity to return
     *
     * @param value
     *          The quantity to return
     * @return The builder for this class.
     */
    public Builder orderedQuantity(String value) {
      this.dataFields.put("orderedQuantity", value);
      return this;
    }

    /**
     * Set the shipmentNumber value
     *
     * Description: Shipment Number
     *
     * @param value
     *          The documentNo
     * @return The builder for this class.
     */
    public Builder shipmentNumber(String value) {
      this.dataFields.put("shipmentNumber", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ReturnToVendorLineData build() {
      return new ReturnToVendorLineData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ReturnToVendorLineData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
