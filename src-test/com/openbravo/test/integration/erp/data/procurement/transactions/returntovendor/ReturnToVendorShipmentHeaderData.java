/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <f.carballo@nectus.com>
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProjectSelectorData;

/**
 *
 * Class for Return to Vendor Header Data
 *
 * @author Nono Carballo
 *
 */
public class ReturnToVendorShipmentHeaderData extends DataObject {

  /**
   * Class builder
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the transactionDocument value
     *
     * Description: The specific document type which should be used for the document.
     *
     * @param value
     *          The transactionDocument value.
     * @return The builder for this class.
     */
    public Builder documentType(String value) {
      this.dataFields.put("documentType", value);
      return this;
    }

    /**
     * Set the documentNo value
     *
     * Description: An often automatically generated identifier for all documents.
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(String value) {
      this.dataFields.put("documentNo", value);
      return this;
    }

    /**
     * Set the orderReference value
     *
     * Description: An often automatically generated identifier for all documents.
     *
     * @param value
     *          The orderReference value.
     * @return The builder for this class.
     */
    public Builder orderReference(String value) {
      this.dataFields.put("orderReference", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the partnerAddress value
     *
     * Description: The location of the selected business partner.
     *
     * @param value
     *          The partnerAddress value.
     * @return The builder for this class.
     */
    public Builder partnerAddress(String value) {
      this.dataFields.put("partnerAddress", value);
      return this;
    }

    /**
     * Set the movementDate value
     *
     * Description: The date the movement.
     *
     * @param value
     *          The movementDate value.
     * @return The builder for this class.
     */
    public Builder movementDate_dateTextField(String value) {
      this.dataFields.put("movementDate_dateTextField", value);
      return this;
    }

    /**
     * Set the accountingDate value
     *
     * Description: The date the accounting.
     *
     * @param value
     *          The accountingDate value.
     * @return The builder for this class.
     */
    public Builder accountingDate_dateTextField(String value) {
      this.dataFields.put("accountingDate_dateTextField", value);
      return this;
    }

    /**
     * Set the warehouse value
     *
     * Description: The location where products arrive to or are sent from.
     *
     * @param value
     *          The warehouse value.
     * @return The builder for this class.
     */
    public Builder warehouse(String value) {
      this.dataFields.put("warehouse", value);
      return this;
    }

    /**
     * Set the project value
     *
     * Description: Identifier of a project defined within the Project Service Management module.
     *
     * @param value
     *          The project value.
     * @return The builder for this class.
     */
    public Builder project(ProjectSelectorData value) {
      this.dataFields.put("project", value);
      return this;
    }

    /**
     * Set the documentStatus value
     *
     * Description: The Document Status indicates the status of a document at this time.
     *
     * @param value
     *          The documentStatus value.
     * @return The builder for this class.
     */
    public Builder documentStatus(String value) {
      this.dataFields.put("documentStatus", value);
      return this;
    }

    /**
     * Set the grandTotalAmount value
     *
     * Description: The final monetary amount (including taxes) charge listed in a document.
     *
     * @param value
     *          The grandTotalAmount value.
     * @return The builder for this class.
     */
    public Builder grandTotalAmount(String value) {
      this.dataFields.put("grandTotalAmount", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ReturnToVendorShipmentHeaderData build() {
      return new ReturnToVendorShipmentHeaderData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ReturnToVendorShipmentHeaderData(Builder builder) {
    dataFields = builder.dataFields;
  }
}
