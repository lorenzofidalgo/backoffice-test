/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.production.transactions.maintenanceplan;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for MaintenanceData
 *
 * @author plujan
 *
 */
public class MaintenanceData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the internalConsumption value
     *
     * Description: An indication that an item will not be sold but rather used internally.
     *
     * @param value
     *          The internalConsumption value.
     * @return The builder for this class.
     */
    public Builder internalConsumption(String value) {
      this.dataFields.put("internalConsumption", value);
      return this;
    }

    /**
     * Set the maintenanceTask value
     *
     * Description: A description to help explain a specified maintenance task.
     *
     * @param value
     *          The maintenanceTask value.
     * @return The builder for this class.
     */
    public Builder maintenanceTask(String value) {
      this.dataFields.put("maintenanceTask", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the timeUsed value
     *
     * Description: Time Used
     *
     * @param value
     *          The timeUsed value.
     * @return The builder for this class.
     */
    public Builder timeUsed(String value) {
      this.dataFields.put("timeUsed", value);
      return this;
    }

    /**
     * Set the comments value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The comments value.
     * @return The builder for this class.
     */
    public Builder comments(String value) {
      this.dataFields.put("comments", value);
      return this;
    }

    /**
     * Set the result value
     *
     * Description: x not implemented
     *
     * @param value
     *          The result value.
     * @return The builder for this class.
     */
    public Builder result(Boolean value) {
      this.dataFields.put("result", value);
      return this;
    }

    /**
     * Set the shift value
     *
     * Description: A partition of the workday into intervals.
     *
     * @param value
     *          The shift value.
     * @return The builder for this class.
     */
    public Builder shift(String value) {
      this.dataFields.put("shift", value);
      return this;
    }

    /**
     * Set the confirmation value
     *
     * Description: An indication that an action is accepted and will be completed as planned.
     *
     * @param value
     *          The confirmation value.
     * @return The builder for this class.
     */
    public Builder confirmation(Boolean value) {
      this.dataFields.put("confirmation", value);
      return this;
    }

    /**
     * Set the plannedDate value
     *
     * Description: Planned Date
     *
     * @param value
     *          The plannedDate value.
     * @return The builder for this class.
     */
    public Builder plannedDate(String value) {
      this.dataFields.put("plannedDate", value);
      return this;
    }

    /**
     * Set the maintenanceType value
     *
     * Description: A set of maintenances which can be performed on a machine.
     *
     * @param value
     *          The maintenanceType value.
     * @return The builder for this class.
     */
    public Builder maintenanceType(String value) {
      this.dataFields.put("maintenanceType", value);
      return this;
    }

    /**
     * Set the machineCategory value
     *
     * Description: A distinct machine characteristic used for processes.
     *
     * @param value
     *          The machineCategory value.
     * @return The builder for this class.
     */
    public Builder machineCategory(String value) {
      this.dataFields.put("machineCategory", value);
      return this;
    }

    /**
     * Set the machine value
     *
     * Description: A tool use to aid in or fully complete a task.
     *
     * @param value
     *          The machine value.
     * @return The builder for this class.
     */
    public Builder machine(String value) {
      this.dataFields.put("machine", value);
      return this;
    }

    /**
     * Set the maintenanceOrder value
     *
     * Description: A distinct characteristic of a machine used for processes and sometimes grouped
     * within a category.
     *
     * @param value
     *          The maintenanceOrder value.
     * @return The builder for this class.
     */
    public Builder maintenanceOrder(String value) {
      this.dataFields.put("maintenanceOrder", value);
      return this;
    }

    /**
     * Set the maintenance value
     *
     * Description: The act of ensuring the proper working order for a specified item.
     *
     * @param value
     *          The maintenance value.
     * @return The builder for this class.
     */
    public Builder maintenance(String value) {
      this.dataFields.put("maintenance", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public MaintenanceData build() {
      return new MaintenanceData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private MaintenanceData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
