/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.production.transactions.workrequirement;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for OperationData
 *
 * @author plujan
 *
 */
public class OperationData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the outsourced value
     *
     * Description: A decision to have a task or phase completed by an external business partner.
     *
     * @param value
     *          The outsourced value.
     * @return The builder for this class.
     */
    public Builder outsourced(Boolean value) {
      this.dataFields.put("outsourced", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the mASequence value
     *
     * Description: The order of records in a specified document.
     *
     * @param value
     *          The mASequence value.
     * @return The builder for this class.
     */
    public Builder mASequence(String value) {
      this.dataFields.put("mASequence", value);
      return this;
    }

    /**
     * Set the globalUse value
     *
     * Description: Global Use
     *
     * @param value
     *          The globalUse value.
     * @return The builder for this class.
     */
    public Builder globalUse(Boolean value) {
      this.dataFields.put("globalUse", value);
      return this;
    }

    /**
     * Set the emptyCellsAreZero value
     *
     * Description: Empty Cells are Zero
     *
     * @param value
     *          The emptyCellsAreZero value.
     * @return The builder for this class.
     */
    public Builder emptyCellsAreZero(Boolean value) {
      this.dataFields.put("emptyCellsAreZero", value);
      return this;
    }

    /**
     * Set the sequenceNumber value
     *
     * Description: The order of records in a specified document.
     *
     * @param value
     *          The sequenceNumber value.
     * @return The builder for this class.
     */
    public Builder sequenceNumber(String value) {
      this.dataFields.put("sequenceNumber", value);
      return this;
    }

    /**
     * Set the quantity value
     *
     * Description: Quantity
     *
     * @param value
     *          The quantity value.
     * @return The builder for this class.
     */
    public Builder quantity(String value) {
      this.dataFields.put("quantity", value);
      return this;
    }

    /**
     * Set the costCenterUseTime value
     *
     * Description: The amount of time a process takes to complete.
     *
     * @param value
     *          The costCenterUseTime value.
     * @return The builder for this class.
     */
    public Builder costCenterUseTime(String value) {
      this.dataFields.put("costCenterUseTime", value);
      return this;
    }

    /**
     * Set the workRequirement value
     *
     * Description: An order authorizing the production of a specific product and product quantity.
     *
     * @param value
     *          The workRequirement value.
     * @return The builder for this class.
     */
    public Builder workRequirement(String value) {
      this.dataFields.put("workRequirement", value);
      return this;
    }

    /**
     * Set the activity value
     *
     * Description: A series of actions carried out in sequential order.
     *
     * @param value
     *          The activity value.
     * @return The builder for this class.
     */
    public Builder activity(String value) {
      this.dataFields.put("activity", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the completedQuantity value
     *
     * Description: Completed Quantity
     *
     * @param value
     *          The completedQuantity value.
     * @return The builder for this class.
     */
    public Builder completedQuantity(String value) {
      this.dataFields.put("completedQuantity", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public OperationData build() {
      return new OperationData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private OperationData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
