/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.production.transactions.workrequirement;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for WorkRequirementHeaderData
 *
 * @author plujan
 *
 */
public class WorkRequirementHeaderData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the conversionRate value
     *
     * Description: Defines the conversion between Quantity and Process Quantity
     *
     * @param value
     *          The conversionRate value.
     * @return The builder for this class.
     */
    public Builder conversionRate(String value) {
      this.dataFields.put("conversionRate", value);
      return this;
    }

    /**
     * Set the processUnit value
     *
     * Description: The name of the main final product obtained by executing a process plan.
     *
     * @param value
     *          The processUnit value.
     * @return The builder for this class.
     */
    public Builder processUnit(String value) {
      this.dataFields.put("processUnit", value);
      return this;
    }

    /**
     * Set the processQuantity value
     *
     * Description: The number of Process Units required to be produced.
     *
     * @param value
     *          The processQuantity value.
     * @return The builder for this class.
     */
    public Builder processQuantity(String value) {
      this.dataFields.put("processQuantity", value);
      return this;
    }

    /**
     * Set the includePhasesWhenInserting value
     *
     * Description: Include Phases when inserting
     *
     * @param value
     *          The includePhasesWhenInserting value.
     * @return The builder for this class.
     */
    public Builder includePhasesWhenInserting(Boolean value) {
      this.dataFields.put("includePhasesWhenInserting", value);
      return this;
    }

    /**
     * Set the documentNo value
     *
     * Description: An often automatically generated identifier for all documents.
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(String value) {
      this.dataFields.put("documentNo", value);
      return this;
    }

    /**
     * Set the processed value
     *
     * Description: The document has been processed
     *
     * @param value
     *          The processed value.
     * @return The builder for this class.
     */
    public Builder processed(Boolean value) {
      this.dataFields.put("processed", value);
      return this;
    }

    /**
     * Set the startingDate value
     *
     * Description: A parameter stating when a specified request will begin.
     *
     * @param value
     *          The startingDate value.
     * @return The builder for this class.
     */
    public Builder startingDate(String value) {
      this.dataFields.put("startingDate", value);
      return this;
    }

    /**
     * Set the quantity value
     *
     * Description: Quantity
     *
     * @param value
     *          The quantity value.
     * @return The builder for this class.
     */
    public Builder quantity(String value) {
      this.dataFields.put("quantity", value);
      return this;
    }

    /**
     * Set the processPlan value
     *
     * Description: A guide stating how a certain item must move through the transformation process.
     *
     * @param value
     *          The processPlan value.
     * @return The builder for this class.
     */
    public Builder processPlan(String value) {
      this.dataFields.put("processPlan", value);
      return this;
    }

    /**
     * Set the wRCreationDate value
     *
     * Description: WR Creation Date
     *
     * @param value
     *          The wRCreationDate value.
     * @return The builder for this class.
     */
    public Builder wRCreationDate(String value) {
      this.dataFields.put("wRCreationDate", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the endingDate value
     *
     * Description: A parameter stating when a specified request will end.
     *
     * @param value
     *          The endingDate value.
     * @return The builder for this class.
     */
    public Builder endingDate(String value) {
      this.dataFields.put("endingDate", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public WorkRequirementHeaderData build() {
      return new WorkRequirementHeaderData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private WorkRequirementHeaderData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
