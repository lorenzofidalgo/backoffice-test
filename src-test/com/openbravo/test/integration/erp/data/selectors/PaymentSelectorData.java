/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-01-25 18:28:18
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *   Nono Carballo <nonofce@gmail.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.selectors;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;

/**
 *
 * Class for payment selector data.
 *
 * @author elopio.
 *
 */
public class PaymentSelectorData extends SelectorDataObject {

  /**
   * Class builder.
   *
   * @author elopio
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the Status value.
     *
     * @param value
     *          The Status value.
     * @return The builder for this class.
     */
    public Builder status(String value) {
      this.dataFields.put("status", value);
      return this;
    }

    /**
     * Set the Financial Account value.
     *
     * @param value
     *          The Financial Account value.
     * @return The builder for this class.
     */
    public Builder account(String value) {
      this.dataFields.put("account", value);
      return this;
    }

    /**
     * Set the Document No value.
     *
     * @param value
     *          The Document No value.
     * @return The builder for this class.
     */
    public Builder documentNo(String value) {
      this.dataFields.put("documentNo", value);
      return this;
    }

    /**
     * Set the Document Type value.
     *
     * @param value
     *          The Document Type value.
     * @return The builder for this class.
     */
    public Builder documentType(String value) {
      this.dataFields.put("documentType", value);
      return this;
    }

    /**
     * Set the Currency value.
     *
     * @param value
     *          The Currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the Trx. Amount value.
     *
     * @param value
     *          The Trx. Amount value.
     * @return The builder for this class.
     */
    public Builder financialTransactionAmount(String value) {
      this.dataFields.put("financialTransactionAmount", value);
      return this;
    }

    /**
     * Set the Business Partner value.
     *
     * @param value
     *          The Business Partner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(String value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the Amount value.
     *
     * @param value
     *          The Amount value.
     * @return The builder for this class.
     */
    public Builder amount(String value) {
      this.dataFields.put("amount", value);
      return this;
    }

    /**
     * Set the Payment Date value.
     *
     * @param value
     *          The Payment Date value.
     * @return The builder for this class.
     */
    public Builder paymentDate(String value) {
      this.dataFields.put("paymentDate", value);
      return this;
    }

    /**
     * Adds an AddPaymentPopUpData object.
     *
     * @param value
     *          The addPayment object.
     * @return The builder for this class.
     */
    public Builder addPayment(AddPaymentPopUpData value) {
      this.dataFields.put("addPayment", value);
      return this;
    }

    /**
     * Adds an AddPaymentPopUpData object to edit values.
     *
     * @param value
     *          The addPayment object.
     * @return The builder for this class.
     */
    public Builder addEditPayment(AddPaymentPopUpData value) {
      this.dataFields.put("addEditPayment", value);
      return this;
    }

    /**
     * The initial value of Payment
     *
     * @param value
     *          The value of Payment
     * @return The builder for this class.
     */
    public Builder initialPayment(AddPaymentPopUpData value) {
      this.dataFields.put("initialPayment", value);
      return this;
    }

    /**
     * The final value of Payment
     *
     * @param value
     *          The value of Payment
     * @return The builder for this class.
     */
    public Builder finalPayment(AddPaymentPopUpData value) {
      this.dataFields.put("finalPayment", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PaymentSelectorData build() {
      return new PaymentSelectorData(this);
    }
  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PaymentSelectorData(Builder builder) {
    dataFields = builder.dataFields;
  }

  /**
   * Get the documentNo of the Payment selector data object.
   *
   * @return the search key of the business partner selector data object.
   */
  @Override
  public String getSearchKey() {
    return (String) dataFields.get("documentNo");
  }

  /**
   * Get the display value of the selector data object.
   *
   * @return the value displayed by the selector data object.
   */
  @Override
  public String getDisplayValue() {
    return (String) dataFields.get("documentNo");
  }

}
