/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data.selectors;

import java.util.LinkedHashMap;

/**
 * Class for location data.
 *
 * @author elopio
 *
 */
public class ProductCompleteSelectorData extends SelectorDataObject {

  /**
   * Class builder.
   *
   * @author elopio
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the search key value.
     *
     * @param value
     *          The search key value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("product.searchKey", value);
      return this;
    }

    /**
     * Set the name value.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("product.name", value);
      return this;
    }

    /**
     * Set the storage bin.
     *
     * @param value
     *          The storage bin value.
     * @return The builder for this class.
     */
    public Builder storageBin(String value) {
      this.dataFields.put("storageBin", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ProductCompleteSelectorData build() {
      return new ProductCompleteSelectorData(this);
    }
  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ProductCompleteSelectorData(Builder builder) {
    dataFields = builder.dataFields;
  }

  /**
   * Get the search key of the business partner selector data object.
   *
   * @return the search key of the business partner selector data object.
   */
  @Override
  public String getSearchKey() {
    return (String) dataFields.get("product.searchKey");
  }

  /**
   * Get the display value of the selector data object.
   *
   * @return the value displayed by the selector data object.
   */
  @Override
  public String getDisplayValue() {
    return (String) dataFields.get("product.name");
  }

  /**
   * Get the display value of the selector data object.
   *
   * @return the value displayed by the selector data object.
   */
  public String getStorageBin() {
    return (String) dataFields.get("storageBin");
  }
}
