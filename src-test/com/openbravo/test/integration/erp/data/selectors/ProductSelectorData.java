/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-01-25 18:28:18
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.comt>
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.selectors;

import java.util.LinkedHashMap;

/**
 *
 * Class for product selector data.
 *
 * @author elopio.
 *
 */
public class ProductSelectorData extends SelectorDataObject {

  /**
   * Class builder.
   *
   * @author elopio
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the search key value.
     *
     * @param value
     *          The search key value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("product.searchKey", value);
      return this;
    }

    /**
     * Set the quantity on hand value
     *
     * Description: The number of a certain item.
     *
     * @param value
     *          The quantity value.
     * @return The builder for this class.
     */
    public Builder quantityOnHand(String value) {
      this.dataFields.put("quantityOnHand", value);
      return this;
    }

    /**
     * Set the uOM value
     *
     * Description: The number of a certain item.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the name value.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("product.name", value);
      return this;
    }

    /**
     * Set the available value.
     *
     * @param value
     *          The available value.
     * @return The builder for this class.
     */
    public Builder available(String value) {
      this.dataFields.put("available", value);
      return this;
    }

    /**
     * Set the warehouse value.
     *
     * @param value
     *          The warehouse value.
     * @return The builder for this class.
     */
    public Builder warehouse(String value) {
      this.dataFields.put("warehouse", value);
      return this;
    }

    /**
     * Set the netListPrice value.
     *
     * @param value
     *          The netListPrice value.
     * @return The builder for this class.
     */
    public Builder netListPrice(String value) {
      this.dataFields.put("netListPrice", value);
      return this;
    }

    /**
     * Set the standardPrice value.
     *
     * @param value
     *          The standardPrice value.
     * @return The builder for this class.
     */
    public Builder standardPrice(String value) {
      this.dataFields.put("standardPrice", value);
      return this;
    }

    /**
     * Set the priceListVersion value.
     *
     * @param value
     *          The priceListVersion value.
     * @return The builder for this class.
     */
    public Builder priceListVersion(String value) {
      this.dataFields.put("productPrice.priceListVersion", value);
      return this;
    }

    /**
     * Set the qtyOnHand value.
     *
     * @param value
     *          The qtyOnHand value.
     * @return The builder for this class.
     */
    public Builder qtyOnHand(String value) {
      this.dataFields.put("qtyOnHand", value);
      return this;
    }

    /**
     * Set the qtyOrdered value.
     *
     * @param value
     *          The qtyOrdered value.
     * @return The builder for this class.
     */
    public Builder qtyOrdered(String value) {
      this.dataFields.put("qtyOrdered", value);
      return this;
    }

    /**
     * Set the priceLimit value.
     *
     * @param value
     *          The priceLimit value.
     * @return The builder for this class.
     */
    public Builder priceLimit(String value) {
      this.dataFields.put("priceLimit", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ProductSelectorData build() {
      return new ProductSelectorData(this);
    }
  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ProductSelectorData(Builder builder) {
    dataFields = builder.dataFields;
  }

  /**
   * Get the search key of the product selector data object.
   *
   * @return the search key of the product selector data object.
   */
  @Override
  public String getSearchKey() {
    return (String) dataFields.get("product.searchKey");
  }

  /**
   * Get the display value of the selector data object.
   *
   * @return the value displayed by the selector data object.
   */
  @Override
  public String getDisplayValue() {
    return (String) dataFields.get("product.name");
  }
}
