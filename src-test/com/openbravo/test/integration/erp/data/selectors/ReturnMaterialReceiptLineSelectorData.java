/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Andy Armaignac <collazoandy4@gmail.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.data.selectors;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for Return Material Receipt Line Selector
 *
 * @author collazoandy4
 *
 */
public class ReturnMaterialReceiptLineSelectorData extends DataObject {

  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the rMOrderNo value.
     *
     * @param value
     *          The RM Order No number.
     * @return The builder for this class.
     */
    public Builder rMOrderNo(String value) {
      this.dataFields.put("rMOrderNo", value);
      return this;
    }

    /**
     * Set the product value.
     *
     * @param value
     *          The Product.
     * @return The builder for this class.
     */
    public Builder product(String value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the returned value.
     *
     * @param value
     *          The Quantity returned.
     * @return The builder for this class.
     */
    public Builder receiving(String value) {
      this.dataFields.put("receiving", value);
      return this;
    }

    /**
     * Set the storage bin value.
     *
     * @param value
     *          The Storage bin alias returned.
     * @return The builder for this class.
     */
    public Builder storageBin(String value) {
      this.dataFields.put("storageBin", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ReturnMaterialReceiptLineSelectorData build() {
      return new ReturnMaterialReceiptLineSelectorData(this);
    }
  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ReturnMaterialReceiptLineSelectorData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
