/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *  David Miguélez <david.miguelez@openbravo.com>
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.sharedtabs;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ExchangeRatesData
 *
 * @author David Miguélez
 *
 */

public class ExchangeRatesData extends DataObject {

  /**
   * Class builder
   *
   * @author David Miguélez
   *
   */

  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the Organization value
     *
     * Description: Organizational entity within client.
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the Active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the Currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the to Currency value
     *
     * Description: Target currency.
     *
     * @param value
     *          The toCurrency value.
     * @return The builder for this class.
     */
    public Builder toCurrency(String value) {
      this.dataFields.put("toCurrency", value);
      return this;
    }

    /**
     * Set the Invoice value
     *
     * Description: A document listing products, quantities and prices, payment terms, etc.
     *
     * @param value
     *          The invoice value.
     * @return The builder for this class.
     */
    public Builder invoice(String value) {
      this.dataFields.put("invoice", value);
      return this;
    }

    /**
     * Set the Payment value
     *
     * Description: Payment event.
     *
     * @param value
     *          The payment value.
     * @return The builder for this class.
     */
    public Builder payment(String value) {
      this.dataFields.put("payment", value);
      return this;
    }

    /**
     * Set the Financial Account Transaction value
     *
     * @param value
     *          The financialAccountTransaction value.
     * @return The builder for this class.
     */
    public Builder financialAccountTransaction(String value) {
      this.dataFields.put("financialAccountTransaction", value);
      return this;
    }

    /**
     * Set the Rate value
     *
     * Description: The percentage to be multiplied by the source to arrive at the tax or exchange.
     * amount.
     *
     * @param value
     *          The rate value.
     * @return The builder for this class.
     */
    public Builder rate(String value) {
      this.dataFields.put("rate", value);
      return this;
    }

    /**
     * Set the Foreign Amount value
     *
     * Description: Amount of transaction in foreign currency.
     *
     * @param value
     *          The foreignAmount value.
     * @return The builder for this class.
     */
    public Builder foreignAmount(String value) {
      this.dataFields.put("foreignAmount", value);
      return this;
    }

    /**
     * Set the Client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ExchangeRatesData build() {
      return new ExchangeRatesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ExchangeRatesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
