/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014 Openbravo S.L.U.
 * All Rights Reserved.

 *************************************************************************
 */
package com.openbravo.test.integration.erp.data.sharedtabs;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

public class UsedCreditSourceData extends DataObject {

  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    public Builder paymentcredit(Boolean value) {
      this.dataFields.put("FIN_Payment_Credit", value);
      return this;
    }

    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    public Builder active(String value) {
      this.dataFields.put("active", value);
      return this;
    }

    public Builder payment(String value) {
      this.dataFields.put("payment", value);
      return this;
    }

    public Builder creditPaymentUsed(String value) {
      this.dataFields.put("creditPaymentUsed", value);
      return this;
    }

    public Builder amount(String value) {
      this.dataFields.put("amount", value);
      return this;
    }

    public Builder toCurrency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    public UsedCreditSourceData build() {
      return new UsedCreditSourceData(this);
    }

  }

  public UsedCreditSourceData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
