/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-01-25 18:28:20
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.warehouse.setup.warehouseandstoragebins;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for StorageBinData
 *
 * @author plujan
 *
 */
public class StorageBinData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the defaultValue value
     *
     * Description: A value that is shown whenever a record is created.
     *
     * @param value
     *          The defaultValue value.
     * @return The builder for this class.
     */
    public Builder defaultValue(Boolean value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Set the relativePriority value
     *
     * Description: Where inventory should be picked from first
     *
     * @param value
     *          The relativePriority value.
     * @return The builder for this class.
     */
    public Builder relativePriority(String value) {
      this.dataFields.put("relativePriority", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the levelZ value
     *
     * Description: Z dimension e.g. Level
     *
     * @param value
     *          The levelZ value.
     * @return The builder for this class.
     */
    public Builder levelZ(String value) {
      this.dataFields.put("levelZ", value);
      return this;
    }

    /**
     * Set the stackY value
     *
     * Description: Y dimension e.g. Bin
     *
     * @param value
     *          The stackY value.
     * @return The builder for this class.
     */
    public Builder stackY(String value) {
      this.dataFields.put("stackY", value);
      return this;
    }

    /**
     * Set the rowX value
     *
     * Description: X dimension e.g. Aisle
     *
     * @param value
     *          The rowX value.
     * @return The builder for this class.
     */
    public Builder rowX(String value) {
      this.dataFields.put("rowX", value);
      return this;
    }

    /**
     * Set the warehouse value
     *
     * Description: The location where products arrive to or are sent from.
     *
     * @param value
     *          The warehouse value.
     * @return The builder for this class.
     */
    public Builder warehouse(String value) {
      this.dataFields.put("warehouse", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the defaultStorage value
     *
     * Description:A flag indicating whether this record is the default storagebin
     *
     * @param value
     *          The defaultStorage value.
     * @return The builder for this class.
     */
    public Builder defaultStorage(Boolean value) {
      this.dataFields.put("default", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public StorageBinData build() {
      return new StorageBinData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private StorageBinData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
