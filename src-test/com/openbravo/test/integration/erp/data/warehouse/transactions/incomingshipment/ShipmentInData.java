/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:15
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.warehouse.transactions.incomingshipment;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProjectSelectorData;

/**
 *
 * Class for ShipmentInData
 *
 * @author plujan
 *
 */
public class ShipmentInData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the freightCategory value
     *
     * Description: A classification used to help calculate shipping company freight amounts.
     *
     * @param value
     *          The freightCategory value.
     * @return The builder for this class.
     */
    public Builder freightCategory(String value) {
      this.dataFields.put("freightCategory", value);
      return this;
    }

    /**
     * Set the deliveryLocation value
     *
     * Description: The specific place or address an order will be shipped to or carried out from.
     *
     * @param value
     *          The deliveryLocation value.
     * @return The builder for this class.
     */
    public Builder deliveryLocation(String value) {
      this.dataFields.put("deliveryLocation", value);
      return this;
    }

    /**
     * Set the 1stDimension value
     *
     * Description: A display of optional elements that are previously defined for this account
     * combination.
     *
     * @param value
     *          The 1stDimension value.
     * @return The builder for this class.
     */
    public Builder firstDimension(String value) {
      this.dataFields.put("1stDimension", value);
      return this;
    }

    /**
     * Set the salesCampaign value
     *
     * Description: An advertising effort aimed at increasing sales.
     *
     * @param value
     *          The salesCampaign value.
     * @return The builder for this class.
     */
    public Builder salesCampaign(String value) {
      this.dataFields.put("salesCampaign", value);
      return this;
    }

    /**
     * Set the activity value
     *
     * Description: A distinct activity defined and used in activity based management.
     *
     * @param value
     *          The activity value.
     * @return The builder for this class.
     */
    public Builder activity(String value) {
      this.dataFields.put("activity", value);
      return this;
    }

    /**
     * Set the project value
     *
     * Description: Identifier of a project defined within the Project Service Management module.
     *
     * @param value
     *          The project value.
     * @return The builder for this class.
     */
    public Builder project(ProjectSelectorData value) {
      this.dataFields.put("project", value);
      return this;
    }

    /**
     * Set the trxOrganization value
     *
     * Description: The organization which performs or initiates the transaction.
     *
     * @param value
     *          The trxOrganization value.
     * @return The builder for this class.
     */
    public Builder trxOrganization(String value) {
      this.dataFields.put("trxOrganization", value);
      return this;
    }

    /**
     * Set the 2ndDimension value
     *
     * Description: A display of optional elements that are previously defined for this account
     * combination.
     *
     * @param value
     *          The 2ndDimension value.
     * @return The builder for this class.
     */
    public Builder secondDimension(String value) {
      this.dataFields.put("2ndDimension", value);
      return this;
    }

    /**
     * Set the salesRepresentative value
     *
     * Description: Purchase or Company Agent
     *
     * @param value
     *          The salesRepresentative value.
     * @return The builder for this class.
     */
    public Builder salesRepresentative(String value) {
      this.dataFields.put("salesRepresentative", value);
      return this;
    }

    /**
     * Set the partnerAddress value
     *
     * Description: Identifies the (ship from) address for this Business Partner
     *
     * @param value
     *          The partnerAddress value.
     * @return The builder for this class.
     */
    public Builder partnerAddress(String value) {
      this.dataFields.put("partnerAddress", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the priority value
     *
     * Description: A defined level of importance or precedence.
     *
     * @param value
     *          The priority value.
     * @return The builder for this class.
     */
    public Builder priority(String value) {
      this.dataFields.put("priority", value);
      return this;
    }

    /**
     * Set the orderReference value
     *
     * Description: A reference or document order number as listed in business partner application.
     *
     * @param value
     *          The orderReference value.
     * @return The builder for this class.
     */
    public Builder orderReference(String value) {
      this.dataFields.put("orderReference", value);
      return this;
    }

    /**
     * Set the deliveryTerms value
     *
     * Description: A definition stating when a specific delivery will occur.
     *
     * @param value
     *          The deliveryTerms value.
     * @return The builder for this class.
     */
    public Builder deliveryTerms(String value) {
      this.dataFields.put("deliveryTerms", value);
      return this;
    }

    /**
     * Set the documentType value
     *
     * Description: A value defining what sequence and process setup are used to handle this
     * document.
     *
     * @param value
     *          The documentType value.
     * @return The builder for this class.
     */
    public Builder documentType(String value) {
      this.dataFields.put("documentType", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the warehouse value
     *
     * Description: The location where products arrive to or are sent from.
     *
     * @param value
     *          The warehouse value.
     * @return The builder for this class.
     */
    public Builder warehouse(String value) {
      this.dataFields.put("warehouse", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the userContact value
     *
     * Description: An acquaintance to reach for information related to the business partner.
     *
     * @param value
     *          The userContact value.
     * @return The builder for this class.
     */
    public Builder userContact(String value) {
      this.dataFields.put("userContact", value);
      return this;
    }

    /**
     * Set the movementType value
     *
     * Description: The type of a certain item being moved from one location to another.
     *
     * @param value
     *          The movementType value.
     * @return The builder for this class.
     */
    public Builder movementType(String value) {
      this.dataFields.put("movementType", value);
      return this;
    }

    /**
     * Set the charge value
     *
     * Description: A cost or expense incurred during business activity.
     *
     * @param value
     *          The charge value.
     * @return The builder for this class.
     */
    public Builder charge(String value) {
      this.dataFields.put("charge", value);
      return this;
    }

    /**
     * Set the freightCostRule value
     *
     * Description: The calculation method used when charging freight.
     *
     * @param value
     *          The freightCostRule value.
     * @return The builder for this class.
     */
    public Builder freightCostRule(String value) {
      this.dataFields.put("freightCostRule", value);
      return this;
    }

    /**
     * Set the movementDate value
     *
     * Description: The date that a certain item is moved from one location to another.
     *
     * @param value
     *          The movementDate value.
     * @return The builder for this class.
     */
    public Builder movementDate(String value) {
      this.dataFields.put("movementDate", value);
      return this;
    }

    /**
     * Set the freightAmount value
     *
     * Description: The charge amount for a specified shipment.
     *
     * @param value
     *          The freightAmount value.
     * @return The builder for this class.
     */
    public Builder freightAmount(String value) {
      this.dataFields.put("freightAmount", value);
      return this;
    }

    /**
     * Set the documentNo value
     *
     * Description: An often automatically generated identifier for all documents.
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(String value) {
      this.dataFields.put("documentNo", value);
      return this;
    }

    /**
     * Set the deliveryMethod value
     *
     * Description: The desired means of getting requested goods to a business partner.
     *
     * @param value
     *          The deliveryMethod value.
     * @return The builder for this class.
     */
    public Builder deliveryMethod(String value) {
      this.dataFields.put("deliveryMethod", value);
      return this;
    }

    /**
     * Set the accountingDate value
     *
     * Description: The date this transaction is recorded for in the general ledger.
     *
     * @param value
     *          The accountingDate value.
     * @return The builder for this class.
     */
    public Builder accountingDate(String value) {
      this.dataFields.put("accountingDate", value);
      return this;
    }

    /**
     * Set the chargeAmount value
     *
     * Description: The amount of a cost or expense incurred during business activity.
     *
     * @param value
     *          The chargeAmount value.
     * @return The builder for this class.
     */
    public Builder chargeAmount(String value) {
      this.dataFields.put("chargeAmount", value);
      return this;
    }

    /**
     * Set the documentStatus value
     *
     * Description: The Document Status indicates the status of a document at this time.
     *
     * @param value
     *          The documentStatus value.
     * @return The builder for this class.
     */
    public Builder documentStatus(String value) {
      this.dataFields.put("documentStatus", value);
      return this;
    }

    /**
     * Set the shippingCompany value
     *
     * Description: The name of the company making the shipment.
     *
     * @param value
     *          The shippingCompany value.
     * @return The builder for this class.
     */
    public Builder shippingCompany(String value) {
      this.dataFields.put("shippingCompany", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ShipmentInData build() {
      return new ShipmentInData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ShipmentInData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
