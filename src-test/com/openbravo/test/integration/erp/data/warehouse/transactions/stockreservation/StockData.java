/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-01-25 18:28:20
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.warehouse.transactions.stockreservation;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for StockReservationStockData
 *
 * @author aferraz
 *
 */
public class StockData extends DataObject {

  /**
   * Class builder
   *
   * @author aferraz
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the storageBin value
     *
     * Description: The storageBin.
     *
     * @param value
     *          The storageBin value.
     * @return The builder for this class.
     */
    public Builder storageBin(String value) {
      this.dataFields.put("storageBin", value);
      return this;
    }

    /**
     * Set the attributeSetValue value
     *
     * Description: The attributeSetValue.
     *
     * @param value
     *          The attributeSetValue value.
     * @return The builder for this class.
     */
    public Builder attributeSetValue(String value) {
      this.dataFields.put("attributeSetValue", value);
      return this;
    }

    /**
     * Set the purchaseOrderLine value
     *
     * Description: The purchaseOrderLine.
     *
     * @param value
     *          The purchaseOrderLine value.
     * @return The builder for this class.
     */
    public Builder purchaseOrderLine(String value) {
      this.dataFields.put("salesOrderLine", value);
      return this;
    }

    /**
     * Set the reservedQuantity value
     *
     * Description: The reservedQuantity.
     *
     * @param value
     *          The reservedQuantity value.
     * @return The builder for this class.
     */
    public Builder reservedQuantity(String value) {
      this.dataFields.put("quantity", value);
      return this;
    }

    /**
     * Set the releasedQuantity value
     *
     * Description: The releasedQuantity.
     *
     * @param value
     *          The releasedQuantity value.
     * @return The builder for this class.
     */
    public Builder releasedQuantity(String value) {
      this.dataFields.put("released", value);
      return this;
    }

    /**
     * Set the allocated value
     *
     * Description: The allocated.
     *
     * @param value
     *          The allocated value.
     * @return The builder for this class.
     */
    public Builder allocated(Boolean value) {
      this.dataFields.put("allocated", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public StockData build() {
      return new StockData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private StockData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
