/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <f.carballo@nectus.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.gui.applicationdictionary.processdefinition;

import com.openbravo.test.integration.erp.data.applicationdictionary.processdefinition.ProcessDefinitionHeaderData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

/**
 *
 * Class for Process Definition Header Tab
 *
 * @author Nono Carballo
 *
 */
public class ProcessDefinitionHeaderTab extends GeneratedTab<ProcessDefinitionHeaderData> {

  /** The tab title. */
  static final String TITLE = "Process Definition";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "FF80818132D7FB620132D819C1720046";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  public ProcessDefinitionHeaderTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new ParameterTab(this));
  }

}
