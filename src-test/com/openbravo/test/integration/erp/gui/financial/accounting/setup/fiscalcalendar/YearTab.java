/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.setup.fiscalcalendar;

import org.openqa.selenium.TimeoutException;

import com.openbravo.test.integration.erp.data.financial.accounting.setup.fiscalcalendar.YearData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Year tab.
 *
 * @author elopio
 *
 */
public class YearTab extends GeneratedTab<YearData> {

  /** The tab title. */
  static final String TITLE = "Year";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "129";
  /** The tab level. */
  private static final int LEVEL = 1;

  /**
   * Class constructor.
   *
   * @param parentTab
   *          The parent of the tab.
   */
  public YearTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
    addChildTab(new PeriodTab(this));
  }

  /**
   * Create year periods. A year record must be selected.
   */
  public void createPeriods() throws OpenbravoERPTestException {
    Button buttonCreatePeriods = new Button(TestRegistry
        .getObjectString("org.openbravo.client.application.toolbar.button.processNow.129"));
    buttonCreatePeriods.click();

    OBClassicPopUp createPeriods = new OBClassicPopUp(TestRegistry
        .getObjectString("org.openbravo.classicpopup./FiscalCalendar/Year_Edition.html"));
    createPeriods.selectPopUpWithOKButton();
    createPeriods.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().window("");
    try {
      waitUntilMessageVisible();
    } catch (TimeoutException toe) {
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      SeleniumSingleton.INSTANCE.switchTo().window("");
      waitUntilMessageVisible();
    }

    // XXX Sometimes the newly created periods take a while to appear.
    Sleep.trySleep(10000);
  }
}
