/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 * 	Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.setup.fiscalcalendar.classic;

import com.openbravo.test.integration.erp.data.financial.accounting.setup.fiscalcalendar.YearData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicButtonPopUp;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Year screen
 *
 * @author elopio
 *
 */
public class YearTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Year";
  /** The tab level. */
  private static final int LEVEL = 1;

  /* GUI components. */
  /** Identifier of the Year tab */
  public static final String TAB = "tabname129";

  /** Identifier of the fiscal year text field */
  private static final String TEXT_FIELD_FISCAL_YEAR = "Year";

  /**
   * Class constructor
   *
   */
  public YearTab() {
    super(TITLE, LEVEL, TAB);
    addSubTab(new PeriodTab());
  }

  /**
   * Create a new fiscal year
   *
   * @param yearData
   *          The year data.
   */
  public void create(YearData yearData) {
    clickCreateNewRecord();
    if (yearData.getDataField("fiscalYear") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_FISCAL_YEAR)
          .sendKeys(yearData.getDataField("fiscalYear").toString());
    }
    clickSave();
  }

  /**
   * Create year periods
   */
  public void createPeriods() throws OpenbravoERPTestException {
    switchToFormView();

    SeleniumSingleton.INSTANCE.findElementById(BUTTON_PROCESSING).click();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    final OBClassicButtonPopUp createPeriods = new OBClassicButtonPopUp() {
      /**
       * Select the pop up frame.
       */
      @Override
      protected void selectFrame() {
        SeleniumSingleton.INSTANCE.switchTo().frame("BUTTON");
        SeleniumSingleton.INSTANCE.switchTo().frame("mainframe");
      }
    };
    createPeriods.selectPopUpWithOKButton();
    createPeriods.clickOK();

    waitForFrame();
  }
}
