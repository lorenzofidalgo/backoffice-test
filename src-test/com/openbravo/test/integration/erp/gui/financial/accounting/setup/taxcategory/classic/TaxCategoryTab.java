/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.setup.taxcategory.classic;

import org.openqa.selenium.WebElement;

import com.openbravo.test.integration.erp.data.financial.accounting.setup.taxcategory.TaxCategoryData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Tax Category tab.
 *
 * @author elopio
 *
 */
public class TaxCategoryTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Tax Category";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the tab. */
  public static final String TAB = "tabname176";

  /** Number of the name column */
  public static final int COLUMN_NAME = 2;

  /** Identifier of the name text field */
  private static final String TEXT_FIELD_NAME = "Name";
  /** Identifier of the Default Checkbox */
  public static final String CHECK_BOX_DEFAULT = "IsDefault";

  /**
   * Class constructor
   *
   */
  public TaxCategoryTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Fill the Tax Category values.
   *
   * @param data
   *          The data object with Tax Category values.
   */
  public void fill(TaxCategoryData data) {
    if (data.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(data.getDataField("name").toString());
    }
    if (data.getDataField("default") != null) {
      WebElement checkBoxDefault = SeleniumSingleton.INSTANCE.findElementById(CHECK_BOX_DEFAULT);
      if (!data.getDataField("default").equals(checkBoxDefault.isSelected())) {
        checkBoxDefault.click();
      }
    }
  }

  /**
   * Create a Tax Category.
   *
   * @param data
   *          The data of the Tax Category that will be created.
   */
  public void create(TaxCategoryData data) {
    clickCreateNewRecord();
    fill(data);
    clickSave();
  }

  /**
   * Edit a Tax Category.
   *
   * @param data
   *          The data of the edited Tax Category.
   */
  public void edit(TaxCategoryData data) throws OpenbravoERPTestException {
    switchToFormView();
    fill(data);
    clickSave();
  }
}
