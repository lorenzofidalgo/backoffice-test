/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.setup.taxrate.classic;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.financial.accounting.setup.taxrate.TaxData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Tax tab.
 *
 * @author elopio
 *
 */
public class TaxTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Tax";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the Tax tab. */
  public static final String TAB = "tabname174";

  /** Identifier of the name text field. */
  private static final String TEXT_FIELD_NAME = "Name";
  /** Identifier of the description text field. */
  private static final String TEXT_FIELD_DESCRIPTION = "Description";
  /** Identifier of the valid from date calendar. */
  private static final String CALENDAR_VALID_FROM_DATE = "ValidFrom";
  /** Identifier of the summary level check box. */
  private static final String CHECK_BOX_SUMMARY_LEVEL = "IsSummary";
  /** Identifier of the parent tax rate combo box. */
  private static final String COMBO_BOX_PARENT_TAX_RATE = "reportParent_Tax_ID_S";
  /** Identifier of the tax category combo box. */
  private static final String COMBO_BOX_TAX_CATEGORY = "reportC_TaxCategory_ID_S";
  /** Identifier of the sales/purchase type combo box. */
  private static final String COMBO_BOX_TYPE = "reportSOPOType_S";
  /** Identifier of the rate text field. */
  private static final String TEXT_FIELD_RATE = "Rate";
  /** Identifier of the country combo box. */
  /** Identifier of the business partner tax category combo box. */
  private static final String COMBO_BOX_BUSINESS_PARTNER_TAX_CATEGORY = "reportC_BP_TaxCategory_ID_S";
  private static final String COMBO_BOX_COUNTRY = "reportC_Country_ID_S";
  /** Identifier of the destination country combo box. */
  private static final String COMBO_BOX_DESTINATION_COUNTRY = "reportTo_Country_ID_S";

  /**
   * Class constructor.
   *
   */
  public TaxTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Create a new Tax.
   *
   * @param data
   *          The data of the Tax that will be created.
   */
  public void create(TaxData data) {
    clickCreateNewRecord();
    if (data.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(data.getDataField("name").toString());
    }
    if (data.getDataField("description") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_DESCRIPTION)
          .sendKeys(data.getDataField("description").toString());
    }
    if (data.getDataField("validFromDate") != null) {
      SeleniumSingleton.INSTANCE.findElementById(CALENDAR_VALID_FROM_DATE)
          .sendKeys(data.getDataField("validFromDate").toString());
    }
    if (data.getDataField("taxCategory") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_TAX_CATEGORY))
          .selectByVisibleText(data.getDataField("taxCategory").toString());
    }
    if (data.getDataField("summaryLevel") != null) {
      WebElement checkBoxSummaryLevel = SeleniumSingleton.INSTANCE
          .findElementById(CHECK_BOX_SUMMARY_LEVEL);
      if (!data.getDataField("summaryLevel").equals(checkBoxSummaryLevel.isSelected())) {
        checkBoxSummaryLevel.click();
      }
    }
    if (data.getDataField("parentTaxRate") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_PARENT_TAX_RATE))
          .selectByVisibleText(data.getDataField("parentTaxRate").toString());
    }
    if (data.getDataField("salesPurchaseType") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_TYPE))
          .selectByVisibleText(data.getDataField("salesPurchaseType").toString());
    }
    if (data.getDataField("rate") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_RATE)
          .sendKeys(data.getDataField("rate").toString());
    }
    if (data.getDataField("businessPartnerTaxCategory") != null) {
      new Select(
          SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_BUSINESS_PARTNER_TAX_CATEGORY))
              .selectByVisibleText(data.getDataField("businessPartnerTaxCategory").toString());
    }
    if (data.getDataField("country") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_COUNTRY))
          .selectByVisibleText(data.getDataField("country").toString());
    }
    if (data.getDataField("destinationCountry") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_DESTINATION_COUNTRY))
          .selectByVisibleText(data.getDataField("destinationCountry").toString());
    }
    clickSave();
  }
}
