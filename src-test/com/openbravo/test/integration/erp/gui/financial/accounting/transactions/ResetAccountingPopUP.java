/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  David Miguélez <david.miguelez@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.transactions;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on the Reset Accounting Pop Up.
 *
 * @author David Miguélez
 *
 */

public class ResetAccountingPopUP extends OBClassicPopUp {

  /** The pop up menu path. */
  public static final String[] MENU_PATH = new String[] { Menu.FINANCIAL_MANAGEMENT,
      Menu.ACCOUNTING, Menu.ACCOUNTING_TRANSACTIONS, Menu.RESET_ACCOUNTING };

  /* Component identifiers. */
  /** The identifier of the client combo box. */
  private static final String IDENTIFIER_COMBO_BOX_CLIENT = "inpadClientId";
  /** The identifier of the organization combo box. */
  private static final String IDENTIFIER_COMBO_BOX_ORGANIZATION = "inpadOrgId";
  /** The identifier of the table combo box. */
  private static final String IDENTIFIER_COMBO_BOX_TABLE = "inpadTableId";
  /** The identifier of the delete existing accounting entries checkbox. */
  private static final String IDENTIFIER_CHECKBOX_DELETE = "inpdeleteposting";

  /**
   * Class constructor.
   *
   */
  public ResetAccountingPopUP() {
    super(TestRegistry.getObjectString(
        "org.openbravo.classicpopup./ad_actionButton/ActionButtonJava_Responser.html?Command=BUTTONE264309FF8244A94936502BF51829109"));
  }

  /**
   * Select the pop up frame.
   */
  @Override
  protected void selectFrame() {
    SeleniumSingleton.INSTANCE.switchTo().frame("E264309FF8244A94936502BF51829109");
  }

  /**
   * Reset Accounting.
   *
   * @param client
   *          The name of the client.
   * @param organization
   *          The name of the organization.
   */
  public void resetAccounting(String client, String organization) {
    selectPopUpWithOKButton();
    verifyElementsAreVisible(
        new String[] { IDENTIFIER_COMBO_BOX_CLIENT, IDENTIFIER_COMBO_BOX_ORGANIZATION,
            IDENTIFIER_COMBO_BOX_TABLE, IDENTIFIER_CHECKBOX_DELETE });
    new Select(SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_COMBO_BOX_CLIENT))
        .selectByVisibleText(client);
    new Select(SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_COMBO_BOX_ORGANIZATION))
        .selectByVisibleText(organization);
    if (!SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_CHECKBOX_DELETE).isSelected()) {
      SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_CHECKBOX_DELETE).click();
    }
    Sleep.trySleep(3000);
    clickOK();
  }

}
