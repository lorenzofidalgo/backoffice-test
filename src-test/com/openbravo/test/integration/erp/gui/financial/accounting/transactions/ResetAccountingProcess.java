/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  David Miguélez <david.miguelez@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.transactions;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import com.openbravo.test.integration.erp.data.selectors.OrganizationSelectorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBParameterWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on the Reset Accounting Pop Up.
 *
 * @author aferraz
 *
 */

@SuppressWarnings("rawtypes")
public class ResetAccountingProcess extends OBParameterWindow {

  private static final String RESET_ACCOUNTING_PROCESS_ID = "C6ED4B93E0D54C08A57072AEEC40E6EC";

  /** The pop up menu path. */
  public static final String[] MENU_PATH = new String[] { Menu.FINANCIAL_MANAGEMENT,
      Menu.ACCOUNTING, Menu.ACCOUNTING_TRANSACTIONS, Menu.RESET_ACCOUNTING };

  /* Component identifiers. */
  /** The identifier of the client combo box. */
  private static final String IDENTIFIER_COMBO_BOX_CLIENT = "AD_Client_ID";
  /** The identifier of the organization combo box. */
  private static final String IDENTIFIER_COMBO_BOX_ORGANIZATION = "AD_Org_ID";
  /** The identifier of the delete existing accounting entries checkbox. */
  private static final String IDENTIFIER_CHECKBOX_DELETE = "DeletePosting";

  public ResetAccountingProcess() {
    this(null, RESET_ACCOUNTING_PROCESS_ID, true);
  }

  public ResetAccountingProcess(GeneratedTab<?> tab) {
    this(tab, RESET_ACCOUNTING_PROCESS_ID, true);
  }

  public ResetAccountingProcess(String processDefinitionId) {
    this(null, processDefinitionId, true);
  }

  @SuppressWarnings("unchecked")
  public ResetAccountingProcess(GeneratedTab<?> tab, String processDefinitionId,
      Boolean waitForLoad) {
    super(RESET_ACCOUNTING_PROCESS_ID, tab);
  }

  /**
   * Reset Accounting.
   *
   * @param client
   *          The name of the client.
   * @param organization
   *          The name of the organization.
   */
  public void resetAccounting(String client, String organization) {
    setParameterValue(IDENTIFIER_COMBO_BOX_CLIENT, client);
    setParameterValue(IDENTIFIER_COMBO_BOX_ORGANIZATION,
        new OrganizationSelectorData.Builder().name(organization).build());
    setParameterValue(IDENTIFIER_CHECKBOX_DELETE, true);
    pressOkButton();
    // TODO L4:Check if the following static sleep can be changed from 20000 to 15000
    Sleep.trySleep(20000);
    int unposted = Integer.valueOf(getMessageContents()
        .substring(getMessageContents().indexOf('=') + 2, getMessageContents().indexOf(',')));
    int undeleted = Integer.valueOf(getMessageContents()
        .substring(getMessageContents().lastIndexOf('=') + 2, getMessageContents().length() - 4));
    assertThat(getMessageContents(),
        is(equalTo(String.format(
            "<b>Number of unposted documents = %d, Number of deleted journal entries = %d</b>",
            unposted, undeleted))));
  }
}
