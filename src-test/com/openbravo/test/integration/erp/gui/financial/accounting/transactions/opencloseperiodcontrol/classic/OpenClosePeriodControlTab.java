/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *      Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.transactions.opencloseperiodcontrol.classic;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.financial.accounting.transactions.opencloseperiodcontrol.OpenClosePeriodControlData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicButtonPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Fiscal Calendar tab.
 *
 * @author elopio
 *
 */
public class OpenClosePeriodControlTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Open/Close Period Control";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the Open/Close Period Control tab. */
  public static final String TAB = "tabnameE429CB8F5CDF4D8395B76D553E72A2D0";

  /** Identifier of the organization combo box. */
  private static final String COMBO_BOX_ORGANIZATION = "reportAD_Org_ID_S";
  /** Identifier of the calendar combo box. */
  @SuppressWarnings("unused")
  private static final String COMBO_BOX_CALENDAR = "reportC_Calendar_ID_S";
  /** Identifier of the until period number combo box. */
  private static final String COMBO_BOX_UNTIL_PERIOD_NUMBER = "reportPeriodno_S";
  /** Identifier of the period action combo box. */
  private static final String COMBO_BOX_PERIOD_ACTION = "reportPeriodaction_S";
  /** Identifier of the Year combo box. */
  private static final String COMBO_BOX_YEAR = "reportC_Year_ID_S";

  /**
   * Class constructor
   *
   */
  public OpenClosePeriodControlTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Create a new open or closed period control.
   *
   * @param openClosePeriodControlData
   *          The data of the period control.
   */
  public void create(OpenClosePeriodControlData openClosePeriodControlData) {
    clickCreateNewRecord();
    // wait for the until period number combo box to load
    waitForDataToLoad();

    if (openClosePeriodControlData.getDataField("organization") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_ORGANIZATION))
          .selectByVisibleText(openClosePeriodControlData.getDataField("organization").toString());
    }
    if (openClosePeriodControlData.getDataField("calendar") != null) {
      // XXX: PLU-2011-03-30 removed since the combobox is read only
      // new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_CALENDAR))
      // .selectByVisibleText(openClosePeriodControlData.getDataField("calendar").toString());
    }
    if (openClosePeriodControlData.getDataField("year") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_YEAR))
          .selectByVisibleText(openClosePeriodControlData.getDataField("year").toString());
      waitForDataToLoad();
    }
    if (openClosePeriodControlData.getDataField("periodNo") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_UNTIL_PERIOD_NUMBER))
          .selectByVisibleText(openClosePeriodControlData.getDataField("periodNo").toString());
    }
    if (openClosePeriodControlData.getDataField("periodAction") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_PERIOD_ACTION))
          .selectByVisibleText(openClosePeriodControlData.getDataField("periodAction").toString());
    }
    clickSave();
  }

  /**
   * Open or close all period documents.
   */
  public void openCloseAll() throws OpenbravoERPTestException {
    // switchToFormView();

    SeleniumSingleton.INSTANCE.findElementById(BUTTON_PROCESS).click();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    final OBClassicPopUp openCloseAll = new OBClassicButtonPopUp() {
      /**
       * Select the pop up frame.
       */
      @Override
      protected void selectFrame() {
        SeleniumSingleton.INSTANCE.switchTo().frame("BUTTON");
        SeleniumSingleton.INSTANCE.switchTo().frame("mainframe");
      }
    };
    openCloseAll.selectPopUpWithOKButton();
    openCloseAll.clickOK();

    waitForFrame();
  }
}
