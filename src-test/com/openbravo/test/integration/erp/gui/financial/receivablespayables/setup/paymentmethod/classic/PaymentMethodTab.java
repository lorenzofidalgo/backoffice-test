/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.setup.paymentmethod.classic;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.setup.paymentmethod.PaymentMethodData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Payment Method tab.
 *
 * @author elopio
 *
 */
public class PaymentMethodTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Payment Method";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the Payment Method tab. */
  public static final String TAB = "tabnameA4A463FA34F946BFA3F687DC8754ED93";

  /** Identifier of the name text field. */
  private static final String TEXT_FIELD_NAME = "Name";
  /* Defaults Payment IN fields. */
  /** Identifier of the Payment In Allowed check box. */
  private static final String CHECK_BOX_PAYMENT_IN_ALLOWED = "Payin_Allow";
  /** Identifier of the Automatic Receipt check box. */
  private static final String CHECK_BOX_AUTOMATIC_RECEIPT = "Automatic_Receipt";
  /** Identifier of the Automatic Deposit check box. */
  private static final String CHECK_BOX_AUTOMATIC_DEPOSIT = "Automatic_Deposit";
  /** Identifier of the Execution Type (IN) combo box. */
  private static final String COMBO_BOX_IN_EXECUTION_TYPE = "reportPayin_Execution_Type_S";
  /** Identifier of the Execution Process (IN) combo box. */
  private static final String COMBO_BOX_IN_EXECUTION_PROCESS = "reportPayin_Execution_Process_ID_S";
  /** Identifier of the Deferred (IN) check box. */
  private static final String CHECK_BOX_IN_DEFERRED = "Payin_Deferred";
  /** Identifier of the Upon Receipt Use combo box. */
  private static final String COMBO_BOX_UPON_RECEIPT_USE = "reportUponreceiptuse_S";
  /** Identifier of the Upon Deposit Use combo box. */
  private static final String COMBO_BOX_UPON_DEPOSIT_USE = "reportUpondeposituse_S";
  /** Identifier of the Upon Reconciliation Use (IN) combo box. */
  private static final String COMBO_BOX_IN_UPON_RECONCILIATION_USE = "reportInuponclearinguse_S";
  /* Defaults Payment OUT fields. */
  /** Identifier of the Payment Out Allowed check box. */
  private static final String CHECK_BOX_PAYMENT_OUT_ALLOWED = "Payout_Allow";
  /** Identifier of the Automatic Payment check box. */
  private static final String CHECK_BOX_AUTOMATIC_PAYMENT = "Automatic_Payment";
  /** Identifier of the Automatic Withdrawn check box. */
  private static final String CHECK_BOX_AUTOMATIC_WITHDRAWN = "Automatic_Withdrawn";
  /** Identifier of the Execution Type (OUT) combo box. */
  private static final String COMBO_BOX_OUT_EXECUTION_TYPE = "reportPayout_Execution_Type_S";
  /** Identifier of the Execution Process (OUT) combo box. */
  private static final String COMBO_BOX_OUT_EXECUTION_PROCESS = "reportPayout_Execution_Process_ID_S";
  /** Identifier of the Deferred (OUT) check box. */
  private static final String CHECK_BOX_OUT_DEFERRED = "Payout_Deferred";
  /** Identifier of the Upon Payment Use combo box. */
  private static final String COMBO_BOX_UPON_PAYMENT_USE = "reportUponpaymentuse_S";
  /** Identifier of the Upon Withdrawal Use combo box. */
  private static final String COMBO_BOX_UPON_WITHDRAWAL_USE = "reportUponwithdrawaluse_S";
  /** Identifier of the Upon Reconciliation Use (OUT) combo box. */
  private static final String COMBO_BOX_OUT_UPON_RECONCILIATION_USE = "reportOutuponclearinguse_S";

  /**
   * Class constructor.
   *
   */
  public PaymentMethodTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Create a new Payment Method.
   *
   * @param data
   *          The data of the Payment Method that will be created.
   */
  public void create(PaymentMethodData data) {
    clickCreateNewRecord();
    if (data.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(data.getDataField("name").toString());
    }
    if (data.getDataField("payinAllow") != null) {
      WebElement checkBoxPayinAllow = SeleniumSingleton.INSTANCE
          .findElementById(CHECK_BOX_PAYMENT_IN_ALLOWED);
      if (!data.getDataField("payinAllow").equals(checkBoxPayinAllow.isSelected())) {
        checkBoxPayinAllow.click();
      }
    }
    if (data.getDataField("automaticReceipt") != null) {
      WebElement checkBoxAutomaticReceipt = SeleniumSingleton.INSTANCE
          .findElementById(CHECK_BOX_AUTOMATIC_RECEIPT);
      if (!data.getDataField("automaticReceipt").equals(checkBoxAutomaticReceipt.isSelected())) {
        checkBoxAutomaticReceipt.click();
      }
    }
    if (data.getDataField("automaticDeposit") != null) {
      WebElement checkBoxAutomaticDeposit = SeleniumSingleton.INSTANCE
          .findElementById(CHECK_BOX_AUTOMATIC_DEPOSIT);
      if (!data.getDataField("automaticDeposit").equals(checkBoxAutomaticDeposit.isSelected())) {
        checkBoxAutomaticDeposit.click();
      }
    }
    if (data.getDataField("payinExecutionType") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_IN_EXECUTION_TYPE))
          .selectByVisibleText(data.getDataField("payinExecutionType").toString());
    }
    if (data.getDataField("payinExecutionProcess") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_IN_EXECUTION_PROCESS))
          .selectByVisibleText(data.getDataField("payinExecutionProcess").toString());
    }
    if (data.getDataField("payinDeferred") != null) {
      WebElement checkBoxInDeferred = SeleniumSingleton.INSTANCE
          .findElementById(CHECK_BOX_IN_DEFERRED);
      if (!data.getDataField("payinDeferred").equals(checkBoxInDeferred.isSelected())) {
        checkBoxInDeferred.click();
      }
    }
    if (data.getDataField("uponReceiptUse") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_UPON_RECEIPT_USE))
          .selectByVisibleText(data.getDataField("uponReceiptUse").toString());
    }
    if (data.getDataField("uponDepositUse") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_UPON_DEPOSIT_USE))
          .selectByVisibleText(data.getDataField("uponDepositUse").toString());
    }
    if (data.getDataField("iNUponClearingUse") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_IN_UPON_RECONCILIATION_USE))
          .selectByVisibleText(data.getDataField("iNUponClearingUse").toString());
    }
    if (data.getDataField("payoutAllow") != null) {
      WebElement checkBoxPayoutAllow = SeleniumSingleton.INSTANCE
          .findElementById(CHECK_BOX_PAYMENT_OUT_ALLOWED);
      if (!data.getDataField("payoutAllow").equals(checkBoxPayoutAllow.isSelected())) {
        checkBoxPayoutAllow.click();
      }
    }
    if (data.getDataField("automaticPayment") != null) {
      WebElement checkBoxAutomaticPayment = SeleniumSingleton.INSTANCE
          .findElementById(CHECK_BOX_AUTOMATIC_PAYMENT);
      if (!data.getDataField("automaticPayment").equals(checkBoxAutomaticPayment.isSelected())) {
        checkBoxAutomaticPayment.click();
      }
    }
    if (data.getDataField("automaticWithdrawn") != null) {
      WebElement checkBoxAutomaticWithdrawn = SeleniumSingleton.INSTANCE
          .findElementById(CHECK_BOX_AUTOMATIC_WITHDRAWN);
      if (!data.getDataField("automaticWithdrawn")
          .equals(checkBoxAutomaticWithdrawn.isSelected())) {
        checkBoxAutomaticWithdrawn.click();
      }
    }
    if (data.getDataField("payoutExecutionType") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_OUT_EXECUTION_TYPE))
          .selectByVisibleText(data.getDataField("payoutExecutionType").toString());
    }
    if (data.getDataField("payoutExecutionProcess") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_OUT_EXECUTION_PROCESS))
          .selectByVisibleText(data.getDataField("payoutExecutionProcess").toString());
    }
    if (data.getDataField("payoutDeferred") != null) {
      WebElement checkBoxOutDeferred = SeleniumSingleton.INSTANCE
          .findElementById(CHECK_BOX_OUT_DEFERRED);
      if (!data.getDataField("payoutDeferred").equals(checkBoxOutDeferred.isSelected())) {
        checkBoxOutDeferred.click();
      }
    }
    if (data.getDataField("uponPaymentUse") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_UPON_PAYMENT_USE))
          .selectByVisibleText(data.getDataField("uponPaymentUse").toString());
    }
    if (data.getDataField("uponWithdrawalUse") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_UPON_WITHDRAWAL_USE))
          .selectByVisibleText(data.getDataField("uponWithdrawalUse").toString());
    }
    if (data.getDataField("oUTUponClearingUse") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_OUT_UPON_RECONCILIATION_USE))
          .selectByVisibleText(data.getDataField("oUTUponClearingUse").toString());
    }
    clickSave();
  }
}
