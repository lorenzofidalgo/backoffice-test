/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddTransactionData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBParameterWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBParameterWindowForm;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentSelectorField;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

import junit.framework.TestCase;

@SuppressWarnings("rawtypes")
public class AddTransactionProcess extends OBParameterWindow {

  private static Logger log = LogManager.getLogger();

  private static final String ADD_TRANSACTION_PROCESS_ID = "E68790A7B65F4D45AB35E2BAE34C1F39";

  @SuppressWarnings("unused")
  private static final String PARAM_TRANSACTION_TYPE = "trxtype";
  @SuppressWarnings("unused")
  private static final String PARAM_TRANSACTION_DATE = "trxdate";
  @SuppressWarnings("unused")
  private static final String PARAM_ACCOUNTING_DATE = "dateacct";
  private static final String PARAM_PAYMENT = "fin_payment_id";
  @SuppressWarnings("unused")
  private static final String PARAM_GLITEM = "c_glitem_id";
  @SuppressWarnings("unused")
  private static final String PARAM_DESCRIPTION = "description";
  @SuppressWarnings("unused")
  private static final String PARAM_CURRENCY = "c_currency_id";
  @SuppressWarnings("unused")
  private static final String PARAM_DEPOSIT_AMT = "depositamt";
  @SuppressWarnings("unused")
  private static final String PARAM_WITHDRAWAL_AMT = "withdrawalamt";
  @SuppressWarnings("unused")
  private static final String PARAM_ORGANIZATION = "ad_org_id";
  @SuppressWarnings("unused")
  private static final String PARAM_BUSINESS_PARTNER = "c_bpartner_id";
  @SuppressWarnings("unused")
  private static final String PARAM_PROJECT = "c_project_id";
  @SuppressWarnings("unused")
  private static final String PARAM_PRODUCT = "m_product_id";
  @SuppressWarnings("unused")
  private static final String PARAM_COST_CENTER = "c_costcenter_id";
  @SuppressWarnings("unused")
  private static final String PARAM_USER1 = "user1_id";
  @SuppressWarnings("unused")
  private static final String PARAM_USER2 = "user2_id";
  @SuppressWarnings("unused")
  private static final String PARAM_SALES_CAMPAIGN = "c_campaign_id";
  @SuppressWarnings("unused")
  private static final String PARAM_ACTIVITY = "c_activity_id";
  @SuppressWarnings("unused")
  private static final String PARAM_SALES_REGION = "c_salesregion_id";
  @SuppressWarnings("unused")
  private static final String PARAM_FOREIGN_CURRENCY = "Foreign_Currency_ID";
  @SuppressWarnings("unused")
  private static final String PARAM_FOREIGN_AMOUNT = "Foreign_Amount";

  private static final String FORMAT_SMART_CLIENT_POPUP_PARTIALMATCH_OK = "isc.Dialog.Warn.okClick()";
  private static final String FORMAT_SMART_CLIENT_POPUP_PARTIALMATCH_CANCEL = "isc.Dialog.Warn.cancelClick()";

  private final OBParameterWindowForm form;

  /** Regular expression to match with the message of the payment creation. */
  public static final String REGEXP_MESSAGE_CREATED_PAYMENT = "Created Payment: .*\\. ";
  public static final String REGEXP_MESSAGE_REFUNDED_PAYMENT = "Refunded payment: .*\\. Created Payment: .*\\. ";
  public static final String REGEXP_ERROR_MESSAGE_PERIOD_NOT_OPENED = "<b>The Period does not exist or it is not opened</b>";
  public static final String REGEXP_ERROR_MESSAGE_AMOUNTS_ARE_ZERO = "<b>Warning</b><br/>Saving failed. Either deposited amount or payment amount should be different than zero";
  public static final String REGEXP_ERROR_MESSAGE_PAYMENT_AND_GLITEM_ARE_EMPTY = "<b>Error</b><br/>Saving failed. This transaction type must define either a Payment or a G/L Item";

  public AddTransactionProcess() {
    this(null, ADD_TRANSACTION_PROCESS_ID, true);
  }

  public AddTransactionProcess(GeneratedTab<?> tab) {
    this(tab, ADD_TRANSACTION_PROCESS_ID, true);
  }

  public AddTransactionProcess(String processDefinitionId) {
    this(null, processDefinitionId, true);
  }

  @SuppressWarnings("unchecked")
  public AddTransactionProcess(GeneratedTab<?> tab, String processDefinitionId,
      Boolean waitForLoad) {
    super(ADD_TRANSACTION_PROCESS_ID, tab);
    this.form = new OBParameterWindowForm(processDefinitionId, waitForLoad);
  }

  @Override
  protected boolean isPopup() {
    return true;
  }

  public AddPaymentProcess openAddPayment() {
    // XXX: Review if it works, not properly tested
    AddPaymentSelectorField paymentSelector = (AddPaymentSelectorField) this.getForm()
        .getField(PARAM_PAYMENT);
    paymentSelector.openPopUpToProcess();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess();
    return addPaymentProcess;
  }

  public void process() {
    process(null);
  }

  public void process(Boolean partialmatch) {
    scrollToBottom();

    if (tab != null) {
      tab.closeMessageIfVisible();
    }

    if (isOkButtonEnabled()) {
      pressOkButton();
    } else {
      // Wait for all displaylogic/onChanges to be executed
      for (int i = 0; i < 20 && !isOkButtonEnabled(); i++) {
        Sleep.trySleep(500);
      }
      if (isOkButtonEnabled()) {
        pressOkButton();
      } else {
        TestCase.fail("DONE button in Add Transaction window is not enabled");
      }
    }

    if (partialmatch != null) {
      Sleep.trySleep(3000);
      String action = partialmatch.booleanValue() ? FORMAT_SMART_CLIENT_POPUP_PARTIALMATCH_OK
          : FORMAT_SMART_CLIENT_POPUP_PARTIALMATCH_CANCEL;
      SeleniumSingleton.INSTANCE.executeScript(action);
    }

    if (tab != null) {
      tab.waitUntilMessageVisible();
    }
  }

  /**
   * Assert the data displayed in the add payment in pop up.
   *
   * @param data
   *          The data of the payment.
   */
  public void assertData(AddTransactionData data) {
    log.debug("Asserting data in add Payment Process");
    for (final String key : data.getDataFields().keySet()) {
      log.trace("[assertOnForm] Searching for key: " + key + " with value "
          + data.getDataFields().get(key));
      form.assertField(key, data.getDataFields().get(key));
    }
  }
}
