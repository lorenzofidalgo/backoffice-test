/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBParameterWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Grid;
import com.openbravo.test.integration.selenium.Sleep;

import junit.framework.TestCase;

@SuppressWarnings("rawtypes")
public class FindTransactionsToMatchProcess extends OBParameterWindow {

  private static final String TRANSACTIONS_TO_MATCH_PROCESS_ID = "154CB4F9274A479CB38A285E16984539";

  private static final String FORMAT_REGISTRY_KEY_GRID_TRANSACTIONS_TO_MATCH = "org.openbravo.client.application.ParameterWindow_Grid_findtransactiontomatch_%s";

  private Grid gridTransactionsToMatch;

  public FindTransactionsToMatchProcess() {
    this(null, TRANSACTIONS_TO_MATCH_PROCESS_ID);
  }

  public FindTransactionsToMatchProcess(GeneratedTab<?> tab) {
    this(tab, TRANSACTIONS_TO_MATCH_PROCESS_ID);
  }

  public FindTransactionsToMatchProcess(String processDefinitionId) {
    this(null, processDefinitionId);
  }

  @SuppressWarnings("unchecked")
  public FindTransactionsToMatchProcess(GeneratedTab<?> tab, String processDefinitionId) {
    super(TRANSACTIONS_TO_MATCH_PROCESS_ID, tab);
    gridTransactionsToMatch = new MatchStatementGrid(TestRegistry.getObjectString(
        String.format(FORMAT_REGISTRY_KEY_GRID_TRANSACTIONS_TO_MATCH, processDefinitionId)));
  }

  @Override
  protected boolean isPopup() {
    return true;
  }

  /**
   * @return Return Order/Invoice Grid +
   */
  public Grid getTransactionsToMatchGrid() {
    return gridTransactionsToMatch;
  }

  public void multipleFilterTransactionsToMatchGrid(DataObject data) {
    // TODO: Review if it works, not properly tested
    multipleFilterGrid(gridTransactionsToMatch, data);
  }

  public void process() {
    scrollToBottom();

    if (tab != null) {
      tab.closeMessageIfVisible();
    }

    if (isOkButtonEnabled()) {
      pressOkButton();
    } else {
      // Wait for all displaylogic/onChanges to be executed
      for (int i = 0; i < 20 && !isOkButtonEnabled(); i++) {
        Sleep.trySleep(500);
      }
      if (isOkButtonEnabled()) {
        pressOkButton();
      } else {
        TestCase.fail("DONE button in Add Payment window is not enabled");
      }
    }
    if (tab != null) {
      tab.waitUntilMessageVisible();
    }
  }

}
