/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 * Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.LinkedHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.GridInputFieldFactory;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBMessageBar;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBParameterWindowForm;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Canvas;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Grid;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.InputField;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SectionItem;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

import junit.framework.TestCase;

/** This class has been changed to fin Functional team tests requirements in 25-05-2015 */

public class MatchStatementProcess<T extends GeneratedTab<?>> extends Canvas {

  private static final String MATCH_STATEMENT_PROCESS_ID = "86F0B1EBE2BC48E3ACF458768D14CC99";

  private static final String FORMAT_REGISTRY_KEY_GRID_MATCH_STATEMENT = "org.openbravo.client.application.ParameterWindow_Grid_match_statement_%s";
  @SuppressWarnings("unused")
  private static final String FORMAT_SMART_CLIENT_POPUP_PROCESS_OK = "isc.Dialog.Warn.okClick()";
  @SuppressWarnings("unused")
  private static final String FORMAT_SMART_CLIENT_POPUP_PROCESS_CANCEL = "isc.Dialog.Warn.cancelClick()";

  private static final String REGEXP_MESSAGE_MATCHING_COMPLETED = "<b>Success</b><br/>The automatic matching algorithm has matched 1 transaction(s)";
  private static final String REGEXP_MESSAGE_UNMATCHING_SELECTED_COMPLETED = "<b>Process completed successfully</b><br/>2 record(s) were properly unmatched.";

  private MatchStatementGrid gridMatchStatement;

  /** START Variables from OBParameterWindow.java */

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  private static final String FORMAT_REGISTRY_KEY_PARAMETER_WINDOW = "org.openbravo.client.application.ParameterWindow_%s";
  private static final String FORMAT_REGISTRY_KEY_PARAMETER_WINDOW_MESSAGE_BAR = "org.openbravo.client.application.ParameterWindow_MessageBar_%s";
  // private static final String FORMAT_REGISTRY_KEY_PARAMETER_WINDOW_OKBUTTON =
  // "org.openbravo.client.application.ParameterWindow_OK_Button_%s"; //Not needed due to OK Button
  // has been hardcoded
  private static final String FORMAT_REGISTRY_KEY_PARAMETER_WINDOW_CANCELBUTTON = "org.openbravo.client.application.ParameterWindow_Cancel_Button_%s";
  private static final String FORMAT_REGISTRY_KEY_FORM_CONTAINER = "org.openbravo.client.application.ParameterWindow_FormContainerLayout_%s";

  private static final String FORMAT_SMART_CLIENT_GRID_FIELD = "%s.getEditForm().getField('%s')";

  /** The form view of the tab. */
  private final OBParameterWindowForm form;
  /** The message bar. */
  private final OBMessageBar barMessage;
  /** The button to invoke the process handler. */
  private Button okButton;
  /** The button to close the popup. */
  private Button cancelButton;
  /** The button to unmatch selected records */
  private Button unmatchSelectedButton;

  private Canvas containerLayout;

  protected GeneratedTab<?> tab;

  protected String processDefinitionId;

  /** END Variables from OBParameterWindow.java */

  public MatchStatementProcess() {
    this(null, MATCH_STATEMENT_PROCESS_ID);
  }

  public MatchStatementProcess(GeneratedTab<?> tab) {
    this(tab, MATCH_STATEMENT_PROCESS_ID);
  }

  public MatchStatementProcess(String processDefinitionId) {
    this(null, processDefinitionId);
  }

  public MatchStatementProcess(GeneratedTab<?> tab, String processDefinitionId) {
    this(MATCH_STATEMENT_PROCESS_ID, true);
    this.tab = tab;
    gridMatchStatement = new MatchStatementGrid(TestRegistry.getObjectString(
        String.format(FORMAT_REGISTRY_KEY_GRID_MATCH_STATEMENT, processDefinitionId)));
  }

  public MatchStatementProcess(String processDefinitionId, Boolean waitForLoad) {
    super(TestRegistry.getObjectString(
        String.format(FORMAT_REGISTRY_KEY_PARAMETER_WINDOW, processDefinitionId)), waitForLoad);
    this.barMessage = new OBMessageBar(
        TestRegistry.getObjectString(
            String.format(FORMAT_REGISTRY_KEY_PARAMETER_WINDOW_MESSAGE_BAR, processDefinitionId)),
        waitForLoad);
    this.okButton = new Button(TestRegistry
        .getObjectString("org.openbravo.client.application.process.pickandexecute.button.RE"));
    this.unmatchSelectedButton = new Button(TestRegistry
        .getObjectString("org.openbravo.client.application.process.pickandexecute.button.UN"));

    if (isPopup()) {
      this.cancelButton = new Button(TestRegistry.getObjectString(
          String.format(FORMAT_REGISTRY_KEY_PARAMETER_WINDOW_CANCELBUTTON, processDefinitionId)));
    }

    this.form = new OBParameterWindowForm(processDefinitionId, waitForLoad);

    containerLayout = new Canvas(TestRegistry.getObjectString(
        String.format(FORMAT_REGISTRY_KEY_FORM_CONTAINER, processDefinitionId)), waitForLoad);

    this.processDefinitionId = processDefinitionId;
    waitUntilDefaultValuesHaveBeenReceived();
  }

  protected boolean isPopup() {
    return true;
  }

  /**
   * @return Match Statement Grid
   */
  public Grid getMatchStatementGrid() {
    return gridMatchStatement;
  }

  public FindTransactionsToMatchProcess clickSearch(int rowNumber) {
    return gridMatchStatement.clickSearch(rowNumber);
  }

  public AddTransactionProcess clickAdd(int rowNumber) {
    return gridMatchStatement.clickAdd(rowNumber);
  }

  public void clickClear(int rowNumber) {
    gridMatchStatement.clickClear(rowNumber);
  }

  public void assertMatchingCompletedSuccessfully() {
    Sleep.trySleep();
    assertThat(getMessageContents(),
        is(equalTo(MatchStatementProcess.REGEXP_MESSAGE_MATCHING_COMPLETED)));
  }

  public void assertUnmatchSelectedCompletedSuccessfully() {
    assertThat(getMessageContents(),
        is(equalTo(MatchStatementProcess.REGEXP_MESSAGE_UNMATCHING_SELECTED_COMPLETED)));
  }

  public void process() {
    scrollToBottom();

    if (tab != null) {
      tab.closeMessageIfVisible();
    }

    if (isOkButtonEnabled()) {
      pressOkButton();
    } else {
      // Wait for all displaylogic/onChanges to be executed
      for (int i = 0; i < 20 && !isOkButtonEnabled(); i++) {
        Sleep.trySleep(500);
      }
      if (isOkButtonEnabled()) {
        pressOkButton();
      } else {
        TestCase.fail("DONE button in Match Statement window is not enabled");
      }
    }

    if (tab != null) {
      tab.waitUntilMessageVisible();
    }
  }

  public void editMatchStatementGrid(int rowNum, DataObject data) {
    editGrid(gridMatchStatement, rowNum, data);
  }

  /** START methods from OBParameterWindow.java */

  private void waitUntilDefaultValuesHaveBeenReceived() {
    final String paramWindowClassName = this.getClass().getSimpleName();

    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.trace("Waiting until defaults are set in {}", paramWindowClassName);
        Boolean result;
        try {
          result = (Boolean) SeleniumSingleton.INSTANCE
              .executeScriptWithReturn(objectString + " .defaultsAreSet === true");
        } catch (Exception e) {
          return false;
        }
        return result;
      }
    });

    // reset this flag in case it is checked before it is reset in ERP
    SeleniumSingleton.INSTANCE.executeScriptWithReturn(objectString + " .defaultsAreSet = false");
  }

  public void pressOkButton() {
    if (okButton.isVisible()) {
      okButton.click();
    }
  }

  public boolean isOkButtonEnabled() {
    return okButton.isEnabled();
  }

  public void pressUnmatchSelectedButton() {
    if (isUnmatchSelectedButtonVisible()) {
      unmatchSelectedButton.click();
    }
  }

  public boolean isUnmatchSelectedButtonVisible() {
    return unmatchSelectedButton.isVisible();
  }

  // /**
  // * Class constructor.
  // *
  // * @param tabIdentifier
  // * The tab identifier.
  // */
  // public OBParameterWindow(String processDefinitionId) {
  // this(processDefinitionId, true);
  // }
  // //
  // public OBParameterWindow(String processDefinitionId, GeneratedTab tab) {
  // this(processDefinitionId, true);
  // this.tab = tab;
  // }

  public Object getParameterValue(String parameterName) {
    return form.getData(parameterName);
  }

  public void setParameterValue(String parameterName, Object value) {
    form.fill(parameterName, value);
  }

  public OBParameterWindowForm getForm() {
    return form;
  }

  /**
   * Close the pop up.
   */
  public void close() {
    logger.debug("Closing Parameter Window pop up.");
    cancelButton.click();
  }

  @Override
  public void scrollToBottom() {
    containerLayout.scrollToBottom();
  }

  @Override
  public void scrollToTop() {
    containerLayout.scrollToTop();
  }

  protected void editGrid(Grid grid, int rowNum, DataObject data) {
    // make first column visible
    grid.scrollToLeft();
    grid.scrollToRight();
    // click on it to start edition
    grid.clickCell(rowNum, 0);
    editCurrentRecord(grid, data);
  }

  protected void editCurrentRecord(Grid grid, DataObject data) {
    // fill the fields
    LinkedHashMap<String, Object> fields = data.getDataFields();
    for (String fieldName : fields.keySet()) {
      InputField field = GridInputFieldFactory.getInputFieldObject(
          String.format(FORMAT_SMART_CLIENT_GRID_FIELD, grid.getObjectString(), fieldName), false);
      field.setValue(fields.get(fieldName));
    }
  }

  public Canvas getField(String fieldName) {
    if (form.getField(fieldName) instanceof Canvas) {
      return (Canvas) form.getField(fieldName);
    } else {
      logger.error(
          "A non expected class type has been found. Check that class is instance of Canvas");
      throw new NullPointerException();
    }
  }

  public SectionItem getFieldSection(String fieldName) {
    return form.getFieldSection(fieldName);
  }

  /**
   * Add multiple filters in a Parameter Window Grid
   *
   * @param grid
   *          The grid where it is wanted to filter
   * @param data
   *          The DataObject that contains the data to filter
   *
   */
  public void multipleFilterGrid(Grid grid, DataObject data) {
    LinkedHashMap<String, Object> fields = data.getDataFields();
    for (String fieldName : fields.keySet()) {
      grid.filter(fieldName, fields.get(fieldName).toString());
    }
  }

  /**
   * Get the contents of the message displayed in the message bar.
   *
   * @return the contents of the message displayed in the message bar.
   */
  public String getMessageContents() {
    barMessage.assertVisible();
    return barMessage.getContents();
  }

  /** END methods from OBParameterWindow.java */
}
