/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on Add Transaction pop up.
 *
 * @author elopio
 *
 */
public class AddTransactionPopUp extends OBClassicPopUp {

  /* Component identifiers. */
  /** The identifier of the document type combo box. */
  private static final String IDENTIFIER_COMBO_BOX_DOCUMENT_TYPE = "paramDocumentType";
  /** The identifier of the add payment button. */
  private static final String IDENTIFIER_BUTTON_ADD_PAYMENT = "AddPayment";
  /** The identifier of the process button. */
  private static final String IDENTIFIER_BUTTON_PROCESS = "buttonProcess";

  /**
   * Formatting string of the xpath to locate a checkbox in the table, using the description. The
   * parameter is the document description.
   */
  private static final String FORMAT_XPATH_CHECK_BOX_BY_DOCUMENT_DESCRIPTION = "//div[@id = 'client2']//td[@class = 'DataGrid_Body_Cell' and contains(text(), '%s')]/..//input[@type = 'checkbox']";

  /**
   * Class constructor. *
   */
  public AddTransactionPopUp() {
    super(TestRegistry.getObjectString(
        "org.openbravo.classicpopup./org.openbravo.advpaymentmngt.ad_actionbutton/AddTransaction.html"));
  }

  /**
   * Select the pop up frame.
   */
  @Override
  protected void selectFrame() {
    SeleniumSingleton.INSTANCE.switchTo().frame("process");
  }

  /**
   * Select the PopUp.
   *
   * This function waits until the process button is present and visible.
   *
   */
  public void selectPopUp() {
    selectPopUp(IDENTIFIER_BUTTON_PROCESS);
  }

  /**
   * Add a transaction.
   *
   * @param document
   *          The document type.
   * @param description
   *          The description of the document.
   */
  public void addTransaction(String document, String description) {
    selectPopUp();
    new Select(SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_COMBO_BOX_DOCUMENT_TYPE))
        .selectByVisibleText(document);
    String orderXpath = String.format(FORMAT_XPATH_CHECK_BOX_BY_DOCUMENT_DESCRIPTION, description);
    SeleniumSingleton.INSTANCE.findElementByXPath(orderXpath).click();
    SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_BUTTON_PROCESS).click();
  }

  /**
   * Add Payment In/Out
   *
   * @param document
   *          The document type.
   * @param toBePaidTo
   *          The business partner to be paid.
   * @param paymentMethod
   *          The payment method.
   * @param purchaseInvoiceNumber
   *          The number of the purchase invoice.
   * @param action
   *          The action to execute on the document.
   */
  public void addPaymentInOut(String document, String toBePaidTo, String paymentMethod,
      String purchaseInvoiceNumber, String action) {
    selectPopUp();
    new Select(SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_COMBO_BOX_DOCUMENT_TYPE))
        .selectByVisibleText(document);
    SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_BUTTON_ADD_PAYMENT).click();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    AddPaymentOutPopUp popUp = new AddPaymentOutPopUp();
    popUp.addPaymentOut(toBePaidTo, paymentMethod, purchaseInvoiceNumber, action);
  }

}
