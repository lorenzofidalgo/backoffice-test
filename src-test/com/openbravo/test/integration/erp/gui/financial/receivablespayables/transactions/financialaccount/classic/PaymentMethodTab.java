/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount.classic;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Payment Method tab.
 *
 * @author elopio
 *
 */
public class PaymentMethodTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Payment Method";
  /** The tab level. */
  private static final int LEVEL = 1;

  /* GUI components. */
  /** Identifier of the Account tab. */
  public static final String TAB = "tabname01F5E95D71544D428E1B9004B05D0298";

  /** Identifier of the Payment Method combo box. */
  private static final String COMBO_BOX_PAYMENT_METHOD = "reportFIN_Paymentmethod_ID_S";

  /**
   * Class constructor.
   *
   */
  public PaymentMethodTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Create a new Payment Method.
   *
   * @param data
   *          The data of the Payment Method that will be created.
   */
  public void create(PaymentMethodData data) {
    clickCreateNewRecord();
    if (data.getDataField("paymentMethod") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_PAYMENT_METHOD))
          .selectByVisibleText(data.getDataField("paymentMethod").toString());
    }
    clickSave();
  }
}
