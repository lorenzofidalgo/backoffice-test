/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentproposal;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentproposal.PaymentProposalHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentproposal.PaymentProposalLinesData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Payment Proposal Header tab.
 *
 * @author elopio
 *
 */
public class PaymentProposalHeaderTab extends GeneratedTab<PaymentProposalHeaderData> {

  /** The tab title. */
  static final String TITLE = "Header";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "2DD6F1E2CAE0456AA9797A1D627BFF5E";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /* Registry keys. */
  /** Registry key of the select expected payments button. */
  private static final String REGISTRY_KEY_BUTTON_SELECT_EXPECTED_PAYMENTS = "org.openbravo.client.application.toolbar.button.aPRMSelExpectedPayments.2DD6F1E2CAE0456AA9797A1D627BFF5E";
  /** Registry key of the generate payments button. */
  private static final String REGISTRY_KEY_BUTTON_GENERATE_PAYMENTS = "org.openbravo.client.application.toolbar.button.aPRMProcessProposal.2DD6F1E2CAE0456AA9797A1D627BFF5E";
  /** Registry key of the Pick and Edit Lines Pop Up */
  private static final String REGISTRY_KEY_POPUP_PICKANDEDIT = "org.openbravo.client.application.process.pickandexecute.popup";

  /* Components. */
  /** Select expected payments button. */
  private final Button buttonSelectExpectedPayments;
  /** Generate payments button. */
  private final Button buttonGeneratePayments;

  /**
   * Class constructor.
   *
   */
  public PaymentProposalHeaderTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new PaymentProposalLinesTab(this));
    buttonSelectExpectedPayments = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_SELECT_EXPECTED_PAYMENTS));
    buttonGeneratePayments = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_GENERATE_PAYMENTS));
  }

  /**
   * Select expected payments.
   *
   * @param orderNumbers
   *          The order numbers that will be selected.
   */
  public void selectExpectedPayments(String... orderNumbers) {
    buttonSelectExpectedPayments.click();
    Sleep.trySleep();
    PickAndExecuteWindow<PaymentProposalLinesData> popUp = new PickAndExecuteWindow<PaymentProposalLinesData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    Sleep.trySleep();
    popUp.select("invoiceDocumentno", orderNumbers);
    popUp.newVersion();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Select all expected payments.
   */
  public void selectAllExpectedPayments() {
    buttonSelectExpectedPayments.click();
    PickAndExecuteWindow<PaymentProposalLinesData> popUp = new PickAndExecuteWindow<PaymentProposalLinesData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    Sleep.trySleep();
    popUp.selectAll();
    popUp.newVersion();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Select all expected payments.
   */
  public void unselectAllExpectedPayments() {
    buttonSelectExpectedPayments.click();
    PickAndExecuteWindow<PaymentProposalLinesData> popUp = new PickAndExecuteWindow<PaymentProposalLinesData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    Sleep.trySleep();
    popUp.unselectAll();
    popUp.newVersion();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Generate payments.
   */
  public void generatePayments() {
    buttonGeneratePayments.click();
    ProcessProposalPopUp popUp = new ProcessProposalPopUp();
    popUp.process();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

}
