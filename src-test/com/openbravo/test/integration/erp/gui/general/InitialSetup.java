/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 * 	Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general;

import org.openqa.selenium.WebElement;

import com.openbravo.test.integration.erp.data.generalsetup.AccountingDimensions;
import com.openbravo.test.integration.erp.modules.client.application.gui.ClassicProcessView;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Contains definitions common to initial setup screens
 *
 * @author elopio
 *
 */
public class InitialSetup extends ClassicProcessView {

  /* Accounting elements */
  /** Identifier of the include accounting check box */
  protected static final String CHECK_BOX_INCLUDE_ACCOUNTING = "inpCreateAccounting";
  /** Identifier of the accounting file input */
  protected static final String FILE_INPUT_ACCOUNTING_FILE = "inpFile";
  /** Identifier of the currency combo box */
  protected static final String COMBO_BOX_CURRENCY = "inpCurrency";

  /* Accounting dimensions elements */
  /** Identifier of the business partner check box */
  private static final String CHECK_BOX_BUSINESS_PARTNER = "inpBPartner";
  /** Identifier of the project check box */
  private static final String CHECK_BOX_PROJECT = "inpProduct";
  /** Identifier of the product check box */
  private static final String CHECK_BOX_PRODUCT = "inpProject";
  /** Identifier of the campaign check box */
  private static final String CHECK_BOX_CAMPAIGN = "inpCampaign";
  /** Identifier of the sales region check box */
  private static final String CHECK_BOX_SALES_REGION = "inpSalesRegion";

  /**
   * Class constructor
   *
   * @param title
   *          The title of the view.
   * @param menuPath
   *          The path to open the view from the menu.
   */
  public InitialSetup(String title, String... menuPath) {
    super(title, menuPath);
  }

  /**
   * Set the values of the accounting dimensions
   *
   * @param accounting
   *          Accounting dimensions
   */
  protected void setAccountingDimensions(AccountingDimensions accounting) {
    if (accounting.isBusinessPartner() != null) {
      WebElement checkBoxBusinessPartner = SeleniumSingleton.INSTANCE
          .findElementByName(CHECK_BOX_BUSINESS_PARTNER);
      if (!accounting.isBusinessPartner().equals(checkBoxBusinessPartner.isSelected())) {
        checkBoxBusinessPartner.click();
      }
    }
    if (accounting.isProject() != null) {
      WebElement checkBoxProject = SeleniumSingleton.INSTANCE.findElementByName(CHECK_BOX_PROJECT);
      if (!accounting.isProject().equals(checkBoxProject.isSelected())) {
        checkBoxProject.click();
      }
    }
    if (accounting.isProduct() != null) {
      WebElement checkBoxProduct = SeleniumSingleton.INSTANCE.findElementByName(CHECK_BOX_PRODUCT);
      if (!accounting.isProduct().equals(checkBoxProduct.isSelected())) {
        checkBoxProduct.click();
      }
    }
    if (accounting.isCampaign() != null) {
      WebElement checkBoxCampaign = SeleniumSingleton.INSTANCE
          .findElementByName(CHECK_BOX_CAMPAIGN);
      if (!accounting.isCampaign().equals(checkBoxCampaign.isSelected())) {
        checkBoxCampaign.click();
      }
    }
    if (accounting.isSalesRegion() != null) {
      WebElement checkBoxSalesRegion = SeleniumSingleton.INSTANCE
          .findElementByName(CHECK_BOX_SALES_REGION);
      if (!accounting.isSalesRegion().equals(checkBoxSalesRegion.isSelected())) {
        checkBoxSalesRegion.click();
      }
    }
  }
}
