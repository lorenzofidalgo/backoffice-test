/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.application;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.openbravo.test.integration.erp.common.TimeoutConstants;
import com.openbravo.test.integration.erp.gui.MessageBox;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions and verifies actions on install module pop up.
 *
 * @author elopio
 *
 */
public class RebuildSystemPopUp extends OBClassicPopUp {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Identifier of the continue button. */
  private static final String BUTTON_CONTINUE = "buttonContinue";
  /** Identifier of the BUTTON frame. */
  public static final String BUTTON_FRAME = "BUTTON";

  /**
   * Class constructor.
   *
   */
  public RebuildSystemPopUp() {
    super(TestRegistry.getObjectString("org.openbravo.classicpopup."));
  }

  /**
   * Select the classic frame.
   */
  @Override
  public void waitForFrame() {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.trace("Waiting for the classic frame to load.");
        // XXX workaround for issue 16786 - https://issues.openbravo.com/view.php?id=16786
        int index = 0;
        while (true) {
          try {
            SeleniumSingleton.INSTANCE.switchTo().frame(index);
          } catch (NoSuchFrameException exception) {
            break;
          }
          try {
            SeleniumSingleton.INSTANCE.switchTo().frame(BUTTON_FRAME);
            return true;
          } catch (NoSuchFrameException exception) {
            index++;
            SeleniumSingleton.INSTANCE.switchTo().defaultContent();
          }
        }
        SeleniumSingleton.INSTANCE.switchTo().defaultContent();
        return false;
      }
    });
  }

  /**
   * Click OK button and wait for the page to load
   */
  public void clickContinue() {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_CONTINUE).click();
  }

  /**
   * Accept to rebuild the system.
   */
  public void rebuildTheSystem() {
    selectPopUpWithOKButton();
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK).click();
    final MessageBox messageBox = new MessageBox();
    Wait<WebDriver> rebuildSystemWait = new WebDriverWait(SeleniumSingleton.INSTANCE,
        TimeoutConstants.REBUILD_TIMEOUT);
    rebuildSystemWait.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for button continue to be loaded or message box to be displayed.");
        return SeleniumSingleton.INSTANCE.findElementById(BUTTON_CONTINUE).isDisplayed()
            || messageBox.isVisible();
      }
    });
  }

  /**
   * Restart the servlet container.
   */
  public void restartServletContainer() {
    clickContinue();
  }
}
