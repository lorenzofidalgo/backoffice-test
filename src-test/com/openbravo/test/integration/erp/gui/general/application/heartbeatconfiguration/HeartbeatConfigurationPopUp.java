/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.application.heartbeatconfiguration;

import com.openbravo.test.integration.erp.gui.MessageBox;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on heartbeat configuration pop up.
 *
 * @author elopio
 *
 */
public class HeartbeatConfigurationPopUp extends OBClassicPopUp {

  /** Name of the PopUp */
  protected static final String POPUP_NAME = "PROCESS";
  /**
   * Title displayed when the heartbeat activation process was completed successfully.
   */
  private static final String TITLE_PROCESS_COMPLETED_SUCCESSFULLY = "Heartbeat Configuration";
  /** Message displayed when the heartbeat activation succeeded. */
  private static final String MESSAGE_HEARTBEAT_CONFIGURATION_SUCCEEDED = "The heartbeat has been configured successfully.";

  /** Identifier of the close button. */
  @SuppressWarnings("hiding")
  private static final String BUTTON_CLOSE = "ButtonClose";

  /**
   * Class constructor.
   *
   */
  public HeartbeatConfigurationPopUp() {
    super(
        TestRegistry.getObjectString("org.openbravo.classicpopup./ad_process/TestHeartbeat.html"));
  }

  /**
   * Select the pop up frame.
   */
  @Override
  protected void selectFrame() {
    SeleniumSingleton.INSTANCE.switchTo().frame("process");
  }

  /**
   * Assert that the process completed successfully.
   */
  @Override
  public void assertProcessCompletedSuccessfully() {
    selectPopUp(BUTTON_CLOSE);
    final MessageBox messageBox = new MessageBox();
    // TODO see issue 10717 - https://issues.openbravo.com/view.php?id=10717
    messageBox.verify(TITLE_PROCESS_COMPLETED_SUCCESSFULLY,
        MESSAGE_HEARTBEAT_CONFIGURATION_SUCCEEDED);
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_CLOSE).click();
  }
}
