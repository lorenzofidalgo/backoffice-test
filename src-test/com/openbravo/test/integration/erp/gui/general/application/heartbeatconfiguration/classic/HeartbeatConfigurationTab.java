/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.application.heartbeatconfiguration.classic;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verify actions on Heartbeat Configuration tab.
 *
 * @author elopio
 *
 */
public class HeartbeatConfigurationTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Heartbeat Configuration";
  /** The tab level. */
  private static final int LEVEL = 0;

  /** Identifier of the Heartbeat Configuration tab. */
  public static final String TAB = "tabname1005400005";

  /** Identifier of the enable heartbeat button. */
  private static final String BUTTON_ENABLE_HEARTBEAT = "Testproxy_linkBTN";

  /**
   * Enumerator with the values of heartbeat status.
   *
   * @author elopio
   *
   */
  private enum Status {
    DISABLED("Enable Heartbeat"), ENABLED("Disable Heartbeat");

    private final String status;

    private Status(String status) {
      this.status = status;
    }

    public String status() {
      return status;
    }
  }

  /**
   * Class constructor.
   *
   */
  public HeartbeatConfigurationTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Indicates whether the heartbeat is enabled or not.
   *
   * @return true if heartbeat is enabled. Otherwise, false.
   */
  public boolean isEnabled() {
    return SeleniumSingleton.INSTANCE.findElementById(BUTTON_ENABLE_HEARTBEAT)
        .getText()
        .equals(Status.ENABLED.status());
  }

  /**
   * Change the status of heartbeat application. If heartbeat is enabled, this function will disable
   * it. If it's disabled, this function will enable it.
   */
  public void changeHeartbeatStatus() {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_ENABLE_HEARTBEAT).click();
  }

  /**
   * Verify the status of the heartbeat application.
   *
   * @param status
   *          The expected status.
   */
  public void verifyHeartbeatStatus(String status) {
    assertThat("Wrong status",
        SeleniumSingleton.INSTANCE.findElementById(BUTTON_ENABLE_HEARTBEAT).getText(),
        is(equalTo(status)));
  }

  /**
   * Verify that the heartbeat status is enabled.
   */
  public void verifyEnabledStatus() {
    verifyHeartbeatStatus(Status.ENABLED.status());
  }

  /**
   * Verify that the heartbeat status is active.
   */
  public void verifyDisabledStatus() {
    verifyHeartbeatStatus(Status.DISABLED.status());
  }

}
