/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.application.instanceactivation;

import com.openbravo.test.integration.erp.gui.popups.PopUp;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on deactivate pop up.
 *
 * @author elopio
 *
 */
public class ActivationProcessesPopUp extends PopUp {

  /**
   * Class constructor.
   *
   */
  public ActivationProcessesPopUp() {
    super();
  }

  /**
   * Click OK button and wait for the page to load with a static sleep.
   */
  @Override
  public void clickOK() {
    // XXX workaround for issue 11795 -
    // https://issues.openbravo.com/view.php?id=11795
    super.clickOK();
    Sleep.trySleep(10000);
  }
}
