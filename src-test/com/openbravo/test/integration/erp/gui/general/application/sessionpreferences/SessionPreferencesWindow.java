/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.application.sessionpreferences;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.modules.client.application.gui.ClassicProcessView;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on OpenbravoERP Session Preferences screen
 *
 * @author elopio
 *
 */
public class SessionPreferencesWindow extends ClassicProcessView {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The window title. */
  public static final String TITLE = "Session Preferences";

  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.GENERAL_SETUP, Menu.APPLICATION,
      Menu.SESSION_PREFERENCES };

  /** Identifier of the Show Accounting Tabs check box */
  private static final String CHECK_BOX_SHOW_ACCOUNTING_TABS = "paramAccounting";
  /** Identifier of the Save Preferences button */
  private static final String BUTTON_SAVE_PREFERENCES = "buttonSave";

  /**
   * Class constructor
   */
  public SessionPreferencesWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * Check the show accounting tabs preference
   */
  public void checkShowAccountingTabs() {
    if (!SeleniumSingleton.INSTANCE.findElementById(CHECK_BOX_SHOW_ACCOUNTING_TABS).isSelected()) {
      logger.trace("Accounting Tabs are not shown, enabling them");
      SeleniumSingleton.INSTANCE.findElementById(CHECK_BOX_SHOW_ACCOUNTING_TABS).click();
    } else {
      logger.trace("Accounting Tabs are currently shown, bypassing");
    }
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_SAVE_PREFERENCES).click();
    // See bug 5641 - https://issues.openbravo.com/view.php?id=5641
  }

}
