/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.client.initialclientsetup;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.generalsetup.ModuleReferenceData;
import com.openbravo.test.integration.erp.data.generalsetup.client.initialclientsetup.InitialClientSetupData;
import com.openbravo.test.integration.erp.gui.MessageBox;
import com.openbravo.test.integration.erp.gui.general.InitialSetup;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;
import com.openbravo.test.integration.selenium.Attribute;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Initial Client Setup process.
 *
 * @author elopio
 *
 */
public class InitialClientSetupProcess extends InitialSetup {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();
  /** The window title. */
  private static final String TITLE = "Initial Client Setup";

  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.GENERAL_SETUP, Menu.CLIENT,
      Menu.INITIAL_CLIENT_SETUP };

  /* Client elements. */
  /** Identifier of the client text field. */
  private static final String TEXT_FIELD_CLIENT = "paramClient";
  /** Identifier of the user name text field. */
  private static final String TEXT_FIELD_CLIENT_USER_NAME = "paramClientUser";
  /** Identifier of the user password text field. */
  private static final String TEXT_FIELD_CLIENT_USER_PASSWORD = "inpPassword";
  /** Identifier of the user password text field (confirmation). */
  private static final String TEXT_FIELD_CLIENT_USER_CONFIRM_PASSWORD = "inpConfirmPassword";
  /**
   * Formatting string used to show module data next to the checkbox. The first parameter is the
   * module description. The second is the version. The third is the language.
   */
  private static final String FORMAT_LABEL_REFERENCE_DATA = "%s - %s - %s";
  /** The maximum of characters in the Reference Data label. */
  private static final int MAX_LABEL_LENGTH_REFERENCE_DATA = 90;
  /** Formatting string used to locate the checkboxes in reference data. */
  private static String FORMAT_CHECK_BOX_REFERENCE_DATA = "(//div[@id='name' and @class='Tree_Text_Title' and contains(text(), '%s')]/../..)//input[@type = 'checkbox']";
  // TODO see issue 11543 - https://issues.openbravo.com/view.php?id=11543.
  /** Identifier of the result table data. */
  private static final String TABLE_DATA_RESULT = "paramResultado";

  /**
   * Class constructor.
   */
  public InitialClientSetupProcess() {
    super(TITLE, MENU_PATH);
  }

  /**
   * Set up the initial client.
   *
   * @param initialClientSetupData
   *          The initial client setup data.
   */
  public void setUp(InitialClientSetupData initialClientSetupData) {
    if (initialClientSetupData.getClient() != null) {
      // FIXME: This clear is to workaround issue
      // https://issues.openbravo.com/view.php?id=16730
      try {
        SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_CLIENT).clear();
      } catch (StaleElementReferenceException sere) {
        logger.trace(
            "StaleElementReferenceException was thrown and caught. Sleep and try clearing again.");
        Sleep.trySleep(7500);
        try {
          SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_CLIENT).clear();
        } catch (StaleElementReferenceException sere2) {
          Sleep.trySleep();
          logger.trace("Two StaleElementReferenceException in a row. Avoiding to clear.");
        }
      } catch (InvalidElementStateException iese) {
        Sleep.trySleep();
        SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_CLIENT).clear();
      }
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_CLIENT)
          .sendKeys(initialClientSetupData.getClient());
    }
    if (initialClientSetupData.getClientUserName() != null) {
      // FIXME: This clear is to workaround issue
      // https://issues.openbravo.com/view.php?id=16730
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_CLIENT_USER_NAME).clear();
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_CLIENT_USER_NAME)
          .sendKeys(initialClientSetupData.getClientUserName());
    }
    if (initialClientSetupData.getPassword() != null) {
      // FIXME: This clear is to workaround issue
      // https://issues.openbravo.com/view.php?id=16730
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_CLIENT_USER_PASSWORD).clear();
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_CLIENT_USER_PASSWORD)
          .sendKeys(initialClientSetupData.getPassword());
    }
    if (initialClientSetupData.getPasswordConfirmation() != null) {
      // FIXME: This clear is to workaround issue
      // https://issues.openbravo.com/view.php?id=16730
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_CLIENT_USER_CONFIRM_PASSWORD).clear();
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_CLIENT_USER_CONFIRM_PASSWORD)
          .sendKeys(initialClientSetupData.getPasswordConfirmation());
    }
    // FIXME this shouldn't be inheritance, it should be composition.
    if (initialClientSetupData.getInitialSetupData().getCurrency() != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_CURRENCY))
          .selectByVisibleText(initialClientSetupData.getInitialSetupData().getCurrency());
    }
    if (initialClientSetupData.getInitialSetupData().incluceAccounting() != null) {
      final WebElement checkBoxIncludeAccounting = SeleniumSingleton.INSTANCE
          .findElementByName(CHECK_BOX_INCLUDE_ACCOUNTING);
      if (!initialClientSetupData.getInitialSetupData()
          .incluceAccounting()
          .equals(checkBoxIncludeAccounting.isSelected())) {
        checkBoxIncludeAccounting.click();
      }
    }
    if (initialClientSetupData.getInitialSetupData().getReferenceDataList() != null) {
      // TODO is this still required?
      // SeleniumSingleton.INSTANCE.allowNativeXpath("false");
      for (final ModuleReferenceData module : initialClientSetupData.getInitialSetupData()
          .getReferenceDataList()) {
        String moduleLabel = String.format(FORMAT_LABEL_REFERENCE_DATA, module.getDescription(),
            module.getName(), module.getLanguage());
        if (moduleLabel.length() > MAX_LABEL_LENGTH_REFERENCE_DATA) {
          moduleLabel = moduleLabel.substring(0, MAX_LABEL_LENGTH_REFERENCE_DATA);
        }
        if (!SeleniumSingleton.INSTANCE
            .findElementByXPath(String.format(FORMAT_CHECK_BOX_REFERENCE_DATA, moduleLabel))
            .isSelected()) {
          // XXX workaround because the click function doesn't work with these checkboxes.
          String checkBoxId = SeleniumSingleton.INSTANCE
              .findElementByXPath(String.format(FORMAT_CHECK_BOX_REFERENCE_DATA, moduleLabel))
              .getAttribute(Attribute.ID.getName());
          SeleniumSingleton.INSTANCE.executeScript(
              String.format("document.getElementById('%s').checked = true", checkBoxId));
        }
      }
      // TODO is this still required?
      // SeleniumSingleton.INSTANCE.allowNativeXpath("true");
    }

    // FIXME set accounting dimensions.
    clickOK();
  }

  /**
   * Verify that the process completed successfully.
   *
   * Overrides the ClassicView method
   */
  @Override
  public void verifyProcessCompletedSuccessfully() {
    logger.debug("Verifying that the process was completed successfully.");
    final MessageBox messageBox = new MessageBox();
    messageBox.verify(MessageBox.TITLE_PROCESS_COMPLETED_SUCCESSFULLY,
        "A context reload is required to fully complete the action. You can reload this context automatically by clicking here or manually by logging out of the application.");
  }

  /**
   * Verify the result message.
   *
   * @param expectedResult
   *          The expected result message.
   */
  public void verifyResult(String expectedResult) {
    final String displayedResult = SeleniumSingleton.INSTANCE.findElementById(TABLE_DATA_RESULT)
        .getText()
        .replaceAll("\\s", " ");
    String parsedExpectedResult = expectedResult.replaceAll("\\s", " ");
    assertThat("Initial client setup result", displayedResult, is(equalTo(parsedExpectedResult)));
  }

}
