/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.enterprise.organization;

import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;

/**
 * Executes actions on Set as Ready Pop Up.
 *
 * @author elopio
 *
 */
public class SetAsReadyPopUp extends OBClassicPopUp {

  /**
   * Class constructor.
   *
   */
  public SetAsReadyPopUp() {
    super(TestRegistry
        .getObjectString("org.openbravo.classicpopup./Organization/Organization_Edition.html"));
  }

  /**
   * Set the organization as ready.
   */
  public void setAsReady() {
    selectPopUpWithOKButton();
    clickOK();
  }
}
