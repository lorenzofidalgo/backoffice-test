/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.enterprise.organization.classic;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.generalsetup.enterprise.organization.OrganizationData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicButtonPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verify actions on Organization tab.
 *
 * @author elopio
 *
 */
public class OrganizationTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Organization";
  /** The tab level. */
  private static final int LEVEL = 0;
  /** Identifier of the tab. */
  public static final String TAB = "tabname143";

  /** Number of the name column */
  public static final int COLUMN_NAME = 2;

  /** Identifier of the calendar combo box */
  private static final String COMBO_BOX_CALENDAR = "reportC_Calendar_ID_S";
  /** Identifier of the allow period control checkbox */
  private static final String CHECK_BOX_ALLOW_PERIOD_CONTROL = "IsPeriodControlAllowed";
  /** Button Identifier */
  private String BUTTON_SET_READY = "IsReady_linkBTN";

  /**
   * Class constructor.
   *
   */
  public OrganizationTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Edit an organization.
   *
   * @param data
   *          The edited organization data.
   */
  public void edit(OrganizationData data) throws OpenbravoERPTestException {
    switchToFormView();
    if (data.getDataField("allowPeriodControl") != null) {
      WebElement checkBoxAllowPeriodControl = SeleniumSingleton.INSTANCE
          .findElementById(CHECK_BOX_ALLOW_PERIOD_CONTROL);
      if (!data.getDataField("allowPeriodControl")
          .equals(checkBoxAllowPeriodControl.isSelected())) {
        checkBoxAllowPeriodControl.click();
      }
    }
    if (data.getDataField("calendar") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_CALENDAR))
          .selectByVisibleText(data.getDataField("calendar").toString());
    }
    clickSave();
  }

  /**
   * Set the organization as ready.
   */

  public void setReady() throws OpenbravoERPTestException {
    switchToFormView();
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_SET_READY).click();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    // XXX make a class for this pop up.
    final OBClassicPopUp ready = new OBClassicButtonPopUp() {
      @Override
      protected void selectFrame() {
        super.selectFrame();
        SeleniumSingleton.INSTANCE.switchTo().frame("mainframe");
      }
    };
    ready.selectPopUpWithOKButton();
    ready.clickOK();
    waitForFrame();
  }

}
