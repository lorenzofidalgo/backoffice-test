/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.processscheduling.processrequest;

import com.openbravo.test.integration.erp.gui.MessageBox;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on the Process Request pop up.
 *
 * @author elopio
 *
 */
public class ProcessRequestPopUp extends OBClassicPopUp {

  /** Name of the PopUp */
  protected static final String POPUP_NAME = "PROCESS";
  /**
   * Title displayed when the process request was completed successfully.
   */
  private static final String TITLE_PROCESS_COMPLETED_SUCCESSFULLY = "Process completed successfully";
  /** Message displayed when the unschedule process request succeeded. */
  private static final String MESSAGE_UNSCHEDULE_SUCCEEDED = "The process has been unscheduled successfully. Note this does not cancel already running processes.";

  /** Identifier of the close button. */
  @SuppressWarnings("hiding")
  private static final String BUTTON_CLOSE = "ButtonClose";

  /**
   * Class constructor.
   *
   * @param registryKey
   *          The registry key of the pop up.
   */
  public ProcessRequestPopUp(String registryKey) {
    super(TestRegistry.getObjectString(registryKey));
  }

  /**
   * Select the pop up frame.
   */
  @Override
  protected void selectFrame() {
    SeleniumSingleton.INSTANCE.switchTo().frame("process");
  }

  /**
   * Assert that the unschedule process completed successfully.
   */
  public void assertUnscheduleSucceeded() {
    selectPopUp(BUTTON_CLOSE);
    final MessageBox messageBox = new MessageBox();
    // TODO see issue 10717 - https://issues.openbravo.com/view.php?id=10717
    messageBox.verify(TITLE_PROCESS_COMPLETED_SUCCESSFULLY, MESSAGE_UNSCHEDULE_SUCCEEDED);
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_CLOSE).click();
  }

}
