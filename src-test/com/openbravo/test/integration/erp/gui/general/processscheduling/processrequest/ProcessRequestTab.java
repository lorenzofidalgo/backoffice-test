/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *      Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.processscheduling.processrequest;

import static org.junit.Assert.assertFalse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.generalsetup.processscheduling.processrequest.ProcessRequestData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Process Request tab.
 *
 * @author elopio
 *
 */
public class ProcessRequestTab extends GeneratedTab<ProcessRequestData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Process Request";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "CD573DF1E351485EA2B2DE487DCACA6F";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /* Registry keys. */
  /** The registry key of the unschedule process button. */
  private static final String REGISTRY_KEY_BUTTON_UNSCHEDULE_PROCESS = "org.openbravo.client.application.toolbar.button.unscheduleProcess.CD573DF1E351485EA2B2DE487DCACA6F";
  /** The registry key of the reschedule process button. */
  @SuppressWarnings("unused")
  private static final String REGISTRY_KEY_BUTTON_RESCHEDULE_PROCESS = "org.openbravo.client.application.toolbar.button.rescheduleProcess.CD573DF1E351485EA2B2DE487DCACA6F";
  /** The registry key of the unschedule process pop up. */
  private static final String REGISTRY_KEY_POP_UP_UNSCHEDULE_PROCESS = "org.openbravo.classicpopup./ad_process/UnscheduleProcess.html";

  /* Components. */
  /** The unschedule process button. */
  private final Button buttonUnscheduleProcess;

  /**
   * Class constructor
   *
   */
  public ProcessRequestTab() {
    super(TITLE, IDENTIFIER);
    buttonUnscheduleProcess = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_UNSCHEDULE_PROCESS));
  }

  /**
   * Unschedule the selected process request.
   *
   * @return true if the process was unscheduled. Otherwise, false.
   */
  public boolean unscheduleProcess() {
    logger.debug("Unscheduling the process.");
    boolean unscheduled = false;
    if (buttonUnscheduleProcess.isVisible()
        && buttonUnscheduleProcess.getText().equals("Unschedule Process")) {
      buttonUnscheduleProcess.click();
      unscheduled = true;
    } else {
      logger.info("The process is already unscheduled");
    }
    return unscheduled;
  }

  /**
   * Assert that the process was successfully unscheduled, and close the pop up.
   */
  public void assertProcessUnscheduleSucceeded() {
    ProcessRequestPopUp popUp = new ProcessRequestPopUp(REGISTRY_KEY_POP_UP_UNSCHEDULE_PROCESS);
    popUp.assertUnscheduleSucceeded();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    // XXX use a wait.
    Sleep.trySleep(5000);
    assertFalse(buttonUnscheduleProcess.isVisible());
  }
}
