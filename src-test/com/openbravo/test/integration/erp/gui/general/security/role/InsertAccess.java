/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 * 	Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.security.role;

import com.openbravo.test.integration.erp.gui.popups.PopUp;

/**
 * Executes actions on Insert Access PopUp
 *
 * @author elopio
 *
 */
public class InsertAccess extends PopUp {

  /** Identifier of the module combo box */
  @SuppressWarnings("unused")
  private static final String COMBO_BOX_MODULE = "inpModules";
  /** Identifier of the access to combo box */
  @SuppressWarnings("unused")
  private static final String COMBO_BOX_ACCESS_TO = "inpType";

  /**
   * Class constructor
   */
  public InsertAccess() {
    super();
  }

  /**
   * Select the insert access PopUp
   */
  public void select() {
    super.selectPopUpWithOKButton();
  }

  // /**
  // * Insert access to a role.
  // *
  // * @param grantAccessData
  // * The data of the access.
  // */
  // public void insertAccess(GrantAccessPopUpData grantAccessData) {
  // select();
  // if (grantAccessData.getModule() != null) {
  // SeleniumSingleton.INSTANCE.select(COMBO_BOX_MODULE, grantAccessData.getModule());
  // }
  // if (grantAccessData.getAccess() != null) {
  // SeleniumSingleton.INSTANCE.select(COMBO_BOX_ACCESS_TO, grantAccessData.getAccess());
  // }
  // SeleniumSingleton.INSTANCE.click(BUTTON_OK);
  // SeleniumSingleton.INSTANCE.waitForPageToLoad(PAGE_LOAD_TIMEOUT);
  // }
}
