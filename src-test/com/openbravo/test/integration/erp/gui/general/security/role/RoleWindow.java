/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Iñaki Garcia <inaki.garcia@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.security.role;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the Role standard window of OpenbravoERP.
 *
 * @author elopio
 *
 */
public class RoleWindow extends StandardWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  public static final String TITLE = "Role";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.GENERAL_SETUP, Menu.SECURITY,
      Menu.ROLE };

  /**
   * Class constructor.
   *
   */
  public RoleWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    RoleTab roleTab = new RoleTab();
    addTopLevelTab(roleTab);
    super.load();
  }

  /**
   * Select and return the Role tab.
   *
   * @return the Role tab.
   */
  public RoleTab selectRoleTab() {
    return (RoleTab) selectTab(RoleTab.IDENTIFIER);
  }

  /**
   * Select and return the Org Access tab.
   *
   * @return the Org Access tab.
   */
  public OrgAccessTab selectOrgAccessTab() {
    return (OrgAccessTab) selectTab(OrgAccessTab.IDENTIFIER);
  }

  /**
   * Select and return the User Assignment tab.
   *
   * @return the User Assignment tab.
   */
  public UserAssignmentTab selectUserAssignmentTab() {
    return (UserAssignmentTab) selectTab(UserAssignmentTab.IDENTIFIER);
  }

  /**
   * Select and return the Window Access tab.
   *
   * @return the Window Access tab.
   */
  public WindowAccessTab selectWindowAccessTab() {
    return (WindowAccessTab) selectTab(WindowAccessTab.IDENTIFIER);
  }

  /**
   * Select and return the Form Access tab.
   *
   * @return the Form Access tab.
   */
  public FormAccessTab selectFormAccessTab() {
    return (FormAccessTab) selectTab(FormAccessTab.IDENTIFIER);
  }

  /**
   * Select and return the Report and Process access tab.
   *
   * @return the Report and Process Access tab.
   */
  public ProcessAccessTab selectProcessAccessTab() {
    return (ProcessAccessTab) selectTab(ProcessAccessTab.IDENTIFIER);
  }

}
