/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.security.role.classic;

import org.openqa.selenium.WebElement;

import com.openbravo.test.integration.erp.data.generalsetup.security.role.RoleData;
import com.openbravo.test.integration.erp.data.generalsetup.security.user.GrantAccessPopUpData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Role tab.
 *
 * @author elopio
 *
 */
public class RoleTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Role";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the Role tab. */
  public static final String TAB = "tabname119";

  /** Identifier of the name text field */
  private static final String TEXT_FIELD_NAME = "Name";
  /** Identifier of the user level combo box */
  private static final String COMBO_BOX_USER_LEVEL = "reportUserLevel_S";
  /** Identifier of the is manual check box */
  private static final String CHECK_BOX_IS_MANUAL = "IsManual";
  /** Identifier of the grant access button */
  private static final String BUTTON_GRANT_ACCESS = "Processing_BTNname";

  /**
   * Class constructor
   *
   */
  public RoleTab() {
    super(TITLE, LEVEL, TAB);
    addSubTab(new OrgAccessTab());
    addSubTab(new UserAssignmentTab());
  }

  /**
   * Create a role.
   *
   * @param roleData
   *          The data of the role to create.
   */
  public void create(RoleData roleData) {
    clickCreateNewRecord();
    if (roleData.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(roleData.getDataField("name").toString());
    }
    if (roleData.getDataField("userLevel") != null) {
      SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_USER_LEVEL)
          .sendKeys(roleData.getDataField("userLevel").toString());
    }
    if (roleData.getDataField("manual") != null) {
      WebElement checkBoxIsManual = SeleniumSingleton.INSTANCE.findElementById(CHECK_BOX_IS_MANUAL);
      if (!roleData.getDataField("manual").equals(checkBoxIsManual.isSelected())) {
        checkBoxIsManual.click();
      }
    }
    clickSave();

  }

  /**
   * Grant access to a role.
   *
   * @param grantAccessData
   *          The data of the access.
   */
  public void grantAccess(GrantAccessPopUpData grantAccessData) {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_GRANT_ACCESS).click();
    final InsertAccess insertAccessPopUp = new InsertAccess();
    insertAccessPopUp.insertAccess(grantAccessData);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
  }
}
