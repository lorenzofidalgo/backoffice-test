/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.security.user.classic;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.generalsetup.security.user.UserRolesData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP User tab.
 *
 * @author elopio
 *
 */
public class UserRolesTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "User Roles";
  /** The tab level. */
  private static final int LEVEL = 1;

  /** Identifier of the user roles tab */
  public static final String TAB = "tabname121";

  /** Identifier of the role combo box */
  private static final String COMBO_BOX_ROLE = "reportAD_Role_ID_S";

  /**
   * Class constructor
   *
   */
  public UserRolesTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Create a user role.
   *
   * @param userRolesData
   *          The data of the user role to create.
   */
  public void create(UserRolesData userRolesData) {
    clickCreateNewRecord();
    if (userRolesData.getDataField("role") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_ROLE))
          .selectByVisibleText(userRolesData.getDataField("role").toString());
    }
    clickSave();

  }

}
