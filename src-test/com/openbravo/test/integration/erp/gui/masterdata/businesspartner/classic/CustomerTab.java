/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.businesspartner.classic;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.masterdata.businesspartner.CustomerData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Customer tab.
 *
 * @author elopio
 *
 */
public class CustomerTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Customer";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the Customer tab. */
  public static final String TAB = "tabname223";

  /** Identifier of the customer check box. */
  @SuppressWarnings("unused")
  private static final String CHECK_BOX_CUSTOMER = "IsCustomer";
  /** Identifier of the Invoice Terms combo box. */
  private static final String COMBO_BOX_INVOICE_TERMS = "reportInvoiceRule_S";
  /** Identifier of the Invoice Schedule combo box. */
  @SuppressWarnings("unused")
  private static final String COMBO_BOX_INVOICE_SCHEDULE = "reportC_InvoiceSchedule_ID_S";
  /** Identifier of the Price List combo box. */
  private static final String COMBO_BOX_PRICE_LIST = "reportM_PriceList_ID_S";
  /** Identifier of the Form of Payment combo box. */
  @SuppressWarnings("unused")
  private static final String COMBO_BOX_FORM_OF_PAYMENT = "reportPaymentRule_S";
  /** Identifier of the Payment Terms combo box. */
  private static final String COMBO_BOX_PAYMENT_TERMS = "reportC_PaymentTerm_ID_S";
  /** Identifier of the SO Business Partner Tax Category combo box. */
  @SuppressWarnings("unused")
  private static final String COMBO_BOX_SO_BP_TAX_CATEGORY = "reportSO_Bp_Taxcategory_ID_S";
  /** Identifier of the Sales Representative combo box. */
  @SuppressWarnings("unused")
  private static final String COMBO_BOX_SALES_REPRESENTATIVE = "reportSalesRep_ID_S";
  /** Identifier of the Transactional Bank Account combo box. */
  @SuppressWarnings("unused")
  private static final String COMBO_BOX_TRANSACTIONAL_BANK_ACCOUNT = "reportSO_Bankaccount_ID_S";

  /**
   * Class constructor
   *
   */
  public CustomerTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Edit a Customer.
   *
   * @param data
   *          The edited data of the Customer.
   */
  public void edit(CustomerData data) throws OpenbravoERPTestException {
    switchToFormView();

    if (data.getDataField("priceList") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_PRICE_LIST))
          .selectByVisibleText(data.getDataField("priceList").toString());
    }
    if (data.getDataField("invoiceTerms") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_INVOICE_TERMS))
          .selectByVisibleText(data.getDataField("invoiceTerms").toString());
    }
    if (data.getDataField("paymentTerms") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_PAYMENT_TERMS))
          .selectByVisibleText(data.getDataField("paymentTerms").toString());
    }

    clickSave();
  }
}
