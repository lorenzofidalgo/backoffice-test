/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Rafael Queralta <rafaelcuba81@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.businesspartnersetup.businesspartnercategory;

import com.openbravo.test.integration.erp.data.masterdata.businesspartnersetup.businesspartnercategory.AccountingData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

/**
 * Executes and verifies actions on OpenbravoERP Accounting tab.
 *
 * @author elopio
 *
 */
public class AccountingTab extends GeneratedTab<AccountingData> {

  /** The tab title. */
  static final String TITLE = "Accounting";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "323";
  /** The tab level. */
  private static final int LEVEL = 1;

  /**
   * Class constructor
   *
   */
  public AccountingTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
  }
}
