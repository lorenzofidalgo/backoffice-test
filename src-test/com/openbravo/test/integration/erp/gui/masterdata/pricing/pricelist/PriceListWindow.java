/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelist;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the Price List standard window of OpenbravoERP.
 *
 * @author elopio
 *
 */
public class PriceListWindow extends StandardWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  public static final String TITLE = "Price List";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.MASTER_DATA_MANAGEMENT,
      Menu.PRICING, Menu.PRICE_LIST };

  /**
   * Class constructor.
   *
   */
  public PriceListWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    PriceListTab priceListTab = new PriceListTab();
    addTopLevelTab(priceListTab);
    super.load();
  }

  /**
   * Select and return the Price List tab.
   *
   * @return the Price List tab.
   */
  public PriceListTab selectPriceListTab() {
    return (PriceListTab) selectTab(PriceListTab.IDENTIFIER);
  }

  /**
   * Select and return the Price List Version tab.
   *
   * @return the Price List Version tab.
   */
  public PriceListVersionTab selectPriceListVersionTab() {
    return (PriceListVersionTab) selectTab(PriceListVersionTab.IDENTIFIER);
  }

  /**
   * Select and return the Product Price tab.
   *
   * @return the Product Price tab.
   */
  public ProductPriceTab selectProductPriceTab() {
    return (ProductPriceTab) selectTab(ProductPriceTab.IDENTIFIER);
  }
}
