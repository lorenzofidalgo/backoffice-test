/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2019 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.product.classic;

import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;

/**
 * Executes and verifies actions on OpenbravoERP Product Window Stock tab.
 *
 */
public class ProductStockTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Stock";
  /** The tab level. */
  private static final int LEVEL = 1;

  /** Identifier of the Stock tab. */
  public static final String TAB = "tabname20DD1153AC7448B19B844B25CF38F54C";

  /**
   * Class constructor
   *
   */
  public ProductStockTab() {
    super(TITLE, LEVEL, TAB);
  }
}
