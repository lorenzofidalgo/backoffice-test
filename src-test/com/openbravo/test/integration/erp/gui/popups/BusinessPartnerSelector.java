/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo LujÃ¡n <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.popups;

import com.openbravo.test.integration.selenium.Checkables;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.selenium.Tables;

/**
 * Executes actions on Business Partner selector PopUp.
 *
 * @author elopio
 *
 */
public class BusinessPartnerSelector extends SelectorPopUp {

  /** Identifier of the business partner button. */
  public static final String BUTTON_BUSINESS_PARTNER = "buttonBusinessPartner";

  /** Identifier of the Key text field. */
  private static final String TEXT_FIELD_KEY = "paramKey";
  /** Identifier of the Name text field. */
  private static final String TEXT_FIELD_NAME = "paramName";
  /** Identifier of the type radio button. */
  private static final String RADIO_BUTTON_BUSINESS_PARTNER_TYPE = "inpBpartner";

  /** Index of the key column. */
  private static final int COLUMN_KEY = 1;

  /* Business Partner Type values */
  /** Value of the Customer option on the radio buttons. */
  public static final String VALUE_TYPE_CUSTOMER = "customer";
  /** Value of the Providers option on the radio buttons. */
  public static final String VALUE_TYPE_PROVIDERS = "vendor";
  /** Value of the All option on the radio buttons. */
  public static final String VALUE_TYPE_ALL = "all";

  /**
   * Class constructor.
   *
   */
  public BusinessPartnerSelector() {
    super();
  }

  /**
   * Class constructor.
   *
   * @param popUpName
   *          name of the popup.
   */
  public BusinessPartnerSelector(String popUpName) {
    super();
    POPUP_NAME = popUpName;
  }

  /**
   * Select Account Selector PopUp.
   */
  @Override
  public void selectPopUp() {
    selectPopUp(POPUP_NAME);
  }

  /**
   * Select a business partner.
   *
   * @param key
   *          The key of the business partner.
   * @param type
   *          The type of customer.
   */
  public void selectBusinessPartner(String key, String type) {
    selectPopUp();

    SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_KEY).sendKeys(key);
    Checkables.check(RADIO_BUTTON_BUSINESS_PARTNER_TYPE, type);
    clickSearch();
    Tables.clickCellWithText(TABLE_RESULTS, COLUMN_KEY, key);

    SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK).click();
    Sleep.trySleep(3000);
  }

  /**
   * Select a business partner.
   *
   * @param key
   *          The key of the business partner.
   */
  public void selectBusinessPartner(String key) {
    selectPopUp();

    SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_KEY).sendKeys(key);
    SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME).sendKeys("");
    clickSearch();
    Tables.clickCellWithText(TABLE_RESULTS, COLUMN_KEY, key);

    SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK).click();
    Sleep.trySleep(3000);
  }
}
