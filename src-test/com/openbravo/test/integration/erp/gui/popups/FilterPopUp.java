/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.popups;

import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on filter PopUp
 *
 * @author elopio
 *
 */
public class FilterPopUp extends PopUp {

  // TODO Unnecessary @supresswarnings
  // TODO see issue 11537 - https://issues.openbravo.com/view.php?id=11537
  /** Name of the PopUp FormWindow */
  @SuppressWarnings("hiding")
  protected static final String POPUP_NAME = "BUSCADOR";

  /** Identifier of the name text field */
  protected static final String TEXT_FIELD_NAME = "inpParamName";

  /**
   * Class constructor
   */
  public FilterPopUp() {
    super();
  }

  /**
   * Select a filter PopUp.
   */
  @Override
  public void selectPopUp() {
    selectPopUp(POPUP_NAME, BUTTON_OK);
  }

  /**
   * Filter records without parameters, i.e, show all records.
   */
  public void filter() {
    selectPopUp(POPUP_NAME, BUTTON_OK);
    clickOK();
    // XXX removed the wait for page to load because sometimes is not
    // required. It will be a task of the following action to wait for data
    // to be fully loaded.
  }

  /**
   * Filter records by name
   *
   * @param name
   *          the name of the record
   */
  public void filterByName(String name) {
    selectPopUp(POPUP_NAME, BUTTON_OK);
    SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME).sendKeys(name);
    clickOK();
  }

  /**
   * Click the ok button.
   */
  @Override
  public void clickOK() {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK).click();
  }
}
