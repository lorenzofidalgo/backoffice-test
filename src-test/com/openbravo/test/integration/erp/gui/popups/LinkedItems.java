/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.popups;

import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on Openbravo ERP Linked Items pop up
 *
 * @author elopio
 *
 */
public class LinkedItems extends PopUp {

  // TODO: Unnecessary @supresswarnings
  /** Name of the PopUp. */
  @SuppressWarnings("hiding")
  private static final String POPUP_NAME = "LINKS";

  /** Identifier of the cancel button. */
  private static final String BUTTON_CANCEL = "buttonCancel";

  /** Locator of the focused link. */
  private static final String LINK_FOCUSED = "xpath=(//a[@class='LabelLink _LabelLink LabelLink_focus'])";

  /**
   * Class constructor.
   *
   */
  public LinkedItems() {
    super();
  }

  /**
   * Select Product Filter PopUp.
   */
  @Override
  public void selectPopUp() {
    selectPopUp(POPUP_NAME, BUTTON_CANCEL);
  }

  /**
   * Go to the first linked screen.
   */
  public void goToFirstLink() {
    selectPopUp();
    SeleniumSingleton.INSTANCE.findElementById(LINK_FOCUSED).click();
    SeleniumSingleton.INSTANCE.findElementById(LINK_FOCUSED).click();
  }

  /**
   * Go to a linked screen.
   *
   * @param link
   *          The locator of the link.
   */
  public void goToLink(String link) {
    selectPopUp();
    SeleniumSingleton.INSTANCE.findElementById(link).click();
    SeleniumSingleton.INSTANCE.findElementById(LINK_FOCUSED).click();
  }
}
