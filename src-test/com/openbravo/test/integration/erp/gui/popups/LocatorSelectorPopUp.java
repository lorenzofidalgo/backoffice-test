/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 * 	Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.popups;

import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Tables;

/**
 * Executes actions on locator selector PopUp
 *
 * @author elopio
 *
 */
public class LocatorSelectorPopUp extends SelectorPopUp {

  /** Identifier of the locator button */
  public static final String BUTTON_LOCATOR = "buttonLocator";

  /** Identifier of the alias text field */
  private static final String TEXT_FIELD_ALIAS = "inpKey";

  /** Index of the alias column */
  private static final int COLUMN_ALIAS = 2;

  /**
   * Class constructor
   */
  public LocatorSelectorPopUp() {
    super();
  }

  /**
   * Select a locator
   *
   * @param alias
   *          locator alias
   */
  public void selectLocator(String alias) {
    selectPopUp();

    SeleniumSingleton.INSTANCE.findElementByName(TEXT_FIELD_ALIAS).sendKeys(alias);

    // FIXME: 2011-03-26-PLU - The Warehouse value is filled when should be not
    SeleniumSingleton.INSTANCE.findElementById("paramWarehouse").clear();
    SeleniumSingleton.INSTANCE.findElementById("paramWarehouse").sendKeys("%");

    clickSearch();
    Tables.clickCellWithText(TABLE_RESULTS, COLUMN_ALIAS, alias);
    clickOK();
  }
}
