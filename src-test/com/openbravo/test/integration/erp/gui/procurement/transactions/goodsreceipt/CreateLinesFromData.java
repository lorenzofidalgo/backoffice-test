/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2019 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.procurement.transactions.goodsreceipt;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 * Class for {@link CreateLinesFromPopUp}
 *
 * @author collazoandy4
 *
 */
public class CreateLinesFromData extends DataObject {

  /**
   * Class builder
   *
   * @author collazoandy4
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the warehouse value
     *
     * Description: The location where products arrive to or are sent from.
     *
     * @param value
     *          The warehouse value.
     * @return The builder for this class.
     */
    public Builder warehouse(String value) {
      this.dataFields.put("warehouse", value);
      return this;
    }

    /**
     * Set the attribute value
     *
     * Description: The attribute value to be set in the PopUp
     *
     * @param value
     *          The attribute value.
     * @return The builder for this class.
     */
    public Builder attribute(String value) {
      this.dataFields.put("attribute", value);
      return this;
    }

    /**
     * Set the purchaseOrder value
     *
     * Description: A unique and often automatically generated identifier for a purchase order.
     *
     * @param value
     *          The purchaseOrder value.
     * @return The builder for this class.
     */
    public Builder purchaseOrder(String value) {
      this.dataFields.put("purchaseOrder", value);
      return this;
    }

    /**
     * Set the operative quantity value
     *
     * @param value
     *          The Operative Quantity
     * @return The builder for this class.
     */
    public Builder operativeQuantity(String value) {
      this.dataFields.put("operativeQuantity", value);
      return this;
    }

    /**
     * Set the orderedQuantity value
     *
     * Description: The number of an item involved in a transaction given in standard units. It is
     * used to determine price per unit.
     *
     * @param value
     *          The orderedQuantity value.
     * @return The builder for this class.
     */
    public Builder orderedQuantity(String value) {
      this.dataFields.put("orderedQuantity", value);
      return this;
    }

    /**
     * Set the lineNumber value
     *
     * Description: The line number to select
     *
     * @param value
     *          The line number value.
     * @return The builder for this class.
     */
    public Builder lineNumber(int value) {
      this.dataFields.put("lineNumber", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public CreateLinesFromData build() {
      return new CreateLinesFromData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private CreateLinesFromData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
