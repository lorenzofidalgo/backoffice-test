/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.procurement.transactions.goodsreceipt;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the Price List standard window of OpenbravoERP.
 *
 * @author plujan
 *
 */
public class GoodsReceiptWindow extends StandardWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  public static final String TITLE = "Goods Receipt";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.PROCUREMENT_MANAGEMENT,
      Menu.PROCUREMENT_MANAGEMENT_TRANSACTIONS, Menu.GOODS_RECEIPT };

  /**
   * Class constructor.
   *
   */
  public GoodsReceiptWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    final GoodsReceiptHeaderTab goodsReceiptTab = new GoodsReceiptHeaderTab();
    addTopLevelTab(goodsReceiptTab);
    super.load();
  }

  /**
   * Select and return the Price List tab.
   *
   * @return the Price List tab.
   */
  public GoodsReceiptHeaderTab selectGoodsReceiptHeaderTab() {
    return (GoodsReceiptHeaderTab) selectTab(GoodsReceiptHeaderTab.IDENTIFIER);
  }

  /**
   * Select and return the Price List Version tab.
   *
   * @return the Price List Version tab.
   */
  public GoodsReceiptLinesTab selectGoodsReceiptLinesTab() {
    return (GoodsReceiptLinesTab) selectTab(GoodsReceiptLinesTab.IDENTIFIER);
  }
}
