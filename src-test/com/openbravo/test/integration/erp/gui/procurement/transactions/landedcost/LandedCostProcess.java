/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014-2019 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.procurement.transactions.landedcost;

import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBParameterWindow;
import com.openbravo.test.integration.selenium.Sleep;

import junit.framework.TestCase;

public class LandedCostProcess extends OBParameterWindow<LandedCostHeaderTab> {

  private static final String ADD_PAYMENT_PROCESS_ID = "96FE01F2F12F45FC8ED4A1978EBD034C";

  public LandedCostProcess() {
    this(null, ADD_PAYMENT_PROCESS_ID);
  }

  public LandedCostProcess(GeneratedTab<?> tab) {
    this(tab, ADD_PAYMENT_PROCESS_ID);
  }

  public LandedCostProcess(String processDefinitionId) {
    this(null, processDefinitionId);
  }

  public LandedCostProcess(GeneratedTab<?> tab, String processDefinitionId) {
    super(ADD_PAYMENT_PROCESS_ID, tab);
  }

  @Override
  protected boolean isPopup() {
    return true;
  }

  public void process() {
    scrollToBottom();
    if (isOkButtonEnabled()) {
      pressOkButton();
    } else {
      // Wait for all displaylogic/onChanges to be executed
      // TODO: smarter wait
      for (int i = 0; i < 20 && !isOkButtonEnabled(); i++) {
        Sleep.trySleep(500);
      }
      if (isOkButtonEnabled()) {
        pressOkButton();
      } else {
        TestCase.fail("DONE button is not enabled");
      }
    }
    if (tab != null) {
      tab.waitUntilMessageVisible();
    }
  }

}
