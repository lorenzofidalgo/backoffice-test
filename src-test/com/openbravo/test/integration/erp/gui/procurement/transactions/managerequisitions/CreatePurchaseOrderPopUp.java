/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.procurement.transactions.managerequisitions;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on user Create Purchase Order Pop Up.
 *
 * @author elopio
 *
 */
public class CreatePurchaseOrderPopUp extends OBClassicPopUp {

  /* Component identifiers. */
  /** Identifier of the order date text field. */
  private static final String IDENTIFIER_TEXT_FIELD_ORDER_DATE = "DateOrdered";
  /** Identifier of the organization combobox. */
  private static final String IDENTIFIER_COMBO_BOX_ORGANIZATION = "reportAD_Org_ID_S";
  /** Identifier of the warehouse combobox. */
  private static final String IDENTIFIER_COMBO_BOX_WAREHOUSE = "reportM_Warehouse_ID_S";

  /**
   * Class constructor.
   *
   */
  public CreatePurchaseOrderPopUp() {
    super(TestRegistry
        .getObjectString("org.openbravo.classicpopup./ManageRequisitions/Header_Edition.html"));
  }

  /**
   * Create a purchase order.
   *
   * @param orderDate
   *          The date of the order.
   * @param organization
   *          The name of the organization.
   * @param warehouse
   *          The name of the warehouse.
   */
  public void createPurchaseOrder(String orderDate, String organization, String warehouse) {
    selectPopUpWithOKButton();
    SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_TEXT_FIELD_ORDER_DATE)
        .sendKeys(orderDate);
    new Select(SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_COMBO_BOX_ORGANIZATION))
        .selectByVisibleText(organization);
    new Select(SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_COMBO_BOX_WAREHOUSE))
        .selectByVisibleText(warehouse);
    clickOK();
  }
}
