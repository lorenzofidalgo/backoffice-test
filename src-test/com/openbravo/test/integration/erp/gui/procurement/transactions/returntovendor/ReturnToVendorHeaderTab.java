/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <f.carballo@nectus.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.gui.procurement.transactions.returntovendor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorHeaderData;
import com.openbravo.test.integration.erp.data.selectors.ReturnToVendorLineSelectorData;
import com.openbravo.test.integration.erp.gui.popups.RequestProcessPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteRTVRFCWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 *
 * Class for Return to Vendor Header Tab
 *
 * @author Nono Carballo
 *
 */
public class ReturnToVendorHeaderTab extends GeneratedTab<ReturnToVendorHeaderData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Header";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "5A5CCFC8359B4D79BA705DC487FE8173";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /** Buttons */
  private static final String REGISTRY_KEY_BUTTON_COMPLETE = "org.openbravo.client.application.toolbar.button.documentAction.5A5CCFC8359B4D79BA705DC487FE8173";
  private static final String REGISTRY_KEY_POP_UP_REQUEST_PROCESS = "org.openbravo.classicpopup./ReturntoVendor/Header_Edition.html";
  private static final String REGISTRY_KEY_BUTTON_PICKANDEDIT = "org.openbravo.client.application.toolbar.button.pickfromreceipt.5A5CCFC8359B4D79BA705DC487FE8173";
  private static final String REGISTRY_KEY_POPUP_PICKANDEDIT = "org.openbravo.client.application.process.pickandexecute.popup";

  private final Button pickEditLines;
  private static Button buttonComplete;

  public ReturnToVendorHeaderTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new ReturnToVendorLinesTab(this));
    addChildTab(new PaymentOutPlanTab(this));
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
    pickEditLines = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_PICKANDEDIT));
  }

  public PickAndExecuteWindow<ReturnToVendorLineSelectorData> openPickAndEditLines() {
    pickEditLines.click();
    Sleep.trySleepWithMessage(5000,
        "Sleeping after clicking in Pick/Edit Lines button to avoid performance problems");
    // TODO: RFC/RTV Pick & Edit popup objectString doesn't match with the generic one, a new
    // strategy is needed to be able to obtain the id for popup grids (using custom
    // PickAndExecuteWindow constructor)
    // This is due to a recent functionality change, as explained in the note of following
    // issue: https://issues.openbravo.com/view.php?id=38718#c105005
    PickAndExecuteWindow<ReturnToVendorLineSelectorData> popup = new PickAndExecuteRTVRFCWindow<ReturnToVendorLineSelectorData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    return popup;
  }

  private void process(String action) {
    logger.debug("Process return to vendor order header with action '{}'.", action);
    buttonComplete.click();
    RequestProcessPopUp popUp = new RequestProcessPopUp(REGISTRY_KEY_POP_UP_REQUEST_PROCESS);
    popUp.process(action);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  public void book() {
    process("Book");
  }

}
