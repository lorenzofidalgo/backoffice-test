/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2012-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   David Miguélez <david.miguelez@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.sales.transactions.returnfromcustomer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.sales.transactions.returnfromcustomer.ReturnFromCustomerHeaderData;
import com.openbravo.test.integration.erp.data.selectors.ReturnFromCustomerLineSelectorData;
import com.openbravo.test.integration.erp.gui.popups.RequestProcessPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteRTVRFCWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Return From Customer Header tab.
 *
 * @author David Miguélez
 *
 */
public class ReturnFromCustomerHeaderTab extends GeneratedTab<ReturnFromCustomerHeaderData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Header";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "AF4090093CFF1431E040007F010048A5";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /** Registry key of the complete button. */
  private static final String REGISTRY_KEY_BUTTON_COMPLETE = "org.openbravo.client.application.toolbar.button.documentAction.AF4090093CFF1431E040007F010048A5";
  /** Registry key of the post button. */
  @SuppressWarnings("unused")
  private static final String REGISTRY_KEY_BUTTON_POST = "org.openbravo.client.application.toolbar.button.posted.AF4090093CFF1431E040007F010048A5";
  /** Registry key of the Pick and Edit Lines Button */
  private static final String REGISTRY_KEY_BUTTON_PICKANDEDIT = "org.openbravo.client.application.toolbar.button.pickFromShipment.AF4090093CFF1431E040007F010048A5";
  /** Registry key of the Pick and Edit Lines Pop Up */
  private static final String REGISTRY_KEY_POPUP_PICKANDEDIT = "org.openbravo.client.application.process.pickandexecute.popup";

  /** Identifier of the delete accounting entry checkbox */
  protected static final String IDENTIFIER_CHECKBOX_DELETE = "inpEliminar";

  /* Toolbar buttons. */
  /** The complete button. */
  private static Button buttonComplete;
  private static Button buttonPickAndEdit;

  /**
   * Class constructor.
   *
   */
  public ReturnFromCustomerHeaderTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new LinesTab(this));
    addChildTab(new PaymentInPlanTab(this));
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
    buttonPickAndEdit = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_PICKANDEDIT));
  }

  /**
   * Process the order.
   *
   * @param action
   *          The action to process.
   */
  private void process(String action) {
    logger.debug("Process return from customer header with action '{}'.", action);
    // XXX workaround issue https://issues.openbravo.com/view.php?id=17409
    buttonComplete.smartClientClick();
    RequestProcessPopUp popUp = new RequestProcessPopUp(
        "org.openbravo.classicpopup./ReturnfromCustomer/Header_Edition.html");
    popUp.process(action);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Complete the order.
   *
   */
  public void book() {
    process("Book");
  }

  /**
   * Pick and Edit Lines.
   *
   * @param order
   *          The order data. This is a string with the form: number - date - amount.
   */
  public void pickAndEditLines(String order) {
    logger.debug("Pick And Edit from order '{}'.", order);
    buttonPickAndEdit.click();
    // TODO: RFC/RTV Pick & Edit popup objectString doesn't match with the generic one, a new
    // strategy is needed to be able to obtain the id for popup grids (using custom
    // PickAndExecuteWindow constructor)
    // This is due to a recent functionality change, as explained in the note of following
    // issue: https://issues.openbravo.com/view.php?id=38718#c105005
    PickAndExecuteWindow<ReturnFromCustomerHeaderData> popUp = new PickAndExecuteRTVRFCWindow<ReturnFromCustomerHeaderData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    popUp.cancel();
    // CreateLinesFromPopUp popUp = new CreateLinesFromPopUp();
    // popUp.createLinesFrom(order);
    // SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    // waitUntilMessageVisible();
    // buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
  }

  public PickAndExecuteWindow<ReturnFromCustomerLineSelectorData> openPickAndEditLines() {
    buttonPickAndEdit.click();
    PickAndExecuteWindow<ReturnFromCustomerLineSelectorData> popup = new PickAndExecuteRTVRFCWindow<ReturnFromCustomerLineSelectorData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    return popup;
  }
}
