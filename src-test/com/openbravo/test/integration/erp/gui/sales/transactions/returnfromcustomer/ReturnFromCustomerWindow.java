/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2012-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  David Miguélez <david.miguelez@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.sales.transactions.returnfromcustomer;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the Return From Customer window of OpenbravoERP.
 *
 * @author David Miguélez
 *
 */
public class ReturnFromCustomerWindow extends StandardWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  public static final String TITLE = "Return from Customer";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.SALES_MANAGEMENT,
      Menu.SALES_MANAGEMENT_TRANSACTIONS, Menu.RETURN_FROM_CUSTOMER };

  /**
   * Class constructor.
   */
  public ReturnFromCustomerWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    ReturnFromCustomerHeaderTab headerTab = new ReturnFromCustomerHeaderTab();
    addTopLevelTab(headerTab);
    super.load();
  }

  /**
   * Select and return the Header tab.
   *
   * @return the Header tab.
   */
  public ReturnFromCustomerHeaderTab selectHeaderTab() {
    return (ReturnFromCustomerHeaderTab) selectTab(ReturnFromCustomerHeaderTab.IDENTIFIER);
  }

  /**
   * Select and return the Lines tab.
   *
   * @return the Lines tab.
   */
  public LinesTab selectLinesTab() {
    return (LinesTab) selectTab(LinesTab.IDENTIFIER);
  }

}
