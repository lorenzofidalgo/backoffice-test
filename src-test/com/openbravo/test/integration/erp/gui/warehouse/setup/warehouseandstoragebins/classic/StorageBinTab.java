/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.warehouse.setup.warehouseandstoragebins.classic;

import com.openbravo.test.integration.erp.data.warehouse.setup.warehouseandstoragebins.StorageBinData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Storage Bin tab.
 *
 * @author elopio
 *
 */
public class StorageBinTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Storage Bin";
  /** The tab level. */
  private static final int LEVEL = 1;

  /** Identifier of the storage bin tab */
  public static final String TAB = "tabname178";

  /** Identifier of the search key text field */
  private static final String TEXT_FIELD_SEARCH_KEY = "Value";
  /** Identifier of the row text field */
  private static final String TEXT_FIELD_ROW = "X";
  /** Identifier of the stack text field */
  private static final String TEXT_FIELD_STACK = "Y";
  /** Identifier of the level text field */
  private static final String TEXT_FIELD_LEVEL = "Z";

  /**
   * Class constructor
   *
   */
  public StorageBinTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Create a Storage Bin.
   *
   * @param data
   *          The data of the Storage Bin that will be created.
   */
  public void create(StorageBinData data) {
    clickCreateNewRecord();
    if (data.getDataField("searchKey") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_SEARCH_KEY)
          .sendKeys(data.getDataField("searchKey").toString());
    }
    if (data.getDataField("rowX") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_ROW)
          .sendKeys(data.getDataField("rowX").toString());
    }
    if (data.getDataField("stackY") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_STACK)
          .sendKeys(data.getDataField("stackY").toString());
    }
    if (data.getDataField("levelZ") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_LEVEL)
          .sendKeys(data.getDataField("levelZ").toString());
    }
    clickSave();

  }
}
