/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.warehouse.setup.warehouseandstoragebins.classic;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.selectors.LocationSelectorData;
import com.openbravo.test.integration.erp.data.warehouse.setup.warehouseandstoragebins.WarehouseData;
import com.openbravo.test.integration.erp.gui.popups.AddressSelectorPopUp;
import com.openbravo.test.integration.erp.gui.popups.AddressSelectorPopUpSc;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Warehouse tab.
 *
 * @author elopio
 *
 */
public class WarehouseTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Warehouse";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the tab. */
  public static final String TAB = "tabname177";

  /** Identifier of the organization combo box. */
  private static final String COMBO_BOX_ORGANIZATION = "reportAD_Org_ID_S";
  /** Identifier of the search key text field. */
  private static final String TEXT_FIELD_SEARCH_KEY = "Value";
  /** Identifier of the name text field. */
  private static final String TEXT_FIELD_NAME = "Name";
  /** Identifier of the storage bin separator text field. */
  @SuppressWarnings("unused")
  private static final String TEXT_FIELD_STORAGE_BIN_SEPARATOR = "Separator";

  /**
   * Class constructor
   *
   */
  public WarehouseTab() {
    super(TITLE, LEVEL, TAB);
    addSubTab(new StorageBinTab());
  }

  /**
   * Create a Warehouse.
   *
   * @param data
   *          The data of the Warehouse that will be created.
   */
  public void create(WarehouseData data) {
    clickCreateNewRecord();
    if (data.getDataField("organization") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_ORGANIZATION))
          .selectByVisibleText(data.getDataField("organization").toString());
      waitForDataToLoad();
    }
    if (data.getDataField("searchKey") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_SEARCH_KEY)
          .sendKeys(data.getDataField("searchKey").toString());
    }
    if (data.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(data.getDataField("name").toString());
    }
    if (data.getDataField("locationAddress") != null) {
      SeleniumSingleton.INSTANCE.findElementById(AddressSelectorPopUp.BUTTON_ADDRESS).click();
      String windowHandle = SeleniumSingleton.INSTANCE.getWindowHandle();
      final AddressSelectorPopUpSc addressSelector = new AddressSelectorPopUpSc();
      addressSelector.selectAddress((LocationSelectorData) data.getDataField("locationAddress"));
      SeleniumSingleton.INSTANCE.switchTo().window(windowHandle);
      waitForFrame();
    }
    Sleep.trySleep(1000);
    clickSave();
  }
}
