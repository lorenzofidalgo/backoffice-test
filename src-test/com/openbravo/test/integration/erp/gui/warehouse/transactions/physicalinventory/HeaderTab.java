/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.warehouse.transactions.physicalinventory;

import org.openqa.selenium.TimeoutException;

import com.openbravo.test.integration.erp.data.warehouse.transactions.physicalinventory.CreateInventoryCountListData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.physicalinventory.PhysicalInventoryHeaderData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Physical Inventory Header tab.
 *
 * @author elopio
 *
 */
public class HeaderTab extends GeneratedTab<PhysicalInventoryHeaderData> {

  /** The tab title. */
  static final String TITLE = "Header";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "255";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /**
   * Format string used to check the message displayed after creating inventory count list.
   */
  @SuppressWarnings("unused")
  private static final String FORMAT_MESSAGE_CREATE_INVENTORY_COUNT_LIST = "Inserted=%d, Updated=%d";

  /**
   * Class constructor
   *
   */
  public HeaderTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new LinesTab(this));
  }

  /**
   * Create the inventory count list.
   *
   * @param data
   *          The data of the Inventory Count to create.
   */
  public void createInventoryCountList(CreateInventoryCountListData data) {
    Button buttonCreateInventoryCountList = new Button(TestRegistry
        .getObjectString("org.openbravo.client.application.toolbar.button.generateList.255"));
    buttonCreateInventoryCountList.click();
    CreateInventoryCountListPopUp createInventoryCountListPopUp = new CreateInventoryCountListPopUp();
    createInventoryCountListPopUp.create(data);
    SeleniumSingleton.INSTANCE.switchTo().window("");
    try {
      waitUntilMessageVisible();
    } catch (TimeoutException toe) {
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      SeleniumSingleton.INSTANCE.switchTo().window("");
      waitUntilMessageVisible();
    }
  }

  /**
   * Verify that the process completed successfully.
   *
   * @param inserted
   *          The number of inserted records.
   * @param updated
   *          The number of updated records.
   */
  public void verifyProcessCompletedSuccessfully(int inserted, int updated) {
    // TODO see issue 17291 - https://issues.openbravo.com/view.php?id=17291.
    // assertProcessCompletedSuccessfully(String.format(FORMAT_MESSAGE_CREATE_INVENTORY_COUNT_LIST,
    // inserted, updated));
  }

  /**
   * Process inventory count.
   */
  public void process() throws OpenbravoERPTestException {
    Button buttonProcessInventoryCount = new Button(TestRegistry
        .getObjectString("org.openbravo.client.application.toolbar.button.processNow.255"));
    buttonProcessInventoryCount.click();

    OBClassicPopUp processInventoryCountPopUp = new OBClassicPopUp(TestRegistry
        .getObjectString("org.openbravo.classicpopup./PhysicalInventory/Header_Edition.html"));
    processInventoryCountPopUp.selectPopUpWithOKButton();
    processInventoryCountPopUp.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().window("");
    try {
      waitUntilMessageVisible();
    } catch (TimeoutException toe) {
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      // TODO L2: Remove the following static sleep once it is stable
      Sleep.trySleep();
      SeleniumSingleton.INSTANCE.switchTo().window("");
      // TODO L2: Remove the following static sleep once it is stable
      Sleep.trySleep();
      waitUntilMessageVisible();
    }
  }

}
