/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SL
 * All portions are Copyright (C) 2011 Openbravo SL
 * All Rights Reserved.
 * Contributor(s):
 *   Juan Pablo Aroztegi <juanpablo.aroztegi@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.ci;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.TimeoutException;

import com.openbravo.test.integration.erp.common.TimeoutConstants;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.generalsetup.ModuleReferenceData;
import com.openbravo.test.integration.erp.gui.LoginPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.modulemanagement.ModuleManagement;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Install modules using the Module Management Console.
 */
@RunWith(Parameterized.class)
public class IntRetailSampledata extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  /** Data of the modules to install. */
  private ModuleReferenceData demo;

  /**
   * Class constructor.
   *
   * @param demo
   */
  public IntRetailSampledata(ModuleReferenceData demo) {
    this.demo = demo;
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<Object[]> modulesList() throws IOException {
    return Arrays.asList(
        new Object[][] { { new ModuleReferenceData.Builder().name("Retail Sampledata").build() } });
  }

  /**
   * Test the setup client and organization flow.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void installModules() throws IOException, OpenbravoERPTestException {
    ModuleManagement.Settings.setStatus(mainPage,
        ConfigurationProperties.INSTANCE.getMaturityStatus(),
        ConfigurationProperties.INSTANCE.getMaturityStatus());

    logger.info("** Start of test case int-modules **");
    ModuleManagement.AddModules.installModuleFromCentralRepository(mainPage, demo);
    ModuleManagement.InstalledModules.rebuildSystemAndRestartServletContainer(mainPage);
    final LoginPage loginPage = new LoginPage();
    try {
      loginPage.waitForFieldsToAppear(TimeoutConstants.RESTART_SERVLET_CONTAINER_TIMEOUT);
    } catch (TimeoutException exception) {
      logger.warn("Timed out while waiting for the servlet container to restart. Exception: {}",
          exception);
      logger.info("Try to open the login page.");
      loginPage.open();
    }
    loginPage.verifyLogout();
    logger.info("** End of test case int-modules **");
  }

  /**
   * Logout from Openbravo.
   */
  @Override
  public void logout() {
    // Do nothing. The rebuild will logout the user.
  }
}
