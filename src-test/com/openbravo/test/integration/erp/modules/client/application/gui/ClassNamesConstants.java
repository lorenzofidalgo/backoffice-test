/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

/**
 * Constants for Openbravo's smartclient object class names.
 *
 * @author elopio
 *
 */
public class ClassNamesConstants {

  /* Input field classes. */
  /** Name of the text item class. */
  public static final String CLASS_TEXT_ITEM = "OBTextItem";
  /** Name of the foreign key class. */
  public static final String CLASS_FOREIGN_KEY_ITEM = "OBFKItem";
  /** TODO: Description missing */
  public static final String CLASS_FOREIGN_KEY_COMBO_ITEM = "OBFKComboItem";
  /** Name of the date item class. */
  public static final String CLASS_DATE_ITEM = "OBDateItem";
  /** Name of the list item class. */
  public static final String CLASS_LIST_ITEM = "OBListItem";
  /** Name of the number item class. */
  public static final String CLASS_NUMBER_ITEM = "OBNumberItem";
  /** Name of the encrypted item class. */
  public static final String CLASS_ENCRYPTED_ITEM = "OBEncryptedItem";
  /** Name of the check box item class. */
  public static final String CLASS_CHECK_BOX_ITEM = "OBCheckboxItem";
  /** Name of the search item class. */
  public static final String CLASS_SEARCH_ITEM = "OBSearchItem";
  /** Name of the selector item class. */
  public static final String CLASS_SELECTOR_ITEM = "OBSelectorItem";
  /** Name of the attribute search item class. */
  public static final String CLASS_SEARCH_ITEM_ATTRIBUTE = "OBPAttributeSearchItem";
  /** Name of the text area item class. */
  public static final String CLASS_TEXT_AREA_ITEM = "OBTextAreaItem";
  /** Name of the section item class. */
  public static final String CLASS_SECTION_ITEM = "OBSectionItem";
  /** Name of the hidden item class. */
  public static final String CLASS_HIDDEN_ITEM = "HiddenItem";
  /** Name of the spacer item class. */
  public static final String CLASS_SPACER_ITEM = "SpacerItem";
  /** Name of the pop up text area item. */
  public static final String CLASS_POP_UP_TEXT_AREA_ITEM = "OBPopUpTextAreaItem";
  /** Name of the canvas item class. */
  public static final String CLASS_CANVAS_ITEM = "OBClientClassCanvasItem";
}
