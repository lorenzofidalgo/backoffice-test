/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 * 	Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Class that represents a Classic Process View on Openbravo ERP
 *
 * @author elopio
 *
 */
public class ClassicProcessView extends ClassicView {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Identifier of the ok button */
  protected static final String BUTTON_OK = "buttonOK";

  /**
   * Class constructor
   *
   * @param title
   *          The title of the view.
   * @param menuPath
   *          The path to open the view from the menu.
   */
  public ClassicProcessView(String title, String... menuPath) {
    super(title, menuPath);
  }

  /**
   * Class constructor.
   *
   * @param title
   *          The title of the view.
   */
  public ClassicProcessView(String title) {
    super(title);
  }

  /**
   * Click OK button and wait for the page to load
   */
  protected void clickOK() {
    logger.debug("Clicking the OK button.");
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK).click();
  }
}
