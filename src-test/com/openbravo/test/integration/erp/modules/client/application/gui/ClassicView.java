/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.gui.MessageBox;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verify actions on an OpenbravoERP classic view (the default of 2.50 version).
 *
 * @author elopio
 *
 */
public abstract class ClassicView extends View {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Identifier of the application frame. */
  public static final String FRAME_APPLICATION = "appFrame";
  /** Identifier of the back button. */
  protected static final String BUTTON_BACK = "buttonBack";
  /** Identifier of the refresh button. */
  protected static final String BUTTON_REFRESH = "linkButtonRefresh";

  /** Locator of the selected tab. */
  private static final String TAB_SELECTED = "//span[contains(@class, 'dojoTabcurrent') or contains(@class, 'dojoTabCurrent')]";

  /**
   * Class constructor
   *
   * @param title
   *          The title of the view.
   */
  public ClassicView(String title) {
    super(title);
  }

  /**
   * Class constructor.
   *
   * @param title
   *          The title of the view.
   * @param menuPath
   *          The path to open the view from the menu.
   */
  public ClassicView(String title, String... menuPath) {
    super(title, menuPath);
  }

  /**
   * Load the view.
   */
  @Override
  public void load() {
    waitForFrame();
    waitUntilViewIsVisible();
  }

  /**
   * Wait until the view is visible.
   */
  public void waitUntilViewIsVisible() {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting until the view is visible.");
        try {
          // Changed to bypass current error of Refresh button not shown
          // SeleniumSingleton.INSTANCE.findElementById(BUTTON_REFRESH);
          SeleniumSingleton.INSTANCE.findElementById("tdToolBar");
        } catch (NoSuchElementException exception) {
          // Commented out since it is not useful
          // logger.debug("Dump of current page: " + SeleniumSingleton.INSTANCE.getPageSource()
          // + " Dump end.");

          return false;
        }
        return true;
      }
    });
  }

  /**
   * Select the classic frame.
   */
  public void waitForFrame() {
    logger.trace("Waiting for the classic frame to load.");
    SeleniumSingleton.WAIT.until(wd -> {
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();

      // Looking in all the top frames those one containing a frame name appFrame. This is much
      // faster directly in JavaSript than switching frames through Selenium.

      // @formatter:off
      Long idx = (Long)SeleniumSingleton.INSTANCE.executeScriptWithReturn(
            "(function (frameName) {\n"
          + "  for (let i = 0; i < frames.length; i++) {\n"
          + "    try {\n"
          + "      let f = frames[i]\n"
          + "      if (f && f[frameName]) {\n"
          + "        return i\n"
          + "      }\n"
          + "    } catch {}\n"
          + "  }\n"
          + "  return -1\n"
          + "})('" + FRAME_APPLICATION + "')");
      // @formatter:on

      logger.trace("Classic frame index {}", idx);

      if (idx.equals(-1L)) {
        logger.trace("Frame not found in JS");
        return false;
      }

      try {
        SeleniumSingleton.INSTANCE.switchTo().frame(idx.intValue());
        SeleniumSingleton.INSTANCE.switchTo().frame(FRAME_APPLICATION);
        return true;
      } catch (NoSuchFrameException exception) {
        return false;
      }
    });
  }

  /**
   * Click the back button.
   */
  public void clickBack() {
    logger.debug("Clicking the Back button.");
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_BACK).click();
  }

  /**
   * Click the refresh button.
   */
  public void clickRefresh() {
    logger.debug("Clicking the Refresh button.");
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_REFRESH).click();
  }

  /**
   * Verify that the process completed successfully.
   */
  public void verifyProcessCompletedSuccessfully() {
    logger.debug("Verifying that the process was completed successfully.");
    final MessageBox messageBox = new MessageBox();
    messageBox.verify(MessageBox.TITLE_PROCESS_COMPLETED_SUCCESSFULLY, MessageBox.TITLE_EMPTY);
  }

  /**
   * Verify that the process completed successfully This is used when the message is displayed in
   * the message field, not in the title. TODO See bug 8248 -
   * https://issues.openbravo.com/view.php?id=8248
   */
  public void verifyProcessCompletedSuccessfully2() {
    logger.debug("Verifying that the process was completed successfully.");
    final MessageBox messageBox = new MessageBox();
    messageBox.verify(MessageBox.TITLE_EMPTY, MessageBox.TITLE_PROCESS_COMPLETED_SUCCESSFULLY);
  }

  /**
   * Verify that the process completed successfully.
   *
   * @param message
   *          The message to verify.
   */
  public void verifyProcessCompletedSuccessfully(String message) {
    logger.debug("Verifying that the process was completed successfully.");
    final MessageBox messageBox = new MessageBox();
    messageBox.verify(MessageBox.TITLE_PROCESS_COMPLETED_SUCCESSFULLY, message);
  }

  /**
   * Get the text of the current selected tab
   *
   * @return The text of the current selected tab
   */
  public String getCurrentTabText() {
    String text = "";
    text = SeleniumSingleton.INSTANCE.findElementByXPath(TAB_SELECTED).getText();
    return text;
  }
}
