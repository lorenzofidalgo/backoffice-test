/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.gui.tabs.Tab;

/**
 * Executes and verify actions on an OpenbravoERP classic window view.
 *
 * @author elopio
 *
 */
public abstract class ClassicWindowView extends ClassicView {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Locator of the top level tab when it's the selected tab */
  @SuppressWarnings("unused")
  private static final String TOP_LEVEL_TAB_SELECTED = "xpath=(//span[@class='dojoTabcurrentfirst' or @class='dojoTabcurrentfirst_focus'])";
  /** Locator of the selected tab, when it's not the top level */
  // TODO does this work with a read only subtab?
  @SuppressWarnings("unused")
  private static final String TAB_SELECTED = "xpath=(//span[@class='dojoTabcurrent'])";

  /** The currently selected tab. */
  protected Tab currentTab;
  /** The tabset of the classic window. Every entry of the list represents a level in the tabSet. */
  private List<Map<String, Tab>> tabSet;

  /**
   * Class constructor.
   *
   * @param title
   *          The title of the view.
   * @param menuPath
   *          The path to open the view from the menu.
   */
  public ClassicWindowView(String title, String... menuPath) {
    super(title, menuPath);
    tabSet = new ArrayList<Map<String, Tab>>();
  }

  /**
   * Class constructor.
   *
   * @param title
   *          The title of the view.
   */
  public ClassicWindowView(String title) {
    super(title);
    tabSet = new ArrayList<Map<String, Tab>>();
  }

  /**
   * Get the current tab of the window.
   *
   * @return the current tab of the window.
   */
  public Tab getCurrentTab() {
    return currentTab;
  }

  /**
   * Add a top level tab.
   *
   * @param tab
   *          The tab to add.
   */
  protected void addTab(Tab tab) {
    if (tabSet.isEmpty()) {
      tabSet.add(new LinkedHashMap<String, Tab>());
    }
    tabSet.get(0).put(tab.getTitle(), tab);
  }

  /**
   * Set the currently open tab.
   *
   * @param tab
   *          The currently open tab.
   */
  protected void setCurrentTab(Tab tab) {
    String currentTabText = getCurrentTabText();
    if (!currentTabText.equals(tab.getTitle())) {
      logger.warn("Current tab text is: {}", currentTabText);
      tab.open();
    }
    currentTab = tab;
    if (tabSet.size() - 1 < tab.getLevel() + 1) {
      tabSet.add(new LinkedHashMap<String, Tab>());
    }
    tabSet.set(tab.getLevel() + 1, tab.getSubtabs());
    // Remove lower subtabs of previously selected tab, if any.
    for (int index = tab.getLevel() + 2; index < tabSet.size(); index++) {
      tabSet.remove(index);
    }
  }

  /**
   * Open a tab.
   *
   * @param tab
   *          The tab to open.
   */
  private void openTab(Tab tab) {
    tab.open();
    setCurrentTab(tab);
  }

  /**
   * Select a tab on the window.
   *
   * @param title
   *          The title of the tab.
   * @return The selected tab.
   */
  public Tab selectTab(String title) {
    Tab selectedTab = null;
    if (currentTab == null || !currentTab.getTitle().equals(title)) {
      boolean found = false;
      for (Map<String, Tab> level : tabSet) {
        if (level.containsKey(title)) {
          found = true;
          openTab(level.get(title));
          selectedTab = currentTab;
          break;
        }
      }
      if (!found) {
        logger.debug("Tab not found.");
        // TODO fail.
      }
    } else {
      logger.debug("The tab is already selected.");
      selectedTab = currentTab;
    }
    return selectedTab;
  }
}
