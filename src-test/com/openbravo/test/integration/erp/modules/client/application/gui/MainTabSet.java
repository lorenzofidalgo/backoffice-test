/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.TabSet;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verify actions on OpenbravoERP the Main Tab Set, part of the client application
 * module.
 *
 * @author elopio
 *
 */
public class MainTabSet extends TabSet {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();
  /**
   * Formatting string used to get the number of opened tabs. The parameter is the object string.
   */
  protected static final String FORMAT_SMART_CLIENT_GET_TABS_NUMBER = "%s.tabs.length";
  /**
   * Formatting string used to get the title of a tab. The first parameter is the object string. The
   * second parameter is the tab index.
   */
  protected static final String FORMAT_SMART_CLIENT_GET_TAB_TITLE = "%s.tabs[%d].tabTitle";
  /**
   * Formatting string used to locate a tab by it's title. The first parameter is the tab set
   * locator, and the second is the tab index.
   */
  private static final String FORMAT_LOCATOR_SMART_CLIENT_TAB = "%s/tab[index=%d]";

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public MainTabSet(String objectString) {
    super(objectString);
  }

  /**
   * Get the index of a tab.
   *
   * @param title
   *          The title of the tab.
   * @return the index of the tab.
   */
  public long getTabIndex(String title) {
    boolean isTabOpen = false;
    final long numberOfTabs = (Long) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_TABS_NUMBER, objectString));

    logger.debug("Number of opened tabs: {}", numberOfTabs);

    long index;
    for (index = 0; index < numberOfTabs; index++) {
      String currentTabTitle = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
          String.format(FORMAT_SMART_CLIENT_GET_TAB_TITLE, objectString, index));
      // XXX the alerts tab have no tabTitle property when opened from the navigation bar widget.
      currentTabTitle = (String) (currentTabTitle.equals("null")
          ? SeleniumSingleton.INSTANCE
              .executeScriptWithReturn(String.format("%s.tabs[%d].title", objectString, index))
          : currentTabTitle);
      if (title.equals(currentTabTitle)) {
        isTabOpen = true;
        break;
      }
      final long numberOfCurrentTabs = (Long) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
          String.format(FORMAT_SMART_CLIENT_GET_TABS_NUMBER, objectString));
      if (numberOfCurrentTabs == 1) {
        logger.debug("Detected that current opened tabs is: {}. Current index: {}",
            numberOfCurrentTabs, index);
        isTabOpen = false;
        break;
      }
    }
    if (isTabOpen) {
      return index;
    } else {
      return -1;
    }
  }

  /**
   * Check if a tab is open.
   *
   * @param title
   *          The title of the tab.
   * @return true if the tab is open. Otherwise, false.
   */
  @Override
  public boolean isTabOpen(String title) {
    // XXX isVisible is not working here. Returns true even when the tab is not visible.
    final long tabIndex = getTabIndex(title);
    return tabIndex != -1;
  }

  /**
   * Get the locator of a tab.
   *
   * @param title
   *          The title of the tab.
   * @return the locator of the tab.
   */
  @Override
  protected String getTabLocator(String title) {
    final long tabIndex = getTabIndex(title);
    if (tabIndex == -1) {
      throw new IllegalArgumentException(
          String.format("The tab with title '%s' is not open.", title));
    }
    return String.format(FORMAT_LOCATOR_SMART_CLIENT_TAB, getSmartClientLocator(), tabIndex);
  }

}
