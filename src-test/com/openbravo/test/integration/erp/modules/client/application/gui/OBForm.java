/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriverException;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.SelectorDataObject;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Canvas;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.DateItem;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.HiddenItem;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.InputField;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SectionItem;
import com.openbravo.test.integration.selenium.NotImplementedException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;
import com.openbravo.test.integration.util.TestUtils;

/**
 * Executes actions on OpenbravoERP forms built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class OBForm extends Canvas {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Formatting string used to get the form from the registry. The parameter is the tab identifier.
   */
  private static final String FORMAT_REGISTRY_KEY_FORM = "org.openbravo.client.application.ViewForm_%s";

  /* Status bar. */
  /**
   * Formatting string used to get the previous button from the registry. The parameter is the tab
   * identifier.
   */
  private static final String FORMAT_REGISTRY_KEY_BUTTON_PREVIOUS = "org.openbravo.client.application.statusbar.button.previous.%s";
  /**
   * Formatting string used to get the next button form the registry. The parameter is the tab
   * identifier.
   */
  private static final String FORMAT_REGISTRY_KEY_BUTTON_NEXT = "org.openbravo.client.application.statusbar.button.next.%s";
  /**
   * Formatting string used to get the maximize button from the registry. The parameter is the tab
   * identifier.
   */
  private static final String FORMAT_REGISTRY_KEY_BUTTON_MAXIMIZE = "org.openbravo.client.application.statusbar.button.maximize.%s";
  /**
   * Formatting string used to get the restore button from the registry. The parameter is the tab
   * identifier.
   */
  private static final String FORMAT_REGISTRY_KEY_BUTTON_RESTORE = "org.openbravo.client.application.statusbar.button.restore.%s";
  /**
   * Formatting string used to get the close button from the registry. The parameter is the tab
   * identifier.
   */
  private static final String FORMAT_REGISTRY_KEY_BUTTON_CLOSE = "org.openbravo.client.application.statusbar.button.close.%s";
  /**
   * Formatting string used to get a field by its name or index. The first parameter is object
   * string. The second parameter can be the field name or the field index.
   */
  private static final String FORMAT_SMART_CLIENT_GET_FIELD = "%s.getField(%s)";
  /**
   * Formatting string used to get the section that contains a field. The first parameter is the
   * object string. The second parameter can be the field name or the field index.
   */
  private static final String FORMAT_SMART_CLIENT_GET_SECTION = FORMAT_SMART_CLIENT_GET_FIELD
      + ".section";
  /**
   * Formatting string used to get the number of fields in the form. The parameter is the object
   * string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_FIELDS_COUNT = "%s.getFields().size()";
  /**
   * Formatting string used to get the name of a field. The first parameter is object string. The
   * second parameter is the field index.
   */
  private static final String FORMAT_SMART_CLIENT_GET_FIELD_NAME = FORMAT_SMART_CLIENT_GET_FIELD
      + ".name";
  /**
   * Formatting string used to get the fields shown in the status bar. The parameter is object
   * string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_STATUS_BAR_FIELDS = "%s.statusBarFields";

  /* Status bar components. */
  /** The left status bar. */
  protected OBStatusBar statusBar;

  /** The previous button. */
  protected Button buttonPrevious;
  /** The next button. */
  protected Button buttonNext;
  /** The maximize button. */
  protected Button buttonMaximize;
  /** The restore button. */
  protected Button buttonRestore;
  /** The close button. */
  protected Button buttonClose;

  /**
   * Class constructor.
   *
   * @param tabIdentifier
   *          The tab identifier.
   *
   * @param waitForLoad
   *          Boolean to determine if the system should wait the object be ready to continue.
   */
  public OBForm(String tabIdentifier, Boolean waitForLoad) {
    super(TestRegistry.getObjectString(String.format(FORMAT_REGISTRY_KEY_FORM, tabIdentifier)),
        waitForLoad);
    statusBar = new OBStatusBar(objectString + ".view.statusBar", waitForLoad);
    buttonPrevious = new Button(TestRegistry.getObjectString(
        String.format(FORMAT_REGISTRY_KEY_BUTTON_PREVIOUS, tabIdentifier)), waitForLoad);
    buttonNext = new Button(
        TestRegistry.getObjectString(String.format(FORMAT_REGISTRY_KEY_BUTTON_NEXT, tabIdentifier)),
        waitForLoad);
    buttonMaximize = new Button(TestRegistry.getObjectString(
        String.format(FORMAT_REGISTRY_KEY_BUTTON_MAXIMIZE, tabIdentifier)), waitForLoad);
    buttonRestore = new Button(TestRegistry.getObjectString(
        String.format(FORMAT_REGISTRY_KEY_BUTTON_RESTORE, tabIdentifier)), waitForLoad);
    buttonClose = new Button(TestRegistry.getObjectString(
        String.format(FORMAT_REGISTRY_KEY_BUTTON_CLOSE, tabIdentifier)), waitForLoad);
  }

  /**
   * Class constructor.
   *
   * @param tabIdentifier
   *          The tab identifier.
   */
  public OBForm(String tabIdentifier) {
    this(tabIdentifier, true);
  }

  /**
   * Get a field object.
   *
   * @param fieldName
   *          The name of the field.
   *
   * @return the field object.
   */
  public InputField getField(String fieldName) {
    return FormInputFieldFactory.getInputFieldObject(String.format(FORMAT_SMART_CLIENT_GET_FIELD,
        objectString, TestUtils.convertFieldSeparator(fieldName)));
  }

  /**
   * Get the number of fields in the form.
   *
   * @return the number of fields in the form.
   */
  private Long getFieldsCount() {
    return (Long) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_FIELDS_COUNT, objectString));
  }

  /**
   * Get the name of a field.
   *
   * @param index
   *          The index of the field.
   * @return the name of the field.
   */
  private String getFieldName(long index) {
    return (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format(FORMAT_SMART_CLIENT_GET_FIELD_NAME, objectString, index));
  }

  /**
   * Get the title of a field.
   *
   * @param fieldName
   *          The name of the field.
   * @return the title of the field.
   */
  private String getFieldTitle(String fieldName) {
    final InputField inputField = getField(fieldName);
    return inputField.getTitle();
  }

  /**
   * Get the class of a field.
   *
   * @param fieldName
   *          The name of the field.
   * @return the class of the field.
   */
  private String getFieldClass(String fieldName) {
    return (String) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_FIELD + ".Class",
            objectString, TestUtils.convertFieldSeparator(fieldName)));
  }

  /**
   * Get the section of a field.
   *
   * @param fieldName
   *          The name of the field.
   * @return The section that contains that field. Or null if it is not contained in a section.
   */
  public SectionItem getFieldSection(String fieldName) {
    String sectionObjectString = String.format(FORMAT_SMART_CLIENT_GET_SECTION, objectString,
        TestUtils.convertFieldSeparator(fieldName));
    if ((Boolean) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(sectionObjectString + " == null")) {
      return null;
    } else {
      return new SectionItem(sectionObjectString);
    }
  }

  /**
   * Get a section object.
   *
   * @param sectionName
   *          The name of the section.
   * @return the section object.
   */
  public SectionItem getSection(String sectionName) {
    // TODO assert that the name corresponds to a section.
    return new SectionItem(String.format(FORMAT_SMART_CLIENT_GET_FIELD, objectString,
        TestUtils.convertFieldSeparator(sectionName)));
  }

  /**
   * Get the data of the selected record in this view.
   *
   * @return the record data.
   */
  public DataObject getData() {
    long fieldsCount = getFieldsCount();
    DataObject data = new DataObject();
    for (long index = 0; index < fieldsCount; index++) {
      String fieldName = getFieldName(index);
      String fieldClass = getFieldClass(fieldName);
      if (fieldClass.equals("OBAuditSectionItem") || fieldClass.equals("OBNoteSectionItem")
          || fieldClass.equals("OBLinkedItemSectionItem")
          || fieldClass.equals("OBAttachmentsSectionItem") || fieldClass.equals("OBImageItem")
          || fieldName.equals("quotation") || fieldName.equals("costcenter")) {
        break;
      }
      if (!fieldClass.equals(ClassNamesConstants.CLASS_SECTION_ITEM)
          && !fieldClass.equals(ClassNamesConstants.CLASS_HIDDEN_ITEM)
          && !fieldClass.equals(ClassNamesConstants.CLASS_SPACER_ITEM)) {
        SectionItem sectionItem = getFieldSection(fieldName);
        if (sectionItem != null) {
          sectionItem.expand();
        }
        InputField inputField;
        try {
          inputField = FormInputFieldFactory
              .getInputFieldObject(String.format(FORMAT_SMART_CLIENT_GET_FIELD, objectString,
                  TestUtils.convertFieldSeparator(fieldName)));
          logger.trace("Adding field {} with value {}", fieldName, inputField.getValue());
          data.addDataField(fieldName, inputField.getValue());
        } catch (NotImplementedException e) {
          logger.error("Field name is '{}' - Field class is '{}'.", fieldName, fieldClass);
          logger.error(e::getMessage);
        }
      }
    }
    return data;
  }

  /**
   * Get a data field of the selected record in this tab.
   *
   * @param field
   *          The name of the field to get.
   *
   * @return the value of the field.
   */
  public Object getData(String field) {
    SectionItem sectionItem = getFieldSection(field);
    if (sectionItem != null) {
      sectionItem.expand();
    }
    final InputField inputField = FormInputFieldFactory.getInputFieldObject(String.format(
        FORMAT_SMART_CLIENT_GET_FIELD, objectString, TestUtils.convertFieldSeparator(field)));
    return inputField.getValue();
  }

  /**
   * Fill a field.
   *
   * @param fieldName
   *          The name of the field.
   * @param newValue
   *          The new value to enter on the field.
   */
  public void fill(String fieldName, Object newValue) {
    // TODO I0: removing static sleeps in favor of fluent waits
    // TODO <selenium waits> improve lambda expression to omit the "return new Object();" required
    // for the functional interface
    logger.trace("Filling the field '{}' with the value '{}'.", fieldName, newValue);
    SectionItem sectionItem = getFieldSection(fieldName);
    if (sectionItem != null) {
      sectionItem.expand();
      sectionItem.waitUntilExpanded();
    }
    InputField inputField;
    inputField = Sleep.setFluentWait(300, 6000, Exception.class).until(wd -> getField(fieldName));
    inputField.waitUntilEnabled();
    Sleep.setFluentWait(300, 6000, JavascriptException.class).until(wd -> {
      inputField.setValue(newValue);
      return new Object();
    });
    Sleep.setFluentWait(300, 5000, WebDriverException.class).until(wd -> {
      try {
        inputField.waitUntilEnabled();
      } catch (WebDriverException wde) {
        SeleniumSingleton.INSTANCE.switchTo().defaultContent();
        inputField.waitUntilEnabled();
      }
      return new Object();
    });
    // TODO Consider if it would be a good change to check if fields are filled with smartwaits like
    // this one: https://code.openbravo.com/tools/automation/pi-smoke/rev/205d0b5ccf4a#l5.22
    // for (int i = 0; i < 20 && (this.isEmpty()); i++) {
    // Sleep.trySleep(150);
    // }
  }

  /**
   * Assert a field.
   *
   * @param fieldName
   *          The name of the field.
   * @param expectedValue
   *          The expected value of the field.
   */
  public void assertField(String fieldName, Object expectedValue) {
    // do not test fields which are included in the statusbar, as those will be checked by the
    // method 'assertStatusBarFields'
    ArrayList<String> statusBarFields = getStatusBarFields();
    for (int index = 0; index < statusBarFields.size(); index++) {
      String statusBarFieldname = statusBarFields.get(index);
      if (statusBarFieldname.equals(fieldName)) {
        return;
      }
    }

    SectionItem sectionItem = getFieldSection(fieldName);
    if (sectionItem != null) {
      sectionItem.expand();
    }
    final InputField inputField = getField(fieldName);
    if (inputField instanceof HiddenItem) {
      logger.info("Field '{}' is a hidden item and will not be checked in the form.", fieldName);
    } else {
      final Object expVal;
      Object expectedDisplayValue;
      if (expectedValue instanceof SelectorDataObject) {
        logger.trace("Expected value is instance of SelectorDataObject");
        expectedDisplayValue = ((SelectorDataObject) expectedValue).getDisplayValue();
      } else if (expectedValue instanceof DataObject) {
        expectedDisplayValue = expectedValue.toString();
      } else {
        expectedDisplayValue = expectedValue;
      }
      if (inputField instanceof DateItem) {
        expectedDisplayValue = OBDate.getDateFromTag(expectedDisplayValue);
      }

      // XXX This is part of a workaround to avoid issue
      // https://issues.openbravo.com/view.php?id=31374
      expectedDisplayValue = TestUtils
          .convertNumericalValueToMeaningfulNumber(expectedDisplayValue);
      // ...to here
      // TODO I0: Adding explicit wait period to ensure that the field to be asserted is
      // loaded
      expVal = expectedDisplayValue;
      try {
        Sleep.setExplicitWait(3000).until(wd -> inputField.getValue().equals(expVal));
      } catch (TimeoutException te) {
        // Continue when condition isn't met, too
      }

      assertThat(fieldName, inputField.getValue(), is(equalTo(expectedDisplayValue)));
    }
  }

  /**
   * Get the names of the status bar fields
   *
   * @return an array with the names of the status bar fields.
   */
  @SuppressWarnings("unchecked")
  private ArrayList<String> getStatusBarFields() {
    return (ArrayList<String>) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format(FORMAT_SMART_CLIENT_GET_STATUS_BAR_FIELDS, objectString));
  }

  /**
   * Assert the fields shown in the status bar.
   *
   * @param dataObject
   *          The object with the data to verify. Only the fields that are shown in the status bar
   *          will be asserted, and the rest will be ignored.
   *
   */
  public void assertStatusBarFields(DataObject dataObject) {
    ArrayList<String> statusBarFields = getStatusBarFields();
    if (statusBarFields.size() > 0) {
      // boolean executeAssertion = false;
      // StringBuilder fieldsRegExpStringBuilder = new StringBuilder();
      // As the fields values may contain some characters with meaning for the regular expression,
      // we surround it with \Q and \E to quote all this especial characters.
      // fieldsRegExpStringBuilder.append("\\Q");
      for (int index = 0; index < statusBarFields.size(); index++) {
        // TODO L2: Create a smart sleep here. Removing it induces errors in aproutreg2
        Sleep.trySleep(500);
        String fieldName = statusBarFields.get(index);
        // fieldsRegExpStringBuilder.append(getFieldTitle(fieldName));
        // fieldsRegExpStringBuilder.append("\\E: {1,2}\\Q");
        Object expectedValue = dataObject.getDataField(fieldName);
        if (expectedValue != null) {
          // At least one field from the data object is shown in the status bar, so the assertion
          // has to be executed.
          // executeAssertion = true;
          if (expectedValue instanceof Boolean) {
            if ((Boolean) expectedValue) {
              expectedValue = "Yes";
            } else {
              expectedValue = "No";
            }
          }
          expectedValue = OBDate.getDateFromTag(expectedValue);
          statusBar.assertField(getFieldTitle(fieldName), (String) expectedValue);
          // } else {
          // We are not interested in asserting this field. Match any value.
          // expectedValue = "\\E.*\\Q";
        }
        // fieldsRegExpStringBuilder.append(expectedValue);
        // Add the fields separator.
        // if (index != statusBarFields.size() - 1) {
        // fieldsRegExpStringBuilder.append(" | ");
        // }
      }
      // Close the quoted string.
      // fieldsRegExpStringBuilder.append("\\E");
      // if (executeAssertion) {
      // Execute the assertion only if some of the fields from the data object are displayed in
      // the status bar.
      // statusBar.assertFields(fieldsRegExpStringBuilder.toString());
      // }
    }
  }

  /**
   * Assert that a section is expanded.
   *
   * @param name
   *          The name of the section.
   *
   */
  public void assertSectionExpanded(String name) {
    final SectionItem sectionItem = getSection(name);
    assertTrue("Section item " + name + " is not expanded.", sectionItem.isExpanded());
  }

  /**
   * Assert that a section is collapsed.
   *
   * @param name
   *          The name of the section.
   *
   */
  public void assertSectionCollapsed(String name) {
    final SectionItem sectionItem = getSection(name);
    assertFalse("Section item " + name + " is not collapsed.", sectionItem.isExpanded());
  }

  /**
   * Assert that a section is visible.
   *
   * @param name
   *          The name of the section.
   *
   */
  public void assertSectionVisible(String name) {
    final SectionItem sectionItem = getSection(name);
    assertTrue("Section item " + name + " is not visible.", sectionItem.isVisible());
  }

  /**
   * Assert that a section is hidden.
   *
   * @param name
   *          The name of the section.
   *
   */
  public void assertSectionHidden(String name) {
    final SectionItem sectionItem = getSection(name);
    assertFalse("Section item " + name + " is not hidden.", sectionItem.isVisible());
  }

  /**
   * Move to the previous record.
   */
  public void previous() {
    logger.debug("Moving to the previous record.");
    buttonPrevious.click();
    // TODO wait.
  }

  /**
   * Move to the next record.
   */
  public void next() {
    logger.debug("Moving to the next record.");
    buttonNext.click();
    // TODO wait.
  }

  /**
   * Maximize the form view.
   */
  public void maximize() {
    logger.debug("Maximizing the form.");
    buttonMaximize.click();
    // TODO wait.
  }

  /**
   * Restore the form view.
   */
  public void restore() {
    logger.debug("Restoring the form.");
    buttonRestore.click();
    // TODO wait.
  }

  /**
   * Close the form view.
   */
  public void close() {
    logger.debug("Closing the form.");
    // XXX the selenium click is not working. Call directly the smartclient click function.
    // buttonClose.click();
    buttonClose.smartClientClick();
  }

  /**
   * Assert that the status bar shows the saved message.
   */
  public void assertSaved() {
    assertNotInEditMode();
    statusBar.assertSaved();
  }

  public void assertNotInEditMode() {
    statusBar.assertNotInEditMode();
  }

  /**
   * Assert that the status bar shows the new message.
   */
  public void assertNewMessage() {
    statusBar.assertNew();
  }

  /**
   * Get the message displayed in the status bar.
   *
   * @return the message displayed in the status bar.
   */
  public String getStatusBarMessage() {
    return statusBar.getMessage();
  }

  /**
   * Check if the saved message is visible. return true if the saved message is visible. Otherwise,
   * false.
   *
   * @return true if the saved message is visible. Otherwise, false.
   */
  public boolean isSavedMessageVisible() {
    return statusBar.isVisible() && statusBar.isSavedMessage();
  }

}
