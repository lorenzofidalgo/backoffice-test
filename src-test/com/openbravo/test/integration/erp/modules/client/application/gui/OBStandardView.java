/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>,
 *  Inigo Sanchez <inigo.sanchez@openbravo.com>.
 *  Nono Carballo <f.carballo@nectus.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.NoSuchElementException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.SelectorDataObject;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Canvas;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.InputField;
import com.openbravo.test.integration.selenium.NotImplementedException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on OpenbravoERP standard view built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class OBStandardView<T extends DataObject> extends Canvas {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Formatting string used to get the view from the registry. The parameter is the tab identifier.
   */
  private static final String FORMAT_REGISTRY_KEY_VIEW = "org.openbravo.client.application.View_%s";
  /**
   * Formatting string used to get the message bar from the registry. The parameter is the object
   * string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_MESSAGE_BAR = "%s.messageBar";
  /**
   * Formatting string used to get the active bar from the registry. The parameter is the object
   * string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_ACTIVE_BAR = "%s.activeBar";

  /* Messages. */
  /** Error message displayed when there are some fields that contain illegal values. */
  private static final String ERROR_MESSAGE_FIELDS_CONTAIN_ILLEGAL_VALUES = "Saving failed. One or more fields contain illegal values, check the errors for each field.";
  /** Error message displayed when showed a column in edit grid mode. */
  private static final String ERROR_MESSAGE_SHOW_COLUMN_EDITING_GRID = "<b>Error</b><br/>New fields cannot be added to the grid while it is being edited.";

  /**
   * Formatting string used to get the revert button from the registry. The parameter is the tab
   * identifier.
   */
  private static final String FORMAT_REGISTRY_KEY_BUTTON_REVERT = "org.openbravo.client.application.toolbar.button.undo.%s";
  private static final String FORMAT_REGISTRY_KEY_BUTTON_SAVE = "org.openbravo.client.application.toolbar.button.save.%s";

  /** The identifier of the tab. */
  private final String tabIdentifier;

  /* Components */
  /** The message bar. */
  private final OBMessageBar barMessage;
  /** The active bar. */
  private final Canvas barActive;
  /** The grid view of the tab. */
  private final OBGrid grid;
  /** The form view of the tab. */
  private final OBForm form;

  /**
   * Class constructor.
   *
   * @param tabIdentifier
   *          The tab identifier. @param waitForLoad Set to true if it is required to wait until it
   *          is loaded
   */
  public OBStandardView(String tabIdentifier, Boolean waitForLoad) {
    super(TestRegistry.getObjectString(String.format(FORMAT_REGISTRY_KEY_VIEW, tabIdentifier)),
        waitForLoad);
    this.tabIdentifier = tabIdentifier;
    this.barActive = new Canvas(String.format(FORMAT_SMART_CLIENT_GET_ACTIVE_BAR, objectString),
        waitForLoad);
    this.barMessage = new OBMessageBar(
        String.format(FORMAT_SMART_CLIENT_GET_MESSAGE_BAR, objectString), waitForLoad);
    this.grid = new OBGrid(tabIdentifier, waitForLoad);
    this.form = new OBForm(tabIdentifier, waitForLoad);
  }

  /**
   * Class constructor.
   *
   * @param tabIdentifier
   *          The tab identifier.
   */
  public OBStandardView(String tabIdentifier) {
    this(tabIdentifier, true);
  }

  /**
   * Returns true if this tab is on form view.
   *
   * @return true if the tab is on form view. Otherwise, false.
   */
  public boolean isOnFormView() {
    return form.isVisible();
  }

  /**
   * Wait for the form to appear.
   */
  public void waitForFormView() {
    try {
      form.waitUntilVisible();
    } catch (TimeoutException toe) {
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      // TODO L2: Remove the following static sleep once it is stable
      Sleep.trySleep();
      form.waitUntilVisible();
    }
  }

  /**
   * Wait for the grid to appear.
   */
  public void waitForGridView() {
    grid.waitUntilVisible();
  }

  /**
   * Returns true if this tab is on form grid.
   *
   * @return true if the tab is on grid view. Otherwise, false.
   */
  public boolean isOnGridView() {
    return grid.isVisible();
  }

  /**
   * Switch to grid view and wait until the grid has been loaded.
   */
  public void switchToGridView() {
    if (!isOnGridView()) {
      logger.debug("Switching to grid view.");
      form.close();
      grid.waitForDataToLoad();
    }
  }

  public void waitForDataToLoad() {
    grid.waitForDataToLoad();
  }

  /**
   * Get the form component.
   *
   * @return the form component.
   */
  public OBForm getForm() {
    return form;
  }

  /**
   * Get the number of records in the tab.
   *
   * @return The number of records in the tab.
   */
  public int getRecordCount() {
    switchToGridView();
    return grid.getRecordCount();
  }

  /**
   * Get the data of the selected record in this view.
   *
   * @return the record data.
   */
  public DataObject getData() {
    try {
      if (!isOnFormView()) {
        open();
      }
      // Catch added after inserting Geckodriver due to performance problems. Selenium's speed >>
      // machine's speed
      // TODO L00: Remove this catch and/or the static sleep once tests have been stabilized
    } catch (JavascriptException jse) {
      logger.warn("JavascriptException thrown. Let's try open the View again.");
      Sleep.trySleep(5000);
      if (!isOnFormView()) {
        open();
      }
    }
    return form.getData();
  }

  /**
   * Get a data field of the selected record in this tab.
   *
   * @param field
   *          The name of the field to get.
   *
   * @return the value of the field.
   */
  public Object getData(String field) {
    if (!isOnFormView()) {
      open();
    }
    return form.getData(field);
  }

  /**
   * Filter the grid.
   *
   * @param dataObject
   *          Data object with the values of the filter.
   */
  public void filter(T dataObject) {
    logger.debug("Filtering with the values '{}'.", dataObject);
    switchToGridView();
    grid.clearFilters();
    for (final String key : dataObject.getDataFields().keySet()) {
      Object dataField = dataObject.getDataFields().get(key);
      String filterValue;
      if (dataField instanceof SelectorDataObject) {
        filterValue = ((SelectorDataObject) dataField).getDisplayValue();
      } else if (dataField instanceof Boolean) {
        filterValue = (Boolean) dataField ? "Yes" : "No";
      } else {
        filterValue = dataField.toString();
      }
      // TODO log a warning if the field is not visible.
      grid.filter(key, filterValue);
    }
    // To apply the last filter, it is required to change focus, so TAB is pressed
    // The following line is not working right in Multicurrency job, so for now it is disabled and
    // its behaviour will be checked.
    // SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.TAB);
    // It can be tested to use Shift + TAB to get same effect but without Multicurrency conflicts
    // (going to previous element)
    // String shiftTAB = Keys.chord(Keys.TAB, Keys.SHIFT);
    // SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(shiftTAB);

    logger.debug("** Sleeping to give time to let the application working properly **");
    Sleep.trySleep();
    grid.waitForDataToLoad();
  }

  public void pickRecordInFilterDropDown(String columnName, int recordNo) {
    grid.pickRecordInFilterDropDown(columnName, recordNo);

  }

  public int openFilterDropDown(String columnName) {
    return grid.openFilterDropDown(columnName);
  }

  /**
   * Clear the filters.
   */
  public void clearFilters() {
    grid.clearFilters();
  }

  /**
   * Unselect all records.
   */
  public void unselectAll() {
    grid.unselectAll();
  }

  /**
   * Click on the column header button of the given name
   *
   * @param colName
   *          The name of the column to sort by
   */
  public void clickColumnHeader(String colName) {
    grid.clickColumnHeader(colName);
  }

  /**
   * Select a record.
   *
   * @param dataObject
   *          Data object with the values of the record to select.
   */
  public void select(T dataObject) {
    logger.debug("Selecting the record with values '{}'.", dataObject);
    switchToGridView();
    filter(dataObject);
    final int recordCount = grid.getRecordCount();
    if (recordCount > 0) {
      if (recordCount > 1) {
        logger.warn("More than one record match the selection values. Selecting the first one.");
      }

      // TODO L2:It is possible the following sleep is not required or maybe reduce clearance
      Sleep.trySleep(1500);
      grid.waitForDataToLoad();
      grid.selectRowByNumber(0);
      // TODO L1: Remove static sleep
      Sleep.trySleep();
    } else {
      throw new NoSuchElementException("Record not found.");
    }
  }

  /**
   * Sort Ascending using the context menu of the column header button of the given column in the
   * DataObject.
   *
   * @param dataObject
   *          The data with the name (key) of the column that will be sorted
   */
  public void sortAscending(T dataObject) {
    logger.debug("Sort Ascending the column '{}'.", dataObject::getNextFieldName);
    switchToGridView();
    try {
      grid.sortAscending(dataObject.getNextFieldName());
    } catch (NoSuchElementException nsee) {
      throw new NoSuchElementException("Column not found.");
    }
  }

  /**
   * Sort Descending using the context menu of the column header button of the given column in the
   * DataObject.
   *
   * @param dataObject
   *          The data with the name (key) of the column that will be sorted
   */
  public void sortDescending(T dataObject) {
    logger.debug("Sort Descending the column '{}'.", dataObject::getNextFieldName);
    switchToGridView();
    try {
      grid.sortDescending(dataObject.getNextFieldName());
    } catch (NoSuchElementException nsee) {
      throw new NoSuchElementException("Column not found.");
    }
  }

  /**
   * Fill the record data on the form.
   *
   * @param dataObject
   *          Data object with the values to fill.
   */
  public void fillForm(T dataObject) {
    logger.debug("Filling the form with values '{}'.", dataObject);
    if (!isOnFormView()) {
      throw new IllegalStateException("The tab is not in form view.");
    } else {
      for (final String key : dataObject.getDataFields().keySet()) {
        form.fill(key, dataObject.getDataFields().get(key));
      }
    }
  }

  /**
   * Fill the edition row on the grid.
   *
   * @param dataObject
   *          Data object with the values to fill.
   */
  public void fillRow(T dataObject) {
    logger
        .debug(String.format("Filling the edition row in the grid with values '%s'.", dataObject));
    if (!isOnGridView()) {
      throw new IllegalStateException("The tab is not in grid view.");
    } else {
      for (final String key : dataObject.getDataFields().keySet()) {
        grid.fill(key, dataObject.getDataFields().get(key));
      }
    }
  }

  /**
   * Delete the selected record from the grid using a shortcut
   *
   */
  public void deleteOnGridWithShortcut() {
    logger.debug("Deleting a record using a shortcut");
    if (!isOnGridView()) {
      throw new IllegalStateException("The tab is not in grid view.");
    }
    grid.deleteRecordWithShortcut();
  }

  /**
   *
   * Edit the selected record from the form.
   *
   * @param dataObject
   *          Data object with the edited values.
   */
  public void editRecord(T dataObject) {
    editOnForm(dataObject);
  }

  /**
   * Edit the selected record from the grid.
   *
   * @param dataObject
   *          Data object with the edited values.
   */
  public void editOnGrid(T dataObject) {
    logger.debug("Editing the record with values '{}'.", dataObject);
    if (!isOnGridView()) {
      throw new IllegalStateException("The tab is not in grid view.");
    }
    // FIXME what if no record was selected previously?
    grid.clickEdit();
    fillRow(dataObject);
  }

  /**
   * Edit the selected record from the form.
   *
   * @param dataObject
   *          Data object with the edited values.
   */
  public void editOnForm(T dataObject) {
    logger.debug("Editing the record with values '{}'.", dataObject);
    try {
      this.waitUntilLoaded();
      if (!isOnFormView()) {
        open();
      }
    } catch (WebDriverException wde) {
      logger.warn("It has not been possible to open the form view");
      Sleep.trySleep(2500);
      if (!isOnFormView()) {
        open();
      }
    }
    for (final String key : dataObject.getDataFields().keySet()) {
      form.fill(key, dataObject.getDataFields().get(key));
    }
    // // Workaround to solve the last field edition problem
    // SeleniumSingleton.INSTANCE.findElementById(tabIdentifier).sendKeys(Keys.TAB);
  }

  /**
   * Assert the data the selected record.
   *
   * @param dataObject
   *          The data object with the expected values.
   */
  public void assertData(T dataObject) {
    assertOnForm(dataObject);
  }

  /**
   * Assert display logic visibility
   *
   * @param visibleDisplayLogic
   *          DataObject with the fields whose visibility has to be asserted.
   */
  public void assertDisplayLogicVisible(DataObject visibleDisplayLogic) {

    for (final String key : visibleDisplayLogic.getDataFields().keySet()) {
      if (visibleDisplayLogic.getDataFields().get(key).equals(true)) {
        assertTrue(key + " is not visible", this.getForm().getSection(key).isVisible());
      } else {
        assertFalse(key + " is visible. It shouldn't", this.getForm().getSection(key).isVisible());
      }
    }
  }

  /**
   * Assert display logic enable status
   *
   * @param enabledDisplayLogic
   *          DataObject with the fields whose enable status has to be asserted.
   */
  public void assertDisplayLogicEnabled(DataObject enabledDisplayLogic) {

    for (final String key : enabledDisplayLogic.getDataFields().keySet()) {
      if (enabledDisplayLogic.getDataFields().get(key).equals(true)) {
        assertTrue(key + " is not enabled", this.getForm().getSection(key).isEnabled());
      } else {
        assertFalse(key + " is enabled. It should't", this.getForm().getSection(key).isEnabled());
      }
    }
  }

  /**
   * Assert display logic expanded status
   *
   * @param expandedDisplayLogic
   *          DataObject with the fields whose expanded status has to be asserted.
   */
  public void assertDisplayLogicExpanded(DataObject expandedDisplayLogic) {

    for (final String key : expandedDisplayLogic.getDataFields().keySet()) {
      if (expandedDisplayLogic.getDataFields().get(key).equals(true)) {
        assertTrue(key + " is not expanded", this.getForm().getSection(key).isExpanded());
      } else {
        assertFalse(key + " is expanded. It shouldn't",
            this.getForm().getSection(key).isExpanded());
      }
    }
  }

  /**
   * Assert the data the selected grid record.
   *
   * @param dataObject
   *          The data object with the expected values.
   */
  public void assertOnGrid(T dataObject) {
    if (!isOnGridView()) {
      throw new IllegalStateException("The tab is not in grid view.");
    }
    // FIXME what if no record was selected previously?
    for (final String key : dataObject.getDataFields().keySet()) {
      // TODO log a warning if the field is not visible.
      grid.assertField(key, dataObject.getDataFields().get(key).toString());
    }
  }

  /**
   * Assert filter value
   *
   * @param fieldName
   *          The name of the field whose filter value will be compared. @param expectedValue Object
   *          with the expected value.
   */
  public void assertFilterValue(String fieldName, Object expectedValue) {
    if (!isOnGridView()) {
      throw new IllegalStateException("The tab is not in grid view.");
    }
    Object filterValue = grid.getFilterValue(fieldName);
    assertThat(filterValue, is(equalTo(expectedValue)));
  }

  /**
   * Assert on form view the data the selected record.
   *
   * @param dataObject
   *          The data object with the expected values.
   */
  public void assertOnForm(T dataObject) {
    if (!isOnFormView()) {
      open();
      Sleep.smartWaitButtonEnabledUsingTestRegistry(TestRegistry
          .getObjectString(String.format(FORMAT_REGISTRY_KEY_BUTTON_REVERT, tabIdentifier)), 400);
    }
    for (final String key : dataObject.getDataFields().keySet()) {
      logger.trace("[assertOnForm] Searching for key: {} with value {}", key,
          dataObject.getDataFields().get(key));
      form.assertField(key, dataObject.getDataFields().get(key));
    }
    form.assertStatusBarFields(dataObject);
  }

  /**
   * Open the selected record.
   */
  public void open() {
    if (!isOnGridView()) {
      throw new IllegalStateException("The tab is not in grid view.");
    }
    // FIXME what if no record was selected previously? <- It throws an
    // org.openqa.selenium.WebDriverException
    grid.clickOpen();
  }

  /**
   * Close the form and wait for the grid to appear.
   */
  public void closeForm() {
    logger.debug("Closing the form.");
    if (isOnFormView()) {
      form.close();
      grid.waitUntilVisible();
    } else {
      logger.info("The tab wasn't in form view.");
    }
  }

  /**
   * Assert the number of records in the tab.
   *
   * @param expectedCount
   *          The expected number of records.
   */
  public void assertCount(int expectedCount) {
    int recordCount = getRecordCount();

    // FIXME: This will fail when expectedCount were 0 and a record were present
    // Wait a while
    if (((recordCount == 0) || (recordCount == -1)) && (expectedCount > 0)) {
      SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver webDriver) {
          logger.debug("Waiting for recordcount to be set");
          try {
            return getRecordCount() > 0;
          } catch (Exception exception) {
            return false;
          }
        }
      });
    }
    assertThat(getRecordCount(), is(equalTo(expectedCount)));
  }

  /**
   * Get the contents of the message displayed in the message bar.
   *
   * @return the contents of the message displayed in the message bar.
   */
  public String getMessageContents() {
    barMessage.assertVisible();
    return barMessage.getContents();
  }

  /**
   * Assert that the process completed successfully.
   */
  public void assertProcessCompletedSuccessfully() {
    Sleep.smartWaitButtonDisabledUsingTestRegistry(
        TestRegistry.getObjectString(String.format(FORMAT_REGISTRY_KEY_BUTTON_SAVE, tabIdentifier)),
        600);
    barMessage.assertProcessCompletedSuccessfully();
    // Close the message bar so it doesn't interfere with other tests.
    barMessage.close();
  }

  /**
   * Assert that the process completed successfully.
   *
   * TODO see issue 17291 - https://issues.openbravo.com/view.php?id=17291.
   */
  public void assertProcessCompletedSuccessfully2() {
    // TODO L00: This static sleep is required due to since Se 3.3.1 it was working so fast the old
    // message was still present.
    Sleep.trySleep();
    // TODO L00: Check if the following smart sleep is working properly
    Sleep.smartWaitButtonDisabledUsingTestRegistry(
        TestRegistry.getObjectString(String.format(FORMAT_REGISTRY_KEY_BUTTON_SAVE, tabIdentifier)),
        600);
    barMessage.assertProcessCompletedSuccessfully2();
    // Close the message bar so it doesn't interfere with other tests.
    barMessage.close();
  }

  /**
   * Assert the process completed successfully message.
   *
   * TODO see issue 17291 - https://issues.openbravo.com/view.php?id=17291.
   *
   * @param message
   *          The expected message body.
   */
  public void assertProcessCompletedSuccessfully2(String message) {
    barMessage.assertProcessCompletedSuccessfully2(message);
    // Close the message bar so it doesn't interfere with other tests.
    barMessage.close();
  }

  /**
   * Assert that the process completed successfully.
   *
   * @param message
   *          The expected message body.
   */
  public void assertProcessCompletedSuccessfully(String message) {
    barMessage.assertProcessCompletedSuccessfully(message);
    barMessage.close();
  }

  /**
   * Assert the process completed successfully message, using regular expression to match the body.
   *
   * @param message
   *          A regular expression for the expected message body. @return a list with all the
   *          capturing groups of the regular expression.
   */
  public List<String> assertProcessCompletedSuccessfullyContentMatch(String message) {
    barMessage.waitUntilVisible();
    List<String> capturingGroups = barMessage
        .assertProcessCompletedSuccessfullyContentMatch(message);
    barMessage.close();
    return capturingGroups;
  }

  /**
   * Assert the process execution succeded message, using regular expression to match the body.
   *
   * @param message
   *          A regular expression for the expected message body. @return a list with all the
   *          capturing groups of the regular expression.
   */
  public List<String> assertProcessExecutionSuccededContentMatch(String message) {
    barMessage.waitUntilVisible();
    List<String> capturingGroups = barMessage.assertProcessExecutionSuceededContentMatch(message);
    barMessage.close();
    return capturingGroups;
  }

  /**
   * Assert that the error message was shown because some fields contain illegal values.
   */
  public void assertErrorFieldsContainIllegalValues() {
    barMessage.assertErrorMessage(ERROR_MESSAGE_FIELDS_CONTAIN_ILLEGAL_VALUES);
    barMessage.close();
  }

  /**
   * Assert that an error message is shown .
   *
   * @param errorMessage
   *          The error message that has to be asserted
   */
  public void assertErrorMessage(String errorMessage) {
    barMessage.assertErrorMessage(errorMessage);
    barMessage.close();
  }

  /**
   * Assert that a warning message is shown .
   *
   * @param warningMessage
   *          The warning message that has to be asserted
   */
  public void assertWarningMessage(String warningMessage) {
    barMessage.assertWarningMessage(warningMessage);
    barMessage.close();
  }

  /**
   * Assert that an info message is shown .
   *
   * @param infoMessage
   *          The informative message that has to be asserted
   */
  public void assertInfoMessage(String infoMessage) {
    barMessage.assertInfoMessage(infoMessage);
    barMessage.close();
  }

  /**
   * Assert that the status bar shows the saved message.
   */
  public void assertSaved() {
    Sleep.smartWaitButtonDisabledUsingTestRegistry(
        TestRegistry.getObjectString(String.format(FORMAT_REGISTRY_KEY_BUTTON_SAVE, tabIdentifier)),
        600);
    if (isOnFormView()) {
      assertFalse("Error message: " + barMessage.getContents(), barMessage.isErrorMessageVisible());
      form.assertSaved();
      // XXX added because on some tabs, after saving the grid is shown and if the following action
      // is done before the grid is loaded, nothing happens.
    } else {
      // TODO assert that no errors appeared.
      grid.assertSaved();
    }
  }

  public void assertNotInEditMode() {
    if (isOnFormView()) {
      form.assertNotInEditMode();
    } else {
      grid.assertNotInEditMode();
    }
  }

  public void assertInEditMode() {
    if (isOnFormView()) {
      throw new NotImplementedException("Unimplemented in edit mode assertion on form.");
    } else {
      grid.assertInEditMode();
    }
  }

  /**
   * Assert that the view is in new mode.
   */
  public void assertNewMode() {
    if (isOnFormView()) {
      assertFalse("Error message: " + barMessage.getContents(), barMessage.isErrorMessageVisible());
      form.assertNewMessage();
    } else {
      throw new NotImplementedException("Unimplemented new mode assertion on grid.");
    }
  }

  /**
   * Wait until the tab has been selected.
   */
  public void waitUntilSelected() {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug(
            String.format("Waiting for the tab with id '%s' to be selected.", tabIdentifier));
        return barActive.isVisible();
      }
    });
  }

  /**
   * Wait for a message to appear.
   */
  public void waitUntilMessageVisible() {
    barMessage.waitUntilVisible();
  }

  /**
   * Wait a bit if Info message is visible.
   */
  public void waitIfInfoMessageVisible() {
    barMessage.waitIfInfoMessageVisible(3000, 4);
  }

  public void closeMessageIfVisible() {
    if (barMessage.isVisible()) {
      barMessage.close();
    }
  }

  /**
   * Wait for the grid edit form to be visible.
   */
  public void waitUntilGridEditFormIsVisible() {
    grid.waitUntilEditFormIsVisible();
  }

  /**
   * Wait until the saved message or an error appears.
   */
  public void waitUntilSavedOrErrorOnForm() {
    try {
      SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver webDriver) {
          logger.debug("Waiting for the record to be saved or an error to occur.");
          return form.isSavedMessageVisible() || barMessage.isErrorMessageVisible();
        }
      });
    } catch (Throwable throwable) {
      logger.error("Record save on tab with id '{}' was not processed. Status bar message: '{}'",
          tabIdentifier, form.getStatusBarMessage());
      fail();
    }
  }

  /**
   * Wait for the record was saved or an error occurred.
   */
  public void waitUntilSavedOrErrorOnGrid() {
    // TODO we are not waiting for the error.
    grid.waitUntilEditFormIsNotVisible();
  }

  public void selectAllRecordsOneCheckboxOnGrid() {
    grid.selectAllRecordsOneCheckbox();

  }

  public void selectMultipleRecordsOnGrid(int[] indexesToBeSelected) {
    grid.selectMultipleRecords(indexesToBeSelected);
  }

  public void cancelOnGridWithShortcut() {
    grid.cancelWithShortcut();
  }

  public void startEditionOnGrid(int row) {
    logger.debug("Starting the edition of the record in row {}", row);
    if (!isOnGridView()) {
      throw new IllegalStateException("The tab is not in grid view.");
    }
    selectWithoutFiltering(row);
    grid.clickEdit();
  }

  public void moveToNextRecordWhileEditingInGrid() {
    grid.moveToNextRecordWhileEditing();
  }

  public InputField getField(String fieldName, boolean onlyAllowTextItems) {
    return grid.getField(fieldName, onlyAllowTextItems);
  }

  public void deleteOnGridUsingContextualMenu() {
    grid.deleteUsingContextualMenu();
  }

  /**
   * Edit the selected record from the grid using the contextual menu.
   *
   * @param dataObject
   *          Data object with the edited values.
   */
  public void editOnGridUsingContextualMenu(T dataObject) {
    logger.debug("Editing using the contextual menu with values '{}'.", dataObject);
    if (!isOnGridView()) {
      throw new IllegalStateException("The tab is not in grid view.");
    }
    // FIXME what if no record was selected previously?
    grid.editSelectedRecordUsingContextualMenu();
    fillRow(dataObject);
  }

  public void selectWithoutFiltering(int row) {
    grid.selectRecord(row);
  }

  public void openFirstRecordInFormWithDoubleClick() {
    grid.openFirstRecordInFormWithDoubleClick();
  }

  public void createOnGridWithContextualMenu() {
    grid.createWithContextualMenu();
  }

  public void showColumnOnGrid(String fieldName) {
    if (!isOnGridView()) {
      throw new IllegalStateException("The tab is not in grid view.");
    }
    grid.showColumnOnGrid(fieldName);
    // To add a column properly it must be verified that there has been no error in the message bar.
    if (barMessage.isErrorMessageVisible()) {
      assertThat(this.getMessageContents(), is(equalTo(ERROR_MESSAGE_SHOW_COLUMN_EDITING_GRID)));
      throw new IllegalStateException(
          "New fields cannot be added to the grid while it is being edited.");
    }
  }

  public void hideColumnOnGrid(String fieldName) {
    if (!isOnGridView()) {
      throw new IllegalStateException("The tab is not in grid view.");
    }
    grid.hideColumnOnGrid(fieldName);
  }

  public void createOnGridWithShortcut() {
    grid.createNewRecordWithShortcut();
  }

  /**
   * Get all rows in the grid
   */
  public Object getRows() {
    return grid.getRows();
  }

  /**
   * Get the selected row in the grid
   */
  public Object getSelectedRow() {
    return grid.getSelectedRows();
  }

}
