/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.TabSet;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on the OpenbravoERP tab set of standard windows build upon SmartClient user
 * interface library.
 *
 * @author elopio
 *
 */
public class StandardWindowTabSet extends TabSet {

  /**
   * Formatting string used to get the children tab set from the registry. The parameter is the tab
   * identifier.
   */
  private static final String FORMAT_REGISTRY_IDENTIFIER_CHILDREN_TABSET = "org.openbravo.client.application.ChildTabSet_%s";
  /**
   * Formatting string used to get the child tab from the registry. The first parameter is the
   * identifier of the parent tab. The second parameter is the identifier of the child tab.
   */
  private static final String FORMAT_REGISTRY_IDENTIFIER_CHILDREN_TAB = "org.openbravo.client.application.ChildTab_%s_%s";
  /**
   * Formatting string used to get the title of a child tab. The parameter is the identifier of the
   * child tab on the test registry.
   */
  private static final String FORMAT_GET_CHILD_TAB_TITLE = "%s.title";

  private String parentIdentifier;

  /**
   * Class constructor.
   *
   * @param parentIdentifier
   *          The parent tab identifier in Openbravo Test Registry.
   */
  public StandardWindowTabSet(String parentIdentifier) {
    super(TestRegistry.getObjectString(
        String.format(FORMAT_REGISTRY_IDENTIFIER_CHILDREN_TABSET, parentIdentifier)));
    this.parentIdentifier = parentIdentifier;
  }

  /**
   * Get the standard window tab set locator.
   *
   * @return the canvas locator.
   */
  @Override
  protected String getSmartClientLocator() {
    // FIXME the wrong object is on the registry.
    String scLocator = "scLocator=" + SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format("%s.$v4.getLocator()", objectString));
    // XXX replace the negative index, that means that the item is not visible. This is necessary to
    // check for the item existence and visibility and wait for it to appear.
    return scLocator.replaceAll("index=-1", "index=1");
  }

  /**
   * Get the title of a child tab.
   *
   * @param childIdentifier
   *          The identifier of the child tab.
   * @return the title of the child tab.
   */
  private String getChildTabTitle(String childIdentifier) {
    return (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format(FORMAT_GET_CHILD_TAB_TITLE, TestRegistry.getObjectString(String
            .format(FORMAT_REGISTRY_IDENTIFIER_CHILDREN_TAB, parentIdentifier, childIdentifier))));
  }

  /**
   * Get the locator of a child tab.
   *
   * @param childIdentifier
   *          The identifier of the child tab.
   * @return the locator of a child tab.
   */
  @Override
  protected String getTabLocator(String childIdentifier) {
    return super.getTabLocator(getChildTabTitle(childIdentifier));
  }

  /**
   * Check if a tab is selected.
   *
   * @param childIdentifier
   *          The identifier of the child tab.
   * @return true if the tab is selected. Otherwise, false.
   */
  @Override
  public boolean isTabSelected(String childIdentifier) {
    return super.isTabSelected(childIdentifier);
  }
}
