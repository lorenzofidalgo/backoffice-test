/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Dialog;

/**
 * Executes actions on OpenbravoERP warn dialog built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class WarnDialog extends Dialog {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** String to use the object of the SmartClient warning dialog. */
  private static final String OBJECT_STRING = "window.isc.Dialog.Warn";

  /** Formatting string to get the ok button object. The parameter is the dialog object string. */
  private static final String FORMAT_OBJECT_STRING_BUTTON_OK = "%s.okButton";

  /** Formatting string to get the yes button object. The parameter is the dialog object string. */
  private static final String FORMAT_OBJECT_STRING_BUTTON_YES = "%s.yesButton";

  /**
   * Class constructor.
   *
   */
  public WarnDialog() {
    super(OBJECT_STRING);
  }

  /**
   * Click the OK button in the warning dialog.
   */
  public void clickOk() {
    logger.debug("Confirming the warning.");
    Button buttonOk = new Button(String.format(FORMAT_OBJECT_STRING_BUTTON_OK, objectString));
    buttonOk.smartClientClick();
  }

  /**
   * Click the yes button in the warning dialog.
   */
  public void clickYes() {
    logger.debug("Confirming the warning.");
    Button buttonYes = new Button(String.format(FORMAT_OBJECT_STRING_BUTTON_YES, objectString));
    buttonYes.smartClientClick();
  }

}
