/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.gui.ViewLauncher;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SelectItem;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verify actions on OpenbravoERP Create New widget added by Navigation Bar Components
 * module.
 *
 * There are three ways to open a window from this widget: typing it's name on the combo box,
 * selecting the name from the combo box and selecting the name from the recent windows list if it
 * was recently opened. The first way is the default.
 *
 * @author elopio
 *
 */
public class CreateNew implements ViewLauncher {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Component elements. */
  /** The quick launch button. */
  private final Button buttonCreateNew;
  /** The recent items form. */
  private RecentForm recentItemsForm;
  /** The role combo box. */
  private SelectItem comboBoxCreateNew;

  /* Registry keys. */
  /** Registry key of the Create New button. */
  private static final String REGISTRY_KEY_BUTTON_CREATE_NEW = "UINAVBA_RecentCreateList_BUTTON";
  /** Registry key of the Create New recent items form. */
  private static final String REGISTRY_KEY_LOCATOR_RECENT_FORM_CREATE_NEW = "UINAVBA_RecentCreateList_RECENTFORM";
  /** Registry key of the Create New text field. */
  private static final String REGISTRY_KEY_LOCATOR_COMBO_BOX_CREATE_NEW = "UINAVBA_RecentCreateList_FIELD";

  /** The mode that will be used to launch the view. */
  private Mode mode;

  /**
   * Enum with the values of the possible create new modes.
   *
   * @author elopio
   *
   */
  public enum Mode {

    /**
     * Launch a view selecting it from the combo box in the create new widget.
     */
    COMBO_BOX,
    /**
     * Launch a view entering its name on the text field in the create new widget.
     */
    TEXT_FIELD,
    /**
     * Launch a view selecting it from the recent items list in the create new widget.
     */
    RECENT_ITEMS
  }

  /**
   * Class constructor.
   *
   */
  CreateNew() {
    this.buttonCreateNew = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_CREATE_NEW));
    mode = Mode.COMBO_BOX;
  }

  /**
   * Set the mode to launch the view.
   *
   * @param mode
   *          The mode that will be used to launch the view.
   */
  public void setMode(Mode mode) {
    this.mode = mode;
  }

  /**
   * Check if the widget is open.
   *
   * @return true if the widget is open. Otherwise, false.
   */
  public boolean isOpen() {
    // XXX it would be better to use the parent component.
    return comboBoxCreateNew != null && comboBoxCreateNew.isPresent()
        && comboBoxCreateNew.isVisible();
  }

  /**
   * Open the widget.
   */
  public void open() {
    if (!isOpen()) {
      logger.debug("Opening the create new widget.");
      buttonCreateNew.smartClientClick();
      comboBoxCreateNew = new SelectItem(
          TestRegistry.getObjectString(REGISTRY_KEY_LOCATOR_COMBO_BOX_CREATE_NEW));
      recentItemsForm = new RecentForm(
          TestRegistry.getObjectString(REGISTRY_KEY_LOCATOR_RECENT_FORM_CREATE_NEW));
    }
  }

  /**
   * Close the widget.
   */
  public void close() {
    if (isOpen()) {
      logger.debug("Closing the create new widget.");
      buttonCreateNew.smartClientClick();
      comboBoxCreateNew = null;
      recentItemsForm = null;
      // TODO wait?
    }
  }

  /**
   * Wait until the widget is visible.
   */
  public void waitUntilVisible() {
    logger.debug("Waiting for the create new widget to open.");
    // FIXME wait for the parent component.
    comboBoxCreateNew.waitUntilVisible();
  }

  /**
   * Open a window with the parent tab on create new mode.
   *
   * @param windowName
   *          The name of the window to open.
   */
  public void createNew(String windowName) {
    logger.info("Launch the window '{}'.", windowName);
    switch (mode) {
      case COMBO_BOX:
        createNewFromCombo(windowName);
        break;
      case RECENT_ITEMS:
        createNewFromRecentItems(windowName);
        break;
      case TEXT_FIELD:
        createNewFromTextField(windowName);
        break;
    }

  }

  /**
   * Open a window with the parent tab on create new mode, selecting the window name from the combo
   * box.
   *
   * @param windowName
   *          The name of the window to open.
   */
  public void createNewFromCombo(String windowName) {
    open();
    logger.debug("Launching the window '{}' selecting its name on the combo box.", windowName);
    comboBoxCreateNew.selectItem(windowName, "_identifier");
  }

  /**
   * Open a window with the parent tab on create new mode, typing the window name on the combo box.
   *
   * @param windowName
   *          The name of the window to open.
   */
  public void createNewFromTextField(String windowName) {
    open();
    logger.debug("Launching the window '{}' typing its name on the combo box.", windowName);
    comboBoxCreateNew.enterText(windowName);
    Sleep.trySleep();
    comboBoxCreateNew.pressEnter();
  }

  /**
   * Open a window with the parent tab on create new mode, selecting it from the recent form.
   *
   * @param windowName
   *          The name of the window to open.
   */
  public void createNewFromRecentItems(String windowName) {
    open();
    // XXX click somewhere to hide the tool tip that might hide the link. This can be removed when
    // the canvas hover is implemented.
    comboBoxCreateNew.smartClientClick();
    logger.debug("Launching the window '{}' selecting its name on the recent items list.",
        windowName);
    recentItemsForm.selectRecentItem("New " + windowName);
  }

  /**
   * Launch a view with the parent tab on create new mode.
   *
   * @param menuPath
   *          The path on the menu to the view that will be launched.
   */
  @Override
  public void launch(String... menuPath) {
    // The window is opened only with the last part of the menu path, i.e, the window name.
    createNew(menuPath[menuPath.length - 1]);
  }
}
