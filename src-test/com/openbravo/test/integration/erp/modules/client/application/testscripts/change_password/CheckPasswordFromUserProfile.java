/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.openbravo.test.integration.erp.modules.client.application.testscripts.change_password;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.ChangePasswordData;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.UserProfile;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.UserProfile.PasswordTab;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test that PasswordStrengthChecker rejects a weak password in Change password in User Widget
 *
 * @author jarmendariz
 */
@RunWith(Parameterized.class)
public class CheckPasswordFromUserProfile extends CreateTemporaryUserTest {

  private static final String TEST_USER_PREFIX = "testpassword_";
  private static final String TEST_USER_PASSWORD = "test";
  private static final String TEST_USER_ROLE = "F&BUser";
  private static Logger logger = LogManager.getLogger();
  private ChangePasswordData changePasswordData;

  public CheckPasswordFromUserProfile(ChangePasswordData passwordData) {
    super();
    changePasswordData = passwordData;
  }

  @Parameters
  public static Collection<Object[]> SalesOrderValues() {
    Object[][] data = new Object[][] {
        { new ChangePasswordData.Builder().currentPassword(TEST_USER_PASSWORD)
            .newPassword("short")
            .newPasswordConfirmation("short")
            .build() } };
    return Arrays.asList(data);
  }

  @Test
  public void changeWeakPasswordShouldBeRejected() {
    loginWithTemporaryUser();
    PasswordTab passwordTab = changePasswordInUserWidget(changePasswordData);
    verifyNewPasswordErrorAppears(passwordTab);
  }

  private void verifyNewPasswordErrorAppears(PasswordTab passwordTab) {
    assertTrue("Password error is not visible.", passwordTab.isNewPasswordErrorVisible());
  }

  private void loginWithTemporaryUser() {
    logger.info("Creating test user");
    String testUser = TEST_USER_PREFIX + System.currentTimeMillis();
    createTestUser(testUser, TEST_USER_PASSWORD, TEST_USER_ROLE);

    logger.info("Moving to login page");
    logout();
    loginWith(new LogInData.Builder().userName(testUser).password(TEST_USER_PASSWORD).build());
  }

  private PasswordTab changePasswordInUserWidget(ChangePasswordData passwordData) {
    UserProfile componentUserProfile = mainPage.getNavigationBar().getComponentUserProfile();
    UserProfile.PasswordTab passwordTab = componentUserProfile.openPasswordTab();
    passwordTab.changePassword(passwordData);
    Sleep.trySleep();

    return passwordTab;
  }

}
