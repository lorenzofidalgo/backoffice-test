/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.help;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.openbravo.test.integration.erp.modules.client.application.gui.LoadingPopUp;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.HelpWidget;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.QuickLaunch;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test the opening of the Help widget with a window opened.
 *
 * @author rwngallego
 *
 */
public abstract class OpenHelpWindow extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The window to launch. */
  private final String window;

  /**
   * Class constructor.
   *
   * @param window
   *          The window to launch.
   */
  public OpenHelpWindow(String window) {
    this.window = window;
  }

  /**
   * Test the help button of the Navigation Bar. [NAV0110] Open Help for a window Having a window
   * open, display Help component and click in Help link
   */
  @Test
  public void helpPopUpShouldBeOpenedAndClosed() {
    // Open a window.
    logger.info("[NAV0110] Open Help for a window");
    final QuickLaunch componentQuickLaunch = mainPage.getNavigationBar().getComponentQuickLaunch();
    logger.info("Opening window");
    // TODO L1: Remove the following static sleep once it is stable
    Sleep.trySleep(5000);
    componentQuickLaunch.quickLaunchFromComboBox(window);
    mainPage.getTabSetMain().waitUntilTabIsOpen(window);

    final LoadingPopUp loadingPopUp = new LoadingPopUp();
    loadingPopUp.waitForDataToLoad();

    final HelpWidget componentHelpWidget = mainPage.getNavigationBar().getComponentHelp();
    componentHelpWidget.open();
    logger.info("Verifying About link is visible");
    componentHelpWidget.verifyAboutLinkVisible();
    logger.info("Verifying Help link is visible");
    Sleep.trySleep(4000);
    componentHelpWidget.verifyHelpLinkVisible();
    Sleep.trySleep(4000);
    componentHelpWidget.openHelp();
    mainPage.getTabSetMain().waitUntilTabIsOpen(window + " - Help");
    componentHelpWidget.verifyClosed();
  }
}
