/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.quicklaunch;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.QuickLaunch;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test that the quick launch widget added by Navigation Bar Components module can be properly
 * displayed and hidden.
 *
 * @author elopio
 *
 */
public class DisplayAndHideQuickLaunch extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Test the display and hide of create new. [NAV0060] Display and hide quick launch
   */
  @Test
  public void quickLaunchWidgetShouldBeDisplayedAndHidden() {
    QuickLaunch componentQuickLaunch = mainPage.getNavigationBar().getComponentQuickLaunch();
    logger.info("[NAV0060] Display and hide quick launch");
    logger.info("Click Quick Launch component to open it");
    componentQuickLaunch.open();
    assertTrue("Quick Launch widget was not opened.", componentQuickLaunch.isOpen());
    logger.info("Click Quick Launch component to close it");
    componentQuickLaunch.close();
    assertFalse("Quick Launch widget was not closed.", componentQuickLaunch.isOpen());
  }
}
