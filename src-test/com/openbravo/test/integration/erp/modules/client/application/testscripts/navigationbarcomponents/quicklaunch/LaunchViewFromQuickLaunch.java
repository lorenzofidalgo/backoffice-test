/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.quicklaunch;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.QuickLaunch;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test the launch of a view from the quick launcher added by Navigation Bar Components module.
 *
 * @author elopio
 *
 */
public class LaunchViewFromQuickLaunch extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The window to launch. */
  private final String window;

  /**
   * Class constructor.
   *
   * @param window
   *          The window to launch.
   */
  public LaunchViewFromQuickLaunch(String window) {
    this.window = window;
  }

  /**
   * Test view quick launch entering its name in the text field. [NAV0020a] Open window from quick
   * launch. Typing the name
   */
  @Ignore("An issue with seleniumserver in linux. Works in Windows")
  @Test
  public void viewShouldOpenEnteringNameInTextField() {
    logger.info("[NAV0020a] Open window from quick launch. Typing the name");
    final QuickLaunch componentQuickLaunch = mainPage.getNavigationBar().getComponentQuickLaunch();
    componentQuickLaunch.quickLaunchFromTextField(window);
    mainPage.getTabSetMain().waitUntilTabIsOpen(window);
  }

  /**
   * Test view quick launch selecting its name from the combo box. [NAV0020b] Open window from quick
   * launch. Selecting from combo box
   */
  @Test
  public void viewShouldOpenSelectingNameInComboBox() {
    logger
        .info(String.format("[NAV0020b] Open window from quick launch. Selecting from combo box"));
    QuickLaunch componentQuickLaunch = mainPage.getNavigationBar().getComponentQuickLaunch();
    componentQuickLaunch.quickLaunchFromComboBox(window);
    mainPage.getTabSetMain().waitUntilTabIsOpen(window);

    // Adding a static wait to avoid false positives on closing
    Sleep.trySleep();

    mainPage.getTabSetMain().closeTab(window);
    logger.info("Close window");
    mainPage.getTabSetMain().waitUntilTabIsClosed(window);

    logger.info("[NAV0020c] Open window from quick launch. Recent items");
    componentQuickLaunch = mainPage.getNavigationBar().getComponentQuickLaunch();
    logger.info("Select window from combo box to have a recent item");
    componentQuickLaunch.quickLaunchFromComboBox(window);
    mainPage.getTabSetMain().waitUntilTabIsOpen(window);

    mainPage.getTabSetMain().closeTab(window);
    logger.info("Close window");
    mainPage.getTabSetMain().waitUntilTabIsClosed(window);
    logger.info("Select window from recent items");
    componentQuickLaunch.quickLaunchFromRecentItems(window);
    mainPage.getTabSetMain().waitUntilTabIsOpen(window);
  }
}
