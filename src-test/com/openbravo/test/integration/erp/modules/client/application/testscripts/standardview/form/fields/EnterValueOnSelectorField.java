/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.form.fields;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Ignore;
import org.junit.Test;

import com.openbravo.test.integration.erp.data.selectors.SelectorDataObject;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SelectorField;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the entering of values in a selector field.
 *
 * @author elopio
 *
 */
public abstract class EnterValueOnSelectorField extends OpenbravoERPTest {

  /** The data to enter in the selector. */
  private final SelectorDataObject selectorData;

  /**
   * Class constructor.
   *
   * @param selectorData
   *          The data to enter.
   */
  public EnterValueOnSelectorField(SelectorDataObject selectorData) {
    this.selectorData = selectorData;
  }

  /**
   * Test the entering of values in a selector field using the selector pop up.
   */
  @Test
  public void valueShouldBeEnteredUsingTheSelectorPopUp() {
    SelectorField<SelectorDataObject> selectorField = getSelectorField();
    selectorField.selectFromPopUp(selectorData);
    assertThat(selectorField.getValue(), is(equalTo(selectorData.getDisplayValue())));
  }

  /**
   * Test the entering of values on the text field of a selector field.
   */
  @Ignore
  @Test
  public void valueShouldBeEnteredOnTheTextField() {
    SelectorField<SelectorDataObject> selectorField = getSelectorField();
    selectorField.enterText(selectorData);
    assertThat(selectorField.getValue(), is(equalTo(selectorData.getDisplayValue())));
  }

  /**
   * Test the entering of values in a selector field using the combo box.
   */
  @Test
  public void valueShouldBeEnteredUsingTheCombobox() {
    SelectorField<SelectorDataObject> selectorField = getSelectorField();
    selectorField.selectFromComboBox(selectorData);
    assertThat(selectorField.getValue(), is(equalTo(selectorData.getDisplayValue())));
  }

  /**
   * Test entering the search key in the text field and select the value from the list box.
   */
  @Test
  public void valueShouldEnteredUsingTheSearchKey() {
    SelectorField<SelectorDataObject> selectorField = getSelectorField();
    selectorField.selectEnteringSearchKey(selectorData);
    assertThat(selectorField.getValue(), is(equalTo(selectorData.getDisplayValue())));
  }

  /**
   * Open the tab that contains the the selector that will be used for the test and return the
   * selector.
   *
   * @return the selector that will be used for the test.
   */
  public abstract SelectorField<SelectorDataObject> getSelectorField();

}
