/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.form.sections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the visible and hidden sections of a form.
 *
 * @author caristu
 */
public abstract class CheckVisibleAndHiddenSections extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  protected static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  /** An array with the names of the sections that should be visible. */
  private final String[] visibleSections;
  /** An array with the names of the sections that should be hidden. */
  private final String[] hiddenSections;

  /**
   * Class constructor.
   *
   * @param visibleSections
   *          An array with the names of the sections that should be visible.
   * @param hiddenSections
   *          An array with the names of the sections that should be hidden.
   */
  public CheckVisibleAndHiddenSections(String[] visibleSections, String[] hiddenSections) {
    this.visibleSections = visibleSections;
    this.hiddenSections = hiddenSections;
  }

  /**
   * Test the visibility status of sections in the form.
   */
  @Test
  public void sectionsShouldHaveVisibilityStatusAsExpected() {
    logger.info("** Tab visibility checking started **");
    GeneratedTab<DataObject> tab = getTab();
    tab.select(getSelectedRecord());
    tab.open();
    for (String visibleSection : visibleSections) {
      tab.getForm().assertSectionVisible(visibleSection);
    }
    for (String hiddenSection : hiddenSections) {
      tab.getForm().assertSectionHidden(hiddenSection);
    }
    logger.info("** Tab visibility checking finished **");
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  public abstract GeneratedTab<DataObject> getTab();

  /**
   * Returns a DataObject which represents the record to be selected in the test.
   *
   * @return the selected record that will be used for the test.
   */
  public abstract DataObject getSelectedRecord();

}
