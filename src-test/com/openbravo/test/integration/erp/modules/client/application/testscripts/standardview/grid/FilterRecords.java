/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.grid;

import org.junit.Ignore;
import org.junit.Test;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the grid filters.
 *
 * @author elopio
 *
 */
public abstract class FilterRecords extends OpenbravoERPTest {

  /* Data for this test. */
  /** The filter data object. */
  private final DataObject filterDataObject;

  /**
   * Class constructor.
   *
   * @param filterDataObject
   *          The filter data object.
   */
  public FilterRecords(DataObject filterDataObject) {
    this.filterDataObject = filterDataObject;
  }

  /**
   * Test grid filtering.
   */
  @Ignore("Blocked by some change in the ERP.")
  @Test
  public void gridShouldBeFiltered() {
    final GeneratedTab<DataObject> tab = getTab();
    tab.clearFilters();
    tab.select(filterDataObject);
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  public abstract GeneratedTab<DataObject> getTab();
}
