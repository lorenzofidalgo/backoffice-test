/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.standardview.form.fields;

import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.HeaderTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.LinesTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.SalesOrderWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.form.fields.EnterValueOnField;
import com.openbravo.test.integration.util.ConfigurationProperties;
//FIXME: this test case needs revision; preconditions need to be more simple, not using the Business Partner selector field popup
//FIXME: the lines tab form is failing to fill the Product field, causing the execution to fail

/**
 * Test the entering of values in a number item field.
 *
 * @author elopio
 *
 */
@RunWith(Parameterized.class)
public class FIE0030_EnterNumberInNumberItem extends EnterValueOnField {

  /**
   * Class constructor.
   *
   * @param fieldName
   *          The name of the field.
   * @param fieldData
   *          The data to enter.
   */
  public FIE0030_EnterNumberInNumberItem(String fieldName, String fieldData) {
    super(fieldName, fieldData);
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<String[]> fieldValues() {
    String[][] data = new String[][] { { "orderedQuantity", "11.2" } };
    return Arrays.asList(data);
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  @SuppressWarnings("unchecked")
  @Override
  public GeneratedTab<DataObject> getTab() {
    mainPage.changeProfile(new ProfileData.Builder().role("QA Testing Admin - QA Testing")
        .client("QA Testing")
        .organization("*")
        .warehouse("Spain East warehouse")
        .build());
    SalesOrderWindow salesOrderWindow = new SalesOrderWindow();
    mainPage.openView(salesOrderWindow);
    HeaderTab headerTab = (HeaderTab) salesOrderWindow.selectTab(HeaderTab.IDENTIFIER);
    headerTab.createRecord(new SalesOrderHeaderData.Builder()
        .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
        .build());
    return (GeneratedTab<DataObject>) salesOrderWindow.selectTab(LinesTab.IDENTIFIER);
  }
}
