/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.standardview.form.sections;

import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.form.sections.CheckVisibleAndHiddenSections;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Test the visible and hidden sections of a form.
 *
 * @author caristu
 */
@RunWith(Parameterized.class)
public class SEC0030_CheckVisibleAndHiddenSections extends CheckVisibleAndHiddenSections {

  /**
   * Class constructor.
   *
   * @param visibleSections
   *          An array with the names of the sections that should be visible.
   * @param hiddenSections
   *          An array with the names of the sections that should be hidden.
   */
  public SEC0030_CheckVisibleAndHiddenSections(String[] visibleSections, String[] hiddenSections) {
    super(visibleSections, hiddenSections);
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<String[][]> sectionsValues() {
    final String auditSection = "1000100001";
    String[][][] data = new String[][][] {
        { new String[] { auditSection, "_notes_", "_linkedItems_", "_attachments_" },
            new String[] {} } };
    return Arrays.asList(data);
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  @SuppressWarnings("unchecked")
  @Override
  public GeneratedTab<DataObject> getTab() {
    mainPage.changeProfile(new ProfileData.Builder().role("QA Testing Admin - QA Testing")
        .client("QA Testing")
        .organization("*")
        .warehouse("Spain East warehouse")
        .build());

    ProductWindow productWindow = new ProductWindow();
    mainPage.openView(productWindow);
    GeneratedTab<DataObject> headerTab = (GeneratedTab<DataObject>) productWindow
        .selectTab(ProductTab.IDENTIFIER);
    return headerTab;
  }

  /**
   * Returns a DataObject which represents the record to be selected in the test.
   *
   * @return the selected record that will be used for the test.
   */
  @Override
  public DataObject getSelectedRecord() {
    final String selectedProduct = "Laptop";
    return new ProductData.Builder().name(selectedProduct).build();
  }
}
