/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.standardview.grid;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.HeaderTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.SalesOrderWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Asserts selecting an item in a filter drop down and reopening keeps the same available elements
 * to select
 *
 * See https://issues.openbravo.com/view.php?id=28386
 *
 * @author alostale
 *
 */
public class GRD0210_ReopenFilterDropDown extends OpenbravoERPTest {

  public GRD0210_ReopenFilterDropDown() {
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Test
  public void filterShouldHaveSameOptionsAfterReopening() {
    SalesOrderWindow salesOrderWindow = new SalesOrderWindow();
    mainPage.openView(salesOrderWindow);
    HeaderTab salesOrderTab = salesOrderWindow.selectHeaderTab();

    int availableDropDownOptions = salesOrderTab.openFilterDropDown("organization");
    assertThat(availableDropDownOptions, is(greaterThan(1)));

    salesOrderTab.pickRecordInFilterDropDown("organization", 0);

    assertThat("Options available in drop down list should stable after selecting and reopening",
        availableDropDownOptions, is(salesOrderTab.openFilterDropDown("organization")));
  }
}
