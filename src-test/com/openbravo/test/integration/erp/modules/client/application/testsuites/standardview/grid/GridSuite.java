/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.standardview.grid;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.SuiteThatStopsIfFailure;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Form fields test suite.
 *
 * @author elopio
 *
 */
@RunWith(SuiteThatStopsIfFailure.class)
@Suite.SuiteClasses({ GRD0000_CleanUOMWindow.class, GRD0030_FilterRecordsByBusinessPartner.class,
    GRD0040_FilterRecordsByProductSelector.class, GRD0050_InsertRow.class,
    GRD0055_InsertRowWithContextualMenu.class, GRD0056_InsertRowWithShortcut.class,
    GRD0060_CreateRecord.class, GRD0080_EditRecord.class,
    GRD0085_EditRecordUsingContextualMenu.class, GRD0086_EditRecordUsingDoubleClick.class,
    GRD0100_CancelEdition.class, GRD0105_CancelEditionWithShortcut.class,
    GRD0110_MoveToNextRecordWhileEditingInGrid.class,
    GRD0115_MoveToNextRecordWithTabKeyWhileEditingInGrid.class, GRD0120_DeleteRecord.class,
    GRD0125_DeleteRecordUsingContextualMenu.class, GRD0130_DeleteMultipleRecordsOneCheckbox.class,
    GRD0135_DeleteMultipleRecordsMultipleCheckboxes.class,
    GFI001_DateFilterPersistsAfterPressingKey.class,
    GFI002_DateFilterRemovedAfterClickingOnFunnel.class, GRD0140_NewAutomaticRecord.class,
    GRD0145_NewAutomaticRecordWithTabKey.class, GRD0150_CancelNewAutomaticRecord.class,
    GRD0220_SortByColumn.class, GDLa010_DisplayLogicInGrid.class,
    GDLa020_DisplayLogicAndShowColumns.class, GDLa050_ReadOnlyAndDisplayLogic.class

    /*
     * , GRD0200_NullifyField.class, commented out because of jenkins error
     * 
     * GRD0210_ReopenFilterDropDown.class
     */ })
public class GridSuite {
  // No content is required, this is just the definition of a test suite.
  @AfterClass
  public static void tearDown() {
    SeleniumSingleton.INSTANCE.quit();
    OpenbravoERPTest.seleniumStarted = false;
    OpenbravoERPTest.forceLoginRequired();
  }
}
