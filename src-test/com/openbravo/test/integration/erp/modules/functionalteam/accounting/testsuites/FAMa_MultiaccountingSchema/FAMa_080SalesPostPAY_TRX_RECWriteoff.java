/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  David Miguélez <david.miguelez@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.accounting.testsuites.FAMa_MultiaccountingSchema;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.ReconciliationsLinesData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;

/**
 * This test case posts a payment, transaction and reconciliation with a writeoff.
 *
 * @author David Miguélez
 */

@RunWith(Parameterized.class)
public class FAMa_080SalesPostPAY_TRX_RECWriteoff extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /* Data for [FAMa_080] Post a payment, transaction and reconciliation with a writeoff. */
  /** The sales order header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /**
   * Expected journal entry lines. Is an array of arrays. Each line has account number, name, debit
   * and credit.
   */
  private String[][] journalEntryLines;
  /** The payment in header data. */
  PaymentInHeaderData paymentInHeaderData;
  /** Expected journal entry lines for the payment in. */
  private String[][] paymentInJournalEntryLines;
  /** The account header data. */
  AccountData accountHeaderData;
  /** The transaction lines data. */
  TransactionsData transactionsHeaderData;
  /** Expected journal entry lines for the transaction. */
  private String[][] transactionJournalEntryLines;
  /** The reconciliations lines data. */
  ReconciliationsLinesData reconciliationsLinesHeaderData;
  /** Expected journal entry lines for the reconciliation. */
  private String[][] reconciliationJournalEntryLines;

  private String[][] journalEntryLines2;

  private String[][] paymentInJournalEntryLines2;

  private String[][] transactionJournalEntryLines2;

  private String[][] reconciliationJournalEntryLines2;

  /**
   * Class constructor.
   *
   * @param salesInvoiceHeaderData
   *          The sales invoice header data.
   * @param journalEntryLines
   *          Expected journal entry lines. Is an array of arrays. Each line has account number,
   *          name, debit and credit.
   * @param paymentInHeaderData
   *          The payment in header data.
   * @param paymentInJournalEntryLines
   *          Expected journal entry lines for the payment in
   * @param accountHeaderData
   *          The account Header Data.
   * @param transactionsHeaderData
   *          The transaction lines data.
   * @param transactionJournalEntryLines
   *          Expected journal entry lines for the transaction.
   * @param transactionJournalEntryLines2
   *          Expected journal entry lines for the transaction.
   * @param reconciliationsLinesHeaderData
   *          The reconciliations lines data.
   * @param reconciliationJournalEntryLines
   *          Expected journal entry lines for the reconciliation.
   * @param reconciliationJournalEntryLines2
   *          Expected journal entry lines for the reconciliation.
   */

  public FAMa_080SalesPostPAY_TRX_RECWriteoff(SalesInvoiceHeaderData salesInvoiceHeaderData,
      String[][] journalEntryLines, String[][] journalEntryLines2,
      PaymentInHeaderData paymentInHeaderData, String[][] paymentInJournalEntryLines,
      String[][] paymentInJournalEntryLines2, AccountData accountHeaderData,
      TransactionsData transactionsHeaderData, String[][] transactionJournalEntryLines,
      String[][] transactionJournalEntryLines2,
      ReconciliationsLinesData reconciliationsLinesHeaderData,
      String[][] reconciliationJournalEntryLines, String[][] reconciliationJournalEntryLines2) {

    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.journalEntryLines = journalEntryLines;
    this.journalEntryLines2 = journalEntryLines2;
    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInJournalEntryLines = paymentInJournalEntryLines;
    this.paymentInJournalEntryLines2 = paymentInJournalEntryLines2;
    this.accountHeaderData = accountHeaderData;
    this.transactionsHeaderData = transactionsHeaderData;
    this.transactionJournalEntryLines = transactionJournalEntryLines;
    this.transactionJournalEntryLines2 = transactionJournalEntryLines2;
    this.reconciliationsLinesHeaderData = reconciliationsLinesHeaderData;
    this.reconciliationJournalEntryLines = reconciliationJournalEntryLines;
    this.reconciliationJournalEntryLines2 = reconciliationJournalEntryLines2;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToCashValues() {

    return Arrays.asList(new Object[][] { {
        // Parameters for [FAMa_080] Post a payment, transaction and reconciliation with a
        // writeoff.
        new SalesInvoiceHeaderData.Builder().documentNo("I/15").build(),
        new String[][] { { "43000", "Clientes (euros)", "30.00", "" },
            { "70000", "Ventas de mercaderías", "", "30.00" } },
        new String[][] { { "11400", "Accounts receivable", "60.00", "" },
            { "41100", "Sales", "", "60.00" } },
        new PaymentInHeaderData.Builder().documentNo("400017").build(),
        new String[][] { { "43100", "Efectos comerciales en cartera", "25.00", "" },
            { "69400", "Pérdidas por deterioro de créditos por operaciones comerciales", "5.00",
                "" },
            { "43000", "Clientes (euros)", "", "30.00" } },
        new String[][] { { "11300", "Bank in transit", "100.00", "" },
            { "59400", "Bad debts", "20.00", "" },
            { "45200", "Bank revaluation gain", "", "60.00" },
            { "11400", "Accounts receivable", "", "60.00" } },
        new AccountData.Builder().name("Accounting Documents EURO").build(),
        new TransactionsData.Builder().documentNo("400017")
            .description("Invoice No.: I/15.")
            .build(),
        new String[][] { { "55500", "Partidas pendientes de aplicación", "25.00", "" },
            { "43100", "Efectos comerciales en cartera", "", "25.00" } },
        new String[][] { { "11200", "Bank Account", "75.00", "" },
            { "55400", "Bank revaluation loss", "25.00", "" },
            { "11300", "Bank in transit", "", "100.00" } },
        new ReconciliationsLinesData.Builder().documentNo("1000043").build(),
        new String[][] {
            { "57200", "Bancos e instituciones de crédito c/c vista euros", "25.00", "" },
            { "55500", "Partidas pendientes de aplicación", "", "25.00" } },
        new String[][] { { "11100", "Petty Cash", "75.00", "" },
            { "11200", "Bank Account", "", "75.00" }, } } });
  }

  /**
   * This test case posts a payment, transaction and reconciliation with a writeoff.
   */
  @Test
  public void FAMa_080PostPaymentTransactionReconciliationWriteOff() {
    logger.info(
        "** Start of test case [FAMa_080] Post a payment, transaction and reconciliation with a writeoff. **");

    final SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.select(salesInvoiceHeaderData);
    if (salesInvoice.isPosted()) {
      salesInvoice.unpost();
      salesInvoice.assertProcessCompletedSuccessfully2(
          "Number of unposted documents = 1, Number of deleted journal entries = 4");
    }
    salesInvoice.post();

    mainPage.closeView("Sales Invoice");

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines.length);
    post.assertJournalLines2(journalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    final PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentIn.select(paymentInHeaderData);
    if (paymentIn.isPosted()) {
      paymentIn.unpost();
      paymentIn.assertProcessCompletedSuccessfully2(
          "Number of unposted documents = 1, Number of deleted journal entries = 7");
    }
    paymentIn.post();

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(paymentInJournalEntryLines.length);
    post.assertJournalLines2(paymentInJournalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(paymentInJournalEntryLines2.length);
    post.assertJournalLines2(paymentInJournalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    final FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Transaction transaction = financialAccount.new Transaction(mainPage);
    transaction.select(transactionsHeaderData);
    if (transaction.isPosted()) {
      transaction.unpost();
      transaction.assertProcessCompletedSuccessfully2(
          "Number of unposted documents = 1, Number of deleted journal entries = 5");
    }
    transaction.post();

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(transactionJournalEntryLines.length);
    post.assertJournalLines2(transactionJournalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(transactionJournalEntryLines2.length);
    post.assertJournalLines2(transactionJournalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    FinancialAccount.Reconciliations reconciliations = financialAccount.new Reconciliations(
        mainPage);
    reconciliations.select(reconciliationsLinesHeaderData);
    if (reconciliations.isPosted()) {
      reconciliations.unpost();
      reconciliations.assertProcessCompletedSuccessfully2(
          "Number of unposted documents = 1, Number of deleted journal entries = 4");
    }
    reconciliations.post();

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(reconciliationJournalEntryLines.length);
    post.assertJournalLines2(reconciliationJournalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(reconciliationJournalEntryLines2.length);
    post.assertJournalLines2(reconciliationJournalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    logger.info(
        "** End of test case [FAMa_080] Post a payment, transaction and reconciliation with a writeoff. **");
  }

}
