/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  David Miguélez <david.miguelez@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.accounting.testsuites.FAMa_MultiaccountingSchema;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;

/**
 * This test case posts a payment.
 *
 * @author David Miguélez
 */

@RunWith(Parameterized.class)
public class FAMa_140PurchasePostPAY extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /* Data for [FAMa_140] Post a payment. */
  /** The purchase invoice header data. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  /**
   * Expected journal entry lines. Is an array of arrays. Each line has account number, name, debit
   * and credit.
   */
  private String[][] journalEntryLines;
  /** The payment out header data. */
  PaymentOutHeaderData paymentOutHeaderData;
  /** Expected journal entry lines for the payment out. */
  private String[][] paymentOutJournalEntryLines;

  private String[][] journalEntryLines2;

  private String[][] paymentOutJournalEntryLines2;

  /** The account header data. */

  /**
   * Class constructor.
   *
   * @param purchaseInvoiceHeaderData
   *          The purchase invoice header data.
   * @param journalEntryLines
   *          Expected journal entry lines. Is an array of arrays. Each line has account number,
   *          name, debit and credit.
   * @param paymentOutHeaderData
   *          The payment out header data.
   * @param paymentOutJournalEntryLines
   *          Expected journal entry lines for the payment out.
   */

  public FAMa_140PurchasePostPAY(PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      String[][] journalEntryLines, String[][] journalEntryLines2,
      PaymentOutHeaderData paymentOutHeaderData, String[][] paymentOutJournalEntryLines,
      String[][] paymentOutJournalEntryLines2) {

    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.journalEntryLines = journalEntryLines;
    this.journalEntryLines2 = journalEntryLines2;
    this.paymentOutHeaderData = paymentOutHeaderData;
    this.paymentOutJournalEntryLines = paymentOutJournalEntryLines;
    this.paymentOutJournalEntryLines2 = paymentOutJournalEntryLines2;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToCashValues() {

    return Arrays.asList(new Object[][] { {
        // Parameters for [FAMa_140] Post a payment.
        new PurchaseInvoiceHeaderData.Builder().documentNo("10000004").build(),
        new String[][] { { "60000", "Compras de mercaderías", "10.00", "" },
            { "40000", "Proveedores (euros)", "", "10.00" } },
        new String[][] { { "51200", "Service costs", "20.00", "" },
            { "21100", "Accounts payable", "", "20.00" } },
        new PaymentOutHeaderData.Builder().documentNo("700004").build(),
        new String[][] { { "40000", "Proveedores (euros)", "10.00", "" },
            { "57200", "Bancos e instituciones de crédito c/c vista euros", "", "10.00" } },
        new String[][] { { "55400", "Bank revaluation loss", "20.00", "" },
            { "21100", "Accounts payable", "20.00", "" },
            { "11100", "Petty Cash", "", "40.00" } } } });
  }

  /**
   * This test case post a payment and reconciliation.
   */
  @Test
  public void FAMa_140PostPayment() {
    logger.info("** Start of test case [FAMa_140] Post a payment. **");

    final PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.select(purchaseInvoiceHeaderData);
    if (purchaseInvoice.isPosted()) {
      purchaseInvoice.unpost();
      // purchaseInvoice
      // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
      // journal entries = 2");
    }
    purchaseInvoice.post();

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines.length);
    post.assertJournalLines2(journalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    final PaymentOut paymentOut = new PaymentOut(mainPage).open();
    paymentOut.select(paymentOutHeaderData);
    if (paymentOut.isPosted()) {
      paymentOut.unpost();
      // paymentOut
      // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
      // journal entries = 2");
    }
    paymentOut.post();

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(paymentOutJournalEntryLines.length);
    post.assertJournalLines2(paymentOutJournalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(paymentOutJournalEntryLines2.length);
    post.assertJournalLines2(paymentOutJournalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    logger.info("** End of test case [FAMa_140] Post a payment. **");
  }

}
