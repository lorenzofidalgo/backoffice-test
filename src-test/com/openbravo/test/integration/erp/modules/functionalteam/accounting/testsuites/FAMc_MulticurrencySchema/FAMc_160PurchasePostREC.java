/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  David Miguélez <david.miguelez@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.accounting.testsuites.FAMc_MulticurrencySchema;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.ReconciliationsLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;

/**
 * This test case posts a reconciliation.
 *
 * @author David Miguélez
 */

@RunWith(Parameterized.class)
public class FAMc_160PurchasePostREC extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /* Data for [FAMc_160] Post a reconciliation. */
  /** The purchase invoice header data. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  /**
   * Expected journal entry lines. Is an array of arrays. Each line has account number, name, debit
   * and credit.
   */
  private String[][] journalEntryLines1;
  private String[][] journalEntryLines2;
  /** The payment out header data. */
  AccountData accountHeaderData;
  /** The reconciliation lines data. */
  ReconciliationsLinesData reconciliationsLinesHeaderData;
  /** Expected journal entry lines for the reconciliation. */
  private String[][] reconciliationJournalEntryLines1;
  private String[][] reconciliationJournalEntryLines2;

  /**
   * Class constructor.
   *
   * @param purchaseInvoiceHeaderData
   *          The purchase invoice header data.
   * @param journalEntryLines1
   *          Expected journal entry lines. Is an array of arrays. Each line has account number,
   *          name, debit and credit.
   * @param journalEntryLines2
   *          Expected journal entry lines. Is an array of arrays. Each line has account number,
   *          name, debit and credit.
   * @param accountHeaderData
   *          The account Header Data.
   * @param reconciliationsLinesHeaderData
   *          The reconciliation lines data.
   * @param reconciliationJournalEntryLines1
   *          Expected journal entry lines for the reconciliation.
   * @param reconciliationJournalEntryLines2
   *          Expected journal entry lines for the reconciliation.
   */

  public FAMc_160PurchasePostREC(PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      String[][] journalEntryLines1, String[][] journalEntryLines2, AccountData accountHeaderData,
      ReconciliationsLinesData reconciliationsLinesHeaderData,
      String[][] reconciliationJournalEntryLines1, String[][] reconciliationJournalEntryLines2) {

    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.journalEntryLines1 = journalEntryLines1;
    this.journalEntryLines2 = journalEntryLines2;
    this.accountHeaderData = accountHeaderData;
    this.reconciliationsLinesHeaderData = reconciliationsLinesHeaderData;
    this.reconciliationJournalEntryLines1 = reconciliationJournalEntryLines1;
    this.reconciliationJournalEntryLines2 = reconciliationJournalEntryLines2;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToCashValues() {

    return Arrays.asList(new Object[][] { {
        // Parameters for [FAMc_160] Post a reconciliation.
        new PurchaseInvoiceHeaderData.Builder().documentNo("10000008").build(),
        new String[][] { { "60000", "Compras de mercaderías", "5.00", "" },
            { "40000", "Proveedores (euros)", "", "5.00" } },
        new String[][] { { "51200", "Service costs", "10.00", "" },
            { "21100", "Accounts payable", "", "10.00" } },
        new AccountData.Builder().name("Accounting Documents DOLLAR").build(),
        new ReconciliationsLinesData.Builder().documentNo("1000053").build(),
        new String[][] { { "40000", "Proveedores (euros)", "5.00", "" },
            { "76800", "Diferencias positivas de cambio", "", "4.17" },
            { "57200", "Bancos e instituciones de crédito c/c vista euros", "", "0.83" } },
        new String[][] { { "21100", "Accounts payable", "10.00", "" },
            { "45200", "Bank revaluation gain", "", "7.50" },
            { "11100", "Petty Cash", "", "2.50" } } } });
  }

  /**
   * This test case posts a reconciliation.
   */
  @Test
  public void FAMc_160PurchasePostReconciliation() {
    logger.info("** Start of test case [FAMc_160] Post a reconciliation. **");

    final PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.select(purchaseInvoiceHeaderData);
    if (purchaseInvoice.isPosted()) {
      purchaseInvoice.unpost();
      // purchaseInvoice
      // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
      // journal entries = 2");
    }
    purchaseInvoice.post();
    mainPage.closeView("Purchase Invoice");

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines1.length);
    post.assertJournalLines2(journalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    final FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Reconciliations reconciliations = financialAccount.new Reconciliations(
        mainPage);
    reconciliations.select(reconciliationsLinesHeaderData);
    if (reconciliations.isPosted()) {
      reconciliations.unpost();
      // reconciliations
      // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
      // journal entries = 2");
    }
    reconciliations.post();

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(reconciliationJournalEntryLines1.length);
    post.assertJournalLines2(reconciliationJournalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(reconciliationJournalEntryLines2.length);
    post.assertJournalLines2(reconciliationJournalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");
    logger.info("** End of test case [FAMc_160] Post a reconciliation. **");
  }

}
