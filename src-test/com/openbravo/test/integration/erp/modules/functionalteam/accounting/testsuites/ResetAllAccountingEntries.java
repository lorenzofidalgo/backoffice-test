/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  David Miguélez <david.miguelez@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.accounting.testsuites;

import org.junit.AfterClass;
import org.junit.Test;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.generalsetup.processscheduling.processrequest.ProcessRequestData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.transactions.ResetAccounting;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.sessionpreferences.SessionPreferences;
import com.openbravo.test.integration.erp.testscripts.generalsetup.processscheduling.processrequest.ProcessRequest;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Reset the accounting data, not required, but recommended for all the accounting test suites.
 *
 * @author David Miguélez
 */

public class ResetAllAccountingEntries extends OpenbravoERPTest {

  /**
   * Class constructor.
   */
  public ResetAllAccountingEntries() {
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Resets the accounting data, unschedules the process that posts entries automatically and
   * verifies that QAAdmin user can posts entries.
   */
  @Test
  public void resetAllAccountingEntries() {
    ProcessRequest processRequest = new ProcessRequest(mainPage).open();
    processRequest.select(
        new ProcessRequestData.Builder().process("Acct Server Process").organization("*").build());
    if (processRequest.unscheduleProcess()) {
      processRequest.assertProcessUnscheduleSucceeded();
    }
    Sleep.trySleep();
    SessionPreferences sessionPreferences = new SessionPreferences(mainPage).open();
    sessionPreferences.checkShowAccountingTabs();
    ResetAccounting resetAccounting = new ResetAccounting(mainPage).open();
    resetAccounting.resetAccounting("QA Testing", "USA");
  }

  @AfterClass
  public static void tearDown() {
    SeleniumSingleton.INSTANCE.quit();
    OpenbravoERPTest.seleniumStarted = false;
    OpenbravoERPTest.forceLoginRequired();
  }
}
