/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_AddPaymentRefactor.APR_AddPaymentRefactor1;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.sharedtabs.UsedCreditSourceData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Refunding a credit payment.
 *
 */
@RunWith(Parameterized.class)
public class APRPurchase007 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  private static final String PARAM_OVERPYMT_ACTION = "overpayment_action";
  /* Data for this test. */
  /** The payment in header data. */
  PaymentOutHeaderData paymentOutHeaderData;
  /** The data to verify the totals data in the add payment pop up. */
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  UsedCreditSourceData usedCreditSourceData;
  PaymentOutHeaderData paymentOutHeaderDatacredit;
  AddPaymentPopUpData addPaymentTotalsVerificationDatacredit;

  /**
   * Class constructor.
   *
   * @param paymentOutHeaderDatacredit
   * @param addPaymentTotalsVerificationDatacredit
   *          The data to verify the data in the add payment pop up.
   * @param paymentOutHeaderData
   * @param addPaymentTotalsVerificationData
   *          The data to verify the data in the add payment pop up.
   */
  public APRPurchase007(PaymentOutHeaderData paymentOutHeaderDatacredit,
      AddPaymentPopUpData addPaymentTotalsVerificationDatacredit,
      PaymentOutHeaderData paymentOutHeaderData,
      AddPaymentPopUpData addPaymentTotalsVerificationData) {
    this.paymentOutHeaderDatacredit = paymentOutHeaderDatacredit;
    this.addPaymentTotalsVerificationDatacredit = addPaymentTotalsVerificationDatacredit;
    this.paymentOutHeaderData = paymentOutHeaderData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToOrderValues() {

    return Arrays
        .asList(
            new Object[][] { {
                new PaymentOutHeaderData.Builder().organization("Spain")
                    .businessPartner(
                        new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
                    .paymentMethod("1 (Spain)")
                    .generatedCredit("100.00")
                    .build(),
                new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
                    .amount_inv_ords("0.00")
                    .total("0.00")
                    .difference("100.00")
                    .build(),
                new PaymentOutHeaderData.Builder()
                    .businessPartner(
                        new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
                    .paymentMethod("1 (Spain)")
                    .generatedCredit("0.00")
                    .build(),
                new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
                    .amount_inv_ords("0.00")
                    .total("0.00")
                    .difference("100.00")
                    .build(), } });
  }

  /**
   * Test the log in and log out.
   */
  @Test
  public void APRPurchase007Test() {

    logger.info("** Start of test case [APRPurchase007] Refunding a credit payment **");

    PaymentOut paymentOutcredit = new PaymentOut(mainPage).open();
    paymentOutcredit.create(paymentOutHeaderDatacredit);
    paymentOutcredit.assertSaved();
    String documentnocredit = paymentOutcredit.getData("documentNo").toString();
    AddPaymentProcess addPaymentProcesscredit = paymentOutcredit.addDetailsOpen();
    addPaymentProcesscredit.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcesscredit.assertTotalsData(addPaymentTotalsVerificationDatacredit);
    addPaymentProcesscredit.process("Process Made Payment(s)");
    PaymentOut paymentOut = new PaymentOut(mainPage).open();
    paymentOut.create(paymentOutHeaderData);
    AddPaymentProcess addPaymentProcess = paymentOut.addDetailsOpen();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.getOrderInvoiceGrid().filter("invoiceNo", "nothing");
    Sleep.trySleep(1000);
    addPaymentProcess.getCreditToUseGrid().waitForDataToLoad();
    addPaymentProcess.getCreditToUseGrid().filter("documentNo", documentnocredit);
    Sleep.trySleep(1000);
    addPaymentProcess.getCreditToUseGrid().selectRecord(0);
    addPaymentProcess.getField("reference_no").focus();
    addPaymentProcess.assertTotalsData(addPaymentTotalsVerificationData);
    addPaymentProcess.setParameterValue(PARAM_OVERPYMT_ACTION, "Refund amount to customer");
    addPaymentProcess.process("Process Made Payment(s)");
    paymentOut.assertProcessCompletedSuccessfully2();

    PaymentOut.UsedCreditSource usedCreditSource = paymentOut.new UsedCreditSource(mainPage);
    usedCreditSource
        .assertData(new UsedCreditSourceData.Builder().creditPaymentUsed(documentnocredit)
            .amount("100")
            .build());

    logger.info("** End of test case [APRPurchase007] Refunding a credit payment **");
  }
}
