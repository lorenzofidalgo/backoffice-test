/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_AddPaymentRefactor.APR_AddPaymentRefactor2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Payment Write off.
 *
 * @author Unai Martirena
 */
@RunWith(Parameterized.class)
public class APRSales008 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  private static final String PARAM_ACTUAL_PAYMENT = "actual_payment";
  private static final String PARAM_REFERENCE_NO = "reference_no";

  /* Data for this test. */

  /* Data for [APRSales008] Create Sales Invoice. */
  /** The Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /** The data to verify the creation of the Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  /** The data to verify the completion of the sales invoice. */
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  /** The data to verify the creation of the Sales Invoice lines data. */
  SalesInvoiceLinesData salesInvoiceLineData;
  /** The data to verify the creation of the Sales Invoice line. */
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  /** The data to verify the payment in plan. */
  PaymentPlanData paymentInPlanData;

  /* Data for [APRSales008] Add Payment. */
  /** The data to verify the data in the add payment pop up. */
  AddPaymentPopUpData addPaymentInVerificationData;
  /** The data to verify the totals data in the add payment pop up. */
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  /** The data to verify the application of the payment. */
  SalesInvoiceHeaderData payedSalesInvoiceHeaderVerificationData;

  /** The data to verify the payment in plan. */
  PaymentPlanData paymentInPlanData2;
  /** The data to verify the payment in details data. */
  PaymentDetailsData paymentInDetailsData;

  /**
   * Class constructor.
   *
   * @param salesInvoiceHeaderData
   *          The Sales Invoice header data.
   * @param salesInvoiceHeaderVerificationData
   *          The data to verify the creation of the Sales Invoice header data.
   * @param completedSalesInvoiceHeaderVerificationData
   *          The data to verify the completion of the sales invoice.
   * @param salesInvoiceLineData
   *          The data to verify the creation of the Sales Invoice lines data.
   * @param salesInvoiceLineVerificationData
   *          The data to verify the creation of the Sales Invoice line.
   * @param paymentInPlanData
   *          The data to verify the payment in plan.
   * @param payedSalesInvoiceHeaderVerificationData
   *          The data to verify the application of the payment.
   */
  public APRSales008(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData, PaymentPlanData paymentInPlanData,
      AddPaymentPopUpData addPaymentInVerificationData,
      AddPaymentPopUpData addPaymentTotalsVerificationData,
      SalesInvoiceHeaderData payedSalesInvoiceHeaderVerificationData,
      PaymentPlanData paymentInPlanData2, PaymentDetailsData paymentInDetailsData) {
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.paymentInPlanData = paymentInPlanData;
    this.addPaymentInVerificationData = addPaymentInVerificationData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.payedSalesInvoiceHeaderVerificationData = payedSalesInvoiceHeaderVerificationData;
    this.paymentInPlanData2 = paymentInPlanData2;
    this.paymentInDetailsData = paymentInDetailsData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        /* Parameters for [APRSales008] Create Sales Invoice. */
        new SalesInvoiceHeaderData.Builder().transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("103.50")
            .documentStatus("Completed")
            .summedLineAmount("100.00")
            .grandTotalAmount("103.50")
            .currency("EUR")
            .paymentComplete(false)
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("FGA")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("50")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A").build())
            .invoicedQuantity("50")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("100.00")
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("1 (Spain)")
            .expected("103.50")
            .received("0.00")
            .outstanding("103.50")
            .build(),
        /* Data for [APRSales008] Add Payment. */
        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .expected_payment("103.50")
            .actual_payment("103.50")
            .payment_date(OBDate.CURRENT_DATE)
            .transaction_type("Invoices")
            .build(),
        new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
            .amount_inv_ords("103.00")
            .total("103.00")
            .difference("0.00")
            .build(),
        new SalesInvoiceHeaderData.Builder().paymentComplete(true)
            .documentStatus("Completed")
            .summedLineAmount("100.00")
            .grandTotalAmount("103.50")
            .currency("EUR")
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("1 (Spain)")
            .expected("103.50")
            .received("103.50")
            .outstanding("0.00")
            .build(),
        new PaymentDetailsData.Builder().paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .received("103.00")
            .amount("103.50")
            .writeoff("0.50")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test to create a sales order, and a goods receipt and sales invoice from that order.
   *
   * @throws ParseException
   */
  @Test
  public void APRSales008Test() throws ParseException {
    logger.info("** Start of test case [APRSales008] Payment Write off. **");
    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.create(salesInvoiceLineData);
    salesInvoiceLines.assertSaved();
    salesInvoiceLines.assertData(salesInvoiceLineVerificationData);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    SalesInvoice.PaymentInPlan paymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    paymentInPlan.assertCount(1);
    paymentInPlan.assertData(paymentInPlanData);

    AddPaymentProcess addPaymentProcess = salesInvoice.openAddPayment();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertData(addPaymentInVerificationData);

    // Set payment to 103 and check write off
    addPaymentProcess.getOrderInvoiceGrid()
        .filter("invoiceNo", salesInvoice.getData("documentNo").toString());
    addPaymentProcess.setParameterValue(PARAM_ACTUAL_PAYMENT, "103.00");
    addPaymentProcess.getOrderInvoiceGrid().selectRecord(0);
    addPaymentProcess.editOrderInvoiceRecord(0,
        new SelectedPaymentData.Builder().amount("103.00").writeoff(true).build());
    Sleep.trySleep(2000);
    addPaymentProcess.getField(PARAM_REFERENCE_NO).focus();
    addPaymentProcess.scrollToBottom();
    addPaymentProcess.assertTotalsData(addPaymentTotalsVerificationData);
    addPaymentProcess.process("Process Received Payment(s)");

    salesInvoice.assertPaymentCreatedSuccessfully();
    salesInvoice.assertData(payedSalesInvoiceHeaderVerificationData);

    paymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    paymentInPlan.assertCount(1);
    paymentInPlan.assertData(paymentInPlanData2);
    SalesInvoice.PaymentInPlan.PaymentInDetails paymentInDetails = paymentInPlan.new PaymentInDetails(
        mainPage);
    paymentInDetails.assertCount(1);
    paymentInDetails.assertData(paymentInDetailsData);

    logger.info("** End of test case [APRSales008] Payment Write off. **");
  }
}
