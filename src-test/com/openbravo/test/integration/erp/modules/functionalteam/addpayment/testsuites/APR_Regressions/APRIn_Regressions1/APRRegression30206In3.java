/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 30206
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression30206In3 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PaymentInHeaderData paymentInHeaderData;
  PaymentInHeaderData paymentInHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  PaymentInHeaderData paymentInHeaderVerificationData2;
  PaymentInHeaderData paymentInHeaderVerificationData3;

  /**
   * Class constructor.
   *
   */
  public APRRegression30206In3(PaymentInHeaderData paymentInHeaderData,
      PaymentInHeaderData paymentInHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      PaymentInHeaderData paymentInHeaderVerificationData2,
      PaymentInHeaderData paymentInHeaderVerificationData3) {
    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.paymentInHeaderVerificationData2 = paymentInHeaderVerificationData2;
    this.paymentInHeaderVerificationData3 = paymentInHeaderVerificationData3;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new PaymentInHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("3.1 (USA)")
            .amount("20.00")
            .build(),
        new PaymentInHeaderData.Builder().documentType("AR Receipt")
            .account("USA Bank - USD")
            .currency("USD")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("3.1 (USA)")
            .fin_financial_account_id("USA Bank - USD")
            .c_currency_id("USD")
            .actual_payment("20.00")
            .expected_payment("0.00")
            .amount_gl_items("20.00")
            .amount_inv_ords("0.00")
            .total("20.00")
            .difference("0.00")
            .build(),

        new PaymentInHeaderData.Builder().status("Awaiting Execution")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),
        new PaymentInHeaderData.Builder().status("Deposited not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 30206 - Payment In flow
   *
   * Payment Method: Automatic deposit: Yes - Execution Type: Automatic - Execution process: Simple
   * execution process - Deferred: yes
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression30206In3Test() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression30206In3] Test regression 30206 - Payment In flow - Payment Method: Automatic deposit: Yes - Execution Type: Automatic - Execution process: Simple execution process - Deferred: yes. **");

    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertSaved();
    paymentIn.assertData(paymentInHeaderVerificationData);

    AddPaymentProcess addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.addGLItem(
        new SelectedPaymentData.Builder().gLItem("Salaries").receivedIn("20.00").build());
    addPaymentProcess.getField("reference_no").focus();
    addPaymentProcess.assertData(addPaymentVerificationData);
    addPaymentProcess.process("Process Received Payment(s) and Deposit");

    paymentIn.assertPaymentCreatedSuccessfully();
    paymentIn.assertData(paymentInHeaderVerificationData2);
    paymentIn.execute();
    Sleep.trySleepWithMessage(7000, "Sleeping to avoid 'Awaiting Execution' false positive");
    paymentIn.assertData(paymentInHeaderVerificationData3);

    logger.info(
        "** End of test case [APRRegression30206In3] Test regression 30206 - Payment In flow - Payment Method: Automatic deposit: Yes - Execution Type: Automatic - Execution process: Simple execution process - Deferred: yes. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
