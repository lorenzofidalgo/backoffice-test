/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions2;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.BusinessPartnerData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.CustomerData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.masterdata.businesspartner.BusinessPartner;

/**
 * Test regression 29368
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression29368In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  BusinessPartnerData businessPartnerData;
  CustomerData customerData;
  PaymentInHeaderData paymentInHeaderData;
  PaymentInHeaderData paymentInHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  PaymentInHeaderData paymentInHeaderData2;
  PaymentInHeaderData paymentInHeaderVerificationData2;
  AddPaymentPopUpData addPaymentVerificationData2;
  PaymentInHeaderData paymentInHeaderData3;
  PaymentInHeaderData paymentInHeaderVerificationData3;
  AddPaymentPopUpData addPaymentVerificationData3;
  CustomerData customerData2;
  AccountData accountHeaderData;
  PaymentMethodData paymentMethodData;
  PaymentMethodData paymentMethodData2;
  PaymentMethodData paymentMethodData3;
  AccountData accountHeaderData2;
  PaymentMethodData paymentMethodData4;
  AccountData accountHeaderData3;
  PaymentMethodData paymentMethodData5;
  PaymentMethodData paymentMethodData6;
  CustomerData customerData3;

  /**
   * Class constructor.
   *
   */
  public APRRegression29368In(BusinessPartnerData businessPartnerData, CustomerData customerData,
      PaymentInHeaderData paymentInHeaderData, PaymentInHeaderData paymentInHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData, PaymentInHeaderData paymentInHeaderData2,
      PaymentInHeaderData paymentInHeaderVerificationData2,
      AddPaymentPopUpData addPaymentVerificationData2, PaymentInHeaderData paymentInHeaderData3,
      PaymentInHeaderData paymentInHeaderVerificationData3,
      AddPaymentPopUpData addPaymentVerificationData3, CustomerData customerData2,
      AccountData accountHeaderData, PaymentMethodData paymentMethodData,
      PaymentMethodData paymentMethodData2, PaymentMethodData paymentMethodData3,
      AccountData accountHeaderData2, PaymentMethodData paymentMethodData4,
      AccountData accountHeaderData3, PaymentMethodData paymentMethodData5,
      PaymentMethodData paymentMethodData6, CustomerData customerData3) {
    this.businessPartnerData = businessPartnerData;
    this.customerData = customerData;
    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.paymentInHeaderData2 = paymentInHeaderData2;
    this.paymentInHeaderVerificationData2 = paymentInHeaderVerificationData2;
    this.addPaymentVerificationData2 = addPaymentVerificationData2;
    this.paymentInHeaderData3 = paymentInHeaderData3;
    this.paymentInHeaderVerificationData3 = paymentInHeaderVerificationData3;
    this.addPaymentVerificationData3 = addPaymentVerificationData3;
    this.customerData2 = customerData2;
    this.accountHeaderData = accountHeaderData;
    this.paymentMethodData = paymentMethodData;
    this.paymentMethodData2 = paymentMethodData2;
    this.paymentMethodData3 = paymentMethodData3;
    this.accountHeaderData2 = accountHeaderData2;
    this.paymentMethodData4 = paymentMethodData4;
    this.accountHeaderData3 = accountHeaderData3;
    this.paymentMethodData5 = paymentMethodData5;
    this.paymentMethodData6 = paymentMethodData6;
    this.customerData3 = customerData3;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new BusinessPartnerData.Builder().name("Customer B").build(),
        new CustomerData.Builder().paymentMethod("").account("").build(),

        new PaymentInHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer B").build())
            .build(),
        new PaymentInHeaderData.Builder().organization("Spain")
            .documentType("AR Receipt")
            .paymentMethod("1 (Spain)")
            .account("Spain Bank - EUR")
            .amount("0.00")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Customer B")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("0.00")
            .expected_payment("0.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("0.00")
            .total("0.00")
            .difference("0.00")
            .build(),

        new PaymentInHeaderData.Builder().paymentMethod("Acc-8 (Transactions)").build(),
        new PaymentInHeaderData.Builder().organization("Spain")
            .documentType("AR Receipt")
            .paymentMethod("Acc-8 (Transactions)")
            .account("Spain Bank - EUR")
            .amount("0.00")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Customer B")
            .fin_paymentmethod_id("Acc-8 (Transactions)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("0.00")
            .expected_payment("0.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("0.00")
            .total("0.00")
            .difference("0.00")
            .build(),

        new PaymentInHeaderData.Builder().account("Mexico Bank - USD").build(),
        new PaymentInHeaderData.Builder().organization("Spain")
            .documentType("AR Receipt")
            .paymentMethod("Acc-8 (Transactions)")
            .account("Mexico Bank - USD")
            .amount("0.00")
            .currency("USD")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Customer B")
            .fin_paymentmethod_id("Acc-8 (Transactions)")
            .fin_financial_account_id("Mexico Bank - USD")
            .c_currency_id("USD")
            .actual_payment("0.00")
            .expected_payment("0.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("0.00")
            .total("0.00")
            .difference("0.00")
            .build(),

        new CustomerData.Builder().paymentMethod("Acc-8 (Transactions)")
            .account("Spain Bank")
            .build(),

        new AccountData.Builder().name("Spain Bank").build(),
        new PaymentMethodData.Builder().paymentMethod("Acc-8 (Transactions)").build(),
        new PaymentMethodData.Builder().payinAllow(false).payinMulticurrencyAllow(true).build(),

        new PaymentMethodData.Builder().payinAllow(true).payinMulticurrencyAllow(false).build(),

        new AccountData.Builder().active(false).build(),
        new PaymentMethodData.Builder().payinAllow(true).payinMulticurrencyAllow(true).build(),

        new AccountData.Builder().active(true).build(),
        new PaymentMethodData.Builder().active(false).build(),

        new PaymentMethodData.Builder().active(true).build(),

        new CustomerData.Builder().paymentMethod("4 (Spain)").account("Spain Bank").build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 29368 - Payment In flow
   *
   * @throws ParseException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void APRRegression29368InTest() throws ParseException, OpenbravoERPTestException {
    logger.info(
        "** Start of test case [APRRegression29368In] Test regression 29368 - Payment In flow. **");

    // No default payment method and no default financial account for Customer B
    BusinessPartner.BusinessPartnerTab.select(mainPage, businessPartnerData);
    BusinessPartner.Customer.edit(mainPage, customerData);
    BusinessPartner.BusinessPartnerTab.close(mainPage);

    // Payment Method: 1 (Spain), Financial Account: Spain Bank, Currency: not enabled
    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertSaved();
    paymentIn.assertData(paymentInHeaderVerificationData);
    assertFalse(paymentIn.getTab().getForm().getField("currency").isEnabled());
    AddPaymentProcess addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.assertData(addPaymentVerificationData);
    addPaymentProcess.close();

    // Set Acc-8 (Transactions) as payment method
    // Payment Method: Acc-8 (Transactions), Financial Account: Spain Bank, Currency: enabled
    paymentIn.edit(paymentInHeaderData2);
    paymentIn.assertData(paymentInHeaderVerificationData2);
    assertTrue(paymentIn.getTab().getForm().getField("currency").isEnabled());
    addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.assertData(addPaymentVerificationData2);
    addPaymentProcess.close();

    // Set Mexico Bank as financial account
    // Payment Method: Acc-8 (Transactions), Financial Account: Mexico Bank, Currency: enabled
    paymentIn.edit(paymentInHeaderData3);
    paymentIn.assertData(paymentInHeaderVerificationData3);
    assertTrue(paymentIn.getTab().getForm().getField("currency").isEnabled());
    addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.assertData(addPaymentVerificationData3);
    addPaymentProcess.close();

    // Acc-8 (Transactions) and Spain Bank as default for Customer B
    BusinessPartner.BusinessPartnerTab.open(mainPage);
    BusinessPartner.BusinessPartnerTab.select(mainPage, businessPartnerData);
    BusinessPartner.Customer.edit(mainPage, customerData2);
    BusinessPartner.BusinessPartnerTab.close(mainPage);

    // Payment Method: Acc-8 (Transactions), Financial Account: Spain Bank, Currency: enabled
    paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertSaved();
    paymentIn.assertData(paymentInHeaderVerificationData2);
    assertTrue(paymentIn.getTab().getForm().getField("currency").isEnabled());
    addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.assertData(addPaymentVerificationData2);
    addPaymentProcess.close();

    // Set Mexico Bank as financial account
    // Payment Method: Acc-8 (Transactions), Financial Account: Mexico Bank, Currency: enabled
    paymentIn.edit(paymentInHeaderData3);
    paymentIn.assertData(paymentInHeaderVerificationData3);
    assertTrue(paymentIn.getTab().getForm().getField("currency").isEnabled());
    addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.assertData(addPaymentVerificationData3);
    addPaymentProcess.close();

    // Set payment method as paymentIn = false and paymentInMulticurrency = true for Spain Bank
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData2);

    // Payment Method: Acc-8 (Transactions), Financial Account: Mexico Bank, Currency: enabled
    paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertSaved();
    paymentIn.assertData(paymentInHeaderVerificationData3);
    assertTrue(paymentIn.getTab().getForm().getField("currency").isEnabled());
    addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.assertData(addPaymentVerificationData3);
    addPaymentProcess.close();

    // Set payment method as paymentIn = true and paymentInMulticurrency = false for Spain Bank
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData3);

    // Payment Method: Acc-8 (Transactions), Financial Account: Spain Bank, Currency: not enabled
    paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertSaved();
    paymentIn.assertData(paymentInHeaderVerificationData2);
    assertFalse(paymentIn.getTab().getForm().getField("currency").isEnabled());
    addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.assertData(addPaymentVerificationData2);
    addPaymentProcess.close();

    // Set Mexico Bank as financial account
    // Payment Method: Acc-8 (Transactions), Financial Account: Mexico Bank, Currency: enabled
    paymentIn.edit(paymentInHeaderData3);
    paymentIn.assertData(paymentInHeaderVerificationData3);
    assertTrue(paymentIn.getTab().getForm().getField("currency").isEnabled());
    addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.assertData(addPaymentVerificationData3);
    addPaymentProcess.close();

    // Set payment method as paymentIn = true and paymentInMulticurrency = true
    // Set financial account as active = false and payment method as active = true for Spain Bank
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    financialAccount.edit(accountHeaderData2);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData4);

    // Payment Method: Acc-8 (Transactions), Financial Account: Mexico Bank, Currency: enabled
    paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertFinancialAccountInactive("Spain Bank - EUR");
    paymentIn.edit(paymentInHeaderData2);
    paymentIn.assertSaved();
    paymentIn.assertData(paymentInHeaderVerificationData3);
    assertTrue(paymentIn.getTab().getForm().getField("currency").isEnabled());
    addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.assertData(addPaymentVerificationData3);
    addPaymentProcess.close();

    // Set financial account as active = true and payment method as active = false for Spain Bank
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    financialAccount.edit(accountHeaderData3);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData5);

    // Payment Method: Acc-8 (Transactions), Financial Account: Mexico Bank, Currency: enabled
    paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertPaymentMethodInactive("Acc-8 (Transactions)");
    paymentIn.edit(paymentInHeaderData2);
    paymentIn.assertSaved();
    paymentIn.assertData(paymentInHeaderVerificationData3);
    assertTrue(paymentIn.getTab().getForm().getField("currency").isEnabled());
    addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.assertData(addPaymentVerificationData3);
    addPaymentProcess.close();

    // Set financial account as active = false and payment method as active = false for Spain Bank
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    financialAccount.edit(accountHeaderData2);

    // Payment Method: Acc-8 (Transactions), Financial Account: Mexico Bank, Currency: enabled
    paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertFinancialAccountAndPaymentMethodInactive("Spain Bank - EUR",
        "Acc-8 (Transactions)");
    paymentIn.edit(paymentInHeaderData2);
    paymentIn.assertSaved();
    paymentIn.assertData(paymentInHeaderVerificationData3);
    assertTrue(paymentIn.getTab().getForm().getField("currency").isEnabled());
    addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.assertData(addPaymentVerificationData3);
    addPaymentProcess.close();

    // Set financial account as active = true and payment method as active = true for Spain Bank
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    financialAccount.edit(accountHeaderData3);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData6);

    // 4 (Spain) and Spain Bank as default for Customer B
    BusinessPartner.BusinessPartnerTab.open(mainPage);
    BusinessPartner.BusinessPartnerTab.select(mainPage, businessPartnerData);
    BusinessPartner.Customer.edit(mainPage, customerData3);
    BusinessPartner.BusinessPartnerTab.close(mainPage);

    logger.info(
        "** End of test case [APRRegression29368In] Test regression 29368 - Payment In flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
