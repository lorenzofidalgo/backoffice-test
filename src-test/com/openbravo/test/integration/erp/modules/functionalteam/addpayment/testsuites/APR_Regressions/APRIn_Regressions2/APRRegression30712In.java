/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.BusinessPartnerData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.CustomerData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.masterdata.businesspartner.BusinessPartner;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;

/**
 * Test regression 30712
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression30712In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  BusinessPartnerData businessPartnerData;
  CustomerData customerData;
  AccountData accountHeaderData;
  PaymentMethodData paymentMethodData;
  PaymentMethodData paymentMethodData2;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  AddPaymentPopUpData addPaymentVerificationData2;
  PaymentMethodData paymentMethodData3;
  PaymentMethodData paymentMethodData4;
  PaymentMethodData paymentMethodData5;
  AccountData accountHeaderData2;
  AccountData accountHeaderData3;
  PaymentMethodData paymentMethodData6;
  AddPaymentPopUpData addPaymentVerificationData3;
  CustomerData customerData2;
  AccountData accountHeaderData4;
  PaymentMethodData paymentMethodData7;
  CustomerData customerData3;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData2;
  CustomerData customerData4;

  /**
   * Class constructor.
   *
   */
  public APRRegression30712In(BusinessPartnerData businessPartnerData, CustomerData customerData,
      AccountData accountHeaderData, PaymentMethodData paymentMethodData,
      PaymentMethodData paymentMethodData2, SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      AddPaymentPopUpData addPaymentVerificationData2, PaymentMethodData paymentMethodData3,
      PaymentMethodData paymentMethodData4, PaymentMethodData paymentMethodData5,
      AccountData accountHeaderData2, AccountData accountHeaderData3,
      PaymentMethodData paymentMethodData6, AddPaymentPopUpData addPaymentVerificationData3,
      CustomerData customerData2, AccountData accountHeaderData4,
      PaymentMethodData paymentMethodData7, CustomerData customerData3,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData2,
      CustomerData customerData4) {
    this.businessPartnerData = businessPartnerData;
    this.customerData = customerData;
    this.accountHeaderData = accountHeaderData;
    this.paymentMethodData = paymentMethodData;
    this.paymentMethodData2 = paymentMethodData2;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.addPaymentVerificationData2 = addPaymentVerificationData2;
    this.paymentMethodData3 = paymentMethodData3;
    this.paymentMethodData4 = paymentMethodData4;
    this.paymentMethodData5 = paymentMethodData5;
    this.accountHeaderData2 = accountHeaderData2;
    this.accountHeaderData3 = accountHeaderData3;
    this.paymentMethodData6 = paymentMethodData6;
    this.addPaymentVerificationData3 = addPaymentVerificationData3;
    this.customerData2 = customerData2;
    this.accountHeaderData4 = accountHeaderData4;
    this.paymentMethodData7 = paymentMethodData7;
    this.customerData3 = customerData3;
    this.completedSalesInvoiceHeaderVerificationData2 = completedSalesInvoiceHeaderVerificationData2;
    this.customerData4 = customerData4;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new BusinessPartnerData.Builder().name("Customer Barcelona").build(),
        new CustomerData.Builder().paymentMethod("").account("").build(),

        new AccountData.Builder().name("Spain Bank").build(),
        new PaymentMethodData.Builder().paymentMethod("Acc-8 (Transactions)").build(),
        new PaymentMethodData.Builder().pmdefault(true).build(),

        new SalesInvoiceHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Customer Barcelona").build())
            .paymentMethod("Acc-8 (Transactions)")
            .priceList("Customer A USD")
            .build(),
        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .partnerAddress(".Barcelona, Little")
            .paymentTerms("90 days")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A").build())
            .invoicedQuantity("5")
            .build(),
        new SalesInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 3%")
            .lineNetAmount("10.00")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("10.30")
            .summedLineAmount("10.00")
            .currency("USD")
            .paymentComplete(false)
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Customer Barcelona")
            .fin_paymentmethod_id("Acc-8 (Transactions)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("USD")
            .actual_payment("10.30")
            .expected_payment("10.30")
            .c_currency_to_id("EUR")
            .converted_amount("4.12")
            .conversion_rate("0.4")
            .amount_gl_items("0.00")
            .amount_inv_ords("10.30")
            .total("10.30")
            .difference("0.00")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Customer Barcelona")
            .fin_paymentmethod_id("Acc-8 (Transactions)")
            .fin_financial_account_id("Mexico Bank - USD")
            .c_currency_id("USD")
            .actual_payment("10.30")
            .expected_payment("10.30")
            .amount_gl_items("0.00")
            .amount_inv_ords("10.30")
            .total("10.30")
            .difference("0.00")
            .build(),

        new PaymentMethodData.Builder().pmdefault(false)
            .payinAllow(false)
            .payinMulticurrencyAllow(true)
            .build(),

        new PaymentMethodData.Builder().pmdefault(false)
            .payinAllow(true)
            .payinMulticurrencyAllow(false)
            .build(),

        new PaymentMethodData.Builder().pmdefault(false)
            .payinAllow(true)
            .payinMulticurrencyAllow(true)
            .build(),
        new AccountData.Builder().active(false).build(),

        new AccountData.Builder().name("Mexico Bank").build(),
        new PaymentMethodData.Builder().active(false).build(),

        new AddPaymentPopUpData.Builder().received_from("Customer Barcelona")
            .fin_paymentmethod_id("")
            .fin_financial_account_id("")
            .c_currency_id("USD")
            .actual_payment("10.30")
            .expected_payment("10.30")
            .amount_gl_items("0.00")
            .amount_inv_ords("10.30")
            .total("10.30")
            .difference("0.00")
            .build(),

        new CustomerData.Builder().paymentMethod("Acc-8 (Transactions)")
            .account("Mexico Bank")
            .build(),

        new AccountData.Builder().active(true).build(),
        new PaymentMethodData.Builder().active(true).build(),

        new CustomerData.Builder().paymentMethod("6 (Spain)").account("Spain Bank").build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("10.30")
            .summedLineAmount("10.00")
            .currency("USD")
            .paymentComplete(true)
            .build(),

        new CustomerData.Builder().paymentMethod("6 (Spain)").account("").build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 30712 - Payment In flow
   *
   * @throws ParseException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void APRRegression30712InTest() throws ParseException, OpenbravoERPTestException {
    logger.info(
        "** Start of test case [APRRegression30712In] Test regression 30712 - Payment In flow. **");

    BusinessPartner.BusinessPartnerTab.select(mainPage, businessPartnerData);
    BusinessPartner.Customer.edit(mainPage, customerData);
    BusinessPartner.BusinessPartnerTab.close(mainPage);

    // Set payment method as default = true, paymentIn = true and paymentInMulticurrency = true for
    // Spain Bank
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData2);

    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    String invoiceNo = salesInvoice.getData("documentNo").toString();
    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.create(salesInvoiceLineData);
    salesInvoiceLines.assertSaved();
    salesInvoiceLines.assertData(salesInvoiceLineVerificationData);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    // Spain Bank and Mexico Bank financial accounts are available, Spain Bank as default
    AddPaymentProcess addPaymentProcess = salesInvoice.openAddPayment();
    addPaymentProcess.assertData(addPaymentVerificationData);
    addPaymentProcess.setParameterValue("fin_financial_account_id", "Mexico Bank - USD");
    addPaymentProcess.assertData(addPaymentVerificationData2);
    addPaymentProcess.close();

    // Set payment method as default = false, paymentIn = false and paymentInMulticurrency = true
    // for Spain Bank
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData3);

    // Mexico Bank is available
    salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    addPaymentProcess = salesInvoice.openAddPayment();
    addPaymentProcess.assertData(addPaymentVerificationData2);
    addPaymentProcess.close();

    // Set payment method as default = false, paymentIn = true and paymentInMulticurrency = false
    // for Spain Bank
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData4);

    // Mexico Bank is available
    salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    addPaymentProcess = salesInvoice.openAddPayment();
    addPaymentProcess.assertData(addPaymentVerificationData2);
    addPaymentProcess.close();

    // Set payment method as default = false, paymentIn = true and paymentInMulticurrency = true and
    // financial account as active = false for Spain Bank
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData5);
    financialAccount.edit(accountHeaderData2);

    // Mexico Bank is available
    salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    addPaymentProcess = salesInvoice.openAddPayment();
    addPaymentProcess.assertData(addPaymentVerificationData2);
    addPaymentProcess.close();

    // Set payment method as active = false for Mexico Bank
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData3);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData6);

    // No financial accounts are available
    salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    addPaymentProcess = salesInvoice.openAddPayment();
    addPaymentProcess.assertData(addPaymentVerificationData3);
    addPaymentProcess.close();

    // Set financial account as active = true for Spain Bank
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    financialAccount.edit(accountHeaderData4);

    // Set payment method as active = true for Mexico Bank
    financialAccount.select(accountHeaderData3);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData7);

    // Set payment method and Mexico Bank financial account as default for Business Partner
    BusinessPartner.BusinessPartnerTab.open(mainPage);
    BusinessPartner.BusinessPartnerTab.select(mainPage, businessPartnerData);
    BusinessPartner.Customer.edit(mainPage, customerData2);
    BusinessPartner.BusinessPartnerTab.close(mainPage);

    // Spain Bank and Mexico Bank financial accounts are available, Mexico Bank as default
    salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    addPaymentProcess = salesInvoice.openAddPayment();
    addPaymentProcess.assertData(addPaymentVerificationData2);
    addPaymentProcess.setParameterValue("fin_financial_account_id", "Spain Bank - EUR");
    addPaymentProcess.assertData(addPaymentVerificationData);
    addPaymentProcess.close();

    // Set original payment method and Spain Bank financial account as default for Business Partner
    BusinessPartner.BusinessPartnerTab.open(mainPage);
    BusinessPartner.BusinessPartnerTab.select(mainPage, businessPartnerData);
    BusinessPartner.Customer.edit(mainPage, customerData3);
    BusinessPartner.BusinessPartnerTab.close(mainPage);

    // Spain Bank and Mexico Bank financial accounts are available, Spain Bank as default
    salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    addPaymentProcess = salesInvoice.openAddPayment();
    addPaymentProcess.assertData(addPaymentVerificationData);
    addPaymentProcess.setParameterValue("fin_financial_account_id", "Mexico Bank - USD");
    addPaymentProcess.assertData(addPaymentVerificationData2);
    addPaymentProcess.process("Process Received Payment(s) and Deposit");
    salesInvoice.assertPaymentCreatedSuccessfully();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData2);

    BusinessPartner.BusinessPartnerTab.open(mainPage);
    BusinessPartner.BusinessPartnerTab.select(mainPage, businessPartnerData);
    BusinessPartner.Customer.edit(mainPage, customerData4);
    BusinessPartner.BusinessPartnerTab.close(mainPage);

    logger.info(
        "** End of test case [APRRegression30712In] Test regression 30712 - Payment In flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
