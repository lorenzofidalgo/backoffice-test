/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions3;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;

/**
 * Test regression 28859
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression28859In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineData2;
  SalesInvoiceLinesData salesInvoiceLineVerificationData2;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData2;
  AddPaymentPopUpData addPaymentVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData3;

  /**
   * Class constructor.
   *
   */
  public APRRegression28859In(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData2,
      SalesInvoiceLinesData salesInvoiceLineVerificationData2,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData2,
      AddPaymentPopUpData addPaymentVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData3) {
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData2 = salesInvoiceLineData2;
    this.salesInvoiceLineVerificationData2 = salesInvoiceLineVerificationData2;
    this.completedSalesInvoiceHeaderVerificationData2 = completedSalesInvoiceHeaderVerificationData2;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.completedSalesInvoiceHeaderVerificationData3 = completedSalesInvoiceHeaderVerificationData3;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesInvoiceHeaderData.Builder().transactionDocument("Reversed Sales Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .paymentTerms("30 days, 5")
            .paymentMethod("1 (Spain)")
            .priceList("Customer A")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("-80")
            .build(),
        new SalesInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("-160.00")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("-165.60")
            .summedLineAmount("-160.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good B")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("-100")
            .build(),
        new SalesInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("-200.00")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("-207.00")
            .summedLineAmount("-200.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("-165.60")
            .expected_payment("-165.60")
            .amount_gl_items("0.00")
            .amount_inv_ords("-165.60")
            .total("-165.60")
            .difference("0.00")
            .build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("-165.60")
            .summedLineAmount("-160.00")
            .currency("EUR")
            .paymentComplete(true)
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 28859 - Payment In flow
   *
   * @throws ParseException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void APRRegression28859InTest() throws ParseException, OpenbravoERPTestException {
    logger.info(
        "** Start of test case [APRRegression28859In] Test regression 28859 - Payment In flow. **");

    SalesInvoice salesInvoice1 = new SalesInvoice(mainPage).open();
    salesInvoice1.create(salesInvoiceHeaderData);
    salesInvoice1.assertSaved();
    salesInvoice1.assertData(salesInvoiceHeaderVerificationData);
    String invoice1No = salesInvoice1.getData("documentNo").toString();
    SalesInvoice.Lines salesInvoiceLines1 = salesInvoice1.new Lines(mainPage);
    salesInvoiceLines1.create(salesInvoiceLineData);
    salesInvoiceLines1.assertSaved();
    salesInvoiceLines1.assertData(salesInvoiceLineVerificationData);
    salesInvoice1.complete();
    salesInvoice1.assertProcessCompletedSuccessfully2();
    salesInvoice1.assertData(completedSalesInvoiceHeaderVerificationData);
    salesInvoice1.closeForm();

    SalesInvoice salesInvoice2 = new SalesInvoice(mainPage).open();
    salesInvoice2.create(salesInvoiceHeaderData);
    salesInvoice2.assertSaved();
    salesInvoice2.assertData(salesInvoiceHeaderVerificationData);
    String invoice2No = salesInvoice2.getData("documentNo").toString();
    SalesInvoice.Lines salesInvoiceLines2 = salesInvoice2.new Lines(mainPage);
    salesInvoiceLines2.create(salesInvoiceLineData2);
    salesInvoiceLines2.assertSaved();
    salesInvoiceLines2.assertData(salesInvoiceLineVerificationData2);
    salesInvoice2.complete();
    salesInvoice2.assertProcessCompletedSuccessfully2();
    salesInvoice2.assertData(completedSalesInvoiceHeaderVerificationData2);
    salesInvoice2.closeForm();

    salesInvoice1.open();
    salesInvoice1.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice1No).build());
    AddPaymentProcess addPaymentProcess = salesInvoice1.openAddPayment();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertData(addPaymentVerificationData);
    addPaymentProcess.process("Process Received Payment(s) and Deposit");
    salesInvoice1.assertPaymentCreatedSuccessfully();
    salesInvoice1.assertData(completedSalesInvoiceHeaderVerificationData3);
    salesInvoice1.closeForm();

    salesInvoice2.open();
    salesInvoice2.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice2No).build());
    salesInvoice2.assertData(completedSalesInvoiceHeaderVerificationData2);
    salesInvoice2.closeForm();

    logger.info(
        "** End of test case [APRRegression28859In] Test regression 28859 - Payment In flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
