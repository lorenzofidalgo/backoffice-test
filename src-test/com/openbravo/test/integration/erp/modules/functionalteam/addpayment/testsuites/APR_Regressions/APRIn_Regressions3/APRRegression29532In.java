/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions3;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 29532
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression29532In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  PaymentInHeaderData paymentInHeaderData;
  PaymentInHeaderData paymentInHeaderVerificationData;
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  PaymentInLinesData paymentInLinesVerificationData;
  PaymentInHeaderData paymentInHeaderData2;
  PaymentInHeaderData paymentInHeaderVerificationData2;
  AddPaymentPopUpData addPaymentTotalsVerificationData2;
  PaymentInLinesData paymentInLinesVerificationData2;

  /**
   * Class constructor.
   *
   */
  public APRRegression29532In(SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      PaymentInHeaderData paymentInHeaderData, PaymentInHeaderData paymentInHeaderVerificationData,
      AddPaymentPopUpData addPaymentTotalsVerificationData,
      PaymentInLinesData paymentInLinesVerificationData, PaymentInHeaderData paymentInHeaderData2,
      PaymentInHeaderData paymentInHeaderVerificationData2,
      AddPaymentPopUpData addPaymentTotalsVerificationData2,
      PaymentInLinesData paymentInLinesVerificationData2) {
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.paymentInLinesVerificationData = paymentInLinesVerificationData;
    this.paymentInHeaderData2 = paymentInHeaderData2;
    this.paymentInHeaderVerificationData2 = paymentInHeaderVerificationData2;
    this.addPaymentTotalsVerificationData2 = addPaymentTotalsVerificationData2;
    this.paymentInLinesVerificationData2 = paymentInLinesVerificationData2;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesOrderHeaderData.Builder().transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("7.2 (Spain)")
            .build(),
        new SalesOrderHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .warehouse("Spain warehouse")
            .priceList("Customer A")
            .paymentTerms("30 days, 5")
            .invoiceTerms("Customer Schedule After Delivery")
            .build(),
        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGA")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("1").build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("2.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("2.00")
            .grandTotalAmount("2.07")
            .currency("EUR")
            .build(),

        new PaymentInHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("7.2 (Spain)")
            .amount("1.00")
            .build(),
        new PaymentInHeaderData.Builder().organization("Spain")
            .documentType("AR Receipt")
            .account("Spain Bank - EUR")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("7.2 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("1.00")
            .expected_payment("2.07")
            .amount_gl_items("0.00")
            .amount_inv_ords("1.00")
            .total("1.00")
            .difference("0.00")
            .build(),

        new PaymentInLinesData.Builder().dueDate(OBDate.CURRENT_DATE)
            .invoiceAmount("2.07")
            .expected("2.07")
            .received("1.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentInHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("7.2 (Spain)")
            .amount("1.07")
            .build(),
        new PaymentInHeaderData.Builder().organization("Spain")
            .documentType("AR Receipt")
            .account("Spain Bank - EUR")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("7.2 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("1.07")
            .expected_payment("1.07")
            .amount_gl_items("0.00")
            .amount_inv_ords("1.07")
            .total("1.07")
            .difference("0.00")
            .build(),

        new PaymentInLinesData.Builder().dueDate(OBDate.CURRENT_DATE)
            .invoiceAmount("2.07")
            .expected("2.07")
            .received("1.07")
            .invoiceno("")
            .glitemname("")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 29532 - Payment In flow
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression29532InTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression29532In] Test regression 29532 - Payment In flow. **");

    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    String orderNo = salesOrder.getData("documentNo").toString();
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.create(salesOrderLinesData);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);

    PaymentIn paymentIn1 = new PaymentIn(mainPage).open();
    paymentIn1.create(paymentInHeaderData);
    paymentIn1.assertSaved();
    paymentIn1.assertData(paymentInHeaderVerificationData);

    AddPaymentProcess addPaymentProcess1 = paymentIn1.addDetailsOpen();
    addPaymentProcess1.process("Orders", orderNo, "Process Received Payment(s) and Deposit",
        addPaymentTotalsVerificationData);
    paymentIn1.assertPaymentCreatedSuccessfully();

    PaymentIn.Lines paymentInLines1 = paymentIn1.new Lines(mainPage);
    paymentInLines1.assertCount(1);
    paymentInLines1.select(new PaymentInLinesData.Builder().orderno(orderNo).build());
    paymentInLines1.assertData((PaymentInLinesData) paymentInLinesVerificationData
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo));

    PaymentIn paymentIn2 = new PaymentIn(mainPage).open();
    paymentIn2.create(paymentInHeaderData2);
    paymentIn2.assertSaved();
    paymentIn2.assertData(paymentInHeaderVerificationData2);

    AddPaymentProcess addPaymentProcess2 = paymentIn2.addDetailsOpen();
    addPaymentProcess2.process("Orders", orderNo, "Process Received Payment(s) and Deposit",
        addPaymentTotalsVerificationData2);
    paymentIn2.assertPaymentCreatedSuccessfully();

    PaymentIn.Lines paymentInLines2 = paymentIn2.new Lines(mainPage);
    paymentInLines2.assertCount(1);
    paymentInLines2.select(new PaymentInLinesData.Builder().orderno(orderNo).build());
    paymentInLines2.assertData((PaymentInLinesData) paymentInLinesVerificationData2
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo));

    logger.info(
        "** End of test case [APRRegression29532In] Test regression 29532 - Payment In flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
