/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions3;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddTransactionData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.MatchStatementData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.BankStatementLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddTransactionProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementGrid;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 30007
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression30007In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  AccountData accountHeaderData;
  BankStatementHeaderData bankStatementHeaderData;
  BankStatementLinesData bankStatementLinesData;
  MatchStatementData matchStatementData;
  AddTransactionData addTransactionVerificationData;
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  MatchStatementData matchStatementData2;
  PaymentPlanData orderPaymentInPlanData;
  PaymentDetailsData orderPaymentInDetailsData;

  /**
   * Class constructor.
   *
   */
  public APRRegression30007In(SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData, AccountData accountHeaderData,
      BankStatementHeaderData bankStatementHeaderData,
      BankStatementLinesData bankStatementLinesData, MatchStatementData matchStatementData,
      AddTransactionData addTransactionVerificationData,
      AddPaymentPopUpData addPaymentTotalsVerificationData, MatchStatementData matchStatementData2,
      PaymentPlanData orderPaymentInPlanData, PaymentDetailsData orderPaymentInDetailsData) {
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.accountHeaderData = accountHeaderData;
    this.bankStatementHeaderData = bankStatementHeaderData;
    this.bankStatementLinesData = bankStatementLinesData;
    this.matchStatementData = matchStatementData;
    this.addTransactionVerificationData = addTransactionVerificationData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.matchStatementData2 = matchStatementData2;
    this.orderPaymentInPlanData = orderPaymentInPlanData;
    this.orderPaymentInDetailsData = orderPaymentInDetailsData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesOrderHeaderData.Builder().transactionDocument("Standard Order")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Customer Madrid").build())
            .paymentMethod("1 (Spain)")
            .priceList("Sales")
            .build(),
        new SalesOrderHeaderData.Builder().organization("Spain")
            .partnerAddress(".Madrid, Little")
            .invoiceAddress(".Madrid, Little")
            .warehouse("Spain warehouse")
            .paymentTerms("90 days")
            .invoiceTerms("After Delivery")
            .build(),
        new SalesOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("FGA").priceListVersion("Sales").build())
            .orderedQuantity("500")
            .build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 3%")
            .lineNetAmount("1,000.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("1,000.00")
            .grandTotalAmount("1,030.00")
            .currency("EUR")
            .build(),

        new AccountData.Builder().name("Spain Bank").build(),
        new BankStatementHeaderData.Builder().name("APRRegression30007In").build(),
        new BankStatementLinesData.Builder().referenceNo("30007In")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Customer Madrid").build())
            .cramount("1,030.00")
            .build(),

        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Customer Madrid")
            .referenceNo("30007In")
            .glitem("")
            .amount("1030")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .transactionDate("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .build(),
        new AddTransactionData.Builder().trxtype("BP Deposit")
            .currency("EUR")
            .depositamt("1,030.00")
            .withdrawalamt("0.00")
            .organization("Spain")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Customer Madrid").build())
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Customer Madrid")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("1,030.00")
            .expected_payment("1,030.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("1,030.00")
            .total("1,030.00")
            .difference("0.00")
            .build(),
        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Customer Madrid")
            .referenceNo("30007In")
            .glitem("")
            .amount("1030")
            .affinity("AD")
            .matchedDocument("T")
            .matchingType("AD")
            .businessPartner("Customer Madrid")
            .transactionAmount("1030")
            .transactionGLItem("")
            .build(),

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .expected("1,030.00")
            .received("1,030.00")
            .outstanding("0.00")
            .numberOfPayments("1")
            .currency("EUR")
            .build(),
        new PaymentDetailsData.Builder().dueDate(OBDate.CURRENT_DATE)
            .expected("1,030.00")
            .received("1,030.00")
            .writeoff("0.00")
            .status("Payment Cleared")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 30007 - Payment In flow
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression30007InTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression30007In] Test regression 30007 - Payment In flow. **");

    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    String orderNo = salesOrder.getData("documentNo").toString();
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.create(salesOrderLinesData);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);

    FinancialAccount.BankStatements bankStatement = financialAccount.new BankStatements(mainPage);
    String date;
    if (bankStatement.getRecordCount() == 0) {
      date = OBDate.CURRENT_DATE;
    } else {
      bankStatement.selectWithoutFiltering(0);
      date = OBDate.addDaysToDate((String) bankStatement.getData("transactionDate"), 1);
    }
    bankStatementHeaderData.addDataField("transactionDate", date);
    bankStatementHeaderData.addDataField("importdate", date);
    bankStatement.create(bankStatementHeaderData);
    bankStatement.assertSaved();

    FinancialAccount.BankStatements.BankStatementLines bankStatementLines = bankStatement.new BankStatementLines(
        mainPage);
    bankStatementLinesData.addDataField("transactionDate", date);
    bankStatementLines.create(bankStatementLinesData);
    bankStatementLines.assertSaved();
    bankStatement.process();
    bankStatement.assertProcessCompletedSuccessfully2();

    @SuppressWarnings("rawtypes")
    MatchStatementProcess matchStatementProcess = financialAccount.openMatchStatement(false);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData.addDataField("banklineDate", date);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData);

    AddTransactionProcess addTransactionProcess = ((MatchStatementGrid) matchStatementProcess
        .getMatchStatementGrid()).clickAdd(0);
    addTransactionProcess.assertData(addTransactionVerificationData);
    AddPaymentProcess addPaymentProcess = addTransactionProcess.openAddPayment();
    addPaymentProcess.setParameterValue("received_from",
        new BusinessPartnerSelectorData.Builder().name("Customer Madrid").build());
    addPaymentProcess.setParameterValue("fin_paymentmethod_id", "1 (Spain)");
    addPaymentProcess.process("Orders", orderNo, "Process Received Payment(s)",
        addPaymentTotalsVerificationData);
    String orderDescription = "Order No.: " + orderNo + "\n";
    addTransactionVerificationData.addDataField("trxdate", date);
    addTransactionVerificationData.addDataField("dateacct", date);
    addTransactionVerificationData.addDataField("description", orderDescription);
    addTransactionProcess.assertData(addTransactionVerificationData);
    addTransactionProcess.process();

    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData2.addDataField("banklineDate", date);
    matchStatementData2.addDataField("trxDate", date);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData2);
    matchStatementProcess.process();
    financialAccount.assertProcessCompletedSuccessfully2();

    salesOrder.open();
    Sleep.trySleep(15000);
    salesOrder.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    SalesOrder.PaymentInPlan orderPaymentInPlan = salesOrder.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlanData.addDataField("lastPayment", date);
    orderPaymentInPlan.assertData(orderPaymentInPlanData);
    SalesOrder.PaymentInPlan.PaymentInDetails orderPaymentInDetails = orderPaymentInPlan.new PaymentInDetails(
        mainPage);
    orderPaymentInDetails.assertCount(1);
    orderPaymentInDetailsData.addDataField("paymentDate", date);
    orderPaymentInDetails.assertData(orderPaymentInDetailsData);

    logger.info(
        "** End of test case [APRRegression30007In] Test regression 30007 - Payment In flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
