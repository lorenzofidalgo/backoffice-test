/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions4;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddTransactionData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.MatchStatementData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.BankStatementLinesData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddTransactionProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementGrid;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 32268
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression32268In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  PaymentPlanData salesInvoicePaymentPlanData;
  AccountData accountHeaderData;
  BankStatementHeaderData bankStatementHeaderData;
  BankStatementHeaderData bankStatementHeaderVerificationData;
  BankStatementLinesData bankStatementLinesData;
  BankStatementLinesData bankStatementLinesVerificationData;
  MatchStatementData matchStatementData;
  AddTransactionData addTransactionVerificationData;
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  MatchStatementData matchStatementData2;
  TransactionsData transactionVerificationData;
  PaymentInHeaderData paymentInHeaderVerificationData;
  PaymentInLinesData paymentInLinesVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData2;
  PaymentPlanData salesInvoicePaymentPlanData2;
  PaymentDetailsData salesInvoicePaymentDetailsData;
  MatchStatementData matchStatementData3;
  TransactionsData transactionVerificationData2;
  PaymentInHeaderData paymentInHeaderVerificationData2;
  PaymentDetailsData salesInvoicePaymentDetailsData2;

  /**
   * Class constructor.
   *
   */
  public APRRegression32268In(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      PaymentPlanData salesInvoicePaymentPlanData, AccountData accountHeaderData,
      BankStatementHeaderData bankStatementHeaderData,
      BankStatementHeaderData bankStatementHeaderVerificationData,
      BankStatementLinesData bankStatementLinesData,
      BankStatementLinesData bankStatementLinesVerificationData,
      MatchStatementData matchStatementData, AddTransactionData addTransactionVerificationData,
      AddPaymentPopUpData addPaymentTotalsVerificationData, MatchStatementData matchStatementData2,
      TransactionsData transactionVerificationData,
      PaymentInHeaderData paymentInHeaderVerificationData,
      PaymentInLinesData paymentInLinesVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData2,
      PaymentPlanData salesInvoicePaymentPlanData2,
      PaymentDetailsData salesInvoicePaymentDetailsData, MatchStatementData matchStatementData3,
      TransactionsData transactionVerificationData2,
      PaymentInHeaderData paymentInHeaderVerificationData2,
      PaymentDetailsData salesInvoicePaymentDetailsData2) {
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.salesInvoicePaymentPlanData = salesInvoicePaymentPlanData;
    this.accountHeaderData = accountHeaderData;
    this.bankStatementHeaderData = bankStatementHeaderData;
    this.bankStatementHeaderVerificationData = bankStatementHeaderVerificationData;
    this.bankStatementLinesData = bankStatementLinesData;
    this.bankStatementLinesVerificationData = bankStatementLinesVerificationData;
    this.matchStatementData = matchStatementData;
    this.addTransactionVerificationData = addTransactionVerificationData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.matchStatementData2 = matchStatementData2;
    this.transactionVerificationData = transactionVerificationData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.paymentInLinesVerificationData = paymentInLinesVerificationData;
    this.completedSalesInvoiceHeaderVerificationData2 = completedSalesInvoiceHeaderVerificationData2;
    this.salesInvoicePaymentPlanData2 = salesInvoicePaymentPlanData2;
    this.salesInvoicePaymentDetailsData = salesInvoicePaymentDetailsData;
    this.matchStatementData3 = matchStatementData3;
    this.transactionVerificationData2 = transactionVerificationData2;
    this.paymentInHeaderVerificationData2 = paymentInHeaderVerificationData2;
    this.salesInvoicePaymentDetailsData2 = salesInvoicePaymentDetailsData2;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesInvoiceHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .paymentTerms("30 days, 5")
            .paymentMethod("1 (Spain)")
            .priceList("Customer A")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("FGA")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("50")
            .build(),
        new SalesInvoiceLinesData.Builder().lineNo("10")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("100.00")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("103.50")
            .summedLineAmount("100.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .expectedDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("1 (Spain)")
            .expected("103.50")
            .received("0.00")
            .outstanding("103.50")
            .currency("EUR")
            .lastPaymentDate("")
            .daysOverdue("0")
            .numberOfPayments("0")
            .build(),

        new AccountData.Builder().name("Spain Bank").build(),
        new BankStatementHeaderData.Builder().name("APRRegression32268In").build(),
        new BankStatementHeaderData.Builder().documentType("Bank Statement File")
            .active(true)
            .fileName("")
            .notes("")
            .build(),
        new BankStatementLinesData.Builder().referenceNo("32268In")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .cramount("103.50")
            .build(),
        new BankStatementLinesData.Builder().active(true)
            .lineNo("10")
            .bpartnername("")
            .gLItem("")
            .dramount("0.00")
            .cramount("103.50")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .financialAccountTransaction("&nbsp;")
            .matchingtype("")
            .description("")
            .build(),

        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Customer A")
            .bpartnername("")
            .referenceNo("32268In")
            .glitem("")
            .description("")
            .amount("103.5")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .trxDescription("")
            .build(),
        new AddTransactionData.Builder().trxtype("BP Deposit")
            .gLItem("")
            .description("")
            .currency("EUR")
            .depositamt("103.50")
            .withdrawalamt("0.00")
            .organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .project("")
            .product("")
            .build(),
        new AddPaymentPopUpData.Builder().trxtype("Received In")
            .reference_no("")
            .received_from("Customer A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .bslamount("103.50")
            .c_currency_id("EUR")
            .actual_payment("103.50")
            .expected_payment("103.50")
            .amount_gl_items("0.00")
            .amount_inv_ords("103.50")
            .total("103.50")
            .difference("0.00")
            .build(),
        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Customer A")
            .bpartnername("")
            .referenceNo("32268In")
            .glitem("")
            .description("")
            .amount("103.5")
            .affinity("AD")
            .matchedDocument("T")
            .matchingType("AD")
            .businessPartner("Customer A")
            .transactionAmount("103.5")
            .transactionGLItem("")
            .build(),

        new TransactionsData.Builder().transactionType("BP Deposit")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .gLItem("")
            .description("")
            .depositAmount("103.50")
            .paymentAmount("0.00")
            .currency("EUR")
            .status("Deposited not Cleared")
            .organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .product(new ProductSelectorData.Builder().name("").build())
            .project("")
            .build(),

        new PaymentInHeaderData.Builder().status("Deposited not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .organization("Spain")
            .documentType("AR Receipt")
            .referenceNo("")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("1 (Spain)")
            .amount("103.50")
            .account("Spain Bank - EUR")
            .currency("EUR")
            .build(),
        new PaymentInLinesData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .invoiceAmount("103.50")
            .expected("103.50")
            .received("103.50")
            .glitemname("")
            .build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("103.50")
            .summedLineAmount("100.00")
            .currency("EUR")
            .paymentComplete(true)
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .expectedDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("1 (Spain)")
            .expected("103.50")
            .received("103.50")
            .outstanding("0.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),
        new PaymentDetailsData.Builder().received("103.50")
            .writeoff("0.00")
            .paymentMethod("1 (Spain)")
            .amount("103.50")
            .account("Spain Bank - EUR")
            .status("Deposited not Cleared")
            .build(),

        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Customer A")
            .bpartnername("")
            .referenceNo("32268In")
            .glitem("")
            .description("")
            .amount("103.5")
            .affinity("AP")
            .matchedDocument("T")
            .matchingType("AP")
            .businessPartner("Customer A")
            .transactionAmount("103.5")
            .transactionGLItem("")
            .build(),

        new TransactionsData.Builder().transactionType("BP Deposit")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .gLItem("")
            .description("")
            .depositAmount("103.50")
            .paymentAmount("0.00")
            .currency("EUR")
            .status("Payment Cleared")
            .organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .product(new ProductSelectorData.Builder().name("").build())
            .project("")
            .build(),

        new PaymentInHeaderData.Builder().status("Payment Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .organization("Spain")
            .documentType("AR Receipt")
            .referenceNo("")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("1 (Spain)")
            .amount("103.50")
            .account("Spain Bank - EUR")
            .currency("EUR")
            .build(),

        new PaymentDetailsData.Builder().received("103.50")
            .writeoff("0.00")
            .paymentMethod("1 (Spain)")
            .amount("103.50")
            .account("Spain Bank - EUR")
            .status("Payment Cleared")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 32268 - Payment In flow
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression32268InTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression32268In] Test regression 32268 - Payment In flow. **");

    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    String invoiceNo = salesInvoice.getData("documentNo").toString();
    SalesInvoice.Lines salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.create(salesInvoiceLineData);
    salesInvoiceLine.assertSaved();
    salesInvoiceLine.assertData(salesInvoiceLineVerificationData);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);
    SalesInvoice.PaymentInPlan salesInvoicePaymentPlan = salesInvoice.new PaymentInPlan(mainPage);
    salesInvoicePaymentPlan.assertCount(1);
    salesInvoicePaymentPlan.assertData(salesInvoicePaymentPlanData);
    SalesInvoice.PaymentInPlan.PaymentInDetails salesInvoicePaymentDetails = salesInvoicePaymentPlan.new PaymentInDetails(
        mainPage);
    salesInvoicePaymentDetails.assertCount(0);

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.BankStatements bankStatement = financialAccount.new BankStatements(mainPage);
    String date;
    if (bankStatement.getRecordCount() == 0) {
      date = OBDate.CURRENT_DATE;
    } else {
      bankStatement.selectWithoutFiltering(0);
      date = OBDate.addDaysToDate((String) bankStatement.getData("transactionDate"), 1);
    }
    bankStatementHeaderData.addDataField("transactionDate", date);
    bankStatementHeaderData.addDataField("importdate", date);
    bankStatement.create(bankStatementHeaderData);
    bankStatement.assertSaved();
    bankStatement.assertData(bankStatementHeaderVerificationData);
    FinancialAccount.BankStatements.BankStatementLines bankStatementLines = bankStatement.new BankStatementLines(
        mainPage);
    bankStatementLinesData.addDataField("transactionDate", date);
    bankStatementLines.create(bankStatementLinesData);
    bankStatementLines.assertSaved();
    bankStatementLines.assertData(bankStatementLinesVerificationData);
    bankStatement.process();
    bankStatement.assertProcessCompletedSuccessfully2();

    @SuppressWarnings("rawtypes")
    MatchStatementProcess matchStatementProcess = financialAccount.openMatchStatement(false);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData.addDataField("banklineDate", date);
    matchStatementData.addDataField("trxDate", "");
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData);

    AddTransactionProcess addTransactionProcess = ((MatchStatementGrid) matchStatementProcess
        .getMatchStatementGrid()).clickAdd(0);
    addTransactionProcess.assertData(addTransactionVerificationData);
    AddPaymentProcess addPaymentProcess = addTransactionProcess.openAddPayment();
    String paymentNo = addPaymentProcess.getParameterValue("payment_documentno").toString();
    paymentNo = paymentNo.substring(1, paymentNo.length() - 1);
    addPaymentProcess.setParameterValue("fin_paymentmethod_id", "1 (Spain)");
    addPaymentProcess.process("Invoices", invoiceNo, "Process Received Payment(s)",
        addPaymentTotalsVerificationData);
    String invoiceDescription = "Invoice No.: " + invoiceNo + "\n";
    addTransactionVerificationData.addDataField("trxdate", date);
    addTransactionVerificationData.addDataField("dateacct", date);
    addTransactionVerificationData.addDataField("description", invoiceDescription);
    addTransactionProcess.assertData(addTransactionVerificationData);
    addTransactionProcess.process();

    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData2.addDataField("banklineDate", date);
    matchStatementData2.addDataField("trxDate", date);
    matchStatementData2.addDataField("fatDescription", invoiceDescription);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData2);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).clickClear(0);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData);
    matchStatementProcess.process();
    financialAccount.assertProcessCompletedSuccessfully2();

    FinancialAccount.Transaction transaction = financialAccount.new Transaction(mainPage);
    transaction.select(new TransactionsData.Builder().documentNo(paymentNo).build());
    transactionVerificationData.addDataField("transactionDate", date);
    transactionVerificationData.addDataField("dateAcct", date);
    transactionVerificationData.addDataField("description", invoiceDescription);
    transaction.assertData(transactionVerificationData);

    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentIn.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    paymentInHeaderVerificationData.addDataField("paymentDate", date);
    paymentInHeaderVerificationData.addDataField("documentNo", paymentNo);
    paymentInHeaderVerificationData.addDataField("description", invoiceDescription);
    paymentIn.assertData(paymentInHeaderVerificationData);
    PaymentIn.Lines paymentInLines = paymentIn.new Lines(mainPage);
    paymentInLines.assertCount(1);
    paymentInLinesVerificationData.addDataField("invoicePaymentSchedule$invoice$documentNo",
        invoiceNo);
    paymentInLines.assertData(paymentInLinesVerificationData);

    salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData2);
    salesInvoicePaymentPlan = salesInvoice.new PaymentInPlan(mainPage);
    salesInvoicePaymentPlan.assertCount(1);
    salesInvoicePaymentPlanData2.addDataField("lastPaymentDate", date);
    salesInvoicePaymentPlan.assertData(salesInvoicePaymentPlanData2);
    salesInvoicePaymentDetails = salesInvoicePaymentPlan.new PaymentInDetails(mainPage);
    salesInvoicePaymentDetails.assertCount(1);
    salesInvoicePaymentDetailsData.addDataField("paymentDetails$finPayment$paymentDate", date);
    salesInvoicePaymentDetailsData.addDataField("paymentDetails$finPayment$documentNo", paymentNo);
    salesInvoicePaymentDetails.assertData(salesInvoicePaymentDetailsData);

    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    matchStatementProcess = financialAccount.openMatchStatement(true);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData3.addDataField("banklineDate", date);
    matchStatementData3.addDataField("trxDate", date);
    matchStatementData3.addDataField("fatDescription", invoiceDescription);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData3);
    matchStatementProcess.process();
    financialAccount.assertProcessCompletedSuccessfully2();

    transaction = financialAccount.new Transaction(mainPage);
    transaction.select(new TransactionsData.Builder().documentNo(paymentNo).build());
    transactionVerificationData2.addDataField("transactionDate", date);
    transactionVerificationData2.addDataField("dateAcct", date);
    transactionVerificationData2.addDataField("description", invoiceDescription);
    transaction.assertData(transactionVerificationData2);

    paymentIn = new PaymentIn(mainPage).open();
    paymentIn.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    paymentInHeaderVerificationData2.addDataField("paymentDate", date);
    paymentInHeaderVerificationData2.addDataField("documentNo", paymentNo);
    paymentInHeaderVerificationData2.addDataField("description", invoiceDescription);
    paymentIn.assertData(paymentInHeaderVerificationData2);
    paymentInLines = paymentIn.new Lines(mainPage);
    paymentInLines.assertCount(1);
    paymentInLines.assertData(paymentInLinesVerificationData);

    salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData2);
    salesInvoicePaymentPlan = salesInvoice.new PaymentInPlan(mainPage);
    salesInvoicePaymentPlan.assertCount(1);
    salesInvoicePaymentPlan.assertData(salesInvoicePaymentPlanData2);
    salesInvoicePaymentDetails = salesInvoicePaymentPlan.new PaymentInDetails(mainPage);
    salesInvoicePaymentDetails.assertCount(1);
    salesInvoicePaymentDetailsData2.addDataField("paymentDetails$finPayment$paymentDate", date);
    salesInvoicePaymentDetailsData2.addDataField("paymentDetails$finPayment$documentNo", paymentNo);
    salesInvoicePaymentDetails.assertData(salesInvoicePaymentDetailsData2);

    logger.info(
        "** End of test case [APRRegression32268In] Test regression 32268 - Payment In flow. **");
  }
}
