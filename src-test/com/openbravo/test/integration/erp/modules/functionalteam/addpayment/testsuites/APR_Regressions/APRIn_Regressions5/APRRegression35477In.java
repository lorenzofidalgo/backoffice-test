/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions5;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddTransactionData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.BankStatementLinesData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddTransactionProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 35477
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression35477In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  TransactionsData transactionData;
  TransactionsData transactionVerificationData;
  TransactionsData depositAmountData;
  TransactionsData depositAmountVerificationData;
  TransactionsData withdrawalAmountData;
  TransactionsData withdrawalAmountVerificationData;
  BankStatementHeaderData bankStatementHeaderData;
  BankStatementHeaderData bankStatementHeaderVerificationData;
  BankStatementLinesData bankStatementLinesData;
  BankStatementLinesData bankStatementLinesVerificationData;
  AddTransactionData addTransactionDepositDataVerification;

  /**
   * Class constructor.
   *
   */
  public APRRegression35477In(TransactionsData transactionData,
      TransactionsData transactionVerificationData, TransactionsData depositAmountData,
      TransactionsData depositAmountVerificationData, TransactionsData withdrawalAmountData,
      TransactionsData withdrawalAmountVerificationData,
      BankStatementHeaderData bankStatementHeaderData,
      BankStatementHeaderData bankStatementHeaderVerificationData,
      BankStatementLinesData bankStatementLinesData,
      BankStatementLinesData bankStatementLinesVerificationData,
      AddTransactionData addTransactionDepositDataVerification) {
    this.transactionData = transactionData;
    this.transactionVerificationData = transactionVerificationData;
    this.depositAmountData = depositAmountData;
    this.depositAmountVerificationData = depositAmountVerificationData;
    this.withdrawalAmountData = withdrawalAmountData;
    this.withdrawalAmountVerificationData = withdrawalAmountVerificationData;
    this.bankStatementHeaderData = bankStatementHeaderData;
    this.bankStatementHeaderVerificationData = bankStatementHeaderVerificationData;
    this.bankStatementLinesData = bankStatementLinesData;
    this.bankStatementLinesVerificationData = bankStatementLinesVerificationData;
    this.addTransactionDepositDataVerification = addTransactionDepositDataVerification;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {

        new TransactionsData.Builder().transactionType("Bank fee")
            .depositAmount("20.00")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),

        new TransactionsData.Builder().transactionType("Bank fee")
            .depositAmount("20.00")
            .paymentAmount("0.00")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),

        new TransactionsData.Builder().depositAmount("10.00").build(),

        new TransactionsData.Builder().depositAmount("10.00").paymentAmount("0.00").build(),

        new TransactionsData.Builder().paymentAmount("10.00").build(),

        new TransactionsData.Builder().paymentAmount("10.00").depositAmount("0.00").build(),

        new BankStatementHeaderData.Builder().name("BankExample")
            .transactionDate(OBDate.addMonthsToSystemDate(1))
            .importDate(OBDate.addMonthsToSystemDate(1))
            .build(),

        new BankStatementHeaderData.Builder().name("BankExample")
            .documentType("Bank Statement File")
            .build(),

        new BankStatementLinesData.Builder().cramount("10.00")
            .transactionDate(OBDate.addMonthsToSystemDate(1))
            .build(),

        new BankStatementLinesData.Builder().cramount("10.00").dramount("0.00").build(),

        new AddTransactionData.Builder().depositamt("10.00").withdrawalamt("0.00").build(), } };
    return Arrays.asList(data);
  }

  /**
   *
   *
   *
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression35477InTest() throws ParseException {
    logger.info("** Start of test case [APRRegression35477In] Test regression 35477In **");

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.filter(new AccountData.Builder().name("Spain Bank").build());

    String date = OBDate.getSystemDate();

    FinancialAccount.Transaction transactions = financialAccount.new Transaction(mainPage);
    transactionData.addDataField("transactionDate", OBDate.addDaysToDate(date, 1));
    transactionData.addDataField("dateAcct", OBDate.addDaysToDate(date, 1));
    transactions.create(transactionData);
    transactions.assertSaved();
    transactions.assertData(transactionVerificationData);

    transactions.edit(depositAmountData);
    transactions.assertSaved();
    transactions.assertData(depositAmountVerificationData);

    transactions.edit(withdrawalAmountData);
    transactions.assertSaved();
    transactions.assertData(withdrawalAmountData);

    transactions.edit(depositAmountData);
    transactions.assertSaved();
    transactions.assertData(depositAmountVerificationData);
    transactions.process();
    transactions.assertProcessCompletedSuccessfully2();
    transactions.assertData(new TransactionsData.Builder().status("Deposited not Cleared").build());

    String faName = "Issue35477Sales";
    bankStatementLinesData.addDataField("referenceNo", faName);

    FinancialAccount.BankStatements bankStatement = financialAccount.new BankStatements(mainPage);
    bankStatement.create(bankStatementHeaderData);
    bankStatement.assertSaved();
    bankStatement.assertData(bankStatementHeaderData);

    FinancialAccount.BankStatements.BankStatementLines bankStatementLines = bankStatement.new BankStatementLines(
        mainPage);
    bankStatementLines.create(bankStatementLinesData);
    bankStatementLines.assertSaved();
    bankStatementLines.assertData(bankStatementLinesVerificationData);
    bankStatement.process();
    bankStatement.assertProcessCompletedSuccessfully2();

    @SuppressWarnings("rawtypes")
    MatchStatementProcess matchStatementProcess = financialAccount.openMatchStatement(false);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("referenceNo", faName);

    AddTransactionProcess transactionProcess = matchStatementProcess.clickAdd(0);

    transactionProcess.setParameterValue("trxtype", "Bank fee");
    transactionProcess.assertData(addTransactionDepositDataVerification);

    transactionProcess.setParameterValue("trxtype", "BP Deposit");
    transactionProcess.setParameterValue("description", "");
    transactionProcess.assertData(addTransactionDepositDataVerification);
    logger.info("** End of test case [APRRegression35477In] Test regression 35477 **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
