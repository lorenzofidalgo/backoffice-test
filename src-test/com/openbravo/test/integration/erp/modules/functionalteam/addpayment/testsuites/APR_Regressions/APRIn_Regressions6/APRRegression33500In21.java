/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions6;

import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.returnfromcustomer.PaymentInDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.returnfromcustomer.PaymentInPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.returnfromcustomer.ReturnFromCustomerHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.returnmaterialreceipt.ReturnMaterialReceiptHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ReturnFromCustomerLineSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ReturnMaterialReceiptLineSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.GoodsShipment;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.ReturnFromCustomer;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.ReturnFromCustomer.PaymentInPlan;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.ReturnMaterialReceipt;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 33500
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression33500In21 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  GoodsReceiptHeaderData goodsReceiptHeaderData;
  GoodsReceiptLinesData goodsReceiptLinesData;
  GoodsReceiptLinesData goodsReceiptLinesVerificationData;
  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  PaymentPlanData orderPaymentInPlanData;
  GoodsShipmentHeaderData goodsShipmentHeaderData;
  GoodsShipmentHeaderData goodsShipmentVerificationHeaderData;
  GoodsShipmentLinesData goodsShipmentLinesVerificationData;
  ReturnFromCustomerHeaderData returnFromCustomerHeaderData;
  ReturnFromCustomerHeaderData returnFromCustomerVerificationHeaderData;
  ReturnFromCustomerLineSelectorData pickEditLineEdit;
  ReturnFromCustomerHeaderData rfcBookedVerificationHeaderData;
  PaymentInPlanData rfcPaymentInVerificationData;
  ReturnMaterialReceiptHeaderData returnMaterialReceiptHeaderData;
  ReturnMaterialReceiptHeaderData returnMaterialReceiptVerificationHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePendingPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData;
  String[][] journalEntryLines;
  String[][] journalEntryLines2;
  PaymentInHeaderData paymentInHeaderVerificationData;
  PaymentInLinesData paymentInLinesVerificationData;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;
  PaymentInPlanData rfcPaidPaymentData;
  PaymentInDetailsData rfcPaidPaymentDetailsData;

  /**
   * Class constructor.
   *
   */
  public APRRegression33500In21(GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptLinesData goodsReceiptLinesData,
      GoodsReceiptLinesData goodsReceiptLinesVerificationData,
      SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      PaymentPlanData orderPaymentInPlanData, GoodsShipmentHeaderData goodsShipmentHeaderData,
      GoodsShipmentHeaderData goodsShipmentVerificationHeaderData,
      GoodsShipmentLinesData goodsShipmentLinesData,
      ReturnFromCustomerHeaderData returnFromCustomerHeaderData,
      ReturnFromCustomerHeaderData returnFromCustomerVerificationHeaderData,
      ReturnFromCustomerLineSelectorData pickEditLineEdit,
      ReturnFromCustomerHeaderData rfcBookedVerificationHeaderData,
      PaymentInPlanData rfcPaymentInVerificationData,
      ReturnMaterialReceiptHeaderData returnMaterialReceiptHeaderData,
      ReturnMaterialReceiptHeaderData returnMaterialReceiptVerificationHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePendingPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData,
      String[][] journalEntryLines, String[][] journalEntryLines2,
      PaymentInHeaderData paymentInHeaderVerificationData,
      PaymentInLinesData paymentInLinesVerificationData, String[][] journalEntryLines3,
      String[][] journalEntryLines4, PaymentInPlanData rfcPaidPaymentData,
      PaymentInDetailsData rfcPaidPaymentDetailsData) {
    this.orderPaymentInPlanData = orderPaymentInPlanData;
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptLinesData = goodsReceiptLinesData;
    this.goodsReceiptLinesVerificationData = goodsReceiptLinesVerificationData;
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.goodsShipmentHeaderData = goodsShipmentHeaderData;
    this.goodsShipmentVerificationHeaderData = goodsShipmentVerificationHeaderData;
    this.goodsShipmentLinesVerificationData = goodsShipmentLinesData;
    this.returnFromCustomerHeaderData = returnFromCustomerHeaderData;
    this.returnFromCustomerVerificationHeaderData = returnFromCustomerVerificationHeaderData;
    this.pickEditLineEdit = pickEditLineEdit;
    this.rfcBookedVerificationHeaderData = rfcBookedVerificationHeaderData;
    this.rfcPaymentInVerificationData = rfcPaymentInVerificationData;
    this.returnMaterialReceiptHeaderData = returnMaterialReceiptHeaderData;
    this.returnMaterialReceiptVerificationHeaderData = returnMaterialReceiptVerificationHeaderData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.invoicePendingPaymentInPlanData = invoicePendingPaymentInPlanData;
    this.invoicePaymentInPlanData = invoicePaymentInPlanData;
    this.invoicePaymentInDetailsData = invoicePaymentInDetailsData;
    this.journalEntryLines = journalEntryLines;
    this.journalEntryLines2 = journalEntryLines2;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.paymentInLinesVerificationData = paymentInLinesVerificationData;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    this.rfcPaidPaymentData = rfcPaidPaymentData;
    this.rfcPaidPaymentDetailsData = rfcPaidPaymentDetailsData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new GoodsReceiptHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),

        new GoodsReceiptLinesData.Builder().product(
            new ProductCompleteSelectorData.Builder().searchKey("VB").name("Volley Ball").build())
            .movementQuantity("100")
            .storageBin("USA111")
            .build(),

        new GoodsReceiptLinesData.Builder().movementQuantity("100").storageBin("USA111").build(),

        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentTerms("90 days")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Customer A")
            .build(),

        new SalesOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().searchKey("VB").name("Volley Ball").build())
            .orderedQuantity("100")
            .tax("Exempt 10%")
            .build(),

        new SalesOrderLinesData.Builder().uOM("Unit")
            .unitPrice("28.50")
            .listPrice("28.50")
            .lineNetAmount("2,850.00")
            .build(),

        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("2,850.00")
            .grandTotalAmount("2,850.00")
            .currency("EUR")
            .build(),

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("2,850.00")
            .received("0.00")
            .outstanding("2,850.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new GoodsShipmentHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),

        new GoodsShipmentHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .movementDate(OBDate.CURRENT_DATE)
            .build(),

        new GoodsShipmentLinesData.Builder().movementQuantity("100").storageBin("USA111").build(),

        new ReturnFromCustomerHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .invoiceTerms("Immediate")
            .paymentTerms("90 days")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new ReturnFromCustomerHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .invoiceTerms("Immediate")
            .paymentTerms("90 days")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .priceList("Customer A")
            .build(),

        new ReturnFromCustomerLineSelectorData.Builder().returned("10").build(),

        new ReturnFromCustomerHeaderData.Builder().documentStatus("Booked")
            .grandTotalAmount("285.00")
            .build(),

        new PaymentInPlanData.Builder().expected("-285.00")
            .outstanding("-285.00")
            .numberOfPayments("0")
            .build(),

        new ReturnMaterialReceiptHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),

        new ReturnMaterialReceiptHeaderData.Builder().organization("USA")
            .accountingDate(OBDate.CURRENT_DATE)
            .build(),

        new SalesInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentTerms("90 days")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new SalesInvoiceHeaderData.Builder().transactionDocument("AR Invoice")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentTerms("90 days")
            .build(),

        new SalesInvoiceLinesData.Builder().invoicedQuantity("-10")
            .lineNetAmount("-285.00")
            .build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("-285.00")
            .summedLineAmount("-285.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-285.00")
            .received("0.00")
            .outstanding("-285.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-285.00")
            .received("-285.00")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("-285.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("-285.00")
            .status("Deposited not Cleared")
            .build(),

        new String[][] { { "70000", "Ventas de mercaderías", "", "-285.00" },
            { "43000", "Clientes (euros)", "-285.00", "" } },

        new String[][] { { "  41100", " Sales", "", "-712.50" },
            { "11400", "Accounts receivable", "-712.50", "" } },

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("-285.00")
            .currency("EUR")
            .status("Deposited not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentInLinesData.Builder().dueDate(OBDate.CURRENT_DATE)
            .invoiceAmount("-285.00")
            .expected("-285.00")
            .received("-285.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new String[][] { { "43000", "Clientes (euros)", "", "-285.00" },
            { "43100", "Efectos comerciales en cartera", "-285.00", "" } },

        new String[][] { { "11400", "Accounts receivable", "", "-712.50" },
            { "11300", "Bank in transit", "-712.50", "" } },

        new PaymentInPlanData.Builder().expected("-285.00")
            .outstanding("0.00")
            .numberOfPayments("1")
            .build(),

        new PaymentInDetailsData.Builder().paidAmount("-285.00").build(),

        } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 33500 - Related to issue 35726
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression33500In21Test() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression33500In21] Test regression 33500 - Related to issue 35726. **");

    // Create a goods receipts from order
    GoodsReceipt goodsReceipt = new GoodsReceipt(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt.create(goodsReceiptHeaderData);
    goodsReceipt.assertSaved();

    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.create(goodsReceiptLinesData);
    goodsReceiptLines.assertSaved();
    goodsReceiptLines.assertData(goodsReceiptLinesVerificationData);

    // Complete the receipt
    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();

    String receiptNo = (String) goodsReceipt.getData().getDataField("documentNo");
    logger.info("** Goods receipt Document No. {} **", receiptNo);

    // Register a sales order
    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    String orderNo = salesOrder.getData("documentNo").toString();

    // Create order lines
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.create(salesOrderLinesData);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);
    String salesOrderIdentifier = String.format("%s - %s - %s", orderNo,
        salesOrder.getData("orderDate"), "2850.00");

    // Book the order
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);

    // Check pending order payment plan
    SalesOrder.PaymentInPlan orderPaymentInPlan = salesOrder.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(orderPaymentInPlanData);

    // Ship the product units
    GoodsShipment shipment = new GoodsShipment(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    shipment.create(goodsShipmentHeaderData);
    shipment.assertSaved();
    shipment.assertData(goodsShipmentVerificationHeaderData);

    String shipmentNo = (String) shipment.getData().getDataField("documentNo");
    logger.info("** Goods Shipment Document No. {} **", shipmentNo);

    shipment.createLinesFrom("USA111", salesOrderIdentifier);
    GoodsShipment.Lines shipmentLines = shipment.new Lines(mainPage);
    shipmentLines.assertCount(1);
    shipmentLines.assertData(goodsShipmentLinesVerificationData);

    // Complete the shipment
    shipment.complete();
    shipment.assertProcessCompletedSuccessfully2();

    // Return from customer the good shipment
    ReturnFromCustomer returnFromCustomer = new ReturnFromCustomer(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnFromCustomer.create(returnFromCustomerHeaderData);
    returnFromCustomer.assertSaved();
    returnFromCustomer.assertData(returnFromCustomerVerificationHeaderData);

    String rfcDocumentNo = (String) returnFromCustomer.getData("documentNo");
    logger.info("** Return From Customer Document No. {} **", rfcDocumentNo);

    PickAndExecuteWindow<ReturnFromCustomerLineSelectorData> pickEdit = returnFromCustomer
        .openPickAndEditLines();
    pickEdit.filter(
        new ReturnFromCustomerLineSelectorData.Builder().shipmentNumber(shipmentNo).build());
    pickEdit.edit(pickEditLineEdit);
    pickEdit.process();
    returnFromCustomer.book();

    returnFromCustomer.assertData(rfcBookedVerificationHeaderData);

    // Check the RFC Payment In Plan
    PaymentInPlan paymentInPlan = returnFromCustomer.new PaymentInPlan(mainPage);
    paymentInPlan.assertData(rfcPaymentInVerificationData);

    // Create the Return Material Receipt from Return From Customer
    ReturnMaterialReceipt materialReceipt = new ReturnMaterialReceipt(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    materialReceipt.create(returnMaterialReceiptHeaderData);
    materialReceipt.assertSaved();
    materialReceipt.assertData(returnMaterialReceiptVerificationHeaderData);

    // Create Receipt lines
    PickAndExecuteWindow<ReturnMaterialReceiptLineSelectorData> pickEditMaterialReceipt = materialReceipt
        .openPickAndEditLines();
    pickEditMaterialReceipt.filter(
        new ReturnMaterialReceiptLineSelectorData.Builder().rMOrderNo(rfcDocumentNo).build());
    pickEditMaterialReceipt
        .edit(new ReturnMaterialReceiptLineSelectorData.Builder().storageBin("USA111").build());
    pickEditMaterialReceipt.process();

    materialReceipt.complete();
    materialReceipt.assertProcessCompletedSuccessfully2();

    String receiptDocumentNo = (String) materialReceipt.getData("documentNo");
    String receiptId = String.format("%s - %s - %s", receiptDocumentNo,
        (String) materialReceipt.getData("movementDate"),
        (String) materialReceipt.getData("businessPartner"));
    logger.info("** Return Material Receipt Document No. {} **", receiptId);

    // Create a sales invoice from order
    SalesInvoice invoice = new SalesInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    invoice.create(salesInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(salesInvoiceHeaderVerificationData);
    invoice.createLinesFromShipment(receiptId);
    invoice.assertProcessCompletedSuccessfully();
    String invoiceNo = invoice.getData("documentNo").toString();
    SalesInvoice.Lines salesInvoiceLine = invoice.new Lines(mainPage);
    salesInvoiceLine.assertCount(1);
    salesInvoiceLine.assertData(salesInvoiceLineVerificationData);

    // Complete the invoice
    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedSalesInvoiceHeaderVerificationData);

    // Check invoice pending payment plan
    SalesInvoice.PaymentInPlan invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.assertData(invoicePendingPaymentInPlanData);

    // Pay the invoice
    AddPaymentProcess adp = invoice.openAddPayment();
    @SuppressWarnings("unchecked")
    List<Map<String, Object>> row = (List<Map<String, Object>>) adp.getOrderInvoiceGrid()
        .getSelectedRows();
    assertTrue("There must be one selected row", row.size() == 1);
    assertTrue(row.get(0).get("salesOrderNo").equals(rfcDocumentNo));
    assertTrue(row.get(0).get("invoiceNo").equals(invoiceNo));
    adp.process("Process Received Payment(s) and Deposit");
    invoice.assertPaymentCreatedSuccessfully();
    invoice.assertData(new SalesInvoiceHeaderData.Builder().paymentComplete(true).build());

    // Check invoice payment in plan
    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData);

    // Check invoice payment in details
    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(
        mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoicePaymentInDetailsData);

    String paymentIdentifier = invoicePaymentInDetails.getData("paymentDetails$finPayment")
        .toString();
    String paymentNo = paymentIdentifier.substring(0, paymentIdentifier.indexOf("-") - 1);

    // Check Sales order payment plan
    salesOrder = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    salesOrder.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentInPlan = salesOrder.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(orderPaymentInPlanData);

    // Check Return to Vendor Payment in Plan
    returnFromCustomer = (ReturnFromCustomer) new ReturnFromCustomer(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnFromCustomer
        .select(new ReturnFromCustomerHeaderData.Builder().documentNo(rfcDocumentNo).build());

    paymentInPlan = returnFromCustomer.new PaymentInPlan(mainPage);
    paymentInPlan.assertData(rfcPaidPaymentData);

    ReturnFromCustomer.PaymentInPlan.PaymentInDetails paymentInDetails = paymentInPlan.new PaymentInDetails(
        mainPage);
    paymentInDetails.assertData((PaymentInDetailsData) rfcPaidPaymentDetailsData
        .addDataField("payment", paymentIdentifier));

    // Check Payment in
    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    paymentIn.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    paymentIn.assertData(paymentInHeaderVerificationData);
    PaymentIn.Lines paymentInLines = paymentIn.new Lines(mainPage);
    paymentInLines.assertCount(1);
    paymentInLines.assertData((PaymentInLinesData) paymentInLinesVerificationData
        .addDataField("orderPaymentSchedule$order$documentNo", rfcDocumentNo)
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo)
        .addDataField("dueDate", OBDate.ADD_DAYS_TO_SYSTEM_DATE_90));

    logger.info(
        "** End of test case [APRRegression33500In21] Test regression 33500 - Related to issue 35726. **");
  }
}
