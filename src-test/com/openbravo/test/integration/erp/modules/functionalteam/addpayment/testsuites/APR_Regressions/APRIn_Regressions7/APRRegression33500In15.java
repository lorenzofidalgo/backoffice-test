/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions7;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 33500
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression33500In15 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPendingPaymentInPlanData;
  AddPaymentPopUpData addPaymentVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData;
  PaymentInHeaderData paymentInHeaderVerificationData;
  PaymentInLinesData paymentInLinesVerificationData;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;//
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentinDetailsData;
  String[][] journalEntryLines;
  String[][] journalEntryLines2;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaymentInPlanData2;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData2;
  PaymentInLinesData paymentInLinesInvoicedVerificationData;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;

  /**
   * Class constructor.
   *
   */
  public APRRegression33500In15(SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPendingPaymentInPlanData,
      AddPaymentPopUpData addPaymentVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData,
      PaymentInHeaderData paymentInHeaderVerificationData,
      PaymentInLinesData paymentInLinesVerificationData,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData,
      String[][] journalEntryLines, String[][] journalEntryLines2,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaymentInPlanData2,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData2,
      PaymentInLinesData paymentInLinesInvoicedVerificationData, String[][] journalEntryLines3,
      String[][] journalEntryLines4) {
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.orderPendingPaymentInPlanData = orderPendingPaymentInPlanData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.orderPaymentInPlanData = orderPaymentInPlanData;
    this.orderPaymentInDetailsData = orderPaymentInDetailsData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.paymentInLinesVerificationData = paymentInLinesVerificationData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.invoicePaymentInPlanData = invoicePaymentInPlanData;
    this.invoicePaymentinDetailsData = invoicePaymentInDetailsData;
    this.journalEntryLines = journalEntryLines;
    this.journalEntryLines2 = journalEntryLines2;
    this.orderPaymentInPlanData2 = orderPaymentInPlanData2;
    this.orderPaymentInDetailsData2 = orderPaymentInDetailsData2;
    this.paymentInLinesInvoicedVerificationData = paymentInLinesInvoicedVerificationData;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> SalesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("30-60")
            .invoiceTerms("Immediate")
            .build(),

        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("30-60")
            .priceList("Customer A")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("30-60")
            .invoiceTerms("Immediate")
            .build(),

        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGC")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("-10").tax("VAT 10% USA").build(),

        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("-20.00")
            .build(),

        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("-20.00")
            .grandTotalAmount("-22.00")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-22.00")
            .received("0.00")
            .outstanding("-22.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("Acc-3 (Payment-Trx-Reconciliation)")
            .c_currency_id("EUR")
            .actual_payment("-22.00")
            .expected_payment("-22.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("-22.00")
            .total("-22.00")
            .difference("0.00")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-22.00")
            .received("-22.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .expected("-22.00")
            .received("-22.00")
            .writeoff("0.00")
            .status("Deposited not Cleared")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("-22.00")
            .currency("EUR")
            .status("Deposited not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentInLinesData.Builder().dueDate(OBDate.CURRENT_DATE)
            .invoiceAmount("-22.00")
            .expected("-22.00")
            .received("-22.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new SalesInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("Reversed Sales Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentTerms("30-60")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new SalesInvoiceHeaderData.Builder().transactionDocument("Reversed Sales Invoice")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentTerms("30-60")
            .build(),

        new SalesInvoiceLinesData.Builder().invoicedQuantity("-10").lineNetAmount("-20.00").build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("-22.00")
            .summedLineAmount("-20.00")
            .currency("EUR")
            .paymentComplete(true)
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-11.00")
            .received("-11.00")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("-11.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("-11.00")
            .status("Deposited not Cleared")
            .build(),

        new String[][] { { "47700", "Hacienda Pública IVA repercutido", "", "-2.00" },
            { "70000", "Ventas de mercaderías", "", "-20.00" },
            { "43800", "438. Anticipos de clientes", "-22.00", "" }, },

        new String[][] { { "21400", "Tax Due", "", "-5.00" }, { "41100", "Sales", "", "-50.00" },
            { "2450", "Customer Prepayments", "-55.00", "" } },

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-22.00")
            .received("-22.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .expected("-11.00")
            .received("-11.00")
            .writeoff("0.00")
            .status("Deposited not Cleared")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("-22.00")
            .expected("-11.00")
            .received("-11.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new String[][] { { "43800", "438. Anticipos de clientes", "", "-22.00" },
            { "43100", "Efectos comerciales en cartera", "-22.00", "" } },
        new String[][] { { "2450", "Customer Prepayments", "", "-55.00" },
            { "11300", "Bank in transit", "-55.00", "" } } } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 33500 - Paid Negative Standard Order - Negative Reversed Invoice with Payment
   * Plan divided
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression33500In15Test() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression33500In15] Test regression 33500 - Paid Negative Standard Order - Negative Reversed Invoice with Payment Plan divided. **");

    // Register a Sales order
    SalesOrder order = new SalesOrder(mainPage).open();
    order.create(salesOrderHeaderData);
    order.assertSaved();
    order.assertData(salesOrderHeaderVerficationData);
    String orderNo = order.getData("documentNo").toString();

    // Create order lines
    SalesOrder.Lines orderLines = order.new Lines(mainPage);
    orderLines.create(salesOrderLinesData);
    orderLines.assertSaved();
    orderLines.assertData(salesOrderLinesVerificationData);

    // Book the order
    order.book();
    order.assertProcessCompletedSuccessfully2();
    order.assertData(bookedSalesOrderHeaderVerificationData);

    // Check pending order payment plan
    SalesOrder.PaymentInPlan orderPaymentPlan = order.new PaymentInPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPendingPaymentInPlanData);

    // Pay the order
    order.addPayment("-22.00", "Process Received Payment(s) and Deposit");
    order.assertPaymentCreatedSuccessfully();
    String salesOrderIdentifier = String.format("%s - %s - %s", orderNo, order.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));

    // Check order payment plan
    orderPaymentPlan = order.new PaymentInPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPaymentInPlanData);

    // Check order payment in details
    SalesOrder.PaymentInPlan.PaymentInDetails orderPaymentInDetails = orderPaymentPlan.new PaymentInDetails(
        mainPage);
    orderPaymentInDetails.assertCount(1);
    orderPaymentInDetails.assertData(orderPaymentInDetailsData);

    String paymentNo = orderPaymentInDetails.getData("payment").toString();
    paymentNo = paymentNo.substring(0, paymentNo.indexOf("-") - 1);

    // Check payment in
    PaymentIn payment = new PaymentIn(mainPage).open();
    payment.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    payment.assertData(paymentInHeaderVerificationData);
    PaymentIn.Lines paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(1);
    paymentInLines.assertData((PaymentInLinesData) paymentInLinesVerificationData
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo));

    // Create a Sales invoice from order
    SalesInvoice invoice = new SalesInvoice(mainPage).open();
    invoice.create(salesInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(salesInvoiceHeaderVerificationData);
    invoice.createLinesFrom(salesOrderIdentifier);
    invoice.assertProcessCompletedSuccessfully();
    SalesInvoice.Lines invoiceLines = invoice.new Lines(mainPage);
    invoiceLines.assertCount(1);
    invoiceLines.assertData(salesInvoiceLineVerificationData);
    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedSalesInvoiceHeaderVerificationData);
    String invoiceNo = (String) invoice.getData("documentNo");

    // Check first invoice payment plan
    SalesInvoice.PaymentInPlan invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(2);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData);

    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(
        mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoicePaymentinDetailsData);

    // Check second invoice payment plan
    invoicePaymentInPlan.closeForm();
    invoicePaymentInPlan.selectWithoutFiltering(1);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoicePaymentinDetailsData);

    // Post invoice and check post
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines.length);
    post.assertJournalLines2(journalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check Sales order payment plan
    order = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentInPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPaymentInPlanData2);

    orderPaymentInDetails = orderPaymentPlan.new PaymentInDetails(mainPage);
    orderPaymentInDetails.assertCount(2);
    orderPaymentInDetails.selectWithoutFiltering(0);
    orderPaymentInDetails.assertData(orderPaymentInDetailsData2);

    orderPaymentInDetails.selectWithoutFiltering(1);
    orderPaymentInDetails.assertData(orderPaymentInDetailsData2);

    // Check Payment in
    payment = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    payment.assertData(paymentInHeaderVerificationData);
    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);

    paymentInLinesInvoicedVerificationData.addDataField("orderPaymentSchedule$order$documentNo",
        orderNo);
    paymentInLinesInvoicedVerificationData.addDataField("invoicePaymentSchedule$invoice$documentNo",
        invoiceNo);
    paymentInLines.selectWithoutFiltering(0);
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData);
    paymentInLines.closeForm();

    paymentInLines.selectWithoutFiltering(1);
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData);
    paymentInLines.closeForm();

    // Post Payment in and check
    payment.open();
    payment.post();

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines3.length);
    post.assertJournalLines2(journalEntryLines3);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines4.length);
    post.assertJournalLines2(journalEntryLines4);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    logger.info(
        "** End of test case [APRRegression33500In15] Test regression 33500 - Paid Negative Standard Order - Negative Reversed Invoice with Payment Plan divided. **");
  }
}
