/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions8;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentGrid;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 26951
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression26951In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLines1Data;
  SalesOrderLinesData salesOrderLines1VerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPrePayPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPrePaidPaymentInDetailsData;
  PaymentInHeaderData paymentInPrepaidHeaderVerificationData;
  PaymentInLinesData paymentInPrePaidLinesVerificationData;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice1PaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice1PaymentInDetailsData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderInvoicedPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderInvoicedPaymentInDetailsData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderInvoicedPaymentInDetailsData1;
  PaymentInLinesData paymentInLinesInvoicedVerificationData;
  PaymentInLinesData paymentInLinesInvoicedVerificationData1;
  SalesInvoiceHeaderData completedSalesInvoice2HeaderVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice2PaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice2PaymentInDetailsData;
  String[][] journalEntryLines;
  String[][] journalEntryLines2;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;
  String[][] journalEntryLines5;
  String[][] journalEntryLines6;

  /**
   * Class constructor.
   *
   */
  public APRRegression26951In(SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData,
      SalesOrderLinesData salesOrderLines1Data,
      SalesOrderLinesData salesOrderLines1VerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPrePayPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData,
      PaymentInHeaderData paymentInPrepaidHeaderVerificationData,
      PaymentInLinesData paymentInPrePaidLinesVerificationData,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice1PaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice1PaymentInDetailsData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderInvoicedPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderInvoicedPaymentInDetailsData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderInvoicedPaymentInDetailsData1,
      PaymentInLinesData paymentInLinesInvoicedVerificationData,
      PaymentInLinesData paymentInLinesInvoicedVerificationData1,
      SalesInvoiceHeaderData completedSalesInvoice2HeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice2PaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice2PaymentInDetailsData,
      String[][] journalEntryLines, String[][] journalEntryLines2, String[][] journalEntryLines3,
      String[][] journalEntryLines4, String[][] journalEntryLines5, String[][] journalEntryLines6) {
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLines1Data = salesOrderLines1Data;
    this.salesOrderLines1VerificationData = salesOrderLines1VerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.orderPaymentInPlanData = orderPaymentInPlanData;
    this.orderPrePayPaymentInPlanData = orderPrePayPaymentInPlanData;
    this.orderPrePaidPaymentInDetailsData = orderPaymentInDetailsData;
    this.paymentInPrepaidHeaderVerificationData = paymentInPrepaidHeaderVerificationData;
    this.paymentInPrePaidLinesVerificationData = paymentInPrePaidLinesVerificationData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.invoice1PaymentInPlanData = invoice1PaymentInPlanData;
    this.invoice1PaymentInDetailsData = invoice1PaymentInDetailsData;
    this.orderInvoicedPaymentInPlanData = orderInvoicedPaymentInPlanData;
    this.orderInvoicedPaymentInDetailsData = orderInvoicedPaymentInDetailsData;
    this.orderInvoicedPaymentInDetailsData1 = orderInvoicedPaymentInDetailsData1;
    this.paymentInLinesInvoicedVerificationData = paymentInLinesInvoicedVerificationData;
    this.paymentInLinesInvoicedVerificationData1 = paymentInLinesInvoicedVerificationData1;
    this.completedSalesInvoice2HeaderVerificationData = completedSalesInvoice2HeaderVerificationData;
    this.invoice2PaymentInPlanData = invoice2PaymentInPlanData;
    this.invoice2PaymentInDetailsData = invoice2PaymentInDetailsData;
    this.journalEntryLines = journalEntryLines;
    this.journalEntryLines2 = journalEntryLines2;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    this.journalEntryLines5 = journalEntryLines5;
    this.journalEntryLines6 = journalEntryLines6;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> SalesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .invoiceTerms("Immediate")
            .build(),

        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Customer A")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .invoiceTerms("Immediate")
            .build(),

        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGC")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("9").tax("VAT 10% USA").build(),

        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("18.00")
            .build(),

        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("18.00")
            .grandTotalAmount("19.80")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("19.80")
            .received("0.00")
            .outstanding("19.80")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("19.80")
            .received("6.00")
            .outstanding("13.80")
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .expected("19.80")
            .received("6.00")
            .writeoff("0.00")
            .status("Deposited not Cleared")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("6.00")
            .currency("EUR")
            .status("Deposited not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("19.80")
            .expected("19.80")
            .received("6.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new SalesInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentTerms("90 days")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new SalesInvoiceHeaderData.Builder().transactionDocument("AR Invoice")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentTerms("90 days")
            .build(),

        new SalesInvoiceLinesData.Builder().invoicedQuantity("2")
            .lineNetAmount("4.00")
            .tax("VAT 10% USA")
            .build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("4.40")
            .summedLineAmount("4.00")
            .currency("EUR")
            .paymentComplete(true)
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("4.40")
            .received("4.40")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("4.40")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("4.40")
            .status("Deposited not Cleared")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("19.80")
            .received("6.00")
            .outstanding("13.80")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .expected("19.80")
            .received("1.60")
            .writeoff("0.00")
            .status("Deposited not Cleared")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .expected("4.40")
            .received("4.40")
            .writeoff("0.00")
            .status("Deposited not Cleared")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("19.80")
            .expected("19.80")
            .received("1.60")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("4.40")
            .expected("4.40")
            .received("4.40")
            .invoiceno("")
            .glitemname("")
            .build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("4.40")
            .summedLineAmount("4.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("4.40")
            .received("1.60")
            .outstanding("2.80")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("1.60")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("4.40")
            .status("Deposited not Cleared")
            .build(),

        new String[][] { { "43000", "Clientes (euros)", "2.80", "" },
            { "43800", "438. Anticipos de clientes", "1.60", "" },
            { "70000", "Ventas de mercaderías", "", "4.00" },
            { "47700", "Hacienda Pública IVA repercutido", "", "0.40" } },

        new String[][] { { "11400", "Accounts receivable", "7.00", "" },
            { "2450", "Customer Prepayments", "4.00", "" }, { "41100", "Sales", "", "10.00" },
            { "21400", "Tax Due", "", "1.00" } },

        new String[][] { { "43800", "438. Anticipos de clientes", "4.40", "" },
            { "70000", "Ventas de mercaderías", "", "4.00" },
            { "47700", "Hacienda Pública IVA repercutido", "", "0.40" } },

        new String[][] { { "2450", "Customer Prepayments", "11.00", "" },
            { "41100", "Sales", "", "10.00" }, { "21400", "Tax Due", "", "1.00" } },

        new String[][] { { "43100", "Efectos comerciales en cartera", "6.00", "" },
            { "43800", "438. Anticipos de clientes", "", "6.00" } },

        new String[][] { { "11300", "Bank in transit", "15.00", "" },
            { "2450", "Customer Prepayments", "", "15.00" }, },

        } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 26951
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression26951Test() throws ParseException {
    logger.info("** Start of test case [APRRegression26951In] Test regression 26951**");

    // Register a Sales order
    SalesOrder order = new SalesOrder(mainPage).open();
    order.create(salesOrderHeaderData);
    order.assertSaved();
    order.assertData(salesOrderHeaderVerficationData);
    String orderNo = order.getData("documentNo").toString();

    // Create order lines
    SalesOrder.Lines orderLines = order.new Lines(mainPage);
    orderLines.create(salesOrderLines1Data);
    orderLines.assertSaved();
    orderLines.assertData(salesOrderLines1VerificationData);

    String salesOrderIdentifier = String.format("%s - %s - %s", orderNo, order.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));

    // Book the order
    order.book();
    order.assertProcessCompletedSuccessfully2();
    order.assertData(bookedSalesOrderHeaderVerificationData);

    // Check pending order payment plan
    SalesOrder.PaymentInPlan orderPaymentInPlan = order.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(orderPaymentInPlanData);

    // Pre pay the order
    AddPaymentProcess paymentDetails = order.openAddPayment();

    AddPaymentGrid orderInvoiceGrid = paymentDetails.getOrderInvoiceGrid();
    orderInvoiceGrid.unselectAll();
    orderInvoiceGrid.clearFilters();
    orderInvoiceGrid.filter(new SelectedPaymentData.Builder().orderNo(orderNo).build());
    Sleep.trySleep(1000);
    orderInvoiceGrid.waitForDataToLoad();
    orderInvoiceGrid.selectRowByNumber(0);

    paymentDetails.setParameterValue("actual_payment", "6.00");
    paymentDetails.getField("reference_no").focus();
    paymentDetails.process("Process Received Payment(s) and Deposit");

    // Check pre pay order payment plan and details
    orderPaymentInPlan = order.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(orderPrePayPaymentInPlanData);

    SalesOrder.PaymentInPlan.PaymentInDetails orderPaymentInDetails = orderPaymentInPlan.new PaymentInDetails(
        mainPage);
    orderPaymentInDetails.assertCount(1);
    orderPaymentInDetails.selectWithoutFiltering(0);
    orderPaymentInDetails.assertData(orderPrePaidPaymentInDetailsData);

    String payment1No = orderPaymentInDetails.getData("payment").toString();
    payment1No = payment1No.substring(0, payment1No.indexOf("-") - 1);

    PaymentIn payment = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentInHeaderData.Builder().documentNo(payment1No).build());
    payment.assertData(paymentInPrepaidHeaderVerificationData);
    PaymentIn.Lines paymentLines = payment.new Lines(mainPage);
    paymentLines.assertCount(1);
    paymentInPrePaidLinesVerificationData.addDataField("orderPaymentSchedule$order$documentNo",
        orderNo);
    paymentLines.assertData(paymentInPrePaidLinesVerificationData);

    // Create a Sales invoice from order
    SalesInvoice invoice = new SalesInvoice(mainPage).open();
    invoice.create(salesInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(salesInvoiceHeaderVerificationData);
    invoice.createLinesFrom(salesOrderIdentifier);
    invoice.assertProcessCompletedSuccessfully();
    SalesInvoice.Lines invoiceLine = invoice.new Lines(mainPage);
    invoiceLine.assertCount(1);
    invoiceLine.edit(new SalesInvoiceLinesData.Builder().invoicedQuantity("2").build());
    invoiceLine.assertData(salesInvoiceLineVerificationData);
    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedSalesInvoiceHeaderVerificationData);
    String invoice1No = (String) invoice.getData("documentNo");

    // Check first invoice payment plan and details
    SalesInvoice.PaymentInPlan invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice1PaymentInPlanData);

    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(
        mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoice1PaymentInDetailsData);

    // Check Sales order payment in plan
    order = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentInPlan = order.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(orderInvoicedPaymentInPlanData);

    // Check Sales order payment in details
    orderPaymentInDetails = orderPaymentInPlan.new PaymentInDetails(mainPage);
    orderPaymentInDetails.assertCount(2);
    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().expected("19.80").received("1.60").build());
    orderPaymentInDetails.assertData(orderInvoicedPaymentInDetailsData);

    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().expected("4.40").received("4.40").build());
    orderPaymentInDetails.assertData(orderInvoicedPaymentInDetailsData1);

    // Check Payment In
    payment = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentInHeaderData.Builder().documentNo(payment1No).build());
    payment.assertData(paymentInPrepaidHeaderVerificationData);
    paymentLines = payment.new Lines(mainPage);
    paymentLines.assertCount(2);

    paymentInLinesInvoicedVerificationData.addDataField("orderPaymentSchedule$order$documentNo",
        orderNo);
    paymentInLinesInvoicedVerificationData1
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo)
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice1No);

    paymentLines.select(new PaymentInLinesData.Builder().received("1.60").orderno(orderNo).build());
    paymentLines.assertData(paymentInLinesInvoicedVerificationData);
    paymentLines.closeForm();

    paymentLines.select(new PaymentInLinesData.Builder().received("4.40")
        .orderno(orderNo)
        .invoiceno(invoice1No)
        .build());
    paymentLines.assertData(paymentInLinesInvoicedVerificationData1);
    paymentLines.closeForm();

    // Create a second invoice from order
    invoice = new SalesInvoice(mainPage).open();
    invoice.create(salesInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(salesInvoiceHeaderVerificationData);
    invoice.createLinesFrom(salesOrderIdentifier);
    invoice.assertProcessCompletedSuccessfully();
    invoiceLine = invoice.new Lines(mainPage);
    invoiceLine.assertCount(1);
    invoiceLine.edit(new SalesInvoiceLinesData.Builder().invoicedQuantity("2").build());
    invoiceLine.assertData(salesInvoiceLineVerificationData);
    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedSalesInvoice2HeaderVerificationData);
    String invoice2No = (String) invoice.getData("documentNo");

    // Check invoice2 payment plan and details
    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice2PaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoice2PaymentInDetailsData);

    // Post invoice2 and check post
    invoice.open();
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines.length);
    post.assertJournalLines2(journalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check invoice1 payment plan and details
    invoice = new SalesInvoice(mainPage).open();
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice1No).build());

    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice1PaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoice1PaymentInDetailsData);

    // Post invoice1 and check post
    invoice.open();
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines3.length);
    post.assertJournalLines2(journalEntryLines3);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines4.length);
    post.assertJournalLines2(journalEntryLines4);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check Sales order payment in plan
    order = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentInPlan = order.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(orderInvoicedPaymentInPlanData);

    // Check Sales order payment in details
    orderInvoicedPaymentInDetailsData.addDataField("expected", "4.40");
    orderPaymentInDetails = orderPaymentInPlan.new PaymentInDetails(mainPage);
    orderPaymentInDetails.assertCount(2);
    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().received("1.60").expected("4.40").build());
    orderPaymentInDetails.assertData(orderInvoicedPaymentInDetailsData);

    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().received("4.40").expected("4.40").build());
    orderPaymentInDetails.assertData(orderInvoicedPaymentInDetailsData1);

    // Check Payment In
    payment = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentInHeaderData.Builder().documentNo(payment1No).build());
    payment.assertData(paymentInPrepaidHeaderVerificationData);
    paymentLines = payment.new Lines(mainPage);
    paymentLines.assertCount(2);

    paymentInLinesInvoicedVerificationData.addDataField("expected", "4.40")
        .addDataField("invoiceAmount", "4.40")
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice2No);

    paymentLines.select(new PaymentInLinesData.Builder().received("1.60").orderno(orderNo).build());
    paymentLines.assertData(paymentInLinesInvoicedVerificationData);
    paymentLines.closeForm();

    paymentLines.select(new PaymentInLinesData.Builder().received("4.40")
        .orderno(orderNo)
        .invoiceno(invoice1No)
        .build());
    paymentLines.assertData(paymentInLinesInvoicedVerificationData1);
    paymentLines.closeForm();

    payment.open();
    payment.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines5.length);
    post.assertJournalLines2(journalEntryLines5);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines6.length);
    post.assertJournalLines2(journalEntryLines6);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    logger.info("** End of test case [APRRegression26951In] Test regression 26951**");
  }
}
