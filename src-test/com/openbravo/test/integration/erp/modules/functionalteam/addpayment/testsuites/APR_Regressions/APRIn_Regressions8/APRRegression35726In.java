/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions8;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.DiscountsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.GoodsShipment;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder.BasicDiscounts;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 35726
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression35726In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  DiscountsData salesOrderDiscountData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  PaymentPlanData orderPaymentInPlanData;
  GoodsShipmentHeaderData goodsShipmentHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceLinesData salesInvoiceLineEditedVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePendingPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData1;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData2;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData3;
  String[][] journalEntryLines;
  String[][] journalEntryLines2;
  PaymentPlanData orderPaidPaymentInPlanData;
  PaymentDetailsData orderPaidPaymentInDetailsData;
  PaymentInHeaderData paymentInHeaderVerificationData;
  PaymentInLinesData paymentInLinesVerificationData;
  PaymentInLinesData paymentInLinesVerificationData2;
  PaymentInLinesData paymentInLinesVerificationData3;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;

  /**
   * Class constructor.
   *
   */
  public APRRegression35726In(SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData, DiscountsData salesOrderDiscountData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      PaymentPlanData orderPaymentInPlanData, GoodsShipmentHeaderData goodsShipmentHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceLinesData salesInvoiceLineEditedVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePendingPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData1,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData2,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData3,
      String[][] journalEntryLines, String[][] journalEntryLines2,
      PaymentPlanData orderPaidPaymentInPlanData, PaymentDetailsData orderPaidPaymentInDetailsData,
      PaymentInHeaderData paymentInHeaderVerificationData,
      PaymentInLinesData paymentInLinesVerificationData,
      PaymentInLinesData paymentInLinesVerificationData2,
      PaymentInLinesData paymentInLinesVerificationData3, String[][] journalEntryLines3,
      String[][] journalEntryLines4) {
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.salesOrderDiscountData = salesOrderDiscountData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.orderPaymentInPlanData = orderPaymentInPlanData;
    this.goodsShipmentHeaderData = goodsShipmentHeaderData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.salesInvoiceLineEditedVerificationData = salesInvoiceLineEditedVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.invoicePendingPaymentInPlanData = invoicePendingPaymentInPlanData;
    this.invoicePaymentInPlanData = invoicePaymentInPlanData;
    this.invoicePaymentInDetailsData1 = invoicePaymentInDetailsData1;
    this.invoicePaymentInDetailsData2 = invoicePaymentInDetailsData2;
    this.invoicePaymentInDetailsData3 = invoicePaymentInDetailsData3;
    this.journalEntryLines = journalEntryLines;
    this.journalEntryLines2 = journalEntryLines2;
    this.orderPaidPaymentInPlanData = orderPaidPaymentInPlanData;
    this.orderPaidPaymentInDetailsData = orderPaidPaymentInDetailsData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.paymentInLinesVerificationData = paymentInLinesVerificationData;
    this.paymentInLinesVerificationData2 = paymentInLinesVerificationData2;
    this.paymentInLinesVerificationData3 = paymentInLinesVerificationData3;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("30-60")
            .build(),

        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("30-60")
            .priceList("Customer A")
            .build(),

        new SalesOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("FGA").priceListVersion("Sales").build())
            .orderedQuantity("1")
            .unitPrice("1400")
            .tax("VAT 10% USA")
            .build(),

        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("1,400.00")
            .lineNetAmount("1,400.00")
            .build(),

        new DiscountsData.Builder().discount("Discount 10%").build(),

        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("1,260.00")
            .grandTotalAmount("1,386.00")
            .currency("EUR")
            .build(),

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("1,386.00")
            .received("0.00")
            .outstanding("1,386.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new GoodsShipmentHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),

        new SalesInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentTerms("30-60")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new SalesInvoiceHeaderData.Builder().transactionDocument("AR Invoice")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentTerms("30-60")
            .build(),

        new SalesInvoiceLinesData.Builder().invoicedQuantity("1").lineNetAmount("1,400.00").build(),

        new SalesInvoiceLinesData.Builder().invoicedQuantity("1")
            .lineNetAmount("25,830.00")
            .build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("28,413.00")
            .summedLineAmount("25,830.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("14,206.50")
            .received("0.00")
            .outstanding("14,206.50")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("14,206.50")
            .received("14,206.50")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("1,386.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("14,206.50")
            .status("Deposited not Cleared")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("12,820.50")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("14,206.50")
            .status("Deposited not Cleared")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("14,206.50")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("14,206.50")
            .status("Deposited not Cleared")
            .build(),

        new String[][] { { "43000", "Clientes (euros)", "28,413.00", "" },
            { "70000", "Ventas de mercaderías", "", "25,830.00" },
            { "47700", "Hacienda Pública IVA repercutido", "", "2,583.00" } },

        new String[][] { { "11400", "Accounts receivable", "71,032.50", "" },
            { "41100", "Sales", "", "64,575.00" }, { "21400", "Tax Due", "", "6,457.50" }, },

        new PaymentPlanData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("1,386.00")
            .received("1,386.00")
            .outstanding("0.00")
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        new PaymentDetailsData.Builder().expected("14,206.50").received("1,386.00").build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("28,413.00")
            .currency("EUR")
            .status("Deposited not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("28,413.00")
            .expected("14,206.50")
            .received("1,386.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("28,413.00")
            .expected("14,206.50")
            .received("12,820.50")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("28,413.00")
            .expected("14,206.50")
            .received("14,206.50")
            .invoiceno("")
            .glitemname("")
            .build(),

        new String[][] { { "43100", "Efectos comerciales en cartera", "28,413.00", "" },
            { "43000", "Clientes (euros)", "", "28,413.00" }, },

        new String[][] { { "11300", "Bank in transit", "71,032.50", "" },
            { "11400", "Accounts receivable", "", "71,032.50" }, },

        } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 35726
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression35726Test() throws ParseException {
    logger.info("** Start of test case [APRRegression35726In] Test regression 35726**");

    // Register a sales order
    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    String orderNo = salesOrder.getData("documentNo").toString();

    // Create order lines
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.create(salesOrderLinesData);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);

    // Create the basic discount
    BasicDiscounts salesOrderDiscount = salesOrder.new BasicDiscounts(mainPage);
    salesOrderDiscount.create(salesOrderDiscountData);
    salesOrderDiscount.assertSaved();

    String salesOrderIdentifier = String.format("%s - %s - %s", orderNo,
        salesOrder.getData("orderDate"), "1386.00");

    // Book the order
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);

    // Check exists two lines
    salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.assertCount(2);

    // Check pending order payment plan
    SalesOrder.PaymentInPlan orderPaymentInPlan = salesOrder.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(orderPaymentInPlanData);

    // Create shipment from order
    GoodsShipment shipment = new GoodsShipment(mainPage).open();
    shipment.create(goodsShipmentHeaderData);
    shipment.assertSaved();
    shipment.createLinesFrom("USA111", salesOrderIdentifier);

    GoodsShipment.Lines shipmentLines = shipment.new Lines(mainPage);
    shipmentLines.assertCount(2);
    shipmentLines.selectWithoutFiltering(0);
    shipmentLines.edit(new GoodsShipmentLinesData.Builder().attributeSetValue(orderNo).build());

    shipment.complete();
    shipment.assertProcessCompletedSuccessfully2();

    // Create a sales invoice from order
    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    salesInvoice.createLinesFrom(salesOrderIdentifier, 1);
    salesInvoice.assertProcessCompletedSuccessfully();
    String invoiceNo = salesInvoice.getData("documentNo").toString();

    SalesInvoice.Lines salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.assertCount(1);
    salesInvoiceLine.assertData(salesInvoiceLineVerificationData);
    salesInvoiceLine.edit(new SalesInvoiceLinesData.Builder().unitPrice("25830").build());
    salesInvoiceLine.assertData(salesInvoiceLineEditedVerificationData);

    // Complete the invoice
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    // Check first invoice payment plan
    SalesInvoice.PaymentInPlan invoicePaymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(2);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoicePendingPaymentInPlanData);

    // Check second invoice payment plan
    invoicePaymentInPlan.closeForm();
    invoicePaymentInPlan.selectWithoutFiltering(1);
    invoicePaymentInPlan.assertData(invoicePendingPaymentInPlanData);

    // Pay the invoice
    AddPaymentProcess addPaymentProcess = salesInvoice.openAddPayment();
    addPaymentProcess.process("Invoices", invoiceNo, "Process Received Payment(s) and Deposit",
        null);

    salesInvoice.assertPaymentCreatedSuccessfully();
    salesInvoice.assertData(new SalesInvoiceHeaderData.Builder().paymentComplete(true).build());

    // Check first invoice payment plan
    invoicePaymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.clearFilters();
    invoicePaymentInPlan.assertCount(2);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData);

    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(
        mainPage);
    int firstPaymentDetailsCount = invoicePaymentInDetails.getRecordCount();

    // Check second invoice payment plan
    invoicePaymentInPlan.closeForm();
    invoicePaymentInPlan.unselectAll();
    invoicePaymentInPlan.selectWithoutFiltering(1);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    int secondPaymentDetailsCount = invoicePaymentInDetails.getRecordCount();

    assertEquals("Payment in details total should be 3", 3,
        firstPaymentDetailsCount + secondPaymentDetailsCount);

    // Check the Payment In Details with two records
    invoicePaymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.closeForm();
    invoicePaymentInPlan.unselectAll();
    invoicePaymentInPlan.selectWithoutFiltering(firstPaymentDetailsCount == 2 ? 0 : 1);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(2);
    invoicePaymentInDetails.select(
        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("1,386.00")
            .amount("14,206.50")
            .build());
    invoicePaymentInDetails.assertData(invoicePaymentInDetailsData1);
    invoicePaymentInDetails.closeForm();
    invoicePaymentInDetails.select(
        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("12,820.50")
            .amount("14,206.50")
            .build());
    invoicePaymentInDetails.assertData(invoicePaymentInDetailsData2);

    // Check the Payment In Details with one record
    invoicePaymentInPlan.unselectAll();
    invoicePaymentInPlan.selectWithoutFiltering(firstPaymentDetailsCount == 1 ? 0 : 1);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData);
    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.clearFilters();
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.selectWithoutFiltering(0);
    invoicePaymentInDetails.assertData(invoicePaymentInDetailsData3);

    String paymentIdentifier = invoicePaymentInDetails.getData("paymentDetails$finPayment")
        .toString();
    String paymentNo = paymentIdentifier.substring(0, paymentIdentifier.indexOf("-") - 1);

    // Post invoice and check
    salesInvoice.open();
    salesInvoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines.length);
    post.assertJournalLines2(journalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check Sales order payment plan
    salesOrder = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    salesOrder.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentInPlan = salesOrder.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(orderPaidPaymentInPlanData);

    SalesOrder.PaymentInPlan.PaymentInDetails orderPaymentDetails = orderPaymentInPlan.new PaymentInDetails(
        mainPage);
    orderPaymentDetails.assertCount(1);
    orderPaymentDetails.assertData(orderPaidPaymentInDetailsData);

    // Check Payment out
    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    paymentIn.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    paymentIn.assertData(paymentInHeaderVerificationData);
    PaymentIn.Lines paymentInLines = paymentIn.new Lines(mainPage);
    paymentInLines.assertCount(3);
    paymentInLines
        .select(new PaymentInLinesData.Builder().orderno(orderNo).invoiceno(invoiceNo).build());

    paymentInLines.assertData((PaymentInLinesData) paymentInLinesVerificationData
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo)
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo));

    paymentInLines.closeForm();
    paymentInLines.select(
        new PaymentInLinesData.Builder().received("12,820.50").invoiceno(invoiceNo).build());
    paymentInLines.assertData((PaymentInLinesData) paymentInLinesVerificationData2
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo));

    paymentInLines.closeForm();
    paymentInLines.select(
        new PaymentInLinesData.Builder().received("14,206.50").invoiceno(invoiceNo).build());
    paymentInLines.assertData((PaymentInLinesData) paymentInLinesVerificationData3
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo));

    // Post payment out and check
    paymentIn.open();
    paymentIn.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines3.length);
    post.assertJournalLines2(journalEntryLines3);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines4.length);
    post.assertJournalLines2(journalEntryLines4);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    logger.info("** End of test case [APRRegression35726In] Test regression 35726**");
  }
}
