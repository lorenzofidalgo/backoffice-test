/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions8;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.TaxData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test to create a sales order with multiple lines, add payment to sales order, create sales
 * invoice from order for each line separately, void the sales invoice.
 *
 * @author
 */

@RunWith(Parameterized.class)
public class APRRegression36297In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /** The sales order header data. */
  SalesOrderHeaderData salesOrderHeaderData;
  /** The data to verify the creation of the sales order header. */
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  /** The sales order line 1 data. */
  SalesOrderLinesData salesOrderLine1Data;
  /** The data to verify the creation of the sales order line 10. */
  SalesOrderLinesData salesOrderLine1VerificationData;
  /** The sales order line 2 data. */
  SalesOrderLinesData salesOrderLine2Data;
  /** The data to verify the creation of the sales order line 20. */
  SalesOrderLinesData salesOrderLine2VerificationData;
  /** The sales order line data 3. */
  SalesOrderLinesData salesOrderLine3Data;
  /** The data to verify the creation of the sales order line 30. */
  SalesOrderLinesData salesOrderLine3VerificationData;
  /** The booked sales order header verification data. */
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  /** The sales order payment plan data after order booking */
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData soPaymentPlanDataAfterBookedOrder;
  /** Add Payment Data */
  AddPaymentPopUpData addPaymentVerificationData;
  /** The taxes data. */
  TaxData[] taxesData;
  /** The sales order payment plan after add payment */
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData soPaymentPlanDataAfterOrderPaid;
  /** The sales order payment details after add payment */
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData paymentDetailsDataAfterOrderPaid;
  /** The sales invoice header data */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /** The data to verify creation of the sales invoice header */
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  /** The completed sales invoice 1 header verification data */
  SalesInvoiceHeaderData completedSalesInvoice1HeaderVerificationData;
  /** The payment plan for first sales invoice */
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice1PaymentPlan;
  /** The payment details for first sales invoice */
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice1PaymentDetails;
  /** The sales order payment plan data after first sales invoice */
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData soPaymentPlanDataAfterFirstSalesInvoice;
  /** The sales order payment details 1 data filter after first sales invoice */
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails1DataChk;
  /** The sales order payment details 1 verification data after first sales invoice */
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails1DataAfterFirstSalesInvoice;
  /** The sales order payment details 2 data filter after first sales invoice */
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails2DataChk;
  /** The sales order payment details 2 verification data after first sales invoice */
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails2Data;
  /** The completed sales invoice 2 header verification data */
  SalesInvoiceHeaderData completedSalesInvoice2HeaderVerificationData;
  /** The payment plan for second sales invoice */
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice2PaymentPlan;
  /** The payment details for second sales invoice */
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice2PaymentDetails;
  /** The sales order payment plan data */
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData soPaymentPlanDataAfterSecondInvoice;
  /** The sales order payment details 1 verification data */
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails1DataAfterSecondInvoice;
  /** The sales order payment details 3 data filter */
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails3DataChk;
  /** The sales order payment details 3 verification data */
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails3Data;
  /** The payment details for second sales invoice after void */
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice2PaymentDetailsAfterVoid;
  /** The payment plan of reverse invoice */
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData reverseInvoicePaymentPlan;
  /** The payment plan details of reverse invoice */
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData reverseInvoicePaymentDetails;
  /** The completed sales invoice 3 header verification data */
  SalesInvoiceHeaderData completedSalesInvoice3HeaderVerificationData;
  /** The payment plan for third sales invoice */
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice3PaymentPlan;
  /** The payment details for third sales invoice */
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice3PaymentDetails;
  /** The sales order payment details 4 data filter */
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails4DataChk;
  /** The sales order payment details 4 verification data */
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails4Data;

  /**
   * Class constructor.
   *
   */

  public APRRegression36297In(SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLine1Data,
      SalesOrderLinesData salesOrderLine1VerificationData, SalesOrderLinesData salesOrderLine2Data,
      SalesOrderLinesData salesOrderLine2VerificationData, SalesOrderLinesData salesOrderLine3Data,
      SalesOrderLinesData salesOrderLine3VerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData soPaymentPlanDataAfterBookedOrder,
      AddPaymentPopUpData addPaymentVerificationData, TaxData[] taxesData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData soPaymentPlanDataAfterOrderPaid,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData paymentDetailsDataAfterOrderPaid,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceHeaderData completedSalesInvoice1HeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice1PaymentPlan,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice1PaymentDetails,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData soPaymentPlanDataAfterFirstSalesInvoice,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails1DataChk,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails1DataAfterFirstSalesInvoice,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails2DataChk,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails2Data,
      SalesInvoiceHeaderData completedSalesInvoice2HeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice2PaymentPlan,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice2PaymentDetails,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData soPaymentPlanDataAfterSecondInvoice,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails1DataAfterSecondInvoice,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails3DataChk,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails3Data,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice2PaymentDetailsAfterVoid,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData reverseInvoicePaymentPlan,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData reverseInvoicePaymentDetails,
      SalesInvoiceHeaderData completedSalesInvoice3HeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice3PaymentPlan,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice3PaymentDetails,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails4DataChk,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData soPaymentDetails4Data) {

    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLine1Data = salesOrderLine1Data;
    this.salesOrderLine1VerificationData = salesOrderLine1VerificationData;
    this.salesOrderLine2Data = salesOrderLine2Data;
    this.salesOrderLine2VerificationData = salesOrderLine2VerificationData;
    this.salesOrderLine3Data = salesOrderLine3Data;
    this.salesOrderLine3VerificationData = salesOrderLine3VerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;

    this.soPaymentPlanDataAfterBookedOrder = soPaymentPlanDataAfterBookedOrder;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.taxesData = taxesData;
    this.soPaymentPlanDataAfterOrderPaid = soPaymentPlanDataAfterOrderPaid;
    this.paymentDetailsDataAfterOrderPaid = paymentDetailsDataAfterOrderPaid;

    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.completedSalesInvoice1HeaderVerificationData = completedSalesInvoice1HeaderVerificationData;
    this.invoice1PaymentPlan = invoice1PaymentPlan;
    this.invoice1PaymentDetails = invoice1PaymentDetails;
    this.soPaymentPlanDataAfterFirstSalesInvoice = soPaymentPlanDataAfterFirstSalesInvoice;
    this.soPaymentDetails1DataChk = soPaymentDetails1DataChk;
    this.soPaymentDetails1DataAfterFirstSalesInvoice = soPaymentDetails1DataAfterFirstSalesInvoice;
    this.soPaymentDetails2DataChk = soPaymentDetails2DataChk;
    this.soPaymentDetails2Data = soPaymentDetails2Data;

    this.completedSalesInvoice2HeaderVerificationData = completedSalesInvoice2HeaderVerificationData;
    this.invoice2PaymentPlan = invoice2PaymentPlan;
    this.invoice2PaymentDetails = invoice2PaymentDetails;
    this.soPaymentPlanDataAfterSecondInvoice = soPaymentPlanDataAfterSecondInvoice;
    this.soPaymentDetails1DataAfterSecondInvoice = soPaymentDetails1DataAfterSecondInvoice;
    this.soPaymentDetails3DataChk = soPaymentDetails3DataChk;
    this.soPaymentDetails3Data = soPaymentDetails3Data;

    this.invoice2PaymentDetailsAfterVoid = invoice2PaymentDetailsAfterVoid;
    this.reverseInvoicePaymentPlan = reverseInvoicePaymentPlan;
    this.reverseInvoicePaymentDetails = reverseInvoicePaymentDetails;

    this.completedSalesInvoice3HeaderVerificationData = completedSalesInvoice3HeaderVerificationData;
    this.invoice3PaymentPlan = invoice3PaymentPlan;
    this.invoice3PaymentDetails = invoice3PaymentDetails;
    this.soPaymentDetails4DataChk = soPaymentDetails4DataChk;
    this.soPaymentDetails4Data = soPaymentDetails4Data;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {

        // Create sales order header - salesOrderHeaderData
        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .warehouse("USA warehouse")
            .invoiceTerms("Immediate")
            .build(),

        // Verify sales order header - salesOrderHeaderVerficationData
        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .warehouse("USA warehouse")
            .priceList("Customer A")
            .paymentTerms("30 days, 5")
            .build(),

        // Create sales order line 10 - salesOrderLine1Data
        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGA")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("10").build(),

        // Verify sales order line 10 - salesOrderLine1VerificationData
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("Exempt 3%")
            .lineNetAmount("20.00")
            .build(),

        // Create sales order line 20 - salesOrderLine2Data
        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGA")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("20").build(),

        // Verify sales order line 20 - salesOrderLine2VerificationData
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("Exempt 3%")
            .lineNetAmount("40.00")
            .build(),

        // Create sales order line 30 - salesOrderLine3Data
        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGA")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("30").build(),

        // Verify sales order line 30 - salesOrderLine3VerificationData
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("Exempt 3%")
            .lineNetAmount("60.00")
            .build(),

        // Verify booked sales order - bookedSalesOrderHeaderVerificationData
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("120.00")
            .grandTotalAmount("120.00")
            .currency("EUR")
            .build(),

        // Verify sales order payment plan after booking - soPaymentPlanDataAfterBookedOrder
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("120.00")
            .received("0.00")
            .outstanding("120.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        // Sales order - addPaymentVerificationData
        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("Acc-3 (Payment-Trx-Reconciliation)")
            .fin_financial_account_id("Accounting Documents EURO - EUR")
            .c_currency_id("EUR")
            .actual_payment("120.00")
            .expected_payment("120.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("120.00")
            .total("120.00")
            .difference("0.00")
            .build(),

        // Verify sales order taxes
        new TaxData[] { new TaxData.Builder().tax("Exempt 3% - VAT 3%")
            .taxAmount("0.00")
            .taxableAmount("120.00")
            .build() },

        // Verify sales order payment plan - soPaymentPlanDataAfterOrderPaid
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("120.00")
            .received("120.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        // Verify sales order payment details - paymentDetailsDataAfterOrderPaid
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.CURRENT_DATE)
            .expected("120.00")
            .received("120.00")
            .writeoff("0.00")
            .status("Deposited not Cleared")
            .build(),

        // Create first sales invoice header - salesInvoiceHeaderData
        new SalesInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        // Verfiy first sales invoice header - salesInvoiceHeaderVerificationData

        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .paymentTerms("30 days, 5")
            .priceList("Customer A")
            .build(),

        // Verify completed first sales invoice - completedSalesInvoice1HeaderVerificationData
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .summedLineAmount("20.00")
            .grandTotalAmount("20.00")
            .currency("EUR")
            .build(),

        // Verify first sales invoice payment plan - invoice1PaymentPlan
        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .expectedDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("20.00")
            .received("20.00")
            .outstanding("0.00")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        // Verify first sales invoice payment details - invoice1PaymentDetails
        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .received("20.00")
            .amount("20.00")
            .writeoff("0.00")
            .status("Deposited not Cleared")
            .build(),

        // Verify sales order payment plan after first sales invoice -
        // soPaymentPlanDataAfterFirstInvoice
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("120.00")
            .received("120.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),

        // Filter 1 for sales order payment details - soPaymentDetails1DataChk
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .expected("120.00")
            .build(),

        // Verify sales order payment details 1 after first sales invoice -
        // soPaymentDetails1DataAfterFirstSalesInvoice
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.CURRENT_DATE)
            .expected("120.00")
            .received("100.00")
            .writeoff("0.00")
            .status("Deposited not Cleared")
            .build(),

        // Filter 2 for sales order payment details - soPaymentDetails2DataChk
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .expected("20.00")
            .build(),

        // Verify sales order payment details 2 after first sales invoice - soPaymentDetails2Data
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .expected("20.00")
            .received("20.00")
            .writeoff("0.00")
            .status("Deposited not Cleared")
            .build(),

        // Verify second sales invoice - - completedSalesInvoice2HeaderVerificationData
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .summedLineAmount("40.00")
            .grandTotalAmount("40.00")
            .currency("EUR")
            .build(),

        // Verify second sales invoice payment plan - invoice2PaymentPlan
        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .expectedDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("40.00")
            .received("40.00")
            .outstanding("0.00")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        // Verify second sales invoice payment details - invoice2PaymentDetails
        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .received("40.00")
            .amount("40.00")
            .writeoff("0.00")
            .status("Deposited not Cleared")
            .build(),

        // Verify sales order payment plan after second sales invoice -
        // soPaymentPlanDataAfterSecondInvoice
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("120.00")
            .received("120.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("3")
            .currency("EUR")
            .build(),

        // Filter 1 for sales order payment details - soPaymentDetails1DataChk

        // Verify sales order payment details 1 after second sales invoice -
        // soPaymentDetails1DataAfterSecondSalesInvoice
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.CURRENT_DATE)
            .expected("120.00")
            .received("60.00")
            .writeoff("0.00")
            .status("Deposited not Cleared")
            .build(),

        // Filter 2 for sales order payment details - soPaymentDetails2DataChk

        // Verify sales order payment details 2 after second sales invoice - soPaymentDetails2Data

        // Filter 3 for sales order payment details - soPaymentDetails3DataChk
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .expected("40.00")
            .build(),

        // Verify sales order payment details 3 after second sales invoice - soPaymentDetails3Data
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .expected("40.00")
            .received("40.00")
            .writeoff("0.00")
            .status("Deposited not Cleared")
            .build(),

        // Verify second sales invoice payment details after voiding -
        // invoice2PaymentDetailsAfterVoid
        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .received("40.00")
            .amount("40.00")
            .writeoff("0.00")
            .status("Payment Received")
            .build(),

        // Verify reverse sales invoice payment plan - reverseInvoicePaymentPlan
        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .expectedDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-40.00")
            .received("-40.00")
            .outstanding("0.00")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        // Verify reverse sales invoice payment details - reverseInvoicePaymentDetails
        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .received("-40.00")
            .amount("-40.00")
            .writeoff("0.00")
            .status("Payment Received")
            .build(),

        // Verify fourth sales invoice for order line no 20 -
        // completedSalesInvoice2HeaderVerificationData

        // Verify fourth sales invoice for order line no 30 -
        // completedSalesInvoice3HeaderVerificationData
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .summedLineAmount("60.00")
            .grandTotalAmount("60.00")
            .currency("EUR")
            .build(),

        // Verify third sales invoice payment plan - invoice3PaymentPlan
        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .expectedDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("60.00")
            .received("60.00")
            .outstanding("0.00")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        // Verify third sales invoice payment details - invoice3PaymentDetails
        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .received("60.00")
            .amount("60.00")
            .writeoff("0.00")
            .status("Deposited not Cleared")
            .build(),

        // Filter 4 for sales order payment details - soPaymentDetails4DataChk
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .expected("60.00")
            .build(),

        // Verify sales order payment details 4 after fourth sales invoice - soPaymentDetails4Data
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .expected("60.00")
            .received("60.00")
            .writeoff("0.00")
            .status("Deposited not Cleared")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test to create a sales order, add payment to order, sales invoice from that order.
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression36297Test() throws ParseException {

    logger.info("** Start of test case APRRegression36297In. **");

    // Create Sales Order with 3 lines
    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    final String orderNo = salesOrder.getData("documentNo").toString();
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.create(salesOrderLine1Data);
    salesOrderLines.assertData(salesOrderLine1VerificationData);
    salesOrderLines.create(salesOrderLine2Data);
    salesOrderLines.assertData(salesOrderLine2VerificationData);
    salesOrderLines.create(salesOrderLine3Data);
    salesOrderLines.assertData(salesOrderLine3VerificationData);
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);

    // Check sales order payment plan
    SalesOrder.PaymentInPlan orderPaymentInPlan = salesOrder.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(soPaymentPlanDataAfterBookedOrder);

    // Add Payment to sales order
    salesOrder.addPayment("Accounting Documents EURO - EUR",
        "Process Received Payment(s) and Deposit", addPaymentVerificationData);
    salesOrder.assertPaymentCreatedSuccessfully();

    // Check sales order payment plan
    orderPaymentInPlan = salesOrder.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(soPaymentPlanDataAfterOrderPaid);
    SalesOrder.PaymentInPlan.PaymentInDetails orderPaymentInDetails = orderPaymentInPlan.new PaymentInDetails(
        mainPage);
    orderPaymentInDetails.assertCount(1);
    orderPaymentInDetails.assertData(paymentDetailsDataAfterOrderPaid);

    DataObject completedSalesOrderHeaderData = salesOrder.getData();

    String salesOrderIdentifier = String.format("%s - %s - %s",
        completedSalesOrderHeaderData.getDataField("documentNo"),
        completedSalesOrderHeaderData.getDataField("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));

    // Create first Sales Invoice for order line no 10
    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    salesInvoice.createLinesFrom(salesOrderIdentifier, 1);
    salesInvoice.assertProcessCompletedSuccessfully();
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoice1HeaderVerificationData);

    // Check payment plan and details of first Sales Invoice
    SalesInvoice.PaymentInPlan invoicePaymentPlan = salesInvoice.new PaymentInPlan(mainPage);
    invoicePaymentPlan.assertCount(1);
    invoicePaymentPlan.assertData(invoice1PaymentPlan);
    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentDetails = invoicePaymentPlan.new PaymentInDetails(
        mainPage);
    invoicePaymentDetails.assertCount(1);
    invoicePaymentDetails.assertData(invoice1PaymentDetails);

    // Check Payment Plan and Payment Plan Details for Sales Order after first Sales Invoice
    SalesOrder salesOrder1 = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    salesOrder1.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    SalesOrder.PaymentInPlan orderPaymentInPlan1 = salesOrder1.new PaymentInPlan(mainPage);
    orderPaymentInPlan1.assertCount(1);
    orderPaymentInPlan1.assertData(soPaymentPlanDataAfterFirstSalesInvoice);
    SalesOrder.PaymentInPlan.PaymentInDetails orderPaymentInDetails1 = orderPaymentInPlan1.new PaymentInDetails(
        mainPage);
    orderPaymentInDetails1.assertCount(2);
    orderPaymentInDetails1.select(soPaymentDetails1DataChk);
    orderPaymentInDetails1.assertData(soPaymentDetails1DataAfterFirstSalesInvoice);
    orderPaymentInDetails1.cancelOnGrid();
    orderPaymentInDetails1.getTab().clearFilters();
    orderPaymentInDetails1.getTab().unselectAll();
    orderPaymentInDetails1.select(soPaymentDetails2DataChk);
    orderPaymentInDetails1.assertData(soPaymentDetails2Data);

    // Create second Sales Invoice for order line no 20
    SalesInvoice salesInvoice2 = new SalesInvoice(mainPage).open();
    salesInvoice2.create(salesInvoiceHeaderData);
    salesInvoice2.assertSaved();
    salesInvoice2.assertData(salesInvoiceHeaderVerificationData);
    salesInvoice2.createLinesFrom(salesOrderIdentifier, 2);
    salesInvoice2.assertProcessCompletedSuccessfully();
    salesInvoice2.complete();
    salesInvoice2.assertProcessCompletedSuccessfully2();
    salesInvoice2.assertData(completedSalesInvoice2HeaderVerificationData);

    // Check payment plan and details of second Sales Invoice
    SalesInvoice.PaymentInPlan invoicePaymentPlan2 = salesInvoice.new PaymentInPlan(mainPage);
    invoicePaymentPlan2.assertCount(1);
    invoicePaymentPlan2.assertData(invoice2PaymentPlan);
    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentDetails2 = invoicePaymentPlan2.new PaymentInDetails(
        mainPage);
    invoicePaymentDetails2.assertCount(1);
    invoicePaymentDetails2.assertData(invoice2PaymentDetails);

    // Store second invoice documentno
    final String invoiceNo = salesInvoice.getData("documentNo").toString();

    // Check Payment Plan and Payment Plan Details for Sales Order
    SalesOrder salesOrder2 = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    salesOrder2.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    SalesOrder.PaymentInPlan orderPaymentInPlan2 = salesOrder2.new PaymentInPlan(mainPage);
    orderPaymentInPlan2.assertCount(1);
    orderPaymentInPlan2.assertData(soPaymentPlanDataAfterSecondInvoice);
    SalesOrder.PaymentInPlan.PaymentInDetails orderPaymentInDetails2 = orderPaymentInPlan.new PaymentInDetails(
        mainPage);
    orderPaymentInDetails2.assertCount(3);
    orderPaymentInDetails2.select(soPaymentDetails1DataChk);
    orderPaymentInDetails2.assertData(soPaymentDetails1DataAfterSecondInvoice);
    orderPaymentInDetails2.cancelOnGrid();
    orderPaymentInDetails2.getTab().clearFilters();
    orderPaymentInDetails2.getTab().unselectAll();
    orderPaymentInDetails2.select(soPaymentDetails2DataChk);
    orderPaymentInDetails2.assertData(soPaymentDetails2Data);
    orderPaymentInDetails2.cancelOnGrid();
    orderPaymentInDetails2.getTab().clearFilters();
    orderPaymentInDetails2.getTab().unselectAll();
    orderPaymentInDetails2.select(soPaymentDetails3DataChk);
    orderPaymentInDetails2.assertData(soPaymentDetails3Data);

    // Void the second Sales Invoice
    SalesInvoice salesInvoice3 = new SalesInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    salesInvoice3.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    if (salesInvoice3.isPosted()) {
      salesInvoice3.unpost();
    }
    String reversedDocumentNo = invoiceNo.substring(0, 2)
        .concat(String.valueOf((Integer.valueOf(invoiceNo.substring(2)) + 1)));
    salesInvoice3.close();
    salesInvoice3
        .assertProcessCompletedSuccessfully2("Reversed by document: " + reversedDocumentNo);

    // Check payment plan and details of second sales invoice after voiding
    SalesInvoice.PaymentInPlan invoicePaymentPlan3 = salesInvoice3.new PaymentInPlan(mainPage);
    invoicePaymentPlan3.assertCount(1);
    invoicePaymentPlan3.assertData(invoice2PaymentPlan);
    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentDetails3 = invoicePaymentPlan3.new PaymentInDetails(
        mainPage);
    invoicePaymentDetails3.assertCount(1);
    invoicePaymentDetails3.assertData(invoice2PaymentDetailsAfterVoid);

    // Check payment plan and details of reverse sales invoice
    SalesInvoice salesInvoice4 = new SalesInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    salesInvoice4.getTab().refresh();
    salesInvoice4
        .select(new SalesInvoiceHeaderData.Builder().documentNo(reversedDocumentNo).build());
    // salesInvoice4.getTab().refresh();
    SalesInvoice.PaymentInPlan invoicePaymentPlan4 = salesInvoice4.new PaymentInPlan(mainPage);
    invoicePaymentPlan4.assertCount(1);
    invoicePaymentPlan4.assertData(reverseInvoicePaymentPlan);
    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentDetails4 = invoicePaymentPlan4.new PaymentInDetails(
        mainPage);
    invoicePaymentDetails4.assertCount(1);
    invoicePaymentDetails4.assertData(reverseInvoicePaymentDetails);

    // Check the Payment Plan and details for sales order after voiding
    SalesOrder salesOrder3 = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    salesOrder3.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());

    SalesOrder.PaymentInPlan orderPaymentInPlan3 = salesOrder3.new PaymentInPlan(mainPage);
    orderPaymentInPlan3.assertCount(1);
    orderPaymentInPlan3.assertData(soPaymentPlanDataAfterFirstSalesInvoice);
    SalesOrder.PaymentInPlan.PaymentInDetails orderPaymentInDetails3 = orderPaymentInPlan.new PaymentInDetails(
        mainPage);
    orderPaymentInDetails3.assertCount(2);
    orderPaymentInDetails3.select(soPaymentDetails1DataChk);
    orderPaymentInDetails3.assertData(soPaymentDetails1DataAfterFirstSalesInvoice);
    orderPaymentInDetails3.cancelOnGrid();
    orderPaymentInDetails3.getTab().clearFilters();
    orderPaymentInDetails3.getTab().unselectAll();
    orderPaymentInDetails3.select(soPaymentDetails2DataChk);
    orderPaymentInDetails3.assertData(soPaymentDetails2Data);

    // Register a sales invoice for order line no 20
    SalesInvoice salesInvoice5 = new SalesInvoice(mainPage).open();
    salesInvoice5.create(salesInvoiceHeaderData);
    salesInvoice5.assertSaved();
    salesInvoice5.assertData(salesInvoiceHeaderVerificationData);
    salesInvoice5.createLinesFrom(salesOrderIdentifier, 2);
    salesInvoice5.assertProcessCompletedSuccessfully();
    salesInvoice5.complete();
    salesInvoice5.assertProcessCompletedSuccessfully2();
    salesInvoice5.assertData(completedSalesInvoice2HeaderVerificationData);
    // Check payment plan and details of third Sales Invoice
    SalesInvoice.PaymentInPlan invoicePaymentPlan5 = salesInvoice.new PaymentInPlan(mainPage);
    invoicePaymentPlan5.assertCount(1);
    invoicePaymentPlan5.assertData(invoice2PaymentPlan);
    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentDetails5 = invoicePaymentPlan.new PaymentInDetails(
        mainPage);
    invoicePaymentDetails5.assertCount(1);
    invoicePaymentDetails5.assertData(invoice2PaymentDetails);

    // Check Payment Plan and Payment Plan Details for Sales Order
    SalesOrder salesOrder4 = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    Sleep.trySleep(20000);
    salesOrder4.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    SalesOrder.PaymentInPlan orderPaymentInPlan4 = salesOrder4.new PaymentInPlan(mainPage);
    orderPaymentInPlan4.assertCount(1);
    orderPaymentInPlan4.assertData(soPaymentPlanDataAfterSecondInvoice);
    SalesOrder.PaymentInPlan.PaymentInDetails orderPaymentInDetails4 = orderPaymentInPlan.new PaymentInDetails(
        mainPage);
    orderPaymentInDetails4.assertCount(3);
    orderPaymentInDetails4.select(soPaymentDetails1DataChk);
    orderPaymentInDetails4.assertData(soPaymentDetails1DataAfterSecondInvoice);
    orderPaymentInDetails4.cancelOnGrid();
    orderPaymentInDetails4.getTab().clearFilters();
    orderPaymentInDetails4.getTab().unselectAll();
    orderPaymentInDetails4.select(soPaymentDetails2DataChk);
    orderPaymentInDetails4.assertData(soPaymentDetails2Data);
    orderPaymentInDetails4.cancelOnGrid();
    orderPaymentInDetails4.getTab().clearFilters();
    orderPaymentInDetails4.getTab().unselectAll();
    orderPaymentInDetails4.select(soPaymentDetails3DataChk);
    orderPaymentInDetails4.assertData(soPaymentDetails3Data);

    // Register a sales invoice for order line no 30
    SalesInvoice salesInvoice6 = new SalesInvoice(mainPage).open();
    salesInvoice6.create(salesInvoiceHeaderData);
    salesInvoice6.assertSaved();
    salesInvoice6.assertData(salesInvoiceHeaderVerificationData);
    salesInvoice6.createLinesFrom(salesOrderIdentifier, 3);
    salesInvoice6.assertProcessCompletedSuccessfully();
    salesInvoice6.complete();
    salesInvoice6.assertProcessCompletedSuccessfully2();
    salesInvoice6.assertData(completedSalesInvoice3HeaderVerificationData);

    // Check payment plan and details of third Sales Invoice
    SalesInvoice.PaymentInPlan invoicePaymentPlan6 = salesInvoice.new PaymentInPlan(mainPage);
    invoicePaymentPlan6.assertCount(1);
    invoicePaymentPlan6.assertData(invoice3PaymentPlan);
    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentDetails6 = invoicePaymentPlan.new PaymentInDetails(
        mainPage);
    invoicePaymentDetails6.assertCount(1);
    invoicePaymentDetails6.assertData(invoice3PaymentDetails);

    // Check Payment Plan and Payment Plan Details for Sales Order
    SalesOrder salesOrder6 = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    salesOrder6.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    SalesOrder.PaymentInPlan orderPaymentInPlan6 = salesOrder.new PaymentInPlan(mainPage);
    orderPaymentInPlan6.assertCount(1);
    orderPaymentInPlan6.assertData(soPaymentPlanDataAfterSecondInvoice);
    SalesOrder.PaymentInPlan.PaymentInDetails orderPaymentInDetails6 = orderPaymentInPlan.new PaymentInDetails(
        mainPage);
    orderPaymentInDetails6.assertCount(3);
    orderPaymentInDetails6.select(soPaymentDetails4DataChk);
    orderPaymentInDetails6.assertData(soPaymentDetails4Data);
    orderPaymentInDetails6.cancelOnGrid();
    orderPaymentInDetails6.getTab().clearFilters();
    orderPaymentInDetails6.getTab().unselectAll();
    orderPaymentInDetails6.select(soPaymentDetails2DataChk);
    orderPaymentInDetails6.assertData(soPaymentDetails2Data);
    orderPaymentInDetails6.cancelOnGrid();
    orderPaymentInDetails6.getTab().clearFilters();
    orderPaymentInDetails6.getTab().unselectAll();
    orderPaymentInDetails6.select(soPaymentDetails3DataChk);
    orderPaymentInDetails6.assertData(soPaymentDetails3Data);

    logger.info("** End of test case APRRegression36297In. **");
  }
}
