/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions9;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentGrid;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount.PaymentMethod;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 36753
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression36753In1 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  AccountData accountData;
  PaymentMethodData paymentMethodData;
  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderLinesData salesOrderLinesData2;
  SalesOrderLinesData salesOrderLinesVerificationData2;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPendingPaymentInPlanData;
  AddPaymentPopUpData orderPaymentVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaidPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaidPaymentInDetailsData;
  PaymentInHeaderData paymentInHeaderVerificationData;
  PaymentInLinesData paymentInLinesVerificationData;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;//
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaidPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaidPaymentInDetailsData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderInvoicedPaymentInPlanData2;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderInvoicedPaymentInDetailsData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderInvoicedPaymentInDetailsData2;
  PaymentInLinesData paymentInLinesInvoicedVerificationData;
  PaymentInLinesData paymentInLinesInvoicedVerificationData2;
  PaymentInHeaderData paymentReactivatedInHeaderVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePendingPaymentInPlanData;
  PaymentInHeaderData paymentProcessInHeaderVerificationData3;
  String[][] journalEntryLines1;
  String[][] journalEntryLines2;
  PaymentPlanData orderPaymentReceivedPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData2;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentinDetailsData2;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;

  /**
   * Class constructor.
   *
   */
  public APRRegression36753In1(AccountData accountData, PaymentMethodData paymentMethodData,
      SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData, SalesOrderLinesData salesOrderLinesData2,
      SalesOrderLinesData salesOrderLinesVerificationData2,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPendingPaymentInPlanData,
      AddPaymentPopUpData orderPaymentVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaidPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaidPaymentInDetailsData,
      PaymentInHeaderData paymentInHeaderVerificationData,
      PaymentInLinesData paymentInLinesVerificationData,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaidPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaidPaymentInDetailsData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderInvoicedPaymentInPlanData2,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderInvoicedPaymentInDetailsData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderInvoicedPaymentInDetailsData2,
      PaymentInLinesData paymentInLinesInvoicedVerificationData,
      PaymentInLinesData paymentInLinesInvoicedVerificationData2,
      PaymentInHeaderData paymentReactivatedInHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePendingPaymentInPlanData,
      PaymentInHeaderData paymentProcessInHeaderVerificationData3, String[][] journalEntryLines1,
      String[][] journalEntryLines2, PaymentPlanData orderPaymentReceivedPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData2,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentinDetailsData2,
      String[][] journalEntryLines3, String[][] journalEntryLines4) {
    this.accountData = accountData;
    this.paymentMethodData = paymentMethodData;
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.salesOrderLinesData2 = salesOrderLinesData2;
    this.salesOrderLinesVerificationData2 = salesOrderLinesVerificationData2;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.orderPendingPaymentInPlanData = orderPendingPaymentInPlanData;
    this.orderPaymentVerificationData = orderPaymentVerificationData;
    this.orderPaidPaymentInPlanData = orderPaidPaymentInPlanData;
    this.orderPaidPaymentInDetailsData = orderPaidPaymentInDetailsData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.paymentInLinesVerificationData = paymentInLinesVerificationData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.invoicePaidPaymentInPlanData = invoicePaidPaymentInPlanData;
    this.invoicePaidPaymentInDetailsData = invoicePaidPaymentInDetailsData;
    this.orderInvoicedPaymentInPlanData2 = orderInvoicedPaymentInPlanData2;
    this.orderInvoicedPaymentInDetailsData = orderInvoicedPaymentInDetailsData;
    this.orderInvoicedPaymentInDetailsData2 = orderInvoicedPaymentInDetailsData2;
    this.paymentInLinesInvoicedVerificationData = paymentInLinesInvoicedVerificationData;
    this.paymentInLinesInvoicedVerificationData2 = paymentInLinesInvoicedVerificationData2;
    this.paymentReactivatedInHeaderVerificationData = paymentReactivatedInHeaderVerificationData;
    this.invoicePendingPaymentInPlanData = invoicePendingPaymentInPlanData;
    this.paymentProcessInHeaderVerificationData3 = paymentProcessInHeaderVerificationData3;
    this.journalEntryLines1 = journalEntryLines1;
    this.journalEntryLines2 = journalEntryLines2;
    this.orderPaymentReceivedPaymentInPlanData = orderPaymentReceivedPaymentInPlanData;
    this.invoicePaymentInPlanData2 = invoicePaymentInPlanData2;
    this.invoicePaymentinDetailsData2 = invoicePaymentinDetailsData2;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> SalesOrderValues() {
    Object[][] data = new Object[][] { {

        new AccountData.Builder().name("Accounting Documents EURO").build(),

        new PaymentMethodData.Builder().automaticDeposit(false)
            .payinExecutionType("Automatic")
            .payinExecutionProcess("Simple Execution Process")
            .payinDeferred(true)
            .build(),

        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .invoiceTerms("Immediate")
            .build(),

        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Customer A")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .invoiceTerms("Immediate")
            .build(),

        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGC")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("10").tax("VAT 10% USA").build(),

        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),

        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGA")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("15").tax("VAT 10% USA").build(),

        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("30.00")
            .build(),

        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("50.00")
            .grandTotalAmount("55.00")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("55.00")
            .received("0.00")
            .outstanding("55.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("Acc-3 (Payment-Trx-Reconciliation)")
            .fin_financial_account_id("Accounting Documents EURO - EUR")
            .c_currency_id("EUR")
            .actual_payment("55.00")
            .expected_payment("55.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("55.00")
            .total("55.00")
            .difference("0.00")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("55.00")
            .received("0.00")
            .outstanding("55.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .expected("55.00")
            .received("55.00")
            .writeoff("0.00")
            .status("Awaiting Execution")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("55.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Execution")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentInLinesData.Builder().dueDate(OBDate.CURRENT_DATE)
            .invoiceAmount("55.00")
            .expected("55.00")
            .received("55.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new SalesInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentTerms("90 days")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new SalesInvoiceHeaderData.Builder().transactionDocument("AR Invoice")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentTerms("90 days")
            .build(),

        new SalesInvoiceLinesData.Builder().invoicedQuantity("10").lineNetAmount("20.00").build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("22.00")
            .summedLineAmount("20.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("22.00")
            .received("0.00")
            .outstanding("22.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("22.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("22.00")
            .account("Accounting Documents EURO - EUR")
            .status("Awaiting Execution")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("55.00")
            .received("0.00")
            .outstanding("55.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .expected("22.00")
            .received("22.00")
            .writeoff("0.00")
            .status("Awaiting Execution")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .expected("55.00")
            .received("33.00")
            .writeoff("0.00")
            .status("Awaiting Execution")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("22.00")
            .expected("22.00")
            .received("22.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("55.00")
            .expected("55.00")
            .received("33.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("0.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Payment")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("22.00")
            .received("0.00")
            .outstanding("22.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("0")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("55.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Payment Received")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new String[][] { { "43100", "Efectos comerciales en cartera", "55.00", "" },
            { "43800", "438. Anticipos de clientes", "", "33.00" },
            { "43000", "Clientes (euros)", "", "22.00" } },
        new String[][] { { "11300", "Bank in transit", "137.50", "" },
            { "2450", "Customer Prepayments", "", "82.50" },
            { "11400", "Accounts receivable", "", "55.00" } },

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("55.00")
            .received("55.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("22.00")
            .received("22.00")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("22.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("22.00")
            .account("Accounting Documents EURO - EUR")
            .status("Payment Received")
            .build(),

        new String[][] { { "43000", "Clientes (euros)", "22.00", "" },
            { "70000", "Ventas de mercaderías", "", "20.00" },
            { "47700", "Hacienda Pública IVA repercutido", "", "2.00" }, },

        new String[][] { { "11400", "Accounts receivable", "55.00", "" },
            { "41100", "Sales", "", "50.00" }, { "21400", "Tax Due", "", "5.00" }, },

        } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 36753 Paying from order
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression36753In1Test() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression36753In1] Test regression 36753 Paying from order**");

    // Change payment method configuration
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountData);
    PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod
        .select(new PaymentMethodData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build());
    paymentMethod.edit(paymentMethodData);

    // // Get Business partner current balance
    // BusinessPartnerWindow businessPartnerWindow = new BusinessPartnerWindow();
    // mainPage.openView(businessPartnerWindow);
    // mainPage.waitForDataToLoad();
    //
    // BusinessPartnerTab businessPartnerTab = businessPartnerWindow.selectBusinessPartnerTab();
    // businessPartnerTab.select(new BusinessPartnerData.Builder().name("Customer A")
    // .searchKey("CUSA").build());
    // String currentBalance = (String) businessPartnerTab.getData("creditUsed");
    // currentBalance = currentBalance.replace(",", "");
    // BigDecimal bpCurrentBalance = new BigDecimal(currentBalance);

    // Register a Sales order
    SalesOrder order = new SalesOrder(mainPage).open();
    order.create(salesOrderHeaderData);
    order.assertSaved();
    order.assertData(salesOrderHeaderVerficationData);
    String orderNo = order.getData("documentNo").toString();

    // Create order lines
    SalesOrder.Lines orderLines = order.new Lines(mainPage);
    orderLines.create(salesOrderLinesData);
    orderLines.assertSaved();
    orderLines.assertData(salesOrderLinesVerificationData);

    orderLines = order.new Lines(mainPage);
    orderLines.create(salesOrderLinesData2);
    orderLines.assertSaved();
    orderLines.assertData(salesOrderLinesVerificationData2);

    // Book the order
    order.book();
    order.assertProcessCompletedSuccessfully2();
    order.assertData(bookedSalesOrderHeaderVerificationData);

    // Check pending order payment plan
    SalesOrder.PaymentInPlan orderPaymentPlan = order.new PaymentInPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPendingPaymentInPlanData);

    // Pay the order
    order.addPayment("Accounting Documents EURO - EUR", "Process Received Payment(s) and Deposit",
        orderPaymentVerificationData);
    order.assertPaymentCreatedSuccessfully();
    String salesOrderIdentifier = String.format("%s - %s - %s", orderNo, order.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));

    // Check order payment plan
    orderPaymentPlan = order.new PaymentInPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPaidPaymentInPlanData);

    // Check order payment in details
    SalesOrder.PaymentInPlan.PaymentInDetails orderPaymentInDetails = orderPaymentPlan.new PaymentInDetails(
        mainPage);
    orderPaymentInDetails.assertCount(1);
    orderPaymentInDetails.assertData(orderPaidPaymentInDetailsData);

    String paymentNo = orderPaymentInDetails.getData("payment").toString();
    paymentNo = paymentNo.substring(0, paymentNo.indexOf("-") - 1);

    // // Check Business Partner current balance
    // bpCurrentBalance = bpCurrentBalance.add(new BigDecimal(
    // (String) bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount")));
    // DecimalFormat format = new DecimalFormat("#,##0.00");
    // businessPartnerWindow = new BusinessPartnerWindow();
    // mainPage.openView(businessPartnerWindow);
    // mainPage.waitForDataToLoad();
    //
    // businessPartnerTab = businessPartnerWindow.selectBusinessPartnerTab();
    // businessPartnerTab.select(new BusinessPartnerData.Builder().name("Customer A")
    // .searchKey("CUSA").build());
    // businessPartnerTab.assertData(new BusinessPartnerData.Builder().creditUsed(
    // format.format(bpCurrentBalance)).build());

    // Check payment in created in add payment
    PaymentIn payment = new PaymentIn(mainPage).open();
    payment.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    payment.assertData(paymentInHeaderVerificationData);
    PaymentIn.Lines paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(1);
    paymentInLines.assertData((PaymentInLinesData) paymentInLinesVerificationData
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo));

    // Create a sales invoice from order
    SalesInvoice invoice = new SalesInvoice(mainPage).open();
    invoice.create(salesInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(salesInvoiceHeaderVerificationData);
    invoice.createLinesFrom(salesOrderIdentifier, 1);
    invoice.assertProcessCompletedSuccessfully();

    // Check invoice line
    SalesInvoice.Lines invoiceLines = invoice.new Lines(mainPage);
    invoiceLines.assertCount(1);
    invoiceLines.assertData(salesInvoiceLineVerificationData);
    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedSalesInvoiceHeaderVerificationData);
    String invoiceNo = (String) invoice.getData("documentNo");

    // Check invoice payment plan
    SalesInvoice.PaymentInPlan invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoicePaidPaymentInPlanData);

    // Check invoice payment details
    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(
        mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoicePaidPaymentInDetailsData);

    // Check invoiced sales order payment plan
    order = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentInPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderInvoicedPaymentInPlanData2);

    orderPaymentInDetails = orderPaymentPlan.new PaymentInDetails(mainPage);
    orderPaymentInDetails.assertCount(2);
    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().expected("22.00").received("22.00").build());
    orderPaymentInDetails.assertData(orderInvoicedPaymentInDetailsData);

    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().received("33.00").expected("55.00").build());
    orderPaymentInDetails.assertData(orderInvoicedPaymentInDetailsData2);

    // Check invoiced payment in
    payment = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    payment.assertData(paymentInHeaderVerificationData);
    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);

    paymentInLinesInvoicedVerificationData.addDataField("orderPaymentSchedule$order$documentNo",
        orderNo);
    paymentInLinesInvoicedVerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo)
        .addDataField("invoiceAmount", "22.00");

    // Check invoiced payment in lines
    paymentInLines
        .select(new PaymentInLinesData.Builder().received("22.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData);
    paymentInLines.closeForm();

    paymentInLines
        .select(new PaymentInLinesData.Builder().received("33.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData2);
    paymentInLines.closeForm();

    // Reactivate payment and delete lines & check
    payment.process("Reactivate and Delete Lines");
    payment.assertProcessCompletedSuccessfully2();
    payment.assertData(paymentReactivatedInHeaderVerificationData);

    // Check payment reactivated sales order payment plan
    order = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentInPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPendingPaymentInPlanData);

    // Check payment reactivated invoice payment plan
    invoice = new SalesInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoicePendingPaymentInPlanData);

    // Add Payment In details
    payment = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());

    AddPaymentProcess paymentDetails = payment.addDetailsOpen();
    paymentDetails.setParameterValue("actual_payment", "55.00");
    paymentDetails.setParameterValue("transaction_type", "Orders and Invoices");

    AddPaymentGrid orderInvoiceGrid = paymentDetails.getOrderInvoiceGrid();
    orderInvoiceGrid.unselectAll();
    orderInvoiceGrid.filter(new SelectedPaymentData.Builder().orderNo(orderNo).build());
    paymentDetails.process("Process Received Payment(s) and Deposit");

    // Check payment in and lines
    payment.assertData(paymentInHeaderVerificationData);
    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);

    paymentInLinesInvoicedVerificationData.addDataField("orderPaymentSchedule$order$documentNo",
        orderNo);
    paymentInLinesInvoicedVerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo)
        .addDataField("invoiceAmount", "22.00");
    paymentInLines
        .select(new PaymentInLinesData.Builder().received("22.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData);
    paymentInLines.closeForm();

    paymentInLines
        .select(new PaymentInLinesData.Builder().received("33.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData2);
    paymentInLines.closeForm();

    // Check sales order payment plan
    order = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentInPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderInvoicedPaymentInPlanData2);

    orderPaymentInDetails = orderPaymentPlan.new PaymentInDetails(mainPage);
    orderPaymentInDetails.assertCount(2);
    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().expected("22.00").received("22.00").build());
    orderPaymentInDetails.assertData(orderInvoicedPaymentInDetailsData);

    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().received("33.00").expected("55.00").build());
    orderPaymentInDetails.assertData(orderInvoicedPaymentInDetailsData2);

    // Check invoice payment plan
    invoice = new SalesInvoice(mainPage).open();
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo).build());

    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoicePaidPaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoicePaidPaymentInDetailsData);

    // Execute Payment in and check
    payment = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    payment.open();
    payment.execute();
    payment.assertData(paymentProcessInHeaderVerificationData3);

    // Check payment lines
    paymentInLines
        .select(new PaymentInLinesData.Builder().received("22.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData);
    paymentInLines.closeForm();

    paymentInLines
        .select(new PaymentInLinesData.Builder().received("33.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData2);
    paymentInLines.closeForm();

    // Post Payment in and check
    payment.open();
    payment.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines1.length);
    post.assertJournalLines2(journalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check payment executed sales order payment plan
    order = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentInPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPaymentReceivedPaymentInPlanData);

    orderInvoicedPaymentInDetailsData.addDataField("status", "Payment Received");
    orderInvoicedPaymentInDetailsData2.addDataField("status", "Payment Received");

    orderPaymentInDetails = orderPaymentPlan.new PaymentInDetails(mainPage);
    orderPaymentInDetails.assertCount(2);
    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().expected("22.00").received("22.00").build());
    orderPaymentInDetails.assertData(orderInvoicedPaymentInDetailsData);

    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().received("33.00").expected("55.00").build());
    orderPaymentInDetails.assertData(orderInvoicedPaymentInDetailsData2);

    // Check payment executed invoice payment plan
    invoice = new SalesInvoice(mainPage).open();
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo).build());

    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData2);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoicePaymentinDetailsData2);

    // Post invoice and check post
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines3.length);
    post.assertJournalLines2(journalEntryLines3);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines4.length);
    post.assertJournalLines2(journalEntryLines4);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Restore the payment method configuration
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountData);
    paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod
        .select(new PaymentMethodData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build());
    paymentMethod.edit(new PaymentMethodData.Builder().automaticDeposit(false)
        .payinExecutionType("Manual")
        .build());

    logger.info(
        "** End of test case [APRRegression36753In1] Test regression 36753 Paying from order**");
  }
}
