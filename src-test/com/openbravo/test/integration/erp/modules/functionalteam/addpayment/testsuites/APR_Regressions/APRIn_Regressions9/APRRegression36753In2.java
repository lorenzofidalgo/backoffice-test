/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions9;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount.PaymentMethod;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 36753
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression36753In2 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  AccountData accountData;
  PaymentMethodData paymentMethodData;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoice1LineData;
  SalesInvoiceLinesData salesInvoice1LineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoice1HeaderVerificationData;
  PaymentPlanData invoice1PendingPaymentInPlanData;
  SalesInvoiceLinesData salesInvoice2LineData;
  SalesInvoiceLinesData salesInvoice2LineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoice2HeaderVerificationData;
  PaymentPlanData invoice2PendingPaymentInPlanData;
  PaymentInHeaderData paymentInHeaderData;
  PaymentInHeaderData paymentInHeaderVerificationData;
  PaymentInLinesData paymentInLines1VerificationData;
  PaymentInLinesData paymentInLines2VerificationData;
  PaymentInHeaderData paymentInDetailsHeaderData;
  PaymentPlanData invoice1PaidPaymentInPlanData;
  PaymentDetailsData invoice1PaidPaymentInDetailsData;
  PaymentPlanData invoice2PaidPaymentInPlanData;
  PaymentDetailsData invoice2PaidPaymentInDetailsData;
  PaymentInHeaderData paymentReactivatedInHeaderVerificationData;
  PaymentInHeaderData paymentInExecutedHeaderData;
  String[][] journalEntryLines1;
  String[][] journalEntryLines2;
  PaymentPlanData invoice1ExecutedPaymentInPlanData;
  PaymentDetailsData invoice1ExecutedPaymentInDetailsData;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;
  PaymentPlanData invoice2ExecutedPaymentInPlanData;
  PaymentDetailsData invoice2ExecutedPaymentInDetailsData;
  String[][] journalEntryLines5;
  String[][] journalEntryLines6;

  /**
   * Class constructor.
   *
   */
  public APRRegression36753In2(AccountData accountData, PaymentMethodData paymentMethodData,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoice1LineData,
      SalesInvoiceLinesData salesInvoice1LineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoice1HeaderVerificationData,
      PaymentPlanData invoice1PendingPaymentInPlanData, SalesInvoiceLinesData salesInvoice2LineData,
      SalesInvoiceLinesData salesInvoice2LineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoice2HeaderVerificationData,
      PaymentPlanData invoice2PendingPaymentInPlanData, PaymentInHeaderData paymentInHeaderData,
      PaymentInHeaderData paymentInHeaderVerificationData,
      PaymentInLinesData paymentInLines1VerificationData,
      PaymentInLinesData paymentInLines2VerificationData,
      PaymentInHeaderData paymentInDetailsHeaderData, PaymentPlanData invoice1PaidPaymentInPlanData,
      PaymentDetailsData invoice1PaidPaymentInDetailsData,
      PaymentPlanData invoice2PaidPaymentInPlanData,
      PaymentDetailsData invoice2PaidPaymentInDetailsData,
      PaymentInHeaderData paymentReactivatedInHeaderVerificationData, String[][] journalEntryLines1,
      String[][] journalEntryLines2, PaymentPlanData invoice1ExecutedPaymentInPlanData,
      PaymentDetailsData invoice1ExecutedPaymentInDetailsData, String[][] journalEntryLines3,
      String[][] journalEntryLines4, PaymentPlanData invoice2ExecutedPaymentInPlanData,
      PaymentDetailsData invoice2ExecutedPaymentInDetailsData, String[][] journalEntryLines5,
      String[][] journalEntryLines6, PaymentInHeaderData paymentInExecutedHeaderData) {
    this.accountData = accountData;
    this.paymentMethodData = paymentMethodData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoice1LineData = salesInvoice1LineData;
    this.salesInvoice1LineVerificationData = salesInvoice1LineVerificationData;
    this.completedSalesInvoice1HeaderVerificationData = completedSalesInvoice1HeaderVerificationData;
    this.invoice1PendingPaymentInPlanData = invoice1PendingPaymentInPlanData;
    this.salesInvoice2LineData = salesInvoice2LineData;
    this.salesInvoice2LineVerificationData = salesInvoice2LineVerificationData;
    this.completedSalesInvoice2HeaderVerificationData = completedSalesInvoice2HeaderVerificationData;
    this.invoice2PendingPaymentInPlanData = invoice2PendingPaymentInPlanData;
    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.paymentInLines1VerificationData = paymentInLines1VerificationData;
    this.paymentInLines2VerificationData = paymentInLines2VerificationData;
    this.paymentInDetailsHeaderData = paymentInDetailsHeaderData;
    this.invoice1PaidPaymentInPlanData = invoice1PaidPaymentInPlanData;
    this.invoice1PaidPaymentInDetailsData = invoice1PaidPaymentInDetailsData;
    this.invoice2PaidPaymentInPlanData = invoice2PaidPaymentInPlanData;
    this.invoice2PaidPaymentInDetailsData = invoice2PaidPaymentInDetailsData;
    this.paymentReactivatedInHeaderVerificationData = paymentReactivatedInHeaderVerificationData;
    this.paymentInExecutedHeaderData = paymentInExecutedHeaderData;
    this.journalEntryLines1 = journalEntryLines1;
    this.journalEntryLines2 = journalEntryLines2;
    this.invoice1ExecutedPaymentInPlanData = invoice1ExecutedPaymentInPlanData;
    this.invoice1ExecutedPaymentInDetailsData = invoice1ExecutedPaymentInDetailsData;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    this.invoice2ExecutedPaymentInPlanData = invoice2ExecutedPaymentInPlanData;
    this.invoice2ExecutedPaymentInDetailsData = invoice2ExecutedPaymentInDetailsData;
    this.journalEntryLines5 = journalEntryLines5;
    this.journalEntryLines6 = journalEntryLines6;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> SalesOrderValues() {
    Object[][] data = new Object[][] { {

        new AccountData.Builder().name("Accounting Documents EURO").build(),

        new PaymentMethodData.Builder().automaticDeposit(false)
            .payinExecutionType("Automatic")
            .payinExecutionProcess("Simple Execution Process")
            .payinDeferred(true)
            .build(),

        new SalesInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentTerms("90 days")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new SalesInvoiceHeaderData.Builder().transactionDocument("AR Invoice")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentTerms("90 days")
            .build(),

        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("FGC")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("10")
            .tax("VAT 10% USA")
            .build(),

        new SalesInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("22.00")
            .summedLineAmount("20.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new PaymentPlanData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("22.00")
            .received("0.00")
            .outstanding("22.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("0")
            .build(),

        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("FGA")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("15")
            .tax("VAT 10% USA")
            .build(),

        new SalesInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("30.00")
            .build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("33.00")
            .summedLineAmount("30.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new PaymentPlanData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("33.00")
            .received("0.00")
            .outstanding("33.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("0")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("55.00")
            .account("Accounting Documents EURO - EUR")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("55.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Payment")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("22.00")
            .expected("22.00")
            .received("22.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("33.00")
            .expected("33.00")
            .received("33.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("55.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Execution")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentPlanData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("22.00")
            .received("0.00")
            .outstanding("22.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new PaymentDetailsData.Builder().received("22.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("22.00")
            .account("Accounting Documents EURO - EUR")
            .status("Awaiting Execution")
            .build(),

        new PaymentPlanData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("33.00")
            .received("0.00")
            .outstanding("33.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new PaymentDetailsData.Builder().received("33.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("33.00")
            .account("Accounting Documents EURO - EUR")
            .status("Awaiting Execution")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("0.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Payment")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new String[][] { { "43100", "Efectos comerciales en cartera", "55.00", "" },
            { "43000", "Clientes (euros)", "", "55.00" } },
        new String[][] { { "11300", "Bank in transit", "137.50", "" },
            { "11400", "Accounts receivable", "", "137.50" } },

        new PaymentPlanData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("22.00")
            .received("22.00")
            .outstanding("0.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new PaymentDetailsData.Builder().received("22.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("22.00")
            .account("Accounting Documents EURO - EUR")
            .status("Payment Received")
            .build(),

        new String[][] { { "43000", "Clientes (euros)", "22.00", "" },
            { "70000", "Ventas de mercaderías", "", "20.00" },
            { "47700", "Hacienda Pública IVA repercutido", "", "2.00" }, },

        new String[][] { { "11400", "Accounts receivable", "55.00", "" },
            { "41100", "Sales", "", "50.00" }, { "21400", "Tax Due", "", "5.00" }, },

        new PaymentPlanData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("33.00")
            .received("33.00")
            .outstanding("0.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new PaymentDetailsData.Builder().received("33.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("33.00")
            .account("Accounting Documents EURO - EUR")
            .status("Payment Received")
            .build(),

        new String[][] { { "43000", "Clientes (euros)", "33.00", "" },
            { "70000", "Ventas de mercaderías", "", "30.00" },
            { "47700", "Hacienda Pública IVA repercutido", "", "3.00" }, },

        new String[][] { { "11400", "Accounts receivable", "82.50", "" },
            { "41100", "Sales", "", "75.00" }, { "21400", "Tax Due", "", "7.50" }, },

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("55.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Payment Received")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 36753 Paying two invoice from payment in
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression36753In2Test() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression36753In2] Test regression 36753 Paying two invoice from payment in**");

    // Change payment method configuration
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountData);
    PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod
        .select(new PaymentMethodData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build());
    paymentMethod.edit(paymentMethodData);

    // // Get Business partner current balance
    // BusinessPartnerWindow businessPartnerWindow = new BusinessPartnerWindow();
    // mainPage.openView(businessPartnerWindow);
    // mainPage.waitForDataToLoad();
    //
    // BusinessPartnerTab businessPartnerTab = businessPartnerWindow.selectBusinessPartnerTab();
    // businessPartnerTab.select(new BusinessPartnerData.Builder().name("Customer A")
    // .searchKey("CUSA").build());
    // String currentBalance = (String) businessPartnerTab.getData("creditUsed");
    // currentBalance = currentBalance.replace(",", "");
    // BigDecimal bpCurrentBalance = new BigDecimal(currentBalance);

    // Create a Sales invoice
    SalesInvoice invoice = new SalesInvoice(mainPage).open();
    invoice.create(salesInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(salesInvoiceHeaderVerificationData);

    SalesInvoice.Lines invoiceLines = invoice.new Lines(mainPage);
    invoiceLines.create(salesInvoice1LineData);
    invoiceLines.assertData(salesInvoice1LineVerificationData);

    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedSalesInvoice1HeaderVerificationData);
    String invoice1No = (String) invoice.getData("documentNo");

    // Check invoice1 payment plan
    SalesInvoice.PaymentInPlan invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice1PendingPaymentInPlanData);

    // Create another invoice
    invoice = new SalesInvoice(mainPage).open();
    invoice.create(salesInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(salesInvoiceHeaderVerificationData);

    invoiceLines = invoice.new Lines(mainPage);
    invoiceLines.create(salesInvoice2LineData);
    invoiceLines.assertData(salesInvoice2LineVerificationData);

    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedSalesInvoice2HeaderVerificationData);

    // Check invoice2 payment plan
    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice2PendingPaymentInPlanData);

    String invoice2No = (String) invoice.getData("documentNo");

    // // Check Business Partner current balance
    // bpCurrentBalance = bpCurrentBalance.add(new BigDecimal(
    // (String) completedSalesInvoice1HeaderVerificationData.getDataField("grandTotalAmount")));
    // bpCurrentBalance = bpCurrentBalance.add(new BigDecimal(
    // (String) completedSalesInvoice2HeaderVerificationData.getDataField("grandTotalAmount")));
    // DecimalFormat format = new DecimalFormat("#,##0.00");
    // businessPartnerWindow = new BusinessPartnerWindow();
    // mainPage.openView(businessPartnerWindow);
    // mainPage.waitForDataToLoad();
    //
    // businessPartnerTab = businessPartnerWindow.selectBusinessPartnerTab();
    // businessPartnerTab.select(new BusinessPartnerData.Builder().name("Customer A")
    // .searchKey("CUSA").build());
    // businessPartnerTab.assertData(new BusinessPartnerData.Builder().creditUsed(
    // format.format(bpCurrentBalance)).build());

    // Create a payment in
    PaymentIn payment = new PaymentIn(mainPage).open();
    payment.create(paymentInHeaderData);
    payment.assertData(paymentInHeaderVerificationData);

    AddPaymentProcess paymentDetails = payment.addDetailsOpen();

    String invoicesFilter = String.format("==%s or ==%s", invoice1No, invoice2No);
    paymentDetails.process("Invoices", invoicesFilter, "Process Received Payment(s) and Deposit",
        null);

    PaymentIn.Lines paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);
    paymentInLines
        .select(new PaymentInLinesData.Builder().received("22.00").invoiceno(invoice1No).build());
    paymentInLines.assertData((PaymentInLinesData) paymentInLines1VerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice1No));

    paymentInLines
        .select(new PaymentInLinesData.Builder().received("33.00").invoiceno(invoice2No).build());
    paymentInLines.assertData((PaymentInLinesData) paymentInLines2VerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice2No));

    payment.assertData(paymentInDetailsHeaderData);
    String paymentNo = (String) payment.getData("documentNo");

    // Check invoice1 payment plan
    invoice = new SalesInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice1No).build());

    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice1PaidPaymentInPlanData);

    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(
        mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoice1PaidPaymentInDetailsData);

    // Check invoice2 payment plan
    invoice = new SalesInvoice(mainPage).open();
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice2No).build());

    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice2PaidPaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoice2PaidPaymentInDetailsData);

    // Reactivate and delete Payment in lines
    payment = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    payment.process("Reactivate and Delete Lines");
    payment.assertProcessCompletedSuccessfully2();
    payment.assertData(paymentReactivatedInHeaderVerificationData);

    // Check invoice1 payment plan
    invoice = new SalesInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice1No).build());
    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice1PendingPaymentInPlanData);

    // Check invoice2 payment plan
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice2No).build());
    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice2PendingPaymentInPlanData);

    // Add Payment details
    payment = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    payment.edit(new PaymentInHeaderData.Builder().amount("55.00").build());

    paymentDetails = payment.addDetailsOpen();
    paymentDetails.process("Invoices", invoicesFilter, "Process Received Payment(s) and Deposit",
        null);

    // Check payment and lines
    payment.assertData(paymentInDetailsHeaderData);

    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);
    paymentInLines
        .select(new PaymentInLinesData.Builder().received("22.00").invoiceno(invoice1No).build());
    paymentInLines.assertData((PaymentInLinesData) paymentInLines1VerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice1No));

    paymentInLines
        .select(new PaymentInLinesData.Builder().received("33.00").invoiceno(invoice2No).build());
    paymentInLines.assertData((PaymentInLinesData) paymentInLines2VerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice2No));

    // Check invoice1 payment plan
    invoice = new SalesInvoice(mainPage).open();
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice1No).build());
    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice1PaidPaymentInPlanData);

    // Check invoice2 payment plan
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice2No).build());
    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice2PaidPaymentInPlanData);

    // Execute Payment in and check
    payment = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    payment.open();
    payment.execute();
    payment.assertData(paymentInExecutedHeaderData);

    // Check payment lines
    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);
    paymentInLines
        .select(new PaymentInLinesData.Builder().received("22.00").invoiceno(invoice1No).build());
    paymentInLines.assertData((PaymentInLinesData) paymentInLines1VerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice1No));

    paymentInLines
        .select(new PaymentInLinesData.Builder().received("33.00").invoiceno(invoice2No).build());
    paymentInLines.assertData((PaymentInLinesData) paymentInLines2VerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice2No));

    // Post Payment in and check
    payment.open();
    payment.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines1.length);
    post.assertJournalLines2(journalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check invoice1 payment plan
    invoice = new SalesInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice1No).build());

    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice1ExecutedPaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoice1ExecutedPaymentInDetailsData);

    invoice.open();
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines3.length);
    post.assertJournalLines2(journalEntryLines3);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines4.length);
    post.assertJournalLines2(journalEntryLines4);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check invoice2 payment plan
    invoice = new SalesInvoice(mainPage).open();
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice2No).build());

    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice2ExecutedPaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoice2ExecutedPaymentInDetailsData);

    // Post invoice2 and check post
    invoice.open();
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines5.length);
    post.assertJournalLines2(journalEntryLines5);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines6.length);
    post.assertJournalLines2(journalEntryLines6);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Restore the payment method configuration
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountData);
    paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod
        .select(new PaymentMethodData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build());
    paymentMethod.edit(new PaymentMethodData.Builder().automaticDeposit(false)
        .payinExecutionType("Manual")
        .build());

    logger.info(
        "** End of test case [APRRegression36753In2] Test regression 36753 Paying two invoice from payment in**");
  }
}
