/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions9;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentGrid;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount.PaymentMethod;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 36753
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression36753In3 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  AccountData accountData;
  PaymentMethodData paymentMethodData;
  PaymentInHeaderData paymentInHeaderData;
  PaymentInHeaderData paymentInHeaderVerificationData;
  PaymentInHeaderData paymentInDetailsHeaderData;
  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderLinesData salesOrderLinesData2;
  SalesOrderLinesData salesOrderLinesVerificationData2;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPendingPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaidPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaidPaymentInDetailsData;
  PaymentInHeaderData paymentIn2HeaderVerificationData;
  PaymentInLinesData paymentIn2LinesVerificationData;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaidPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaidPaymentInDetailsData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderInvoicedPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderInvoicedPaymentInDetailsData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderInvoicedPaymentInDetailsData2;
  PaymentInLinesData paymentInLinesInvoicedVerificationData;
  PaymentInLinesData paymentInLinesInvoicedVerificationData2;
  SalesInvoiceLinesData salesInvoice2LineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoice2HeaderVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice2PaidPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice2PaidPaymentInDetailsData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderInvoiced2PaymentInDetailsData;
  PaymentInLinesData paymentInLinesInvoiced2VerificationData;
  PaymentInHeaderData paymentReactivatedInHeaderVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice1PendingPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice2PendingPaymentInPlanData;
  PaymentInHeaderData paymentInExecutedHeaderData;
  String[][] journalEntryLines1;
  String[][] journalEntryLines2;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaymentReceivedPaymentInPlanData;
  PaymentPlanData invoice1ExecutedPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice1ExecutedPaymentInDetailsData;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;
  PaymentPlanData invoice2ExecutedPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice2ExecutedPaymentInDetailsData;
  String[][] journalEntryLines5;
  String[][] journalEntryLines6;

  /**
   * Class constructor.
   *
   */
  public APRRegression36753In3(AccountData accountData, PaymentMethodData paymentMethodData,
      PaymentInHeaderData paymentInHeaderData, PaymentInHeaderData paymentInHeaderVerificationData,
      PaymentInHeaderData paymentInDetailsHeaderData, SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData, SalesOrderLinesData salesOrderLinesData2,
      SalesOrderLinesData salesOrderLinesVerificationData2,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPendingPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaidPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaidPaymentInDetailsData,
      PaymentInHeaderData paymentIn2HeaderVerificationData,
      PaymentInLinesData paymentIn2LinesVerificationData,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderInvoicedPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData2,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData3,
      PaymentInLinesData paymentInLinesInvoicedVerificationData,
      PaymentInLinesData paymentInLinesInvoicedVerificationData2,
      SalesInvoiceLinesData salesInvoice2LineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoice2HeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice2PaidPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice2PaidPaymentInDetailsData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderInvoiced2PaymentInDetailsData,
      PaymentInLinesData paymentInLinesInvoiced2VerificationData,
      PaymentInHeaderData paymentReactivatedInHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice1PendingPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice2PendingPaymentInPlanData,
      PaymentInHeaderData paymentInExecutedHeaderData, String[][] journalEntryLines1,
      String[][] journalEntryLines2,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaymentReceivedPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice1ExecutedPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice1ExecutedPaymentInDetailsData,
      String[][] journalEntryLines3, String[][] journalEntryLines4,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice2ExecutedPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice2ExecutedPaymentInDetailsData,
      String[][] journalEntryLines5, String[][] journalEntryLines6) {

    this.accountData = accountData;
    this.paymentMethodData = paymentMethodData;
    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.paymentInDetailsHeaderData = paymentInDetailsHeaderData;
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.salesOrderLinesData2 = salesOrderLinesData2;
    this.salesOrderLinesVerificationData2 = salesOrderLinesVerificationData2;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.orderPendingPaymentInPlanData = orderPendingPaymentInPlanData;
    this.orderPaidPaymentInPlanData = orderPaidPaymentInPlanData;
    this.orderPaidPaymentInDetailsData = orderPaidPaymentInDetailsData;
    this.paymentIn2HeaderVerificationData = paymentIn2HeaderVerificationData;
    this.paymentIn2LinesVerificationData = paymentIn2LinesVerificationData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.invoicePaidPaymentInPlanData = invoicePaymentInPlanData;
    this.invoicePaidPaymentInDetailsData = invoicePaymentInDetailsData;
    this.orderInvoicedPaymentInPlanData = orderInvoicedPaymentInPlanData;
    this.orderInvoicedPaymentInDetailsData = orderPaymentInDetailsData2;
    this.orderInvoicedPaymentInDetailsData2 = orderPaymentInDetailsData3;
    this.paymentInLinesInvoicedVerificationData = paymentInLinesInvoicedVerificationData;
    this.paymentInLinesInvoicedVerificationData2 = paymentInLinesInvoicedVerificationData2;
    this.salesInvoice2LineVerificationData = salesInvoice2LineVerificationData;
    this.completedSalesInvoice2HeaderVerificationData = completedSalesInvoice2HeaderVerificationData;
    this.invoice2PaidPaymentInPlanData = invoice2PaidPaymentInPlanData;
    this.invoice2PaidPaymentInDetailsData = invoice2PaidPaymentInDetailsData;
    this.orderInvoiced2PaymentInDetailsData = orderInvoiced2PaymentInDetailsData;
    this.paymentInLinesInvoiced2VerificationData = paymentInLinesInvoiced2VerificationData;
    this.paymentReactivatedInHeaderVerificationData = paymentReactivatedInHeaderVerificationData;
    this.invoice1PendingPaymentInPlanData = invoice1PendingPaymentInPlanData;
    this.invoice2PendingPaymentInPlanData = invoice2PendingPaymentInPlanData;
    this.paymentInExecutedHeaderData = paymentInExecutedHeaderData;
    this.journalEntryLines1 = journalEntryLines1;
    this.journalEntryLines2 = journalEntryLines2;
    this.orderPaymentReceivedPaymentInPlanData = orderPaymentReceivedPaymentInPlanData;
    this.invoice1ExecutedPaymentInPlanData = invoice1ExecutedPaymentInPlanData;
    this.invoice1ExecutedPaymentInDetailsData = invoice1ExecutedPaymentInDetailsData;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    this.invoice2ExecutedPaymentInPlanData = invoice2ExecutedPaymentInPlanData;
    this.invoice2ExecutedPaymentInDetailsData = invoice2ExecutedPaymentInDetailsData;
    this.journalEntryLines5 = journalEntryLines5;
    this.journalEntryLines6 = journalEntryLines6;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> SalesOrderValues() {
    Object[][] data = new Object[][] { {

        new AccountData.Builder().name("Accounting Documents EURO").build(),

        new PaymentMethodData.Builder().automaticDeposit(false)
            .payinExecutionType("Automatic")
            .payinExecutionProcess("Simple Execution Process")
            .payinDeferred(true)
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("1000")
            .account("Accounting Documents EURO - EUR")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("1,000.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Payment")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("1,000.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Execution")
            .generatedCredit("1,000.00")
            .usedCredit("0.00")
            .build(),

        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .invoiceTerms("Immediate")
            .build(),

        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .transactionDocument("Standard Order")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Customer A")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .invoiceTerms("Immediate")
            .build(),

        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGC")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("745").tax("Exempt").build(),

        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("1,490.00")
            .build(),

        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGA")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("255").tax("Exempt").build(),

        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("510.00")
            .build(),

        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("2,000.00")
            .grandTotalAmount("2,000.00")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("2,000.00")
            .received("0.00")
            .outstanding("2,000.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("2,000.00")
            .received("0.00")
            .outstanding("2,000.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .expected("2,000.00")
            .received("2,000.00")
            .writeoff("0.00")
            .status("Awaiting Execution")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("1,000.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Execution")
            .generatedCredit("0.00")
            .usedCredit("1,000.00")
            .build(),

        new PaymentInLinesData.Builder().dueDate(OBDate.CURRENT_DATE)
            .invoiceAmount("2,000.00")
            .expected("2,000.00")
            .received("2,000.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new SalesInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentTerms("90 days")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new SalesInvoiceHeaderData.Builder().transactionDocument("AR Invoice")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentTerms("90 days")
            .build(),

        new SalesInvoiceLinesData.Builder().invoicedQuantity("745")
            .lineNetAmount("1,490.00")
            .build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("1,490.00")
            .summedLineAmount("1,490.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("1,490.00")
            .received("0.00")
            .outstanding("1,490.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("1,490.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("1,490.00")
            .account("Accounting Documents EURO - EUR")
            .status("Awaiting Execution")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("2,000.00")
            .received("0.00")
            .outstanding("2,000.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .expected("2,000.00")
            .received("510.00")
            .writeoff("0.00")
            .status("Awaiting Execution")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .expected("1,490.00")
            .received("1,490.00")
            .writeoff("0.00")
            .status("Awaiting Execution")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("2,000.00")
            .expected("2,000.00")
            .received("510.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("1,490.00")
            .expected("1,490.00")
            .received("1,490.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new SalesInvoiceLinesData.Builder().invoicedQuantity("255").lineNetAmount("510.00").build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("510.00")
            .summedLineAmount("510.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("510.00")
            .received("0.00")
            .outstanding("510.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("510.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("510.00")
            .account("Accounting Documents EURO - EUR")
            .status("Awaiting Execution")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .expected("510.00")
            .received("510.00")
            .writeoff("0.00")
            .status("Awaiting Execution")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("510.00")
            .expected("510.00")
            .received("510.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("0.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Payment")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("1,490.00")
            .received("0.00")
            .outstanding("1,490.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("0")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("510.00")
            .received("0.00")
            .outstanding("510.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("0")
            .build(),
        //
        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("1,000.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Payment Received")
            .generatedCredit("0.00")
            .usedCredit("1,000.00")
            .build(),

        new String[][] { { "43800", "438. Anticipos de clientes", "1,000.00", "" },
            { "43100", "Efectos comerciales en cartera", "1,000.00", "" },
            { "43000", "Clientes (euros)", "", "2,000.00" } },
        new String[][] { { "2450", "Customer Prepayments", "2,500.00", "" },
            { "11300", "Bank in transit", "2,500.00", "" },
            { "11400", "Accounts receivable", "", "5,000.00" } },

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("2,000.00")
            .received("2,000.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),

        new PaymentPlanData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("1,490.00")
            .received("1,490.00")
            .outstanding("0.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("1,490.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("1,490.00")
            .account("Accounting Documents EURO - EUR")
            .status("Payment Received")
            .build(),

        new String[][] { { "43000", "Clientes (euros)", "1,490.00", "" },
            { "70000", "Ventas de mercaderías", "", "1,490.00" }, },

        new String[][] { { "11400", "Accounts receivable", "3,725.00", "" },
            { "41100", "Sales", "", "3,725.00" } },

        new PaymentPlanData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("510.00")
            .received("510.00")
            .outstanding("0.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("510.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("510.00")
            .account("Accounting Documents EURO - EUR")
            .status("Payment Received")
            .build(),

        new String[][] { { "43000", "Clientes (euros)", "510.00", "" },
            { "70000", "Ventas de mercaderías", "", "510.00" }, },

        new String[][] { { "11400", "Accounts receivable", "1,275.00", "" },
            { "41100", "Sales", "", "1,275.00" } },

        } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 36753 Paying from order using credit
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression36753In3Test() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression36753In3] Test regression 36753 Paying from order using credit**");

    // Change payment method configuration
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountData);
    PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod
        .select(new PaymentMethodData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build());
    paymentMethod.edit(paymentMethodData);

    // // Get Business partner current balance
    // BusinessPartnerWindow businessPartnerWindow = new BusinessPartnerWindow();
    // mainPage.openView(businessPartnerWindow);
    // mainPage.waitForDataToLoad();
    //
    // BusinessPartnerTab businessPartnerTab = businessPartnerWindow.selectBusinessPartnerTab();
    // businessPartnerTab.select(new BusinessPartnerData.Builder().name("Customer A")
    // .searchKey("CUSA").build());
    //
    // businessPartnerTab.getForm().assertStatusBarFields(
    // new BusinessPartnerData.Builder().creditUsed("8,432.80").build());
    //
    // String currentBalance = (String) businessPartnerTab.getData("creditUsed");
    // currentBalance = currentBalance.replace(",", "");
    // BigDecimal bpCurrentBalance = new BigDecimal(currentBalance);

    // Create a payment in with credit of 1000
    PaymentIn payment = new PaymentIn(mainPage).open();
    payment.create(paymentInHeaderData);
    payment.assertData(paymentInHeaderVerificationData);

    AddPaymentProcess paymentDetails = payment.addDetailsOpen();
    AddPaymentGrid orderInvoiceGrid = paymentDetails.getOrderInvoiceGrid();
    orderInvoiceGrid.unselectAll();
    paymentDetails.process("Process Received Payment(s) and Deposit",
        "Leave the credit to be used later",
        new AddPaymentPopUpData.Builder().actual_payment("1,000.00").build());

    payment.assertData(paymentInDetailsHeaderData);
    PaymentIn.Lines paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(1);
    paymentInLines.assertData(new PaymentInLinesData.Builder().received("1,000.00").build());

    String payment1No = (String) payment.getData("documentNo");

    // Execute payment and check status
    payment.execute();
    payment.assertData(new PaymentInHeaderData.Builder().status("Payment Received").build());

    // // Check Business Partner current balance
    // bpCurrentBalance = bpCurrentBalance.subtract(new BigDecimal("1000.00"));
    // DecimalFormat format = new DecimalFormat("#,##0.00");
    // businessPartnerWindow = new BusinessPartnerWindow();
    // mainPage.openView(businessPartnerWindow);
    // mainPage.waitForDataToLoad();
    //
    // businessPartnerTab = businessPartnerWindow.selectBusinessPartnerTab();
    // businessPartnerTab.select(new BusinessPartnerData.Builder().name("Customer A")
    // .searchKey("CUSA").build());
    // businessPartnerTab.assertData(new BusinessPartnerData.Builder().creditUsed(
    // format.format(bpCurrentBalance)).build());

    // Register a Sales order
    SalesOrder order = new SalesOrder(mainPage).open();
    order.create(salesOrderHeaderData);
    order.assertSaved();
    order.assertData(salesOrderHeaderVerficationData);
    String orderNo = order.getData("documentNo").toString();

    // Create order lines
    SalesOrder.Lines orderLines = order.new Lines(mainPage);
    orderLines.create(salesOrderLinesData);
    orderLines.assertSaved();
    orderLines.assertData(salesOrderLinesVerificationData);

    orderLines = order.new Lines(mainPage);
    orderLines.create(salesOrderLinesData2);
    orderLines.assertSaved();
    orderLines.assertData(salesOrderLinesVerificationData2);

    // Book the order
    order.book();
    order.assertProcessCompletedSuccessfully2();
    order.assertData(bookedSalesOrderHeaderVerificationData);

    // Check pending order payment plan
    SalesOrder.PaymentInPlan orderPaymentPlan = order.new PaymentInPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPendingPaymentInPlanData);

    // Pay the order using the credit and a payment amount of 1000
    order.openAddPayment()
        .process("Orders", "1000.00", orderNo, payment1No,
            "Process Received Payment(s) and Deposit", null, null);

    String salesOrderIdentifier = String.format("%s - %s - %s", orderNo, order.getData("orderDate"),
        "2000.00");

    // Check order payment plan
    orderPaymentPlan = order.new PaymentInPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPaidPaymentInPlanData);

    // Check order payment in details
    SalesOrder.PaymentInPlan.PaymentInDetails orderPaymentInDetails = orderPaymentPlan.new PaymentInDetails(
        mainPage);
    orderPaymentInDetails.assertCount(1);
    orderPaymentInDetails.assertData(orderPaidPaymentInDetailsData);

    String payment2No = orderPaymentInDetails.getData("payment").toString();
    payment2No = payment2No.substring(0, payment2No.indexOf("-") - 1);

    // Check payment in created in add payment
    payment = new PaymentIn(mainPage).open();
    payment.select(new PaymentInHeaderData.Builder().documentNo(payment2No).build());
    payment.assertData(paymentIn2HeaderVerificationData);
    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(1);
    paymentInLines.assertData((PaymentInLinesData) paymentIn2LinesVerificationData
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo));

    // Create a Sales invoice from order
    SalesInvoice invoice = new SalesInvoice(mainPage).open();
    invoice.create(salesInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(salesInvoiceHeaderVerificationData);
    invoice.createLinesFrom(salesOrderIdentifier, 1);
    invoice.assertProcessCompletedSuccessfully();

    SalesInvoice.Lines invoiceLines = invoice.new Lines(mainPage);
    invoiceLines.assertCount(1);
    invoiceLines.assertData(salesInvoiceLineVerificationData);
    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedSalesInvoiceHeaderVerificationData);
    String invoice1No = (String) invoice.getData("documentNo");

    // Check invoice payment plan
    SalesInvoice.PaymentInPlan invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoicePaidPaymentInPlanData);

    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(
        mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoicePaidPaymentInDetailsData);

    // Check Sales order payment plan
    order = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentInPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderInvoicedPaymentInPlanData);

    orderPaymentInDetails = orderPaymentPlan.new PaymentInDetails(mainPage);
    orderPaymentInDetails.assertCount(2);
    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().expected("2000.00").received("510.00").build());
    orderPaymentInDetails.assertData(orderInvoicedPaymentInDetailsData);

    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().received("1490.00").expected("1490.00").build());
    orderPaymentInDetails.assertData(orderInvoicedPaymentInDetailsData2);

    paymentInHeaderVerificationData.addDataField("status", "Awaiting Execution")
        .addDataField("usedCredit", "1,000.00");

    // Check Payment in
    payment = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentInHeaderData.Builder().documentNo(payment2No).build());
    payment.assertData((PaymentInHeaderData) paymentInHeaderVerificationData
        .addDataField("status", "Awaiting Execution")
        .addDataField("usedCredit", "1,000.00"));
    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);

    paymentInLinesInvoicedVerificationData.addDataField("orderPaymentSchedule$order$documentNo",
        orderNo);
    paymentInLinesInvoicedVerificationData2
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo)
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice1No);
    paymentInLines
        .select(new PaymentInLinesData.Builder().received("510.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData);
    paymentInLines.closeForm();

    paymentInLines
        .select(new PaymentInLinesData.Builder().received("1,490.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData2);
    paymentInLines.closeForm();

    // Create a second Sales invoice from order
    invoice = new SalesInvoice(mainPage).open();
    invoice.create(salesInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(salesInvoiceHeaderVerificationData);
    invoice.createLinesFrom(salesOrderIdentifier, 2);
    invoice.assertProcessCompletedSuccessfully();

    invoiceLines = invoice.new Lines(mainPage);
    invoiceLines.assertCount(1);
    invoiceLines.assertData(salesInvoice2LineVerificationData);
    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedSalesInvoice2HeaderVerificationData);
    String invoice2No = (String) invoice.getData("documentNo");

    // Check invoice payment plan
    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice2PaidPaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoice2PaidPaymentInDetailsData);

    // Check Sales order payment plan
    order = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentInPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderInvoicedPaymentInPlanData);

    orderPaymentInDetails = orderPaymentPlan.new PaymentInDetails(mainPage);
    orderPaymentInDetails.assertCount(2);
    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().expected("510.00").received("510.00").build());
    orderPaymentInDetails.assertData(orderInvoiced2PaymentInDetailsData);

    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().received("1490.00").expected("1490.00").build());
    orderPaymentInDetails.assertData(orderInvoicedPaymentInDetailsData2);

    // Check Payment in
    payment = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentInHeaderData.Builder().documentNo(payment2No).build());
    payment.assertData(paymentInHeaderVerificationData);
    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);

    paymentInLinesInvoiced2VerificationData
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo)
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice2No);
    paymentInLines
        .select(new PaymentInLinesData.Builder().received("510.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoiced2VerificationData);
    paymentInLines.closeForm();

    paymentInLines
        .select(new PaymentInLinesData.Builder().received("1,490.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData2);
    paymentInLines.closeForm();

    // Reactivate payment and delete lines and check
    payment.open();
    payment.process("Reactivate and Delete Lines");
    payment.assertProcessCompletedSuccessfully2();
    payment.assertData(paymentReactivatedInHeaderVerificationData);

    // Check Sales order payment plan
    order = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentInPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPendingPaymentInPlanData);

    // Check invoice1 payment plan
    invoice = new SalesInvoice(mainPage).open();
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice1No).build());
    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice1PendingPaymentInPlanData);

    // Check invoice2 payment plan
    invoice = new SalesInvoice(mainPage).open();
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice2No).build());
    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice2PendingPaymentInPlanData);

    // Add Payment in details
    payment = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentInHeaderData.Builder().documentNo(payment2No).build());

    payment.addDetailsOpen()
        .process("Orders", "1000.00", orderNo, payment1No,
            "Process Received Payment(s) and Deposit", null, null);

    // Check Payment in
    payment.assertData(paymentInHeaderVerificationData);
    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);

    paymentInLines
        .select(new PaymentInLinesData.Builder().received("510.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoiced2VerificationData);
    paymentInLines.closeForm();

    paymentInLines
        .select(new PaymentInLinesData.Builder().received("1,490.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData2);

    // Check Sales order payment plan
    order = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentInPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderInvoicedPaymentInPlanData);

    orderPaymentInDetails = orderPaymentPlan.new PaymentInDetails(mainPage);
    orderPaymentInDetails.assertCount(2);
    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().expected("510.00").received("510.00").build());
    orderPaymentInDetails.assertData(orderInvoiced2PaymentInDetailsData);

    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().received("1490.00").expected("1490.00").build());
    orderPaymentInDetails.assertData(orderInvoicedPaymentInDetailsData2);

    // Check invoice1 payment plan
    invoice = new SalesInvoice(mainPage).open();
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice1No).build());
    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoicePaidPaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoicePaidPaymentInDetailsData);

    // Check invoice2 payment plan
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice2No).build());
    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice2PaidPaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoice2PaidPaymentInDetailsData);

    // Execute Payment in and check
    payment = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentInHeaderData.Builder().documentNo(payment2No).build());
    payment.execute();
    payment.assertData(paymentInExecutedHeaderData);

    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);

    paymentInLines
        .select(new PaymentInLinesData.Builder().received("510.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoiced2VerificationData);
    paymentInLines.closeForm();

    paymentInLines
        .select(new PaymentInLinesData.Builder().received("1,490.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData2);
    paymentInLines.closeForm();

    // Post Payment in and check
    payment.open();
    payment.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines1.length);
    post.assertJournalLines2(journalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check Sales order payment plan
    order = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentInPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPaymentReceivedPaymentInPlanData);

    orderInvoicedPaymentInDetailsData2.addDataField("status", "Payment Received");
    orderInvoiced2PaymentInDetailsData.addDataField("status", "Payment Received");

    orderPaymentInDetails = orderPaymentPlan.new PaymentInDetails(mainPage);
    orderPaymentInDetails.assertCount(2);
    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().expected("510.00").received("510.00").build());
    orderPaymentInDetails.assertData(orderInvoiced2PaymentInDetailsData);

    orderPaymentInDetails
        .select(new PaymentDetailsData.Builder().received("1490.00").expected("1490.00").build());
    orderPaymentInDetails.assertData(orderInvoicedPaymentInDetailsData2);

    // Check invoice1 payment plan
    invoice = new SalesInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice1No).build());
    invoice.assertData(new SalesInvoiceHeaderData.Builder().paymentComplete(true).build());

    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice1ExecutedPaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoice1ExecutedPaymentInDetailsData);

    invoice.open();
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines3.length);
    post.assertJournalLines2(journalEntryLines3);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines4.length);
    post.assertJournalLines2(journalEntryLines4);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check invoice2 payment plan
    invoice = new SalesInvoice(mainPage).open();
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice2No).build());
    invoice.assertData(new SalesInvoiceHeaderData.Builder().paymentComplete(true).build());

    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice2ExecutedPaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoice2ExecutedPaymentInDetailsData);

    // Post invoice2 and check post
    invoice.open();
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines5.length);
    post.assertJournalLines2(journalEntryLines5);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines6.length);
    post.assertJournalLines2(journalEntryLines6);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Restore the payment method configuration
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountData);
    paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod
        .select(new PaymentMethodData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build());
    paymentMethod.edit(new PaymentMethodData.Builder().automaticDeposit(false)
        .payinExecutionType("Manual")
        .build());

    logger.info(
        "** End of test case [APRRegression36753In3] Test regression 36753 Paying from order using credit**");
  }
}
