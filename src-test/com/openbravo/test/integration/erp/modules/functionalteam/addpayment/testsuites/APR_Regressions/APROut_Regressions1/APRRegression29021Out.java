/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;

/**
 * Test regression 29021
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression29021Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PaymentOutHeaderData paymentOutHeaderData;
  PaymentOutHeaderData paymentOutHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  PaymentOutHeaderData paymentOutHeaderVerificationData2;

  /**
   * Class constructor.
   *
   */
  public APRRegression29021Out(PaymentOutHeaderData paymentOutHeaderData,
      PaymentOutHeaderData paymentOutHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      PaymentOutHeaderData paymentOutHeaderVerificationData2) {
    this.paymentOutHeaderData = paymentOutHeaderData;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.paymentOutHeaderVerificationData2 = paymentOutHeaderVerificationData2;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new PaymentOutHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("1 (Spain)")
            .build(),
        new PaymentOutHeaderData.Builder().organization("Spain")
            .documentType("AP Payment")
            .account("Spain Cashbook - EUR")
            .amount("0.00")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Cashbook - EUR")
            .c_currency_id("EUR")
            .actual_payment("0.00")
            .expected_payment("0.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("0.00")
            .total("0.00")
            .difference("0.00")
            .build(),

        new PaymentOutHeaderData.Builder().status("Awaiting Payment")
            .amount("0.00")
            .usedCredit("0.00")
            .generatedCredit("0.00")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 29021 - Payment Out flow
   *
   * Payment Method: Automatic Deposit: Yes - Execution Type: Automatic and Execution Process:
   * Simple Execution Process
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression29021OutTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression29021Out] Test regression 29021 - Payment Out flow. **");

    PaymentOut paymentOut = new PaymentOut(mainPage).open();
    paymentOut.create(paymentOutHeaderData);
    paymentOut.assertSaved();
    paymentOut.assertData(paymentOutHeaderVerificationData);

    AddPaymentProcess addPaymentProcess = paymentOut.addDetailsOpen();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.getOrderInvoiceGrid().unselectAll();
    addPaymentProcess.addGLItem(new SelectedPaymentData.Builder().gLItem("Salaries").build());
    addPaymentProcess.getField("reference_no").focus();
    addPaymentProcess.assertData(addPaymentVerificationData);
    addPaymentProcess.processWithError("Process Made Payment(s) and Withdrawal",
        AddPaymentProcess.REGEXP_ERROR_MESSAGE_GLITEM_AMOUNTS_ARE_ZERO);
    addPaymentProcess.close();

    paymentOut.assertData(paymentOutHeaderVerificationData2);
    PaymentOut.Lines paymentOutLines = paymentOut.new Lines(mainPage);
    paymentOutLines.assertCount(0);

    logger.info(
        "** End of test case [APRRegression29021Out] Test regression 29021 - Payment Out flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
