/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 29972
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression29972Out2 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PurchaseOrderHeaderData purchaseOrderHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  PurchaseOrderLinesData purchaseOrderLinesData;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData orderPaymentOutDetailsData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentOutDetailsData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaymentOutPlanData2;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData orderPaymentOutDetailsData2;

  /**
   * Class constructor.
   *
   */
  public APRRegression29972Out2(PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData orderPaymentOutDetailsData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentOutDetailsData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaymentOutPlanData2,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData orderPaymentOutDetailsData2) {
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.orderPaymentOutPlanData = orderPaymentOutPlanData;
    this.orderPaymentOutDetailsData = orderPaymentOutDetailsData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.invoicePaymentOutPlanData = invoicePaymentOutPlanData;
    this.invoicePaymentOutDetailsData = invoicePaymentOutDetailsData;
    this.orderPaymentOutPlanData2 = orderPaymentOutPlanData2;
    this.orderPaymentOutDetailsData2 = orderPaymentOutDetailsData2;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderValues() {
    Object[][] data = new Object[][] { {
        new PurchaseOrderHeaderData.Builder().transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("4 (Spain)")
            .build(),
        new PurchaseOrderHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("Spain warehouse")
            .paymentTerms("90 days")
            .priceList("Purchase")
            .build(),
        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMA").priceListVersion("Purchase").build())
            .orderedQuantity("100")
            .build(),
        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("200.00")
            .build(),
        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("200.00")
            .grandTotalAmount("220.00")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("4 (Spain)")
            .expected("220.00")
            .paid("220.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.CURRENT_DATE)
            .expected("220.00")
            .paid("220.00")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("4 (Spain)")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .priceList("Purchase")
            .paymentTerms("90 days")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("220.00")
            .summedLineAmount("200.00")
            .currency("EUR")
            .paymentComplete(true)
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material A").build())
            .invoicedQuantity("100")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("200.00")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("4 (Spain)")
            .expected("220.00")
            .paid("220.00")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paid("220.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("4 (Spain)")
            .amount("220.00")
            .account("Spain Cashbook - EUR")
            .status("Withdrawn not Cleared")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("4 (Spain)")
            .expected("220.00")
            .paid("220.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .expected("220.00")
            .paid("220.00")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 29972 - Payment Out flow
   *
   * Payment from Order
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression29972Out2Test() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression29972Out2] Test regression 29972 - Payment Out flow - Payment from Order. **");

    PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerficationData);
    PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);
    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);
    Sleep.trySleep();
    purchaseOrder.addPayment("Process Made Payment(s) and Withdrawal");
    Sleep.trySleep();
    purchaseOrder.assertPaymentCreatedSuccessfully();
    DataObject completedPurchaseOrderHeaderData = purchaseOrder.getData();
    String purchaseOrderIdentifier = String.format("%s - %s - %s",
        completedPurchaseOrderHeaderData.getDataField("documentNo"),
        completedPurchaseOrderHeaderData.getDataField("orderDate"),
        bookedPurchaseOrderHeaderVerificationData.getDataField("grandTotalAmount"));

    PurchaseOrder.PaymentOutPlan orderPaymentOutPlan1 = purchaseOrder.new PaymentOutPlan(mainPage);
    // TODO L00: The following static sleep seems that can be removed. Check it with several
    // executions before removing it
    Sleep.trySleep();
    orderPaymentOutPlan1.assertCount(1);
    orderPaymentOutPlan1.assertData(orderPaymentOutPlanData);
    PurchaseOrder.PaymentOutPlan.PaymentOutDetails orderPaymentOutDetails1 = orderPaymentOutPlan1.new PaymentOutDetails(
        mainPage);
    orderPaymentOutDetails1.assertCount(1);
    orderPaymentOutDetails1.assertData(orderPaymentOutDetailsData);

    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    purchaseInvoice.createLinesFrom(purchaseOrderIdentifier);
    purchaseInvoice.assertProcessCompletedSuccessfully();
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);
    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.assertCount(1);
    purchaseInvoiceLines.assertData(purchaseInvoiceLineVerificationData);

    PurchaseInvoice.PaymentOutPlan invoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(
        mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.assertData(invoicePaymentOutPlanData);
    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoicePaymentOutDetailsData);

    purchaseOrder.open();
    Sleep.trySleep(15000);
    purchaseOrder.select(new PurchaseOrderHeaderData.Builder()
        .documentNo((String) completedPurchaseOrderHeaderData.getDataField("documentNo"))
        .build());
    PurchaseOrder.PaymentOutPlan orderPaymentOutPlan2 = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan2.assertCount(1);
    orderPaymentOutPlan2.assertData(orderPaymentOutPlanData2);
    PurchaseOrder.PaymentOutPlan.PaymentOutDetails orderPaymentOutDetails2 = orderPaymentOutPlan2.new PaymentOutDetails(
        mainPage);
    orderPaymentOutDetails2.assertCount(1);
    orderPaymentOutDetails2.assertData(orderPaymentOutDetailsData2);

    logger.info(
        "** End of test case [APRRegression29972Out2] Test regression 29972 - Payment Out flow - Payment from Order. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
