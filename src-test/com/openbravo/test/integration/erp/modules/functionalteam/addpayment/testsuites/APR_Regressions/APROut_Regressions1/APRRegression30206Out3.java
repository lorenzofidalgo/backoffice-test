/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 30206
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression30206Out3 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PaymentOutHeaderData paymentOutHeaderData;
  PaymentOutHeaderData paymentOutHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  PaymentOutHeaderData paymentOutHeaderVerificationData2;
  PaymentOutHeaderData paymentOutHeaderVerificationData3;

  /**
   * Class constructor.
   *
   */
  public APRRegression30206Out3(PaymentOutHeaderData paymentOutHeaderData,
      PaymentOutHeaderData paymentOutHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      PaymentOutHeaderData paymentOutHeaderVerificationData2,
      PaymentOutHeaderData paymentOutHeaderVerificationData3) {
    this.paymentOutHeaderData = paymentOutHeaderData;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.paymentOutHeaderVerificationData2 = paymentOutHeaderVerificationData2;
    this.paymentOutHeaderVerificationData3 = paymentOutHeaderVerificationData3;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new PaymentOutHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Employee A").build())
            .paymentMethod("3.1 (USA)")
            .build(),
        new PaymentOutHeaderData.Builder().documentType("AP Payment")
            .account("USA Bank - USD")
            .currency("USD")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Employee A")
            .fin_paymentmethod_id("3.1 (USA)")
            .fin_financial_account_id("USA Bank - USD")
            .c_currency_id("USD")
            .actual_payment("-20.00")
            .expected_payment("0.00")
            .amount_gl_items("-20.00")
            .amount_inv_ords("0.00")
            .total("-20.00")
            .difference("0.00")
            .build(),

        new PaymentOutHeaderData.Builder().status("Awaiting Execution")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),
        new PaymentOutHeaderData.Builder().amount("-20.00")
            .status("Withdrawn not Cleared")
            .usedCredit("0.00")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 30206 - Payment Out flow
   *
   * Payment Method: Automatic deposit: Yes - Execution Type: Automatic - Execution process: Simple
   * execution process - Deferred: yes
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression30206Out3Test() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression30206Out3] Test regression 30206 - Payment Out flow - Payment Method: Automatic deposit: Yes - Execution Type: Automatic - Execution process: Simple execution process - Deferred: yes. **");

    PaymentOut paymentOut = new PaymentOut(mainPage).open();
    paymentOut.create(paymentOutHeaderData);
    paymentOut.assertSaved();
    paymentOut.assertData(paymentOutHeaderVerificationData);

    AddPaymentProcess addPaymentProcess = paymentOut.addDetailsOpen();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.addGLItem(
        new SelectedPaymentData.Builder().gLItem("Salaries").receivedIn("20.00").build());
    addPaymentProcess.getField("reference_no").focus();
    addPaymentProcess.assertData(addPaymentVerificationData);
    addPaymentProcess.process("Process Made Payment(s) and Withdrawal");

    paymentOut.assertPaymentCreatedSuccessfully();
    paymentOut.assertData(paymentOutHeaderVerificationData2);
    paymentOut.execute();
    Sleep.trySleepWithMessage(7000, "Sleeping to avoid 'Awaiting Execution' false positive");
    paymentOut.assertData(paymentOutHeaderVerificationData3);

    logger.info(
        "** End of test case [APRRegression30206Out3] Test regression 30206 - Payment Out flow - Payment Method: Automatic deposit: Yes - Execution Type: Automatic - Execution process: Simple execution process - Deferred: yes. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
