/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions4;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddTransactionData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.MatchStatementData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.BankStatementLinesData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.PaymentSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddTransactionProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementGrid;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regressions 30898 - 31385 - 31387 - 31389
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression30898Out31385Out31387Out31389Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager
      .getLogger();

  /* Data for this test. */

  AccountData accountHeaderData;
  PaymentMethodData paymentMethodData;
  PaymentMethodData paymentMethodData2;
  PaymentOutHeaderData paymentOutHeaderData;
  PaymentOutHeaderData paymentOutHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  PaymentOutHeaderData paymentOutHeaderVerificationData2;
  BankStatementHeaderData bankStatementHeaderData;
  BankStatementLinesData bankStatementLinesData;
  MatchStatementData matchStatementData;
  AddTransactionData addTransactionVerificationData;
  AddTransactionData addTransactionVerificationData2;
  MatchStatementData matchStatementData2;
  PaymentMethodData paymentMethodData3;

  /**
   * Class constructor.
   *
   */
  public APRRegression30898Out31385Out31387Out31389Out(AccountData accountHeaderData,
      PaymentMethodData paymentMethodData, PaymentMethodData paymentMethodData2,
      PaymentOutHeaderData paymentOutHeaderData,
      PaymentOutHeaderData paymentOutHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      PaymentOutHeaderData paymentOutHeaderVerificationData2,
      BankStatementHeaderData bankStatementHeaderData,
      BankStatementLinesData bankStatementLinesData, MatchStatementData matchStatementData,
      AddTransactionData addTransactionVerificationData,
      AddTransactionData addTransactionVerificationData2, MatchStatementData matchStatementData2,
      PaymentMethodData paymentMethodData3) {
    this.accountHeaderData = accountHeaderData;
    this.paymentMethodData = paymentMethodData;
    this.paymentMethodData2 = paymentMethodData2;
    this.paymentOutHeaderData = paymentOutHeaderData;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.paymentOutHeaderVerificationData2 = paymentOutHeaderVerificationData2;
    this.bankStatementHeaderData = bankStatementHeaderData;
    this.bankStatementLinesData = bankStatementLinesData;
    this.matchStatementData = matchStatementData;
    this.addTransactionVerificationData = addTransactionVerificationData;
    this.addTransactionVerificationData2 = addTransactionVerificationData2;
    this.matchStatementData2 = matchStatementData2;
    this.paymentMethodData3 = paymentMethodData3;
    logInData = new LogInData.Builder().userName("Openbravo").password("openbravo").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { { new AccountData.Builder().name("Cuenta de Banco").build(),
        new PaymentMethodData.Builder().paymentMethod("Transferencia").build(),
        new PaymentMethodData.Builder().automaticWithdrawn(false).build(),

        new PaymentOutHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Bebidas Alegres, S.L.").build())
            .build(),
        new PaymentOutHeaderData.Builder().organization("F&B España - Región Norte")
            .documentType("AP Payment")
            .paymentMethod("Transferencia")
            .account("Cuenta de Banco - EUR")
            .currency("EUR")
            .generatedCredit("0.00")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Bebidas Alegres, S.L.")
            .fin_paymentmethod_id("Transferencia")
            .fin_financial_account_id("Cuenta de Banco - EUR")
            .c_currency_id("EUR")
            .actual_payment("15.00")
            .expected_payment("0.00")
            .amount_gl_items("15.00")
            .amount_inv_ords("0.00")
            .total("15.00")
            .difference("0.00")
            .build(),
        new PaymentOutHeaderData.Builder().status("Payment Made")
            .amount("15.00")
            .usedCredit("0.00")
            .build(),

        new BankStatementHeaderData.Builder().name("APRRegression30898Out31385Out31387Out31389Out")
            .build(),
        new BankStatementLinesData.Builder().referenceNo("30898Out")
            .bpartnername("BebidasAlegres business partner")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Bebidas Alegres, S.L.").build())
            .gLItem("")
            .dramount("15.00")
            .cramount("0.00")
            .description("BebidasAlegres description")
            .build(),

        new MatchStatementData.Builder().bankStatementType("P")
            .sender("Bebidas Alegres, S.L.")
            .bpartnername("BebidasAlegres business partner")
            .referenceNo("30898Out")
            .glitem("")
            .description("BebidasAlegres description")
            .amount("-15")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .transactionDate("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .trxDescription("")
            .build(),
        new AddTransactionData.Builder().trxtype("BP Withdrawal")
            .description("BebidasAlegres business partner\nBebidasAlegres description")
            .currency("EUR")
            .depositamt("0.00")
            .withdrawalamt("15.00")
            .organization("F&B España, S.A")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Bebidas Alegres, S.L.").build())
            .build(),
        new AddTransactionData.Builder().trxtype("BP Withdrawal")
            .description(
                "BebidasAlegres business partner\nBebidasAlegres description\nGL Item: Capital social\n")
            .currency("EUR")
            .depositamt("0.00")
            .withdrawalamt("15.00")
            .organization("F&B España, S.A")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Bebidas Alegres, S.L.").build())
            .build(),
        new MatchStatementData.Builder().bankStatementType("P")
            .sender("Bebidas Alegres, S.L.")
            .bpartnername("BebidasAlegres business partner")
            .referenceNo("30898Out")
            .glitem("")
            .description("BebidasAlegres description")
            .amount("-15")
            .affinity("AD")
            .matchedDocument("T")
            .matchingType("AD")
            .businessPartner("Bebidas Alegres, S.L.")
            .transactionAmount("-15")
            .transactionGLItem("")
            .trxDescription(
                "BebidasAlegres business partner\nBebidasAlegres description\nGL Item: Capital social\n")
            .build(),

        new PaymentMethodData.Builder().automaticWithdrawn(true).build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regressions 30898 - 31385 - 31387 - 31389 - Payment Out flow
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression30898Out31385Out31387Out31389OutTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression30898Out31385Out31387Out31389Out] Test regressions 30898 - 31385 - 31387 - 31389 - Payment Out flow. **");

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);

    FinancialAccount.PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData2);

    PaymentOut paymentOut = new PaymentOut(mainPage).open();
    paymentOut.create(paymentOutHeaderData);
    paymentOut.assertSaved();
    paymentOut.assertData(paymentOutHeaderVerificationData);
    AddPaymentProcess addPaymentProcess = paymentOut.addDetailsOpen();
    String paymentNo = addPaymentProcess.getParameterValue("payment_documentno").toString();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.getOrderInvoiceGrid().unselectAll();
    addPaymentProcess.addGLItem(
        new SelectedPaymentData.Builder().gLItem("Capital social").paidOut("15.00").build());
    addPaymentProcess.getField("reference_no").focus();
    addPaymentProcess.process("Process Made Payment(s)", addPaymentVerificationData);
    paymentOut.assertPaymentCreatedSuccessfully();
    paymentOut.assertData(paymentOutHeaderVerificationData2);

    financialAccount = new FinancialAccount(mainPage).open();
    Sleep.trySleep(15000);
    financialAccount.select(accountHeaderData);

    FinancialAccount.BankStatements bankStatement = financialAccount.new BankStatements(mainPage);
    String date;
    if (bankStatement.getRecordCount() == 0) {
      date = OBDate.CURRENT_DATE;
    } else {
      bankStatement.selectWithoutFiltering(0);
      date = OBDate.addDaysToDate((String) bankStatement.getData("transactionDate"), 1);
    }
    bankStatementHeaderData.addDataField("transactionDate", date);
    bankStatementHeaderData.addDataField("importdate", date);
    bankStatement.create(bankStatementHeaderData);
    bankStatement.assertSaved();

    FinancialAccount.BankStatements.BankStatementLines bankStatementLines = bankStatement.new BankStatementLines(
        mainPage);
    bankStatementLinesData.addDataField("transactionDate", date);
    bankStatementLines.create(bankStatementLinesData);
    bankStatementLines.assertSaved();
    bankStatement.process();
    bankStatement.assertProcessCompletedSuccessfully2();

    @SuppressWarnings("rawtypes")
    MatchStatementProcess matchStatementProcess = financialAccount.openMatchStatement(false);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData.addDataField("banklineDate", date);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData);

    AddTransactionProcess addTransactionProcess = ((MatchStatementGrid) matchStatementProcess
        .getMatchStatementGrid()).clickAdd(0);
    addTransactionVerificationData.addDataField("trxdate", date);
    addTransactionVerificationData.addDataField("dateacct", date);
    addTransactionProcess.assertData(addTransactionVerificationData);
    addTransactionProcess.setParameterValue("fin_payment_id",
        new PaymentSelectorData.Builder().documentNo(paymentNo).build());
    addTransactionVerificationData2.addDataField("trxdate", date);
    addTransactionVerificationData2.addDataField("dateacct", date);
    addTransactionProcess.assertData(addTransactionVerificationData2);
    addTransactionProcess.process();

    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData2.addDataField("banklineDate", date);
    matchStatementData2.addDataField("trxDate", date);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData2);
    matchStatementProcess.process();
    financialAccount.assertProcessCompletedSuccessfully2();

    paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData3);

    logger.info(
        "** End of test case [APRRegression30898Out31385Out31387Out31389Out] Test regressions 30898 - 31385 - 31387 - 31389 - Payment Out flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
