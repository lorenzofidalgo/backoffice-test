/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions5;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 32678
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression32678Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PurchaseOrderHeaderData purchaseOrderHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  PurchaseOrderLinesData purchaseOrderLinesData;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  PurchaseOrderLinesData purchaseOrderLinesData2;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData2;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaymentOutPlanData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePendingPaymentOutPlanData;
  AddPaymentPopUpData addPaymentVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentOutDetailsData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaymentOutPlanData2;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData orderPaymentOutDetailsData2;
  PaymentOutHeaderData paymentOutHeaderVerificationData;
  PaymentOutLinesData paymentOutLinesVerificationData;

  PurchaseInvoiceHeaderData completedPurchaseInvoice2HeaderVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice2PendingPaymentOutPlanData;
  AddPaymentPopUpData addPayment2VerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice2PaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice2PaymentOutDetailsData;
  PaymentOutHeaderData paymentOutHeaderVerificationData2;
  PaymentOutLinesData paymentOutLinesVerificationData2;

  /**
   * Class constructor.
   *
   */
  public APRRegression32678Out(PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      PurchaseOrderLinesData purchaseOrderLinesData2,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData2,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaymentOutPlanData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePendingPaymentOutPlanData,
      AddPaymentPopUpData addPaymentVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentOutDetailsData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaymentOutPlanData2,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData orderPaymentOutDetailsData2,
      PaymentOutHeaderData paymentOutHeaderVerificationData,
      PaymentOutLinesData paymentOutLinesVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoice2HeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice2PendingPaymentOutPlanData,
      AddPaymentPopUpData addPayment2VerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice2PaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice2PaymentOutDetailsData,
      PaymentOutHeaderData paymentOutHeaderVerificationData2,
      PaymentOutLinesData paymentOutLinesVerificationData2) {
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.purchaseOrderLinesData2 = purchaseOrderLinesData2;
    this.purchaseOrderLinesVerificationData2 = purchaseOrderLinesVerificationData2;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.orderPaymentOutPlanData = orderPaymentOutPlanData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.invoicePendingPaymentOutPlanData = invoicePendingPaymentOutPlanData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.invoicePaymentOutPlanData = invoicePaymentOutPlanData;
    this.invoicePaymentOutDetailsData = invoicePaymentOutDetailsData;
    this.orderPaymentOutPlanData2 = orderPaymentOutPlanData2;
    this.orderPaymentOutDetailsData2 = orderPaymentOutDetailsData2;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    this.paymentOutLinesVerificationData = paymentOutLinesVerificationData;

    this.completedPurchaseInvoice2HeaderVerificationData = completedPurchaseInvoice2HeaderVerificationData;
    this.invoice2PendingPaymentOutPlanData = invoice2PendingPaymentOutPlanData;
    this.addPayment2VerificationData = addPayment2VerificationData;
    this.invoice2PaymentOutPlanData = invoice2PaymentOutPlanData;
    this.invoice2PaymentOutDetailsData = invoice2PaymentOutDetailsData;
    this.paymentOutHeaderVerificationData2 = paymentOutHeaderVerificationData2;
    this.paymentOutLinesVerificationData2 = paymentOutLinesVerificationData2;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderValues() {
    Object[][] data = new Object[][] { {
        new PurchaseOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentTerms("30-60")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("30-60")
            .priceList("Purchase")
            .build(),

        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMA").priceListVersion("Purchase").build())
            .orderedQuantity("800")
            .unitPrice("1.00")
            .tax("Exempt")
            .build(),

        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("1.00")
            .listPrice("2.00")
            .lineNetAmount("800.00")
            .build(),

        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMC").priceListVersion("Purchase").build())
            .orderedQuantity("200")
            .unitPrice("1.00")
            .tax("Exempt")
            .build(),

        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("1.00")
            .listPrice("2.00")
            .lineNetAmount("200.00")
            .build(),

        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("1,000.00")
            .grandTotalAmount("1,000.00")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("1,000.00")
            .paid("0.00")
            .outstanding("1,000.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentTerms("30-60")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .priceList("Purchase")
            .paymentTerms("30-60")
            .build(),

        new PurchaseInvoiceLinesData.Builder().invoicedQuantity("800")
            .lineNetAmount("800.00")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("800.00")
            .summedLineAmount("800.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("400.00")
            .paid("0.00")
            .outstanding("400.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("Acc-3 (Payment-Trx-Reconciliation)")
            .c_currency_id("EUR")
            .actual_payment("800.00")
            .expected_payment("800.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("800.00")
            .total("800.00")
            .difference("0.00")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("400.00")
            .paid("400.00")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paid("400.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("400.00")
            .status("Withdrawn not Cleared")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("1,000.00")
            .paid("800.00")
            .outstanding("200.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .expected("400.00")
            .paid("400.00")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("800.00")
            .currency("EUR")
            .status("Withdrawn not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("800.00")
            .expected("400.00")
            .paid("400.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("-320.00")
            .summedLineAmount("-320.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-160.00")
            .paid("0.00")
            .outstanding("-160.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("Acc-3 (Payment-Trx-Reconciliation)")
            .c_currency_id("EUR")
            .actual_payment("-320.00")
            .expected_payment("-320.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("-320.00")
            .total("-320.00")
            .difference("0.00")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-160.00")
            .paid("-160.00")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paid("-160.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("-160.00")
            .status("Withdrawn not Cleared")
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("-320.00")
            .currency("EUR")
            .status("Withdrawn not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("-320.00")
            .expected("-160.00")
            .paid("-160.00")
            .invoiceno("")
            .glitemname("")
            .build(), } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 32678
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression32678Test() throws ParseException {
    logger.info("** Start of test case [APRRegression32678] Test regression 32678. **");

    // Register a purchase order
    PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerficationData);
    String orderNo = purchaseOrder.getData("documentNo").toString();

    // Create order lines
    PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);

    purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData2);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData2);

    String purchaseOrderIdentifier = String.format("%s - %s - %s", orderNo,
        purchaseOrder.getData("orderDate"), "1000.00");

    // Book the order
    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);

    // Check pending order payment plan
    PurchaseOrder.PaymentOutPlan orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(orderPaymentOutPlanData);

    // Create a purchase invoice from order line
    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);

    purchaseInvoice.createLinesFrom(purchaseOrderIdentifier, 1);
    purchaseInvoice.assertProcessCompletedSuccessfully();
    String invoice1No = purchaseInvoice.getData("documentNo").toString();
    PurchaseInvoice.Lines purchaseInvoiceLine = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLine.assertCount(1);
    purchaseInvoiceLine.assertData(purchaseInvoiceLineVerificationData);

    // Create second invoice from same order line
    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);

    purchaseInvoice.createLinesFrom(purchaseOrderIdentifier, 1);
    purchaseInvoice.assertProcessCompletedSuccessfully();
    String invoice2No = purchaseInvoice.getData("documentNo").toString();
    purchaseInvoiceLine = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLine.assertCount(1);
    purchaseInvoiceLine.assertData(purchaseInvoiceLineVerificationData);
    purchaseInvoiceLine
        .edit(new PurchaseInvoiceLinesData.Builder().invoicedQuantity("-320").build());

    // Select and complete the first invoice
    purchaseInvoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice1No).build());
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);

    // Check first invoice payment plan
    PurchaseInvoice.PaymentOutPlan invoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(
        mainPage);
    invoicePaymentOutPlan.assertCount(2);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoicePendingPaymentOutPlanData);

    // Check first invoice second payment plan
    invoicePaymentOutPlan.closeForm();
    invoicePaymentOutPlan.selectWithoutFiltering(1);
    invoicePaymentOutPlan.assertData(invoicePendingPaymentOutPlanData);

    // Pay the invoice1
    purchaseInvoice.addPayment("Process Made Payment(s) and Withdrawal",
        addPaymentVerificationData);
    purchaseInvoice.assertPaymentCreatedSuccessfully();
    purchaseInvoice
        .assertData(new PurchaseInvoiceHeaderData.Builder().paymentComplete(true).build());

    // Check first invoice payment plan and payment details
    invoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.clearFilters();
    invoicePaymentOutPlan.assertCount(2);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoicePaymentOutPlanData);

    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoicePaymentOutDetailsData);

    // Check first invoice second payment plan and payment details
    invoicePaymentOutDetails.closeForm();
    invoicePaymentOutPlan.selectWithoutFiltering(1);
    invoicePaymentOutPlan.assertData(invoicePaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoicePaymentOutDetailsData);

    // Check Purchase order payment out plan
    purchaseOrder = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseOrder.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(orderPaymentOutPlanData2);

    // Check Purchase order payment out details
    PurchaseOrder.PaymentOutPlan.PaymentOutDetails orderPaymentOutDetails = orderPaymentOutPlan.new PaymentOutDetails(
        mainPage);
    orderPaymentOutDetails.assertCount(2);
    orderPaymentOutDetails.selectWithoutFiltering(0);
    orderPaymentOutDetails.assertData(orderPaymentOutDetailsData2);

    orderPaymentOutDetails.selectWithoutFiltering(1);
    orderPaymentOutDetails.assertData(orderPaymentOutDetailsData2);

    String paymentNo = orderPaymentOutDetails.getData("payment").toString();
    paymentNo = paymentNo.substring(0, paymentNo.indexOf("-") - 1);

    // Check Payment out
    PaymentOut paymentOut = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    paymentOut.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());
    paymentOut.assertData(paymentOutHeaderVerificationData);
    PaymentOut.Lines paymentOutLines = paymentOut.new Lines(mainPage);
    paymentOutLines.assertCount(2);
    paymentOutLinesVerificationData.addDataField("orderPaymentSchedule$order$documentNo", orderNo)
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice1No);
    paymentOutLines.selectWithoutFiltering(0);
    paymentOutLines.assertData(paymentOutLinesVerificationData);
    paymentOutLines.closeForm();

    paymentOutLines.selectWithoutFiltering(1);
    paymentOutLines.assertData(paymentOutLinesVerificationData);
    paymentOutLines.closeForm();

    // Select and complete the second invoice
    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseInvoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice2No).build());
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoice2HeaderVerificationData);

    // Check invoice2 payment plan
    invoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(2);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice2PendingPaymentOutPlanData);

    // Check invoice2 second payment plan
    invoicePaymentOutPlan.closeForm();
    invoicePaymentOutPlan.selectWithoutFiltering(1);
    invoicePaymentOutPlan.assertData(invoice2PendingPaymentOutPlanData);

    // Pay the invoice2
    purchaseInvoice.addPayment("Process Made Payment(s) and Withdrawal",
        addPayment2VerificationData);
    purchaseInvoice.assertPaymentCreatedSuccessfully();
    purchaseInvoice
        .assertData(new PurchaseInvoiceHeaderData.Builder().paymentComplete(true).build());

    // Check invoice2 payment plan and payment details
    invoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.clearFilters();
    invoicePaymentOutPlan.assertCount(2);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice2PaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoice2PaymentOutDetailsData);

    // Check invoice2 second payment plan and payment details
    invoicePaymentOutDetails.closeForm();
    invoicePaymentOutPlan.selectWithoutFiltering(1);
    invoicePaymentOutPlan.assertData(invoice2PaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoice2PaymentOutDetailsData);

    String payment2No = invoicePaymentOutDetails.getData("paymentDetails$finPayment").toString();
    payment2No = payment2No.substring(0, payment2No.indexOf("-") - 1);

    // Check first invoice payment plan and payment details
    purchaseInvoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice1No).build());

    invoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.clearFilters();
    invoicePaymentOutPlan.assertCount(2);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoicePaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoicePaymentOutDetailsData);

    // Check first invoice second payment plan and payment details
    invoicePaymentOutDetails.closeForm();
    invoicePaymentOutPlan.selectWithoutFiltering(1);
    invoicePaymentOutPlan.assertData(invoicePaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoicePaymentOutDetailsData);

    // Check Purchase order payment out plan
    purchaseOrder = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseOrder.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(orderPaymentOutPlanData2);

    // Check Purchase order payment out details
    orderPaymentOutDetails = orderPaymentOutPlan.new PaymentOutDetails(mainPage);
    orderPaymentOutDetails.assertCount(2);
    orderPaymentOutDetails.selectWithoutFiltering(0);
    orderPaymentOutDetails.assertData(orderPaymentOutDetailsData2);

    orderPaymentOutDetails.selectWithoutFiltering(1);
    orderPaymentOutDetails.assertData(orderPaymentOutDetailsData2);

    // Check Payment1
    paymentOut = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    paymentOut.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());
    paymentOut.assertData(paymentOutHeaderVerificationData);
    paymentOutLines = paymentOut.new Lines(mainPage);
    paymentOutLines.assertCount(2);
    paymentOutLines.selectWithoutFiltering(0);
    paymentOutLines.assertData(paymentOutLinesVerificationData);
    paymentOutLines.closeForm();

    paymentOutLines.selectWithoutFiltering(1);
    paymentOutLines.assertData(paymentOutLinesVerificationData);
    paymentOutLines.closeForm();

    // Check Payment2
    paymentOut.select(new PaymentOutHeaderData.Builder().documentNo(payment2No).build());
    paymentOut.assertData(paymentOutHeaderVerificationData2);

    paymentOutLinesVerificationData2.addDataField("invoicePaymentSchedule$invoice$documentNo",
        invoice2No);

    paymentOutLines = paymentOut.new Lines(mainPage);
    paymentOutLines.assertCount(2);
    paymentOutLines.selectWithoutFiltering(0);
    paymentOutLines.assertData(paymentOutLinesVerificationData2);
    paymentOutLines.closeForm();

    paymentOutLines.selectWithoutFiltering(1);
    paymentOutLines.assertData(paymentOutLinesVerificationData2);
    paymentOutLines.closeForm();

    logger.info("** End of test case [APRRegression32678] Test regression 32678. **");
  }
}
