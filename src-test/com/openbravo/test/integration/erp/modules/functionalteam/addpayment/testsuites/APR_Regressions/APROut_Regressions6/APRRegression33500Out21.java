/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions6;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.PaymentOutDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.PaymentOutPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentLineData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ReturnToVendorLineSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendor;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendor.PaymentOutPlan;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendorShipment;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 33500
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression33500Out21 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PurchaseOrderHeaderData purchaseOrderHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  PurchaseOrderLinesData purchaseOrderLinesData;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaymentOutPlanData;
  GoodsReceiptHeaderData goodsReceiptHeaderData;
  GoodsReceiptLinesData goodsReceiptLinesVerificationData;
  ReturnToVendorHeaderData rtvHeaderData;
  ReturnToVendorLineSelectorData rtvLineSelectorData;
  ReturnToVendorHeaderData rtvHeaderVerificationData;
  PaymentOutPlanData rtvPendingPaymentData;
  ReturnToVendorShipmentHeaderData rtvShipmentHeaderData;
  ReturnToVendorShipmentHeaderData rtvShipmentHeaderVerificationData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePendingPaymentOutPlanData;
  AddPaymentPopUpData addPaymentVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentOutDetailsData;
  String[][] journalEntryLines;
  String[][] journalEntryLines2;
  PaymentOutHeaderData paymentOutHeaderVerificationData;
  PaymentOutLinesData paymentOutLinesVerificationData;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;
  PaymentOutPlanData rtvPaidPaymentData;
  PaymentOutDetailsData rtvPaidPaymentDetailsData;

  /**
   * Class constructor.
   *
   */
  public APRRegression33500Out21(PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaymentOutPlanData,
      GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptLinesData goodsReceiptLinesVerificationData,
      ReturnToVendorHeaderData rtvHeaderData, ReturnToVendorLineSelectorData rtvLineSelectorData,
      ReturnToVendorHeaderData rtvHeaderVerificationData, PaymentOutPlanData rtvPendingPaymentData,
      ReturnToVendorShipmentHeaderData rtvShipmentHeaderData,
      ReturnToVendorShipmentHeaderData rtvShipmentHeaderVerificationData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePendingPaymentOutPlanData,
      AddPaymentPopUpData addPaymentVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentOutDetailsData,
      String[][] journalEntryLines, String[][] journalEntryLines2,
      PaymentOutHeaderData paymentOutHeaderVerificationData,
      PaymentOutLinesData paymentOutLinesVerificationData, String[][] journalEntryLines3,
      String[][] journalEntryLines4, PaymentOutPlanData rtvPaidPaymentData,
      PaymentOutDetailsData rtvPaidPaymentDetailsData) {
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.orderPaymentOutPlanData = orderPaymentOutPlanData;
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptLinesVerificationData = goodsReceiptLinesVerificationData;
    this.rtvHeaderData = rtvHeaderData;
    this.rtvLineSelectorData = rtvLineSelectorData;
    this.rtvHeaderVerificationData = rtvHeaderVerificationData;
    this.rtvPendingPaymentData = rtvPendingPaymentData;
    this.rtvShipmentHeaderData = rtvShipmentHeaderData;
    this.rtvShipmentHeaderVerificationData = rtvShipmentHeaderVerificationData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.invoicePendingPaymentOutPlanData = invoicePendingPaymentOutPlanData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.invoicePaymentOutPlanData = invoicePaymentOutPlanData;
    this.invoicePaymentOutDetailsData = invoicePaymentOutDetailsData;
    this.journalEntryLines = journalEntryLines;
    this.journalEntryLines2 = journalEntryLines2;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    this.paymentOutLinesVerificationData = paymentOutLinesVerificationData;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    this.rtvPaidPaymentData = rtvPaidPaymentData;
    this.rtvPaidPaymentDetailsData = rtvPaidPaymentDetailsData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderValues() {
    Object[][] data = new Object[][] { {
        new PurchaseOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Purchase")
            .build(),

        new PurchaseOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().searchKey("VB").name("Volley Ball").build())
            .orderedQuantity("100")
            .tax("Exempt 10%")
            .build(),

        new PurchaseOrderLinesData.Builder().uOM("Unit")
            .unitPrice("28.50")
            .listPrice("28.50")
            .lineNetAmount("2,850.00")
            .build(),

        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("2,850.00")
            .grandTotalAmount("2,850.00")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("2,850.00")
            .paid("0.00")
            .outstanding("2,850.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new GoodsReceiptHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),

        new GoodsReceiptLinesData.Builder().movementQuantity("100").build(),

        new ReturnToVendorHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new ReturnToVendorLineSelectorData.Builder().returned("10").build(),

        new ReturnToVendorHeaderData.Builder().documentStatus("Booked")
            .grandTotalAmount("285.00")
            .paymentTerms("90 days")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new PaymentOutPlanData.Builder().expected("-285.00")
            .outstanding("-285.00")
            .paid("0.00")
            .numberOfPayments("0")
            .build(),

        new ReturnToVendorShipmentHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),

        new ReturnToVendorShipmentHeaderData.Builder().documentStatus("Completed").build(),

        new PurchaseInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .priceList("Purchase")
            .paymentTerms("90 days")
            .build(),

        new PurchaseInvoiceLinesData.Builder().invoicedQuantity("-10")
            .lineNetAmount("-285.00")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("-285.00")
            .summedLineAmount("-285.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-285.00")
            .paid("0.00")
            .outstanding("-285.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("Acc-3 (Payment-Trx-Reconciliation)")
            .c_currency_id("EUR")
            .actual_payment("-285.00")
            .expected_payment("-285.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("-285.00")
            .total("-285.00")
            .difference("0.00")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-285.00")
            .paid("-285.00")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paid("-285.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("-285.00")
            .status("Withdrawn not Cleared")
            .build(),

        new String[][] { { "40000", "Proveedores (euros)", "", "-285.00" },
            { "60000", "Compras de mercaderías", "-285.00", "" } },

        new String[][] { { "21100", "Accounts payable", "", "-712.50" },
            { "51200", "Service costs", "-712.50", "" } },

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("-285.00")
            .currency("EUR")
            .status("Withdrawn not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentOutLinesData.Builder().dueDate(OBDate.CURRENT_DATE)
            .invoiceAmount("-285.00")
            .expected("-285.00")
            .paid("-285.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new String[][] { { "40100", "Proveedores efectos comerciales a pagar", "", "-285.00" },
            { "40000", "Proveedores (euros)", "-285.00", "" } },

        new String[][] { { "11300", "Bank in transit", "", "-712.50" },
            { "21100", "Accounts payable", "-712.50", "" } },

        new PaymentOutPlanData.Builder().expected("-285.00")
            .outstanding("0.00")
            .paid("-285.00")
            .numberOfPayments("1")
            .build(),

        new PaymentOutDetailsData.Builder().paidAmount("-285.00").build(),

        } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 33500 - Related to issue 35726
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression33500Out21Test() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression33500Out21] Test regression 33500 - Related to issue 35726. **");

    // Register a purchase order
    PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerficationData);
    String orderNo = purchaseOrder.getData("documentNo").toString();

    // Create order lines
    PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);
    String purchaseOrderIdentifier = String.format("%s - %s - %s", orderNo,
        purchaseOrder.getData("orderDate"), "2850.00");

    // Book the order
    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);

    // Check pending order payment plan
    PurchaseOrder.PaymentOutPlan orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(orderPaymentOutPlanData);

    // Create a goods receipts from order
    GoodsReceipt goodsReceipt = new GoodsReceipt(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt.create(goodsReceiptHeaderData);

    goodsReceipt.createLinesFrom("USA111", purchaseOrderIdentifier);
    goodsReceipt.assertProcessCompletedSuccessfully();

    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.assertCount(1);
    goodsReceiptLines.assertData(goodsReceiptLinesVerificationData);

    // Complete the receipt
    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();

    String receiptNo = (String) goodsReceipt.getData().getDataField("documentNo");
    logger.info("** Goods receipt Document No. {} **", receiptNo);

    // Return to vendor the good receipt
    ReturnToVendor returnToVendor = (ReturnToVendor) new ReturnToVendor(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnToVendor.create(rtvHeaderData);
    returnToVendor.assertSaved();
    String returnToVendorNo = (String) returnToVendor.getData().getDataField("documentNo");
    logger.info("** Return to vendor Document No. {} **", returnToVendorNo);
    PickAndExecuteWindow<ReturnToVendorLineSelectorData> rtvPickEdit = returnToVendor
        .openPickAndEditLines();
    rtvPickEdit
        .filter(new ReturnToVendorLineSelectorData.Builder().shipmentNumber(receiptNo).build());
    rtvPickEdit.edit(rtvLineSelectorData);
    rtvPickEdit.process();

    // Book the RTV
    returnToVendor.book();
    returnToVendor.assertProcessCompletedSuccessfully2();
    returnToVendor.assertData(rtvHeaderVerificationData);

    // Check the RTV Payment Out Plan
    PaymentOutPlan rtvPaymentPlan = returnToVendor.new PaymentOutPlan(mainPage);
    rtvPaymentPlan.assertData(rtvPendingPaymentData);

    // Create the Return To Vendor Shipment from RTV
    ReturnToVendorShipment rtvShipment = (ReturnToVendorShipment) new ReturnToVendorShipment(
        mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    rtvShipment.create(rtvShipmentHeaderData);
    PickAndExecuteWindow<ReturnToVendorShipmentLineData> rtvShipmentPickEdit = rtvShipment
        .openPickAndEditLines();
    rtvShipmentPickEdit
        .filter(new ReturnToVendorShipmentLineData.Builder().rMOrderNo(returnToVendorNo).build());
    rtvShipmentPickEdit.process();

    // Complete the shipment
    rtvShipment.complete();
    rtvShipment.assertProcessCompletedSuccessfully2();
    rtvShipment.assertData(rtvShipmentHeaderVerificationData);

    String rtvShipmentId = String.format("%s - %s - %s",
        (String) rtvShipment.getData().getDataField("documentNo"),
        (String) rtvShipment.getData().getDataField("movementDate"),
        (String) rtvShipment.getData().getDataField("businessPartner"));
    logger.info("** Return to vendor Shipment Document No. {} **", rtvShipmentId);

    // Create a purchase invoice from order
    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    purchaseInvoice.createLinesFromShipment(rtvShipmentId);
    purchaseInvoice.assertProcessCompletedSuccessfully();
    String invoiceNo = purchaseInvoice.getData("documentNo").toString();
    PurchaseInvoice.Lines purchaseInvoiceLine = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLine.assertCount(1);
    purchaseInvoiceLine.assertData(purchaseInvoiceLineVerificationData);

    // Complete the invoice
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);

    // Check invoice pending payment plan
    PurchaseInvoice.PaymentOutPlan invoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(
        mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.assertData(invoicePendingPaymentOutPlanData);

    // Pay the invoice
    purchaseInvoice.addPayment("Process Made Payment(s) and Withdrawal",
        addPaymentVerificationData);
    purchaseInvoice.assertPaymentCreatedSuccessfully();
    purchaseInvoice
        .assertData(new PurchaseInvoiceHeaderData.Builder().paymentComplete(true).build());

    // Check invoice payment out plan
    invoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.assertData(invoicePaymentOutPlanData);

    // Check invoice payment out details
    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    mainPage.waitForDataToLoad();
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoicePaymentOutDetailsData);

    String paymentIdentifier = invoicePaymentOutDetails.getData("paymentDetails$finPayment")
        .toString();
    String paymentNo = paymentIdentifier.substring(0, paymentIdentifier.indexOf("-") - 1);

    // Post invoice and check
    purchaseInvoice.open();
    purchaseInvoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines.length);
    post.assertJournalLines2(journalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check Purchase order payment plan
    purchaseOrder = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseOrder.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(orderPaymentOutPlanData);

    // Check Return to Vendor Payment out Plan
    returnToVendor = (ReturnToVendor) new ReturnToVendor(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnToVendor
        .select(new ReturnToVendorHeaderData.Builder().documentNo(returnToVendorNo).build());
    rtvPaymentPlan = returnToVendor.new PaymentOutPlan(mainPage);
    rtvPaymentPlan.assertData(rtvPaidPaymentData);

    ReturnToVendor.PaymentOutPlan.PaymentOutDetails rtvPaymentDetails = rtvPaymentPlan.new PaymentOutDetails(
        mainPage);
    rtvPaymentDetails.assertData((PaymentOutDetailsData) rtvPaidPaymentDetailsData
        .addDataField("payment", paymentIdentifier));

    // Check Payment out
    PaymentOut paymentOut = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    paymentOut.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());
    paymentOut.assertData(paymentOutHeaderVerificationData);
    PaymentOut.Lines paymentOutLines = paymentOut.new Lines(mainPage);
    paymentOutLines.assertCount(1);
    paymentOutLines.assertData((PaymentOutLinesData) paymentOutLinesVerificationData
        .addDataField("orderPaymentSchedule$order$documentNo", returnToVendorNo)
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo)
        .addDataField("dueDate", OBDate.ADD_DAYS_TO_SYSTEM_DATE_90));

    // Post payment out and check
    paymentOut.open();
    paymentOut.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines3.length);
    post.assertJournalLines2(journalEntryLines3);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines4.length);
    post.assertJournalLines2(journalEntryLines4);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    logger.info(
        "** End of test case [APRRegression33500Out21] Test regression 33500 - Related to issue 35726. **");
  }
}
