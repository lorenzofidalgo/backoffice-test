/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions8;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.DiscountsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder.BasicDiscounts;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 35726
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression35726Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PurchaseOrderHeaderData purchaseOrderHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  PurchaseOrderLinesData purchaseOrderLinesData;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  DiscountsData purchaseOrderDiscountData;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaymentOutPlanData;
  GoodsReceiptHeaderData goodsReceiptHeaderData;
  GoodsReceiptLinesData goodsReceiptLinesVerificationData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineEditedVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePendingPaymentOutPlanData;
  AddPaymentPopUpData addPaymentVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentOutDetailsData1;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentOutDetailsData2;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentOutDetailsData3;
  String[][] journalEntryLines;
  String[][] journalEntryLines2;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaidPaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData orderPaidPaymentOutDetailsData;
  PaymentOutHeaderData paymentOutHeaderVerificationData;
  PaymentOutLinesData paymentOutLinesVerificationData;
  PaymentOutLinesData paymentOutLinesVerificationData2;
  PaymentOutLinesData paymentOutLinesVerificationData3;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;

  /**
   * Class constructor.
   *
   */
  public APRRegression35726Out(PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      DiscountsData purchaseOrderDiscountData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaymentOutPlanData,
      GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptLinesData goodsReceiptLinesVerificationData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineEditedVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePendingPaymentOutPlanData,
      AddPaymentPopUpData addPaymentVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentOutDetailsData1,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentOutDetailsData2,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentOutDetailsData3,
      String[][] journalEntryLines, String[][] journalEntryLines2,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaidPaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData orderPaidPaymentOutDetailsData,
      PaymentOutHeaderData paymentOutHeaderVerificationData,
      PaymentOutLinesData paymentOutLinesVerificationData,
      PaymentOutLinesData paymentOutLinesVerificationData2,
      PaymentOutLinesData paymentOutLinesVerificationData3, String[][] journalEntryLines3,
      String[][] journalEntryLines4) {
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.purchaseOrderDiscountData = purchaseOrderDiscountData;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.orderPaymentOutPlanData = orderPaymentOutPlanData;
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptLinesVerificationData = goodsReceiptLinesVerificationData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.purchaseInvoiceLineEditedVerificationData = purchaseInvoiceLineEditedVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.invoicePendingPaymentOutPlanData = invoicePendingPaymentOutPlanData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.invoicePaymentOutPlanData = invoicePaymentOutPlanData;
    this.invoicePaymentOutDetailsData1 = invoicePaymentOutDetailsData1;
    this.invoicePaymentOutDetailsData2 = invoicePaymentOutDetailsData2;
    this.invoicePaymentOutDetailsData3 = invoicePaymentOutDetailsData3;
    this.journalEntryLines = journalEntryLines;
    this.journalEntryLines2 = journalEntryLines2;
    this.orderPaidPaymentOutPlanData = orderPaidPaymentOutPlanData;
    this.orderPaidPaymentOutDetailsData = orderPaidPaymentOutDetailsData;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    this.paymentOutLinesVerificationData = paymentOutLinesVerificationData;
    this.paymentOutLinesVerificationData2 = paymentOutLinesVerificationData2;
    this.paymentOutLinesVerificationData3 = paymentOutLinesVerificationData3;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderValues() {
    Object[][] data = new Object[][] { {
        new PurchaseOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("30-60")
            .build(),

        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("30-60")
            .priceList("Purchase")
            .build(),

        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMC").priceListVersion("Purchase").build())
            .orderedQuantity("1")
            .unitPrice("1400")
            .tax("VAT 10% USA")
            .build(),

        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("1,400.00")
            .lineNetAmount("1,400.00")
            .build(),

        new DiscountsData.Builder().discount("Discount 10%").build(),

        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("1,260.00")
            .grandTotalAmount("1,386.00")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("1,386.00")
            .paid("0.00")
            .outstanding("1,386.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new GoodsReceiptHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),

        new GoodsReceiptLinesData.Builder().movementQuantity("1").build(),

        new PurchaseInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentTerms("30-60")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .priceList("Purchase")
            .paymentTerms("30-60")
            .build(),

        new PurchaseInvoiceLinesData.Builder().invoicedQuantity("1")
            .lineNetAmount("1,400.00")
            .build(),

        new PurchaseInvoiceLinesData.Builder().invoicedQuantity("1")
            .lineNetAmount("25,830.00")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("28,413.00")
            .summedLineAmount("25,830.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("14,206.50")
            .paid("0.00")
            .outstanding("14,206.50")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("Acc-3 (Payment-Trx-Reconciliation)")
            .c_currency_id("EUR")
            .actual_payment("28,413.00")
            .expected_payment("28,413.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("28,413.00")
            .total("28,413.00")
            .difference("0.00")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("14,206.50")
            .paid("14,206.50")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paid("1,386.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("14,206.50")
            .status("Withdrawn not Cleared")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paid("12,820.50")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("14,206.50")
            .status("Withdrawn not Cleared")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paid("14,206.50")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("14,206.50")
            .status("Withdrawn not Cleared")
            .build(),

        new String[][] { { "60000", "Compras de mercaderías", "25,830.00", "" },
            { "47200", "Hacienda Pública IVA soportado", "2,583.00", "" },
            { "40000", "Proveedores (euros)", "", "28,413.00" } },

        new String[][] { { "51200", "Service costs", "64,575.00", "" },
            { "11600", "Tax Receivables", "6,457.50", "" },
            { "21100", "Accounts payable", "", "71,032.50" }, },

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("1,386.00")
            .paid("1,386.00")
            .outstanding("0.00")
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData.Builder()
            .expected("14,206.50")
            .paid("1,386.00")
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("28,413.00")
            .currency("EUR")
            .status("Withdrawn not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("28,413.00")
            .expected("14,206.50")
            .paid("1,386.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("28,413.00")
            .expected("14,206.50")
            .paid("12,820.50")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("28,413.00")
            .expected("14,206.50")
            .paid("14,206.50")
            .invoiceno("")
            .glitemname("")
            .build(),

        new String[][] { { "40000", "Proveedores (euros)", "28,413.00", "" },
            { "40100", "Proveedores efectos comerciales a pagar", "", "28,413.00" }, },

        new String[][] { { "21100", "Accounts payable", "71,032.50", "" },
            { "11300", "Bank in transit", "", "71,032.50" } },

        } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 35726
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression35726Test() throws ParseException {
    logger.info("** Start of test case [APRRegression35726] Test regression 35726**");

    // Register a purchase order
    PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerficationData);
    String orderNo = purchaseOrder.getData("documentNo").toString();

    // Create order lines
    PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);

    // Create the basic discount
    BasicDiscounts purchaseOrderDiscount = purchaseOrder.new BasicDiscounts(mainPage);
    purchaseOrderDiscount.create(purchaseOrderDiscountData);
    purchaseOrderDiscount.assertSaved();

    String purchaseOrderIdentifier = String.format("%s - %s - %s", orderNo,
        purchaseOrder.getData("orderDate"), "1386.00");

    // Book the order
    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);

    // Check exists two lines
    purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.assertCount(2);

    // Check pending order payment plan
    PurchaseOrder.PaymentOutPlan orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(orderPaymentOutPlanData);

    // Create a goods receipts from order
    GoodsReceipt goodsReceipt = new GoodsReceipt(mainPage).open();
    goodsReceipt.create(goodsReceiptHeaderData);

    goodsReceipt.createLinesFrom("USA111", purchaseOrderIdentifier);
    goodsReceipt.assertProcessCompletedSuccessfully();

    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.assertCount(2);
    goodsReceiptLines.selectWithoutFiltering(0);
    goodsReceiptLines.assertData(goodsReceiptLinesVerificationData);
    goodsReceiptLines.edit(new GoodsReceiptLinesData.Builder().attributeSetValue(orderNo).build());

    // Complete the receipt
    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();

    String receiptNo = (String) goodsReceipt.getData().getDataField("documentNo");
    logger.info("** Goods receipt Document No. {} **", receiptNo);

    // Create a purchase invoice from order
    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    purchaseInvoice.createLinesFrom(purchaseOrderIdentifier, 1);
    purchaseInvoice.assertProcessCompletedSuccessfully();
    String invoiceNo = purchaseInvoice.getData("documentNo").toString();

    PurchaseInvoice.Lines purchaseInvoiceLine = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLine.assertCount(1);
    purchaseInvoiceLine.assertData(purchaseInvoiceLineVerificationData);
    purchaseInvoiceLine.edit(new PurchaseInvoiceLinesData.Builder().unitPrice("25830").build());
    purchaseInvoiceLine.assertData(purchaseInvoiceLineEditedVerificationData);

    // Complete the invoice
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);

    // Check first invoice payment plan
    PurchaseInvoice.PaymentOutPlan invoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(
        mainPage);
    invoicePaymentOutPlan.assertCount(2);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoicePendingPaymentOutPlanData);

    // Check second invoice payment plan
    invoicePaymentOutPlan.closeForm();
    invoicePaymentOutPlan.selectWithoutFiltering(1);
    invoicePaymentOutPlan.assertData(invoicePendingPaymentOutPlanData);

    // Pay the invoice
    purchaseInvoice.addPayment("Process Made Payment(s) and Withdrawal",
        addPaymentVerificationData);
    purchaseInvoice.assertPaymentCreatedSuccessfully();
    purchaseInvoice
        .assertData(new PurchaseInvoiceHeaderData.Builder().paymentComplete(true).build());

    // Check first invoice payment plan
    invoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.clearFilters();
    invoicePaymentOutPlan.assertCount(2);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoicePaymentOutPlanData);
    invoicePaymentOutPlan.unselectAll();

    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    int firstPaymentDetailsCount = invoicePaymentOutDetails.getRecordCount();

    // Check second invoice payment plan
    invoicePaymentOutPlan.selectWithoutFiltering(1);
    invoicePaymentOutPlan.assertData(invoicePaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    int secondPaymentDetailsCount = invoicePaymentOutDetails.getRecordCount();

    assertEquals("Payment out details total should be 3", 3,
        firstPaymentDetailsCount + secondPaymentDetailsCount);

    // Check the Payment Out Details with two records
    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutPlan.closeForm();
    invoicePaymentOutPlan.unselectAll();
    invoicePaymentOutPlan.selectWithoutFiltering(firstPaymentDetailsCount == 2 ? 0 : 1);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(2);
    invoicePaymentOutDetails
        .select(new PaymentDetailsData.Builder().paid("1,386.00").amount("14,206.50").build());
    invoicePaymentOutDetails.assertData(invoicePaymentOutDetailsData1);
    invoicePaymentOutDetails.closeForm();

    invoicePaymentOutDetails
        .select(new PaymentDetailsData.Builder().paid("12,820.50").amount("14,206.50").build());
    invoicePaymentOutDetails.assertData(invoicePaymentOutDetailsData2);

    // Check the Payment Out Details with one record
    invoicePaymentOutPlan.unselectAll();
    invoicePaymentOutPlan.selectWithoutFiltering(firstPaymentDetailsCount == 1 ? 0 : 1);
    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.clearFilters();
    Sleep.trySleep();
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.selectWithoutFiltering(0);
    invoicePaymentOutDetails.assertData(invoicePaymentOutDetailsData3);

    String paymentIdentifier = invoicePaymentOutDetails.getData("paymentDetails$finPayment")
        .toString();
    String paymentNo = paymentIdentifier.substring(0, paymentIdentifier.indexOf("-") - 1);

    // Post invoice and check
    purchaseInvoice.open();
    purchaseInvoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines.length);
    post.assertJournalLines2(journalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check Purchase order payment plan
    purchaseOrder = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseOrder.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(orderPaidPaymentOutPlanData);

    PurchaseOrder.PaymentOutPlan.PaymentOutDetails orderPaymentDetails = orderPaymentOutPlan.new PaymentOutDetails(
        mainPage);
    orderPaymentDetails.assertCount(1);
    orderPaymentDetails.assertData(orderPaidPaymentOutDetailsData);

    // Check Payment out
    PaymentOut paymentOut = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    paymentOut.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());
    paymentOut.assertData(paymentOutHeaderVerificationData);
    PaymentOut.Lines paymentOutLines = paymentOut.new Lines(mainPage);
    paymentOutLines.assertCount(3);
    paymentOutLines
        .select(new PaymentOutLinesData.Builder().orderno(orderNo).invoiceno(invoiceNo).build());

    paymentOutLines.assertData((PaymentOutLinesData) paymentOutLinesVerificationData
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo)
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo));

    paymentOutLines.closeForm();
    paymentOutLines
        .select(new PaymentOutLinesData.Builder().paid("12,820.50").invoiceno(invoiceNo).build());
    paymentOutLines.assertData((PaymentOutLinesData) paymentOutLinesVerificationData2
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo));

    paymentOutLines.closeForm();
    paymentOutLines
        .select(new PaymentOutLinesData.Builder().paid("14,206.50").invoiceno(invoiceNo).build());
    paymentOutLines.assertData((PaymentOutLinesData) paymentOutLinesVerificationData3
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo));

    // Post payment out and check
    paymentOut.open();
    paymentOut.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines3.length);
    post.assertJournalLines2(journalEntryLines3);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines4.length);
    post.assertJournalLines2(journalEntryLines4);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    logger.info("** End of test case [APRRegression35726] Test regression 35726**");
  }
}
