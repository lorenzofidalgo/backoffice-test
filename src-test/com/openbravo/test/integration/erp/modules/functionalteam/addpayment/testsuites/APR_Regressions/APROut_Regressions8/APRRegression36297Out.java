/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions8;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.TaxData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test to create a purchase order with multiple lines, add payment to purchase order, create
 * purchase invoice from order for each line separately, void the purchase invoice.
 *
 * @author
 */

@RunWith(Parameterized.class)
public class APRRegression36297Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /** The purchase order header data. */
  PurchaseOrderHeaderData purchaseOrderHeaderData;
  /** The data to verify the creation of the purchase order header. */
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  /** The purchase order line 1 data. */
  PurchaseOrderLinesData purchaseOrderLine1Data;
  /** The data to verify the creation of the purchase order line 1. */
  PurchaseOrderLinesData purchaseOrderLine1VerificationData;
  /** The purchase order line 2 data. */
  PurchaseOrderLinesData purchaseOrderLine2Data;
  /** The data to verify the creation of the purchase order line 2. */
  PurchaseOrderLinesData purchaseOrderLine2VerificationData;
  /** The purchase order line data 3. */
  PurchaseOrderLinesData purchaseOrderLine3Data;
  /** The data to verify the creation of the purchase order line 3. */
  PurchaseOrderLinesData purchaseOrderLine3VerificationData;
  /** The booked purchase order header verification data. */
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;
  /** The purchase order payment plan data after order booking */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData poPaymentPlanDataAfterBookedOrder;
  /** Add Payment Data */
  AddPaymentPopUpData addPaymentVerificationData;
  /** The taxes data. */
  TaxData[] taxesData;
  /** The purchase order payment plan after add payment */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData poPaymentPlanDataAfterOrderPaid;
  /** The purchase order payment details after add payment */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetailsDataAfterOrderPaid;
  /** The purchase invoice header data */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  /** The data to verify creation of the purchase invoice header */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  /** The completed purchase invoice 1 header verification data */
  PurchaseInvoiceHeaderData completedPurchaseInvoice1HeaderVerificationData;
  /** The payment plan for first purchase invoice */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice1PaymentPlan;
  /** The payment details for first purchase invoice */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice1PaymentDetails;
  /** The purchase order payment plan data after first purchase invoice */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData poPaymentPlanDataAfterFirstInvoice;
  /** The purchase order payment details 1 data filter after first purchase invoice */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails1DataChk;
  /** The purchase order payment details 1 verification data after first purchase invoice */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails1DataAfterFirstInvoice;
  /** The purchase order payment details 2 data filter after first purchase invoice */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails2DataChk;
  /** The purchase order payment details 2 verification data after first purchase invoice */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails2Data;
  /** The completed purchase invoice 2 header verification data */
  PurchaseInvoiceHeaderData completedPurchaseInvoice2HeaderVerificationData;
  /** The payment plan for second purchase invoice */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice2PaymentPlan;
  /** The payment details for second purchase invoice */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice2PaymentDetails;
  /** The purchase order payment plan data */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData poPaymentPlanDataAfterSecondInvoice;
  /** The purchase order payment details 1 verification data */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails1DataAfterSecondInvoice;
  /** The purchase order payment details 3 data filter */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails3DataChk;
  /** The purchase order payment details 3 verification data */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails3Data;
  /** The payment details for second purchase invoice after void */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice2PaymentDetailsAfterVoid;
  /** The payment plan of reverse invoice */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData reverseInvoicePaymentPlan;
  /** The payment plan details of reverse invoice */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData reverseInvoicePaymentDetails;
  /** The completed purchase invoice 3 header verification data */
  PurchaseInvoiceHeaderData completedPurchaseInvoice3HeaderVerificationData;
  /** The payment plan for third purchase invoice */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice3PaymentPlan;
  /** The payment details for third purchase invoice */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice3PaymentDetails;
  /** The purchase order payment details 4 Filter data */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails4DataChk;
  /** The purchase order payment details 4 verification data */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails4Data;

  /**
   * Class constructor.
   *
   */

  public APRRegression36297Out(PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLine1Data,
      PurchaseOrderLinesData purchaseOrderLine1VerificationData,
      PurchaseOrderLinesData purchaseOrderLine2Data,
      PurchaseOrderLinesData purchaseOrderLine2VerificationData,
      PurchaseOrderLinesData purchaseOrderLine3Data,
      PurchaseOrderLinesData purchaseOrderLine3VerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData poPaymentPlanDataAfterBookedOrder,
      AddPaymentPopUpData addPaymentVerificationData, TaxData[] taxesData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData poPaymentPlanDataAfterOrderPaid,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetailsDataAfterOrderPaid,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoice1HeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice1PaymentPlan,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice1PaymentDetails,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData poPaymentPlanDataAfterFirstInvoice,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails1DataChk,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails1DataAfterFirstInvoice,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails2DataChk,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails2Data,
      PurchaseInvoiceHeaderData completedPurchaseInvoice2HeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice2PaymentPlan,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice2PaymentDetails,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData poPaymentPlanDataAfterSecondInvoice,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails1DataAfterSecondInvoice,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails3DataChk,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails3Data,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice2PaymentDetailsAfterVoid,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData reverseInvoicePaymentPlan,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData reverseInvoicePaymentDetails,
      PurchaseInvoiceHeaderData completedPurchaseInvoice3HeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice3PaymentPlan,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice3PaymentDetails,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails4DataChk,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData poPaymentDetails4Data) {

    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLine1Data = purchaseOrderLine1Data;
    this.purchaseOrderLine1VerificationData = purchaseOrderLine1VerificationData;
    this.purchaseOrderLine2Data = purchaseOrderLine2Data;
    this.purchaseOrderLine2VerificationData = purchaseOrderLine2VerificationData;
    this.purchaseOrderLine3Data = purchaseOrderLine3Data;
    this.purchaseOrderLine3VerificationData = purchaseOrderLine3VerificationData;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;

    this.poPaymentPlanDataAfterBookedOrder = poPaymentPlanDataAfterBookedOrder;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.taxesData = taxesData;
    this.poPaymentPlanDataAfterOrderPaid = poPaymentPlanDataAfterOrderPaid;
    this.poPaymentDetailsDataAfterOrderPaid = poPaymentDetailsDataAfterOrderPaid;

    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.completedPurchaseInvoice1HeaderVerificationData = completedPurchaseInvoice1HeaderVerificationData;
    this.invoice1PaymentPlan = invoice1PaymentPlan;
    this.invoice1PaymentDetails = invoice1PaymentDetails;
    this.poPaymentPlanDataAfterFirstInvoice = poPaymentPlanDataAfterFirstInvoice;
    this.poPaymentDetails1DataChk = poPaymentDetails1DataChk;
    this.poPaymentDetails1DataAfterFirstInvoice = poPaymentDetails1DataAfterFirstInvoice;
    this.poPaymentDetails2DataChk = poPaymentDetails2DataChk;
    this.poPaymentDetails2Data = poPaymentDetails2Data;

    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.completedPurchaseInvoice1HeaderVerificationData = completedPurchaseInvoice1HeaderVerificationData;
    this.invoice1PaymentPlan = invoice1PaymentPlan;
    this.invoice1PaymentDetails = invoice1PaymentDetails;

    this.completedPurchaseInvoice2HeaderVerificationData = completedPurchaseInvoice2HeaderVerificationData;
    this.invoice2PaymentPlan = invoice2PaymentPlan;
    this.invoice2PaymentDetails = invoice2PaymentDetails;
    this.poPaymentPlanDataAfterSecondInvoice = poPaymentPlanDataAfterSecondInvoice;
    this.poPaymentDetails1DataAfterSecondInvoice = poPaymentDetails1DataAfterSecondInvoice;
    this.poPaymentDetails3DataChk = poPaymentDetails3DataChk;
    this.poPaymentDetails3Data = poPaymentDetails3Data;

    this.invoice2PaymentDetailsAfterVoid = invoice2PaymentDetailsAfterVoid;
    this.reverseInvoicePaymentPlan = reverseInvoicePaymentPlan;
    this.reverseInvoicePaymentDetails = reverseInvoicePaymentDetails;

    this.completedPurchaseInvoice3HeaderVerificationData = completedPurchaseInvoice3HeaderVerificationData;
    this.invoice3PaymentPlan = invoice3PaymentPlan;
    this.invoice3PaymentDetails = invoice3PaymentDetails;
    this.poPaymentDetails4DataChk = poPaymentDetails4DataChk;
    this.poPaymentDetails4Data = poPaymentDetails4Data;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderValues() {
    Object[][] data = new Object[][] { {
        /* Parameters for this test. */

        new PurchaseOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .warehouse("USA warehouse")
            .build(),

        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("USA warehouse")
            .priceList("Purchase")
            .paymentTerms("90 days")
            .build(),

        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMA").priceListVersion("Purchase").build())
            .orderedQuantity("10")
            .build(),

        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("Exempt 10%")
            .lineNetAmount("20.00")
            .build(),

        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMA").priceListVersion("Purchase").build())
            .orderedQuantity("20")
            .build(),

        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("Exempt 10%")
            .lineNetAmount("40.00")
            .build(),

        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMA").priceListVersion("Purchase").build())
            .orderedQuantity("30")
            .build(),

        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("Exempt 10%")
            .lineNetAmount("60.00")
            .build(),

        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("120.00")
            .grandTotalAmount("120.00")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("120.00")
            .paid("0.00")
            .outstanding("120.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("Acc-3 (Payment-Trx-Reconciliation)")
            .fin_financial_account_id("Accounting Documents EURO - EUR")
            .c_currency_id("EUR")
            .actual_payment("120.00")
            .expected_payment("120.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("120.00")
            .total("120.00")
            .difference("0.00")
            .build(),

        new TaxData[] { new TaxData.Builder().tax("Exempt 10% - VAT 10%")
            .taxAmount("0.00")
            .taxableAmount("120.00")
            .build() },

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("120.00")
            .paid("120.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.CURRENT_DATE)
            .expected("120.00")
            .paid("120.00")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .paymentTerms("90 days")
            .priceList("Purchase")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .summedLineAmount("20.00")
            .grandTotalAmount("20.00")
            .currency("EUR")
            .build(),

        // Verify first purchase invoice payment plan - invoice1PaymentPlan
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("20.00")
            .paid("20.00")
            .outstanding("0.00")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        // Verify first purchase invoice payment details - invoice1PaymentDetails
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paid("20.00")
            .amount("20.00")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("120.00")
            .paid("120.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData.Builder()
            .expected("120.00")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.CURRENT_DATE)
            .expected("120.00")
            .paid("100.00")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData.Builder()
            .expected("20.00")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .expected("20.00")
            .paid("20.00")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .summedLineAmount("40.00")
            .grandTotalAmount("40.00")
            .currency("EUR")
            .build(),

        // Verify second purchase invoice payment plan - invoice2PaymentPlan
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("40.00")
            .paid("40.00")
            .outstanding("0.00")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        // Verify second purchase invoice payment details - invoice2PaymentDetails
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paid("40.00")
            .amount("40.00")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("120.00")
            .paid("120.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("3")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.CURRENT_DATE)
            .expected("120.00")
            .paid("60.00")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData.Builder()
            .expected("40.00")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .expected("40.00")
            .paid("40.00")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build(),

        // Verify second purchase invoice payment details after voiding -
        // invoice2PaymentDetailsAfterVoid
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .paid("40.00")
            .amount("40.00")
            .writeoff("0.00")
            .status("Payment Made")
            .build(),

        // Verify reverse purchase invoice payment plan - reverseInvoicePaymentPlan
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-40.00")
            .paid("-40.00")
            .outstanding("0.00")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        // Verify reverse purchase invoice payment details - reverseInvoicePaymentDetails
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .paid("-40.00")
            .amount("-40.00")
            .writeoff("0.00")
            .status("Payment Made")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .summedLineAmount("60.00")
            .grandTotalAmount("60.00")
            .currency("EUR")
            .build(),

        // Verify third purchase invoice payment plan - invoice3PaymentPlan
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("60.00")
            .paid("60.00")
            .outstanding("0.00")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        // Verify third purchase invoice payment details - invoice3PaymentDetails
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paid("60.00")
            .amount("60.00")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData.Builder()
            .expected("60.00")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .expected("60.00")
            .paid("60.00")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test to create a purchase order, add payment to order, purchase invoice from that order.
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression36297Test() throws ParseException {

    logger.info("** Start of test case APRRegression36297. **");

    // Create Purchase Order with 3 lines
    PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(purchaseOrderHeaderData);
    final String orderNo = purchaseOrder.getData("documentNo").toString();
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerficationData);
    PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLine1Data);
    purchaseOrderLines.assertData(purchaseOrderLine1VerificationData);
    purchaseOrderLines.create(purchaseOrderLine2Data);
    purchaseOrderLines.assertData(purchaseOrderLine2VerificationData);
    purchaseOrderLines.create(purchaseOrderLine3Data);
    purchaseOrderLines.assertData(purchaseOrderLine3VerificationData);
    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);

    PurchaseOrder.PaymentOutPlan orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(poPaymentPlanDataAfterBookedOrder);

    // Add Payment to purchase order
    purchaseOrder.addPayment("Accounting Documents EURO - EUR",
        "Process Made Payment(s) and Withdrawal", addPaymentVerificationData);
    purchaseOrder.assertPaymentCreatedSuccessfully();

    orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(poPaymentPlanDataAfterOrderPaid);
    PurchaseOrder.PaymentOutPlan.PaymentOutDetails orderPaymentInDetails = orderPaymentOutPlan.new PaymentOutDetails(
        mainPage);
    orderPaymentInDetails.assertCount(1);
    orderPaymentInDetails.assertData(poPaymentDetailsDataAfterOrderPaid);

    DataObject completedPurchaseOrderHeaderData = purchaseOrder.getData();

    String purchaseOrderIdentifier = String.format("%s - %s - %s",
        completedPurchaseOrderHeaderData.getDataField("documentNo"),
        completedPurchaseOrderHeaderData.getDataField("orderDate"),
        bookedPurchaseOrderHeaderVerificationData.getDataField("grandTotalAmount"));

    // Create first Purchase Invoice for order line no 10
    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    purchaseInvoice.createLinesFrom(purchaseOrderIdentifier, 1);
    purchaseInvoice.assertProcessCompletedSuccessfully();
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoice1HeaderVerificationData);

    // Check payment plan and details of first purchase Invoice
    PurchaseInvoice.PaymentOutPlan invoicePaymentPlan = purchaseInvoice.new PaymentOutPlan(
        mainPage);
    invoicePaymentPlan.assertCount(1);
    invoicePaymentPlan.assertData(invoice1PaymentPlan);
    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails invoicePaymentDetails = invoicePaymentPlan.new PaymentOutDetails(
        mainPage);
    invoicePaymentDetails.assertCount(1);
    invoicePaymentDetails.assertData(invoice1PaymentDetails);

    // Check order payment plan after first invoice
    purchaseOrder = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseOrder.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(poPaymentPlanDataAfterFirstInvoice);
    orderPaymentInDetails = orderPaymentOutPlan.new PaymentOutDetails(mainPage);
    orderPaymentInDetails.assertCount(2);
    orderPaymentInDetails.select(poPaymentDetails1DataChk);
    orderPaymentInDetails.assertData(poPaymentDetails1DataAfterFirstInvoice);
    orderPaymentInDetails.cancelOnGrid();
    orderPaymentInDetails.getTab().clearFilters();
    orderPaymentInDetails.getTab().unselectAll();
    orderPaymentInDetails.select(poPaymentDetails2DataChk);
    orderPaymentInDetails.assertData(poPaymentDetails2Data);

    // Create second Purchase Invoice for order line no 20
    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    purchaseInvoice.createLinesFrom(purchaseOrderIdentifier, 2);
    purchaseInvoice.assertProcessCompletedSuccessfully();
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoice2HeaderVerificationData);

    // Check payment plan and details of second purchase Invoice
    invoicePaymentPlan = purchaseInvoice.new PaymentOutPlan(mainPage);
    invoicePaymentPlan.assertCount(1);
    invoicePaymentPlan.assertData(invoice2PaymentPlan);
    invoicePaymentDetails = invoicePaymentPlan.new PaymentOutDetails(mainPage);
    invoicePaymentDetails.assertCount(1);
    invoicePaymentDetails.assertData(invoice2PaymentDetails);

    // Store second invoice documentno
    final String invoiceNo = purchaseInvoice.getData("documentNo").toString();

    // Check Payment Plan and Payment Plan Details for Purchase Order
    purchaseOrder = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseOrder.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(poPaymentPlanDataAfterSecondInvoice);
    orderPaymentInDetails = orderPaymentOutPlan.new PaymentOutDetails(mainPage);
    orderPaymentInDetails.assertCount(3);
    orderPaymentInDetails.select(poPaymentDetails1DataChk);
    orderPaymentInDetails.assertData(poPaymentDetails1DataAfterSecondInvoice);
    orderPaymentInDetails.cancelOnGrid();
    orderPaymentInDetails.getTab().clearFilters();
    orderPaymentInDetails.getTab().unselectAll();
    orderPaymentInDetails.select(poPaymentDetails2DataChk);
    orderPaymentInDetails.assertData(poPaymentDetails2Data);
    orderPaymentInDetails.cancelOnGrid();
    orderPaymentInDetails.getTab().clearFilters();
    orderPaymentInDetails.getTab().unselectAll();
    orderPaymentInDetails.select(poPaymentDetails3DataChk);
    orderPaymentInDetails.assertData(poPaymentDetails3Data);

    // Void the second Purchase Invoice
    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseInvoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    if (purchaseInvoice.isPosted()) {
      purchaseInvoice.unpost();
    }
    String reversedDocumentNo = String.valueOf(Integer.valueOf(invoiceNo) + 1);
    purchaseInvoice.close();
    purchaseInvoice
        .assertProcessCompletedSuccessfully2("Reversed by document: " + reversedDocumentNo);

    // Check payment plan and details of second sales invoice after voiding
    invoicePaymentPlan = purchaseInvoice.new PaymentOutPlan(mainPage);
    invoicePaymentPlan.assertCount(1);
    invoicePaymentPlan.assertData(invoice2PaymentPlan);
    invoicePaymentDetails = invoicePaymentPlan.new PaymentOutDetails(mainPage);
    invoicePaymentDetails.assertCount(1);
    invoicePaymentDetails.assertData(invoice2PaymentDetailsAfterVoid);

    // Check payment plan and details of reverse invoice
    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseInvoice.getTab().refresh();
    purchaseInvoice
        .select(new PurchaseInvoiceHeaderData.Builder().documentNo(reversedDocumentNo).build());
    invoicePaymentPlan = purchaseInvoice.new PaymentOutPlan(mainPage);
    invoicePaymentPlan.assertCount(1);
    invoicePaymentPlan.assertData(reverseInvoicePaymentPlan);
    invoicePaymentDetails = invoicePaymentPlan.new PaymentOutDetails(mainPage);
    invoicePaymentDetails.assertCount(1);
    invoicePaymentDetails.assertData(reverseInvoicePaymentDetails);

    // Check the Payment Plan and Payment Plan details for Purchase Order
    purchaseOrder = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseOrder.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());

    orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(poPaymentPlanDataAfterFirstInvoice);
    orderPaymentInDetails = orderPaymentOutPlan.new PaymentOutDetails(mainPage);
    orderPaymentInDetails.assertCount(2);
    orderPaymentInDetails.select(poPaymentDetails1DataChk);
    orderPaymentInDetails.assertData(poPaymentDetails1DataAfterFirstInvoice);
    orderPaymentInDetails.cancelOnGrid();
    orderPaymentInDetails.getTab().clearFilters();
    orderPaymentInDetails.getTab().unselectAll();
    orderPaymentInDetails.select(poPaymentDetails2DataChk);
    orderPaymentInDetails.assertData(poPaymentDetails2Data);

    // Create another Purchase Invoice for order line no 20
    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    purchaseInvoice.createLinesFrom(purchaseOrderIdentifier, 2);
    purchaseInvoice.assertProcessCompletedSuccessfully();
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoice2HeaderVerificationData);

    // Check payment plan and details of another purchase Invoice for order line 20
    invoicePaymentPlan = purchaseInvoice.new PaymentOutPlan(mainPage);
    invoicePaymentPlan.assertCount(1);
    invoicePaymentPlan.assertData(invoice2PaymentPlan);
    invoicePaymentDetails = invoicePaymentPlan.new PaymentOutDetails(mainPage);
    invoicePaymentDetails.assertCount(1);
    invoicePaymentDetails.assertData(invoice2PaymentDetails);

    // Check Payment Plan and Payment Plan Details for Purchase Order
    purchaseOrder = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseOrder.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(poPaymentPlanDataAfterSecondInvoice);
    orderPaymentInDetails = orderPaymentOutPlan.new PaymentOutDetails(mainPage);
    orderPaymentInDetails.assertCount(3);
    orderPaymentInDetails.select(poPaymentDetails1DataChk);
    orderPaymentInDetails.assertData(poPaymentDetails1DataAfterSecondInvoice);
    orderPaymentInDetails.cancelOnGrid();
    orderPaymentInDetails.getTab().clearFilters();
    orderPaymentInDetails.getTab().unselectAll();
    orderPaymentInDetails.select(poPaymentDetails2DataChk);
    orderPaymentInDetails.assertData(poPaymentDetails2Data);
    orderPaymentInDetails.cancelOnGrid();
    orderPaymentInDetails.getTab().clearFilters();
    orderPaymentInDetails.getTab().unselectAll();
    orderPaymentInDetails.select(poPaymentDetails3DataChk);
    orderPaymentInDetails.assertData(poPaymentDetails3Data);

    // Create third Purchase Invoice for order line no 30
    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    purchaseInvoice.createLinesFrom(purchaseOrderIdentifier, 3);
    purchaseInvoice.assertProcessCompletedSuccessfully();
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoice3HeaderVerificationData);

    // Check payment plan and details of third purchase Invoice for order line 30
    invoicePaymentPlan = purchaseInvoice.new PaymentOutPlan(mainPage);
    invoicePaymentPlan.assertCount(1);
    invoicePaymentPlan.assertData(invoice3PaymentPlan);
    invoicePaymentDetails = invoicePaymentPlan.new PaymentOutDetails(mainPage);
    invoicePaymentDetails.assertCount(1);
    invoicePaymentDetails.assertData(invoice3PaymentDetails);

    // Check Payment Plan and Payment Plan Details for Purchase Order
    purchaseOrder = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();

    purchaseOrder.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(poPaymentPlanDataAfterSecondInvoice);
    orderPaymentInDetails = orderPaymentOutPlan.new PaymentOutDetails(mainPage);
    orderPaymentInDetails.assertCount(3);
    orderPaymentInDetails.select(poPaymentDetails4DataChk);
    orderPaymentInDetails.assertData(poPaymentDetails4Data);
    orderPaymentInDetails.cancelOnGrid();
    orderPaymentInDetails.getTab().clearFilters();
    orderPaymentInDetails.getTab().unselectAll();
    orderPaymentInDetails.select(poPaymentDetails2DataChk);
    orderPaymentInDetails.assertData(poPaymentDetails2Data);
    orderPaymentInDetails.cancelOnGrid();
    orderPaymentInDetails.getTab().clearFilters();
    orderPaymentInDetails.getTab().unselectAll();
    orderPaymentInDetails.select(poPaymentDetails3DataChk);
    orderPaymentInDetails.assertData(poPaymentDetails3Data);

    logger.info("** End of test case APRRegression36297. **");
  }

  @AfterClass
  public static void tearDown() {
    SeleniumSingleton.INSTANCE.quit();
    OpenbravoERPTest.seleniumStarted = false;
    OpenbravoERPTest.forceLoginRequired();
  }
}
