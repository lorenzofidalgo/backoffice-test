/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions9;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentGrid;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount.PaymentMethod;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 36753
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression36753Out3 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  AccountData accountData;
  PaymentMethodData paymentMethodData;
  PaymentOutHeaderData paymentInHeaderData;
  PaymentOutHeaderData paymentInHeaderVerificationData;
  PaymentOutHeaderData paymentInDetailsHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  PurchaseOrderLinesData purchaseOrderLinesData;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  PurchaseOrderLinesData purchaseOrderLinesData2;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData2;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;
  PaymentPlanData orderPendingPaymentOutPlanData;
  PaymentPlanData orderPaidPaymentOutPlanData;
  PaymentDetailsData orderPaidPaymentOutDetailsData;
  PaymentOutHeaderData paymentIn2HeaderVerificationData;
  PaymentOutLinesData paymentIn2LinesVerificationData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePaidPaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaidPaymentOutDetailsData;
  PaymentPlanData orderInvoicedPaymentOutPlanData;
  PaymentDetailsData orderInvoicedPaymentOutDetailsData;
  PaymentDetailsData orderInvoicedPaymentOutDetailsData2;
  PaymentOutLinesData paymentInLinesInvoicedVerificationData;
  PaymentOutLinesData paymentInLinesInvoicedVerificationData2;
  PurchaseInvoiceLinesData purchaseInvoice2LineVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoice2HeaderVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice2PaidPaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice2PaidPaymentOutDetailsData;
  PaymentDetailsData orderInvoiced2PaymentOutDetailsData;
  PaymentOutLinesData paymentInLinesInvoiced2VerificationData;
  PaymentOutHeaderData paymentReactivatedInHeaderVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice1PendingPaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice2PendingPaymentOutPlanData;
  PaymentOutHeaderData paymentInExecutedHeaderData;
  String[][] journalEntryLines1;
  String[][] journalEntryLines2;
  PaymentPlanData orderPaymentReceivedPaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice1ExecutedPaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice1ExecutedPaymentOutDetailsData;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice2ExecutedPaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice2ExecutedPaymentOutDetailsData;
  String[][] journalEntryLines5;
  String[][] journalEntryLines6;

  /**
   * Class constructor.
   *
   */
  public APRRegression36753Out3(AccountData accountData, PaymentMethodData paymentMethodData,
      PaymentOutHeaderData paymentInHeaderData,
      PaymentOutHeaderData paymentInHeaderVerificationData,
      PaymentOutHeaderData paymentInDetailsHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      PurchaseOrderLinesData purchaseOrderLinesData2,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData2,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      PaymentPlanData orderPendingPaymentOutPlanData, PaymentPlanData orderPaidPaymentOutPlanData,
      PaymentDetailsData orderPaidPaymentOutDetailsData,
      PaymentOutHeaderData paymentIn2HeaderVerificationData,
      PaymentOutLinesData paymentIn2LinesVerificationData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentOutDetailsData,
      PaymentPlanData orderInvoicedPaymentOutPlanData,
      PaymentDetailsData orderPaymentOutDetailsData2,
      PaymentDetailsData orderPaymentOutDetailsData3,
      PaymentOutLinesData paymentInLinesInvoicedVerificationData,
      PaymentOutLinesData paymentInLinesInvoicedVerificationData2,
      PurchaseInvoiceLinesData purchaseInvoice2LineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoice2HeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice2PaidPaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice2PaidPaymentOutDetailsData,
      PaymentDetailsData orderInvoiced2PaymentOutDetailsData,
      PaymentOutLinesData paymentInLinesInvoiced2VerificationData,
      PaymentOutHeaderData paymentReactivatedInHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice1PendingPaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice2PendingPaymentOutPlanData,
      PaymentOutHeaderData paymentInExecutedHeaderData, String[][] journalEntryLines1,
      String[][] journalEntryLines2, PaymentPlanData orderPaymentReceivedPaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice1ExecutedPaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice1ExecutedPaymentOutDetailsData,
      String[][] journalEntryLines3, String[][] journalEntryLines4,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoice2ExecutedPaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoice2ExecutedPaymentOutDetailsData,
      String[][] journalEntryLines5, String[][] journalEntryLines6) {

    this.accountData = accountData;
    this.paymentMethodData = paymentMethodData;
    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.paymentInDetailsHeaderData = paymentInDetailsHeaderData;
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.purchaseOrderLinesData2 = purchaseOrderLinesData2;
    this.purchaseOrderLinesVerificationData2 = purchaseOrderLinesVerificationData2;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.orderPendingPaymentOutPlanData = orderPendingPaymentOutPlanData;
    this.orderPaidPaymentOutPlanData = orderPaidPaymentOutPlanData;
    this.orderPaidPaymentOutDetailsData = orderPaidPaymentOutDetailsData;
    this.paymentIn2HeaderVerificationData = paymentIn2HeaderVerificationData;
    this.paymentIn2LinesVerificationData = paymentIn2LinesVerificationData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.invoicePaidPaymentOutPlanData = invoicePaymentOutPlanData;
    this.invoicePaidPaymentOutDetailsData = invoicePaymentOutDetailsData;
    this.orderInvoicedPaymentOutPlanData = orderInvoicedPaymentOutPlanData;
    this.orderInvoicedPaymentOutDetailsData = orderPaymentOutDetailsData2;
    this.orderInvoicedPaymentOutDetailsData2 = orderPaymentOutDetailsData3;
    this.paymentInLinesInvoicedVerificationData = paymentInLinesInvoicedVerificationData;
    this.paymentInLinesInvoicedVerificationData2 = paymentInLinesInvoicedVerificationData2;
    this.purchaseInvoice2LineVerificationData = purchaseInvoice2LineVerificationData;
    this.completedPurchaseInvoice2HeaderVerificationData = completedPurchaseInvoice2HeaderVerificationData;
    this.invoice2PaidPaymentOutPlanData = invoice2PaidPaymentOutPlanData;
    this.invoice2PaidPaymentOutDetailsData = invoice2PaidPaymentOutDetailsData;
    this.orderInvoiced2PaymentOutDetailsData = orderInvoiced2PaymentOutDetailsData;
    this.paymentInLinesInvoiced2VerificationData = paymentInLinesInvoiced2VerificationData;
    this.paymentReactivatedInHeaderVerificationData = paymentReactivatedInHeaderVerificationData;
    this.invoice1PendingPaymentOutPlanData = invoice1PendingPaymentOutPlanData;
    this.invoice2PendingPaymentOutPlanData = invoice2PendingPaymentOutPlanData;
    this.paymentInExecutedHeaderData = paymentInExecutedHeaderData;
    this.journalEntryLines1 = journalEntryLines1;
    this.journalEntryLines2 = journalEntryLines2;
    this.orderPaymentReceivedPaymentOutPlanData = orderPaymentReceivedPaymentOutPlanData;
    this.invoice1ExecutedPaymentOutPlanData = invoice1ExecutedPaymentOutPlanData;
    this.invoice1ExecutedPaymentOutDetailsData = invoice1ExecutedPaymentOutDetailsData;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    this.invoice2ExecutedPaymentOutPlanData = invoice2ExecutedPaymentOutPlanData;
    this.invoice2ExecutedPaymentOutDetailsData = invoice2ExecutedPaymentOutDetailsData;
    this.journalEntryLines5 = journalEntryLines5;
    this.journalEntryLines6 = journalEntryLines6;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> PurchaseOrderValues() {
    Object[][] data = new Object[][] { {

        new AccountData.Builder().name("Accounting Documents EURO").build(),

        new PaymentMethodData.Builder().automaticDeposit(false)
            .payoutExecutionType("Automatic")
            .payoutExecutionProcess("Simple Execution Process")
            .payoutDeferred(true)
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .generatedCredit("1000")
            .account("Accounting Documents EURO - EUR")
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("0.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Payment")
            .generatedCredit("1,000.00")
            .usedCredit("0.00")
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("1,000.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Execution")
            .generatedCredit("1,000.00")
            .usedCredit("0.00")
            .build(),

        new PurchaseOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .build(),

        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .transactionDocument("Purchase Order")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Purchase")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .build(),

        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMC").priceListVersion("Purchase").build())
            .orderedQuantity("745")
            .tax("Exempt")
            .build(),

        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("1,490.00")
            .build(),

        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMA").priceListVersion("Purchase").build())
            .orderedQuantity("255")
            .tax("Exempt")
            .build(),

        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("510.00")
            .build(),

        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("2,000.00")
            .grandTotalAmount("2,000.00")
            .currency("EUR")
            .build(),

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("2,000.00")
            .paid("0.00")
            .outstanding("2,000.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("2,000.00")
            .paid("0.00")
            .outstanding("2,000.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        new PaymentDetailsData.Builder().paymentDate(OBDate.CURRENT_DATE)
            .expected("2,000.00")
            .paid("2,000.00")
            .writeoff("0.00")
            .status("Awaiting Execution")
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("1,000.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Execution")
            .generatedCredit("0.00")
            .usedCredit("1,000.00")
            .build(),

        new PaymentOutLinesData.Builder().dueDate(OBDate.CURRENT_DATE)
            .invoiceAmount("2,000.00")
            .expected("2,000.00")
            .paid("2,000.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentTerms("90 days")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .priceList("Purchase")
            .paymentTerms("90 days")
            .build(),

        new PurchaseInvoiceLinesData.Builder().invoicedQuantity("745")
            .lineNetAmount("1,490.00")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("1,490.00")
            .summedLineAmount("1,490.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("1,490.00")
            .paid("0.00")
            .outstanding("1,490.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paid("1,490.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("1,490.00")
            .account("Accounting Documents EURO - EUR")
            .status("Awaiting Execution")
            .build(),

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("2,000.00")
            .paid("0.00")
            .outstanding("2,000.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),

        new PaymentDetailsData.Builder().expected("2,000.00")
            .paid("510.00")
            .writeoff("0.00")
            .status("Awaiting Execution")
            .build(),

        new PaymentDetailsData.Builder().expected("1,490.00")
            .paid("1,490.00")
            .writeoff("0.00")
            .status("Awaiting Execution")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("2,000.00")
            .expected("2,000.00")
            .paid("510.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("1,490.00")
            .expected("1,490.00")
            .paid("1,490.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PurchaseInvoiceLinesData.Builder().invoicedQuantity("255")
            .lineNetAmount("510.00")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("510.00")
            .summedLineAmount("510.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("510.00")
            .paid("0.00")
            .outstanding("510.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paid("510.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("510.00")
            .account("Accounting Documents EURO - EUR")
            .status("Awaiting Execution")
            .build(),

        new PaymentDetailsData.Builder().expected("510.00")
            .paid("510.00")
            .writeoff("0.00")
            .status("Awaiting Execution")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("510.00")
            .expected("510.00")
            .paid("510.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("0.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Awaiting Payment")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("1,490.00")
            .paid("0.00")
            .outstanding("1,490.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("0")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("510.00")
            .paid("0.00")
            .outstanding("510.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("0")
            .build(),
        //
        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("1,000.00")
            .account("Accounting Documents EURO - EUR")
            .currency("EUR")
            .status("Payment Made")
            .generatedCredit("0.00")
            .usedCredit("1,000.00")
            .build(),

        new String[][] { { "40000", "Proveedores (euros)", "2,000.00", "" },
            { "40700", "Anticipos a proveedores", "", "1,000.00" },
            { "40100", "Proveedores efectos comerciales a pagar", "", "1,000.00" } },
        new String[][] { { "21100", "Accounts payable", "5,000.00", "" },
            { "1410", "Vendor prepayment", "", "2,500.00" },
            { "11300", "Bank in transit", "", "2,500.00" } },

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("2,000.00")
            .paid("2,000.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("1,490.00")
            .paid("1,490.00")
            .outstanding("0.00")
            .currency("EUR")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paid("1,490.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("1,490.00")
            .account("Accounting Documents EURO - EUR")
            .status("Payment Made")
            .build(),

        new String[][] { { "60000", "Compras de mercaderías", "1,490.00", "" },
            { "40000", "Proveedores (euros)", "", "1,490.00" }, },

        new String[][] { { "51200", "Service costs", "3,725.00", "" },
            { "21100", "Accounts payable", "", "3,725.00" } },

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("510.00")
            .paid("510.00")
            .outstanding("0.00")
            .currency("EUR")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paid("510.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("510.00")
            .account("Accounting Documents EURO - EUR")
            .status("Payment Made")
            .build(),

        new String[][] { { "60000", "Compras de mercaderías", "510.00", "" },
            { "40000", "Proveedores (euros)", "", "510.00" }, },

        new String[][] { { "51200", "Service costs", "1,275.00", "" },
            { "21100", "Accounts payable", "", "1,275.00" } },

        } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 36753 Paying from order using credit
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression36753Out3Test() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression36753Out3] Test regression 36753 Paying from order using credit**");

    // Change payment method configuration
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountData);
    PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod
        .select(new PaymentMethodData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build());
    paymentMethod.edit(paymentMethodData);

    // // Get Business partner current balance
    // BusinessPartnerWindow businessPartnerWindow = new BusinessPartnerWindow();
    // mainPage.openView(businessPartnerWindow);
    // mainPage.waitForDataToLoad();
    //
    // BusinessPartnerTab businessPartnerTab = businessPartnerWindow.selectBusinessPartnerTab();
    // businessPartnerTab.select(new BusinessPartnerData.Builder().name("Vendor A")
    // .searchKey("CUSA").build());
    //
    // businessPartnerTab.getForm().assertStatusBarFields(
    // new BusinessPartnerData.Builder().creditUsed("8,432.80").build());
    //
    // String currentBalance = (String) businessPartnerTab.getData("creditUsed");
    // currentBalance = currentBalance.replace(",", "");
    // BigDecimal bpCurrentBalance = new BigDecimal(currentBalance);

    // Create a payment out with credit of 1000
    PaymentOut payment = new PaymentOut(mainPage).open();
    payment.create(paymentInHeaderData);
    payment.assertData(paymentInHeaderVerificationData);

    AddPaymentProcess paymentDetails = payment.addDetailsOpen();
    AddPaymentGrid orderInvoiceGrid = paymentDetails.getOrderInvoiceGrid();
    orderInvoiceGrid.unselectAll();
    paymentDetails.process("Process Made Payment(s) and Withdrawal",
        "Leave the credit to be used later",
        new AddPaymentPopUpData.Builder().actual_payment("1,000.00").build());

    payment.assertData(paymentInDetailsHeaderData);
    PaymentOut.Lines paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(1);
    paymentInLines.assertData(new PaymentOutLinesData.Builder().paid("1,000.00").build());

    String payment1No = (String) payment.getData("documentNo");

    // Execute payment and check status
    payment.execute();
    payment.assertData(new PaymentOutHeaderData.Builder().status("Payment Made").build());

    // // Check Business Partner current balance
    // bpCurrentBalance = bpCurrentBalance.subtract(new BigDecimal("1000.00"));
    // DecimalFormat format = new DecimalFormat("#,##0.00");
    // businessPartnerWindow = new BusinessPartnerWindow();
    // mainPage.openView(businessPartnerWindow);
    // mainPage.waitForDataToLoad();
    //
    // businessPartnerTab = businessPartnerWindow.selectBusinessPartnerTab();
    // businessPartnerTab.select(new BusinessPartnerData.Builder().name("Vendor A")
    // .searchKey("CUSA").build());
    // businessPartnerTab.assertData(new BusinessPartnerData.Builder().creditUsed(
    // format.format(bpCurrentBalance)).build());

    // Register a Purchase order
    PurchaseOrder order = new PurchaseOrder(mainPage).open();
    order.create(purchaseOrderHeaderData);
    order.assertSaved();
    order.assertData(purchaseOrderHeaderVerficationData);
    String orderNo = order.getData("documentNo").toString();

    // Create order lines
    PurchaseOrder.Lines orderLines = order.new Lines(mainPage);
    orderLines.create(purchaseOrderLinesData);
    orderLines.assertSaved();
    orderLines.assertData(purchaseOrderLinesVerificationData);

    orderLines = order.new Lines(mainPage);
    orderLines.create(purchaseOrderLinesData2);
    orderLines.assertSaved();
    orderLines.assertData(purchaseOrderLinesVerificationData2);

    // Book the order
    order.book();
    order.assertProcessCompletedSuccessfully2();
    order.assertData(bookedPurchaseOrderHeaderVerificationData);

    // Check pending order payment plan
    PurchaseOrder.PaymentOutPlan orderPaymentPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPendingPaymentOutPlanData);

    // Pay the order using the credit and a payment amount of 1000
    AddPaymentProcess paymentProcess = order.openAddPayment();
    paymentProcess.setParameterValue("fin_financial_account_id", "Accounting Documents EURO - EUR");
    paymentProcess.process("Orders", orderNo, payment1No, "Process Made Payment(s) and Withdrawal",
        null, null);

    String purchaseOrderIdentifier = String.format("%s - %s - %s", orderNo,
        order.getData("orderDate"), "2000.00");

    // Check order payment plan
    orderPaymentPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPaidPaymentOutPlanData);

    // Check order payment out details
    PurchaseOrder.PaymentOutPlan.PaymentOutDetails orderPaymentOutDetails = orderPaymentPlan.new PaymentOutDetails(
        mainPage);
    orderPaymentOutDetails.assertCount(1);
    orderPaymentOutDetails.assertData(orderPaidPaymentOutDetailsData);

    String payment2No = orderPaymentOutDetails.getData("payment").toString();
    payment2No = payment2No.substring(0, payment2No.indexOf("-") - 1);
    Sleep.trySleep(15000);
    // Check payment out created in add payment
    payment = new PaymentOut(mainPage).open();
    Sleep.trySleep(15000);
    payment.select(new PaymentOutHeaderData.Builder().documentNo(payment2No).build());
    Sleep.trySleep(15000);
    payment.assertData(paymentIn2HeaderVerificationData);
    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(1);
    paymentInLines.assertData((PaymentOutLinesData) paymentIn2LinesVerificationData
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo));

    // Create a Purchase invoice from order
    PurchaseInvoice invoice = new PurchaseInvoice(mainPage).open();
    invoice.create(purchaseInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(purchaseInvoiceHeaderVerificationData);
    invoice.createLinesFrom(purchaseOrderIdentifier, 1);
    invoice.assertProcessCompletedSuccessfully();

    PurchaseInvoice.Lines invoiceLines = invoice.new Lines(mainPage);
    invoiceLines.assertCount(1);
    invoiceLines.assertData(purchaseInvoiceLineVerificationData);
    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedPurchaseInvoiceHeaderVerificationData);
    String invoice1No = (String) invoice.getData("documentNo");

    // Check invoice payment plan
    PurchaseInvoice.PaymentOutPlan invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoicePaidPaymentOutPlanData);

    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoicePaidPaymentOutDetailsData);

    // Check Purchase order payment plan
    order = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderInvoicedPaymentOutPlanData);

    orderPaymentOutDetails = orderPaymentPlan.new PaymentOutDetails(mainPage);
    orderPaymentOutDetails.assertCount(2);
    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().expected("2000.00").paid("510.00").build());
    orderPaymentOutDetails.assertData(orderInvoicedPaymentOutDetailsData);

    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().paid("1490.00").expected("1490.00").build());
    orderPaymentOutDetails.assertData(orderInvoicedPaymentOutDetailsData2);

    // Check payment out
    payment = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentOutHeaderData.Builder().documentNo(payment2No).build());
    payment.assertData(paymentIn2HeaderVerificationData);
    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);

    paymentInLinesInvoicedVerificationData.addDataField("orderPaymentSchedule$order$documentNo",
        orderNo);
    paymentInLinesInvoicedVerificationData2
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo)
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice1No);
    paymentInLines
        .select(new PaymentOutLinesData.Builder().paid("510.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData);
    paymentInLines.closeForm();

    paymentInLines
        .select(new PaymentOutLinesData.Builder().paid("1,490.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData2);
    paymentInLines.closeForm();

    // Create a second Purchase invoice from order
    invoice = new PurchaseInvoice(mainPage).open();
    invoice.create(purchaseInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(purchaseInvoiceHeaderVerificationData);
    invoice.createLinesFrom(purchaseOrderIdentifier, 2);
    invoice.assertProcessCompletedSuccessfully();

    invoiceLines = invoice.new Lines(mainPage);
    invoiceLines.assertCount(1);
    invoiceLines.assertData(purchaseInvoice2LineVerificationData);
    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedPurchaseInvoice2HeaderVerificationData);
    String invoice2No = (String) invoice.getData("documentNo");

    // Check invoice payment plan
    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice2PaidPaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoice2PaidPaymentOutDetailsData);

    // Check Purchase order payment plan
    order = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderInvoicedPaymentOutPlanData);

    orderPaymentOutDetails = orderPaymentPlan.new PaymentOutDetails(mainPage);
    orderPaymentOutDetails.assertCount(2);
    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().expected("510.00").paid("510.00").build());
    orderPaymentOutDetails.assertData(orderInvoiced2PaymentOutDetailsData);

    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().paid("1490.00").expected("1490.00").build());
    orderPaymentOutDetails.assertData(orderInvoicedPaymentOutDetailsData2);

    // Check payment out
    payment = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentOutHeaderData.Builder().documentNo(payment2No).build());
    payment.assertData(paymentIn2HeaderVerificationData);
    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);

    paymentInLinesInvoiced2VerificationData
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo)
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice2No);
    paymentInLines
        .select(new PaymentOutLinesData.Builder().paid("510.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoiced2VerificationData);
    paymentInLines.closeForm();

    paymentInLines
        .select(new PaymentOutLinesData.Builder().paid("1,490.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData2);
    paymentInLines.closeForm();

    // Reactivate payment and delete lines and check
    payment.open();
    payment.reactivate("Reactivate and Delete Lines");
    payment.assertProcessCompletedSuccessfully2();
    payment.assertData(paymentReactivatedInHeaderVerificationData);

    // Check Purchase order payment plan
    order = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPendingPaymentOutPlanData);

    // Check invoice1 payment plan
    invoice = new PurchaseInvoice(mainPage).open();
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice1No).build());
    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice1PendingPaymentOutPlanData);

    // Check invoice2 payment plan
    invoice = new PurchaseInvoice(mainPage).open();
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice2No).build());
    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice2PendingPaymentOutPlanData);

    // Add payment out details
    payment = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentOutHeaderData.Builder().documentNo(payment2No).build());

    payment.addDetailsOpen()
        .process("Orders", orderNo, payment1No, "Process Made Payment(s) and Withdrawal", null,
            null);

    // Check payment out
    payment.assertData(paymentIn2HeaderVerificationData);
    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);

    paymentInLines
        .select(new PaymentOutLinesData.Builder().paid("510.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoiced2VerificationData);
    paymentInLines.closeForm();

    paymentInLines
        .select(new PaymentOutLinesData.Builder().paid("1,490.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData2);

    // Check Purchase order payment plan
    order = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderInvoicedPaymentOutPlanData);

    orderPaymentOutDetails = orderPaymentPlan.new PaymentOutDetails(mainPage);
    orderPaymentOutDetails.assertCount(2);
    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().expected("510.00").paid("510.00").build());
    orderPaymentOutDetails.assertData(orderInvoiced2PaymentOutDetailsData);

    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().paid("1490.00").expected("1490.00").build());
    orderPaymentOutDetails.assertData(orderInvoicedPaymentOutDetailsData2);

    // Check invoice1 payment plan
    invoice = new PurchaseInvoice(mainPage).open();
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice1No).build());
    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoicePaidPaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoicePaidPaymentOutDetailsData);

    // Check invoice2 payment plan
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice2No).build());
    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice2PaidPaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoice2PaidPaymentOutDetailsData);

    // Execute payment out and check
    payment = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentOutHeaderData.Builder().documentNo(payment2No).build());
    payment.execute();
    payment.assertPaymentExecutedSuccessfully();
    payment.assertData(paymentInExecutedHeaderData);

    paymentInLines = payment.new Lines(mainPage);
    paymentInLines.assertCount(2);

    paymentInLines
        .select(new PaymentOutLinesData.Builder().paid("510.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoiced2VerificationData);
    paymentInLines.closeForm();

    paymentInLines
        .select(new PaymentOutLinesData.Builder().paid("1,490.00").orderno(orderNo).build());
    paymentInLines.assertData(paymentInLinesInvoicedVerificationData2);
    paymentInLines.closeForm();

    // Post payment out and check
    payment.open();
    payment.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines1.length);
    post.assertJournalLines2(journalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check Purchase order payment plan
    order = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentPlan = order.new PaymentOutPlan(mainPage);
    orderPaymentPlan.assertCount(1);
    orderPaymentPlan.assertData(orderPaymentReceivedPaymentOutPlanData);

    orderInvoicedPaymentOutDetailsData2.addDataField("status", "Payment Made");
    orderInvoiced2PaymentOutDetailsData.addDataField("status", "Payment Made");

    orderPaymentOutDetails = orderPaymentPlan.new PaymentOutDetails(mainPage);
    orderPaymentOutDetails.assertCount(2);
    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().expected("510.00").paid("510.00").build());
    orderPaymentOutDetails.assertData(orderInvoiced2PaymentOutDetailsData);

    orderPaymentOutDetails
        .select(new PaymentDetailsData.Builder().paid("1490.00").expected("1490.00").build());
    orderPaymentOutDetails.assertData(orderInvoicedPaymentOutDetailsData2);

    // Check invoice1 payment plan
    invoice = new PurchaseInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice1No).build());
    invoice.assertData(new PurchaseInvoiceHeaderData.Builder().paymentComplete(true).build());

    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice1ExecutedPaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoice1ExecutedPaymentOutDetailsData);

    invoice.open();
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines3.length);
    post.assertJournalLines2(journalEntryLines3);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines4.length);
    post.assertJournalLines2(journalEntryLines4);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check invoice2 payment plan
    invoice = new PurchaseInvoice(mainPage).open();
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoice2No).build());
    invoice.assertData(new PurchaseInvoiceHeaderData.Builder().paymentComplete(true).build());

    invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoice2ExecutedPaymentOutPlanData);

    invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoice2ExecutedPaymentOutDetailsData);

    // Post invoice2 and check post
    invoice.open();
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines5.length);
    post.assertJournalLines2(journalEntryLines5);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines6.length);
    post.assertJournalLines2(journalEntryLines6);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Restore the payment method configuration
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountData);
    paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod
        .select(new PaymentMethodData.Builder().paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build());
    paymentMethod.edit(new PaymentMethodData.Builder().automaticDeposit(false)
        .payoutExecutionType("Manual")
        .build());

    logger.info(
        "** End of test case [APRRegression36753Out3] Test regression 36753 Paying from order using credit**");
  }
}
