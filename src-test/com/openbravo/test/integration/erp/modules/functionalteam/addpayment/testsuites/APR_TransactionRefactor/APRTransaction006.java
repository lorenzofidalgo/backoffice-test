/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_TransactionRefactor;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.PaymentSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.util.OBDate;

/**
 * Create a Transaction with an existing payment.
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRTransaction006 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  // /** The account header data. */

  // /** The transaction header data. */

  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceLinesData salesInvoiceLinesData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  AddPaymentPopUpData paymentInPlanData;
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  SalesInvoiceHeaderData payedSalesInvoiceHeaderVerificationData;
  AccountData accountHeaderData;
  TransactionsData transactionLinesData;

  /**
   * Class constructor.
   *
   * @param accountHeaderData
   *          The account Header Data.
   * @param transactionLinesData
   *          The transaction Header Data.
   */

  public APRTransaction006(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceLinesData salesInvoiceLinesData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      AddPaymentPopUpData paymentInPlanData, AddPaymentPopUpData addPaymentTotalsVerificationData,
      SalesInvoiceHeaderData payedSalesInvoiceHeaderVerificationData, AccountData accountHeaderData,
      TransactionsData transactionLinesData) {

    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceLinesData = salesInvoiceLinesData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.paymentInPlanData = paymentInPlanData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.payedSalesInvoiceHeaderVerificationData = payedSalesInvoiceHeaderVerificationData;
    this.accountHeaderData = accountHeaderData;
    this.transactionLinesData = transactionLinesData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {

        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer B").build())
            .paymentTerms("30 days, 5")
            .paymentMethod("1 (Spain)")
            .build(),

        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Soccer Ball").build())
            .invoicedQuantity("100")
            .build(),

        new SalesInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Customer center nº23")
            .priceList("Customer B")
            .invoiceDate(OBDate.CURRENT_DATE)
            .accountingDate(OBDate.CURRENT_DATE)
            .build(),

        new SalesInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("4,070.00")
            .documentStatus("Completed")
            .summedLineAmount("3,700.00")
            .grandTotalAmount("4,070.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new AddPaymentPopUpData.Builder().fin_paymentmethod_id("1 (Spain)")
            .actual_payment("4,070.00")
            .build(),

        new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
            .amount_inv_ords("4,070.00")
            .total("4,070.00")
            .difference("0.00")
            .build(),

        new SalesInvoiceHeaderData.Builder().paymentComplete(true)
            .documentStatus("Completed")
            .summedLineAmount("3,700.00")
            .grandTotalAmount("4,070.00")
            .currency("EUR")
            .build(),

        new AccountData.Builder().name("Spain Bank").build(),

        new TransactionsData.Builder().transactionType("BP Deposit").build() } };

    return Arrays.asList(data);
  }

  /**
   * Test to create a sales order, and a goods receipt and sales invoice from that order.
   *
   * @throws ParseException
   */
  @Test
  public void APRTransaction006Test() throws ParseException {
    logger.info(
        "** Start of test case [APRTransaction006] Create a deposit transaction creating a payment in the selector without modifying the document. **");

    final SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    String invoiceNo = salesInvoice.getData("documentNo").toString();

    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.create(salesInvoiceLinesData);
    salesInvoiceLines.assertCount(1);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Transaction transactions = financialAccount.new Transaction(mainPage);

    PaymentSelectorData addPaymentData = new PaymentSelectorData.Builder().amount("4,070.00")
        .addPayment(addPaymentTotalsVerificationData)
        .addEditPayment(paymentInPlanData)
        .build();
    addPaymentData.addDataField("invoiceNo", invoiceNo);
    transactionLinesData.addDataField("finPayment", addPaymentData);
    addPaymentData.addDataField("currentTab", financialAccount.getTab());
    addPaymentData.addDataField("transactions", transactions);

    transactions.create(transactionLinesData);
    transactions.assertSaved();
    transactions.process();

    salesInvoice.open();
    salesInvoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    salesInvoice.assertData(payedSalesInvoiceHeaderVerificationData);

    logger.info(
        "** End of test case [APRTransaction006] Create a deposit transaction creating a payment in the selector without modifying the document. **");
  }
}
