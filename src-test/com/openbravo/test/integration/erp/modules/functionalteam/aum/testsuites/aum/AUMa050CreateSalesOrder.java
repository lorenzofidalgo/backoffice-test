/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <f.carballo@nectus.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.aum;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;

/**
 *
 * Class for Test UOMa050
 *
 * @author Nono Carballo
 *
 */

@RunWith(Parameterized.class)
public class AUMa050CreateSalesOrder extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  SalesOrderHeaderData headerData;
  SalesOrderLinesData lineData;
  SalesOrderLinesData lineDataSaved;
  SalesOrderLinesData lineDataAum;
  SalesOrderLinesData lineDataSavedAum;

  public AUMa050CreateSalesOrder(SalesOrderHeaderData headerData, SalesOrderLinesData lineData,
      SalesOrderLinesData lineDataSaved, SalesOrderLinesData lineDataAum,
      SalesOrderLinesData lineDataSavedAum) {
    super();
    this.headerData = headerData;
    this.lineData = lineData;
    this.lineDataSaved = lineDataSaved;
    this.lineDataAum = lineDataAum;
    this.lineDataSavedAum = lineDataSavedAum;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> returnValues() {
    Object[][] data = new Object[][] { {
        new SalesOrderHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .transactionDocument("Standard Order")
            .build(),
        new SalesOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().name("Final good B").build())
            .operativeQuantity("10")
            .operativeUOM("Bag")
            .build(),
        new SalesOrderLinesData.Builder().orderedQuantity("10").build(),
        new SalesOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().name("Final good A").build())
            .operativeQuantity("10")
            .build(),
        new SalesOrderLinesData.Builder().orderedQuantity("20").build() } };
    return Arrays.asList(data);
  }

  @Test
  public void UOMa050Test() throws ParseException {

    logger.debug("** Start test case [UOMa050] Create Sales Order **");

    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(headerData);
    salesOrder.assertSaved();

    SalesOrder.Lines salesOrderLine = salesOrder.new Lines(mainPage);
    salesOrderLine.create(lineData);
    salesOrderLine.assertSaved();
    salesOrderLine.assertData(lineDataSaved);

    salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(headerData);
    salesOrder.assertSaved();

    salesOrderLine = salesOrder.new Lines(mainPage);
    salesOrderLine.create(lineDataAum);
    salesOrderLine.assertSaved();
    salesOrderLine.assertData(lineDataSavedAum);

    salesOrder.book();

    logger.debug("** End test case [UOMa050] Create Sales Order **");
  }

}
