/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2017-05-29 09:37:14
 * Contributor(s):
 *   Alejandro Matos <alekosmp86@gmail.com>
 *************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.product.AUMData;
import com.openbravo.test.integration.erp.data.masterdata.product.PriceData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.gui.masterdata.product.AUMTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.PriceTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;
import com.openbravo.test.integration.selenium.Sleep;

/**
 *
 * Class for Test AUMRTVa000
 *
 * @author Alejandro Matos
 *
 */
@RunWith(Parameterized.class)
public class AUMRTVa000CreateReturnableProductWithAUM extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager
      .getLogger();

  /** Data for Test */

  ProductData productData;
  PriceData productPrice;
  AUMData productAUM;
  ProductData productSB;
  ProductData productTB;

  public AUMRTVa000CreateReturnableProductWithAUM(ProductData productData, PriceData productPrice,
      AUMData productAUM, ProductData productSB, ProductData productTB) {
    super();
    this.productData = productData;
    this.productPrice = productPrice;
    this.productAUM = productAUM;
    this.productSB = productSB;
    this.productTB = productTB;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> returnToVendorValues() {
    Object[][] data = new Object[][] { {
        new ProductData.Builder().name("Basket Ball")
            .searchKey("BB")
            .uOM("Unit")
            .productCategory("Distribution Goods")
            .taxCategory("VAT 10%")
            .build(),
        new PriceData.Builder().priceListVersion("Sport items")
            .standardPrice("28.50")
            .listPrice("28.50")
            .build(),
        new AUMData.Builder().uOM("Ounce")
            .conversionRate("2")
            .sales("Primary")
            .purchase("Primary")
            .logistics("Primary")
            .gtin("1234567890")
            .build(),
        new ProductData.Builder().name("Soccer Ball").searchKey("SB").build(),
        new ProductData.Builder().name("Tennis Ball").searchKey("TB").build() } };
    return Arrays.asList(data);
  }

  @Test
  public void AUMRTVa000test() throws ParseException {
    logger.debug("** Start test case [AUMRTVa000] Create returnable product **");

    ProductWindow productWindow = Product.ProductTab.open(mainPage);
    ProductTab productTab = productWindow.selectProductTab();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();

    productTab.filter(new ProductData.Builder().name("Basket Ball").searchKey("BB").build());
    int recordCount = productTab.getRecordCount();
    if (recordCount == 0) {
      productTab.clearFilters();
      mainPage.waitForDataToLoad();
      Sleep.trySleep();
      productTab.createRecord(productData);
      productTab.assertSaved();

      PriceTab priceTab = productWindow.selectPriceTab();
      priceTab.createRecord(productPrice);
      priceTab.assertSaved();

      AUMTab aumTab = productWindow.selectAUMTab();
      aumTab.createRecord(productAUM);
      aumTab.assertSaved();
    }

    ProductData setProductReturnable = new ProductData.Builder().build();
    setProductReturnable.addDataField("returnable", true);

    productTab.filter(productSB);
    productTab.editOnForm(setProductReturnable);
    productTab.assertSaved();

    productTab.filter(productTB);
    productTab.editOnForm(setProductReturnable);
    productTab.assertSaved();

    logger.debug("** End test case [AUMRTVa000] Create and configure a returnable product **");
  }
}
