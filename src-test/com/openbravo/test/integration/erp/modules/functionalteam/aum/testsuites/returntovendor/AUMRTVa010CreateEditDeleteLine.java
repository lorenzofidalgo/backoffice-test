/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <nonofce@gmail.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor;

import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorLineData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ReturnToVendorLineSelectorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendor;
import com.openbravo.test.integration.selenium.Sleep;

/**
 *
 * Class for Test AUMRTVa010
 *
 * @author Nono Carballo
 *
 */
@RunWith(Parameterized.class)
public class AUMRTVa010CreateEditDeleteLine extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Data for Test */

  GoodsReceiptHeaderData goodsReceiptHeader;
  GoodsReceiptLinesData goodsReceiptLine;
  ReturnToVendorHeaderData documentCreated;
  ReturnToVendorLineSelectorData linePicked;
  ReturnToVendorLineSelectorData linePickedEdit;
  ReturnToVendorLineData assertLinePicked;
  ReturnToVendorLineSelectorData lineEdited;
  ReturnToVendorLineData assertLineEdited;

  public AUMRTVa010CreateEditDeleteLine(GoodsReceiptHeaderData goodsReceiptHeader,
      GoodsReceiptLinesData goodsReceiptLine, ReturnToVendorHeaderData documentCreated,
      ReturnToVendorLineSelectorData linePicked, ReturnToVendorLineSelectorData linePickedEdit,
      ReturnToVendorLineData assertLinePicked, ReturnToVendorLineSelectorData lineEdited,
      ReturnToVendorLineData assertLineEdited) {
    super();
    this.goodsReceiptHeader = goodsReceiptHeader;
    this.goodsReceiptLine = goodsReceiptLine;
    this.documentCreated = documentCreated;
    this.linePicked = linePicked;
    this.linePickedEdit = linePickedEdit;
    this.assertLinePicked = assertLinePicked;
    this.lineEdited = lineEdited;
    this.assertLineEdited = assertLineEdited;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> returnToVendorValues() {
    Object[][] data = new Object[][] { {
        new GoodsReceiptHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .build(),
        new GoodsReceiptLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Basket Ball").build())
            .operativeQuantity("10")
            .storageBin("Y02")
            .build(),

        new ReturnToVendorHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .build(),
        new ReturnToVendorLineSelectorData.Builder().product("Basket Ball").build(),
        new ReturnToVendorLineSelectorData.Builder().returned("6")
            .returnedUOM("Ounce")
            .returnReason("Defective")
            .build(),
        new ReturnToVendorLineData.Builder().lineNo("10")
            .product("Basket Ball")
            .uOM("Unit")
            .unitPrice("28.50")
            .tax("VAT 10%")
            .returnReason("Defective")
            .lineNetAmount("342.00")
            .orderedQuantity("12")
            .operativeUOM("Ounce")
            .operativeQuantity("6")
            .build(),
        new ReturnToVendorLineSelectorData.Builder().returned("4")
            .returnedUOM("Ounce")
            .returnReason("Don't like it")
            .unitPrice("10.00")
            .build(),
        new ReturnToVendorLineData.Builder().product("Basket Ball")
            .uOM("Unit")
            .tax("VAT 10%")
            .returnReason("**Don't like it")
            .lineNetAmount("80.00")
            .orderedQuantity("8")
            .operativeUOM("Ounce")
            .operativeQuantity("4")
            .unitPrice("10.00")
            .build() } };
    return Arrays.asList(data);
  }

  @Test
  public void AUMRTVa010test() throws ParseException {
    logger.debug("** Start test case [AUMRTVa010] Create, edit and delete a line **");

    GoodsReceipt goodsReceipt = (GoodsReceipt) new GoodsReceipt(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt.create(goodsReceiptHeader);
    goodsReceipt.assertSaved();
    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.create(goodsReceiptLine);
    goodsReceiptLines.assertSaved();
    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();
    String documentNo = (String) goodsReceipt.getData("documentNo");
    linePicked.addDataField("shipmentNumber", documentNo);

    logger.debug("** Create a line **");
    ReturnToVendor returnToVendor = (ReturnToVendor) new ReturnToVendor(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnToVendor.create(documentCreated);
    returnToVendor.assertSaved();

    PickAndExecuteWindow<ReturnToVendorLineSelectorData> popup = returnToVendor
        .openPickAndEditLines();
    popup.filter(linePicked);
    popup.edit(linePickedEdit);
    popup.process();

    ReturnToVendor.Lines returnToVendorLines = returnToVendor.new Lines(mainPage);
    returnToVendorLines.assertData(assertLinePicked);
    String goodsShipmentLine = (String) returnToVendorLines.getData("goodsShipmentLine");
    assertTrue(!goodsShipmentLine.isEmpty());

    logger.debug("** Edit a line **");
    popup = returnToVendor.openPickAndEditLines();
    popup.edit(lineEdited);
    popup.process();
    Sleep.trySleep(5000);
    returnToVendorLines.assertData(assertLineEdited);
    goodsShipmentLine = (String) returnToVendorLines.getData("goodsShipmentLine");
    assertTrue(!goodsShipmentLine.isEmpty());

    logger.debug("** Delete a line **");
    popup = returnToVendor.openPickAndEditLines();
    popup.unselectAll();
    popup.process();
    returnToVendorLines.assertCount(0);

    goodsReceipt.open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt.select(new GoodsReceiptHeaderData.Builder()
        .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
        .documentNo(documentNo)
        .build());
    goodsReceipt.close();

    logger.debug("** End test case [AUMRTVa010] Create, edit and delete a line **");
  }

}
