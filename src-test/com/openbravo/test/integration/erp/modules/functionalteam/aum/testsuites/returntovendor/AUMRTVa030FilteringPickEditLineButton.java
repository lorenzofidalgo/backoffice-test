/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <nonofce@gmail.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor;

import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ReturnToVendorLineSelectorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendor;
import com.openbravo.test.integration.selenium.Sleep;

/**
 *
 * Class for Test AUMRTVa030
 *
 * @author Nono Carballo
 *
 */

@RunWith(Parameterized.class)
public class AUMRTVa030FilteringPickEditLineButton extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Data for Test */
  GoodsReceiptHeaderData goodsReceiptHeader;
  GoodsReceiptLinesData goodsReceiptLine;
  ReturnToVendorHeaderData documentCreated;
  ReturnToVendorLineSelectorData linePicked;
  ReturnToVendorLineSelectorData linePickedEdit;
  ReturnToVendorLineSelectorData linePicked2;

  public AUMRTVa030FilteringPickEditLineButton(GoodsReceiptHeaderData goodsReceiptHeader,
      GoodsReceiptLinesData goodsReceiptLine, ReturnToVendorHeaderData documentCreated,
      ReturnToVendorLineSelectorData linePicked, ReturnToVendorLineSelectorData linePickedEdit,
      ReturnToVendorLineSelectorData linePicked2) {
    super();
    this.goodsReceiptHeader = goodsReceiptHeader;
    this.goodsReceiptLine = goodsReceiptLine;
    this.documentCreated = documentCreated;
    this.linePicked = linePicked;
    this.linePickedEdit = linePickedEdit;
    this.linePicked2 = linePicked2;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> returnToVendorValues() {
    Object[][] data = new Object[][] { {
        new GoodsReceiptHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .build(),
        new GoodsReceiptLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Basket Ball").build())
            .operativeQuantity("10")
            .storageBin("Y02")
            .build(),
        new ReturnToVendorHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .build(),
        new ReturnToVendorLineSelectorData.Builder().product("Basket Ball").build(),
        new ReturnToVendorLineSelectorData.Builder().returned("4").returnedUOM("Ounce").build(),
        new ReturnToVendorLineSelectorData.Builder().shipmentNumber("10000002").build() } };
    return Arrays.asList(data);
  }

  @Test
  public void AUMRTVa030test() throws ParseException {
    logger.debug("** Start test case [AUMRTVa030] Filtering Pick/Edit line button **");

    GoodsReceipt goodsReceipt = (GoodsReceipt) new GoodsReceipt(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt.create(goodsReceiptHeader);
    goodsReceipt.assertSaved();
    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.create(goodsReceiptLine);
    goodsReceiptLines.assertSaved();
    goodsReceipt.complete();
    String documentNo = (String) goodsReceipt.getData("documentNo");
    linePicked.addDataField("shipmentNumber", documentNo);

    GoodsReceipt goodsReceipt2 = (GoodsReceipt) new GoodsReceipt(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt2.create(goodsReceiptHeader);
    goodsReceipt2.assertSaved();
    GoodsReceipt.Lines goodsReceiptLines2 = goodsReceipt2.new Lines(mainPage);
    goodsReceiptLines2.create(goodsReceiptLine);
    goodsReceiptLines2.assertSaved();
    goodsReceipt2.complete();
    String documentNo2 = (String) goodsReceipt2.getData("documentNo");
    linePicked2.addDataField("shipmentNumber", documentNo2);

    ReturnToVendor returnToVendor = (ReturnToVendor) new ReturnToVendor(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnToVendor.create(documentCreated);
    returnToVendor.assertSaved();

    PickAndExecuteWindow<ReturnToVendorLineSelectorData> popup = returnToVendor
        .openPickAndEditLines();
    popup.filter(linePicked);
    popup.edit(linePickedEdit);

    List<Map<String, Object>> rows = popup.getSelectedRows();
    assertTrue(rows.size() == 1);
    popup.process();

    popup = returnToVendor.openPickAndEditLines();
    Sleep.trySleep();
    popup.clearFilters();

    rows = popup.getSelectedRows();
    assertTrue(rows.size() == 1);
    assertTrue(((Long) rows.get(0).get("returned")).intValue() == 4);
    popup.filterAndKeepSelection(linePicked2);
    rows = popup.getRows();
    assertTrue(rows.size() == 2);
    assertTrue(((String) rows.get(0).get("shipmentNumber")).equals(documentNo));
    assertTrue(((String) rows.get(1).get("shipmentNumber")).equals(documentNo2));

    // Void the goods receipts since the stock created was not used...
    goodsReceipt.open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt.select(new GoodsReceiptHeaderData.Builder()
        .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
        .documentNo(documentNo)
        .build());
    goodsReceipt.close();
    goodsReceipt.select(new GoodsReceiptHeaderData.Builder()
        .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
        .documentNo(documentNo2)
        .build());
    goodsReceipt.close();

    logger.debug("** End test case [AUMRTVa030] Filtering Pick/Edit line button **");
  }
}
