/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2016-03-03 21:37:14
 * Contributor(s):
 *   Rafael Queralta <r.queralta@nectus.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor;

import static org.junit.Assert.assertNotNull;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentLineData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendorShipment;
import com.openbravo.test.integration.selenium.Sleep;

/**
 *
 * Class for Test AUMRTVb030 Validations Pick/Edit lines button
 *
 * @author Rafael Queralta
 *
 */
@RunWith(Parameterized.class)
public class AUMRTVb030ValidationsPickEditLinesButton extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager
      .getLogger();

  /** Data for Test */

  ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderData;
  ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderVerificationData;

  public AUMRTVb030ValidationsPickEditLinesButton(
      ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderData,
      ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderVerificationData) {
    this.returnToVendorShipmentHeaderData = returnToVendorShipmentHeaderData;
    this.returnToVendorShipmentHeaderVerificationData = returnToVendorShipmentHeaderVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> returnToVendorValues() {
    Object[][] data = new Object[][] { {
        new ReturnToVendorShipmentHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .warehouse("Spain warehouse")
            .build(),
        new ReturnToVendorShipmentHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Pamplona")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .documentType("RTV Shipment")
            .warehouse("Spain warehouse")
            .documentStatus("Draft")
            .build() } };
    logger.debug("Building parameters");
    return Arrays.asList(data);
  }

  @Test
  public void AUMRTVb030test() throws ParseException {
    logger.debug("** Start test case [AUMRTVb030] Validations Pick/Edit lines button **");

    final ReturnToVendorShipment returnToVendorShipment = (ReturnToVendorShipment) new ReturnToVendorShipment(
        mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnToVendorShipment.create(returnToVendorShipmentHeaderData);
    returnToVendorShipment.assertSaved();
    returnToVendorShipment.assertData(returnToVendorShipmentHeaderData);

    PickAndExecuteWindow<ReturnToVendorShipmentLineData> popUp = returnToVendorShipment
        .openPickAndEditLines();

    // Find a row with more Available QTY than Pending QTY
    popUp.clearFilters();
    ReturnToVendorShipmentLineData availableQtyFilterValue = null;
    ReturnToVendorShipmentLineData pendingQtyFilterValue = null;
    Long availableQty = 0L;
    Long pendingQty = 0L;

    List<Map<String, Object>> rows = popUp.getRows();
    for (int i = 0; i < rows.size()
        && (availableQtyFilterValue == null || pendingQtyFilterValue == null); i++) {
      Map<String, Object> row = rows.get(i);
      availableQty = (Long) row.get("availableQty");
      pendingQty = (Long) row.get("pending");
      if (pendingQty > availableQty + 1) {
        availableQtyFilterValue = new ReturnToVendorShipmentLineData.Builder()
            .returned(String.valueOf(row.get("returned")))
            .uOM(String.valueOf(row.get("returnedUOM$_identifier")))
            .storageBin(String.valueOf(row.get("storageBin$_identifier")))
            .lineNo(">0")
            .availableQty(String.valueOf(row.get("availableQty")))
            .operativeQuantity(String.valueOf(row.get("quantityInAlternativeUOM")))
            .build();
      }

      if (pendingQty + 1 < availableQty) {
        pendingQtyFilterValue = new ReturnToVendorShipmentLineData.Builder()
            .returned(String.valueOf(row.get("returned")))
            .uOM(String.valueOf(row.get("returnedUOM$_identifier")))
            .storageBin(String.valueOf(row.get("storageBin$_identifier")))
            .lineNo(">0")
            .pending(String.valueOf(row.get("pending")))
            .operativeQuantity(String.valueOf(row.get("quantityInAlternativeUOM")))
            .build();
      }
    }
    assertNotNull("The Available qty need to be less than Pending qty", availableQtyFilterValue);
    assertNotNull("The Pending qty need to be less than Available qty", pendingQtyFilterValue);

    availableQty = Long.parseLong((String) availableQtyFilterValue.getDataField("availableQty"));
    pendingQty = Long.parseLong((String) pendingQtyFilterValue.getDataField("pending"));

    popUp.filter(availableQtyFilterValue);
    popUp.unselectAll();
    popUp.selectRecord(0);
    popUp.edit(new ReturnToVendorShipmentLineData.Builder()
        .movementQuantity(String.valueOf(availableQty + 1))
        .build());
    popUp.process();
    popUp.assertError(String.format(
        "Ship Qty exceeds the Available Qty. Please enter a valid range: 0 - %s.", availableQty));

    popUp.filter(pendingQtyFilterValue);
    popUp.unselectAll();
    popUp.selectRecord(0);
    popUp.edit(new ReturnToVendorShipmentLineData.Builder()
        .movementQuantity(String.valueOf(pendingQty + 1))
        .build());
    popUp.process();
    popUp.assertError(String.format(
        "Ship Qty exceeds the Pending Qty. Please enter a valid range: 0 - %s.", pendingQty));
    popUp.cancel();

    logger.debug("** End test case [AUMRTVb030] Validations Pick/Edit lines button **");
  }

}
