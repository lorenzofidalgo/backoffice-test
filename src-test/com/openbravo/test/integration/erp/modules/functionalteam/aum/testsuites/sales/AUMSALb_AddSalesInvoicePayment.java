/**************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rafael Queralta Pozo <rafaelcuba81@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.sales;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test to add a payment to a sales invoice.
 *
 * @author Rafael Queralta Pozo
 */
@RunWith(Parameterized.class)
public class AUMSALb_AddSalesInvoicePayment extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /* Data for [SALAUMb010] Create Sales Invoice. */
  /** The Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /** The data to verify the creation of the Sales Invoice header. */
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  /** The Sales Invoice line data. */
  SalesInvoiceLinesData salesInvoiceLineData;
  /** The data to verify the creation of the Sales Invoice line. */
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  /** The data to verify the completion of the sales invoice. */
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  /** The data to verify the data in the add payment pop up. */
  AddPaymentPopUpData addPaymentInVerificationData;
  /** The data to verify the application of the payment. */
  SalesInvoiceHeaderData payedSalesInvoiceHeaderVerificationData;
  /** The data to verify the payment in plan data. */
  PaymentPlanData paymentInPlanData;
  /** The data to verify the payment in details data. */
  PaymentDetailsData paymentInDetailsData;

  /**
   * Class constructor.
   *
   * @param salesInvoiceHeaderData
   *          The Sales Invoice header data.
   * @param salesInvoiceHeaderVerificationData
   *          The data to verify the creation of the Sales Invoice header data.
   * @param salesInvoiceLineData
   *          The Sales Invoice line data.
   * @param salesInvoiceLineVerificationData
   *          The data to verify the creation of the Sales Invoice line.
   * @param completedSalesInvoiceHeaderVerificationData
   *          The data to verify the completion of the sales invoice.
   * @param addPaymentInVerificationData
   *          The data to verify the data in the add payment pop up
   * @param payedSalesInvoiceHeaderVerificationData
   *          The data to verify the application of the payment.
   * @param paymentInPlanData
   *          The data to verify the payment in plan data.
   * @param paymentInDetailsData
   *          The data to verify the payment in details data.
   */
  public AUMSALb_AddSalesInvoicePayment(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      AddPaymentPopUpData addPaymentInVerificationData,
      SalesInvoiceHeaderData payedSalesInvoiceHeaderVerificationData,
      PaymentPlanData paymentInPlanData, PaymentDetailsData paymentInDetailsData) {
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.addPaymentInVerificationData = addPaymentInVerificationData;
    this.payedSalesInvoiceHeaderVerificationData = payedSalesInvoiceHeaderVerificationData;
    this.paymentInPlanData = paymentInPlanData;
    this.paymentInDetailsData = paymentInDetailsData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        /* Parameters for [SALAUMb010] Create Sales Invoice. */
        new SalesInvoiceHeaderData.Builder().transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .invoiceDate(OBDate.CURRENT_DATE)
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("FGA")
                .priceListVersionName("Customer A")
                .build())
            .operativeQuantity("13.13")
            .build(),
        new SalesInvoiceLinesData.Builder().operativeUOM("Ounce")
            .unitPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("52.52")
            .build(),
        new SalesInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("54.36")
            .dueAmount("0.00")
            .paymentComplete(false)
            .documentStatus("Completed")
            .summedLineAmount("52.52")
            .grandTotalAmount("54.36")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .expected_payment("54.36")
            .actual_payment("54.36")
            .payment_date(OBDate.CURRENT_DATE)
            .transaction_type("Invoices")
            .build(),
        new SalesInvoiceHeaderData.Builder().paymentComplete(true)
            .documentStatus("Completed")
            .summedLineAmount("52.52")
            .grandTotalAmount("54.36")
            .currency("EUR")
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("1 (Spain)")
            .expected("54.36")
            .received("54.36")
            .outstanding("0.00")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),
        new PaymentDetailsData.Builder().paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .received("54.36")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test to add a payment to a sales invoice.
   */
  @Test
  public void salesInvoicePaymentShouldBeAdded() {
    logger.info("** Start of test case [SALAUMb010] Create Sales Invoice. **");
    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.create(salesInvoiceLineData);
    salesInvoiceLines.assertSaved();
    salesInvoiceLines.assertData(salesInvoiceLineVerificationData);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);
    salesInvoice.assertAddPaymentInData(addPaymentInVerificationData);
    salesInvoice.addPayment("54.36", "Process Received Payment(s)");
    // TODO see issue https://issues.openbravo.com/view.php?id=17291
    salesInvoice.assertPaymentCreatedSuccessfully();
    // TODO see issue 17038 - https://issues.openbravo.com/view.php?id=17038
    salesInvoice.assertData(payedSalesInvoiceHeaderVerificationData);
    SalesInvoice.PaymentInPlan paymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    paymentInPlan.assertCount(1);
    paymentInPlan.assertData(paymentInPlanData);
    SalesInvoice.PaymentInPlan.PaymentInDetails paymentInDetails = paymentInPlan.new PaymentInDetails(
        mainPage);
    paymentInDetails.assertCount(1);
    paymentInDetails.assertData(paymentInDetailsData);
    logger.info("** End of test case [SALAUMb010] Create Sales Invoice. **");
  }

}
