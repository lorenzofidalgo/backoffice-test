/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rafael Queralta Pozo <rafaelcuba81@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.sales;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.sales.transactions.returnfromcustomer.ReturnFromCustomerHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.ReturnFromCustomer;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test to create a sales order, and a goods receipt and sales invoice from that order.
 *
 * @author Rafael Queralta Pozo
 */
@RunWith(Parameterized.class)
public class AUMSALd_ReturnMaterial extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  /** Return From Customer Header Data */
  ReturnFromCustomerHeaderData returnFromCustomerHeaderData;

  /**
   * Class constructor.
   *
   * @param returnFromCustomerHeaderData
   *          .
   */
  public AUMSALd_ReturnMaterial(ReturnFromCustomerHeaderData returnFromCustomerHeaderData) {
    this.returnFromCustomerHeaderData = returnFromCustomerHeaderData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { { new ReturnFromCustomerHeaderData.Builder()
        .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
        .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test to create a sales order, and a goods receipt and sales invoice from that order.
   *
   * @throws ParseException
   */
  @Test
  public void goodsReceiptAndSalesInvoiceShouldBeCreatedFromOrder() throws ParseException {
    logger.info("** Start of test case [SALAUMd040] Create Return From Customer. **");
    ReturnFromCustomer returnFromCustomer = new ReturnFromCustomer(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnFromCustomer.create(returnFromCustomerHeaderData);
    returnFromCustomer.pickAndEditLines("50012");
  }

}
