/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.editablepayment.testsuites.EPPc_BasicFlow;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.generalsetup.processscheduling.processrequest.ProcessRequestData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.sessionpreferences.SessionPreferences;
import com.openbravo.test.integration.erp.testscripts.generalsetup.processscheduling.processrequest.ProcessRequest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * This test case posts a reconciliation.
 *
 * @author Pablo Luján <plu@openbravo.com>
 */

@RunWith(Parameterized.class)
public class EPPc_010CreateSalesInvoice extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /* Data for [FAMa_010] Post a reconciliation. */
  /** The sales invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /** The sales invoice lines data. */
  SalesInvoiceLinesData salesInvoiceLinesData;

  /**
   * Class constructor.
   *
   * @param salesInvoiceHeaderData
   *          The sales invoice header data. journalEntryLines Expected journal entry lines. Is an
   *          array of arrays. Each line has account number, name, debit and credit.
   */

  public EPPc_010CreateSalesInvoice(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceLinesData salesInvoiceLinesData) {

    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceLinesData = salesInvoiceLinesData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToCashValues() {

    Object[][] data = new Object[][] { {
        /* Parameters for [SALa030] Create Sales Invoice. */
        new SalesInvoiceHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentTerms("30-60-90")
            .paymentMethod("2.1 (Spain)")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A").build())
            .invoicedQuantity("100")
            .build() } };
    // new SalesInvoiceHeaderData.Builder().totalPaid("0.00").outstandingAmount("23.18")
    // .documentStatus("Completed").summedLineAmount("22.40").grandTotalAmount("23.18")
    // .currency("EUR").paymentComplete(false).build(),
    // new SalesInvoiceLinesData.Builder().product(
    // new ProductSelectorData.Builder().name("Final good A").build())
    // .invoicedQuantity("11.2").uOM("Bag").unitPrice("2.00").listPrice("2.00").tax(
    // "VAT(3)+CHARGE(0.5)").lineNetAmount("22.40").build(),
    // new PaymentPlanData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE).paymentMethod(
    // "1 (Spain)").expected("23.18").received("0.00").outstanding("23.18").build(),
    return Arrays.asList(data);
  }

  /**
   * Test case post a reconciliation.
   */
  @Test
  public void EPPc010_CreateSalesInvoice() {
    // logger.info("** Start of test case [FAMa_010] Post a reconciliation. **");
    //
    // final SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    // salesInvoice.select(salesInvoiceHeaderData);
    // if (salesInvoice.isPosted()) {
    // salesInvoice.unpost();
    // salesInvoice
    // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
    // journal entries = 2");
    // }
    // salesInvoice.post();
    // mainPage.loadView(new PostWindow());
    // Post post = new Post(mainPage);
    // post.assertJournalLinesCount(journalEntryLines.length);
    // post.assertJournalLines2(journalEntryLines);
    // final FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    // financialAccount.select(accountHeaderData);
    // FinancialAccount.Reconciliations reconciliations = financialAccount.new Reconciliations(
    // mainPage);
    // reconciliations.select(reconciliationsLinesData);
    // if (!reconciliations.isPosted()) {
    // reconciliations.unpost();
    // reconciliations
    // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
    // journal entries = 2");
    // }
    // reconciliations.post();
    // mainPage.loadView(new PostWindow());
    // new Post(mainPage);
    // post.assertJournalLinesCount(reconciliationJournalEntryLines.length);
    // post.assertJournalLines2(reconciliationJournalEntryLines);
    // logger.info("** End of test case [FAMa_010] Post a reconciliation. **");

    logger.info("** Start of test case [EPPa010] Create Sales Invoice. **");
    ProcessRequest processRequest = new ProcessRequest(mainPage).open();
    processRequest.select(new ProcessRequestData.Builder().process("Acct Server Process").build());
    if (processRequest.unscheduleProcess()) {
      processRequest.assertProcessUnscheduleSucceeded();
    }
    Sleep.trySleep();
    SessionPreferences sessionPreferences = new SessionPreferences(mainPage).open();
    sessionPreferences.checkShowAccountingTabs();
    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.create(salesInvoiceLinesData);
    salesInvoiceLines.assertSaved();
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    // TODO: Assert this (data object required)
    // salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);
    SalesInvoice.PaymentInPlan paymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    paymentInPlan.assertCount(3);
    // TODO: Assert the three lines values (data object required)
    // SalesInvoice.ArchivedPaymentInPlan archivedPaymentInPlan = salesInvoice.new
    // ArchivedPaymentInPlan(
    // mainPage);
    // archivedPaymentInPlan.assertCount(3);
    // TODO: Assert the three lines values (data object required)
    logger.info("** End of test case [EPPa010] Create Sales Invoice. **");

  }

}
