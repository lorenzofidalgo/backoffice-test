/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rafael Queralta <rafaelcuba81@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.PRO_Regressions;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.accounting.setup.accountingschema.AccountingSchemaData;
import com.openbravo.test.integration.erp.data.financial.accounting.setup.accountingschema.DefaultsData;
import com.openbravo.test.integration.erp.data.generalsetup.enterprise.organization.OrganizationData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.BusinessPartnerData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.VendorCreditorData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartnersetup.businesspartnercategory.BusinessPartnerCategoryData;
import com.openbravo.test.integration.erp.gui.financial.accounting.setup.generalledgerconfiguration.DefaultsTab;
import com.openbravo.test.integration.erp.gui.financial.accounting.setup.generalledgerconfiguration.GeneralLedgerConfigurationTab;
import com.openbravo.test.integration.erp.gui.financial.accounting.setup.generalledgerconfiguration.GeneralLedgerConfigurationWindow;
import com.openbravo.test.integration.erp.gui.masterdata.businesspartnersetup.businesspartnercategory.AccountingTab;
import com.openbravo.test.integration.erp.gui.masterdata.businesspartnersetup.businesspartnercategory.BusinessPartnerCategoryTab;
import com.openbravo.test.integration.erp.gui.masterdata.businesspartnersetup.businesspartnercategory.BusinessPartnerCategoryWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.generalsetup.enterprise.Organization;
import com.openbravo.test.integration.erp.testscripts.masterdata.businesspartner.BusinessPartner;
import com.openbravo.test.integration.util.UUIDs;

/**
 * Test regression 33877
 *
 * @author rqueralta
 */
@RunWith(Parameterized.class)
public class PRORegression33877 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();
  private static String randomName = UUIDs.getRandomDatabaseUUID().substring(0, 15);

  /* Data for this test. */
  OrganizationData organizationData1;
  OrganizationData verifyOrganizationData1;
  OrganizationData organizationData2;
  OrganizationData verifyOrganizationData2;
  OrganizationData organizationData3;
  OrganizationData verifyOrganizationData3;
  OrganizationData organizationData4;
  OrganizationData verifyOrganizationData4;
  OrganizationData organizationData5;
  OrganizationData verifyOrganizationData5;
  OrganizationData organizationData6;
  OrganizationData verifyOrganizationData6;
  OrganizationData organizationData7;
  OrganizationData verifyOrganizationData7;

  /* Business Partner Category Data */
  BusinessPartnerCategoryData businessPartnerCategoryData1;
  BusinessPartnerCategoryData businessPartnerCategoryData2;
  BusinessPartnerCategoryData businessPartnerCategoryData3;
  BusinessPartnerCategoryData businessPartnerCategoryData4;

  /* Business Partner Data */
  BusinessPartnerData businessPartnerData1;
  BusinessPartnerData businessPartnerData2;
  BusinessPartnerData businessPartnerData3;
  BusinessPartnerData businessPartnerData4;
  BusinessPartnerData businessPartnerData5;

  /**
   * Class constructor.
   *
   */
  public PRORegression33877(OrganizationData organizationData1,
      OrganizationData verifyOrganizationData1, OrganizationData organizationData2,
      OrganizationData verifyOrganizationData2, OrganizationData organizationData3,
      OrganizationData verifyOrganizationData3, OrganizationData organizationData4,
      OrganizationData verifyOrganizationData4, OrganizationData organizationData5,
      OrganizationData verifyOrganizationData5, OrganizationData organizationData6,
      OrganizationData verifyOrganizationData6, OrganizationData organizationData7,
      OrganizationData verifyOrganizationData7,
      BusinessPartnerCategoryData businessPartnerCategoryData1,
      BusinessPartnerCategoryData businessPartnerCategoryData2,
      BusinessPartnerCategoryData businessPartnerCategoryData3,
      BusinessPartnerCategoryData businessPartnerCategoryData4,
      BusinessPartnerData businessPartnerData1, BusinessPartnerData businessPartnerData2,
      BusinessPartnerData businessPartnerData3, BusinessPartnerData businessPartnerData4,
      BusinessPartnerData businessPartnerData5) {
    this.organizationData1 = organizationData1;
    this.verifyOrganizationData1 = verifyOrganizationData1;
    this.organizationData2 = organizationData2;
    this.verifyOrganizationData2 = verifyOrganizationData2;
    this.organizationData3 = organizationData3;
    this.verifyOrganizationData3 = verifyOrganizationData3;
    this.organizationData4 = organizationData4;
    this.verifyOrganizationData4 = verifyOrganizationData4;
    this.organizationData5 = organizationData5;
    this.verifyOrganizationData5 = verifyOrganizationData5;
    this.organizationData6 = organizationData6;
    this.verifyOrganizationData6 = verifyOrganizationData6;
    this.organizationData7 = organizationData7;
    this.verifyOrganizationData7 = verifyOrganizationData7;
    this.businessPartnerCategoryData1 = businessPartnerCategoryData1;
    this.businessPartnerCategoryData2 = businessPartnerCategoryData2;
    this.businessPartnerCategoryData3 = businessPartnerCategoryData3;
    this.businessPartnerCategoryData4 = businessPartnerCategoryData4;
    this.businessPartnerData1 = businessPartnerData1;
    this.businessPartnerData2 = businessPartnerData2;
    this.businessPartnerData3 = businessPartnerData3;
    this.businessPartnerData4 = businessPartnerData4;
    this.businessPartnerData5 = businessPartnerData5;

    logInData = new LogInData.Builder().userName("Openbravo").password("openbravo").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> testValues() {
    Object[][] data = new Object[][] {
        { new OrganizationData.Builder().searchKey("International").build(),
            new OrganizationData.Builder().generalLedger("F&B International Group US/A/US Dollar")
                .build(),
            new OrganizationData.Builder().searchKey("US,").build(),
            new OrganizationData.Builder().generalLedger("").build(),
            new OrganizationData.Builder().searchKey("East").build(),
            new OrganizationData.Builder().generalLedger("").build(),
            new OrganizationData.Builder().searchKey("West").build(),
            new OrganizationData.Builder().generalLedger("").build(),
            new OrganizationData.Builder().searchKey("España,").build(),
            new OrganizationData.Builder().generalLedger("F&B España, S.A. US/A/Euro").build(),
            new OrganizationData.Builder().searchKey("Norte").build(),
            new OrganizationData.Builder().generalLedger("").build(),
            new OrganizationData.Builder().searchKey("Sur").build(),
            new OrganizationData.Builder().generalLedger("").build(),
            new BusinessPartnerCategoryData.Builder().organization("F&B España, S.A")
                .searchKey("España-Vendor-" + randomName)
                .name("España-Vendor-" + randomName)
                .build(),
            new BusinessPartnerCategoryData.Builder().organization("F&B US, Inc.")
                .searchKey("US-Vendor-" + randomName)
                .name("US-Vendor-" + randomName)
                .build(),
            new BusinessPartnerCategoryData.Builder().organization("*")
                .searchKey("Head-Vendor-" + randomName)
                .name("Head-Vendor-" + randomName)
                .build(),
            new BusinessPartnerCategoryData.Builder().organization("F&B International Group")
                .searchKey("VT1-" + randomName)
                .name("VT1-" + randomName)
                .build(),
            new BusinessPartnerData.Builder().organization("*")
                .searchKey("Head-Vendor-" + randomName)
                .name("Head Vendor-" + randomName)
                .businessPartnerCategory("Head-Vendor-" + randomName)
                .build(),
            new BusinessPartnerData.Builder().organization("*")
                .searchKey("US-Vendor-" + randomName)
                .name("US Vendor-" + randomName)
                .businessPartnerCategory("US-Vendor-" + randomName)
                .build(),
            new BusinessPartnerData.Builder().organization("F&B International Group")
                .searchKey("VT1-" + randomName)
                .name("VT1-" + randomName)
                .businessPartnerCategory("VT1-" + randomName)
                .build(),
            new BusinessPartnerData.Builder().organization("F&B España - Región Norte")
                .searchKey("EspañaNorte-Vendor-" + randomName)
                .name("España Norte Vendor-" + randomName)
                .businessPartnerCategory("España-Vendor-" + randomName)
                .build(),
            new BusinessPartnerData.Builder().organization("F&B US East Coast")
                .searchKey("EastCoast-Vendor-" + randomName)
                .name("East Coast Vendor-" + randomName)
                .businessPartnerCategory("US-Vendor-" + randomName)
                .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 33877 - Accounting default values not transferred from BP Category to Business
   * Partner
   *
   * @throws ParseException
   */
  @Test
  public void PRORegression33877Test() throws ParseException, OpenbravoERPTestException {
    logger.info(
        "** Start of test case [SALRegression33877] Test regression 33877 - Accounting default values not transferred from BP Category to Business Partner. **");

    Organization organization = new Organization(mainPage).open();
    organization.select(organizationData1);
    organization.assertData(verifyOrganizationData1);
    organization.select(organizationData2);
    organization.assertData(verifyOrganizationData2);
    organization.select(organizationData3);
    organization.assertData(verifyOrganizationData3);
    organization.select(organizationData4);
    organization.assertData(verifyOrganizationData4);
    organization.select(organizationData5);
    organization.assertData(verifyOrganizationData5);
    organization.select(organizationData6);
    organization.assertData(verifyOrganizationData6);
    organization.select(organizationData7);
    organization.assertData(verifyOrganizationData7);

    GeneralLedgerConfigurationWindow generalLedgerConfigurationWindow = new GeneralLedgerConfigurationWindow();
    mainPage.openView(generalLedgerConfigurationWindow);
    GeneralLedgerConfigurationTab generalLedgerConfigurationTab = generalLedgerConfigurationWindow
        .selectGeneralLedgerConfigurationTab();
    generalLedgerConfigurationTab.select(new AccountingSchemaData.Builder().name("España").build());
    DefaultsTab defaultsTab = generalLedgerConfigurationWindow.selectDefaultsTab();
    defaultsTab.assertData(new DefaultsData.Builder()
        .customerReceivablesNo("43000 - Clientes (euros) a corto plazo")
        .vendorLiability("40000 - Proveedores (euros) a corto plazo")
        .fixedAsset("35000 - Productos terminados A")
        .productExpense("60000 - Compras de mercaderías")
        .productRevenue("70000 - Ventas de mercaderías")
        .productCOGS("99900 - Coste de productos vendidos")
        .warehouseDifferences("61000 - Variación de existencias de mercaderías")
        .bankAsset("57200 - Bancos e instituciones de crédito c/c vista euros")
        .bankInTransit("55500 - Partidas pendientes de aplicación")
        .bankExpense("62600 - Servicios bancarios y similares")
        .bankRevaluationGain("76800 - Diferencias positivas de cambio")
        .bankRevaluationLoss("66800 - Diferencias negativas de cambio")
        .taxDue("47700 - Hacienda Pública IVA repercutido")
        .taxCredit("47200 - Hacienda Pública IVA soportado")
        .cashBookAsset("57000 - Caja euros")
        .cashBookDifferences("65000 -  Pérdidas de créditos comerciales incobrables")
        .cashTransfer("57001 - Caja euros")
        .depreciation("68200 - Amortización de las inversiones inmobiliarias")
        .accumulatedDepreciation("28200 - Amortización acumulada de las inversiones inmobiliarias")
        .build());

    generalLedgerConfigurationTab
        .select(new AccountingSchemaData.Builder().name("International").build());
    defaultsTab = generalLedgerConfigurationWindow.selectDefaultsTab();
    defaultsTab
        .assertData(new DefaultsData.Builder().customerReceivablesNo("1130 - Accounts Receivable")
            .vendorLiability("2110 - Accounts Payables")
            .fixedAsset("1520 - Inventory Asset")
            .productExpense("5360 - Product Expense")
            .productRevenue("4100 - Revenue - Product")
            .productCOGS("5100 - Product Costs")
            .warehouseDifferences("5350 - Inventory Adjustment")
            .bankAsset("1110 - Checking Account")
            .bankInTransit("1140 - Checking In-Transfer")
            .bankExpense("6020 - Bank Service Charges")
            .bankRevaluationGain("8050 - Bank revaluation gain")
            .bankRevaluationLoss("8251 - Bank revaluation loss")
            .taxDue("2260 - Sales Tax Payable")
            .taxCredit("7410 - Tax expense")
            .cashBookAsset("1190 - Petty Cash")
            .cashBookDifferences("7830 - Petty Cash Over/Short")
            .cashTransfer("1191 - Petty Cash In-Transfer")
            .depreciation("6080 - Depreciation Expense")
            .accumulatedDepreciation("1800 - Accumulated Depreciation")
            .build());

    BusinessPartnerCategoryWindow businessPartnerCategoryWindow = new BusinessPartnerCategoryWindow();
    mainPage.openView(businessPartnerCategoryWindow);
    BusinessPartnerCategoryTab businessPartnerCategory = businessPartnerCategoryWindow
        .selectBusinessPartnerCategoryTab();
    businessPartnerCategory.createOnForm(businessPartnerCategoryData1);
    AccountingTab accountingTab = businessPartnerCategoryWindow.selectAccountingTab();
    accountingTab.assertCount(2);
    businessPartnerCategory.createOnForm(businessPartnerCategoryData2);
    accountingTab = businessPartnerCategoryWindow.selectAccountingTab();
    accountingTab.assertCount(1);
    businessPartnerCategory.createOnForm(businessPartnerCategoryData3);
    accountingTab = businessPartnerCategoryWindow.selectAccountingTab();
    accountingTab.assertCount(2);
    businessPartnerCategory.createOnForm(businessPartnerCategoryData4);
    accountingTab = businessPartnerCategoryWindow.selectAccountingTab();
    accountingTab.assertCount(2);

    BusinessPartner.BusinessPartnerTab.create(mainPage, businessPartnerData1);
    BusinessPartner.VendorCreditor.edit(mainPage,
        new VendorCreditorData.Builder().vendor(true).build());
    BusinessPartner.VendorAccountingTab.verifyExpectedCount(mainPage, 2);
    BusinessPartner.BusinessPartnerTab.create(mainPage, businessPartnerData2);
    BusinessPartner.VendorCreditor.edit(mainPage,
        new VendorCreditorData.Builder().vendor(true).build());
    BusinessPartner.VendorAccountingTab.verifyExpectedCount(mainPage, 1);
    BusinessPartner.BusinessPartnerTab.create(mainPage, businessPartnerData3);
    BusinessPartner.VendorCreditor.edit(mainPage,
        new VendorCreditorData.Builder().vendor(true).build());
    BusinessPartner.VendorAccountingTab.verifyExpectedCount(mainPage, 2);
    BusinessPartner.BusinessPartnerTab.create(mainPage, businessPartnerData4);
    BusinessPartner.VendorCreditor.edit(mainPage,
        new VendorCreditorData.Builder().vendor(true).build());
    BusinessPartner.VendorAccountingTab.verifyExpectedCount(mainPage, 2);
    BusinessPartner.BusinessPartnerTab.create(mainPage, businessPartnerData5);
    BusinessPartner.VendorCreditor.edit(mainPage,
        new VendorCreditorData.Builder().vendor(true).build());
    BusinessPartner.VendorAccountingTab.verifyExpectedCount(mainPage, 1);

    logger.info(
        "** End of test case [SALRegression33877] Test regression 33877 - Accounting default values not transferred from BP Category to Business Partner. **");
  }
}
