/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.copyfromorder;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;

/**
 * Testing Copy From Multiple Orders
 *
 * @author Mark
 *
 */

@RunWith(Parameterized.class)
public class CFPO_CopyFromMultipleOrders extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  // Purchase Order Copied from 1
  PurchaseOrderHeaderData purchaseOrderHeaderFromData1;
  PurchaseOrderHeaderData purchaseOrderHeaderFromVerficationData1;
  PurchaseOrderLinesData purchaseOrderFirstLineData1;
  PurchaseOrderLinesData purchaseOrderFirstLineVerificationData1;
  PurchaseOrderLinesData purchaseOrderSecondLineData1;
  PurchaseOrderLinesData purchaseOrderSecondLineVerificationData1;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderFromVerificationData1;

  // Purchase Order Copied from 2
  PurchaseOrderHeaderData purchaseOrderHeaderFromData2;
  PurchaseOrderHeaderData purchaseOrderHeaderFromVerficationData2;
  PurchaseOrderLinesData purchaseOrderFirstLineData2;
  PurchaseOrderLinesData purchaseOrderFirstLineVerificationData2;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderFromVerificationData2;

  // Purchase Order Copied To
  PurchaseOrderHeaderData purchaseOrderHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  PurchaseOrderLinesData purchaseOrderFirstLineVerificationData;
  PurchaseOrderLinesData purchaseOrderSecondLineVerificationData;
  PurchaseOrderLinesData purchaseOrderThirdLineVerificationData;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;

  /**
   * Class constructor.
   *
   */
  public CFPO_CopyFromMultipleOrders(PurchaseOrderHeaderData purchaseOrderHeaderFromData1,
      PurchaseOrderHeaderData purchaseOrderHeaderFromVerficationData1,
      PurchaseOrderLinesData purchaseOrderFirstLineData1,
      PurchaseOrderLinesData purchaseOrderFirstLineVerificationData1,
      PurchaseOrderLinesData purchaseOrderSecondLineData1,
      PurchaseOrderLinesData purchaseOrderSecondLineVerificationData1,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderFromVerificationData1,
      PurchaseOrderHeaderData purchaseOrderHeaderFromData2,
      PurchaseOrderHeaderData purchaseOrderHeaderFromVerficationData2,
      PurchaseOrderLinesData purchaseOrderFirstLineData2,
      PurchaseOrderLinesData purchaseOrderFirstLineVerificationData2,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderFromVerificationData2,
      PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderFirstLineVerificationData,
      PurchaseOrderLinesData purchaseOrderSecondLineVerificationData,
      PurchaseOrderLinesData purchaseOrderThirdLineVerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData) {
    this.purchaseOrderHeaderFromData1 = purchaseOrderHeaderFromData1;
    this.purchaseOrderHeaderFromVerficationData1 = purchaseOrderHeaderFromVerficationData1;
    this.purchaseOrderFirstLineData1 = purchaseOrderFirstLineData1;
    this.purchaseOrderFirstLineVerificationData1 = purchaseOrderFirstLineVerificationData1;
    this.purchaseOrderSecondLineData1 = purchaseOrderSecondLineData1;
    this.purchaseOrderSecondLineVerificationData1 = purchaseOrderSecondLineVerificationData1;
    this.bookedPurchaseOrderHeaderFromVerificationData1 = bookedPurchaseOrderHeaderFromVerificationData1;

    this.purchaseOrderHeaderFromData2 = purchaseOrderHeaderFromData2;
    this.purchaseOrderHeaderFromVerficationData2 = purchaseOrderHeaderFromVerficationData2;
    this.purchaseOrderFirstLineData2 = purchaseOrderFirstLineData2;
    this.purchaseOrderFirstLineVerificationData2 = purchaseOrderFirstLineVerificationData2;
    this.bookedPurchaseOrderHeaderFromVerificationData2 = bookedPurchaseOrderHeaderFromVerificationData2;

    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderFirstLineVerificationData = purchaseOrderFirstLineVerificationData;
    this.purchaseOrderSecondLineVerificationData = purchaseOrderSecondLineVerificationData;
    this.purchaseOrderThirdLineVerificationData = purchaseOrderThirdLineVerificationData;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> PurchaseOrderValues() {
    Object[][] data = new Object[][] { {
        // purchaseOrderHeaderFromData1
        new PurchaseOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("1 (USA)")
            .paymentTerms("90 days")
            .build(),
        // purchaseOrderHeaderFromVerficationData1
        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Purchase")
            .paymentMethod("1 (USA)")
            .build(),
        // purchaseOrderFirstLineData1
        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMA").priceListVersion("Purchase").build())
            .orderedQuantity("10")
            .tax("Exempt 10%")
            .build(),
        // purchaseOrderFirstLineVerificationData1
        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),
        // purchaseOrderSecondLineData1
        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMB").priceListVersion("Purchase").build())
            .orderedQuantity("20")
            .tax("Exempt 10%")
            .build(),
        // purchaseOrderSecondLineVerificationData1
        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("40.00")
            .build(),
        // bookedPurchaseOrderHeaderFromVerificationData1
        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("60.00")
            .grandTotalAmount("60.00")
            .currency("EUR")
            .build(),

        // purchaseOrderHeaderFromData2
        new PurchaseOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor B").build())
            .paymentMethod("1 (USA)")
            .paymentTerms("30 days, 5")
            .build(),
        // purchaseOrderHeaderFromVerficationData2
        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº12")
            .warehouse("USA warehouse")
            .paymentTerms("30 days, 5")
            .priceList("Purchase")
            .paymentMethod("1 (USA)")
            .build(),
        // purchaseOrderFirstLineData2
        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMC").priceListVersion("Purchase").build())
            .orderedQuantity("30")
            .tax("Exempt 10%")
            .build(),
        // purchaseOrderFirstLineVerificationData2
        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("60.00")
            .build(),

        // bookedPurchaseOrderHeaderFromVerificationData2
        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("60.00")
            .grandTotalAmount("60.00")
            .currency("EUR")
            .build(),

        // purchaseOrderHeaderData
        new PurchaseOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("1 (USA)")
            .paymentTerms("90 days")
            .build(),
        // purchaseOrderHeaderVerficationData
        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Purchase")
            .paymentMethod("1 (USA)")
            .build(),
        // purchaseOrderFirstLineVerificationData
        new PurchaseOrderLinesData.Builder().orderedQuantity("10")
            .tax("Exempt 10%")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),
        // purchaseOrderSecondLineVerificationData
        new PurchaseOrderLinesData.Builder().orderedQuantity("20")
            .tax("Exempt 10%")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("40.00")
            .build(),
        // purchaseOrderThirdLineVerificationData
        new PurchaseOrderLinesData.Builder().orderedQuantity("30")
            .tax("Exempt 10%")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("60.00")
            .build(),
        // bookedPurchaseOrderHeaderVerificationData
        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("120.00")
            .grandTotalAmount("120.00")
            .currency("EUR")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test copy lines from multiple orders
   */
  @Test
  public void CFO_CopyFromMultipleOrdersTest() {
    logger.info(
        "** Start of test case [CFPO_CopyFromMultipleOrders]. Copy lines from multiple orders **");

    /** Register the first Sales order will be copied from */
    PurchaseOrder orderFrom1 = new PurchaseOrder(mainPage).open();
    orderFrom1.create(purchaseOrderHeaderFromData1);
    orderFrom1.assertSaved();
    orderFrom1.assertData(purchaseOrderHeaderFromVerficationData1);
    String orderNoFrom1 = orderFrom1.getData("documentNo").toString();

    // Create order lines
    PurchaseOrder.Lines orderLinesFrom = orderFrom1.new Lines(mainPage);
    orderLinesFrom.create(purchaseOrderFirstLineData1);
    orderLinesFrom.assertSaved();
    orderLinesFrom.assertData(purchaseOrderFirstLineVerificationData1);

    orderLinesFrom.create(purchaseOrderSecondLineData1);
    orderLinesFrom.assertSaved();
    orderLinesFrom.assertData(purchaseOrderSecondLineVerificationData1);

    // Book the order
    orderFrom1.book();
    orderFrom1.assertProcessCompletedSuccessfully2();
    orderFrom1.assertData(bookedPurchaseOrderHeaderFromVerificationData1);

    /** Register the second Sales order will be copied from */
    PurchaseOrder orderFrom2 = new PurchaseOrder(mainPage).open();
    orderFrom2.create(purchaseOrderHeaderFromData2);
    orderFrom2.assertSaved();
    orderFrom2.assertData(purchaseOrderHeaderFromVerficationData2);
    String orderNoFrom2 = orderFrom2.getData("documentNo").toString();

    // Create order lines
    PurchaseOrder.Lines orderLinesFrom2 = orderFrom2.new Lines(mainPage);
    orderLinesFrom2.create(purchaseOrderFirstLineData2);
    orderLinesFrom2.assertSaved();
    orderLinesFrom2.assertData(purchaseOrderFirstLineVerificationData2);

    // Book the order
    orderFrom2.book();
    orderFrom2.assertProcessCompletedSuccessfully2();
    orderFrom2.assertData(bookedPurchaseOrderHeaderFromVerificationData2);

    /** Register a Sales order and copy from the previously order */
    PurchaseOrder order = new PurchaseOrder(mainPage).open();
    order.create(purchaseOrderHeaderData);
    order.assertSaved();
    order.assertData(purchaseOrderHeaderVerficationData);

    PickAndExecuteWindow<PurchaseOrderHeaderData> popup = order.copyFromOrders();
    popup.filterAndKeepSelection(
        new PurchaseOrderHeaderData.Builder().documentNo(orderNoFrom1).build());
    popup.filterAndKeepSelection(
        new PurchaseOrderHeaderData.Builder().documentNo(orderNoFrom2).build());
    List<Map<String, Object>> rows = popup.getSelectedRows();
    assertTrue(rows.size() == 2);
    popup.process();

    PurchaseOrder.Lines orderLines = order.new Lines(mainPage);
    orderLines.assertCount(3);
    orderLines.selectWithoutFiltering(0);
    orderLines.assertData(purchaseOrderFirstLineVerificationData);
    orderLines.selectWithoutFiltering(1);
    orderLines.assertData(purchaseOrderSecondLineVerificationData);
    orderLines.selectWithoutFiltering(2);
    orderLines.assertData(purchaseOrderThirdLineVerificationData);

    // Book the order
    order.book();
    order.assertProcessCompletedSuccessfully2();
    order.assertData(bookedPurchaseOrderHeaderVerificationData);

    logger.info(
        "** End of test case [CFPO_CopyFromMultipleOrders]. Copy lines from multiple orders**");
  }
}
