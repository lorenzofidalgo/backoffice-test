/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.createlinesfrom.createlinesfrom2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;

/**
 * Test Create Lines From Purchase Order Partially Received
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class CLF_PurchaseOrderPartiallyReceived extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PurchaseOrderHeaderData purchaseOrderHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  PurchaseOrderLinesData purchaseOrderLinesData;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  PurchaseOrderLinesData purchaseOrderLinesData2;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData2;
  PurchaseOrderLinesData purchaseOrderLinesData3;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData3;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;

  GoodsReceiptHeaderData goodsReceiptHeaderData;
  GoodsReceiptHeaderData goodsReceiptHeaderVerificationData;
  GoodsReceiptLinesData goodsReceiptLineVerificationData;
  GoodsReceiptLinesData goodsReceiptLineVerificationData2;
  GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData;

  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData2;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData3;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;

  /**
   * Class constructor.
   *
   */
  public CLF_PurchaseOrderPartiallyReceived(PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      PurchaseOrderLinesData purchaseOrderLinesData2,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData2,
      PurchaseOrderLinesData purchaseOrderLinesData3,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData3,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptHeaderData goodsReceiptHeaderVerificationData,
      GoodsReceiptLinesData goodsReceiptLineVerificationData,
      GoodsReceiptLinesData goodsReceiptLineVerificationData2,
      GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData2,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData3,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData) {
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.purchaseOrderLinesData2 = purchaseOrderLinesData2;
    this.purchaseOrderLinesVerificationData2 = purchaseOrderLinesVerificationData2;
    this.purchaseOrderLinesData3 = purchaseOrderLinesData3;
    this.purchaseOrderLinesVerificationData3 = purchaseOrderLinesVerificationData3;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptLineVerificationData = goodsReceiptLineVerificationData;
    this.goodsReceiptLineVerificationData2 = goodsReceiptLineVerificationData2;
    this.goodsReceiptHeaderVerificationData = goodsReceiptHeaderVerificationData;
    this.completedGoodsReceiptHeaderVerificationData = completedGoodsReceiptHeaderVerificationData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.purchaseInvoiceLineVerificationData2 = purchaseInvoiceLineVerificationData2;
    this.purchaseInvoiceLineVerificationData3 = purchaseInvoiceLineVerificationData3;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderValues() {
    Object[][] data = new Object[][] { {
        new PurchaseOrderHeaderData.Builder().organization("Spain")
            .transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("1 (Spain)")
            .build(),
        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("Spain warehouse")
            .priceList("Purchase")
            .paymentTerms("90 days")
            .build(),
        new PurchaseOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().searchKey("RMA")
                .priceListVersion("Purchase")
                .warehouse("Spain warehouse")
                .build())
            .orderedQuantity("10")
            .tax("VAT 10%")
            .build(),
        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),

        new PurchaseOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().searchKey("RMB")
                .priceListVersion("Purchase")
                .warehouse("Spain warehouse")
                .build())
            .orderedQuantity("15")
            .tax("VAT 10%")
            .build(),
        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("30.00")
            .build(),

        new PurchaseOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().searchKey("RMC")
                .priceListVersion("Purchase")
                .warehouse("Spain warehouse")
                .build())
            .orderedQuantity("20")
            .tax("VAT 10%")
            .build(),
        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("40.00")
            .build(),

        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("90.00")
            .grandTotalAmount("99.00")
            .currency("EUR")
            .build(),

        new GoodsReceiptHeaderData.Builder().organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .warehouse("Spain warehouse")
            .build(),
        new GoodsReceiptHeaderData.Builder().documentType("MM Receipt")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("Spain warehouse")
            .build(),
        new GoodsReceiptLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Raw material A").build())
            .movementQuantity("10")
            .uOM("Bag")
            .storageBin("L01")
            .attributeSetValue("#7890")
            .organization("Spain")
            .build(),
        new GoodsReceiptLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Raw material B").build())
            .movementQuantity("8")
            .uOM("Bag")
            .storageBin("L01")
            .attributeSetValue("#7890")
            .organization("Spain")
            .build(),
        new GoodsReceiptHeaderData.Builder().documentStatus("Completed").build(),

        new PurchaseInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("1 (Spain)")
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .priceList("Purchase")
            .paymentTerms("90 days")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material A").build())
            .invoicedQuantity("10")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("20.00")
            .organization("Spain")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material B").build())
            .invoicedQuantity("8")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("16.00")
            .organization("Spain")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material C").build())
            .invoicedQuantity("20")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("40.00")
            .organization("Spain")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("83.60")
            .summedLineAmount("76.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(), } };
    return Arrays.asList(data);
  }

  /**
   * Test Create Lines From Purchase Order Partially Receipt
   *
   * @throws ParseException
   */
  @Test
  public void CLF_PurchaseOrderPartiallyReceiptTest() throws ParseException {
    logger.info(
        "** Start of test case [CLF_PurchaseOrderPartiallyReceiptTest] Test Create Lines From Purchase Order Partially Receipt **");

    PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerficationData);

    String orderNo = purchaseOrder.getData("documentNo").toString();
    bookedPurchaseOrderHeaderVerificationData.addDataField("documentNo", orderNo);

    PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);

    purchaseOrderLines.create(purchaseOrderLinesData2);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData2);

    purchaseOrderLines.create(purchaseOrderLinesData3);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData3);

    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);

    String purchaseOrderIdentifier = String.format("%s - %s - %s", orderNo,
        purchaseOrder.getData("orderDate"),
        bookedPurchaseOrderHeaderVerificationData.getDataField("grandTotalAmount"));

    String purchaseOrderLineIdentifier1 = String.format("%s - %s - %s - %s - %s", orderNo,
        purchaseOrder.getData("orderDate"),
        bookedPurchaseOrderHeaderVerificationData.getDataField("grandTotalAmount"), "10",
        purchaseOrderLinesVerificationData.getDataField("lineNetAmount"));

    String purchaseOrderLineIdentifier2 = String.format("%s - %s - %s - %s - %s", orderNo,
        purchaseOrder.getData("orderDate"),
        bookedPurchaseOrderHeaderVerificationData.getDataField("grandTotalAmount"), "20",
        purchaseOrderLinesVerificationData2.getDataField("lineNetAmount"));

    String purchaseOrderLineIdentifier3 = String.format("%s - %s - %s - %s - %s", orderNo,
        purchaseOrder.getData("orderDate"),
        bookedPurchaseOrderHeaderVerificationData.getDataField("grandTotalAmount"), "30",
        purchaseOrderLinesVerificationData3.getDataField("lineNetAmount"));

    // Partially Deliver the order
    completedGoodsReceiptHeaderVerificationData.addDataField("salesOrder", purchaseOrderIdentifier);
    goodsReceiptLineVerificationData.addDataField("salesOrderLine", purchaseOrderLineIdentifier1);
    goodsReceiptLineVerificationData2.addDataField("salesOrderLine", purchaseOrderLineIdentifier2);

    GoodsReceipt goodsReceipt = new GoodsReceipt(mainPage).open();
    goodsReceipt.create(goodsReceiptHeaderData);
    goodsReceipt.assertSaved();
    goodsReceipt.assertData(goodsReceiptHeaderVerificationData);
    goodsReceipt.createLinesFrom("L01", purchaseOrderIdentifier, "7890", 1);
    goodsReceipt.assertProcessCompletedSuccessfully();
    goodsReceipt.createLinesFrom("L01", purchaseOrderIdentifier, "7890", 2);
    goodsReceipt.assertProcessCompletedSuccessfully();

    String goodsReceiptNo = goodsReceipt.getData("documentNo").toString();
    completedGoodsReceiptHeaderVerificationData.addDataField("documentNo", goodsReceiptNo);

    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.assertCount(2);

    goodsReceiptLines.selectWithoutFiltering(0);
    goodsReceiptLines.assertData(goodsReceiptLineVerificationData);

    String goodsReceiptLineIdentifier = String.format("%s - %s - %s - %s - %s - %s", goodsReceiptNo,
        goodsReceipt.getData("movementDate"), "Vendor A", goodsReceiptLines.getData("lineNo"),
        "Raw material A", "10");

    goodsReceiptLines.selectWithoutFiltering(1);
    goodsReceiptLines.edit(new GoodsReceiptLinesData.Builder().movementQuantity("8").build());
    goodsReceiptLines.assertData(goodsReceiptLineVerificationData2);

    String goodsReceiptLineIdentifier2 = String.format("%s - %s - %s - %s - %s - %s",
        goodsReceiptNo, goodsReceipt.getData("movementDate"), "Vendor A",
        goodsReceiptLines.getData("lineNo"), "Raw material B", "8");

    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();
    goodsReceipt.assertData(completedGoodsReceiptHeaderVerificationData);

    // Check order partially delivered
    purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);

    purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.selectWithoutFiltering(0);
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);

    purchaseOrderLines.selectWithoutFiltering(1);
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData2);

    purchaseOrderLines.selectWithoutFiltering(2);
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData3);

    // Invoice the order
    completedPurchaseInvoiceHeaderVerificationData.addDataField("salesOrder",
        purchaseOrderIdentifier);

    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    purchaseInvoice.createLinesFrom(purchaseOrderIdentifier);
    purchaseInvoice.assertProcessCompletedSuccessfully();

    purchaseInvoiceLineVerificationData.addDataField("salesOrderLine",
        purchaseOrderLineIdentifier1);
    purchaseInvoiceLineVerificationData.addDataField("goodsShipmentLine",
        goodsReceiptLineIdentifier);
    purchaseInvoiceLineVerificationData2.addDataField("salesOrderLine",
        purchaseOrderLineIdentifier2);
    purchaseInvoiceLineVerificationData2.addDataField("goodsShipmentLine",
        goodsReceiptLineIdentifier2);
    purchaseInvoiceLineVerificationData3.addDataField("salesOrderLine",
        purchaseOrderLineIdentifier3);

    PurchaseInvoice.Lines purchaseInvoiceLine = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLine.assertCount(3);

    purchaseInvoiceLine.select(new PurchaseInvoiceLinesData.Builder()
        .product(new ProductSimpleSelectorData.Builder().productName("Raw material A").build())
        .invoicedQuantity("10")
        .build());
    purchaseInvoiceLine.assertData(purchaseInvoiceLineVerificationData);

    purchaseInvoiceLine.select(new PurchaseInvoiceLinesData.Builder()
        .product(new ProductSimpleSelectorData.Builder().productName("Raw material B").build())
        .invoicedQuantity("8")
        .build());
    purchaseInvoiceLine.assertData(purchaseInvoiceLineVerificationData2);

    purchaseInvoiceLine.select(new PurchaseInvoiceLinesData.Builder()
        .product(new ProductSimpleSelectorData.Builder().productName("Raw material C").build())
        .invoicedQuantity("20")
        .build());
    purchaseInvoiceLine.assertData(purchaseInvoiceLineVerificationData3);

    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);

    // Check invoiced and partially delivered order
    purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);

    purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.selectWithoutFiltering(0);
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);

    purchaseOrderLines.selectWithoutFiltering(1);
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData2);

    purchaseOrderLines.selectWithoutFiltering(2);
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData3);

    // Check invoiced shipment
    goodsReceipt = new GoodsReceipt(mainPage).open();
    goodsReceipt.select(new GoodsReceiptHeaderData.Builder().documentNo(goodsReceiptNo).build());
    goodsReceipt.assertData(completedGoodsReceiptHeaderVerificationData);

    goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.assertCount(2);
    goodsReceiptLines.select(new GoodsReceiptLinesData.Builder()
        .product(new ProductCompleteSelectorData.Builder().name("Raw material A").build())
        .movementQuantity("10")
        .build());
    goodsReceiptLines.assertData(goodsReceiptLineVerificationData);

    goodsReceiptLines.select(new GoodsReceiptLinesData.Builder()
        .product(new ProductCompleteSelectorData.Builder().name("Raw material B").build())
        .movementQuantity("15")
        .build());
    goodsReceiptLines.assertData(goodsReceiptLineVerificationData2);

    logger.info(
        "** End of test case [CLF_PurchaseOrderPartiallyReceiptTest] Test Create Lines From Purchase Order Partially Receipt **");
  }
}
