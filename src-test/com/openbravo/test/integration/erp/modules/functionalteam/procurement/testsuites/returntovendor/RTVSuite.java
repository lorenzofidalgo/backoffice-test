/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2018 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.returntovendor;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.openbravo.test.integration.erp.testscripts.SuiteThatStopsIfFailure;

/**
 *
 * Test Suite for RTV test cases
 *
 * @author Nono Carballo
 *
 */
@RunWith(SuiteThatStopsIfFailure.class)
@Suite.SuiteClasses({ RTVa002CreateReturnableProduct.class, RTVa010CreateEditDeleteLine.class,
    RTVa020ReturnedQyt.class, RTVa030FilteringPickEditLineButton.class, RTVa040LoadingPELines.class,
    RTVa050ReturnedQtyInOthers.class, RTVb010CreateAReturnToVendorDocument.class,
    RTVc010InvoiceAReturnToVendorShipment.class,
    RTVd010CreateReversedInvoiceFromReturnToVendorShipment.class })
public class RTVSuite {

}
