/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2017 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2016-03-03 21:37:14
 * Contributor(s):
 *   Rafael Queralta <r.queralta@nectus.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.returntovendor;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.product.PriceData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentLineData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ReturnToVendorLineSelectorData;
import com.openbravo.test.integration.erp.gui.masterdata.product.PriceTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendor;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendorShipment;
import com.openbravo.test.integration.selenium.Sleep;

/**
 *
 * Class for Test RTVb010 Create a Return to vendor document
 *
 * @author Rafael Queralta
 *
 */
@RunWith(Parameterized.class)
public class RTVb010CreateAReturnToVendorDocument extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  ProductData filterProductData;
  ProductData productData;
  PriceData productPrice;
  PriceData salesProductPrice;

  /** Data for Test */
  GoodsReceiptHeaderData goodsReceiptHeader;
  /** The return to vendor header data. */
  ReturnToVendorHeaderData returnToVendorHeaderData;
  /** The data to verify the return to vendor header creation. */
  ReturnToVendorHeaderData returnToVendorHeaderVerificationData;
  /** The booked return to vendor verification data. */
  ReturnToVendorHeaderData bookedReturnToVendorHeaderVerificationData;

  ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderData;
  ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderVerificationData;
  ReturnToVendorShipmentLineData returnToVendorShipmentCreateLine;
  ReturnToVendorShipmentLineData returnToVendorShipmentEditLine;

  public RTVb010CreateAReturnToVendorDocument(ProductData filterProductData,
      ProductData productData, PriceData productPrice, PriceData salesProductPrice,
      GoodsReceiptHeaderData goodsReceiptHeader, ReturnToVendorHeaderData returnToVendorHeaderData,
      ReturnToVendorHeaderData returnToVendorHeaderVerificationData,
      ReturnToVendorHeaderData bookedReturnToVendorHeaderVerificationData,
      ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderData,
      ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderVerificationData,
      ReturnToVendorShipmentLineData returnToVendorShipmentCreateLine,
      ReturnToVendorShipmentLineData returnToVendorShipmentEditLine) {
    this.filterProductData = filterProductData;
    this.productData = productData;
    this.productPrice = productPrice;
    this.salesProductPrice = salesProductPrice;
    this.goodsReceiptHeader = goodsReceiptHeader;
    this.returnToVendorHeaderData = returnToVendorHeaderData;
    this.returnToVendorHeaderVerificationData = returnToVendorHeaderVerificationData;
    this.bookedReturnToVendorHeaderVerificationData = bookedReturnToVendorHeaderVerificationData;
    this.returnToVendorShipmentHeaderData = returnToVendorShipmentHeaderData;
    this.returnToVendorShipmentHeaderVerificationData = returnToVendorShipmentHeaderVerificationData;
    this.returnToVendorShipmentCreateLine = returnToVendorShipmentCreateLine;
    this.returnToVendorShipmentEditLine = returnToVendorShipmentEditLine;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> returnToVendorValues() {
    Object[][] data = new Object[][] {
        { new ProductData.Builder().name("ProductTest-").searchKey("PT-").build(),
            new ProductData.Builder().organization("*")
                .uOM("Unit")
                .productCategory("Distribution Goods")
                .taxCategory("VAT 10%")
                .build(),
            new PriceData.Builder().priceListVersion("Sport items")
                .listPrice("28.50")
                .standardPrice("28.50")
                .build(),

            new PriceData.Builder().priceListVersion("Sales")
                .listPrice("28.50")
                .standardPrice("28.50")
                .build(),
            new GoodsReceiptHeaderData.Builder()
                .businessPartner(
                    new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
                .build(),
            new ReturnToVendorHeaderData.Builder()
                .businessPartner(
                    new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
                .build(),
            new ReturnToVendorHeaderData.Builder().organization("Spain")
                .partnerAddress(".Pamplona, Pamplona")
                .businessPartner(
                    new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
                .transactionDocument("RTV Order")
                .warehouse("Spain warehouse")
                .paymentMethod("1 (Spain)")
                .paymentTerms("30 days, 5")
                .priceList("Sport items")
                .grandTotalAmount("0.00")
                .documentStatus("Draft")
                .build(),
            new ReturnToVendorHeaderData.Builder().documentStatus("Booked").build(),

            new ReturnToVendorShipmentHeaderData.Builder()
                .businessPartner(
                    new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
                .build(),
            new ReturnToVendorShipmentHeaderData.Builder().organization("Spain")
                .partnerAddress(".Pamplona, Pamplona")
                .businessPartner(
                    new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
                .documentType("RTV Shipment")
                .warehouse("Spain warehouse")
                .documentStatus("Draft")
                .build(),
            new ReturnToVendorShipmentLineData.Builder().lineNo("10")
                .movementQuantity("5")
                .storageBin("T01")
                .build(),
            new ReturnToVendorShipmentLineData.Builder().lineNo("10")
                .movementQuantity("4")
                .storageBin("T01")
                .build() } };
    logger.debug("Building parameters");
    return Arrays.asList(data);
  }

  @Test
  public void RTVb010test() throws ParseException {
    logger.debug("** Start test case [RTVb010] Create a Return to vendor document **");

    int recordCount = Product.ProductTab.getRecordCount(mainPage, this.filterProductData);

    String productName = "ProductTest-" + recordCount;

    productData.addDataField("name", productName);
    productData.addDataField("searchKey", "PT-" + recordCount);

    returnToVendorShipmentCreateLine.addDataField("product", productName);
    returnToVendorShipmentEditLine.addDataField("product", productName);

    ProductWindow productWindow = Product.ProductTab.open(mainPage);
    ProductTab productTab = productWindow.selectProductTab();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    productTab.createRecord(productData);
    productTab.assertSaved();

    PriceTab priceTab = productWindow.selectPriceTab();
    priceTab.createRecord(productPrice);
    priceTab.assertSaved();

    priceTab.createRecord(salesProductPrice);
    priceTab.assertSaved();

    GoodsReceipt goodsReceipt = (GoodsReceipt) new GoodsReceipt(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt.create(goodsReceiptHeader);
    goodsReceipt.assertSaved();
    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.create(new GoodsReceiptLinesData.Builder()
        .product(new ProductCompleteSelectorData.Builder().name(productName).build())
        .movementQuantity("5")
        .storageBin("T01")
        .build());
    goodsReceiptLines.assertSaved();
    goodsReceiptLines.create(new GoodsReceiptLinesData.Builder()
        .product(new ProductCompleteSelectorData.Builder().name(productName).build())
        .movementQuantity("20")
        .storageBin("T02")
        .build());
    goodsReceiptLines.assertSaved();
    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();
    String documentNo = (String) goodsReceipt.getData("documentNo");

    final ReturnToVendor returnToVendor = (ReturnToVendor) new ReturnToVendor(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnToVendor.create(returnToVendorHeaderData);
    returnToVendor.assertSaved();
    returnToVendor.assertData(returnToVendorHeaderVerificationData);
    PickAndExecuteWindow<ReturnToVendorLineSelectorData> popup = returnToVendor
        .openPickAndEditLines();
    popup.filter(new ReturnToVendorLineSelectorData.Builder().product(productName)
        .movementQuantity("5")
        .shipmentNumber(documentNo)
        .build());
    popup.unselectAll();
    popup.edit(new ReturnToVendorLineSelectorData.Builder().returned("5")
        .returnReason("Defective")
        .build());
    popup.process();
    returnToVendor.book();
    returnToVendor.assertProcessCompletedSuccessfully2();
    returnToVendor.assertData(bookedReturnToVendorHeaderVerificationData);

    String rtvDocumentNo = (String) returnToVendor.getData("documentNo");

    returnToVendor.create(returnToVendorHeaderData);
    returnToVendor.assertSaved();
    popup = returnToVendor.openPickAndEditLines();
    popup.filter(new ReturnToVendorLineSelectorData.Builder().product(productName)
        .movementQuantity("20")
        .build());
    popup.edit(new ReturnToVendorLineSelectorData.Builder().returned("10")
        .returnReason("Defective")
        .build());
    popup.process();
    returnToVendor.book();
    returnToVendor.assertProcessCompletedSuccessfully2();
    returnToVendor.assertData(bookedReturnToVendorHeaderVerificationData);

    ReturnToVendorShipment returnToVendorShipment = (ReturnToVendorShipment) new ReturnToVendorShipment(
        mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnToVendorShipment.create(returnToVendorShipmentHeaderData);
    returnToVendorShipment.assertSaved();
    returnToVendorShipment.assertData(returnToVendorShipmentHeaderVerificationData);

    /** Create a line **/
    PickAndExecuteWindow<ReturnToVendorShipmentLineData> popUp = returnToVendorShipment
        .openPickAndEditLines();
    popUp.filter(new ReturnToVendorShipmentLineData.Builder().rMOrderNo(rtvDocumentNo)
        .storageBin("T01")
        .lineNo(">0")
        .build());
    popUp.unselectAll();
    popUp.selectRecord(0);
    popUp.process();
    ReturnToVendorShipment.Lines returnToVendorShipmentLines = returnToVendorShipment.new Lines(
        mainPage);
    returnToVendorShipmentLines.assertData(returnToVendorShipmentCreateLine);

    /** Edit a line **/
    popUp = returnToVendorShipment.openPickAndEditLines();
    Sleep.trySleep();
    popUp.filter(new ReturnToVendorShipmentLineData.Builder().rMOrderNo(rtvDocumentNo)
        .storageBin("T01")
        .lineNo(">0")
        .build());
    Sleep.trySleep();
    popUp.edit(new ReturnToVendorShipmentLineData.Builder().movementQuantity("4").build());
    popUp.process();
    Sleep.trySleep();
    returnToVendorShipmentLines.assertData(returnToVendorShipmentEditLine);

    /** Delete a line **/
    popUp = returnToVendorShipment.openPickAndEditLines();
    popUp.filter(new ReturnToVendorShipmentLineData.Builder().rMOrderNo(rtvDocumentNo)
        .storageBin("T01")
        .lineNo(">0")
        .build());
    Sleep.trySleep();
    popUp.unselectAll();
    popUp.process();
    Sleep.trySleep();
    returnToVendorShipmentLines.assertCount(0);

    PickAndExecuteWindow<ReturnToVendorShipmentLineData> rtvsPopUp = returnToVendorShipment
        .openPickAndEditLines();

    Sleep.trySleep();
    rtvsPopUp.clearFilters();
    Sleep.trySleep();

    int availableQty = 5;
    int pendingQty = 5;
    ReturnToVendorShipmentLineData availableQuantityEdit = new ReturnToVendorShipmentLineData.Builder()
        .movementQuantity(String.valueOf(availableQty + 1))
        .build();

    rtvsPopUp.filter(new ReturnToVendorShipmentLineData.Builder().product(productName)
        .returned("10")
        .uOM("Unit")
        .storageBin("T01")
        .availableQty("5")
        .build());
    Sleep.trySleep();
    rtvsPopUp.edit(availableQuantityEdit);
    rtvsPopUp.process();
    rtvsPopUp.assertError(String.format(
        "Ship Qty exceeds the Available Qty. Please enter a valid range: 0 - %s.", availableQty));

    rtvsPopUp.filter(new ReturnToVendorShipmentLineData.Builder().product(productName)
        .returned("5")
        .uOM("Unit")
        .storageBin("T02")
        .pending("5")
        .build());
    Sleep.trySleep();
    rtvsPopUp.edit(new ReturnToVendorShipmentLineData.Builder()
        .movementQuantity(String.valueOf(pendingQty + 1))
        .build());
    rtvsPopUp.process();
    rtvsPopUp.assertError(String.format(
        "Ship Qty exceeds the Pending Qty. Please enter a valid range: 0 - %s.", pendingQty));
    rtvsPopUp.cancel();
    logger.debug("** End test case [RTVb010] Create a Return to vendor document **");
  }
}
