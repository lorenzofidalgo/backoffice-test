/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.reservation.testsuites.RES_Regressions.RES_Regressions1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.generalsetup.application.preference.PreferenceData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.preference.Preference;

/**
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class EnableStockPreference extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PreferenceData filterData;
  PreferenceData preferenceData;

  /**
   * Class constructor.
   *
   */
  public EnableStockPreference(PreferenceData filterData, PreferenceData preferenceData) {
    this.filterData = filterData;
    this.preferenceData = preferenceData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] {
        { new PreferenceData.Builder().property("Enable Stock Reservations").active(true).build(),
            new PreferenceData.Builder().client("QA Testing")
                .organization("*")
                .active(true)
                .selected(false)
                .propertyList(true)
                .property("Enable Stock Reservations")
                .value("Y")
                .visibleAtUser("QAAdmin")
                .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test EnableStockPreference
   *
   * @throws ParseException
   */
  @Test
  public void EnableStockPreferenceTest() throws ParseException {
    logger.info("** Start of test case [EnableStockPreference] **");

    Preference.PreferenceTab.filter(mainPage, filterData);
    int recordCount = Preference.PreferenceTab.getRecordCount(mainPage);
    if (recordCount == 0) {
      Preference.PreferenceTab.create(mainPage, preferenceData);
    }
    logger.info("** End of test case [EnableStockPreference] **");
  }

  // It is required to logout + login after this preference change due to that preference is not
  // always applied correctly otherwise.
  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
