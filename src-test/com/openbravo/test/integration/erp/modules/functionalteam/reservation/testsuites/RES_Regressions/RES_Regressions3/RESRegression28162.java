/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.reservation.testsuites.RES_Regressions.RES_Regressions3;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.product.PriceData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.ManageReservationData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.stockreservation.ReservationData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.stockreservation.StockData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.GoodsShipment;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.erp.testscripts.warehouse.transactions.stockreservation.StockReservation;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 28162
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class RESRegression28162 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  ProductData productData;
  PriceData productPriceData;
  GoodsReceiptHeaderData goodsReceiptHeaderData;
  GoodsReceiptHeaderData goodsReceiptHeaderVerificationData;
  GoodsReceiptLinesData goodsReceiptLinesData;
  GoodsReceiptLinesData goodsReceiptLineVerificationData;
  GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData;
  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  ReservationData reservationVerificationData;
  StockData stockVerificationData;
  GoodsShipmentHeaderData goodsShipmentHeaderData;
  GoodsShipmentHeaderData goodsShipmentHeaderVerificationData;
  GoodsShipmentLinesData goodsShipmentLineData;
  GoodsShipmentLinesData goodsShipmentLineVerificationData;
  GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData;
  ReservationData reservationVerificationData2;
  StockData stockVerificationData2;
  ManageReservationData manageReservationData;
  ReservationData reservationVerificationData3;
  StockData stockVerificationData3;
  GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData2;
  ReservationData reservationVerificationData4;
  StockData stockVerificationData4;

  /**
   * Class constructor.
   *
   */
  public RESRegression28162(ProductData productData, PriceData productPriceData,
      GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptHeaderData goodsReceiptHeaderVerificationData,
      GoodsReceiptLinesData goodsReceiptLinesData,
      GoodsReceiptLinesData goodsReceiptLineVerificationData,
      GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData,
      SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      ReservationData reservationVerificationData, StockData stockVerificationData,
      GoodsShipmentHeaderData goodsShipmentHeaderData,
      GoodsShipmentHeaderData goodsShipmentHeaderVerificationData,
      GoodsShipmentLinesData goodsShipmentLineData,
      GoodsShipmentLinesData goodsShipmentLineVerificationData,
      GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData,
      ReservationData reservationVerificationData2, StockData stockVerificationData2,
      ManageReservationData manageReservationData, ReservationData reservationVerificationData3,
      StockData stockVerificationData3,
      GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData2,
      ReservationData reservationVerificationData4, StockData stockVerificationData4) {
    this.productData = productData;
    this.productPriceData = productPriceData;
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptHeaderVerificationData = goodsReceiptHeaderVerificationData;
    this.goodsReceiptLinesData = goodsReceiptLinesData;
    this.goodsReceiptLineVerificationData = goodsReceiptLineVerificationData;
    this.completedGoodsReceiptHeaderVerificationData = completedGoodsReceiptHeaderVerificationData;
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.reservationVerificationData = reservationVerificationData;
    this.stockVerificationData = stockVerificationData;
    this.goodsShipmentHeaderData = goodsShipmentHeaderData;
    this.goodsShipmentHeaderVerificationData = goodsShipmentHeaderVerificationData;
    this.goodsShipmentLineData = goodsShipmentLineData;
    this.goodsShipmentLineVerificationData = goodsShipmentLineVerificationData;
    this.completedGoodsShipmentHeaderVerificationData = completedGoodsShipmentHeaderVerificationData;
    this.reservationVerificationData2 = reservationVerificationData2;
    this.stockVerificationData2 = stockVerificationData2;
    this.manageReservationData = manageReservationData;
    this.reservationVerificationData3 = reservationVerificationData3;
    this.stockVerificationData3 = stockVerificationData3;
    this.completedGoodsShipmentHeaderVerificationData2 = completedGoodsShipmentHeaderVerificationData2;
    this.reservationVerificationData4 = reservationVerificationData4;
    this.stockVerificationData4 = stockVerificationData4;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new ProductData.Builder().organization("Spain")
            .uOM("Unit")
            .productCategory("Finished Goods")
            .taxCategory("VAT 10%")
            .purchase(false)
            .sale(true)
            .productType("Item")
            .stocked(true)
            .build(),
        new PriceData.Builder().priceListVersion("Customer A")
            .standardPrice("2.00")
            .listPrice("2.00")
            .build(),

        new GoodsReceiptHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new GoodsReceiptHeaderData.Builder().organization("Spain")
            .documentType("MM Receipt")
            .warehouse("Spain warehouse")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .build(),
        new GoodsReceiptLinesData.Builder().storageBin("L01").build(),
        new GoodsReceiptLinesData.Builder().lineNo("10").uOM("Unit").organization("Spain").build(),
        new GoodsReceiptHeaderData.Builder().documentStatus("Completed").build(),

        new SalesOrderHeaderData.Builder().transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesOrderHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .warehouse("Spain warehouse")
            .invoiceTerms("Customer Schedule After Delivery")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .build(),
        new SalesOrderLinesData.Builder().orderedQuantity("10").build(),
        new SalesOrderLinesData.Builder().uOM("Unit")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("20.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("20.00")
            .grandTotalAmount("22.00")
            .currency("EUR")
            .build(),

        new ReservationData.Builder().organization("Spain")
            .quantity("10")
            .reservedQuantity("4")
            .releasedQuantity("0")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("4")
            .allocated(false)
            .build(),

        new GoodsShipmentHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new GoodsShipmentHeaderData.Builder().documentType("MM Shipment")
            .warehouse("Spain warehouse")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("4").build(),
        new GoodsShipmentLinesData.Builder().lineNo("10")
            .movementQuantity("4")
            .uOM("Unit")
            .storageBin("L01")
            .organization("Spain")
            .build(),
        new GoodsShipmentHeaderData.Builder().documentStatus("Completed").build(),

        new ReservationData.Builder().organization("Spain")
            .quantity("10")
            .reservedQuantity("4")
            .releasedQuantity("4")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("4")
            .releasedQuantity("4")
            .allocated(false)
            .build(),

        new ManageReservationData.Builder().quantity("6").build(),

        new ReservationData.Builder().organization("Spain")
            .quantity("10")
            .reservedQuantity("6")
            .releasedQuantity("4")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("6")
            .releasedQuantity("4")
            .allocated(false)
            .build(),

        new GoodsShipmentHeaderData.Builder().documentStatus("Voided").build(),

        new ReservationData.Builder().organization("Spain")
            .quantity("10")
            .reservedQuantity("6")
            .releasedQuantity("0")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("6")
            .releasedQuantity("0")
            .allocated(false)
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 28162
   *
   * @throws ParseException
   */
  @Test
  public void RESRegression28162Test() throws ParseException {
    logger.info("** Start of test case [RESRegression28162] Test regression 28162. **");

    String productName = "Test28162-";
    int num1 = Product.ProductTab.getRecordCount(mainPage,
        new ProductData.Builder().name(productName).build());
    productName = productName + String.valueOf(num1);
    ProductData productData1 = productData;
    productData1.addDataField("searchKey", productName);
    productData1.addDataField("name", productName);
    Product.ProductTab.create(mainPage, productData1);
    Product.Price.create(mainPage, productPriceData);

    GoodsReceipt goodsReceipt1 = new GoodsReceipt(mainPage).open();
    goodsReceipt1.create(goodsReceiptHeaderData);
    goodsReceipt1.assertSaved();
    goodsReceipt1.assertData(goodsReceiptHeaderVerificationData);
    GoodsReceipt.Lines goodsReceiptLines1 = goodsReceipt1.new Lines(mainPage);
    goodsReceiptLinesData.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName).build());
    goodsReceiptLinesData.addDataField("movementQuantity", "4");
    goodsReceiptLines1.create(goodsReceiptLinesData);
    goodsReceiptLines1.assertSaved();
    goodsReceiptLines1.assertData(goodsReceiptLineVerificationData);
    goodsReceipt1.complete();
    goodsReceipt1.assertProcessCompletedSuccessfully2();
    goodsReceipt1.assertData(completedGoodsReceiptHeaderVerificationData);

    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    String orderNo = salesOrder.getData("documentNo").toString();
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLinesData.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName).build());
    salesOrderLines.create(salesOrderLinesData);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);
    String orderLineIdentifier = String.format(" - %s - %s", salesOrderLines.getData("lineNo"),
        salesOrderLines.getData("lineNetAmount"));
    salesOrder.book();
    // TODO L1: It is possible it is required to wait after the previous salesOrder.book(); If it is
    // not required, delete this comment
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);
    String orderIdentifier = String.format("%s - %s - %s", orderNo, salesOrder.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));
    orderLineIdentifier = orderIdentifier + orderLineIdentifier;
    Sleep.trySleep(15000);
    salesOrderLines.manageReservation();
    Sleep.trySleep(15000);
    StockReservation stockReservation = new StockReservation(mainPage).open();
    stockReservation
        .select(new ReservationData.Builder().salesOrderLine(orderLineIdentifier).build());
    stockReservation.assertCount(1);
    stockReservation.assertData(
        (ReservationData) reservationVerificationData.addDataField("product", productName)
            .addDataField("salesOrderLine", orderLineIdentifier));
    StockReservation.Stock stock = stockReservation.new Stock(mainPage);
    stock.assertCount(1);
    stock.assertData(stockVerificationData);

    GoodsShipment goodsShipment = new GoodsShipment(mainPage).open();
    goodsShipment.create(goodsShipmentHeaderData);
    goodsShipment.assertSaved();
    goodsShipment.assertData(goodsShipmentHeaderVerificationData);
    goodsShipment.createLinesFrom("L01", orderIdentifier);
    goodsShipment.assertProcessCompletedSuccessfully();
    String shipmentNo = goodsShipment.getData("documentNo").toString();
    GoodsShipment.Lines goodsShipmentLines = goodsShipment.new Lines(mainPage);
    goodsShipmentLines.assertCount(1);
    goodsShipmentLines.edit(goodsShipmentLineData);
    goodsShipmentLines.assertData(goodsShipmentLineVerificationData);
    goodsShipment.complete();
    goodsShipment.assertProcessCompletedSuccessfully2();
    goodsShipment.assertData(completedGoodsShipmentHeaderVerificationData);

    stockReservation = new StockReservation(mainPage).open();
    stockReservation
        .select(new ReservationData.Builder().salesOrderLine(orderLineIdentifier).build());
    Sleep.trySleep();
    stockReservation.assertCount(1);
    stockReservation.assertData(
        (ReservationData) reservationVerificationData2.addDataField("product", productName)
            .addDataField("salesOrderLine", orderLineIdentifier));
    stock = stockReservation.new Stock(mainPage);
    Sleep.trySleep();
    stock.assertCount(1);
    stock.assertData(stockVerificationData2);

    GoodsReceipt goodsReceipt2 = new GoodsReceipt(mainPage).open();
    goodsReceipt2.create(goodsReceiptHeaderData);
    goodsReceipt2.assertSaved();
    goodsReceipt2.assertData(goodsReceiptHeaderVerificationData);
    GoodsReceipt.Lines goodsReceiptLines2 = goodsReceipt2.new Lines(mainPage);
    goodsReceiptLinesData.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName).build());
    goodsReceiptLinesData.addDataField("movementQuantity", "6");
    goodsReceiptLines2.create(goodsReceiptLinesData);
    goodsReceiptLines2.assertSaved();
    goodsReceiptLines2.assertData(goodsReceiptLineVerificationData);
    goodsReceipt2.complete();
    goodsReceipt2.assertProcessCompletedSuccessfully2();
    goodsReceipt2.assertData(completedGoodsReceiptHeaderVerificationData);

    salesOrder = new SalesOrder(mainPage).open();
    salesOrder.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.manageReservation(manageReservationData);

    stockReservation = new StockReservation(mainPage).open();
    stockReservation
        .select(new ReservationData.Builder().salesOrderLine(orderLineIdentifier).build());
    Sleep.trySleep();
    stockReservation.assertCount(1);
    stockReservation.assertData(
        (ReservationData) reservationVerificationData3.addDataField("product", productName)
            .addDataField("salesOrderLine", orderLineIdentifier));
    stock = stockReservation.new Stock(mainPage);
    Sleep.trySleep();
    stock.assertCount(1);
    stock.assertData(stockVerificationData3);

    goodsShipment = new GoodsShipment(mainPage).open();
    goodsShipment.select(new GoodsShipmentHeaderData.Builder().documentNo(shipmentNo).build());
    goodsShipment.close();
    goodsShipment.assertProcessCompletedSuccessfully2();
    goodsShipment.assertData(completedGoodsShipmentHeaderVerificationData2);

    stockReservation = new StockReservation(mainPage).open();
    stockReservation
        .select(new ReservationData.Builder().salesOrderLine(orderLineIdentifier).build());
    Sleep.trySleep();
    stockReservation.assertCount(1);
    stockReservation.assertData(
        (ReservationData) reservationVerificationData4.addDataField("product", productName)
            .addDataField("salesOrderLine", orderLineIdentifier));
    stock = stockReservation.new Stock(mainPage);
    Sleep.trySleep();
    stock.assertCount(1);
    stock.assertData(stockVerificationData4);

    logger.info("** End of test case [RESRegression28162] Test regression 28162. **");
  }
}
