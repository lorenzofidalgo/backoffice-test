/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.SAL_Regressions;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.GoodsShipment;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;

/**
 * Test regression 28226
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class SALRegression28226 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderLinesData salesOrderLinesData2;
  SalesOrderLinesData salesOrderLinesVerificationData2;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  GoodsShipmentHeaderData goodsShipmentHeaderData;
  GoodsShipmentHeaderData goodsShipmentHeaderVerificationData;
  GoodsShipmentLinesData goodsShipmentLineVerificationData;
  GoodsShipmentLinesData goodsShipmentLineVerificationData2;
  GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData;
  GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData2;

  /**
   * Class constructor.
   *
   */
  public SALRegression28226(SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData, SalesOrderLinesData salesOrderLinesData2,
      SalesOrderLinesData salesOrderLinesVerificationData2,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      GoodsShipmentHeaderData goodsShipmentHeaderData,
      GoodsShipmentHeaderData goodsShipmentHeaderVerificationData,
      GoodsShipmentLinesData goodsShipmentLineVerificationData,
      GoodsShipmentLinesData goodsShipmentLineVerificationData2,
      GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData,
      GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData2) {
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.salesOrderLinesData2 = salesOrderLinesData2;
    this.salesOrderLinesVerificationData2 = salesOrderLinesVerificationData2;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.goodsShipmentHeaderData = goodsShipmentHeaderData;
    this.goodsShipmentHeaderVerificationData = goodsShipmentHeaderVerificationData;
    this.goodsShipmentLineVerificationData = goodsShipmentLineVerificationData;
    this.goodsShipmentLineVerificationData2 = goodsShipmentLineVerificationData2;
    this.completedGoodsShipmentHeaderVerificationData = completedGoodsShipmentHeaderVerificationData;
    this.completedGoodsShipmentHeaderVerificationData2 = completedGoodsShipmentHeaderVerificationData2;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesOrderHeaderData.Builder().transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .warehouse("Spain warehouse")
            .build(),
        new SalesOrderHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .warehouse("Spain warehouse")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .invoiceTerms("Customer Schedule After Delivery")
            .build(),
        new SalesOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("DGA").priceListVersion("Sales").build())
            .orderedQuantity("1")
            .build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("2.00")
            .build(),
        new SalesOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("DGB").priceListVersion("Sales").build())
            .orderedQuantity("-1")
            .build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("-2.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("0.00")
            .grandTotalAmount("0.00")
            .currency("EUR")
            .build(),

        new GoodsShipmentHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .warehouse("Spain warehouse")
            .build(),
        new GoodsShipmentHeaderData.Builder().documentType("MM Shipment")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .warehouse("Spain warehouse")
            .build(),
        new GoodsShipmentLinesData.Builder().lineNo("10")
            .product(new ProductCompleteSelectorData.Builder().name("Distribution good A").build())
            .movementQuantity("1")
            .uOM("Bag")
            .storageBin("L01")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().lineNo("20")
            .product(new ProductCompleteSelectorData.Builder().name("Distribution good B").build())
            .movementQuantity("-1")
            .uOM("Bag")
            .storageBin("L01")
            .organization("Spain")
            .build(),
        new GoodsShipmentHeaderData.Builder().documentStatus("Completed").build(),
        new GoodsShipmentHeaderData.Builder().documentStatus("Voided").build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 28226
   *
   * @throws ParseException
   */
  @Test
  public void SALRegression28226Test() throws ParseException {
    logger.info("** Start of test case [SALRegression28226] Test regression 28226. **");

    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    SalesOrder.Lines salesOrderLines1 = salesOrder.new Lines(mainPage);
    salesOrderLines1.create(salesOrderLinesData);
    salesOrderLines1.assertSaved();
    salesOrderLines1.assertData(salesOrderLinesVerificationData);
    SalesOrder.Lines salesOrderLines2 = salesOrder.new Lines(mainPage);
    salesOrderLines2.create(salesOrderLinesData2);
    salesOrderLines2.assertSaved();
    salesOrderLines2.assertData(salesOrderLinesVerificationData2);
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);
    DataObject completedSalesOrderHeaderData = salesOrder.getData();

    GoodsShipment goodsShipment = new GoodsShipment(mainPage).open();
    goodsShipment.create(goodsShipmentHeaderData);
    goodsShipment.assertSaved();
    goodsShipment.assertData(goodsShipmentHeaderVerificationData);
    String salesOrderIdentifier = String.format("%s - %s - %s",
        completedSalesOrderHeaderData.getDataField("documentNo"),
        completedSalesOrderHeaderData.getDataField("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));
    goodsShipment.createLinesFrom("L01", salesOrderIdentifier);
    goodsShipment.assertProcessCompletedSuccessfully();

    GoodsShipment.Lines goodsShipmentLines = goodsShipment.new Lines(mainPage);
    goodsShipmentLines.assertCount(2);
    goodsShipmentLines.selectWithoutFiltering(0);
    goodsShipmentLines.assertData(goodsShipmentLineVerificationData);
    goodsShipmentLines.closeForm();
    goodsShipmentLines.selectWithoutFiltering(1);
    goodsShipmentLines.assertData(goodsShipmentLineVerificationData2);
    goodsShipment.complete();
    goodsShipment.assertProcessCompletedSuccessfully2();
    goodsShipment.assertData(completedGoodsShipmentHeaderVerificationData);

    goodsShipment.close();
    goodsShipment.assertProcessCompletedSuccessfully2();
    goodsShipment.assertData(completedGoodsShipmentHeaderVerificationData2);

    logger.info("** End of test case [SALRegression28226] Test regression 28226. **");
  }
}
