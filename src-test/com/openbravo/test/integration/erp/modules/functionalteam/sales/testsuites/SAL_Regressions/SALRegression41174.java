/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2019 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s): Andy Armaignac Collazo <collazoandy4@gmail.com>
 *
 ************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.SAL_Regressions;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.generalsetup.application.preference.PreferenceData;
import com.openbravo.test.integration.erp.data.masterdata.product.AUMData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.masterdata.product.AUMTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.preference.Preference;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.util.ConfigurationProperties;

@RunWith(Parameterized.class)
public class SALRegression41174 extends OpenbravoERPTest {
  private static Logger logger = LogManager.getLogger();

  PreferenceData enablePreference;
  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerificationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;

  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;

  public SALRegression41174(PreferenceData enablePreference,
      SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerificationData,
      SalesOrderLinesData salesOrderLinesData, SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData) {

    this.enablePreference = enablePreference;

    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerificationData = salesOrderHeaderVerificationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;

    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;

    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesInvoiceValues() {
    Object[][] data = new Object[][] { {

        new PreferenceData.Builder().property("Enable UOM Management")
            .propertyList(Boolean.TRUE)
            .value("Y")
            .visibleAtUser("")
            .build(),

        new SalesOrderHeaderData.Builder().organization("Spain")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("1 (Spain)")
            .invoiceTerms("Immediate")
            .build(),
        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .paymentTerms("30 days, 5")
            .priceList("Customer A")
            .invoiceTerms("Immediate")
            .build(),

        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("LT")
            .priceListVersion("Customer A")
            .warehouse("Spain warehouse")
            .build()).operativeQuantity("6").operativeUOM("Bag").tax("VAT 10%").build(),
        new SalesOrderLinesData.Builder().orderedQuantity("30")
            .uOM("Unit")
            .unitPrice("1,300.00")
            .listPrice("1,300.00")
            .lineNetAmount("39,000.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("39,000.00")
            .grandTotalAmount("42,900.00")
            .currency("EUR")
            .build(),

        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("1 (Spain)")
            .build(),
        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .paymentTerms("30 days, 5")
            .priceList("Customer A")
            .build(),
        new SalesInvoiceLinesData.Builder().operativeUOM("Bag")
            .uOM("Unit")
            .operativeQuantity("6")
            .invoicedQuantity("30")
            .unitPrice("1,300.00")
            .listPrice("1,300.00")
            .lineNetAmount("39,000.00")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .summedLineAmount("39,000.00")
            .grandTotalAmount("42,900.00")
            .currency("EUR")
            .build(), } };
    return Arrays.asList(data);
  }

  @Test
  public void SAL_Regression41174Test() throws ParseException {

    logger.info("** Start of test case SAL_Regression41174. **");

    mainPage.changeProfile(new ProfileData.Builder().role("System Administrator - System")
        .client("System")
        .organization("*")
        .build());

    Preference.PreferenceTab.filter(mainPage,
        new PreferenceData.Builder().property("Enable UOM Management").active(true).build());
    int recordCount = Preference.PreferenceTab.getRecordCount(mainPage);
    if (recordCount == 0) {
      Preference.PreferenceTab.create(mainPage, enablePreference);
    }

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
    login();

    ProductWindow productWindow = Product.ProductTab.open(mainPage);
    ProductTab productTab = productWindow.selectProductTab();
    productTab.filter(new ProductData.Builder().name("Laptop").build());

    AUMTab aumTab = productWindow.selectAUMTab();
    recordCount = aumTab.getRecordCount();
    if (recordCount == 0) {
      aumTab.createRecord(new AUMData.Builder().uOM("Bag")
          .conversionRate("5")
          .sales("Secondary")
          .purchase("Secondary")
          .logistics("Secondary")
          .build());
      aumTab.assertSaved();
    }

    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerificationData);
    String salesOrderIdentifier = String.format("%s - %s - %s", salesOrder.getData("documentNo"),
        salesOrder.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.create(salesOrderLinesData);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);

    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);

    salesInvoice.createLinesFrom(salesOrderIdentifier);
    salesInvoice.assertProcessCompletedSuccessfully();

    SalesInvoice.Lines salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.assertData(salesInvoiceLineVerificationData);

    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();

    productWindow = Product.ProductTab.open(mainPage);
    productTab = productWindow.selectProductTab();
    productTab.filter(new ProductData.Builder().name("Laptop").build());

    aumTab = productWindow.selectAUMTab();
    aumTab.filter(new AUMData.Builder().uOM("Bag").build());
    aumTab.deleteOnGrid();

    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
    login();

    mainPage.changeProfile(new ProfileData.Builder().role("System Administrator - System")
        .client("System")
        .organization("*")
        .build());

    Preference.PreferenceTab.delete(mainPage,
        new PreferenceData.Builder().property("Enable UOM Management")
            .active(Boolean.TRUE)
            .build());

    logger.info("** End of test case SAL_Regression41174. **");
  }
}
