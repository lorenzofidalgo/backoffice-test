/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2018 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * Contributor(s):
 *   Mark <markmm82@gmail.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.createlinesfrom.createlinesfrom1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.product.AUMData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.gui.masterdata.product.AUMTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;

@RunWith(Parameterized.class)
public class CLF_AddAUMProduct extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Data for Test */

  ProductData selectProduct;
  AUMData createAUM;
  ProductData selectProductFGB;
  ProductData selectProductPurchase;
  AUMData createAUMPurchase;
  ProductData selectProductRTV;
  ProductData selectProductRTV1;

  public CLF_AddAUMProduct(ProductData selectProduct, AUMData createAUM,
      ProductData selectProductFGB, ProductData selectProductPurchase, AUMData createAUMPurchase,
      ProductData selectProductRTV, ProductData selectProductRTV1) {
    super();
    this.selectProduct = selectProduct;
    this.createAUM = createAUM;
    this.selectProductFGB = selectProductFGB;
    this.selectProductPurchase = selectProductPurchase;
    this.createAUMPurchase = createAUMPurchase;
    this.selectProductRTV = selectProductRTV;
    this.selectProductRTV1 = selectProductRTV1;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> returnToVendorValues() {
    Object[][] data = new Object[][] { { new ProductData.Builder().name("Final good A").build(),
        new AUMData.Builder().uOM("Ounce")
            .conversionRate("2")
            .sales("Primary")
            .purchase("Primary")
            .logistics("Primary")
            .gtin("1234567890")
            .build(),
        new ProductData.Builder().name("Final good B").build(),
        new ProductData.Builder().name("Raw material A").build(),
        new AUMData.Builder().uOM("Ounce")
            .conversionRate("3")
            .sales("Primary")
            .purchase("Primary")
            .logistics("Primary")
            .gtin("1234567890")
            .build(),
        new ProductData.Builder().name("Soccer Ball").build(),
        new ProductData.Builder().name("Tennis ball").build() } };
    return Arrays.asList(data);
  }

  @Test
  public void AddAUMProductTest() throws ParseException {
    logger.debug("** Start test case [AddAUMProductTest] Add AUM to product **");

    ProductWindow productWindow = Product.ProductTab.open(mainPage);
    ProductTab productTab = productWindow.selectProductTab();
    productTab.filter(selectProduct);

    AUMTab aumTab = productWindow.selectAUMTab();
    aumTab.createRecord(createAUM);
    aumTab.assertSaved();

    productWindow = Product.ProductTab.open(mainPage);
    productTab = productWindow.selectProductTab();
    productTab.filter(selectProductFGB);

    aumTab = productWindow.selectAUMTab();
    aumTab.createRecord(createAUM);
    aumTab.assertSaved();

    productWindow = Product.ProductTab.open(mainPage);
    productTab = productWindow.selectProductTab();
    productTab.filter(selectProductPurchase);

    aumTab = productWindow.selectAUMTab();
    aumTab.createRecord(createAUMPurchase);
    aumTab.assertSaved();

    productWindow = Product.ProductTab.open(mainPage);
    productTab = productWindow.selectProductTab();
    productTab.filter(selectProductRTV);

    aumTab = productWindow.selectAUMTab();
    aumTab.createRecord(createAUM);
    aumTab.assertSaved();

    productWindow = Product.ProductTab.open(mainPage);
    productTab = productWindow.selectProductTab();
    productTab.filter(selectProductRTV1);

    aumTab = productWindow.selectAUMTab();
    aumTab.createRecord(createAUM);
    aumTab.assertSaved();

    logger.debug("** End test case [AddAUMProductTest] Add AUM to product **");
  }

}
