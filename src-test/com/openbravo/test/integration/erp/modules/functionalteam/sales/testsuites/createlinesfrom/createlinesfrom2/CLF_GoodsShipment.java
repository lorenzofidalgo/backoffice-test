/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.createlinesfrom.createlinesfrom2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.GoodsShipment;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Test Create Lines From Goods Shipment
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class CLF_GoodsShipment extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  GoodsShipmentHeaderData goodsShipmentHeaderData;
  GoodsShipmentHeaderData goodsShipmentHeaderVerficationData;
  GoodsShipmentLinesData goodsShipmentLinesData;
  GoodsShipmentLinesData goodsShipmentLinesVerificationData;
  GoodsShipmentHeaderData bookedGoodsShipmentHeaderVerificationData;

  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;

  /**
   * Class constructor.
   *
   */
  public CLF_GoodsShipment(GoodsShipmentHeaderData goodsShipmentHeaderData,
      GoodsShipmentHeaderData goodsShipmentHeaderVerficationData,
      GoodsShipmentLinesData goodsShipmentLinesData,
      GoodsShipmentLinesData goodsShipmentLinesVerificationData,
      GoodsShipmentHeaderData bookedGoodsShipmentHeaderVerificationData,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData) {
    this.goodsShipmentHeaderData = goodsShipmentHeaderData;
    this.goodsShipmentHeaderVerficationData = goodsShipmentHeaderVerficationData;
    this.goodsShipmentLinesData = goodsShipmentLinesData;
    this.goodsShipmentLinesVerificationData = goodsShipmentLinesVerificationData;
    this.bookedGoodsShipmentHeaderVerificationData = bookedGoodsShipmentHeaderVerificationData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> goodsShipmentValues() {
    Object[][] data = new Object[][] { {
        new GoodsShipmentHeaderData.Builder().organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .warehouse("Spain warehouse")
            .build(),
        new GoodsShipmentHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .warehouse("Spain warehouse")
            .build(),
        new GoodsShipmentLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().searchKey("DGA").build())
            .movementQuantity("10")
            .storageBin("L01")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("10")
            .storageBin("L01")
            .uOM("Bag")
            .build(),
        new GoodsShipmentHeaderData.Builder().documentStatus("Completed").build(),

        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("1 (Spain)")
            .priceList("Sales")
            .build(),
        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentTerms("30 days, 5")
            .priceList("Sales")
            .build(),
        new SalesInvoiceLinesData.Builder().lineNo("10")
            .product(
                new ProductSimpleSelectorData.Builder().productName("Distribution good A").build())
            .invoicedQuantity("10")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("20.00")
            .organization("Spain")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("22.00")
            .summedLineAmount("20.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(), } };
    return Arrays.asList(data);
  }

  /**
   * Test Create Lines From Goods Shipment
   *
   * @throws ParseException
   */
  @Test
  public void CLF_GoodsShipmentTest() throws ParseException {
    logger.info(
        "** Start of test case [CLF_GoodsShipmentTest] Test Create Lines From Goods Shipment **");

    GoodsShipment goodsShipment = new GoodsShipment(mainPage).open();
    goodsShipment.create(goodsShipmentHeaderData);
    goodsShipment.assertSaved();
    goodsShipment.assertData(goodsShipmentHeaderVerficationData);

    String goodsShipmentNo = goodsShipment.getData("documentNo").toString();

    GoodsShipment.Lines goodsShipmentLines = goodsShipment.new Lines(mainPage);
    goodsShipmentLines.create(goodsShipmentLinesData);
    goodsShipmentLines.assertSaved();
    goodsShipmentLines.assertData(goodsShipmentLinesVerificationData);

    goodsShipment.complete();
    goodsShipment.assertProcessCompletedSuccessfully2();
    goodsShipment.assertData(bookedGoodsShipmentHeaderVerificationData);

    String goodsShipmentIdentifier = String.format("%s - %s - %s", goodsShipmentNo,
        goodsShipment.getData("movementDate"), "Customer A");

    String goodsShipmentLineIdentifier = String.format("%s - %s - %s - %s - %s - %s",
        goodsShipmentNo, goodsShipment.getData("movementDate"), "Customer A",
        goodsShipmentLines.getData("lineNo"), "Distribution good A", "10");

    // Create a line from Goods Shipment
    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    salesInvoice.createLinesFromShipment(goodsShipmentIdentifier);
    salesInvoice.assertProcessCompletedSuccessfully();

    SalesInvoice.Lines salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.assertCount(1);

    salesInvoiceLineVerificationData.addDataField("goodsShipmentLine", goodsShipmentLineIdentifier);
    salesInvoiceLine.assertData(salesInvoiceLineVerificationData);

    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    // Check invoiced shipment
    bookedGoodsShipmentHeaderVerificationData.addDataField("invoiceStatus", "100 %");
    goodsShipment = new GoodsShipment(mainPage).open();
    goodsShipment.select(new GoodsShipmentHeaderData.Builder().documentNo(goodsShipmentNo).build());

    goodsShipment.assertData(bookedGoodsShipmentHeaderVerificationData);

    goodsShipmentLines = goodsShipment.new Lines(mainPage);
    goodsShipmentLines.assertData(goodsShipmentLinesVerificationData);

    logger.info(
        "** End of test case [CLF_GoodsShipmentTest] Test Create Lines From Goods Shipment **");
  }

  @AfterClass
  public static void tearDown() {
    SeleniumSingleton.INSTANCE.quit();
    OpenbravoERPTest.seleniumStarted = false;
    OpenbravoERPTest.forceLoginRequired();
  }
}
