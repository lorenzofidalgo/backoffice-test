/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Pablo Lujan <pablo.lujan@openbravo.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.modules.initialdataload.data.masterdata.process.importwindow;

/**
 *
 * @author plujan
 *
 */
public class ImportData {

  /* Data fields */
  /** Entity */
  private String entity;
  /** File name */
  private String file;

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /* Builder fields */
    /** Entity */
    private String entity = null;
    /** File name */
    private String file = null;

    /**
     * Set the Entity value.
     *
     * @param value
     *          The Entity value.
     * @return The builder for this class.
     */
    public Builder entity(String value) {
      this.entity = value;
      return this;
    }

    /**
     * Set the File Name value.
     *
     * @param value
     *          The File Name value.
     * @return The builder for this class.
     */
    public Builder file(String value) {
      this.file = value;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ImportData build() {
      return new ImportData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ImportData(Builder builder) {
    entity = builder.entity;
    file = builder.file;
  }

  public String getEntity() {
    return entity;
  }

  public String getFile() {
    return file;
  }

  @Override
  public String toString() {
    return "ImportData [entity=" + entity + ", file=" + file + "]";
  }

}
