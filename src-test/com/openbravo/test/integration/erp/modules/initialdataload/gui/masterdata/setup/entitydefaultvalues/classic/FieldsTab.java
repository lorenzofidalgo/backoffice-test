/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.initialdataload.gui.masterdata.setup.entitydefaultvalues.classic;

import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.erp.modules.initialdataload.data.masterdata.setup.entitydefaultvalues.FieldsData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Fields tab.
 *
 * @author elopio
 *
 */
public class FieldsTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Fields";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the Fields tab. */
  public static final String TAB = "tabname884860D303934BB68E23849B1FFA7B69";

  /** Number of name column. */
  public static final int COLUMN_NAME = 1;

  /** Identifier of the default value text field. */
  private static final String TEXT_FIELD_DEFAULT_VALUE = "Defaultvalue";
  /** Identifier of the active check box. */
  @SuppressWarnings("unused")
  private static final String CHECK_BOX_ACTIVE = "Isactive";

  /**
   * Class constructor.
   *
   */
  public FieldsTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Edit a field.
   *
   * @param data
   *          The edited data of the field.
   * @throws OpenbravoERPTestException
   */
  public void edit(FieldsData data) throws OpenbravoERPTestException {
    switchToFormView();
    if (data.getDataField("defaultValue") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_DEFAULT_VALUE)
          .sendKeys((String) data.getDataField("defaultValue"));
    }
    clickSave();
  }
}
