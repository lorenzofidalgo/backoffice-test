/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.initialdataload.testscripts.masterdata.setup;

import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.modules.initialdataload.data.masterdata.setup.entitydefaultvalues.EntitiesData;
import com.openbravo.test.integration.erp.modules.initialdataload.data.masterdata.setup.entitydefaultvalues.FieldsData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * Executes and verifies actions on the window.
 *
 * @author Pablo Lujan
 */
public class EntityDefaultValues {

  /**
   * Executes and verifies actions on the tab.
   *
   * @author plujan
   *
   */
  public static class Entities {

    /**
     * Select a record.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     *          The data of the record that will be selected.
     * @throws OpenbravoERPTestException
     */
    public static void select(MainPage mainPage, EntitiesData data)
        throws OpenbravoERPTestException {
      if (mainPage.isOnNewLayout()) {
        EntitiesTabScript.select(mainPage, data);
      } else {
        EntitiesTabClassicScript.select(mainPage, data);
      }
    }

  }

  /**
   * Executes and verifies actions on the tab.
   *
   * @author plujan
   *
   */
  public static class Fields {

    /**
     * Select a record.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     *          The data of the record that will be selected.
     * @throws OpenbravoERPTestException
     */
    public static void select(MainPage mainPage, FieldsData data) throws OpenbravoERPTestException {
      if (mainPage.isOnNewLayout()) {
        FieldsTabScript.select(mainPage, data);
      } else {
        FieldsTabClassicScript.select(mainPage, data);
      }
    }

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     * @throws OpenbravoERPTestException
     *
     */
    public static void edit(MainPage mainPage, FieldsData data) throws OpenbravoERPTestException {
      if (mainPage.isOnNewLayout()) {
        FieldsTabScript.edit(mainPage, data);
      } else {
        FieldsTabClassicScript.edit(mainPage, data);
      }
    }
  }
}
