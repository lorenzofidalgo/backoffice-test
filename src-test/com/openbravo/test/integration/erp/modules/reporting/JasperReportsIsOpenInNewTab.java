/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2020 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */
package com.openbravo.test.integration.erp.modules.reporting;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.modules.reporting.windows.OpenJasperReportsView;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.ConfigurationProperties;

public class JasperReportsIsOpenInNewTab extends OpenbravoERPTest {

  public JasperReportsIsOpenInNewTab() {
    super();
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  @Test
  public void openJasperReportsShouldOpenInNewTab() {
    boolean opensInNewTab = true;
    Set<String> windowHandlesBefore = SeleniumSingleton.INSTANCE.getWindowHandles();
    mainPage.openView(new OpenJasperReportsView(), opensInNewTab);
    List<String> windowHandlesAfter = new ArrayList<>(
        SeleniumSingleton.INSTANCE.getWindowHandles());

    windowHandlesAfter.removeAll(windowHandlesBefore);
    SeleniumSingleton.INSTANCE.switchTo().window(windowHandlesAfter.get(0));

    assertTrue("TIBCO Jaspersoft: Home".equals(SeleniumSingleton.INSTANCE.getTitle()));
  }

}
