/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2020 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.reporting.windows;

import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.Sleep;

public class ReportsServerReportListingTab extends GeneratedTab<ReportsServerReportListingData> {

  private static final String REGISTRY_KEY_OPEN_REPORT_PROCESS = "org.openbravo.client.application.toolbar.button.openReport.F6ABA9C38A0A4707B2976537A284D499";
  static final String TAB_TITLE = "Reports Server Report Listing";
  public static final String TAB_IDENTIFIER = "F6ABA9C38A0A4707B2976537A284D499";

  private Button openReportProcess;

  public ReportsServerReportListingTab() {
    super(TAB_TITLE, TAB_IDENTIFIER);
    openReportProcess = new Button(TestRegistry.getObjectString(REGISTRY_KEY_OPEN_REPORT_PROCESS));
  }

  public void openReport() {
    openReportProcess.click();
    Sleep.trySleepWithMessage(5_000, "Waiting some time for the reporting iframe to load");
  }

}
