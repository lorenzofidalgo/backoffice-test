/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Iñaki Garcia <inaki.garcia@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.roletest.testsuites;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.FormAccessData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.OrgAccessData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.ProcessAccessData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.RoleData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.UserAssignmentData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.WindowAccessData;
import com.openbravo.test.integration.erp.data.generalsetup.security.user.UserData;
import com.openbravo.test.integration.erp.data.selectors.OrganizationSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.generalsetup.security.role.Role;
import com.openbravo.test.integration.erp.testscripts.generalsetup.security.user.User;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Execute the Setup User and Role flow of the smoke test suite.
 *
 * @author inaki_garcia
 */
@RunWith(Parameterized.class)
public class ROL_userA_SetupUserAndRole_userA extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  private UserData userData;
  private UserData userFilterData;
  private RoleData roleSalesData;
  private OrgAccessData[] orgAccessesData;
  private WindowAccessData[] windowAccessDatas;
  private FormAccessData[] formAccessDatas;
  private ProcessAccessData[] processAccessDatas;
  private UserAssignmentData userAssignmentData;

  /**
   * Class constructor.
   */
  public ROL_userA_SetupUserAndRole_userA(UserData userData, UserData userFilterData,
      RoleData roleSalesData, OrgAccessData[] orgAccessesData, WindowAccessData[] windowAccessDatas,
      FormAccessData[] formAccessDatas, ProcessAccessData[] processAccessDatas,
      UserAssignmentData userAssignmentData) {
    this.userData = userData;
    this.userFilterData = userFilterData;
    this.roleSalesData = roleSalesData;
    this.orgAccessesData = orgAccessesData;
    this.windowAccessDatas = windowAccessDatas;
    this.formAccessDatas = formAccessDatas;
    this.processAccessDatas = processAccessDatas;
    this.userAssignmentData = userAssignmentData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<Object[]> setupUserAndRoleValues() throws IOException {
    return Arrays.asList(new Object[][] { {
        // Create new user data.
        new UserData.Builder().organization("*")
            .name("userA")
            .username("userA")
            .password("userA")
            .build(),
        new UserData.Builder().name("userA").username("userA").build(),
        // Create Role data.
        new RoleData.Builder().name("userA Admin")
            .userLevel("Client+Organization")
            .manual(true)
            .build(),
        // Organization Access data.
        new OrgAccessData[] {
            new OrgAccessData.Builder()
                .organization(new OrganizationSelectorData.Builder().name("*").build())
                .build(),
            new OrgAccessData.Builder()
                .organization(new OrganizationSelectorData.Builder().name("Spain").build())
                .build() },
        // Window Access Data
        new WindowAccessData[] {
            new WindowAccessData.Builder().window("Sales Order").editableField(true).build(),
            new WindowAccessData.Builder().window("Goods Shipment").editableField(true).build(),
            new WindowAccessData.Builder().window("Process Request").editableField(true).build(),
            new WindowAccessData.Builder().window("Sales Invoice").editableField(true).build(),
            new WindowAccessData.Builder().window("Business Partner").editableField(true).build(),
            new WindowAccessData.Builder().window("Product").editableField(true).build() },
        // Form Access Data
        new FormAccessData[] {
            new FormAccessData.Builder().specialForm("Session Preferences").build(),
            new FormAccessData.Builder().specialForm("Create Shipments from Orders").build(),
            new FormAccessData.Builder().specialForm("Create Invoices from Orders").build() },
        // Report and Process Access Data
        new ProcessAccessData[] {
            new ProcessAccessData.Builder().process("Generate Invoices").build(),
            new ProcessAccessData.Builder().process("Journal Entries Report").build() },
        // User Assignment data
        new UserAssignmentData.Builder().userContact("userA").build() } });
  }

  /**
   * Test the setup client and organization flow.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void userAndRoleShouldBeSetUp() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [ROLgen] Create User and Role. **");

    User user = new User(mainPage).open();
    user.filter(userFilterData);

    if (user.getRecordCount() != 0) {
      logger
          .info("[ROLgen] User has already been created. Aborting user and role creation process.");
    } else {
      logger.info("[ROLgen] User hasn't been created, starting user and role creation process.");
      user.create(userData);
      user.assertSaved();
      // Creating a new Role
      Role.RoleTab.create(mainPage, roleSalesData);
      // Setting the Org Accesses for the role
      for (final OrgAccessData orgAccessData : orgAccessesData) {
        Role.RoleTab.OrgAccess.create(mainPage, orgAccessData);
      }
      // Setting the specific Window Accesses for the role, given by the test cases the user has to
      // execute
      for (final WindowAccessData windowAccessData : windowAccessDatas) {
        Role.RoleTab.WindowAccess.create(mainPage, windowAccessData);
      }
      // Setting the specific Form Accesses for the role, given by the test cases the user has to
      // execute
      for (final FormAccessData formAccessData : formAccessDatas) {
        Role.RoleTab.FormAccess.create(mainPage, formAccessData);
      }
      // Setting the specific Form Accesses for the role, given by the test cases the user has to
      // execute
      for (final ProcessAccessData processAccessData : processAccessDatas) {
        Role.RoleTab.ProcessAccess.create(mainPage, processAccessData);
      }
      // Setting the newly created role to the user
      Role.RoleTab.UserAssignment.create(mainPage, userAssignmentData);
    }
    // If test.user or test.password is not set as userA, then set these values.
    if (!ConfigurationProperties.INSTANCE.getUserName().equals("userA")
        || !ConfigurationProperties.INSTANCE.getPassword().equals("userA")) {
      ConfigurationProperties.INSTANCE.setProperty("test.user", "userA");
      ConfigurationProperties.INSTANCE.setProperty("test.password", "userA");
    }
    logger.info("** End of test case [ROLgen] Create User and Role. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
    // OpenbravoERPTest.seleniumStarted = false;
    // SeleniumSingleton.INSTANCE.quit();
  }
}
