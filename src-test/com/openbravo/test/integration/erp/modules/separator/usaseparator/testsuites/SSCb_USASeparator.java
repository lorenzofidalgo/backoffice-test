/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Inigo Sanchez <inigo.sanchez@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.separator.usaseparator.testsuites;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.data.sharedtabs.ExchangeRatesData;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesinvoice.SalesInvoiceWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;

/**
 * Test to create a sales invoice with a line. Then create a exchange rate from that sales invoice
 * in USA format (Dot as decimal separator). In this flow SE_CalculateExchangeRate SimpleCallout is
 * raised.
 *
 * See regression issue #35334
 *
 * @author inigo.sanchez
 *
 */
@RunWith(Parameterized.class)
public class SSCb_USASeparator extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  /* Data for [SSCb020] Create Sales Invoice. */
  /** The Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /** The data to verify the creation of the Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;

  /* Data for [SSCb020] Create a Line for the created sales invoice. */
  /** The data for the creation of the Sales Invoice line data. */
  SalesInvoiceLinesData salesInvoiceLineData;
  /** The data to verify the creation of the Sales Invoice Line data. */
  SalesInvoiceLinesData salesInvoiceLineVerificationData;

  /* Data for [SSCb020] Create a Exchange Rates. */
  /** The Exchange Rates data. */
  ExchangeRatesData salesInvoiceExchangeRatesData;
  /** The data to verify the creation of the Exchange Rates data. */
  ExchangeRatesData salesInvoiceExchangeRatesVerificationData;

  /**
   * Class constructor.
   *
   * @param salesInvoiceHeaderData
   *          The Sales Invoice header data.
   * @param salesInvoiceHeaderVerificationData
   *          The data to verify the creation of the Sales Invoice header data.
   * @param salesInvoiceLineData
   *          The Line for the created sales invoice.
   * @param salesInvoiceLineVerificationData
   *          The data to verify the creation of the Sales Invoice lines data.
   * @param salesInvoiceExchangeRatesData
   *          The Exchange Rates data.
   * @param salesInvoiceExchangeRatesVerificationData
   *          The data to verify the creation of the Exchange Rates data.
   */
  public SSCb_USASeparator(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      ExchangeRatesData salesInvoiceExchangeRatesData,
      ExchangeRatesData salesInvoiceExchangeRatesVerificationData) {
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.salesInvoiceExchangeRatesData = salesInvoiceExchangeRatesData;
    this.salesInvoiceExchangeRatesVerificationData = salesInvoiceExchangeRatesVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesInvoiceValues() {
    Object[][] data = new Object[][] { {
        /*
         * Parameters for [SSCb020] Create Sales Invoice with a Exchange Rates in USA format (Dot as
         * decimal separator).
         */
        new SalesInvoiceHeaderData.Builder().transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A").build())
            .invoicedQuantity("11.2")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A").build())
            .invoicedQuantity("11.2")
            .uOM("Bag")
            .unitPrice("2.00")
            .lineNetAmount("22.40")
            .tax("VAT(3)+CHARGE(0.5)")
            .build(),
        new ExchangeRatesData.Builder().toCurrency("USD").foreignAmount("5").build(),
        new ExchangeRatesData.Builder().toCurrency("USD")
            .foreignAmount("5.00")
            .rate("0.215703")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test to create a sales invoice with a line. Then create a exchange rate from that sales invoice
   * in USA format (Dot as decimal separator).
   */
  @Test
  public void exchangeRateShouldBeCreatedWithCorrectFormat() {
    logger.info(
        "** Start of test case [SSCb020] Create Sales Invoice with a Exchange Rate in USA format (Dot as decimal separator). **");

    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);

    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.assertCount(0);
    salesInvoiceLines.create(salesInvoiceLineData);
    salesInvoiceLines.assertSaved();
    salesInvoiceLines.assertData(salesInvoiceLineVerificationData);

    SalesInvoice.ExchangeRates salesInvoiceExchangeRates = salesInvoice.new ExchangeRates(mainPage);
    salesInvoiceExchangeRates.create(salesInvoiceExchangeRatesData);
    salesInvoiceExchangeRates.assertSaved();
    salesInvoiceExchangeRates.assertData(salesInvoiceExchangeRatesVerificationData);

    mainPage.closeView(SalesInvoiceWindow.TITLE);
    logger.info(
        "** Start of test case [SSCb020] Create Sales Invoice with a Exchange Rate in USA format (Dot as decimal separator). **");
  }
}
