/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */
package com.openbravo.test.integration.erp.modules.userinterface.selectorexamples.gui;

import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;

/**
 * Executes and verifies actions on OpenbravoERP Selector Example screen. This window is added by
 * the Selector Examples module.
 *
 * @author elopio
 *
 */
public class SelectorExample extends GeneratedTab {

  /**
   * Class constructor.
   *
   */
  public SelectorExample() {
    // super();
    /* FIXME */
    super("", 0, "");
  }

  @Override
  public void open() {
    // TODO Auto-generated method stub

  }

}
