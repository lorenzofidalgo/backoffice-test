/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import org.openqa.selenium.Keys;

import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on OpenbravoERP buttons built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class Button extends Canvas {

  /**
   * Formatting string used to locate SmartClient buttons with Selenium. The parameter is the canvas
   * locator, as returned by the parent Canvas class.
   */
  private static final String FORMAT_LOCATOR_SMART_CLIENT_BUTTON = "%s/element";

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   * @param waitForLoad
   *          Set to true if it is required to wait until it is loaded
   */
  public Button(String objectString, Boolean waitForLoad) {
    super(objectString, waitForLoad);
  }

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public Button(String objectString) {
    this(objectString, true);
  }

  /**
   * Class constructor.
   *
   * This constructor will just instantiate the button, but after this the button will not be ready
   * to be used. It has to be initialized using the function setSmartClientLocator in order to be
   * used with Selenium.
   *
   * This is an alternate way to access the SmartClient object, and should be used only if there is
   * no way to get the object string. Always prefer to call the other constructor with the object
   * string as a parameter, because it will set the SmartClient Locator. With this method the
   * actions that require to call SmartClient functions and attributes directly will not be
   * available.
   */
  public Button() {
  }

  /**
   * Get the SmartClient locator of the button.
   *
   * @return the SmartClient locator of the button.
   */
  @Override
  protected String getSmartClientLocator() {
    return String.format(FORMAT_LOCATOR_SMART_CLIENT_BUTTON, super.getSmartClientLocator());
  }

  /**
   * Get the real title of the button.
   *
   * XXX avoid this function. Use getTitle instead. It was added because:
   *
   * - On Heartbeat Configuration tab, the only way to know the status of the heartbeat is to get
   * the real title of the buttons.
   *
   * @return the real title of the button.
   */
  public String getRealTitle() {
    return (String) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format("%s.realTitle", objectString));
  }

  public void useHotKeyToSave() {
    SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
        .sendKeys(Keys.CONTROL + "s");
  }
}
