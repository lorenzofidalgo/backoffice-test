/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.MoveTargetOutOfBoundsException;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.selenium.Attribute;
import com.openbravo.test.integration.selenium.NotImplementedException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.selenium.SmartClientNotDefinedException;

/**
 * Executes actions on OpenbravoERP canvases built upon SmartClient user interface library.
 *
 * @author lorenzo.fidalgo
 *
 */
public class Canvas {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * JavaScript expression that can be evaluated to get a reference to the SmartClient GUI object.
   *
   * This string that can be used with SeleniumSingleton.INSTANCE.getEval function to get the
   * object. It allows us to directly call SmartClient functions defined for this object.
   */
  protected String objectString;
  /**
   * The canvas SmartClient locator. It allows us to execute actions on the object through Selenium.
   */
  protected String smartClientLocator;

  /**
   * Formatting string to locate the canvas. The parameter is the object string.
   */
  protected final static String FORMAT_SMART_CLIENT_GET_LOCATOR = "%s.getLocator()";
  /**
   * Formatting string to get the canvas title. The parameter is the object string.
   */
  private final static String FORMAT_SMART_CLIENT_GET_TITLE = "%s.getTitle()";
  /**
   * Formatting string to check if the canvas is visible. The parameter is the object string.
   */
  private final static String FORMAT_SMART_CLIENT_IS_VISIBLE = "%s.isVisible()";
  /**
   * Formatting string to check if the canvas is disabled. The parameter is the object string.
   */
  private final static String FORMAT_SMART_CLIENT_IS_DISABLED = "%s.isDisabled()";
  /**
   * Formatting string to focus on an item. The parameter is the object string.
   */
  private final static String FORMAT_SMART_CLIENT_FOCUS_IN_ITEM = "%s.focusInItem()";
  /**
   * Formatting string to focus on an item. The parameter is the object string.
   */
  private final static String FORMAT_SMART_CLIENT_TAB_FOCUS = "%s.virtualClick()";
  /**
   * Formatting string to check if an item is focused. The parameter is the object string.
   */
  private final static String FORMAT_SMART_CLIENT_HAS_FOCUS = "%s.hasFocus";
  /**
   * Formatting string to get web element via smart client locator. The parameter will usually be
   * obtained with getSmartClientLocator()
   */
  protected final static String FORMAT_WEB_ELEMENT_SMART_CLIENT_LOCATOR = "window.isc.AutoTest.getElement('%s')";
  /**
   * Formatting string to focus on an web element. The parameter is the smart client web element.
   */
  protected final static String FORMAT_SMART_CLIENT_WEB_ELEMENT_FOCUS = "%s.focus()";

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   * @param waitForLoad
   *          Set to true if it is required to wait until it is loaded
   */
  public Canvas(String objectString, Boolean waitForLoad) {
    this.objectString = objectString;
    if (waitForLoad) {
      try {
        waitUntilLoaded();
      } catch (TimeoutException toe) {
        SeleniumSingleton.INSTANCE.switchTo().defaultContent();
        waitUntilLoaded();
      }
    }
  }

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public Canvas(String objectString) {
    this(objectString, true);
  }

  /**
   * Class constructor.
   *
   * This constructor will just instantiate the canvas, but after this the canvas will not be ready
   * to be used. It has to be initialized using the function setSmartClientLocator in order to be
   * used with Selenium.
   *
   * This is an alternate way to access the SmartClient object, and should be used only if there is
   * no way to get the object string. Always prefer to call the other constructor with the object
   * string as a parameter, because it will set the SmartClient Locator. With this method the
   * actions that require to call SmartClient functions and attributes directly will not be
   * available.
   */
  public Canvas() {
  }

  /**
   * Set the SmartClient Locator of the canvas.
   *
   * @param smartClientLocator
   *          The SmartClient locator of the canvas.
   */
  public void setSmartClientLocator(String smartClientLocator) {
    if (this.smartClientLocator != null) {
      throw new IllegalStateException("The SmartClient Locator has already been set.");
    }
    this.smartClientLocator = smartClientLocator;
  }

  /**
   * Get the SmartClient locator of the canvas.
   *
   * @return the SmartClient locator of the canvas.
   */
  protected String getSmartClientLocator() {
    // First, try to get the locator from the object string. It is safer to do this every time
    // because the locator might change.
    if (objectString != null) {
      String locator = (String) SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_LOCATOR, objectString));
      return cleanSmartClientLocator(locator);
    } else if (smartClientLocator != null) {
      // Then, try to get the locator as set by the setSmartClientLocator function.
      return smartClientLocator;
    } else {
      throw new IllegalStateException(
          "It is not possible to get the SmartClient locator of this canvas. "
              + "When possible, you should instantiate the Canvas calling the "
              + "constructor with the objectString as parameter, because it will"
              + " always set the locator properly. If it is not possible to get "
              + "the object string, then you have to set the locator manually "
              + "calling the function setSmartClientLocator before executing any action.");
    }
  }

  /**
   * Clean the SmartClient locator returned by SmartClient.
   *
   * XXX this function is required to workaround some problems on the locator that returns Smart
   * Client:
   *
   * - When the element is not visible, it will return a negative index. In order to check if the
   * element exists but is not visible, we need this index to be positive.
   *
   * - There is a bug when the text of the item has a slash, so the locator has to be properly
   * escaped.
   *
   * @param locator
   *          The SmartClient locator of the canvas, as returned by SmartClient.
   * @return a cleaned locator to avoid the issues in SmartClient.
   */
  protected String cleanSmartClientLocator(String locator) {
    // Replace the negative index, that means that the item is not visible. This is necessary
    // to check for the item existence and visibility and wait for it to appear. It probably won't
    // work if the index is other than 1.
    String cleanedLocator = locator.replaceAll("index=-1", "index=1");
    // SmartClient has an error when escaping the slash.
    cleanedLocator = cleanedLocator.replaceAll("%24fs%24", "%2f");
    return cleanedLocator;
  }

  /**
   * Waits until the canvas is loaded.
   */
  protected void waitUntilLoaded() {
    if (objectString != null) {
      // XXX: <performance issues> Fluent Wait temporally increased by 30 seconds (30000ms) to
      // specifically assess whether the ReturnToVendor window taking time to load is related to a
      // performance issue of the CI machines. Revert changeset 5550 when done
      Sleep.setFluentWait(300, 40000, TimeoutException.class)
          .until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
              logger.trace("Waiting until Canvas '{}' is loaded.", objectString);
              Boolean result;
              try {
                result = (Boolean) SeleniumSingleton.INSTANCE
                    .executeScriptWithReturn(objectString + " != null")
                    && (Boolean) SeleniumSingleton.INSTANCE
                        .executeScriptWithReturn(objectString + ".getLocator != null");
              } catch (Exception e) {
                logger.debug("Exception happened using executeScriptWithReturn");
                logger.debug("{} != null is {}", objectString,
                    SeleniumSingleton.INSTANCE.executeScriptWithReturn(objectString + " != null"));
                logger.debug("{}.getLocator != null is {}", objectString, SeleniumSingleton.INSTANCE
                    .executeScriptWithReturn(objectString + ".getLocator != null"));
                return false;
              }
              return result;
            }
          });
    }
    // TODO what to do if the object string wasn't defined?
  }

  /**
   * Get information from the canvas, that will be added to the log.
   *
   * @return information from the canvas.
   */
  protected String getInformationForLog() {
    StringBuilder information = new StringBuilder();
    information.append("{");
    if (objectString != null || smartClientLocator != null) {
      try {
        String title = getTitle();
        if (title != null && !title.isEmpty()) {
          information.append(String.format("Title='%s' ", title));
        }
      } catch (WebDriverException exception) {
        // The object doesn't have a title attribute. Just don't add it to the log.
        // TODO if this is the case, then the class inheriting from Canvas should override this
        // function throwing an UnsupportedOperationException.
      }
      try {
        String text = getText();
        if (text != null && !text.isEmpty()) {
          information.append(String.format("Text='%s' ", text));
        }
      } catch (WebDriverException exception) {
        // The object doesn't have a text attribute. Just don't add it to the log.
        // TODO if this is the case, then the class inheriting from Canvas should override this
        // function throwing an UnsupportedOperationException.
      }
      if (objectString != null) {
        information.append(String.format("Object string='%s'", objectString));
      } else {
        information.append(String.format("SmartClient locator='%s'", smartClientLocator));
      }
    } else {
      throw new IllegalStateException(
          "It is not possible to get information from this canvas because it doesn't have an object string nor a SmartClient locator defined.");
    }
    information.append("}");
    return information.toString();
  }

  /**
   * Return true if the canvas is present.
   *
   * @return true if the canvas is present. Otherwise, false.
   */
  public boolean isPresent() {
    try {
      logger.trace("Checking if element is present: {}.getLocator()", objectString);
      SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator(), true);
      logger.trace("Checked that element was present: {}", this::getSmartClientLocator);
    } catch (final NoSuchElementException exception) {
      return false;
    } catch (Exception e) {
      logger.trace(e::getMessage);
      return false;
    }
    return true;
  }

  /**
   * Return true if the canvas is visible.
   *
   * @return true if the canvas is visible. Otherwise, false.
   */
  public boolean isVisible() {
    if (objectString != null) {
      logger.trace("Checking if element is visible: {}",
          String.format(FORMAT_SMART_CLIENT_IS_VISIBLE, objectString));
      // It is safer to call the isVisible function of SmartClient directly.
      Boolean result = (Boolean) SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_IS_VISIBLE, objectString));
      logger.trace("Checked that element was visible, result: {}", result);
      return result;
    } else {
      return SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
          .isDisplayed();
    }
  }

  /**
   * Return true if the canvas is enabled.
   *
   * @return true if the canvas is enabled. Otherwise, false.
   */
  public boolean isEnabled() {
    if (objectString != null && (Boolean) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format("%s.isDisabled != null", objectString))) {
      // It is safer to call the isDisabled function of SmartClient directly.
      final Boolean isDisabled = (Boolean) SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_IS_DISABLED, objectString));
      // TODO help and about links have a disabled function, but it returns null always. Find a
      // better solution, to keep the check as isDisabled != false.
      return isDisabled == null ? true : !isDisabled;
    } else {
      try {
        return isEnabledSmartClient();
        // return SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
        // .isEnabled();
      } catch (StaleElementReferenceException exception) {
        logger.warn(exception::getMessage);
        // Retry.
        return isEnabledSmartClient();
        // return SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
        // .isEnabled();
      }
    }
  }

  /**
   * Return true if the canvas is enabled using the smartclient functions
   *
   * @return true if the canvas is enabled. Otherwise, false.
   */
  public boolean isEnabledSmartClient() {

    final Boolean isDisabled = (Boolean) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(smartClientLocator + "==null;");

    return isDisabled == null ? true : !isDisabled;
  }

  /**
   * Return true if the canvas has the focus.
   *
   * @return true if the canvas has the focus. Otherwise, false.
   */
  public boolean isFocused() {
    if (objectString != null) {
      // It is safer to call the isFocused function of SmartClient directly.
      Boolean isFocused = (Boolean) SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_HAS_FOCUS, objectString));
      if (isFocused == null) {
        return false;
      } else {
        return isFocused;
      }
    } else {
      throw new NotImplementedException(
          "The verification to check if an HTML element is focused using Selenium is not yet implemented.");
    }
  }

  /**
   * Wait until the canvas is visible.
   */
  public void waitUntilVisible() {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting until element {} is visible.", getInformationForLog());
        Boolean visible = isPresent();
        logger.trace("visible(present)={}", visible);
        visible = visible && isVisible();
        logger.trace("visible(visible)={}", visible);
        return visible;
      }
    });
  }

  /**
   * Wait until the canvas is not visible.
   */
  public void waitUntilNotVisible() {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting until element {} is not visible.", getInformationForLog());
        return !isPresent() || !isVisible();
      }
    });
  }

  /**
   * Wait until the canvas is enabled.
   */
  public void waitUntilEnabled() {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting until element {} is enabled.", getInformationForLog());
        return isEnabled();
      }
    });
  }

  /**
   * Set the focus in the canvas.
   */
  public void focus() {
    logger.trace("Focusing element {}.", this::getInformationForLog);
    if (objectString != null) {
      SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_FOCUS_IN_ITEM, objectString));
      // TODO: <chromedriver> Specific method for Chrome to focus on element when previous if
      // condition is not met
    } else if (getSmartClientLocator() != null && SeleniumSingleton.runsChrome()) {
      SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_WEB_ELEMENT_FOCUS,
              String.format(FORMAT_WEB_ELEMENT_SMART_CLIENT_LOCATOR, getSmartClientLocator())));
    } else {
      logger
          .info("The function that focuses an HTML element using Selenium is not yet implemented.");
    }
  }

  /**
   * Click the canvas.
   */
  public void click() {
    logger.trace("Clicking element {}", this::getInformationForLog);
    assertTrue(String.format("Element %s should be enabled.", getInformationForLog()), isEnabled());
    // TODO hover();
    try {
      // FIXME: <chromedriver> Replacing the standard way of clicking on an element by a
      // smartClientClick
      if (!SeleniumSingleton.runsChrome()) {
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator(), true).click();
      } else {
        try {
          // Trying to click via Selenium API with the SmartClient locator
          SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator(), true).click();
        } catch (WebDriverException wde1) {
          // Trying to click on the standard WebElement concatenating it with
          // the .click() method
          logger.debug("<chromedriver> exception caught in click(): \n{}", wde1::getMessage);
          try {
            Sleep.trySleep(1000);
            smartClientClick();
          } catch (WebDriverException wde2) {
            // Trying to click on the standard WebElement obtained via SmartClient concatenating it
            // with the .click() method
            logger.debug("<chromedriver> exception caught in click() (2): \n{}", wde2::getMessage);
            String objStrAux = objectString;
            objectString = String.format("window.isc.AutoTest.getElement(\"%s\")",
                getSmartClientLocator());
            smartClientClick();
            objectString = objStrAux;
          }
        }
      }
    } catch (StaleElementReferenceException exception) {
      logger.warn(exception::getMessage);
      // retry.
      // TODO L1: Review the following sleep
      Sleep.trySleep(6000);
      SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).click();
    } catch (ElementNotVisibleException | NoSuchElementException e1) {
      // This is a workaround made when updating from FF19 & Selenium2.33 to FF33 & Selenium2.44
      // Test ADMe failed to create a Price List Version since the scroll did not automatically put
      // the first field visible.
      // So the solution is to catch the exception and force the scroll to get the field into view.
      // Note: Probably this focusing could have been moved to be the standard behavior, but I
      // prefer to keep it as an exception since it is like this for us.
      // Note 2: I do not understand why the update failed, but I've tested other FF and Selenium
      // combinations and as low as FF24 and Selenium2.37 the issue can be reproduced.
      logger.warn("{} exception caught. ", e1::getMessage);
      logger.warn(
          "Interaction with field [{}] is not possible since it is not visible, forcing the scroll and waiting for element to become visible.",
          this::getSmartClientLocator);
      WebElement obj = SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator());
      SeleniumSingleton.INSTANCE.executeScript("arguments[0].scrollIntoView(false);", obj);
      Sleep.explicitWaitForVisibilityOfElement(obj);
      try {
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
            .sendKeys(Keys.END);
      } catch (ElementNotInteractableException enie) {
        logger.warn("ElementNotInteractableException .Exception: {}", enie::getMessage);
        Actions actionToClick = new Actions(SeleniumSingleton.INSTANCE);
        actionToClick
            .sendKeys(SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()),
                Keys.END)
            .build()
            .perform();
      }
      SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).click();
    } catch (WebDriverException wde) {
      // This extra catch is required because "WebDriverException" started to be thrown after
      // updating to Geckodriver 0.15.0 + Se 3.3.1 + FF 52.1.2esr. It is now required sometimes to
      // wait before clicking.
      logger.warn("{} exception caught. ", wde::getMessage);
      logger.warn(
          "Interaction with field [{}] is not possible since it is not visible, forcing the scroll and waiting for element to become visible.",
          this::getSmartClientLocator);
      WebElement obj = SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator());
      SeleniumSingleton.INSTANCE.executeScript("arguments[0].scrollIntoView(false);", obj);
      Sleep.explicitWaitForVisibilityOfElement(obj);
      // Might need to add this to a retry loop
      // TODO L1: Test to remove the following lines ###FROM HERE### and check if they are required

      try {
        // This extra try is required because "Element not visible" started to be thrown after
        // updating to geckodriver + Se 3.3.1 + FF 52.0.2esr. It is now required to wait before
        // "sendKeys(Keys.END)" in this specific case.
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
            .sendKeys(Keys.END);
      } catch (ElementNotVisibleException enve) {
        logger.warn("ElementNotVisibleException .Exception: {}", enve::getMessage);
        // TODO L: Remove this static sleep when it is no longer required
        Sleep.trySleep(5000);
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
            .sendKeys(Keys.END);
      } catch (ElementNotInteractableException enie) {
        logger.warn("ElementNotInteractableException .Exception: {}", enie::getMessage);
        Actions actions = new Actions(SeleniumSingleton.INSTANCE);
        actions.sendKeys(Keys.TAB).build().perform();
      }
      // ###TO HERE###

      try {
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).click();
      } catch (ElementNotInteractableException enie) {
        logger.warn("ElementNotInteractableException .Exception: {}", enie::getMessage);
        Actions actionToClick = new Actions(SeleniumSingleton.INSTANCE);
        // In case it does not work, we can try the following:
        // actionToClick.moveToElement(SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())).click().build().perform();
        actionToClick
            .click(SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()))
            .build()
            .perform();
      }
    }
  }

  /**
   * Right click the canvas and keep the context menu.
   */
  public void rightClickAndKeepContextMenu() {
    logger.trace("Right clicking element {}.", this::getInformationForLog);
    assertTrue(String.format("Element %s should be enabled.", getInformationForLog()), isEnabled());
    Actions action = new Actions(SeleniumSingleton.INSTANCE);
    try {
      // It is required to use ARROW_DOWN after right click to keep the contextual menu. Otherwise,
      // it would just appear and disappear.
      action
          .contextClick(
              SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator(), true))
          .sendKeys(Keys.ARROW_DOWN)
          .build()
          .perform();
    } catch (StaleElementReferenceException exception) {
      logger.warn(exception::getMessage);
      // retry.
      action
          .contextClick(
              SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator(), true))
          .sendKeys(Keys.ARROW_DOWN)
          .build()
          .perform();
    } catch (ElementNotVisibleException | MoveTargetOutOfBoundsException e2) {
      // This is a workaround made when updating from FF19 & Selenium2.33 to FF33 & Selenium2.44
      // Test ADMe failed to create a Price List Version since the scroll did not automatically put
      // the first field visible.
      // So the solution is to catch the exception and force the scroll to get the field into view.
      // Note: Probably this focusing could have been moved to be the standard behavior, but I
      // prefer to keep it as an exception since it is like this for us.
      // Note 2: I do not understand why the update failed, but I've tested other FF and Selenium
      // combinations and as low as FF24 and Selenium2.37 the issue can be reproduced.
      logger.warn(
          "Interaction with field [{}] is not possible since it is not visibile, forcing the scroll.",
          this::getSmartClientLocator);
      WebElement obj = SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator());
      SeleniumSingleton.INSTANCE.executeScript("arguments[0].scrollIntoView(false);", obj);
      try {
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
            .sendKeys(Keys.END);
      } catch (ElementNotInteractableException enie) {
        logger.warn("ElementNotInteractableException .Exception: {}", enie::getMessage);
        Actions actionToClick = new Actions(SeleniumSingleton.INSTANCE);
        actionToClick
            .sendKeys(SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()),
                Keys.END)
            .build()
            .perform();
      }
      action
          .contextClick(SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()))
          .sendKeys(Keys.ARROW_DOWN)
          .build()
          .perform();
    }
  }

  /**
   * Press the enter key on the canvas.
   */
  public void pressEnter() {
    logger.trace("Pressing enter on element {}.", this::getInformationForLog);
    SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).sendKeys(Keys.ENTER);
  }

  /**
   * Press the delete key on the canvas.
   */
  public void pressDelete() {
    logger.trace("Pressing delete on element {}.", this::getInformationForLog);
    SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
        .sendKeys(Keys.DELETE);
  }

  /**
   * Press the tab key on the canvas.
   */
  public void pressTab() {
    logger.trace("Pressing tab on element {}.", this::getInformationForLog);
    try {
      SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).sendKeys(Keys.TAB);
    } catch (WebDriverException wde1) {
      logger.debug("<chromedriver> exception caught in pressTab(): \n{}", wde1::getMessage);
      Actions actions = new Actions(SeleniumSingleton.INSTANCE);
      actions.sendKeys(Keys.TAB).build().perform();
      // TODO: <chromedriver> maybe remove, only happened once
      // smartClientSetValue(FORMAT_SMART_CLIENT_FOCUS_IN_ITEM);
    }
  }

  /**
   * Get the canvas title.
   *
   * @return the canvas title.
   */
  public String getTitle() {
    if (objectString != null) {
      return (String) SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_TITLE, objectString));
    } else {
      return getAttribute(Attribute.TITLE);
    }
  }

  /**
   * Get the text of the canvas.
   *
   * @return the text of the canvas.
   */
  public String getText() {
    try {
      return SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).getText();
    } catch (StaleElementReferenceException e) {
      logger.warn(
          "StaleElementReferenceException for locator [{}], in Canvas.getText(), trying it once again...",
          this::getSmartClientLocator);
      return SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).getText();
    }
  }

  /**
   * Get the value of an attribute of the canvas.
   *
   * @param attribute
   *          The attribute to get.
   * @return the value of the attribute.
   */
  public String getAttribute(Attribute attribute) {
    String result;
    try {
      result = SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator(), true)
          .getAttribute(attribute.getName());
    } catch (StaleElementReferenceException e) {
      result = SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
          .getAttribute(attribute.getName());
    }
    return result;
  }

  /**
   * Call the SmartClient click function directly.
   *
   * XXX Avoid this function. It was added because:
   *
   * - The button that closes the form to switch back to the grid view is not working correctly. The
   * form doesn't close.
   *
   * - The complete button has a different title than the displayed. See issue 17409 -
   * https://issues.openbravo.com/view.php?id=17409
   *
   * - The button that closes the message was not working correctly. The buttons bar disappears and
   * the message bar doesn't.
   */
  public void smartClientClick() {
    if (objectString != null) {
      logger.trace("Performing a smartClientClick on: {}", objectString);
      // TODO: <chromedriver> Adding wait when clicking on form save button in the Product Category
      // window of ADMe test
      if (SeleniumSingleton.runsChrome()) {
        try {
          waitUntilEnabled();
        } catch (WebDriverException wde) {
          logger.warn("Exception caught in smartClientClick() method: {}", wde::getMessage);
        }
      }
      try {
        SeleniumSingleton.INSTANCE
            .executeScriptWithReturn(String.format("%s.click()", objectString));
      } catch (JavascriptException jse) {
        // In case there was a JavascriptException for reasons such as "undefined object" we wait
        // and try again
        // TODO L1: Check after stabilizing due to appliance upgrade if the following sleep is still
        // required
        Sleep.trySleep(10000);
        SeleniumSingleton.INSTANCE
            .executeScriptWithReturn(String.format("%s.click()", objectString));
      }
    } else {
      throw new SmartClientNotDefinedException(
          "The SmartClient click can be only executed if the object string is defined.");
    }
  }

  /**
   * Call the SmartClient click function directly.
   *
   * XXX Avoid this function. It was added because:
   *
   * - The enter text in dates was not working correctly.
   *
   * @param value
   *          The value to set.
   */
  public void smartClientSetValue(String value) {
    if (objectString != null) {
      SeleniumSingleton.INSTANCE
          .executeScript(String.format("%s.setValue('%s')", objectString, value));
    } else {
      throw new SmartClientNotDefinedException(
          "The SmartClient set value can be only executed if the object string is defined.");
    }
  }

  /**
   * Set the focus in the canvas.
   */
  public void virtualClick() {
    logger.trace("Focusing element {}.", this::getInformationForLog);
    SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_TAB_FOCUS, objectString));
  }

  /**
   * Call the SmartClient blur function directly.
   */
  public void smartClientBlur() {
    if (objectString != null) {
      SeleniumSingleton.INSTANCE.executeScript(String.format("%s.blur()", objectString));
    } else {
      throw new SmartClientNotDefinedException(
          "The SmartClient set value can be only executed if the object string is defined.");
    }
  }

  /**
   * Replaces canvas click for NavBar items.
   */
  public void showMenuClick() {
    logger.trace("Showing the menu for NavBar element {}.", this::getInformationForLog);
    if (objectString != null) {
      SeleniumSingleton.INSTANCE.executeScript(String.format("%s.showMenu()", objectString));
    } else {
      throw new SmartClientNotDefinedException(
          "The SmartClient set value can be only executed if the object string is defined.");
    }
  }

  /**
   * Replaces canvas click for NavBar items.
   */
  public void hideMenuClick() {
    logger.trace("Hiding the menu for NavBar element {}.", this::getInformationForLog);
    if (objectString != null) {
      SeleniumSingleton.INSTANCE.executeScript(String.format("%s.menu.hide()", objectString));
    } else {
      throw new SmartClientNotDefinedException(
          "The SmartClient set value can be only executed if the object string is defined.");
    }
  }

  public void scrollToTop() {
    if (objectString != null) {
      SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format("%s.scrollToTop()", objectString));
    } else {
      throw new SmartClientNotDefinedException(
          "The SmartClient click can be only executed if the object string is defined.");
    }
  }

  public void scrollToBottom() {
    if (objectString != null) {
      SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format("%s.scrollToBottom()", objectString));
    } else {
      throw new SmartClientNotDefinedException(
          "The SmartClient click can be only executed if the object string is defined.");
    }
  }

  @Deprecated
  public String getObjectString() {
    return this.objectString;
  }
}
