/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.OBDate;

/**
 * Executes actions on OpenbravoERP date items built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class DateItem implements InputField {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Formatting string to locate the text box. The parameter is the form item locator. */
  private final static String FORMAT_LOCATOR_SMART_CLIENT_TEXT_FIELD = "%s/item[name=dateTextField]";

  /**
   * Formatting string used to get the date text field of a SmartClient date item. The parameter is
   * the object string of the date item.
   */
  private final static String FORMAT_SMART_CLIENT_GET_DATE_TEXT_ITEM = "%s.dateTextField";

  /* Components. */
  /** The date text item component. */
  private TextItem textField;

  // TODO the button.
  // TODO the pop up.

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public DateItem(String objectString) {
    textField = new DateTextItem(
        String.format(FORMAT_SMART_CLIENT_GET_DATE_TEXT_ITEM, objectString));
  }

  /**
   * Class constructor.
   *
   * This constructor will just instantiate the date item, but after this the date item will not be
   * ready to be used. It has to be initialized using the function setSmartClientLocator in order to
   * be used with Selenium.
   *
   * This is an alternate way to access the SmartClient object, and should be used only if there is
   * no way to get the object string. Always prefer to call the other constructor with the object
   * string as a parameter, because it will set the SmartClient Locator. With this method the
   * actions that require to call SmartClient functions and attributes directly will not be
   * available.
   */
  public DateItem() {
  }

  /**
   * Set the SmartClient Locator of the date item.
   *
   * @param smartClientLocator
   *          The SmartClient locator of the item.
   */
  public void setSmartClientLocator(String smartClientLocator) {
    textField = new TextItem();
    textField.setSmartClientLocator(
        String.format(FORMAT_LOCATOR_SMART_CLIENT_TEXT_FIELD, smartClientLocator));
  }

  /**
   * Click the date item.
   */
  public void click() {
    logger.trace("Clicking date item.");
    textField.click();
  }

  /**
   * Press the enter key on the date item.
   */
  public void pressEnter() {
    logger.trace("Pressing enter on the date item.");
    textField.pressEnter();
  }

  /**
   * Clear the contents of the date item.
   */
  public void clear() {
    logger.trace("Clearing the contents of the date item.");
    textField.clear();
  }

  /**
   * Enter a text on the date item.
   *
   * @param value
   *          The text to enter.
   */
  @Override
  public void setValue(Object value) {
    if (value instanceof String) {
      // OBDate.getDateFromTag in case some tag is being used. E.g. #currentDate#
      textField.setValue(OBDate.getDateFromTag((String) value));
    } else {
      throw new IllegalArgumentException("Valid values for date items are only Strings.");
    }
  }

  /**
   * Get the text of the date item.
   */
  @Override
  public String getValue() {
    return (String) OBDate.getDateFromTag(textField.getValue());
  }

  /**
   * Get the title of the date item.
   */
  @Override
  public String getTitle() {
    return textField.getTitle();
  }

  /**
   * Set the focus to the date item.
   */
  @Override
  public void focus() {
    textField.focus();
  }

  /**
   * Return true if the date item is present.
   *
   * @return true if the date item is present. Otherwise, false.
   */
  @Override
  public boolean isPresent() {
    // TODO check for the button too.
    return textField.isPresent();
  }

  /**
   * Return true if the date item is visible.
   *
   * @return true if the date item is visible. Otherwise, false.
   */
  @Override
  public boolean isVisible() {
    // TODO check for the button too.
    return textField.isVisible();
  }

  /**
   * Return true if the date item is enabled.
   *
   * @return true if the date item is enabled. Otherwise, false.
   */
  @Override
  public boolean isEnabled() {
    // TODO check for the button too.
    return textField.isEnabled();
  }

  /**
   * Wait until the date item is enabled.
   */
  @Override
  public void waitUntilEnabled() {
    // TODO check for the button too.
    textField.waitUntilEnabled();
  }

  /**
   * Executes actions on OpenbravoERP the text fields of date items built upon SmartClient user
   * interface library.
   *
   * XXX this class was added because the text items of the dates behave weird, so some functions
   * had to be overridden with workarounds.
   *
   * @author elopio
   */
  private class DateTextItem extends TextItem {

    /**
     * Class constructor.
     *
     * @param objectString
     *          JavaScript expression that can be evaluated to get a reference to the SmartClient
     *          GUI object.
     */
    public DateTextItem(String objectString) {
      super(objectString);
    }

    /**
     * Clear the contents of the text item.
     */
    // XXX The clear using backspace doesn't work with the date item in the form.
    @Override
    public void clear() {
      logger.trace("Clearing the contents of the date text item {}.", this::getInformationForLog);
      try {
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).clear();
      } catch (StaleElementReferenceException e) {
        // Stale exception was repeating the previous command, creating an Element Not Visible
        // exception but preventing below catch to happen. So I copied what is done for focusing the
        // element here.
        logger.warn(
            "Interaction with field [{}] is not possible since it is not visibile, forcing the scroll.",
            getSmartClientLocator());
        WebElement obj = SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator());
        SeleniumSingleton.INSTANCE.executeScript("arguments[0].scrollIntoView(false);", obj);
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
            .sendKeys(Keys.END);
      } catch (ElementNotVisibleException e2) {
        // This is a workaround made when updating from FF19 & Selenium2.33 to FF33 & Selenium2.44
        // Test ADMe failed to create a Price List Version since the scroll did not automatically
        // put
        // the first field visible.
        // So the solution is to catch the exception and force the scroll to get the field into
        // view.
        // Note: Probably this focusing could have been moved to be the standard behavior, but I
        // prefer to keep it as an exception since it is like this for us.
        // Note 2: I do not understand why the update failed, but I've tested other FF and Selenium
        // combinations and as low as FF24 and Selenium2.37 the issue can be reproduced.
        logger.warn(
            "Interaction with field [{}] is not possible since it is not visibile, forcing the scroll.",
            this::getSmartClientLocator);
        WebElement obj = SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator());
        SeleniumSingleton.INSTANCE.executeScript("arguments[0].scrollIntoView(false);", obj);
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
            .sendKeys(Keys.END);
      }
    }

    /**
     * Set the value of the text item.
     *
     * @param value
     *          The new value of the check box.
     */
    // XXX The enter text is not working properly.
    @Override
    public void setValue(Object value) {
      String text = (String) value;
      logger.trace("Entering the text '{}' into the date text item {}.", text,
          getInformationForLog());
      smartClientSetValue(text);
      smartClientBlur();
    }
  }

}
