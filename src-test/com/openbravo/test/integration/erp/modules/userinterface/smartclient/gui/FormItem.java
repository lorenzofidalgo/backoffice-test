/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on OpenbravoERP form items built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class FormItem extends Canvas {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Formatting string to locate the form. The parameter is the object string. */
  private final static String FORMAT_SMART_CLIENT_GET_FORM_LOCATOR = "%s.form.getLocator()";
  /**
   * Formatting string to locate the item starting form the form location. Both parameters are the
   * same object string of the form item.
   */
  private final static String FORMAT_SMART_CLIENT_GET_ITEM_LOCATOR = "%s.form.getItemLocator(%s)";
  /**
   * Formatting string to locate the form item. The first parameter is the form locator as returned
   * by SmartClient and the second is the item locator as returned by SmartClient.
   */
  private final static String FORMAT_LOCATOR_SMART_CLIENT_FORM_ITEM = "%s/%s";

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public FormItem(String objectString) {
    super(objectString);
  }

  /**
   * Class constructor.
   *
   * This constructor will just instantiate the form item, but after this the form item will not be
   * ready to be used. It has to be initialized using the function setSmartClientLocator in order to
   * be used with Selenium.
   *
   * This is an alternate way to access the SmartClient object, and should be used only if there is
   * no way to get the object string. Always prefer to call the other constructor with the object
   * string as a parameter, because it will set the SmartClient Locator. With this method the
   * actions that require to call SmartClient functions and attributes directly will not be
   * available.
   */
  public FormItem() {
  }

  /**
   * Waits until the canvas is loaded.
   */
  @Override
  protected void waitUntilLoaded() {
    if (objectString != null) {
      SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver webDriver) {
          logger.trace("Waiting until Canvas '{}' is loaded.", objectString);
          return (Boolean) SeleniumSingleton.INSTANCE
              .executeScriptWithReturn(objectString + " != null")
              && (Boolean) SeleniumSingleton.INSTANCE
                  .executeScriptWithReturn(objectString + ".form.getLocator != null");
        }
      });
    }
    // TODO what to do if the object string wasn't defined?
  }

  /**
   * Get the SmartClient locator of the form. This is the path to the form that contains this item.
   *
   * @return the SmartClient locator of the form.
   */
  private String getFormLocator() {
    return (String) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_FORM_LOCATOR, objectString));
  }

  /**
   * Get the relative SmartClient locator of the item. This is the path starting from the form that
   * contains this item.
   *
   * @return the SmartClient locator of the item.
   */
  private String getRelativeItemLocator() {
    return (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format(FORMAT_SMART_CLIENT_GET_ITEM_LOCATOR, objectString, objectString));
  }

  /**
   * Get the full SmartClient locator of the item. This is the full path, including the form locator
   * and the item locator.
   *
   * @return the full SmartClient locator of the item.
   */
  @Override
  protected String getSmartClientLocator() {
    if (objectString != null && (Boolean) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(objectString + ".getLocator == null")) {
      // There is no getLocator function on this object, so the locator will have to be created by
      // the concatenation of the form locator and the item relative locator.
      String scLocator = String.format(FORMAT_LOCATOR_SMART_CLIENT_FORM_ITEM, getFormLocator(),
          getRelativeItemLocator());
      return cleanSmartClientLocator(scLocator);
    } else {
      return super.getSmartClientLocator();
    }
  }
}
