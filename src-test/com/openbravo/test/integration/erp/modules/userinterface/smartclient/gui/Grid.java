/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *  Nono Carballo <f.carballo@nectus.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.modules.client.application.gui.GridInputFieldFactory;
import com.openbravo.test.integration.selenium.AlertHandler;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;
import com.openbravo.test.integration.util.TestUtils;

/**
 * Executes actions on OpenbravoERP grids built upon SmartClient user interface library.
 *
 * TODO should this class be moved to client.application.gui?
 *
 * @author elopio
 *
 */
public class Grid extends Canvas {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Formatting string used to make a row visible. The first parameter is the object string. The
   * second parameter is the row number.
   */
  private static final String FORMAT_SMART_CLIENT_MAKE_ROW_VISIBLE = "%s.scrollRecordIntoView(%d)";

  /**
   * Formatting string used scroll the grid to the left. The first parameter is the object string.
   */
  private static final String FORMAT_SMART_CLIENT_SCROLL_LEFT = "%s.body.scrollToLeft()";

  /**
   * Formatting string to focus the grid. The parameter is the object string.
   */
  private static final String FORMAT_SMART_CLIENT_FOCUS_GRID = "%s.focus()";

  /**
   * Formatting string used scroll the grid to the left. The first parameter is the object string.
   */
  private static final String FORMAT_SMART_CLIENT_SCROLL_RIGHT = "%s.body.scrollToRight()";
  /**
   * Formatting string used to get the first visible row index. The parameter is the object string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_FIRST_VISIBLE_ROW_INDEX = "%s.getVisibleRows()[0]";
  /**
   * Formatting string used to get the last visible row index. The parameter is the object string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_LAST_VISIBLE_ROW_INDEX = "%s.getVisibleRows()[1]";
  /**
   * Formatting string used to locate a cell. The first parameter is the grid locator. The second is
   * the row locator part and the third is the column locator part.
   */
  private static final String FORMAT_LOCATOR_SMART_CLIENT_CELL = "%s/body/row[%s]/col[%s]";
  /**
   * Formatting string used to locate a row by its identifier. It should be passed as the second
   * parameter of the FORMAT_LOCATOR_SMART_CLIENT_CELL. The parameter is the record identifier.
   */
  private static final String FORMAT_LOCATOR_PART_SMART_CLIENT_ROW_BY_ID = "id=%s";
  /**
   * Formatting string used to locate a row by its contents. It should be passed as the second
   * parameter of the FORMAT_LOCATOR_SMART_CLIENT_CELL. The first parameter is the column name, and
   * the second is the value.
   */
  private static final String FORMAT_LOCATOR_PART_SMART_CLIENT_ROW_BY_CONTENT = "%s=%s";
  /**
   * Formatting string used to locate a column. It should be used as the third parameter of the
   * FORMAT_LOCATOR_CELL. The parameter is the name the column.
   */
  private static final String FORMAT_LOCATOR_PART_SMART_CLIENT_COLUMN = "fieldName=%s";
  /**
   * String used to located the check box column. It should be used as the third parameter of the
   * FORMAT_LOCATOR_CELL.
   */
  private static final String LOCATOR_PART_SMART_CLIENT_CHECK_BOX_COLUMN = "isCheckboxField=true";
  /** Formatting string used to locate the grid filter. The parameter the object string. */
  private static final String FORMAT_SMART_CLIENT_GET_FILTER_LOCATOR = "%s.filterEditor.getLocator()";
  /**
   * Formatting string used to get a filter field by its name or index. The first parameter is the
   * the object string of the grid. The second parameter can be the field name or the field index.
   */
  @SuppressWarnings("unused")
  private static final String FORMAT_SMART_CLIENT_GET_FILTER_FIELD = "%s.filterEditor.getField(%s)";
  /**
   * Formatting string used to the grid clear filter button. The parameter is the object string of
   * the grid.
   */
  private static final String FORMAT_SMART_CLIENT_GET_BUTTON_CLEAR_FILTER = "%s.filterEditor.filterImage";

  private static final String FORMAT_SMART_CLIENT_GET_BUTTON_SELECT_ALL = "%s.headers[0].getButtons()[0]";

  private static final String FORMAT_SMART_CLIENT_GET_BUTTON_COLUMN_HEADER = "%s.headers[1].getButton('%s')";
  /**
   * Formatting string used to locate a grid column filter by its name. The first parameter is the
   * grid filter locator. The second parameter is the column name.
   */
  protected static final String FORMAT_LOCATOR_SMART_CLIENT_FORM_ROW_EDITOR_BY_NAME = "%s/editRowForm/item[name=%s]";
  /**
   * Formatting string used to locate a grid column filter by its index. The first parameter is the
   * grid filter locator. The second parameter is the column index.
   */
  protected static final String FORMAT_LOCATOR_SMART_CLIENT_FORM_ROW_EDITOR_BY_INDEX = "%s/editRowForm/item[index=%s]";

  /**
   * Formatting string used to check if fetching operation is being performed on grid
   */
  private static final String FORMAT_SMART_CLIENT_GRID_FETCHING_DATA = "%s.isFetchingData()";

  /**
   * Formatting string used to select a record using SC javascript
   */
  private static final String FORMAT_SMART_CLIENT_SELECT_RECORD = "%s.selectRecord(%s)";

  /**
   * Formatting string used get rows on grid
   */
  private static final String FORMAT_SMART_CLIENT_GRID_GET_DATA = "%s.getData().localData";

  /**
   * Formatting string used get rows on grid
   */
  private static final String FORMAT_SMART_CLIENT_GRID_GET_SELECTED_DATA = "%s.pneSelectedRecords";

  /**
   * Class constructor.
   *
   * @param objectString
   *          the string that can be used with SeleniumSingleton.INSTANCE.getEval function to get
   *          the object.
   * @param waitForLoad
   *          Set to true if it is required to wait until it is loaded
   */
  public Grid(String objectString, Boolean waitForLoad) {
    super(objectString, waitForLoad);
  }

  /**
   * Class constructor.
   *
   * @param objectString
   *          the string that can be used with SeleniumSingleton.INSTANCE.getEval function to get
   *          the object.
   */
  public Grid(String objectString) {
    this(objectString, true);
  }

  /**
   * Get the SmartClient locator of the filter.
   *
   * @return the SmartClient locator of the filter.
   */
  public String getFilterLocator() {
    String locator = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format(FORMAT_SMART_CLIENT_GET_FILTER_LOCATOR, objectString));
    return locator;
  }

  /**
   *
   * Get the index of the first visible row.
   *
   * @return the index of the first visible row.
   */

  public int getFirstVisibleRowIndex() {
    return Integer.parseInt((String) (SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format(FORMAT_SMART_CLIENT_GET_FIRST_VISIBLE_ROW_INDEX, objectString))).toString());
  }

  /**
   *
   * Get the index of the last visible row.
   *
   * @return the index of the last visible row.
   */

  public int getLastVisibleRowIndex() {
    return Integer.parseInt((String) (SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format(FORMAT_SMART_CLIENT_GET_LAST_VISIBLE_ROW_INDEX, objectString))).toString());
  }

  /**
   * Make a row visible.
   *
   * @param rowNumber
   *          The number of the row.
   */
  public void makeRowVisible(int rowNumber) {
    logger.trace("Making visible the row number {}.", rowNumber);
    SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format(FORMAT_SMART_CLIENT_MAKE_ROW_VISIBLE, objectString, rowNumber));
  }

  /**
   * Click on the column header button of the given name
   *
   * @param colName
   *          The name of the column to sort by
   */
  public void clickColumnHeader(String colName) {
    logger.trace("Sorting by {}", colName);

    String objectStringButtonSort = String.format(FORMAT_SMART_CLIENT_GET_BUTTON_COLUMN_HEADER,
        objectString, colName);
    final Button buttonColumnHeaderName = new Button(objectStringButtonSort);
    if (buttonColumnHeaderName.isVisible()) {
      // Clicks on column header to sort
      buttonColumnHeaderName.smartClientClick();
      // Testing to avoid "batches" false positives
      Sleep.trySleep(1500);
      this.waitUntilLoaded();
      Sleep.trySleep(1500);

      // TODO: check if already select to decide if 0 or 1 clicks are needed
    } else {
      logger.debug("Check button for selection is no visible");
    }
  }

  /**
   * Sort Ascending using the context menu of the column header button of the given column in the
   * DataObject
   *
   * @param colName
   *          The name of the column to sort by
   */
  public void sortAscending(String colName) {
    logger.trace("Sorting ascending by {}", colName);
    String objectStringButtonSort = String.format(FORMAT_SMART_CLIENT_GET_BUTTON_COLUMN_HEADER,
        objectString, colName);
    final Button buttonColumnHeaderName = new Button(objectStringButtonSort);
    if (buttonColumnHeaderName.isVisible()) {
      // Clicks on column header to sort
      buttonColumnHeaderName.rightClickAndKeepContextMenu();
      // Once the context menu is open, it is only required to click Enter key to Sort Ascending
      SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.ENTER);
      // Testing to avoid "batches" false positives
      Sleep.trySleep(1500);
      this.waitUntilLoaded();
      Sleep.trySleep(1500);
    } else {
      logger.debug("Check button for selection is no visible");
    }
  }

  /**
   * Sort Descending using the context menu of the column header button of the given column in the
   * DataObject
   *
   * @param colName
   *          The name of the column to sort by
   */
  public void sortDescending(String colName) {
    logger.trace("Sorting descending by {}", colName);
    String objectStringButtonSort = String.format(FORMAT_SMART_CLIENT_GET_BUTTON_COLUMN_HEADER,
        objectString, colName);
    final Button buttonColumnHeaderName = new Button(objectStringButtonSort);
    if (buttonColumnHeaderName.isVisible()) {
      // Clicks on column header to sort
      buttonColumnHeaderName.rightClickAndKeepContextMenu();
      // Once the context menu is open, it is only required to click Enter key to Sort Ascending
      SeleniumSingleton.INSTANCE.switchTo().activeElement().sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
      // Testing to avoid "batches" false positives
      Sleep.trySleep(1500);
      this.waitUntilLoaded();
      Sleep.trySleep(1500);
    } else {
      logger.debug("Check button for selection is no visible");
    }
  }

  /**
   * Click a cell by the number of its row and column.
   *
   * @param rowNumber
   *          The row number of the cell.
   * @param columnNumber
   *          The column number of the cell.
   */
  public void clickCell(int rowNumber, int columnNumber) {
    logger.trace("Clicking the cell on row {} and column {}.", rowNumber, columnNumber);
    SeleniumSingleton.INSTANCE
        .findElementByScLocator(String.format(FORMAT_LOCATOR_SMART_CLIENT_CELL,
            getSmartClientLocator(), Integer.toString(rowNumber), Integer.toString(columnNumber)))
        .click();
  }

  /**
   * Select a record number using SC javascript
   *
   * @param rowNumber
   *          The number of the row that wants to be selected
   */
  public void selectRecord(int rowNumber) {
    logger.trace("Selecting record number {}", rowNumber);
    SeleniumSingleton.INSTANCE.executeScript(String.format(FORMAT_SMART_CLIENT_SELECT_RECORD,
        objectString, Integer.toString(rowNumber)));
  }

  /**
   * Click a cell by the number of its row.
   *
   * @param rowNumber
   *          The row number of the cell.
   * @param columnName
   *          The column name of the cell.
   */
  public void clickCellByRowNumber(int rowNumber, String columnName) {
    logger.trace("Clicking the cell on row {} with column name '{}'.", rowNumber, columnName);
    String columnLocatorPart = String.format(FORMAT_LOCATOR_PART_SMART_CLIENT_COLUMN,
        TestUtils.convertFieldSeparator(columnName, false));

    Sleep.setFluentWait(500, WebDriverException.class).until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver driver) {
        try {
          SeleniumSingleton.INSTANCE
              .findElementByScLocator(String.format(FORMAT_LOCATOR_SMART_CLIENT_CELL,
                  getSmartClientLocator(), Integer.toString(rowNumber), columnLocatorPart))
              .click();
          return true;
        } catch (NoSuchElementException | StaleElementReferenceException nsere) {
          throw nsere;
        }
      }
    });
  }

  /**
   * Click a cell by its record identifier.
   *
   * @param recordIdentifier
   *          The record identifier of the cell.
   * @param columnName
   *          The column name of the cell.
   */
  public void clickCellByRowId(String recordIdentifier, String columnName) {
    logger
        .trace(String.format("Clicking the cell with record identifier '%s' and column name '%s'.",
            recordIdentifier, columnName));
    String rowLocatorPart = String.format(FORMAT_LOCATOR_PART_SMART_CLIENT_ROW_BY_ID,
        recordIdentifier);
    String columnLocatorPart = String.format(FORMAT_LOCATOR_PART_SMART_CLIENT_COLUMN,
        TestUtils.convertFieldSeparator(columnName, false));
    SeleniumSingleton.INSTANCE
        .findElementByScLocator(String.format(FORMAT_LOCATOR_SMART_CLIENT_CELL,
            getSmartClientLocator(), rowLocatorPart, columnLocatorPart))
        .click();
  }

  /**
   * Click a cell by its contents.
   *
   * @param columnName
   *          The column name of the cell.
   * @param columnValue
   *          The value of the column.
   */
  public void clickCellByContents(String columnName, String columnValue) {
    logger.trace("Clicking the cell with value '{}' on the column '{}'.", columnValue, columnName);
    String rowLocatorPart = String.format(FORMAT_LOCATOR_PART_SMART_CLIENT_ROW_BY_CONTENT,
        columnName, columnValue);
    String columnLocatorPart = String.format(FORMAT_LOCATOR_PART_SMART_CLIENT_COLUMN,
        TestUtils.convertFieldSeparator(columnName, false));
    SeleniumSingleton.INSTANCE
        .findElementByScLocator(String.format(FORMAT_LOCATOR_SMART_CLIENT_CELL,
            getSmartClientLocator(), rowLocatorPart, columnLocatorPart))
        .click();
  }

  /**
   * Select a row by its number.
   *
   * @param rowNumber
   *          The row number of the cell.
   */
  public void selectRowByNumber(int rowNumber) {
    logger.trace("Selecting the row number {}.", rowNumber);
    String checkBoxLocator = String.format(FORMAT_LOCATOR_SMART_CLIENT_CELL,
        getSmartClientLocator(), Integer.toString(rowNumber),
        LOCATOR_PART_SMART_CLIENT_CHECK_BOX_COLUMN);
    CheckBox rowCheckBox = new CheckBox();
    rowCheckBox.setSmartClientLocator(checkBoxLocator);
    rowCheckBox.check();
  }

  /**
   * Unselect a row by its number.
   *
   * @param rowNumber
   *          The row number of the cell.
   */
  public void unSelectRowByNumber(int rowNumber) {
    logger.trace("Selecting the row number {}.", rowNumber);
    String checkBoxLocator = String.format(FORMAT_LOCATOR_SMART_CLIENT_CELL,
        getSmartClientLocator(), Integer.toString(rowNumber),
        LOCATOR_PART_SMART_CLIENT_CHECK_BOX_COLUMN);
    CheckBox rowCheckBox = new CheckBox();
    rowCheckBox.setSmartClientLocator(checkBoxLocator);
    rowCheckBox.uncheck();
  }

  /**
   * Select a record by its identifier.
   *
   * @param recordIdentifier
   *          The record identifier of the cell.
   */
  public void selectRowById(int recordIdentifier) {
    logger.trace("Selecting the record with identifier '{}'.", recordIdentifier);
    String rowLocatorPart = String.format(FORMAT_LOCATOR_PART_SMART_CLIENT_ROW_BY_ID,
        recordIdentifier);
    String checkBoxLocator = String.format(FORMAT_LOCATOR_SMART_CLIENT_CELL,
        getSmartClientLocator(), rowLocatorPart, LOCATOR_PART_SMART_CLIENT_CHECK_BOX_COLUMN);
    CheckBox rowCheckBox = new CheckBox();
    rowCheckBox.setSmartClientLocator(checkBoxLocator);
    rowCheckBox.check();
  }

  /**
   * Select a record by its contents.
   *
   * @param columnName
   *          The column name of the cell.
   * @param columnValue
   *          The value of the column.
   */
  public void selectRowByContents(String columnName, String columnValue) {
    logger.trace("Selecting the row with value '{}' on the column '{}'.", columnValue, columnName);
    String rowLocatorPart = String.format(FORMAT_LOCATOR_PART_SMART_CLIENT_ROW_BY_CONTENT,
        columnName, columnValue);
    String checkBoxLocator = String.format(FORMAT_LOCATOR_SMART_CLIENT_CELL,
        getSmartClientLocator(), rowLocatorPart, LOCATOR_PART_SMART_CLIENT_CHECK_BOX_COLUMN);
    CheckBox rowCheckBox = new CheckBox();
    rowCheckBox.setSmartClientLocator(checkBoxLocator);
    Sleep.trySleep(800);
    this.waitUntilLoaded();
    rowCheckBox.check();
  }

  /**
   * Clear the filters.
   */
  public void clearFilters() {
    logger.debug("Clearing the filter.");
    String objectStringButtonClearFilter = String
        .format(FORMAT_SMART_CLIENT_GET_BUTTON_CLEAR_FILTER, objectString);
    final Button buttonClearFilter = new Button(objectStringButtonClearFilter);
    if (buttonClearFilter.isVisible()) {
      buttonClearFilter.smartClientClick();
      waitForDataToLoad();
    } else {
      logger.debug("No filters are applied on the grid.");
    }
  }

  /**
   * Filter the grid rows.
   *
   * @param columnName
   *          The name of the column.
   * @param filterValue
   *          The value of the filter.
   */
  public void filter(String columnName, String filterValue) {
    logger.trace("Filtering the grid using the value '{}' on the column '{}'.", filterValue,
        columnName);

    // XXX This is part of a workaround to avoid issue
    // https://issues.openbravo.com/view.php?id=31374
    String usefulTransformedNumber = TestUtils.convertNumericalValueToMeaningfulNumber(filterValue)
        .toString();
    // ...to here

    String fieldSmartClientLocator = String.format(
        FORMAT_LOCATOR_SMART_CLIENT_FORM_ROW_EDITOR_BY_NAME, getFilterLocator(),
        TestUtils.convertFieldSeparator(columnName, false));
    InputField filterInputField = GridInputFieldFactory.getFilterInputFieldObject(objectString,
        columnName, fieldSmartClientLocator);
    filterInputField.focus();
    // TODO L1: Check if this sleep could be upgraded. It would
    // have great speed impact.
    Sleep.trySleep(1500);
    filterInputField.setValue(OBDate.getDateFromTag(usefulTransformedNumber));

    // The following if clause is commented due to conflicts with several test cases
    // if (filterInputField instanceof TextItem) {
    // ((TextItem) filterInputField).pressEnter();
    // }

    // Removed since it was creating a Stale Element Exception
    // if (filterInputField instanceof NumberItem) {
    // logger.trace("Pressing Enter since '{}' is a Numeric field.", columnName);
    // ((NumberItem) filterInputField).pressEnter();
    // }
  }

  /**
   * Filter the grid rows.
   *
   * @param columnName
   *          The name of the column.
   * @param filterValue
   *          The value of the filter.
   */
  public void pickAndExecutefilter(String columnName, String filterValue) {
    logger.trace("Filtering the grid using the value '{}' on the column '{}'.", filterValue,
        columnName);

    // XXX This is part of a workaround to avoid issue
    // https://issues.openbravo.com/view.php?id=31374
    String usefulTransformedNumber = TestUtils.convertNumericalValueToMeaningfulNumber(filterValue)
        .toString();
    // ...to here

    String fieldSmartClientLocator = String.format(
        FORMAT_LOCATOR_SMART_CLIENT_FORM_ROW_EDITOR_BY_NAME, getFilterLocator(),
        TestUtils.convertFieldSeparator(columnName, false));
    InputField filterInputField = GridInputFieldFactory.getFilterInputFieldObject(objectString,
        columnName, fieldSmartClientLocator);
    Sleep.trySleep(3500);
    filterInputField.focus();
    Sleep.trySleep();
    filterInputField.setValue(OBDate.getDateFromTag(usefulTransformedNumber));

    // The following if clause is commented due to conflicts with several test cases
    if (filterInputField instanceof TextItem) {
      ((TextItem) filterInputField).pressEnter();
    }

    // Removed since it was creating a Stale Element Exception
    if (filterInputField instanceof NumberItem) {
      logger.trace("Pressing Enter since '{}' is a Numeric field.", columnName);
      ((NumberItem) filterInputField).pressEnter();
    }
  }

  /**
   * Picks a record in a filter drop down
   *
   * @param columnName
   *          The name of the column where the record is going to be picked
   * @param recordNo
   *          The number of the record that is going to be picked
   */
  public void pickRecordInFilterDropDown(String columnName, int recordNo) {
    logger.trace("Picking record {} in drop down filter {}", recordNo, columnName);
    String s = "var %s.filterEditor.getEditForm().getField('%s');\n" + //
        "dom.showPicker();\n" + //
        "setTimeout(function() {" + //
        "  dom.pickValue([dom.pickList.getRecord(%d)._identifier]);" + //
        "  dom.pickList.selectRecord(%d);" + //
        "}, 2000);";

    SeleniumSingleton.INSTANCE
        .executeScript(String.format(s, objectString, columnName, recordNo, recordNo));

    // TODO: drop down filter selector does not implement smart wait for data
    Sleep.trySleep(2500);
  }

  /**
   * Opens a filter drop down and returns the number of available records to select
   *
   * @param columnName
   *          The name of the column where the drop down filter is going to be opened
   * @return the number of available records to select
   */
  public int openFilterDropDown(String columnName) {
    logger.trace("Opening drop down filter {}", columnName);

    String l = "%s.filterEditor.getEditForm().getField('%s').showPicker()";
    SeleniumSingleton.INSTANCE.executeScript(String.format(l, objectString, columnName));

    // TODO: drop down filter selector does not implement smart wait for data
    Sleep.trySleep(500);

    Long r = (Long) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format("%s.filterEditor.getEditForm().getField('%s').pickList.data.localData.length",
            objectString, columnName));
    return r.intValue();
  }

  public void unselectAll() {
    logger.trace("Unselect all items");
    String objectStringButtonClearFilter = String.format(FORMAT_SMART_CLIENT_GET_BUTTON_SELECT_ALL,
        objectString);
    final Button buttonSelectAll = new Button(objectStringButtonClearFilter);
    if (buttonSelectAll.isVisible() && !isThereRecordsInGrid()) {
      // There are not records in grid, so it is not required unSelecting records
    } else if (buttonSelectAll.isVisible() && isSelectedAllRecords()) {
      // Have to wait until filtering is done. Otherwise, the "batches" false positive happens
      // Sleep.smartWaitExecuteScript(objectString + ".isDataLoaded() == true", 150);
      Sleep.trySleep(1500);
      // 1st click selects all
      buttonSelectAll.smartClientClick();
      // Testing to avoid "batches" false positives
      // Sleep.trySleep(1500);
      this.waitUntilLoaded();
    } else if (buttonSelectAll.isVisible() && !isSelectedAllRecords()) {
      // Have to wait until filtering is done. Otherwise, the "batches" false positive happens
      // Sleep.smartWaitExecuteScript(objectString + ".isDataLoaded() == true", 150);
      Sleep.trySleep(1500);
      // Scroll through all records to unselect those that need to, one by one
      int recNumInGrid = nRecordsInGrid();
      for (int i = 0; i < recNumInGrid; i++) {
        // Finish if all records were unselected or there are no more records to unselect
        if (nSelectedRecordsInGrid() == 0) {
          break;
        }
        makeRowVisible(i);
        waitForDataToLoad();
        unSelectRowByNumber(i);
        waitForDataToLoad();
      }
    } else {
      logger.debug("Check button for selection is no visible");
    }
  }

  public void selectAll() {
    logger.trace("Select all items");
    if (AlertHandler.isAlertPresent()) {
      logger.info("There is an alert window at the beginning of selectAll function");
    }
    String objectStringButtonClearFilter = String.format(FORMAT_SMART_CLIENT_GET_BUTTON_SELECT_ALL,
        objectString);
    final Button buttonSelectAll = new Button(objectStringButtonClearFilter);
    if (buttonSelectAll.isVisible()) {
      // 1st click selects all
      buttonSelectAll.smartClientClick();
      // Testing to avoid "batches" false positives
      Sleep.trySleep(1500);
      this.waitUntilLoaded();
      Sleep.trySleep(1500);
      // END Testing to avoid "batches" false positives
      if (AlertHandler.isAlertPresent()) {
        logger.info(
            "There is an alert window in Grid.selectAll() function. After the buttonSelectAll.smartClientClick();");
      }
      // TODO: check if already select to decide if 0 or 1 clicks are needed
    } else {
      logger.debug("Check button for selection is no visible");
    }
  }

  /**
   * Wait for data to be fully loaded.
   */
  public void waitForDataToLoad() {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(final WebDriver webDriver) {
        logger.debug("Waiting for data to load in grid.");
        Boolean fetching = (Boolean) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
            String.format(FORMAT_SMART_CLIENT_GRID_FETCHING_DATA, objectString));
        if (fetching == null) {
          fetching = true;
        }
        return !fetching;
      }
    });
  }

  @Override
  public void focus() {
    SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_FOCUS_GRID, objectString));
  }

  /**
   * Scrolls to left
   */
  public void scrollToLeft() {
    logger.trace("Scroll grid to left");
    SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_SCROLL_LEFT, objectString));
  }

  /**
   * Scrolls to right
   */
  public void scrollToRight() {
    logger.trace("Scroll grid to right");
    SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_SCROLL_RIGHT, objectString));
  }

  /**
   * Get rows on grid
   */
  public Object getRows() {
    logger.trace("Get rows on grid");
    return SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GRID_GET_DATA, objectString));
  }

  /**
   * Get selected rows on grid
   */
  public Object getSelectedRows() {
    logger.trace("Get selected rows on grid");
    return SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format(FORMAT_SMART_CLIENT_GRID_GET_SELECTED_DATA, objectString));
  }

  /**
   *
   * @return false if the number of records and the number of selected records are different. true
   *         if there are no records or the number of records matches with the number of selected
   *         records.
   */
  public boolean isSelectedAllRecords() {
    // The following isThereRecordsInGrid() method is executed to get interesting log in case it is
    // required
    isThereRecordsInGrid();
    // It is checked the number of data (records) in the client match with the selected records of
    // the grid
    boolean allRecordsSelected = nRecordsInGrid() == nSelectedRecordsInGrid();
    return allRecordsSelected;
  }

  /**
   * @return true if there are records in Grid. Otherwise, false.
   */
  public boolean isThereRecordsInGrid() {
    if (nRecordsInGrid() == 0) {
      logger.trace("There are no records in the grid: data.localData.length === 0");
      return false;
    } else {
      logger.trace("There are records in the grid: data.localData.length !== 0");
      return true;
    }
  }

  /**
   * @return the number of records in Grid
   */
  public int nRecordsInGrid() {
    String gridObject = objectString.replace("dom=", "");
    int nRecordsInGrid;
    try {
      nRecordsInGrid = ((Long) SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(gridObject + ".data.localData.length")).intValue();
    } catch (JavascriptException jse) {
      // Try again in case there was a problem retrieving the value
      Sleep.trySleep(5500);
      nRecordsInGrid = ((Long) SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(gridObject + ".data.localData.length")).intValue();
    }
    return nRecordsInGrid;
  }

  /**
   * @return the number of selected records in Grid
   */
  public int nSelectedRecordsInGrid() {
    String gridObject = objectString.replace("dom=", "");
    int nSelectedRecordsInGrid = ((Long) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(gridObject + ".getSelectedRecords().length")).intValue();
    return nSelectedRecordsInGrid;
  }
}
