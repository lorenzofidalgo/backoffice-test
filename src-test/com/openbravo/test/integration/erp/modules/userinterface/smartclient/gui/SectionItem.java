/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on OpenbravoERP section items built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class SectionItem extends FormItem {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Formatting string to check if the section item is expanded. The parameter is object string. */
  private final static String FORMAT_SMART_CLIENT_IS_EXPANDED = "%s.isExpanded()";
  /** Formatting string to check if the section item is drawn. The parameter is object string. */
  private final static String FORMAT_SMART_CLIENT_IS_VISIBLE = "%s.isVisible()";
  /**
   * Formatting string used to locate the section icon. The parameter is the SmartClient locator of
   * the section item.
   */
  private final static String FORMAT_SMART_CLIENT_LOCATOR_ICON = "%s/canvas/background/icon";

  /* Components. */
  /** The section item icon. */
  private static Canvas icon;

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public SectionItem(String objectString) {
    super(objectString);
    icon = new Canvas();
    icon.setSmartClientLocator(
        String.format(FORMAT_SMART_CLIENT_LOCATOR_ICON, getSmartClientLocator()));
  }

  /**
   * Check if the section item is expanded.
   *
   * @return true if the section item is expanded. Otherwise, false.
   */
  public boolean isExpanded() {
    return (Boolean) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_IS_EXPANDED, objectString));
  }

  /**
   * Check if the section item is visible.
   *
   * @return true if the section item is visible. Otherwise, false.
   */
  @Override
  public boolean isVisible() {
    return (Boolean) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_IS_VISIBLE, objectString));
  }

  /**
   * Expand the section item.
   */
  public void expand() {
    if (!isExpanded()) {
      logger.info("Expand the section item '{}'.", this::getInformationForLog);
      focus();
      icon.click();
      // XXX this was added because the tax rate creation was failing to expand the More information
      // section.
      if (!isExpanded()) {
        logger.warn("The section was not expanded. Try again.");
        icon.click();
        Sleep.trySleep();
      }
    }
  }

  /**
   * Collapse the section item.
   */
  public void collapse() {
    if (isExpanded()) {
      logger.info("Collapse the section item '{}'.", this::getInformationForLog);
      focus();
      icon.click();
    }
  }

  /**
   * Wait until the section item is expanded.
   */
  public void waitUntilExpanded() {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting until section item '{}' is expanded.", getInformationForLog());
        return isExpanded();
      }
    });
  }

  /**
   * Wait until the section item is collapsed.
   */
  public void waitUntilCollapsed() {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting until section item '{}' is collapsed.", getInformationForLog());
        return !isExpanded();
      }
    });
  }
}
