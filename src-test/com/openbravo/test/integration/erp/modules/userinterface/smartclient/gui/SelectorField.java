/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>
 *  Iñaki Garcia <inaki.garcia@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.SelectorDataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.selectors.OBSelectorPopUpWindow;
import com.openbravo.test.integration.selenium.NotImplementedException;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Executes actions on OpenbravoERP selectors built upon SmartClient user interface library.
 *
 * @author elopio
 * @param <T>
 *          The selector data object.
 *
 */
public class SelectorField<T extends SelectorDataObject> extends Canvas implements InputField {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Components. */
  /** The selector item. */
  protected final SelectorItem selectorItem;
  /** The selector pop up window. */
  private OBSelectorPopUpWindow<T> popUp;

  /**
   * Class constructor.
   *
   * @param selectorItem
   *          The selector item of this selector.
   */
  public SelectorField(SelectorItem selectorItem) {
    this.selectorItem = selectorItem;

  }

  /**
   * Set a value on the selector using the pop up.
   *
   * @param value
   *          The value that will be set.
   */
  @SuppressWarnings("unchecked")
  public void selectFromPopUp(Object value) {
    logger.debug("Selecting from the pop up the record with data: '{}'.", value);
    // XXX this is an unsafe cast, and may fail on runtime.
    T selectorObjectValue = (T) value;
    // The field might have the list box with the options opened. If this is the case, the following
    // click will just close the box. Click it in order to make sure
    // it's closed and that the next click will work.
    Sleep.trySleep(800);
    selectorItem.click();
    Sleep.trySleep(400);
    selectorItem.openSelectorPopUp();
    popUp = new OBSelectorPopUpWindow<T>(selectorItem.getSelectorWindowObjectString());
    popUp.waitUntilVisible();
    popUp.waitForDataToLoad();

    // Added to solve a false positive
    // Sleep.trySleep();
    Sleep.trySleep(150);
    popUp.waitForDataToLoad();

    popUp.select(selectorObjectValue);
  }

  /**
   * Open and clears the filters on the selector using the pop up.
   */
  public void openPopUpAndClearFilters() {
    // The field might have the list box with the options opened. If this is the case, the following
    // click will just close the box. Click it in order to make sure
    // it's closed and that the next click will work.
    Sleep.trySleep(800);
    selectorItem.click();
    Sleep.trySleep(400);
    selectorItem.openSelectorPopUp();
    popUp = new OBSelectorPopUpWindow<T>(selectorItem.getSelectorWindowObjectString());
    popUp.waitUntilVisible();
    popUp.waitForDataToLoad();

    // Added to solve a false positive
    // Sleep.trySleep();
    Sleep.trySleep(150);
    popUp.waitForDataToLoad();

    popUp.clearFilters();
  }

  /**
   * Get the value present on the filter of a selector field.
   *
   * @param fieldName
   *          The name of the field whose filter value will be returned.
   */
  public Object getClearedFilterValue(String fieldName) {
    openPopUpAndClearFilters();
    Sleep.trySleep(250);
    return popUp.getFilterValue(fieldName);
  }

  /**
   * Set a value on the selector using the combo box.
   *
   * @param data
   *          The value that will be set.
   */
  public void selectFromComboBox(DataObject data) {
    selectorItem.selectFromComboBox(data);
  }

  /**
   * Set a record from the selector entering the search key on the text field and selecting the
   * value from the list item.
   *
   * @param data
   *          The data of the record that will be selected.
   */
  public void selectEnteringSearchKey(SelectorDataObject data) {
    selectorItem.selectEnteringSearchKey(data);
  }

  /**
   * Set a record from the selector entering a search criteria (name,...; or search key if all
   * others are missing) on the text field and selecting the value from the list item.
   *
   * @param data
   *          The data of the record that will be selected.
   */
  public void selectEnteringSearchCriteria(SelectorDataObject data) {
    selectorItem.selectEnteringSearchCriteria(data);
  }

  /**
   * Set a value on the selector using the text field.
   *
   * @param value
   *          The value that will be set.
   */
  public void enterText(Object value) {
    selectorItem.enterText(value.toString());
  }

  /**
   * Get the value of the selector.
   *
   * @return the value of the selector.
   */
  @Override
  public String getValue() {
    return selectorItem.getText();
  }

  /**
   * Set a value on the selector.
   *
   * @param value
   *          The value that will be set.
   */
  @Override
  public void setValue(Object value) {
    // These are the choices for the user input preference (this doesn't mean that always a
    // fixed method is used, but depending on the data given for each selector field)
    // Case t | TEXTBOX: enter text using the selector text field
    // Case c | COMBO: select using combo box
    // Case p | POPUP: select using popup (default flow)
    switch (ConfigurationProperties.INSTANCE.getSelectorInputPref()) {
      case 't':
        SelectorDataObject selectorObjectValue1 = (SelectorDataObject) value;
        if (selectorObjectValue1.getDisplayValue() == null
            && selectorObjectValue1.getSearchKey() == null) {
          selectFromPopUp(value);
        } else {
          selectSelectorValueFromTextBox(value);
        }
        break;
      case 'c':
        SelectorDataObject selectorObjectValue2 = (SelectorDataObject) value;
        if (selectorObjectValue2.getDisplayValue() == null) {
          selectFromPopUp(value);
        } else {
          selectSelectorValueFromComboBox(value);
        }
        break;
      case 'p':
        if (selectorItem.isButtonSearchPresent()) {
          selectFromPopUp(value);
        } else {
          selectFromComboBox((SelectorDataObject) value);
        }
        break;
      default:
        if (selectorItem.isButtonSearchPresent()) {
          selectFromPopUp(value);
        } else {
          selectFromComboBox((SelectorDataObject) value);
        }
        break;
    }
  }

  /**
   * Get the title of the selector field.
   */
  @Override
  public String getTitle() {
    // TODO
    throw new NotImplementedException();
  }

  /**
   * Set the focus on the selector.
   */
  @Override
  public void focus() {
    selectorItem.focus();
  }

  /**
   * Wait until the selector field is enabled.
   */
  @Override
  public void waitUntilEnabled() {
    selectorItem.waitUntilEnabled();
  }

  /**
   * Return true if the selector field is present.
   *
   * @return true if the selector field is present. Otherwise, false.
   */
  @Override
  public boolean isPresent() {
    return selectorItem.isPresent();
  }

  /**
   * Return true if the selector field is visible.
   *
   * @return true if the selector field is visible. Otherwise, false.
   */
  @Override
  public boolean isVisible() {
    return selectorItem.isVisible();
  }

  /**
   * Return true if the selector field is enabled.
   *
   * @return true if the selector field is enabled. Otherwise, false.
   */
  @Override
  public boolean isEnabled() {
    return selectorItem.isEnabled();
  }

  /**
   * Check if the text field of this SelectorField is empty.
   *
   * @return true if the text field is empty. Otherwise, false.
   */
  public boolean isEmpty() {
    return getValue().trim().length() == 0;
  }

  /**
   * Select the value of the Selector through the ComboBox selection
   *
   * @param value
   *          The DataObject with the values required
   */
  private void selectSelectorValueFromComboBox(Object value) {
    // If looking in the combo box list isn't enough to find a record (not displayed due to form
    // pre-filtering), then use the default way
    try {
      // Cast made to narrow the input parameter
      selectFromComboBox((SelectorDataObject) value);
    } catch (NoSuchElementException nsee) {
      selectFromPopUp(value);
    } catch (IllegalArgumentException iae) {
      selectFromPopUp(value);
    }
  }

  /**
   * Select the value of the Selector through the TextBox writing
   *
   * @param value
   *          The DataObject with the values required
   */
  private void selectSelectorValueFromTextBox(Object value) {
    // If entering text isn't enough to find a record (not displayed due to form pre-filtering),
    // then use the default way
    try {
      // Cast made to narrow the input parameter
      selectEnteringSearchCriteria((SelectorDataObject) value);
    } catch (NoSuchElementException nsee) {
      selectFromPopUp(value);
    } catch (IllegalArgumentException iae) {
      selectFromPopUp(value);
    }
  }

}
