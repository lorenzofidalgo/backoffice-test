/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Rowinson Gallego <rwn.gallego@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.selenium.Attribute;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on OpenbravoERP tab sets built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class TabSet extends Canvas {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Formatting string used to locate the close button of a tab. The parameter is the SmartClient
   * locator of the tab.
   */
  private static final String FORMAT_LOCATOR_SMART_CLIENT_BUTTON_CLOSE_TAB_VIEW = "%s/icon";
  /**
   * Formatting string used to locate a tab by it's title. The first parameter is the tab set
   * SmartClient locator, and the second is the tab index.
   */
  private static final String FORMAT_LOCATOR_SMART_CLIENT_TAB = "%s/tab[title=%s]";
  /**
   * Formatting string used to get the title of the selected tab. The parameter is object string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_SELECTED_TAB_TITLE = "%s.tabBar.lastSelectedButton.title";

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public TabSet(String objectString) {
    super(objectString);
  }

  /**
   * Class constructor.
   *
   * This constructor will just instantiate the tab set, but after this the tab set will not be
   * ready to be used. It has to be initialized using the function setSmartClientLocator in order to
   * be used with Selenium.
   *
   * This is an alternate way to access the SmartClient object, and should be used only if there is
   * no way to get the object string. Always prefer to call the other constructor with the object
   * string as a parameter, because it will set the SmartClient Locator. With this method the
   * actions that require to call SmartClient functions and attributes directly will not be
   * available.
   */
  public TabSet() {
  }

  /**
   * Check if a tab is open.
   *
   * @param title
   *          The title of the tab.
   * @return true if the tab is open. Otherwise, false.
   */
  public boolean isTabOpen(String title) {
    final String tabLocator = getTabLocator(title);
    return SeleniumSingleton.INSTANCE.findElementByScLocator(tabLocator) != null
        && SeleniumSingleton.INSTANCE.findElementByScLocator(tabLocator).isDisplayed();
  }

  /**
   * Wait until a tab is open.
   *
   * @param title
   *          The title of the tab.
   */
  public void waitUntilTabIsOpen(final String title) {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for the tab '{}' to be opened.", title);
        return isTabOpen(title);
      }
    });
  }

  /**
   * Wait until a tab is closed.
   *
   * @param title
   *          The title of the tab.
   */
  public void waitUntilTabIsClosed(final String title) {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for the tab '{}' to be closed.", title);
        try {
          return !isTabOpen(title);
        } catch (JavascriptException jse) {
          logger.warn("JavascriptException was thrown. Sleeping and trying again.");
          Sleep.trySleep();
          return !isTabOpen(title);
        }
      }
    });
  }

  /**
   * Verify that a tab is open.
   *
   * @param title
   *          The title of the tab.
   */
  public void verifyOpenTab(String title) {
    assertTrue("The tab should be open.", isTabOpen(title));
  }

  /**
   * Verify that a tab is not open.
   *
   * @param title
   *          The title of the tab.
   */
  public void verifyCloseTab(String title) {
    assertFalse("The tab should be closed.", isTabOpen(title));
  }

  /**
   * Get the SmartClient locator of a tab.
   *
   * @param title
   *          The title of the tab.
   * @return the SmartClient locator of the tab.
   */
  protected String getTabLocator(String title) {
    return String.format(FORMAT_LOCATOR_SMART_CLIENT_TAB, getSmartClientLocator(), title);
  }

  /**
   * Get the SmartClient locator of the close button.
   *
   * @param title
   *          The title of the tab.
   * @return the SmartClient locator of the close button.
   */
  private String getCloseButtonLocator(String title) {
    return String.format(FORMAT_LOCATOR_SMART_CLIENT_BUTTON_CLOSE_TAB_VIEW, getTabLocator(title));
  }

  /**
   * Select a tab.
   *
   * @param title
   *          The title of the tab.
   */
  public void selectTab(String title) {
    logger.trace("Selecting tab '{}'.", title);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    // TODO: <chromedriver> Catching exception for the specific case in which Selenium click()
    // fails: when creating more than one record with lines, switching back to the parent tab
    // doesn't work. Obtaining parent tab directly from objectString, which contains the TabSet
    try {
      SeleniumSingleton.INSTANCE.findElementByScLocator(getTabLocator(title)).click();
    } catch (WebDriverException wde1) {
      logger.debug(
          "WebDriverException has been launched. Let's try using Action instead of usual click");
      Actions actionToClick = new Actions(SeleniumSingleton.INSTANCE);
      // In case it does not work, we can apply the following pattern:
      // actionToClick.moveToElement(SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())).click().build().perform();
      try {
        actionToClick.click(SeleniumSingleton.INSTANCE.findElementByScLocator(getTabLocator(title)))
            .build()
            .perform();
      } catch (WebDriverException wde) {
        logger.debug("<chromedriver> exception caught in TabSet.selectTab() - click(): \n"
            + wde1.getMessage());
        // Select a tab by index: objectString.selectTab(1);
        // Iterate through tab names with object.getTab(i).getTitle();
        SeleniumSingleton.INSTANCE.executeScriptWithReturn(objectString + ".selectParentTab()");
      }
    }
  }

  /**
   * Close a tab. FIXME not all tabs can be closed.
   *
   * @param title
   *          The title of the tab.
   */
  public void closeTab(String title) {
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    final String closeButtonLocator = getCloseButtonLocator(title);
    final Button buttonClose = new Button() {
      // XXX This is ugly, but this is the only button that doesn't work with the "/element" added
      // to the path.
      @Override
      protected String getSmartClientLocator() {
        return super.getSmartClientLocator().replace("/element", "");
      }
    };
    buttonClose.setSmartClientLocator(closeButtonLocator);
    buttonClose.click();
  }

  // XXX: <performance issues> Added custom closing of tab to ensure that the target tab is
  // closed whenever the Journal Entry tabs appear
  /**
   * Close a tab and retry as needed until closed. FIXME not all tabs can be closed.
   *
   * @param title
   *          The title of the tab.
   */
  public void closeTabAndWait(String title) {
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    final String closeButtonLocator = getCloseButtonLocator(title);
    final Button buttonClose = new Button() {
      // XXX This is ugly, but this is the only button that doesn't work with the "/element" added
      // to the path.
      @Override
      protected String getSmartClientLocator() {
        return super.getSmartClientLocator().replace("/element", "");
      }
    };
    buttonClose.setSmartClientLocator(closeButtonLocator);
    logger.debug("Waiting for the tab '{}' to be closed.", title);
    Sleep.setFluentWait(1000, 30000, JavascriptException.class)
        .until(new ExpectedCondition<Boolean>() {
          @Override
          public Boolean apply(WebDriver webDriver) {
            buttonClose.click();
            return !isTabOpen(title);
          }
        });
  }

  /**
   * Check if a tab is selected.
   *
   * @param title
   *          The title of the tab.
   * @return true if the tab is selected. Otherwise, false.
   */
  public boolean isTabSelected(String title) {
    if (objectString != null) {
      return SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(
              String.format(FORMAT_SMART_CLIENT_GET_SELECTED_TAB_TITLE, objectString))
          .equals(title);
    } else {
      // XXX this is fragile.
      return getTabAttribute(title, Attribute.CLASS).contains("Selected");
    }
  }

  /**
   * Get the value of an attribute of the canvas.
   *
   * @param attribute
   *          The attribute to get.
   * @return the value of the attribute.
   */
  private String getTabAttribute(String title, Attribute attribute) {
    return SeleniumSingleton.INSTANCE.findElementByScLocator(getTabLocator(title))
        .getAttribute(attribute.getName());
  }
}
