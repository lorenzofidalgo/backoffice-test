/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.applicationdictionary.module;

import com.openbravo.test.integration.erp.data.applicationdictionary.module.ModuleData;
import com.openbravo.test.integration.erp.gui.applicationdictionary.module.ModuleTab;
import com.openbravo.test.integration.erp.gui.applicationdictionary.module.ModuleWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;

/**
 * Executes and verifies actions on the Module window.
 *
 * @author elopio
 */
public class Module extends SmokeTabScript<ModuleData, ModuleTab>
    implements SmokeWindowScript<ModuleWindow> {

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public Module(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the Module window.
   *
   * @return the opened Module window.
   */
  @Override
  public Module open() {
    ModuleWindow window = new ModuleWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the Module tab.
   *
   * Before this, the Module window must be opened.
   *
   * @return the Module tab GUI object.
   */
  @Override
  protected ModuleTab selectTab() {
    ModuleWindow window = (ModuleWindow) mainPage.getView(ModuleWindow.TITLE);
    return (ModuleTab) window.selectTab(ModuleTab.IDENTIFIER);
  }

  public void assertErrorMessage(String errorMessage) {
    ((ModuleTab) getTab()).assertErrorMessage(errorMessage);
  }
}
