/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.financial.accounting.setup.fiscalcalendar;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import com.openbravo.test.integration.erp.data.financial.accounting.setup.fiscalcalendar.PeriodData;
import com.openbravo.test.integration.erp.gui.financial.accounting.setup.fiscalcalendar.classic.FiscalCalendarWindow;
import com.openbravo.test.integration.erp.gui.financial.accounting.setup.fiscalcalendar.classic.PeriodTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * Executes and verifies actions on the Period tab of OpenbravoERP.
 *
 * @author elopio
 *
 */
class PeriodTabClassicScript {

  /**
   * Select a record.
   *
   * @param mainPage
   *          The application main page.
   * @param periodData
   *          The record to select.
   * @throws OpenbravoERPTestException
   */
  static void select(MainPage mainPage, PeriodData periodData) throws OpenbravoERPTestException {
    final FiscalCalendarWindow fiscalCalendarWindow = new FiscalCalendarWindow();
    mainPage.openView(fiscalCalendarWindow);

    fiscalCalendarWindow.selectYearTab();
    final PeriodTab periodTab = fiscalCalendarWindow.selectPeriodTab();
    periodTab.clickRecordWithText(PeriodTab.COLUMN_NAME,
        periodData.getDataField("name").toString());
  }

  /**
   * Verify the number of records.
   *
   * @param mainPage
   *          The application main page.
   * @param count
   *          The expected number of records.
   * @throws OpenbravoERPTestException
   */
  static void verifyCount(MainPage mainPage, int count) throws OpenbravoERPTestException {
    final FiscalCalendarWindow fiscalCalendarWindow = new FiscalCalendarWindow();
    mainPage.openView(fiscalCalendarWindow);

    fiscalCalendarWindow.selectCalendarTab();
    fiscalCalendarWindow.selectYearTab();
    final PeriodTab periodTab = fiscalCalendarWindow.selectPeriodTab();
    assertThat("Number of periods", periodTab.getRowCount(), is(equalTo(count)));
  }

}
