/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Pablo Lujan <pablo.lujan@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.financial.accounting.setup.generalledgerconfiguration;

import com.openbravo.test.integration.erp.data.financial.accounting.setup.accountingschema.GeneralLedgerData;
import com.openbravo.test.integration.erp.gui.financial.accounting.setup.generalledgerconfiguration.classic.GeneralAccountsTab;
import com.openbravo.test.integration.erp.gui.financial.accounting.setup.generalledgerconfiguration.classic.GeneralLedgerConfigurationWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the General Accounts tab of OpenbravoERP.
 *
 * @author aferraz
 *
 */
class GeneralAccountsTabClassicScript {

  /**
   * Create a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data of the record that will be created.
   */
  static void create(MainPage mainPage, GeneralLedgerData data) {
    final GeneralLedgerConfigurationWindow generalLedgerConfigurationWindow = new GeneralLedgerConfigurationWindow();
    mainPage.openView(generalLedgerConfigurationWindow);

    final GeneralAccountsTab generalLedgerConfigurationTab = generalLedgerConfigurationWindow
        .selectGeneralAccountsTab();
    generalLedgerConfigurationTab.create(data);
    generalLedgerConfigurationTab.verifySave();
  }

  /**
   * Create a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data of the record that will be created.
   */
  static void edit(MainPage mainPage, GeneralLedgerData data) {
    throw new UnsupportedOperationException();
  }

}
