/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.financial.accounting.transactions.opencloseperiodcontrol;

import com.openbravo.test.integration.erp.data.financial.accounting.transactions.opencloseperiodcontrol.OpenClosePeriodControlData;
import com.openbravo.test.integration.erp.gui.financial.accounting.transactions.opencloseperiodcontrol.OpenClosePeriodControlTab;
import com.openbravo.test.integration.erp.gui.financial.accounting.transactions.opencloseperiodcontrol.OpenClosePeriodControlWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * Executes and verifies actions on the Calendar tab of OpenbravoERP.
 *
 * @author elopio
 *
 */
class OpenClosePeriodControlTabScript {

  /**
   * Create a period control record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param openClosePeriodControlData
   *          The data of the period control that will be created.
   */
  static void create(MainPage mainPage, OpenClosePeriodControlData openClosePeriodControlData) {
    OpenClosePeriodControlWindow openClosePeriodControlWindow = new OpenClosePeriodControlWindow();
    mainPage.openView(openClosePeriodControlWindow);

    OpenClosePeriodControlTab openClosePeriodControlTab = openClosePeriodControlWindow
        .selectOpenClosePeriodControlTab();
    openClosePeriodControlTab.createOnForm(openClosePeriodControlData);
    openClosePeriodControlTab.assertSaved();
  }

  /**
   * Open or close all period documents.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @throws OpenbravoERPTestException
   */
  static void openCloseAll(MainPage mainPage) throws OpenbravoERPTestException {
    OpenClosePeriodControlWindow openClosePeriodControlWindow = new OpenClosePeriodControlWindow();
    mainPage.openView(openClosePeriodControlWindow);

    OpenClosePeriodControlTab openClosePeriodControlTab = openClosePeriodControlWindow
        .selectOpenClosePeriodControlTab();
    openClosePeriodControlTab.openCloseAll();
    // TODO see issue 17291 - https://issues.openbravo.com/view.php?id=17291
    // openClosePeriodControlTab.assertProcessCompletedSuccessfully();
  }

  /**
   * Open or close all period documents.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @throws OpenbravoERPTestException
   */
  static void openAllPeriods(MainPage mainPage) throws OpenbravoERPTestException {
    OpenClosePeriodControlWindow openClosePeriodControlWindow = new OpenClosePeriodControlWindow();
    mainPage.openView(openClosePeriodControlWindow);

    OpenClosePeriodControlTab openClosePeriodControlTab = openClosePeriodControlWindow
        .selectOpenClosePeriodControlTab();
    openClosePeriodControlTab.openAllPeriods();
    // TODO see issue 17291 - https://issues.openbravo.com/view.php?id=17291
    // openClosePeriodControlTab.assertProcessCompletedSuccessfully();
  }
}
