/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   David Miguélez <david.miguelez@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.BankStatementLinesData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.ReconciliationsLinesData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.sharedtabs.ExchangeRatesData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount.AccountTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount.BankStatementLinesTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount.BankStatementsTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount.ExchangeRatesTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount.FinancialAccountWindow;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount.PaymentMethodTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount.ReconciliationsTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount.TransactionTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on the Financial Account window.
 *
 * @author elopio
 */
public class FinancialAccount extends SmokeTabScript<AccountData, AccountTab>
    implements SmokeWindowScript<FinancialAccountWindow> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Registry key of the post button in the reconciliations tab. */
  private static final String REGISTRY_KEY_BUTTON_POST_RECONCILIATIONS = "org.openbravo.client.application.toolbar.button.posted.FF8080813320657F0133209DE21B0042";
  /** Registry key of the post button in the transaction tab. */
  private static final String REGISTRY_KEY_BUTTON_POST_TRANSACTION = "org.openbravo.client.application.toolbar.button.posted.23691259D1BD4496BCC5F32645BCA4B9";
  /** Registry key of the post button in the bank statements tab. */
  private static final String REGISTRY_KEY_BUTTON_POST_BANK_STATEMENTS = "org.openbravo.client.application.toolbar.button.posted.C56E698100314ABBBBD3A89626CA551C";

  /** Registry key of the process button in the transaction tab. */
  @SuppressWarnings("unused")
  private static final String REGISTRY_KEY_BUTTON_ADD_PAYMENT = "org.openbravo.client.application.toolbar.button.aprmProcessed.23691259D1BD4496BCC5F32645BCA4B9";

  /** Identifier of the delete accounting entry checkbox */
  protected static final String IDENTIFIER_CHECKBOX_DELETE = "inpEliminar";

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public FinancialAccount(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the Financial Account window.
   *
   * @return this script class.
   */
  @Override
  public FinancialAccount open() {
    final FinancialAccountWindow window = new FinancialAccountWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the tab.
   *
   * Before this, the window must be opened.
   *
   * @return the tab GUI object.
   */
  @Override
  protected AccountTab selectTab() {
    final FinancialAccountWindow window = (FinancialAccountWindow) mainPage
        .getView(FinancialAccountWindow.TITLE);
    mainPage.getTabSetMain().selectTab(FinancialAccountWindow.TITLE);
    final AccountTab tab = (AccountTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  /**
   * Add a transaction.
   *
   * @param document
   *          The document type.
   * @param description
   *          The description of the document.
   */
  public void addTransaction(String document, String description) {
    AccountTab tab = selectTab();
    tab.addTransaction(document, description);
  }

  /**
   * Add Payment In/Out
   *
   * @param document
   *          The document type.
   * @param toBePaidTo
   *          The business partner to be paid.
   * @param paymentMethod
   *          The payment method.
   * @param purchaseInvoiceNumber
   *          The number of the purchase invoice.
   * @param action
   *          The action to execute on the document.
   */
  public void addPaymentInOut(String document, String toBePaidTo, String paymentMethod,
      String purchaseInvoiceNumber, String action) {
    AccountTab tab = selectTab();
    tab.addPaymentInOut(document, toBePaidTo, paymentMethod, purchaseInvoiceNumber, action);
  }

  /**
   * Process a reconciliation.
   *
   * @param endingBalance
   *          The reconciliation ending balance.
   * @param hideTransactionsAfterStatementDate
   *          Indicates if the transactions after statement date should be hidden.
   * @param description
   *          The description of the transaction to clear.
   */
  public void reconcile(String endingBalance, boolean hideTransactionsAfterStatementDate,
      String description) {
    AccountTab tab = selectTab();
    tab.reconcile(endingBalance, hideTransactionsAfterStatementDate, description);
  }

  /**
   * Open Add Payment pop
   */
  public MatchStatementProcess<?> openMatchStatement(boolean runAlgorithm) {
    logger.info("Opening Match Statement.");
    AccountTab tab = selectTab();
    MatchStatementProcess<?> matchStatementProcess = tab.openMatchStatement(runAlgorithm);
    return matchStatementProcess;
  }

  /**
   * Executes and verifies actions on the Payment Method tab.
   *
   * @author aferraz
   *
   */
  public class PaymentMethod extends SmokeTabScript<PaymentMethodData, PaymentMethodTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public PaymentMethod(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Payment Method tab.
     *
     * Before this, the Financial Account window must be opened and this tab has to be accessible
     * from the currently selected one.
     *
     * @return the Transaction tab GUI object.
     */
    @Override
    protected PaymentMethodTab selectTab() {
      FinancialAccountWindow window = (FinancialAccountWindow) mainPage
          .getView(FinancialAccountWindow.TITLE);
      PaymentMethodTab tab = (PaymentMethodTab) window.selectTab(PaymentMethodTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }

  /**
   * Executes and verifies actions on the Transaction tab.
   *
   * @author elopio
   *
   */
  public class Transaction extends SmokeTabScript<TransactionsData, TransactionTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Transaction(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Transaction tab.
     *
     * Before this, the Financial Account window must be opened and this tab has to be accessible
     * from the currently selected one.
     *
     * @return the Transaction tab GUI object.
     */
    @Override
    protected TransactionTab selectTab() {
      FinancialAccountWindow window = (FinancialAccountWindow) mainPage
          .getView(FinancialAccountWindow.TITLE);
      TransactionTab tab = (TransactionTab) window.selectTab(TransactionTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

    /**
     * Check if the transaction is already posted.
     */
    public boolean isPosted() {
      logger.debug("Check if the transaction is already posted.");
      Button buttonPost = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST_TRANSACTION));
      return (buttonPost.getText().equals("Unpost"));
    }

    /**
     * Post the transaction.
     */
    public void post() {
      logger.debug("Posting the transaction.");
      Button buttonPost = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST_TRANSACTION));
      // Temporal sleep. Test
      Sleep.trySleep();
      buttonPost.click();
      OBClassicPopUp popUp = new OBClassicPopUp(TestRegistry.getObjectString(
          "org.openbravo.classicpopup./FinancialAccount/Transaction_Edition.html")) {
        @Override
        protected void selectFrame() {
          SeleniumSingleton.INSTANCE.switchTo().frame("process");
        }
      };
      popUp.selectPopUpWithOKButton();
      // Temporal sleep. Test
      Sleep.trySleep();
      popUp.clickOK();
      // Temporal sleep. Test
      Sleep.trySleep(8000);
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      // XXX: <performance issues> Waiting until all Journal Entries window are open to prevent
      // overlap and/or interruption by the actions following
      // TODO: jse will be thrown when the JS script returns "null", due to header tabs not being in
      // focus in the event of a process message shown (warning or error, such as in the
      // APRRegression33688 test) or other. This could be better handled by checking whether any
      // of the tabs is in focus
      try {
        Sleep.setFluentWait(300, 600000).until(new ExpectedCondition<Boolean>() {
          @Override
          public Boolean apply(WebDriver driver) {
            String strOfJsScript = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
                "document.getElementsByClassName('OBTabBarButtonMainTitleSelected').item(0).textContent");
            String strComp = "Journal Entries Report";
            String strComp2 = " - Main";
            String strComp3 = " - USA U...";
            if (strOfJsScript.toLowerCase().contains(strComp2.toLowerCase())) {
              logger.trace("Sleeping for the first 'Main US/A/Euro' Journal Entry tab to load");
              Sleep.trySleep(5000);
            }
            return strOfJsScript.toLowerCase().equals(strComp.toLowerCase())
                || strOfJsScript.toLowerCase().contains(strComp3.toLowerCase());
          }
        });
      } catch (JavascriptException jse) {
        // Don't perform JE tab opening check, and continue
        logger.trace(
            "Exception has been caught, omitting to check whether the Journal Entry tabs have been opened");
      }
    }

    /**
     * UnPost the transaction.
     */
    public void unpost() {
      logger.debug("Unposting the transaction.");
      Button buttonUnpost = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST_TRANSACTION));
      buttonUnpost.click();
      OBClassicPopUp popUp = new OBClassicPopUp(TestRegistry.getObjectString(
          "org.openbravo.classicpopup./FinancialAccount/Transaction_Edition.html")) {
        @Override
        protected void selectFrame() {
          SeleniumSingleton.INSTANCE.switchTo().frame("process");
        }
      };
      popUp.selectPopUpWithOKButton();
      popUp.verifyElementsAreVisible(new String[] { IDENTIFIER_CHECKBOX_DELETE });
      if (!SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_CHECKBOX_DELETE).isSelected()) {
        SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_CHECKBOX_DELETE).click();
      }
      popUp.clickOK();
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      Sleep.trySleep();
    }

    /**
     * Process the transaction.
     */
    public void process() {
      logger.debug("Processing the transaction.");
      TransactionTab tab = selectTab();
      tab.process();
    }

    /**
     * Reactivate the transaction.
     */
    public void reactivate() {
      logger.debug("Reactivating the transaction.");
      TransactionTab tab = selectTab();
      tab.process();
    }

    /**
     * Create a record.
     *
     * @param data
     *          The data of the record that will be created.
     */
    public void createWithoutSaving(TransactionsData data) {
      logger.info("Create record without saving with data: {}.", data);
      TransactionTab tab = selectTab();
      tab.createOnFormWithoutSaving(data);
    }

    /**
     * Open Add Payment pop
     */
    public AddPaymentProcess openAddPayment() {
      logger.info("Opening Add Payment.");
      TransactionTab tab = selectTab();
      AddPaymentProcess addPaymentProcess = tab.openAddPayment();
      return addPaymentProcess;
    }

    /**
     * Get the value present on the filter of a selector field.
     *
     * @param fieldName
     *          The name of the field whose filter value will be returned.
     */
    public Object getSelectorFieldFilterClearedValue(String fieldName) {
      TransactionTab tab = selectTab();
      return tab.getSelectorFieldFilterClearedValue(fieldName);
    }

    /**
     * Assert that deposit and payment amounts are zero.
     */
    public void assertAmountsAreZero() {
      TransactionTab tab = selectTab();
      tab.assertAmountsAreZero();
    }

    /**
     * Assert that payment and glitem are empty.
     */
    public void assertPaymentAndGlitemAreEmpty() {
      TransactionTab tab = selectTab();
      tab.assertPaymentAndGlitemAreEmpty();
    }

    /**
     * Assert that the period does not exist or it is not opened.
     */
    public void assertPeriodDoesNotExistOrItIsNotOpened() {
      TransactionTab tab = selectTab();
      tab.assertPeriodDoesNotExistOrItIsNotOpened();
    }

    /**
     * Executes and verifies actions on the Transaction Exchange rates tab.
     *
     * @author David Miguélez
     *
     */
    public class ExchangeRates extends SmokeTabScript<ExchangeRatesData, ExchangeRatesTab> {

      /**
       * Class constructor.
       *
       * @param mainPage
       *          The application main page.
       */
      public ExchangeRates(MainPage mainPage) {
        super(mainPage);
      }

      /**
       * Select and return the Exchange rates tab.
       *
       * Before this, the Financial Account window must be opened and this tab has to be accessible
       * from the currently selected one.
       *
       * @return the Header tab GUI object.
       */
      @Override
      protected ExchangeRatesTab selectTab() {
        FinancialAccountWindow window = (FinancialAccountWindow) mainPage
            .getView(FinancialAccountWindow.TITLE);
        ExchangeRatesTab tab = (ExchangeRatesTab) window.selectTab(ExchangeRatesTab.IDENTIFIER);
        tab.waitUntilSelected();
        return tab;
      }

    }
  }

  /**
   * Executes and verifies actions on the Reconciliations tab.
   *
   * @author David Miguélez
   *
   */
  public class Reconciliations
      extends SmokeTabScript<ReconciliationsLinesData, ReconciliationsTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Reconciliations(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Reconciliation tab.
     *
     * Before this, the Financial Account window must be opened and this tab has to be accessible
     * from the currently selected one.
     *
     * @return the Reconciliation tab GUI object.
     */
    @Override
    protected ReconciliationsTab selectTab() {
      FinancialAccountWindow window = (FinancialAccountWindow) mainPage
          .getView(FinancialAccountWindow.TITLE);
      ReconciliationsTab tab = (ReconciliationsTab) window.selectTab(ReconciliationsTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

    /**
     * Check if the reconciliation is already posted.
     */
    public boolean isPosted() {
      logger.debug("Check if the reconciliation is already posted.");
      Button buttonPost = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST_RECONCILIATIONS));
      return (buttonPost.getText().equals("Unpost"));
    }

    /**
     * Post the reconciliation.
     */
    public void post() {
      logger.debug("Posting the reconciliation.");
      Button buttonPost = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST_RECONCILIATIONS));
      buttonPost.click();
      OBClassicPopUp popUp = new OBClassicPopUp(TestRegistry.getObjectString(
          "org.openbravo.classicpopup./FinancialAccount/ReconciliationsFF8080813320657F0133209DE21B0042_Edition.html")) {
        @Override
        protected void selectFrame() {
          SeleniumSingleton.INSTANCE.switchTo().frame("process");
        }
      };
      popUp.selectPopUpWithOKButton();
      popUp.clickOK();
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      // XXX: <performance issues> Waiting until all Journal Entries window are open to prevent
      // overlap and/or interruption by the actions following
      Sleep.setFluentWait(300, 600000).until(new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver driver) {
          String strOfJsScript = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
              "document.getElementsByClassName('OBTabBarButtonMainTitleSelected').item(0).textContent");
          String strComp = "Journal Entries Report";
          String strComp2 = " - Main";
          String strComp3 = " - USA U...";
          if (strOfJsScript.toLowerCase().contains(strComp2.toLowerCase())) {
            logger.trace("Sleeping for the first 'Main US/A/Euro' Journal Entry tab to load");
            Sleep.trySleep(2000);
          }
          return strOfJsScript.toLowerCase().equals(strComp.toLowerCase())
              || strOfJsScript.toLowerCase().contains(strComp3.toLowerCase());
        }
      });
    }

    /**
     * UnPost the reconciliation.
     */
    public void unpost() {
      logger.debug("Unposting the reconciliation.");
      Button buttonUnpost = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST_RECONCILIATIONS));
      buttonUnpost.click();
      OBClassicPopUp popUp = new OBClassicPopUp(TestRegistry.getObjectString(
          "org.openbravo.classicpopup./FinancialAccount/ReconciliationsFF8080813320657F0133209DE21B0042_Edition.html")) {
        @Override
        protected void selectFrame() {
          SeleniumSingleton.INSTANCE.switchTo().frame("process");
        }
      };
      popUp.selectPopUpWithOKButton();
      popUp.verifyElementsAreVisible(new String[] { IDENTIFIER_CHECKBOX_DELETE });
      if (!SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_CHECKBOX_DELETE).isSelected()) {
        SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_CHECKBOX_DELETE).click();
      }
      popUp.clickOK();
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      Sleep.trySleep();
    }
  }

  /**
   * Executes and verifies actions on the Bank Statements tab.
   *
   * @author elopio
   *
   */
  public class BankStatements extends SmokeTabScript<BankStatementHeaderData, BankStatementsTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public BankStatements(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Bank Statement tab.
     *
     * Before this, the Financial Account window must be opened and this tab has to be accessible
     * from the currently selected one.
     *
     * @return the Bank Statement tab GUI object.
     */
    @Override
    protected BankStatementsTab selectTab() {
      FinancialAccountWindow window = (FinancialAccountWindow) mainPage
          .getView(FinancialAccountWindow.TITLE);
      BankStatementsTab tab = (BankStatementsTab) window.selectTab(BankStatementsTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

    /**
     * Check if the transaction is already posted.
     */
    public boolean isPosted() {
      logger.debug("Check if the Bank Statement is already posted.");
      Button buttonPost = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST_BANK_STATEMENTS));
      return (buttonPost.getText().equals("Unpost"));
    }

    /**
     * Post the transaction.
     */
    public void post() {
      logger.debug("Posting the Bank Statement.");
      Button buttonPost = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST_BANK_STATEMENTS));
      buttonPost.click();
      OBClassicPopUp popUp = new OBClassicPopUp(TestRegistry.getObjectString(
          "org.openbravo.classicpopup./FinancialAccount/Transaction_Edition.html")) {
        @Override
        protected void selectFrame() {
          SeleniumSingleton.INSTANCE.switchTo().frame("process");
        }
      };
      popUp.selectPopUpWithOKButton();
      popUp.clickOK();
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      Sleep.smartWaitExecuteScript("document.readyState === 'complete'", 1000);
    }

    /**
     * UnPost the Bank Statement.
     */
    public void unpost() {
      logger.debug("Unposting the Bank Statement.");
      Button buttonUnpost = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST_TRANSACTION));
      buttonUnpost.click();
      OBClassicPopUp popUp = new OBClassicPopUp(TestRegistry.getObjectString(
          "org.openbravo.classicpopup./FinancialAccount/Transaction_Edition.html")) {
        @Override
        protected void selectFrame() {
          SeleniumSingleton.INSTANCE.switchTo().frame("process");
        }
      };
      popUp.selectPopUpWithOKButton();
      popUp.verifyElementsAreVisible(new String[] { IDENTIFIER_CHECKBOX_DELETE });
      if (!SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_CHECKBOX_DELETE).isSelected()) {
        SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_CHECKBOX_DELETE).click();
      }
      popUp.clickOK();
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      Sleep.trySleep();
    }

    /**
     * Process the Bank Statement.
     */
    public void process() {
      logger.debug("Processing the Bank Statement.");
      BankStatementsTab tab = selectTab();
      tab.process();
    }

    /**
     * Executes and verifies actions on the Bank Statement Lines tab.
     *
     * @author David Miguélez
     *
     */
    public class BankStatementLines
        extends SmokeTabScript<BankStatementLinesData, BankStatementLinesTab> {

      /**
       * Class constructor.
       *
       * @param mainPage
       *          The application main page.
       */
      public BankStatementLines(MainPage mainPage) {
        super(mainPage);
      }

      /**
       * Select and return the Bank Statement Lines tab.
       *
       * Before this, the Financial Account window must be opened and this tab has to be accessible
       * from the currently selected one.
       *
       * @return the Header tab GUI object.
       */
      @Override
      protected BankStatementLinesTab selectTab() {
        FinancialAccountWindow window = (FinancialAccountWindow) mainPage
            .getView(FinancialAccountWindow.TITLE);
        BankStatementLinesTab tab = (BankStatementLinesTab) window
            .selectTab(BankStatementLinesTab.IDENTIFIER);
        tab.waitUntilSelected();
        return tab;
      }

    }
  }

}
