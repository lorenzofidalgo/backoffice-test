/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentDisplayLogic;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sharedtabs.ExchangeRatesData;
import com.openbravo.test.integration.erp.data.sharedtabs.UsedCreditSourceData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentin.ExchangeRatesTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentin.PaymentInLinesTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentin.PaymentInTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentin.PaymentInWindow;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentin.UsedCreditSourceTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on the Payment In window.
 *
 * @author elopio
 */
public class PaymentIn extends SmokeTabScript<PaymentInHeaderData, PaymentInTab>
    implements SmokeWindowScript<PaymentInWindow> {

  /** Registry key of the post button in the transaction tab. */
  private static final String REGISTRY_KEY_BUTTON_POST_PAYMENT = "org.openbravo.client.application.toolbar.button.posted.C4B6506838E14A349D6717D6856F1B56";

  /** Identifier of the delete accounting entry checkbox */
  protected static final String IDENTIFIER_CHECKBOX_DELETE = "inpEliminar";

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public PaymentIn(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the Payment In window.
   *
   * @return this script class.
   */
  @Override
  public PaymentIn open() {
    final PaymentInWindow window = new PaymentInWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the tab.
   *
   * Before this, the window must be opened.
   *
   * @return the tab GUI object.
   */
  @Override
  protected PaymentInTab selectTab() {
    final PaymentInWindow window = (PaymentInWindow) mainPage.getView(PaymentInWindow.TITLE);
    mainPage.getTabSetMain().selectTab(PaymentInWindow.TITLE);
    final PaymentInTab tab = (PaymentInTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  /**
   * Assert the data of the add payment out popup.
   *
   * @param payingTo
   *          The business partner that will be payed.
   * @param amount
   *          The amount of the payment.
   * @param transactionType
   *          The type of the transaction.
   */
  public void assertAddPaymentInData(String payingTo, String amount, String transactionType) {
    final PaymentInTab tab = selectTab();
    tab.assertAddPaymentInData(payingTo, amount, transactionType);
  }

  /**
   * Add payment in details.
   *
   * @param transactionType
   *          The type of the transaction.
   * @param documentNumber
   *          The number of the document.
   * @param action
   *          The process action to request after the payment.
   */
  public void addDetails(String transactionType, String documentNumber, String action) {
    logger.info("Adding Payment details.");
    final PaymentInTab tab = selectTab();
    tab.addPaymentIn(transactionType, documentNumber, action);
  }

  public AddPaymentProcess addDetailsOpen() {
    logger.info("Adding Payment details.");
    final PaymentInTab tab = selectTab();
    AddPaymentProcess addPaymentProcess = tab.addPaymentInOpen();
    return addPaymentProcess;
  }

  /**
   * Check if the payment is already posted.
   */
  public boolean isPosted() {
    logger.debug("Checking if the payment is already posted.");
    Button buttonPost = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST_PAYMENT));
    return (buttonPost.getText().equals("Unpost"));
  }

  /**
   * Post the payment.
   */
  public void post() {
    logger.debug("Posting the payment.");
    Button buttonPost = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST_PAYMENT));
    buttonPost.click();
    OBClassicPopUp popUp = new OBClassicPopUp(
        TestRegistry.getObjectString("org.openbravo.classicpopup./PaymentIn/Header_Edition.html")) {
      @Override
      protected void selectFrame() {
        SeleniumSingleton.INSTANCE.switchTo().frame("process");
      }
    };
    popUp.selectPopUpWithOKButton();
    popUp.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    // XXX: <performance issues> Waiting until all Journal Entries window are open to prevent
    // overlap and/or interruption by the actions following;
    Sleep.setFluentWait(300, 600000).until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver driver) {
        String strOfJsScript = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
            "document.getElementsByClassName('OBTabBarButtonMainTitleSelected').item(0).textContent");
        String strComp2 = " - Main";
        String strComp3 = " - USA U...";
        if (strOfJsScript.toLowerCase().contains(strComp2.toLowerCase())) {
          logger.trace("Sleeping for the first 'Main US/A/Euro' Journal Entry tab to load");
          Sleep.trySleep(2000);
        }
        return strOfJsScript.toLowerCase().contains(strComp3.toLowerCase());
      }
    });
  }

  /**
   * UnPost the payment.
   */
  public void unpost() {
    logger.debug("Unposting the payment.");
    Button buttonUnpost = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST_PAYMENT));
    buttonUnpost.click();
    OBClassicPopUp popUp = new OBClassicPopUp(
        TestRegistry.getObjectString("org.openbravo.classicpopup./PaymentIn/Header_Edition.html")) {
      @Override
      protected void selectFrame() {
        SeleniumSingleton.INSTANCE.switchTo().frame("process");
      }
    };
    popUp.selectPopUpWithOKButton();
    popUp.verifyElementsAreVisible(new String[] { IDENTIFIER_CHECKBOX_DELETE });
    if (!SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_CHECKBOX_DELETE).isSelected()) {
      SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_CHECKBOX_DELETE).click();
    }
    popUp.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    Sleep.trySleep();
  }

  /**
   * Process the payment in.
   *
   * @param action
   */
  public void process(String action) {
    final PaymentInTab tab = selectTab();
    tab.process(action);
  }

  /**
   * Execute the payment in.
   */
  public void execute() {
    final PaymentInTab tab = selectTab();
    tab.execute();
  }

  /**
   * Execute the payment in.
   */
  public void execute(String number) {
    final PaymentInTab tab = selectTab();
    tab.execute(number);
  }

  /**
   * Reverse the payment in.
   *
   * @param action
   *          The reverse action to request for the payment.
   * @param date
   *          The reverse date to request for the payment.
   */
  public void reverse(String action, String date) {
    final PaymentInTab tab = selectTab();
    tab.reverse(action, date);
  }

  public void assertPaymentCreatedSuccessfully() {
    Sleep.trySleep(3000);
    selectTab().assertPaymentCreatedSuccessfully();
  }

  public void assertRefundedPaymentCreatedSuccessfully() {
    selectTab().assertRefundedPaymentCreatedSuccessfully();
  }

  public void assertFinancialAccountInactive(String financialAccount) {
    selectTab().assertFinancialAccountInactive(financialAccount);
  }

  public void assertPaymentMethodInactive(String paymentMethod) {
    selectTab().assertPaymentMethodInactive(paymentMethod);
  }

  public void assertFinancialAccountAndPaymentMethodInactive(String financialAccount,
      String paymentMethod) {
    selectTab().assertFinancialAccountAndPaymentMethodInactive(financialAccount, paymentMethod);
  }

  /**
   * Assert display logic visibility from AddPayment Popup
   *
   * @param visibleDisplayLogic
   *          The AddPaymentDisplayLogic with the fields whose visibility has to be asserted.
   */
  public void assertAddPaymentDisplayLogicVisible(AddPaymentDisplayLogic visibleDisplayLogic) {
    logger.info("Asserting display logic visibility from AddPayment Popup.");
    final PaymentInTab tab = selectTab();
    tab.assertAddPaymentDisplayLogicVisible(visibleDisplayLogic);
  }

  /**
   * Assert display logic enable status from AddPayment Popup
   *
   * @param enabledDisplayLogic
   *          DataOject with the fields whose enable status has to be asserted.
   */
  public void assertAddPaymentDisplayLogicEnabled(AddPaymentDisplayLogic enabledDisplayLogic) {
    logger.info("Asserting display logic enable status from AddPayment Popup.");
    final PaymentInTab tab = selectTab();
    tab.assertAddPaymentDisplayLogicEnabled(enabledDisplayLogic);
  }

  /**
   * Assert display logic expanded status from AddPayment Popup
   *
   * @param expandedDisplayLogic
   *          DataOject with the fields whose expanded status has to be asserted.
   */
  public void assertAddPaymentDisplayLogicExpanded(AddPaymentDisplayLogic expandedDisplayLogic) {
    logger.info("Asserting display logic expanded status from AddPayment Popup.");
    final PaymentInTab tab = selectTab();
    tab.assertAddPaymentDisplayLogicExpanded(expandedDisplayLogic);
  }

  /**
   * Executes and verifies actions on the Payment In Lines tab.
   *
   * @author elopio
   *
   */
  public class Lines extends SmokeTabScript<PaymentInLinesData, PaymentInLinesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Lines(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Lines tab.
     *
     * Before this, the Payment In window must be opened and this tab has to be accessible from the
     * currently selected one.
     *
     * @return the Lines tab GUI object.
     */
    @Override
    protected PaymentInLinesTab selectTab() {
      PaymentInWindow window = (PaymentInWindow) mainPage.getView(PaymentInWindow.TITLE);
      PaymentInLinesTab tab = (PaymentInLinesTab) window.selectTab(PaymentInLinesTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }

  /**
   * Executes and verifies actions on the PaymentIn Exchange rates tab.
   *
   * @author David Miguélez
   *
   */
  public class ExchangeRates extends SmokeTabScript<ExchangeRatesData, ExchangeRatesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public ExchangeRates(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Exchange rates tab.
     *
     * Before this, the PaymentIn window must be opened and this tab has to be accessible from the
     * currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected ExchangeRatesTab selectTab() {
      PaymentInWindow window = (PaymentInWindow) mainPage.getView(PaymentInWindow.TITLE);
      ExchangeRatesTab tab = (ExchangeRatesTab) window.selectTab(ExchangeRatesTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }

  public class UsedCreditSource extends SmokeTabScript<UsedCreditSourceData, UsedCreditSourceTab> {

    public UsedCreditSource(MainPage mainPage) {
      super(mainPage);
    }

    @Override
    protected UsedCreditSourceTab selectTab() {
      PaymentInWindow window = (PaymentInWindow) mainPage.getView(PaymentInWindow.TITLE);
      UsedCreditSourceTab tab = (UsedCreditSourceTab) window
          .selectTab(UsedCreditSourceTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }
}
