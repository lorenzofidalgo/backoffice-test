/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 *   David Miguélez <david.miguelez@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.application.conversionrates;

import com.openbravo.test.integration.erp.data.generalsetup.application.currency.ConversionRatesData;
import com.openbravo.test.integration.erp.gui.general.application.conversionrates.ConversionRatesTab;
import com.openbravo.test.integration.erp.gui.general.application.conversionrates.ConversionRatesWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;

/**
 * Executes and verifies actions on the Conversion Rates window.
 *
 * @author aferraz
 */
public class ConversionRates extends SmokeTabScript<ConversionRatesData, ConversionRatesTab>
    implements SmokeWindowScript<ConversionRatesWindow> {

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public ConversionRates(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the Conversion Rates window.
   *
   * @return this script class.
   */
  @Override
  public ConversionRates open() {
    final ConversionRatesWindow window = new ConversionRatesWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the tab.
   *
   * Before this, the window must be opened.
   *
   * @return the tab GUI object.
   */
  @Override
  protected ConversionRatesTab selectTab() {
    final ConversionRatesWindow window = (ConversionRatesWindow) mainPage
        .getView(ConversionRatesWindow.TITLE);
    mainPage.getTabSetMain().selectTab(ConversionRatesWindow.TITLE);
    final ConversionRatesTab tab = (ConversionRatesTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

}
