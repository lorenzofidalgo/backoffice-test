/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.application.modulemanagement;

import com.openbravo.test.integration.erp.data.generalsetup.ModuleReferenceData;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Module Management classic window of OpenbravoERP.
 *
 * @author elopio
 */
public class ModuleManagement {

  /**
   * Executes and verifies actions on the Add Modules form tab of OpenbravoERP.
   *
   * @author elopio
   */
  public static class AddModules {

    /**
     * Install a module from central repository.
     *
     * @param mainPage
     *          The application main page.
     * @param module
     *          The name of the module.
     * @return true if the module was installed. Otherwise, false.
     */
    public static boolean installModuleFromCentralRepository(MainPage mainPage,
        ModuleReferenceData module) {
      return AddModulesTabScript.installModuleFromCentralRepository(mainPage, module);
    }

    /**
     * Install a module from central repository.
     *
     * @param mainPage
     *          The application main page.
     * @param module
     *          The name of the module.
     * @return true if the module was installed. Otherwise, false.
     */
    public static boolean installModuleFromFileSystem(MainPage mainPage,
        ModuleReferenceData module) {
      return AddModulesTabScript.installModuleFromFileSystem(mainPage, module);
    }
  }

  /**
   * Executes and verifies actions on the Installed Modules form tab of OpenbravoERP.
   *
   * @author elopio
   */
  public static class InstalledModules {

    /**
     * Enable all installed modules.
     *
     * @param mainPage
     *          The application main page.
     */
    public static void enableAllModules(MainPage mainPage) {
      InstalledModulesTabScript.enableAllModules(mainPage);
    }

    /**
     * Disable all installed modules.
     *
     * @param mainPage
     *          The application main page.
     */
    public static void disableModules(MainPage mainPage) {
      InstalledModulesTabScript.disableModules(mainPage);
    }

    /**
     * Rebuild the system and restart the servlet container.
     *
     * @param mainPage
     *          The application main page.
     */
    public static void rebuildSystemAndRestartServletContainer(MainPage mainPage) {
      InstalledModulesTabScript.rebuildSystemAndRestartServletContainer(mainPage);
    }
  }

  /**
   * Executes and verifies actions on the Settings form tab of OpenbravoERP.
   *
   * @author elopio
   */
  public static class Settings {

    /**
     * Set the status for the scan and search
     *
     * @param mainPage
     *          The application main page.
     * @param scanStatus
     *          The status that will be set for the scan.
     * @param searchStatus
     *          The status that will be set for the search.
     * @return true if at least one of the status was changed. False if both statuses were already
     *         the requested.
     */
    public static boolean setStatus(MainPage mainPage, String scanStatus, String searchStatus) {
      return SettingsTabScript.setStatus(mainPage, scanStatus, searchStatus);
    }

    /**
     * Set the scan status.
     *
     * @param mainPage
     *          The application main page.
     * @param status
     *          The status that will be set.
     * @return true if the status was changed. False if the status was already the desired.
     */
    public static boolean setScanStatus(MainPage mainPage, String status) {
      return SettingsTabScript.setScanStatus(mainPage, status);
    }

    /**
     * Set the search status.
     *
     * @param mainPage
     *          The application main page.
     * @param status
     *          The status that will be set.
     * @return true if the status was changed. False if the status was already the desired.
     */
    public static boolean setSearchStatus(MainPage mainPage, String status) {
      return SettingsTabScript.setSearchStatus(mainPage, status);
    }

  }
}
