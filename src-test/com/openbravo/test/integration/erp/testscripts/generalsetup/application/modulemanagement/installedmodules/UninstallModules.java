/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Mikel Irurita <mikel.irurita@openbravo.com>,
 *   Leo Arias <yo@elopio.net>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.application.modulemanagement.installedmodules;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * Test module uninstall.
 *
 * @author plujan
 *
 */
@RunWith(Parameterized.class)
public abstract class UninstallModules extends OpenbravoERPTest {

  /** Name of the user that will be used for this test. */
  protected static final String USER_NAME = "Openbravo";
  /** Password of the user that will be used for this test. */
  protected static final String PASSWORD = "openbravo";

  /* Data for this test */

  /** Identifiers (UUID) of the modules. */
  @SuppressWarnings("unused")
  private final String[] modulesId;

  /**
   * Class constructor.
   *
   * @param modulesId
   *          Identifiers (UUID) of the modules.
   */
  public UninstallModules(String[] modulesId) {
    this.modulesId = modulesId;
  }

  /**
   * Class constructor.
   *
   * @param moduleId
   *          Identifier (UUID) of the module.
   */
  public UninstallModules(String moduleId) {
    String[] idList = { "" };
    idList[0] = moduleId;
    this.modulesId = idList;
  }

  /**
   * Test the module uninstallation.
   *
   * @throws OpenbravoERPTestException
   */
  @Test
  public void testUninstallModules() throws OpenbravoERPTestException {

    // final Login login = new Login();
    // login.open(openbravoContext);
    // login.login(USER_NAME, PASSWORD);
    // login.verify(true, USER_NAME);
    //
    // final Menu menu = new Menu();
    // menu.select(Menu.GENERAL_SETUP, Menu.APPLICATION, Menu.MODULE_MANAGEMENT);
    //
    // final InstalledModules installedModules = new InstalledModules();
    //
    // for (int i = 0; i < modulesId.length; i++) {
    // String moduleId = modulesId[i];
    // if (installedModules.isModuleInstalled(moduleId)) {
    // installedModules.selectModule(moduleId);
    // installedModules.uninstallModule();
    // } else {
    // System.out.println("On uninstalling, module " + moduleId + " was not found, skipping it.");
    // }
    // }
    // installedModules.clickRebuild();
    // final RebuildSystemPopUp rebuildSystemPopUp = new RebuildSystemPopUp();
    // rebuildSystemPopUp.rebuildTheSystem();
    // rebuildSystemPopUp.restartServletContainer();
    //
    // SeleniumSingleton.INSTANCE.selectWindow(Window.MAIN_WINDOW);
    // login.waitForFieldsToAppear(TimeoutConstants.RESTART_SERVLET_CONTAINER_TIMEOUT);

    // login.login(USER_NAME, PASSWORD);
    // login.verify(true, USER_NAME);
    // menu.select(Menu.GENERAL_SETUP, Menu.APPLICATION,
    // Menu.MODULE_MANAGEMENT);

    // assertFalse("The module " + moduleId + " is not unistalled.",
    // installedModules.isModuleInstalled(moduleId));

    // SeleniumSingleton.INSTANCE.selectWindow(Window.MAIN_WINDOW);
    // final Logout logout = new Logout();
    // logout.logout();

  }
}
