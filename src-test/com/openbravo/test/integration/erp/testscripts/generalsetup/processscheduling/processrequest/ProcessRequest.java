/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.processscheduling.processrequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.generalsetup.processscheduling.processrequest.ProcessRequestData;
import com.openbravo.test.integration.erp.gui.general.processscheduling.processrequest.ProcessRequestTab;
import com.openbravo.test.integration.erp.gui.general.processscheduling.processrequest.ProcessRequestWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on the Process Request window of OpenbravoERP.
 *
 * @author Pablo Lujan
 */
public class ProcessRequest extends SmokeTabScript<ProcessRequestData, ProcessRequestTab>
    implements SmokeWindowScript<ProcessRequestWindow> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public ProcessRequest(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the Process Request window.
   *
   * @return the opened window.
   */
  @Override
  public ProcessRequest open() {
    ProcessRequestWindow window = new ProcessRequestWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the Process Request tab.
   *
   * Before this, the User Process Request must be opened.
   *
   * @return the tab GUI object.
   */
  @Override
  protected ProcessRequestTab selectTab() {
    ProcessRequestWindow window = (ProcessRequestWindow) mainPage
        .getView(ProcessRequestWindow.TITLE);
    mainPage.getTabSetMain().selectTab(ProcessRequestWindow.TITLE);
    ProcessRequestTab tab = (ProcessRequestTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  /**
   * Unschedule the selected process request.
   *
   * @return true if the process was unscheduled. Otherwise, false.
   */
  public boolean unscheduleProcess() {
    logger.info("Unschedule process.");
    ProcessRequestTab tab = selectTab();
    return tab.unscheduleProcess();
  }

  /**
   * Assert that the process was successfully unscheduled.
   */
  public void assertProcessUnscheduleSucceeded() {
    ProcessRequestTab tab = selectTab();
    tab.assertProcessUnscheduleSucceeded();
    logger.info("Verifying in grid that the process is Unscheduled.");
    Sleep.trySleep(15000);
    tab.select(new ProcessRequestData.Builder().process("Acct Server Process")
        .status("Unscheduled")
        .organization("*")
        .build());
    waitUntilAnyRunningProcessIsFinished();
  }

  /**
   * TODO: Replace this function by a smart wait checking the Process Monitor window for any
   * Processing row.
   */
  public void waitUntilAnyRunningProcessIsFinished() {
    logger.info("[debug] Start - Waiting for 5 minutes");
    Sleep.trySleep(60000);
    Sleep.trySleep(60000);
    Sleep.trySleep(60000);
    Sleep.trySleep(60000);
    Sleep.trySleep(60000);
    logger.info("[debug] Finish - Waiting for 5 minutes");
  }
}
