/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.security.role;

import com.openbravo.test.integration.erp.data.generalsetup.security.role.OrgAccessData;
import com.openbravo.test.integration.erp.gui.general.security.role.classic.OrgAccessTab;
import com.openbravo.test.integration.erp.gui.general.security.role.classic.RoleWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the User tab of OpenbravoERP.
 *
 * @author Pablo Lujan
 */
class OrgAccessClassicScript {

  /**
   * Creates the organization access.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param orgAccessData
   *          The organization access data.
   */
  static void create(MainPage mainPage, OrgAccessData orgAccessData) {
    final RoleWindow roleWindow = new RoleWindow();
    mainPage.openView(roleWindow);

    final OrgAccessTab orgAccessTab = roleWindow.selectOrgAccessTab();
    orgAccessTab.create(orgAccessData);
    orgAccessTab.verifySave();
  }

}
