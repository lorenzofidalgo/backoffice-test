/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <pablo.lujan@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.gui;

import org.junit.After;
import org.junit.Test;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.gui.LoginPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the error displayed when wrong credentials are entered in the login page.
 *
 * @author Leo Arias
 */
public abstract class FailedLogin extends OpenbravoERPTest {

  /**
   * Class constructor.
   *
   * @param userName
   *          Name of the user that will be used for this test.
   * @param password
   *          Password of the user that will be used for this test.
   */
  public FailedLogin(String userName, String password) {
    logInData = new LogInData.Builder().userName(userName).password(password).build();
  }

  /**
   * Test the error displayed when wrong credentials are entered in the login page.
   */
  @Test
  public void errorShouldAppearWhenLoginWithWrongCredentials() {
    final LoginPage login = new LoginPage();
    login.open();
    login.logIn(logInData);

    login.verifyWrongLogin();
  }

  /**
   * Override the login with NOP.
   */
  @Override
  public void login() {
    // Do nothing. The test will execute a failed login.
  }

  /**
   * Override the logout with NOP.
   */
  @Override
  @After
  public void logout() {
    // Do nothing. The test should never login.
  }
}
