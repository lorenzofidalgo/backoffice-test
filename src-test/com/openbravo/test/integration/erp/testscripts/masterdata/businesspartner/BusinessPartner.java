/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.masterdata.businesspartner;

import com.openbravo.test.integration.erp.data.masterdata.businesspartner.BusinessPartnerData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.CustomerAccountingData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.CustomerData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.LocationAddressData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.VendorAccountingData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.VendorCreditorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * Executes and verifies actions on the window.
 *
 * @author Pablo Lujan
 */
public class BusinessPartner {

  /**
   * Executes and verifies actions on the tab.
   *
   * @author plujan
   *
   */
  public static class BusinessPartnerTab {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     *
     */
    public static void create(MainPage mainPage, BusinessPartnerData data) {
      if (mainPage.isOnNewLayout()) {
        BusinessPartnerScript.create(mainPage, data);
      } else {
        BusinessPartnerClassicScript.create(mainPage, data);
      }
    }

    /**
     * Select the record.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     *
     */
    public static void select(MainPage mainPage, BusinessPartnerData data) {
      if (mainPage.isOnNewLayout()) {
        BusinessPartnerScript.select(mainPage, data);
      } else {
        // Unimplemented for classic script.
      }
    }

    /**
     * Select the record.
     *
     * @param mainPage
     *          The application main page.
     *
     */
    public static void open(MainPage mainPage) {
      if (mainPage.isOnNewLayout()) {
        BusinessPartnerScript.open(mainPage);
      } else {
        // Unimplemented for classic script.
      }
    }

    /**
     * Close the window.
     */
    public static void close(MainPage mainPage) {
      if (mainPage.isOnNewLayout()) {
        BusinessPartnerScript.close(mainPage);
      } else {
        // Unimplemented for classic script.
      }
    }

  }

  /**
   * Executes and verifies actions on the tab.
   *
   * @author plujan
   *
   */
  public static class Customer {

    /**
     * Edit a record.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     *          The data of the edited record.
     * @throws OpenbravoERPTestException
     */
    public static void edit(MainPage mainPage, CustomerData data) throws OpenbravoERPTestException {
      if (mainPage.isOnNewLayout()) {
        CustomerScript.edit(mainPage, data);
      } else {
        CustomerClassicScript.edit(mainPage, data);
      }
    }

  }

  /**
   * Executes and verifies actions on the tab.
   *
   * @author aferraz
   *
   */
  public static class VendorCreditor {

    /**
     * Edit a record.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     *          The data of the edited record.
     * @throws OpenbravoERPTestException
     */
    public static void edit(MainPage mainPage, VendorCreditorData data)
        throws OpenbravoERPTestException {
      if (mainPage.isOnNewLayout()) {
        VendorCreditorScript.edit(mainPage, data);
      } else {
        VendorCreditorClassicScript.edit(mainPage, data);
      }
    }

  }

  /**
   * Executes and verifies actions on the tab.
   *
   * @author plujan
   *
   */
  public static class Location {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     *
     */
    public static void create(MainPage mainPage, LocationAddressData data) {
      if (mainPage.isOnNewLayout()) {
        LocationScript.create(mainPage, data);
      } else {
        LocationClassicScript.create(mainPage, data);
      }
    }

  }

  /**
   * Executes and verifies actions on the Customer Accounting tab.
   *
   * @author rqueralta
   *
   */
  public static class CustomerAccountingTab {

    public static void verify(MainPage mainPage, CustomerAccountingData customerAccountingData) {
      if (mainPage.isOnNewLayout()) {
        CustomerAccountingScript.verify(mainPage, customerAccountingData);
      } else {
        CustomerAccountingClassicScript.verify(mainPage, customerAccountingData);
      }
    }

    public static void verifyExpectedCount(MainPage mainPage, int expectedCount) {
      if (mainPage.isOnNewLayout()) {
        CustomerAccountingScript.verifyExpectedCount(mainPage, expectedCount);
      } else {
        CustomerAccountingClassicScript.verifyExpectedCount(mainPage, expectedCount);
      }
    }
  }

  /**
   * Executes and verifies actions on the Vendor Accounting tab.
   *
   * @author rqueralta
   *
   */
  public static class VendorAccountingTab {

    public static void verify(MainPage mainPage, VendorAccountingData vendorAccountingData) {
      if (mainPage.isOnNewLayout()) {
        VendorAccountingScript.verify(mainPage, vendorAccountingData);
      } else {
        VendorAccountingClassicScript.verify(mainPage, vendorAccountingData);
      }
    }

    public static void verifyExpectedCount(MainPage mainPage, int expectedCount) {
      if (mainPage.isOnNewLayout()) {
        VendorAccountingScript.verifyExpectedCount(mainPage, expectedCount);
      } else {
        VendorAccountingClassicScript.verifyExpectedCount(mainPage, expectedCount);
      }
    }
  }

}
