/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Pablo Lujan <pablo.lujan@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.masterdata.pricing.pricelist;

import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.PriceListVersionData;
import com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelist.PriceListVersionTab;
import com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelist.PriceListWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * Executes and verifies actions on the Price List Version tab of OpenbravoERP.
 *
 * @author elopio
 *
 */
class PriceListVersionScript {

  /**
   * Create a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data of the record that will be created.
   */
  static void create(MainPage mainPage, PriceListVersionData data) {
    final PriceListWindow priceListWindow = (PriceListWindow) mainPage
        .getView(PriceListWindow.TITLE);

    final PriceListVersionTab priceListVersionTab = priceListWindow.selectPriceListVersionTab();
    priceListVersionTab.createRecord(data);
    priceListVersionTab.assertSaved();
  }

  /**
   * Execute the process that creates the Price List.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @throws OpenbravoERPTestException
   */
  static void processCreatePricelist(MainPage mainPage) throws OpenbravoERPTestException {
    final PriceListWindow priceListWindow = (PriceListWindow) mainPage
        .getView(PriceListWindow.TITLE);

    final PriceListVersionTab priceListVersionTab = priceListWindow.selectPriceListVersionTab();
    priceListVersionTab.process();
    // TODO see issue 17291 - https://issues.openbravo.com/view.php?id=17291.
    // priceListVersionTab.assertProcessCompletedSuccessfully();
  }

  /**
   * Execute the process that creates the Price List.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @throws OpenbravoERPTestException
   */
  static void select(MainPage mainPage, PriceListVersionData data)
      throws OpenbravoERPTestException {
    final PriceListWindow priceListWindow = (PriceListWindow) mainPage
        .getView(PriceListWindow.TITLE);

    final PriceListVersionTab priceListVersionTab = priceListWindow.selectPriceListVersionTab();
    priceListVersionTab.select(data);
  }

}
