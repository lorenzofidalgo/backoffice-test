/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2019 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.masterdata.product;

import com.openbravo.test.integration.erp.data.masterdata.product.CharacteristicsData;
import com.openbravo.test.integration.erp.data.masterdata.product.PriceData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductStockData;
import com.openbravo.test.integration.erp.gui.masterdata.product.CharacteristicsTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductCharacteristicConfigurationTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductStockTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;

/**
 * Executes and verifies actions on the window.
 *
 * @author Pablo Lujan
 */
public class Product extends
    SmokeTabScript<ProductData, com.openbravo.test.integration.erp.gui.masterdata.product.ProductTab>
    implements SmokeWindowScript<ProductWindow> {

  public Product(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Executes and verifies actions on the tab.
   *
   * @author plujan
   *
   */
  public static class ProductTab {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param productData
     *
     */
    public static void create(MainPage mainPage, ProductData productData) {
      if (mainPage.isOnNewLayout()) {
        ProductScript.create(mainPage, productData);
      } else {
        ProductClassicScript.create(mainPage, productData);
      }
    }

    /**
     * Select the record.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     *
     */
    public static void select(MainPage mainPage, ProductData data) {
      if (mainPage.isOnNewLayout()) {
        ProductScript.select(mainPage, data);
      } else {
        // Unimplemented for classic script.
      }
    }

    /**
     * Open Product window.
     *
     *
     */
    public static ProductWindow open(MainPage mainPage) {
      ProductWindow productWindow = new ProductWindow();
      mainPage.openView(productWindow);

      return productWindow;
    }

    /**
     * Count the records.
     *
     * @param mainPage
     *          The application main page.
     * @param productData
     *
     */
    public static int getRecordCount(MainPage mainPage, ProductData productData) {
      if (mainPage.isOnNewLayout()) {
        return ProductScript.getRecordCount(mainPage, productData);
      } else {
        return ProductClassicScript.getRecordCount(mainPage, productData);
      }
    }

    /**
     * Delete a record.
     *
     * @param mainPage
     *          The application main page.
     * @param productData
     *          Record to delete.
     */
    public static void delete(MainPage mainPage, ProductData productData) {
      ProductWindow productWindow = new ProductWindow();
      mainPage.openView(productWindow);

      com.openbravo.test.integration.erp.gui.masterdata.product.ProductTab productTab = productWindow
          .selectProductTab();
      productTab.closeForm();
      productTab.clearFilters();
      productTab.filter(productData);
      productTab.deleteMultipleRecordsOneCheckboxOnGrid();
      productTab.closeMessageIfVisible();
    }
  }

  public static class ProductCharacteristicsValueTab {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param productData
     *
     */
    public static void create(MainPage mainPage, CharacteristicsData productData) {
      if (mainPage.isOnNewLayout()) {
        ProductScript.create(mainPage, productData);
      }
    }

    /**
     * Delete some records.
     *
     * @param mainPage
     *          The application main page.
     */
    public static void delete(MainPage mainPage) {
      ProductWindow productWindow = new ProductWindow();
      mainPage.openView(productWindow);

      CharacteristicsTab productTab = productWindow.selectCharacteristicsTab();
      productTab.clearFilters();
      productTab.deleteMultipleRecordsOneCheckboxOnGrid();
      productTab.closeMessageIfVisible();
    }

    public static void select(MainPage mainPage, CharacteristicsData productData) {
      final ProductWindow productPriceWindow = (ProductWindow) mainPage
          .getView(ProductWindow.TITLE);

      CharacteristicsTab productTab = productPriceWindow.selectCharacteristicsTab();
      productTab.select(productData);
    }

  }

  /**
   * Executes and verifies actions on the tab.
   *
   * @author plujan
   *
   */
  public static class Price {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param priceData
     *
     */
    public static void create(MainPage mainPage, PriceData priceData) {
      if (mainPage.isOnNewLayout()) {
        PriceScript.create(mainPage, priceData);
      } else {
        PriceClassicScript.create(mainPage, priceData);
      }
    }
  }

  public static class CharacteristicsConfiguration {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     *
     */
    public static void verifyCount(MainPage mainPage, int count) {
      final ProductWindow productPriceWindow = (ProductWindow) mainPage
          .getView(ProductWindow.TITLE);
      ProductCharacteristicConfigurationTab productCCTab = productPriceWindow
          .selectCharacteristicConfigurationTab();
      productCCTab.assertCount(count);
    }

  }

  @Override
  protected com.openbravo.test.integration.erp.gui.masterdata.product.ProductTab selectTab() {
    ProductWindow window = (ProductWindow) mainPage.getView(ProductWindow.TITLE);
    com.openbravo.test.integration.erp.gui.masterdata.product.ProductTab tab = (com.openbravo.test.integration.erp.gui.masterdata.product.ProductTab) window
        .selectTab(com.openbravo.test.integration.erp.gui.masterdata.product.ProductTab.IDENTIFIER);
    tab.waitUntilSelected();
    return tab;
  }

  @Override
  public SmokeWindowScript<ProductWindow> open() {
    final ProductWindow productWindow = new ProductWindow();
    mainPage.openView(productWindow);
    return this;
  }

  public class ProductStock extends SmokeTabScript<ProductStockData, ProductStockTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public ProductStock(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Stock tab.
     *
     * Before this, the Product window must be opened and this tab has to be accessible from the
     * currently selected one.
     *
     * @return the Stock tab GUI object.
     */
    @Override
    protected ProductStockTab selectTab() {
      ProductWindow window = (ProductWindow) mainPage.getView(ProductWindow.TITLE);
      ProductStockTab tab = (ProductStockTab) window.selectTab(ProductStockTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }
}
