/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>,
 *   David Miguélez <david.miguelez@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.procurement.transactions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentDisplayLogic;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.DiscountsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.TaxData;
import com.openbravo.test.integration.erp.data.sharedtabs.ExchangeRatesData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseinvoice.BasicDiscountsTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseinvoice.ExchangeRatesTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseinvoice.PaymentOutDetailsTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseinvoice.PaymentOutPlanTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseinvoice.PurchaseInvoiceWindow;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseinvoice.TaxTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on the window.
 *
 * @author Pablo Lujan
 */
public class PurchaseInvoice
    extends SmokeTabScript<PurchaseInvoiceHeaderData, PurchaseInvoiceHeaderTab>
    implements SmokeWindowScript<PurchaseInvoiceWindow> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public PurchaseInvoice(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the window.
   *
   * @return the opened window.
   */
  @Override
  public PurchaseInvoice open() {
    final PurchaseInvoiceWindow window = new PurchaseInvoiceWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the tab.
   *
   * Before this, the window must be opened.
   *
   * @return the tab GUI object.
   */
  @Override
  protected PurchaseInvoiceHeaderTab selectTab() {
    final PurchaseInvoiceWindow window = (PurchaseInvoiceWindow) mainPage
        .getView(PurchaseInvoiceWindow.TITLE);
    mainPage.getTabSetMain().selectTab(PurchaseInvoiceWindow.TITLE);
    final PurchaseInvoiceHeaderTab tab = (PurchaseInvoiceHeaderTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  /**
   * Complete the record.
   *
   */
  public void complete() {
    logger.info("Completing purchase invoice.");
    final PurchaseInvoiceHeaderTab tab = selectTab();
    tab.complete();
  }

  /**
   * Void the record.
   *
   */
  public void close() {
    logger.info("Void purchase invoice.");
    final PurchaseInvoiceHeaderTab tab = selectTab();
    tab.close();
  }

  /**
   * Payment process.
   *
   */
  // public void process() {
  // logger.info("Process payment plan.");
  // final PurchaseInvoiceHeaderTab tab = selectTab();
  // // TODO: Add the process button
  // // tab.process();
  // }

  /**
   * Create lines from order.
   *
   * @param order
   *          The order identifier.
   */
  public void createLinesFrom(String order) {
    logger.info("Create lines from order.");
    final PurchaseInvoiceHeaderTab tab = selectTab();
    tab.createLinesFrom(order);
  }

  /**
   * Create lines from shipment.
   *
   * @param shipment
   *          The shipment identifier.
   */
  public void createLinesFromShipment(String shipment) {
    logger.info("Create lines from shipment.");
    final PurchaseInvoiceHeaderTab tab = selectTab();
    tab.createLinesFromShipment(shipment);
  }

  /**
   * Create lines from shipment.
   *
   * @param shipment
   *          The shipment identifier.
   * @param lineNo
   *          The shipment line No.
   */
  public void createLinesFromShipment(String shipment, int lineNo) {
    logger.info("Create lines from createLinesFromShipment.");
    final PurchaseInvoiceHeaderTab tab = selectTab();
    tab.createLinesFromShipment(shipment, lineNo);
  }

  /**
   * Create lines from order.
   *
   * @param order
   *          The order identifier.
   * @param lineNo
   *          The order line No.
   */
  public void createLinesFrom(String order, int lineNo) {
    logger.info("Create lines from order.");
    final PurchaseInvoiceHeaderTab tab = selectTab();
    tab.createLinesFrom(order, lineNo);
  }

  /**
   * Check whether the purchase invoice is already posted or not.
   */
  public boolean isPosted() {
    logger.info("Checking whether the purchase invoice is already posted or not.");
    PurchaseInvoiceHeaderTab tab = selectTab();
    return tab.isPosted();
  }

  /**
   * Post the sales invoice.
   */
  public void post() {
    logger.info("Post the purchase invoice.");
    PurchaseInvoiceHeaderTab tab = selectTab();
    tab.post();
  }

  /**
   * Unpost the sales invoice.
   */
  public void unpost() {
    logger.info("Unpost the sales invoice.");
    PurchaseInvoiceHeaderTab tab = selectTab();
    tab.unpost();
  }

  /**
   * Assert the data displayed in the add payment in pop up.
   *
   * @param data
   *          The data of the payment.
   */
  public void assertAddPaymentInData(AddPaymentPopUpData data) {
    PurchaseInvoiceHeaderTab tab = selectTab();
    tab.assertAddPaymentInData(data);
  }

  /**
   * Add a payment and process it.
   *
   * @param action
   *          The process action to request after the payment.
   */
  public void addPayment(String action) {
    logger.info("Add payment.");
    PurchaseInvoiceHeaderTab tab = selectTab();
    tab.addPayment(action);
  }

  public void addPayment(String action, AddPaymentPopUpData addPaymentTotalsVerificationData) {
    logger.info("Add payment.");
    PurchaseInvoiceHeaderTab tab = selectTab();
    tab.addPayment(action, addPaymentTotalsVerificationData);
  }

  /**
   * Open Add Payment pop
   */
  public AddPaymentProcess openAddPayment() {
    logger.info("Opening Add Payment.");
    PurchaseInvoiceHeaderTab tab = selectTab();
    AddPaymentProcess addPaymentProcess = tab.openAddPayment();
    return addPaymentProcess;
  }

  /**
   * Assert that the payment creation completed successfully.
   */
  public void assertPaymentCreatedSuccessfully() {
    PurchaseInvoiceHeaderTab tab = selectTab();
    tab.assertPaymentCreatedSuccessfully();
  }

  /**
   * Assert that the payment creation completed successfully.
   */
  public void assertPaymentCreatedSuccessfully2() {
    PurchaseInvoiceHeaderTab tab = selectTab();
    tab.assertPaymentCreatedSuccessfully2();
  }

  /**
   * Assert display logic visibility from AddPayment Popup
   *
   */
  public void assertDisplayLogicVisible(AddPaymentDisplayLogic visibleDisplayLogic) {
    logger.info("Asserting display logic visibility from AddPayment Popup.");
    final PurchaseInvoiceHeaderTab tab = selectTab();
    tab.assertDisplayLogicVisibleAddPaymentOut(visibleDisplayLogic);
  }

  /**
   * Assert display logic enable status from AddPayment Popup
   *
   */
  public void assertDisplayLogicEnabled(AddPaymentDisplayLogic enabledDisplayLogic) {
    logger.info("Asserting display logic enable status from AddPayment Popup.");
    final PurchaseInvoiceHeaderTab tab = selectTab();
    tab.assertDisplayLogicEnabledAddPaymentOut(enabledDisplayLogic);
  }

  /**
   * Assert display logic expanded status from AddPayment Popup
   *
   */
  public void assertDisplayLogicExpanded(AddPaymentDisplayLogic expandedDisplayLogic) {
    logger.info("Asserting display logic expanded status from AddPayment Popup.");
    final PurchaseInvoiceHeaderTab tab = selectTab();
    tab.assertDisplayLogicExpandedAddPaymentOut(expandedDisplayLogic);
  }

  /**
   * Executes and verifies actions on the Purchase Invoice Lines tab.
   *
   * @author elopio
   *
   */
  public class Lines extends SmokeTabScript<PurchaseInvoiceLinesData, PurchaseInvoiceLinesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Lines(MainPage mainPage) {
      super(mainPage);
      // Selenium was working too fast and due to a problem with the focus, the following static
      // sleep has been added. Changing from
      // PurchaseInvoice to PurchaseInvoiceLines the focus did not change to PurchaseInvoiceLines
      // and stayed in PurchaseInvoice.
      Sleep.trySleep(500);
    }

    /**
     * Select and return the Lines tab. SalesInvoiceWindow Before this, the Sales Order window must
     * be opened and this tab has to be accessible from the currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected PurchaseInvoiceLinesTab selectTab() {
      final PurchaseInvoiceWindow window = (PurchaseInvoiceWindow) mainPage
          .getView(PurchaseInvoiceWindow.TITLE);
      final PurchaseInvoiceLinesTab tab = (PurchaseInvoiceLinesTab) window
          .selectTab(PurchaseInvoiceLinesTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }

  /**
   * Executes and verifies actions on the Payment Out Plan tab.
   *
   * @author plujan
   *
   */
  public class PaymentOutPlan extends SmokeTabScript<PaymentPlanData, PaymentOutPlanTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public PaymentOutPlan(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Payment In Plan tab.
     *
     * Before this, the Sales Invoice window must be opened and this tab has to be accessible from
     * the currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected PaymentOutPlanTab selectTab() {
      PurchaseInvoiceWindow window = (PurchaseInvoiceWindow) mainPage
          .getView(PurchaseInvoiceWindow.TITLE);
      PaymentOutPlanTab tab = (PaymentOutPlanTab) window.selectTab(PaymentOutPlanTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

    /**
     * Executes and verifies actions on the Payment Out Details tab.
     *
     * @author elopio
     *
     */
    public class PaymentOutDetails
        extends SmokeTabScript<PaymentDetailsData, PaymentOutDetailsTab> {

      /**
       * Class constructor.
       *
       * @param mainPage
       *          The application main page.
       */
      public PaymentOutDetails(MainPage mainPage) {
        super(mainPage);
      }

      /**
       * Select and return the Payment Out Plan tab.
       *
       * Before this, the Purchase Invoice window must be opened and this tab has to be accessible
       * from the currently selected one.
       *
       * @return the Header tab GUI object.
       */
      @Override
      protected PaymentOutDetailsTab selectTab() {
        PurchaseInvoiceWindow window = (PurchaseInvoiceWindow) mainPage
            .getView(PurchaseInvoiceWindow.TITLE);
        PaymentOutDetailsTab tab = (PaymentOutDetailsTab) window
            .selectTab(PaymentOutDetailsTab.IDENTIFIER);
        tab.waitUntilSelected();
        return tab;
      }

      public void clearFilters() {
        PaymentOutDetailsTab tab = selectTab();
        tab.clearFilters();
      }
    }

    public void clearFilters() {
      PaymentOutPlanTab tab = selectTab();
      tab.clearFilters();
    }

    public void unselectAll() {
      PaymentOutPlanTab tab = selectTab();
      tab.unselectAll();
    }
  }

  /**
   * Executes and verifies actions on the Sales Invoice Exchange rates tab.
   *
   * @author David Miguélez
   *
   */
  public class ExchangeRates extends SmokeTabScript<ExchangeRatesData, ExchangeRatesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public ExchangeRates(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Exchange rates tab.
     *
     * Before this, the Purchase Invoice window must be opened and this tab has to be accessible
     * from the currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected ExchangeRatesTab selectTab() {
      PurchaseInvoiceWindow window = (PurchaseInvoiceWindow) mainPage
          .getView(PurchaseInvoiceWindow.TITLE);
      ExchangeRatesTab tab = (ExchangeRatesTab) window.selectTab(ExchangeRatesTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

  }

  /**
   * Executes and verifies actions on the Purchase Invoice Basic Discounts tab.
   *
   * @author Atul Gaware
   *
   */

  public class BasicDiscounts extends SmokeTabScript<DiscountsData, BasicDiscountsTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public BasicDiscounts(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Basic Discounts tab.
     *
     * Before this, the Purchase Invoice window must be opened and this tab has to be accessible
     * from the currently selected one.
     *
     * @return the Header tab GUI object.
     */

    @Override
    protected BasicDiscountsTab selectTab() {
      PurchaseInvoiceWindow window = (PurchaseInvoiceWindow) mainPage
          .getView(PurchaseInvoiceWindow.TITLE);
      BasicDiscountsTab tab = (BasicDiscountsTab) window.selectTab(BasicDiscountsTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

  }

  /**
   * Executes and verifies actions on the Purchase Invoice Tax tab.
   *
   * @author Atul Gaware
   *
   */

  public class Tax extends SmokeTabScript<TaxData, TaxTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Tax(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Tax tab.
     *
     * Before this, the Purchase Invoice window must be opened and this tab has to be accessible
     * from the currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected TaxTab selectTab() {
      PurchaseInvoiceWindow window = (PurchaseInvoiceWindow) mainPage
          .getView(PurchaseInvoiceWindow.TITLE);
      TaxTab tab = (TaxTab) window.selectTab(TaxTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

  }

}
