/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *   Nono Carballo <nonofce@gmail.com>,
 *   Mark Molina <markmm82@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.procurement.transactions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentDisplayLogic;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.DiscountsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.TaxData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseorder.BasicDiscountsTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseorder.PaymentOutDetailsTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseorder.PaymentOutPlanTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseorder.PurchaseOrderHeaderTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseorder.PurchaseOrderLinesTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseorder.PurchaseOrderWindow;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseorder.TaxTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on the Purchase Order window.
 *
 * @author Pablo Lujan
 */
public class PurchaseOrder extends SmokeTabScript<PurchaseOrderHeaderData, PurchaseOrderHeaderTab>
    implements SmokeWindowScript<PurchaseOrderWindow> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public PurchaseOrder(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the window.
   *
   * @return the opened window.
   */
  @Override
  public PurchaseOrder open() {
    final PurchaseOrderWindow window = new PurchaseOrderWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the tab.
   *
   * Before this, the window must be opened.
   *
   * @return the tab GUI object.
   */
  @Override
  protected PurchaseOrderHeaderTab selectTab() {
    PurchaseOrderWindow window = (PurchaseOrderWindow) mainPage.getView(PurchaseOrderWindow.TITLE);
    mainPage.getTabSetMain().selectTab(PurchaseOrderWindow.TITLE);
    PurchaseOrderHeaderTab tab = (PurchaseOrderHeaderTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  /**
   * Book the order.
   *
   */
  public void book() {
    logger.info("Book the purchase order.");
    final PurchaseOrderHeaderTab tab = selectTab();
    tab.book();
  }

  /**
   * Open Add Payment pop
   */
  public AddPaymentProcess openAddPayment() {
    logger.info("Opening Add Payment.");
    PurchaseOrderHeaderTab tab = selectTab();
    AddPaymentProcess addPaymentProcess = tab.openAddPayment();
    return addPaymentProcess;
  }

  /*
   * Assert display logic visibility from AddPayment Popup
   */
  public void assertAddPaymentDisplayLogicVisible(AddPaymentDisplayLogic visibleDisplayLogic) {
    logger.info("Asserting display logic visibility from AddPayment Popup.");
    final PurchaseOrderHeaderTab tab = selectTab();
    tab.assertAddPaymentDisplayLogicVisible(visibleDisplayLogic);
  }

  /**
   * Assert display logic enable status from AddPayment Popup
   *
   */
  public void assertAddPaymentDisplayLogicEnabled(AddPaymentDisplayLogic enabledDisplayLogic) {
    logger.info("Asserting display logic enable status from AddPayment Popup.");
    final PurchaseOrderHeaderTab tab = selectTab();
    tab.assertAddPaymentDisplayLogicEnabled(enabledDisplayLogic);
  }

  /**
   * Assert display logic expanded status from AddPayment Popup
   *
   */
  public void assertAddPaymentDisplayLogicExpanded(AddPaymentDisplayLogic expandedDisplayLogic) {
    logger.info("Asserting display logic expanded status from AddPayment Popup.");
    final PurchaseOrderHeaderTab tab = selectTab();
    tab.assertAddPaymentDisplayLogicExpanded(expandedDisplayLogic);
  }

  public void assertPaymentCreatedSuccessfully() {
    selectTab().assertPaymentCreatedSuccessfully();
  }

  /**
   * Add a payment and process it.
   *
   * @param action
   *          The process action to request after the payment.
   */
  public void addPayment(String action) {
    logger.info("Add payment.");
    PurchaseOrderHeaderTab tab = selectTab();
    tab.addPayment(null, null, action, null);
  }

  /**
   * Add a payment and process it.
   *
   * @param payment
   *          The payment amount.
   * @param action
   *          The process action to request after the payment.
   */
  public void addPayment(String payment, String action) {
    logger.info("Add payment.");
    PurchaseOrderHeaderTab tab = selectTab();
    tab.addPayment(null, payment, action, null);
  }

  /**
   * Add a payment and process it.
   *
   * @param action
   *          The process action to request after the payment.
   * @param addPaymentTotalsVerificationData
   *          The verification data.
   */
  public void addPayment(String action, AddPaymentPopUpData addPaymentTotalsVerificationData) {
    logger.info("Add payment.");
    PurchaseOrderHeaderTab tab = selectTab();
    tab.addPayment(null, null, action, addPaymentTotalsVerificationData);
  }

  /**
   * Add a payment and process it.
   *
   * @param account
   *          The financial account.
   * @param action
   *          The process action to request after the payment.
   * @param addPaymentPopUpData
   *          The verification data.
   */
  public void addPayment(String account, String action, AddPaymentPopUpData addPaymentPopUpData) {
    logger.info("Add payment.");
    PurchaseOrderHeaderTab tab = selectTab();
    tab.addPayment(account, null, action, addPaymentPopUpData);
  }

  /**
   * Add a payment and process it.
   *
   * @param account
   *          The financial account.
   * @param payment
   *          The payment amount.
   * @param action
   *          The process action to request after the payment.
   * @param addPaymentPopUpData
   */
  public void addPayment(String account, String payment, String action,
      AddPaymentPopUpData addPaymentPopUpData) {
    logger.info("Add payment.");
    PurchaseOrderHeaderTab tab = selectTab();
    tab.addPayment(account, payment, action, addPaymentPopUpData);
  }

  public void copyRecord() {
    PurchaseOrderHeaderTab tab = selectTab();
    tab.copyRecord();
  }

  public void copyLines(boolean isUomManagementEnabled) {
    PurchaseOrderHeaderTab tab = selectTab();
    tab.copyLines(isUomManagementEnabled);
  }

  public PickAndExecuteWindow<PurchaseOrderHeaderData> copyFromOrders() {
    PurchaseOrderHeaderTab tab = selectTab();
    return tab.copyFromOrders();
  }

  /**
   * Executes and verifies actions on the Purchase Order Lines tab.
   *
   * @author elopio
   *
   */
  public class Lines extends SmokeTabScript<PurchaseOrderLinesData, PurchaseOrderLinesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Lines(MainPage mainPage) {
      super(mainPage);
      // Selenium was working too fast and due to a problem with the focus, the following static
      // sleep has been added. Changing from
      // GoodShipment to GoodShipmentLines the focus did not change to GoodShipmentLines
      // and stayed in GoodShipment.
      Sleep.trySleep(500);
    }

    /**
     * Select and return the Lines tab.
     *
     * Before this, the Purchase Order window must be opened and this tab has to be accessible from
     * the currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected PurchaseOrderLinesTab selectTab() {
      final PurchaseOrderWindow window = (PurchaseOrderWindow) mainPage
          .getView(PurchaseOrderWindow.TITLE);
      final PurchaseOrderLinesTab tab = (PurchaseOrderLinesTab) window
          .selectTab(PurchaseOrderLinesTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }

  /**
   * Executes and verifies actions on the Payment Out Plan tab.
   *
   * @author elopio
   *
   */
  public class PaymentOutPlan extends SmokeTabScript<PaymentPlanData, PaymentOutPlanTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public PaymentOutPlan(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Lines tab.
     *
     * Before this, the Purchase Order window must be opened and this tab has to be accessible from
     * the currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected PaymentOutPlanTab selectTab() {
      final PurchaseOrderWindow window = (PurchaseOrderWindow) mainPage
          .getView(PurchaseOrderWindow.TITLE);
      final PaymentOutPlanTab tab = (PaymentOutPlanTab) window
          .selectTab(PaymentOutPlanTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

    /**
     * Executes and verifies actions on the Payment Out Details tab.
     *
     * @author aferraz
     *
     */
    public class PaymentOutDetails
        extends SmokeTabScript<PaymentDetailsData, PaymentOutDetailsTab> {

      /**
       * Class constructor.
       *
       * @param mainPage
       *          The application main page.
       */
      public PaymentOutDetails(MainPage mainPage) {
        super(mainPage);
      }

      /**
       * Select and return the Payment Out Details tab.
       *
       * Before this, the Purchase Order window must be opened and this tab has to be accessible
       * from the currently selected one.
       *
       * @return the Header tab GUI object.
       */
      @Override
      protected PaymentOutDetailsTab selectTab() {
        PurchaseOrderWindow window = (PurchaseOrderWindow) mainPage
            .getView(PurchaseOrderWindow.TITLE);
        PaymentOutDetailsTab tab = (PaymentOutDetailsTab) window
            .selectTab(PaymentOutDetailsTab.IDENTIFIER);
        tab.waitUntilSelected();
        return tab;
      }
    }
  }

  /**
   * Executes and verifies actions on the Purchase Order Tax tab.
   *
   * @author nonofce
   *
   */
  public class Tax extends SmokeTabScript<TaxData, TaxTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Tax(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Tax tab.
     *
     * Before this, the Purchase Order window must be opened and this tab has to be accessible from
     * the currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected TaxTab selectTab() {
      PurchaseOrderWindow window = (PurchaseOrderWindow) mainPage
          .getView(PurchaseOrderWindow.TITLE);
      TaxTab tab = (TaxTab) window.selectTab(TaxTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }

  /**
   * Executes and verifies actions on the Purchase Order BasicDiscounts tab.
   *
   * @author collazoandy4
   *
   */
  public class BasicDiscounts extends SmokeTabScript<DiscountsData, BasicDiscountsTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public BasicDiscounts(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the {@link BasicDiscountsTab} tab.
     *
     * Before this, the Purchase Order window must be opened and this tab has to be accessible from
     * the currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected BasicDiscountsTab selectTab() {
      PurchaseOrderWindow window = (PurchaseOrderWindow) mainPage
          .getView(PurchaseOrderWindow.TITLE);
      BasicDiscountsTab tab = (BasicDiscountsTab) window.selectTab(BasicDiscountsTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }
}
