/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <f.carballo@nectus.com>
 *************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.procurement.transactions;

import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.PaymentOutDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.PaymentOutPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorLineData;
import com.openbravo.test.integration.erp.data.selectors.ReturnToVendorLineSelectorData;
import com.openbravo.test.integration.erp.gui.procurement.transactions.returntovendor.PaymentOutDetailsTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.returntovendor.PaymentOutPlanTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.returntovendor.ReturnToVendorHeaderTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.returntovendor.ReturnToVendorLinesTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.returntovendor.ReturnToVendorWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;

/**
 *
 * Class for Return to Vendor
 *
 * @author Nono Carballo
 *
 */
public class ReturnToVendor
    extends SmokeTabScript<ReturnToVendorHeaderData, ReturnToVendorHeaderTab>
    implements SmokeWindowScript<ReturnToVendorWindow> {

  public ReturnToVendor(MainPage mainPage) {
    super(mainPage);
  }

  @Override

  public SmokeWindowScript<ReturnToVendorWindow> open() {
    final ReturnToVendorWindow returnToVendorWindow = new ReturnToVendorWindow();
    mainPage.openView(returnToVendorWindow);
    return this;
  }

  @Override
  protected ReturnToVendorHeaderTab selectTab() {
    ReturnToVendorWindow window = (ReturnToVendorWindow) mainPage
        .getView(ReturnToVendorWindow.TITLE);
    mainPage.getTabSetMain().selectTab(ReturnToVendorWindow.TITLE);
    ReturnToVendorHeaderTab tab = (ReturnToVendorHeaderTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  public void book() {
    final ReturnToVendorHeaderTab tab = selectTab();
    tab.book();
  }

  public void assertErrorFieldsContainIllegalValues11() {
    ReturnToVendorHeaderTab tab = selectTab();
    tab.assertErrorFieldsContainIllegalValues();
    tab.assertProcessCompletedSuccessfully2();
  }

  public PickAndExecuteWindow<ReturnToVendorLineSelectorData> openPickAndEditLines() {
    return selectTab().openPickAndEditLines();
  }

  public class Lines extends SmokeTabScript<ReturnToVendorLineData, ReturnToVendorLinesTab> {

    public Lines(MainPage mainPage) {
      super(mainPage);
    }

    @Override
    protected ReturnToVendorLinesTab selectTab() {
      final ReturnToVendorWindow window = (ReturnToVendorWindow) mainPage
          .getView(ReturnToVendorWindow.TITLE);
      final ReturnToVendorLinesTab tab = (ReturnToVendorLinesTab) window
          .selectTab(ReturnToVendorLinesTab.IDENTIFIER);
      return tab;
    }

  }

  public class PaymentOutPlan extends SmokeTabScript<PaymentOutPlanData, PaymentOutPlanTab> {

    public PaymentOutPlan(MainPage mainPage) {
      super(mainPage);
    }

    @Override
    protected PaymentOutPlanTab selectTab() {
      final ReturnToVendorWindow window = (ReturnToVendorWindow) mainPage
          .getView(ReturnToVendorWindow.TITLE);
      final PaymentOutPlanTab tab = (PaymentOutPlanTab) window
          .selectTab(PaymentOutPlanTab.IDENTIFIER);
      return tab;
    }

    public PaymentOutPlanTab selectPaymentOutPlanTab() {
      return this.selectTab();
    }

    public class PaymentOutDetails
        extends SmokeTabScript<PaymentOutDetailsData, PaymentOutDetailsTab> {

      public PaymentOutDetails(MainPage mainPage) {
        super(mainPage);
      }

      @Override
      protected PaymentOutDetailsTab selectTab() {
        final ReturnToVendorWindow window = (ReturnToVendorWindow) mainPage
            .getView(ReturnToVendorWindow.TITLE);
        window.selectTab(PaymentOutPlanTab.IDENTIFIER);
        final PaymentOutDetailsTab tab = (PaymentOutDetailsTab) window
            .selectTab(PaymentOutDetailsTab.IDENTIFIER);
        return tab;
      }

    }

  }
}
