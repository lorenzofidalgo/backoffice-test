/**
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <f.carballo@nectus.com>
 *************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.procurement.transactions;

import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentLineData;
import com.openbravo.test.integration.erp.gui.procurement.transactions.returntovendor.ReturnToVendorShipmentHeaderTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.returntovendor.ReturnToVendorShipmentLinesTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.returntovendor.ReturnToVendorShipmentWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;

/**
 *
 * Class for Return to Vendor
 *
 * @author Rafael Queralta
 *
 */
public class ReturnToVendorShipment
    extends SmokeTabScript<ReturnToVendorShipmentHeaderData, ReturnToVendorShipmentHeaderTab>
    implements SmokeWindowScript<ReturnToVendorShipmentWindow> {

  public ReturnToVendorShipment(MainPage mainPage) {
    super(mainPage);
    // TODO Auto-generated constructor stub
  }

  @Override

  public SmokeWindowScript<ReturnToVendorShipmentWindow> open() {
    final ReturnToVendorShipmentWindow returnToVendorShipmentWindow = new ReturnToVendorShipmentWindow();
    mainPage.openView(returnToVendorShipmentWindow);
    return this;
  }

  @Override
  protected ReturnToVendorShipmentHeaderTab selectTab() {
    ReturnToVendorShipmentWindow window = (ReturnToVendorShipmentWindow) mainPage
        .getView(ReturnToVendorShipmentWindow.TITLE);
    mainPage.getTabSetMain().selectTab(ReturnToVendorShipmentWindow.TITLE);
    ReturnToVendorShipmentHeaderTab tab = (ReturnToVendorShipmentHeaderTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  public PickAndExecuteWindow<ReturnToVendorShipmentLineData> openPickAndEditLines() {
    ReturnToVendorShipmentHeaderTab tab = selectTab();
    return tab.openPickAndEditLines();
  }

  public void complete() {
    final ReturnToVendorShipmentHeaderTab tab = selectTab();
    tab.complete();
  }

  public class Lines
      extends SmokeTabScript<ReturnToVendorShipmentLineData, ReturnToVendorShipmentLinesTab> {

    public Lines(MainPage mainPage) {
      super(mainPage);
    }

    @Override
    protected ReturnToVendorShipmentLinesTab selectTab() {
      final ReturnToVendorShipmentWindow window = (ReturnToVendorShipmentWindow) mainPage
          .getView(ReturnToVendorShipmentWindow.TITLE);
      final ReturnToVendorShipmentLinesTab tab = (ReturnToVendorShipmentLinesTab) window
          .selectTab(ReturnToVendorShipmentLinesTab.IDENTIFIER);
      return tab;
    }

  }

}
