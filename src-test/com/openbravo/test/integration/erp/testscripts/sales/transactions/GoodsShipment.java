/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.sales.transactions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentLinesData;
import com.openbravo.test.integration.erp.gui.sales.transactions.goodsshipment.GoodsShipmentHeaderTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.goodsshipment.GoodsShipmentLinesTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.goodsshipment.GoodsShipmentWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on the Goods Shipment window.
 *
 * @author elopio
 */
public class GoodsShipment extends SmokeTabScript<GoodsShipmentHeaderData, GoodsShipmentHeaderTab>
    implements SmokeWindowScript<GoodsShipmentWindow> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public GoodsShipment(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the Goods Shipment window.
   *
   * @return this Goods Shipment script.
   */
  @Override
  public GoodsShipment open() {
    GoodsShipmentWindow window = new GoodsShipmentWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the Header tab.
   *
   * Before this, the Goods Shipment window must be opened.
   *
   * @return the Header tab GUI object.
   */
  @Override
  protected GoodsShipmentHeaderTab selectTab() {
    GoodsShipmentWindow window = (GoodsShipmentWindow) mainPage.getView(GoodsShipmentWindow.TITLE);
    mainPage.getTabSetMain().selectTab(GoodsShipmentWindow.TITLE);
    GoodsShipmentHeaderTab tab = (GoodsShipmentHeaderTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  /**
   * Create goods shipment lines from order.
   *
   * @param warehouseAlias
   *          The alias of the warehouse.
   * @param order
   *          The order data. This is a string with the form: number - date - amount.
   */
  public void createLinesFrom(String warehouseAlias, String order) {
    createLinesFrom(warehouseAlias, order, 0);
  }

  /**
   * Create goods shipment lines from order.
   *
   * @param warehouseAlias
   *          The alias of the warehouse.
   * @param order
   *          The order data. This is a string with the form: number - date - amount.
   * @param lineNo
   *          The order line number to select or 0 to select all
   */
  public void createLinesFrom(String warehouseAlias, String order, int lineNo) {
    logger.info("Create lines from order '{}'.", order);
    GoodsShipmentHeaderTab tab = selectTab();
    tab.createLinesFrom(warehouseAlias, order, lineNo);
  }

  /**
   * Complete the shipment.
   *
   */
  public void complete() {
    logger.info("Complete the goods shipment.");
    GoodsShipmentHeaderTab tab = selectTab();
    tab.complete();
  }

  /**
   * Complete the shipment.
   *
   */
  public void close() {
    logger.info("Voiding the goods shipment.");
    GoodsShipmentHeaderTab tab = selectTab();
    tab.close();
  }

  /**
   * Executes and verifies actions on the Goods Shipment Lines tab.
   *
   * @author elopio
   *
   */
  public class Lines extends SmokeTabScript<GoodsShipmentLinesData, GoodsShipmentLinesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Lines(MainPage mainPage) {
      super(mainPage);
      // Selenium was working too fast and due to a problem with the focus, the following static
      // sleep has been added. Changing from
      // GoodShipment to GoodShipmentLines the focus did not change to GoodShipmentLines
      // and stayed in GoodShipment.
      Sleep.trySleep(500);
    }

    /**
     * Select and return the Lines tab.
     *
     * Before this, the Goods Shipment window must be opened and this tab has to be accessible from
     * the currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected GoodsShipmentLinesTab selectTab() {
      GoodsShipmentWindow window = (GoodsShipmentWindow) mainPage
          .getView(GoodsShipmentWindow.TITLE);
      GoodsShipmentLinesTab tab = (GoodsShipmentLinesTab) window
          .selectTab(GoodsShipmentLinesTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

    /**
     * Clear the filters.
     */
    public void clearFilters() {
      GoodsShipmentLinesTab tab = selectTab();
      tab.clearFilters();
    }
  }
}
