/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.financial;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentproposal.PaymentProposalHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentProposal;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.util.OBDate;

/**
 * Execute the Setup Client and Organization flow of the smoke test suite.
 *
 * @author Pablo Lujan
 */
@RunWith(Parameterized.class)
public class FINb_PaymentProposal extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  /* Data for [FINb010] Create Purchase Invoice A. */
  /** The purchase invoice header data. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  /** The data to verify the purchase invoice header. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  /** The purchase invoice lines data. */
  PurchaseInvoiceLinesData purchaseInvoiceLinesData;
  /** The data to verify the purchase invoice lines creation. */
  PurchaseInvoiceLinesData purchaseInvoiceLinesVerificationData;
  /** The data to verify the completed purchase invoice header. */
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderData;

  /* Data for [FINb020] Create Purchase Invoice B. */
  /** The purchase invoice header data. */
  PurchaseInvoiceHeaderData purchaseInvoiceBHeaderData;
  /** The data to verify the purchase invoice header. */
  PurchaseInvoiceHeaderData purchaseInvoiceBHeaderVerificationData;
  /** The purchase invoice lines data. */
  PurchaseInvoiceLinesData purchaseInvoiceBLinesData;
  /** The data to verify the purchase invoice lines creation. */
  PurchaseInvoiceLinesData purchaseInvoiceBLinesVerificationData;
  /** The data to verify the completed purchase invoice header. */
  PurchaseInvoiceHeaderData completedPurchaseInvoiceBHeaderData;

  /* Data for [FINb030] Payment proposal. */
  /** The payment proposal header data. */
  PaymentProposalHeaderData paymentProposalHeaderData;

  /**
   * Class constructor.
   *
   * @param purchaseInvoiceHeaderData
   *          The purchase invoice header data.
   * @param purchaseInvoiceHeaderVerificationData
   *          The data to verify the purchase invoice header.
   * @param purchaseInvoiceLinesData
   *          The purchase invoice lines data.
   * @param purchaseInvoiceLinesVerificationData
   *          The data to verify the purchase invoice lines creation.
   * @param completedPurchaseInvoiceHeaderData
   *          The data to verify the completed purchase invoice header.
   * @param paymentProposalHeaderData
   *          The payment proposal header data.
   */
  public FINb_PaymentProposal(PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLinesData,
      PurchaseInvoiceLinesData purchaseInvoiceLinesVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderData,

      PurchaseInvoiceHeaderData purchaseInvoiceBHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceBHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceBLinesData,
      PurchaseInvoiceLinesData purchaseInvoiceBLinesVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceBHeaderData,
      PaymentProposalHeaderData paymentProposalHeaderData) {

    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLinesData = purchaseInvoiceLinesData;
    this.purchaseInvoiceLinesVerificationData = purchaseInvoiceLinesVerificationData;
    this.completedPurchaseInvoiceHeaderData = completedPurchaseInvoiceHeaderData;

    this.purchaseInvoiceBHeaderData = purchaseInvoiceBHeaderData;
    this.purchaseInvoiceBHeaderVerificationData = purchaseInvoiceBHeaderVerificationData;
    this.purchaseInvoiceBLinesData = purchaseInvoiceBLinesData;
    this.purchaseInvoiceBLinesVerificationData = purchaseInvoiceBLinesVerificationData;
    this.completedPurchaseInvoiceBHeaderData = completedPurchaseInvoiceBHeaderData;

    this.paymentProposalHeaderData = paymentProposalHeaderData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<DataObject[]> paymentProposalValues() throws IOException {
    DataObject[][] data = new DataObject[][] { {
        // Parameters for [FINb010] Create Purchase Invoice A.
        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .invoiceDate(OBDate.CURRENT_DATE)
            .priceList("Purchase")
            .paymentMethod("1 (Spain)")
            .paymentTerms("90 days")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("RMA")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("11.2")
            .build(),
        new PurchaseInvoiceLinesData.Builder().uOM("Bag")
            .listPrice("2.00")
            .unitPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("22.40")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("24.64")
            .daysTillDue("90")
            .dueAmount("0.00")
            .paymentComplete(false)
            .summedLineAmount("22.40")
            .grandTotalAmount("24.64")
            .documentStatus("Completed")
            .build(),

        // Parameters for [FINb020] Create Purchase Invoice B.
        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VB").build())
            .paymentMethod("1 (Spain)")
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº12")
            .invoiceDate(OBDate.CURRENT_DATE)
            .priceList("Purchase")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("RMB").build())
            .invoicedQuantity("10")
            .build(),
        new PurchaseInvoiceLinesData.Builder().uOM("Bag")
            .listPrice("2.00")
            .unitPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("20.00")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("22.00")
            .dueAmount("0.00")
            .paymentComplete(false)
            .summedLineAmount("20.00")
            .grandTotalAmount("22.00")
            .documentStatus("Completed")
            .build(),

        // Parameters for [FINb030] Payment proposal.
        new PaymentProposalHeaderData.Builder().paymentMethod("1 (Spain)")
            .account("Spain Cashbook - EUR")
            .duedate(OBDate.addDaysToSystemDate(90))
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * This test case proves that you can create the payment proposal.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void paymentsShouldBeGeneratedFromPaymentProposal()
      throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [FINb0010] Create Purchase Invoice A. **");
    final PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.create(purchaseInvoiceLinesData);
    purchaseInvoiceLines.assertSaved();
    purchaseInvoiceLines.assertData(purchaseInvoiceLinesVerificationData);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderData);
    String purchaseIvoiceADocumentNumber = (String) purchaseInvoice.getData("documentNo");
    logger.info("** End of test case [FINb0010] Create Purchase Invoice A. **");

    logger.info("** Start of test case [FINb0020] Create Purchase Invoice B. **");
    purchaseInvoice.create(purchaseInvoiceBHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceBHeaderVerificationData);
    purchaseInvoiceLines.create(purchaseInvoiceBLinesData);
    purchaseInvoiceLines.assertSaved();
    purchaseInvoiceLines.assertData(purchaseInvoiceBLinesVerificationData);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceBHeaderData);
    String purchaseIvoiceBDocumentNumber = (String) purchaseInvoice.getData("documentNo");
    logger.info("** End of test case [FINb0020] Create Purchase Invoice B. **");

    logger.info("** Start of test case [FINb030] Payment proposal. **");
    final PaymentProposal paymentProposal = new PaymentProposal(mainPage).open();
    paymentProposal.create(paymentProposalHeaderData);
    paymentProposal.assertSaved();
    paymentProposal.selectExpectedPayments(purchaseIvoiceADocumentNumber);
    paymentProposal.assertProcessCompletedSuccessfully2();
    paymentProposal.selectExpectedPayments(purchaseIvoiceBDocumentNumber);
    paymentProposal.assertProcessCompletedSuccessfully2();
    paymentProposal.generatePayments();
    paymentProposal.assertProcessCompletedSuccessfullyContentMatch(
        "Payment .* \\(Vendor A\\) <br>Payment .* \\(Vendor B\\)<br>");
    PaymentProposal.Lines paymentProposalLines = paymentProposal.new Lines(mainPage);
    paymentProposalLines.assertCount(2);
    logger.info("** End of test case [FINb030] Payment proposal. **");
  }
}
