/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.financial;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test to add a payment to a sales invoice.
 *
 * @author Pablo Lujan
 */
@RunWith(Parameterized.class)
public class FINc_InvoiceToPayment extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /* Data for [FINc010] Create Sales Invoice. */
  /** The Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /** The data to verify the creation of the Sales Invoice header. */
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  /** The Sales Invoice line data. */
  SalesInvoiceLinesData salesInvoiceLineData;
  /** The data to verify the creation of the Sales Invoice line. */
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  /** The data to verify the completion of the sales invoice. */
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;

  /* Data for [FINc020] Create Payment In. */
  /** The Payment In header data. */
  PaymentInHeaderData paymentInHeaderData;
  /** The data to verify the creation of they Payment In header. */
  PaymentInHeaderData paymentInHeaderVerificationData;
  /** The data to verify the payed Payment In header. */
  PaymentInHeaderData payedPaymentInHeaderVerificationData;
  /** The data to verify the Payment In line. */
  PaymentInLinesData paymentInLinesVerificationData;

  /**
   * Class constructor.
   *
   * @param salesInvoiceHeaderData
   *          The Sales Invoice header data.
   * @param salesInvoiceHeaderVerificationData
   *          The data to verify the creation of the Sales Invoice header data.
   * @param salesInvoiceLineData
   *          The Sales Invoice line data.
   * @param salesInvoiceLineVerificationData
   *          The data to verify the creation of the Sales Invoice line.
   * @param completedSalesInvoiceHeaderVerificationData
   *          The data to verify the completion of the sales invoice.
   * @param paymentInHeaderData
   *          The Payment In header data.
   * @param paymentInHeaderVerificationData
   *          The data to verify the creation of they Payment In header.
   * @param payedPaymentInHeaderVerificationData
   *          The data to verify the payed Payment In header.
   * @param paymentInLinesVerificationData
   *          The data to verify the Payment In line.
   */
  public FINc_InvoiceToPayment(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      PaymentInHeaderData paymentInHeaderData, PaymentInHeaderData paymentInHeaderVerificationData,
      PaymentInHeaderData payedPaymentInHeaderVerificationData,
      PaymentInLinesData paymentInLinesVerificationData) {
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.payedPaymentInHeaderVerificationData = payedPaymentInHeaderVerificationData;
    this.paymentInLinesVerificationData = paymentInLinesVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        /* Parameters for [SALb010] Create Sales Invoice. */
        new SalesInvoiceHeaderData.Builder().transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .invoiceDate(OBDate.CURRENT_DATE)
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("FGA")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("13.13")
            .build(),
        new SalesInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("26.26")
            .build(),
        new SalesInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("27.18")
            .dueAmount("0.00")
            .paymentComplete(false)
            .documentStatus("Completed")
            .summedLineAmount("26.26")
            .grandTotalAmount("27.18")
            .build(),

        /* Parameters for [FINc020] Create Payment In. */
        new PaymentInHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .amount("27.18")
            .build(),
        new PaymentInHeaderData.Builder().documentType("AR Receipt")
            .account("Spain Bank - EUR")
            .paymentMethod("1 (Spain)")
            .currency("EUR")
            .status("Awaiting Payment")
            .build(),
        new PaymentInHeaderData.Builder().status("Payment Received").build(),
        new PaymentInLinesData.Builder().invoiceAmount("27.18")
            .expected("27.18")
            .received("27.18")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test to add a payment to a sales invoice.
   */
  @Test
  public void salesInvoicePaymentShouldBeAdded() {
    logger.info("** Start of test case [FINc010] Create Sales Invoice. **");
    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.create(salesInvoiceLineData);
    salesInvoiceLines.assertSaved();
    salesInvoiceLines.assertData(salesInvoiceLineVerificationData);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);
    String salesInvoiceDocumentNumber = (String) salesInvoice.getData("documentNo");
    logger.info("** End of test case [FINc010] Create Sales Invoice. **");

    logger.info("** Start of test case [FINc020] Create Payment In.");
    final PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertSaved();
    paymentIn.assertData(paymentInHeaderVerificationData);
    paymentIn.assertAddPaymentInData("Customer A", "27.18", "Invoices");
    paymentIn.addDetails("Invoices", salesInvoiceDocumentNumber, "Process Received Payment(s)");
    paymentIn.assertPaymentCreatedSuccessfully();
    paymentIn.assertData(payedPaymentInHeaderVerificationData);
    final PaymentIn.Lines paymentInLines = paymentIn.new Lines(mainPage);
    paymentInLines.assertCount(1);
    paymentInLines.assertData(paymentInLinesVerificationData);
    logger.info("** End of test case [FINc020] Create Payment In.");
  }
}
