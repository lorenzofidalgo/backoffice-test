/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2019 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.masterdata;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.common.TestFiles;
import com.openbravo.test.integration.erp.common.TimeoutConstants;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.applicationdictionary.module.ModuleData;
import com.openbravo.test.integration.erp.data.financial.accounting.setup.fiscalcalendar.CalendarData;
import com.openbravo.test.integration.erp.data.financial.accounting.setup.fiscalcalendar.PeriodData;
import com.openbravo.test.integration.erp.data.financial.accounting.setup.fiscalcalendar.YearData;
import com.openbravo.test.integration.erp.data.financial.accounting.transactions.opencloseperiodcontrol.OpenClosePeriodControlData;
import com.openbravo.test.integration.erp.data.generalsetup.InitialSetupData;
import com.openbravo.test.integration.erp.data.generalsetup.ModuleReferenceData;
import com.openbravo.test.integration.erp.data.generalsetup.client.initialclientsetup.InitialClientSetupData;
import com.openbravo.test.integration.erp.data.generalsetup.enterprise.enterprisemodulemanagement.EnterpriseModuleManagementData;
import com.openbravo.test.integration.erp.data.generalsetup.enterprise.initialorganizationsetup.InitialOrganizationSetupData;
import com.openbravo.test.integration.erp.data.generalsetup.enterprise.organization.OrganizationData;
import com.openbravo.test.integration.erp.data.generalsetup.security.user.UserData;
import com.openbravo.test.integration.erp.data.selectors.LocationSelectorData;
import com.openbravo.test.integration.erp.gui.LoginPage;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.applicationdictionary.module.Module;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.setup.fiscalcalendar.FiscalCalendar;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.transactions.opencloseperiodcontrol.OpenClosePeriodControl;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.modulemanagement.ModuleManagement;
import com.openbravo.test.integration.erp.testscripts.generalsetup.client.initialclientsetup.InitialClientSetup;
import com.openbravo.test.integration.erp.testscripts.generalsetup.enterprise.Organization;
import com.openbravo.test.integration.erp.testscripts.generalsetup.enterprise.enterprisemodulemanagement.EnterpriseModuleManagement;
import com.openbravo.test.integration.erp.testscripts.generalsetup.enterprise.initialorganizationsetup.InitialOrganizationSetup;
import com.openbravo.test.integration.erp.testscripts.generalsetup.enterprise.organization.OrganizationScript;
import com.openbravo.test.integration.erp.testscripts.generalsetup.security.user.User;
import com.openbravo.test.integration.erp.testscripts.generalsetup.security.user.UserClassicScript;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.ConfigurationProperties;
import com.openbravo.test.integration.util.OBDate;

/**
 * Execute the Setup Client and Organization flow of the smoke test suite.
 *
 * @author Pablo Lujan
 */
@RunWith(Parameterized.class)
public class ADMc_SetupClientAndOrganization extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  /** Initial client setup data. */
  private InitialClientSetupData initialClientSetupData;
  /** Data of the module to install. */
  private ModuleReferenceData spanishPackModuleData;
  /** Initial organization setup data. */
  private InitialOrganizationSetupData initialOrganizationSetupData;
  /** Module application data. */
  private EnterpriseModuleManagementData enterpriseModuleManagementData;
  /** Fiscal calendar data. */
  private CalendarData calendarData;
  /** Years data. */
  private YearData[] yearsData;
  private OrganizationData editedOrganizationData;

  /**
   * Class constructor.
   *
   *
   * @param initialClientSetupData
   *          Initial client setup data.
   * @param spanishPackModuleData
   *          Data of the module to install.
   * @param initialOrganizationSetupData
   *          Initial organization setup data.
   * @param enterpriseModuleManagementData
   *          Module application data.
   * @param calendarData
   *          Fiscal calendar data.
   * @param yearsData
   *          Years data.
   * @param openClosePeriodsControlData
   *          Open/Close period control data.
   */
  public ADMc_SetupClientAndOrganization(InitialClientSetupData initialClientSetupData,
      ModuleReferenceData spanishPackModuleData,
      InitialOrganizationSetupData initialOrganizationSetupData,
      EnterpriseModuleManagementData enterpriseModuleManagementData, CalendarData calendarData,
      YearData[] yearsData, OpenClosePeriodControlData[] openClosePeriodsControlData,
      OrganizationData editedOrganizationData) {
    this.initialClientSetupData = initialClientSetupData;
    this.spanishPackModuleData = spanishPackModuleData;
    this.initialOrganizationSetupData = initialOrganizationSetupData;
    this.enterpriseModuleManagementData = enterpriseModuleManagementData;
    this.calendarData = calendarData;
    this.yearsData = yearsData;
    this.editedOrganizationData = editedOrganizationData;

    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<Object[]> setupClientAndOrganizationValues() throws IOException {
    return Arrays.asList(new Object[][] { {
        // Initial client setup data.
        new InitialClientSetupData.Builder()
            .requiredFields("SampleClient", "SampleClientAdmin", "SampleClientAdmin",
                "SampleClientAdmin")
            .initialSetupData(new InitialSetupData.Builder().currency("EUR")
                .includeAccounting(false)
                .addReferenceData(new ModuleReferenceData.Builder()
                    .description("Pre-configured collection of standard ERP roles")
                    .name("Standard Roles")
                    .language("English (USA)")
                    .build())
                .addReferenceData(new ModuleReferenceData.Builder()
                    .description("Standard document types for orders, invoices, etc. and settings")
                    .name("Core")
                    .language("English (USA)")
                    .build())
                .addReferenceData(new ModuleReferenceData.Builder()
                    .description(
                        "Document types and default algorithm for bank statement auto matching")
                    .name("Advanced Payables")
                    .build())
                .build())
            .build(),
        // Modules installation data.
        new ModuleReferenceData.Builder().name("Localization pack Spain (España)").build(),
        // Initial organization setup data.
        new InitialOrganizationSetupData.Builder()
            .requiredFields("Spain", "SampleClientUser", "SampleClientUser", "SampleClientUser",
                "Legal with accounting", "*")
            .location(new LocationSelectorData.Builder().firstLine("P.I. Landaben")
                .secondLine("Calle J. Edif. SLAN")
                .postalCode("31012")
                .city("Pamplona")
                .country("Spain")
                .region("NAVARRA")
                .build())
            .initialSetupData(new InitialSetupData.Builder().includeAccounting(true)
                .currency("EUR")
                .addReferenceData(new ModuleReferenceData.Builder()
                    .description("Plan general contable 2007 (PYMEs)")
                    .name("Chart of accounts: PGC 2007 PYMEs")
                    .language("Spanish (Spain)")
                    .build())
                .dimensionBusinessPartner(true)
                .dimensionProduct(true)
                .dimensionProject(true)
                .build())
            .build(),
        // Modules application data.
        // Note: version is hardcoded on purpose. The idea is to force the test to fail in case
        // a new release is installed.
        // If version is not checked, unexpected erros might raise later in the flow and finding
        // it was due to the module update could be a hard task.
        new EnterpriseModuleManagementData.Builder()
            .addReferenceData(new ModuleReferenceData.Builder().name("Alerts: Accounting")
                .version("3.0.401")
                .language("Spanish (Spain)")
                .build())
            .build(),
        // Fiscal calendar data.
        new CalendarData.Builder().name("Sample client fiscal calendar").build(),
        new YearData[] { new YearData.Builder().fiscalYear(OBDate.getCurrentYear()).build(),
            new YearData.Builder().fiscalYear(OBDate.getYearBefore()).build() },
        // Open Close Periods data.
        new OpenClosePeriodControlData[] {
            new OpenClosePeriodControlData.Builder().organization("Spain")
                .year(OBDate.getCurrentYear())
                .periodNo(OBDate.getPeriod(12, Integer.parseInt(OBDate.getCurrentYear())))
                .periodAction("Open Period")
                .build(),
            new OpenClosePeriodControlData.Builder().organization("Spain")
                .year(OBDate.getYearBefore())
                .periodNo(OBDate.getPeriod(12, Integer.parseInt(OBDate.getYearBefore())))
                .periodAction("Open Period")
                .build() },
        new OrganizationData.Builder().allowPeriodControl(true)
            .calendar("Sample client fiscal calendar")
            .build()

        } });

  }

  /**
   * Test the setup client and organization flow.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void clientAndOrganizationShouldBeSetUp() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [ADMc010] Initial Client Setup. **");
    InitialClientSetup.setupInitialClient(mainPage, initialClientSetupData,
        FileUtils.readFileToString(
            new File(TestFiles.getRelativeFilePath("InitialClientSetupLog.txt")), (String) null));
    logger.info("** End of test case [ADMc010] Initial Client Setup. **");

    logger.info("** Start of test case [ADMc020] Install Spanish Pack. **");
    ModuleManagement.Settings.setStatus(mainPage,
        ConfigurationProperties.INSTANCE.getMaturityStatus(),
        ConfigurationProperties.INSTANCE.getMaturityStatus());
    LoginPage loginPage = new LoginPage();
    if (ModuleManagement.AddModules.installModuleFromCentralRepository(mainPage,
        spanishPackModuleData)) {
      ModuleManagement.InstalledModules.rebuildSystemAndRestartServletContainer(mainPage);
      loginPage.waitForFieldsToAppear(TimeoutConstants.RESTART_SERVLET_CONTAINER_TIMEOUT);
    } else {
      mainPage.quit();
    }

    loginPage = new LoginPage();
    loginPage.open();
    loginPage.logIn(logInData);
    mainPage = new MainPage();
    mainPage.assertLogin(logInData.getUserName());

    if (mainPage.isOnNewLayout()) {
      Module module = new Module(mainPage).open();
      module.select(new ModuleData.Builder().name(spanishPackModuleData.getName()).build());
      module.assertData(new ModuleData.Builder().inDevelopment(false).build());
    }

    mainPage.quit();
    logger.info("** End of test case [ADMc020] Install Spanish Pack. **");

    loginPage = new LoginPage();
    final LogInData clientAdministratorLogInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getClientAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getClientAdministratorPassword())
        .build();
    loginPage.logIn(clientAdministratorLogInData);
    mainPage = new MainPage();
    mainPage.assertLogin(clientAdministratorLogInData.getUserName());

    logger.info("** Start of test case [ADMc030] Initial Organization Setup. **");
    // TODO L0: Remove static sleeps once it is stable
    Sleep.trySleep();
    InitialOrganizationSetup.setupInitialOrganization(mainPage, initialOrganizationSetupData);
    OrganizationData organizationVerificationData = new OrganizationData.Builder()
        .name(initialOrganizationSetupData.getOrganization())
        .build();
    if (mainPage.isOnNewLayout()) {
      Organization organization = new Organization(mainPage).open();
      organization.select(organizationVerificationData);
    } else {
      OrganizationScript.OrganizationTab.select(mainPage, organizationVerificationData);
    }
    UserData userData = new UserData.Builder()
        .name(initialOrganizationSetupData.getOrganizationUserName())
        .build();
    if (mainPage.isOnNewLayout()) {
      User user = new User(mainPage).open();
      user.select(userData);
    } else {
      UserClassicScript.select(mainPage, userData);
    }
    // TODO Verify that the accounting schema for that COA has been created for that organization
    logger.info("** End of test case [ADMc030] Initial Organization Setup. **");

    logger.info("** Start of test case [ADMc040] Apply Payment Flow Module. **");
    EnterpriseModuleManagement.applyModules(mainPage, enterpriseModuleManagementData);
    logger.info("** End of test case [ADMc040] Apply Payment Flow Module. **");

    logger.info("** Start of test case [ADMc050] Create Fiscal Calendar Periods. **");
    FiscalCalendar.CalendarTab.create(mainPage, calendarData);
    for (final YearData yearData : yearsData) {
      FiscalCalendar.CalendarTab.YearTab.create(mainPage, yearData);
      // TODO L00: Remove the following static sleep once it is stable
      Sleep.trySleep(6000);
      FiscalCalendar.CalendarTab.YearTab.createPeriods(mainPage);
      FiscalCalendar.CalendarTab.YearTab.PeriodTab.verifyCount(mainPage, 12);
      for (int index = 1; index <= 12; index++) {
        final String periodName = OBDate.getPeriod(index,
            Integer.parseInt(yearData.getDataField("fiscalYear").toString()));
        FiscalCalendar.CalendarTab.YearTab.PeriodTab.select(mainPage,
            new PeriodData.Builder().name(periodName).build());
      }
    }
    logger.info("** End of test case [ADMc050] Create Fiscal Calendar Periods. **");

    logger.info("** Start of test case [ADMc060] Edit Organization. **");
    if (mainPage.isOnNewLayout()) {
      Organization organization = new Organization(mainPage).open();
      organization.select(organizationVerificationData);
      organization.edit(editedOrganizationData);
      organization.setAsReady();
      organization.assertProcessCompletedSuccessfully2();
    } else {
      OrganizationScript.OrganizationTab.select(mainPage, organizationVerificationData);
      OrganizationScript.OrganizationTab.edit(mainPage, editedOrganizationData);
      OrganizationScript.OrganizationTab.setAsReady(mainPage);
    }
    logger.info("** End of test case [ADMc060] Edit Organization.");

    logger.info("** Start of test case [ADMc070] Open Period Control. **");
    // for (final OpenClosePeriodControlData periodData : openClosePeriodsControlData) {
    // OpenClosePeriodControl.OpenClosePeriodControlTab.create(mainPage, periodData);
    OpenClosePeriodControl.OpenClosePeriodControlTab.openAllPeriods(mainPage);
    // }
    // TODO Verify that all the records have the "Period Status" Open.
    // OrganizationScript.OrganizationTab.select(mainPage, organizationVerificationData);
    logger.info("** End of test case [ADMc070] Open Period Control. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
    OpenbravoERPTest.seleniumStarted = false;
    SeleniumSingleton.INSTANCE.quit();
  }
}
