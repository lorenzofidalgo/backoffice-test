/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.masterdata;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.OrgAccessData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.RoleData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.UserAssignmentData;
import com.openbravo.test.integration.erp.data.generalsetup.security.user.GrantAccessPopUpData;
import com.openbravo.test.integration.erp.data.generalsetup.security.user.UserData;
import com.openbravo.test.integration.erp.data.generalsetup.security.user.UserRolesData;
import com.openbravo.test.integration.erp.data.selectors.OrganizationSelectorData;
import com.openbravo.test.integration.erp.gui.LoginPage;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.generalsetup.security.role.Role;
import com.openbravo.test.integration.erp.testscripts.generalsetup.security.user.User;
import com.openbravo.test.integration.erp.testscripts.generalsetup.security.user.User.UserRoles;
import com.openbravo.test.integration.erp.testscripts.generalsetup.security.user.UserClassicScript;
import com.openbravo.test.integration.erp.testscripts.generalsetup.security.user.UserRolesClassicScript;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.ConfigurationProperties;

//import com.openbravo.test.integration.erp.testscripts.generalsetup.security.user.CreateUser;

/**
 * Execute the Setup User and Role flow of the smoke test suite.
 *
 * @author Pablo Lujan
 */
@RunWith(Parameterized.class)
public class ADMd_SetupUserAndRole extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  private UserData userAData;
  private UserRolesData assignUserARoleData;
  private UserData userBData;
  private RoleData roleSalesData;
  private OrgAccessData[] orgAccessesData;
  private UserAssignmentData userAssignmentData;

  /**
   * Class constructor.
   */
  public ADMd_SetupUserAndRole(UserData userAData, UserRolesData assignUserARoleData,
      UserData userBData, RoleData roleSalesData, GrantAccessPopUpData grantAccessPopUpData,
      OrgAccessData[] orgAccessesData, UserAssignmentData userAssignmentData) {
    this.userAData = userAData;
    this.assignUserARoleData = assignUserARoleData;
    this.userBData = userBData;
    this.roleSalesData = roleSalesData;
    this.orgAccessesData = orgAccessesData;
    this.userAssignmentData = userAssignmentData;
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getClientAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getClientAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<Object[]> setupUserAndRoleValues() throws IOException {
    return Arrays.asList(new Object[][] { {
        // Create userA data.
        // new
        new UserData.Builder().organization("*")
            .name("userA")
            .username("userA")
            .password("userA")
            .build(),

        // Create UserRoles data for SampleClient to userA.
        new UserRolesData.Builder().role("SampleClient Admin").build(),

        // Create userB data.
        // new
        // TODO: Removes username field from filling to workaround issue 16730
        // UserData.Builder().organization("*").name("userB").username("userB").password("userB")
        // .build(),
        // Uncomment above lines and delete next line when issue were fixed
        new UserData.Builder().organization("*")
            .name("userB")
            .username("userB")
            .password("userB")
            .build(),

        // Create Sales Role data.
        new RoleData.Builder().name("Sales Admin")
            .userLevel("Client+Organization")
            .manual(true)
            .build(),
        new GrantAccessPopUpData.Builder().module("Sales Management").access("All").build(),

        // Organization Access data.
        new OrgAccessData[] {
            new OrgAccessData.Builder()
                .organization(new OrganizationSelectorData.Builder().name("*").build())
                .build(),
            new OrgAccessData.Builder()
                .organization(new OrganizationSelectorData.Builder().name("Spain").build())
                .build() },

        new UserAssignmentData.Builder().userContact("userB").build()

        } });
  }

  /**
   * Test the setup client and organization flow.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void userAndRoleShouldBeSetUp() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [ADMd010] Create User. **");
    if (mainPage.isOnNewLayout()) {
      User user = new User(mainPage).open();
      user.create(userAData);
      user.assertSaved();
      UserRoles userRoles = user.new UserRoles(mainPage);
      userRoles.create(assignUserARoleData);
      userRoles.assertSaved();
    } else {
      UserClassicScript.create(mainPage, userAData);
      UserRolesClassicScript.create(mainPage, assignUserARoleData);
    }
    mainPage.quit();
    LoginPage loginPage = new LoginPage();
    loginPage.waitForFieldsToAppear();
    final LogInData userALogInData = new LogInData.Builder()
        .userName((String) userAData.getDataField("name"))
        .password((String) userAData.getDataField("password"))
        .build();
    loginPage.logIn(userALogInData);

    mainPage = new MainPage();
    mainPage.assertLogin(userALogInData.getUserName());

    // TODO verify that user has access to all windows.

    mainPage.quit();
    logger.info("** End of test case [ADMd010] Create User. **");

    logger.info("** Start of test case [ADMd020] Create Role. **");
    loginPage.open();
    loginPage.logIn(logInData);

    mainPage = new MainPage();
    mainPage.assertLogin(logInData.getUserName());

    if (mainPage.isOnNewLayout()) {
      User user = new User(mainPage).open();
      user.create(userBData);
      user.assertSaved();
    } else {
      UserClassicScript.create(mainPage, userBData);
    }
    Role.RoleTab.create(mainPage, roleSalesData);
    // TODO
    // Role.RoleTab.grantAccess(mainPage, grantAccessPopUpData);
    for (final OrgAccessData orgAccessData : orgAccessesData) {
      Role.RoleTab.OrgAccess.create(mainPage, orgAccessData);
    }
    Role.RoleTab.UserAssignment.create(mainPage, userAssignmentData);
    mainPage.quit();

    final LogInData userBLogInData = new LogInData.Builder()
        .userName((String) userBData.getDataField("name"))
        .password((String) userBData.getDataField("password"))
        .build();
    loginPage.logIn(userBLogInData);
    mainPage = new MainPage();
    mainPage.assertLogin(userBLogInData.getUserName());

    // TODO Verify that only sales management menu is visible.
    logger.info("** End of test case [ADMd020] Create Role. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
    OpenbravoERPTest.seleniumStarted = false;
    SeleniumSingleton.INSTANCE.quit();
  }
}
