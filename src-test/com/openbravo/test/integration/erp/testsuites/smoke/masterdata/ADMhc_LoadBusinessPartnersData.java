/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.masterdata;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.common.TestFiles;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.setup.paymentmethod.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountingConfigurationData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.BusinessPartnerData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.CustomerData;
import com.openbravo.test.integration.erp.modules.initialdataload.data.masterdata.process.importwindow.ImportData;
import com.openbravo.test.integration.erp.modules.initialdataload.testscripts.masterdata.process.importwindow.Import;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.financialaccount.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.paymentmethod.PaymentMethod;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Execute the Setup Initial Data Load flow of the smoke test suite.
 *
 * @author Pablo Lujan
 */
@RunWith(Parameterized.class)
public class ADMhc_LoadBusinessPartnersData extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  PaymentMethodData[] paymentMethodsData;

  ImportData importFinancialAccountData;

  AccountData financialAccountData;
  AccountingConfigurationData accountingData;

  ImportData importBusinessPartnerData;

  BusinessPartnerData businessPartnerData;
  CustomerData customerData;

  /**
   * Class constructor.
   */
  public ADMhc_LoadBusinessPartnersData(PaymentMethodData[] paymentMethodsData,
      ImportData importFinancialAccountData, AccountData financialAccountData,
      AccountingConfigurationData accountingData, ImportData importBusinessPartnerData,
      BusinessPartnerData businessPartnerData, CustomerData customerData) {

    this.paymentMethodsData = paymentMethodsData;
    this.importFinancialAccountData = importFinancialAccountData;
    this.financialAccountData = financialAccountData;
    this.accountingData = accountingData;
    this.importBusinessPartnerData = importBusinessPartnerData;
    this.businessPartnerData = businessPartnerData;
    this.customerData = customerData;

    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getClientAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getClientAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<Object[]> setupProductValues() throws IOException {
    return Arrays.asList(new Object[][] { {

        // Payment methods
        new PaymentMethodData[] {
            new PaymentMethodData.Builder().organization("Spain")
                .name("1 (Spain)")
                .payinAllow(true)
                .automaticReceipt(false)
                .automaticDeposit(false)
                .payinExecutionType("Manual")
                .iNUponClearingUse("Cleared Payment Account")
                .payoutAllow(true)
                .automaticPayment(false)
                .automaticWithdrawn(false)
                .payoutExecutionType("Manual")
                .oUTUponClearingUse("Cleared Payment Account")
                .build(),
            new PaymentMethodData.Builder().organization("Spain")
                .name("4 (Spain)")
                .payinAllow(true)
                .automaticReceipt(true)
                .automaticDeposit(false)
                .payinExecutionType("Manual")
                .uponDepositUse("Deposited Payment Account")
                .iNUponClearingUse("Cleared Payment Account")
                .payoutAllow(true)
                .automaticPayment(true)
                .automaticWithdrawn(false)
                .payoutExecutionType("Manual")
                .uponWithdrawalUse("Withdrawn Payment Account")
                .oUTUponClearingUse("Cleared Payment Account")
                .build(),
            new PaymentMethodData.Builder().organization("Spain")
                .name("6 (Spain)")
                .payinAllow(true)
                .automaticReceipt(true)
                .automaticDeposit(true)
                .payinExecutionType("Manual")
                .uponReceiptUse("In Transit Payment Account")
                .uponDepositUse("Deposited Payment Account")
                .iNUponClearingUse("Cleared Payment Account")
                .payoutAllow(true)
                .automaticPayment(true)
                .automaticWithdrawn(true)
                .payoutExecutionType("Manual")
                .uponPaymentUse("In Transit Payment Account")
                .uponWithdrawalUse("Withdrawn Payment Account")
                .oUTUponClearingUse("Cleared Payment Account")
                .build(),
            new PaymentMethodData.Builder().organization("Spain")
                .name("2.1 (Spain)")
                .payinAllow(true)
                .automaticReceipt(false)
                .automaticDeposit(false)
                .payinExecutionType("Automatic")
                .payinExecutionProcess("Simple Execution Process")
                .payinDeferred(true)
                .iNUponClearingUse("Cleared Payment Account")
                .payoutAllow(true)
                .automaticPayment(false)
                .automaticWithdrawn(false)
                .payoutExecutionType("Automatic")
                .payoutExecutionProcess("Simple Execution Process")
                .payoutDeferred(true)
                .oUTUponClearingUse("Cleared Payment Account")
                .build(),
            new PaymentMethodData.Builder().organization("Spain")
                .name("5.1 (Spain)")
                .payinAllow(true)
                .automaticReceipt(true)
                .automaticDeposit(false)
                .payinExecutionType("Automatic")
                .payinExecutionProcess("Simple Execution Process")
                .payinDeferred(false)
                .iNUponClearingUse("Cleared Payment Account")
                .payoutAllow(true)
                .automaticPayment(true)
                .automaticWithdrawn(false)
                .payoutExecutionType("Automatic")
                .payoutExecutionProcess("Simple Execution Process")
                .payoutDeferred(false)
                .oUTUponClearingUse("Cleared Payment Account")
                .build() },

        // Import Financial Account
        new ImportData.Builder().file(TestFiles.getFullFilePath("FinancialAccount.csv"))
            .entity("Financial Account")
            .build(),
        new AccountData.Builder().name("Spain Testing Bank").build(),
        new AccountingConfigurationData.Builder()./*
                                                   * inTransitPaymentAccountIN("%40100%")
                                                   * .depositAccount
                                                   * ("55500").clearedPaymentAccount("57200"
                                                   * ).fINOutIntransitAcct("%43100%")
                                                   * .withdrawalAccount
                                                   * ("55500").clearedPaymentAccountOUT("57200").
                                                   */build(),

        // Import Business Partner
        new ImportData.Builder().file(TestFiles.getFullFilePath("BusinessPartners.csv"))
            .entity("Business Partner")
            .build(),

        // Edit Business Partner
        new BusinessPartnerData.Builder().name("Customer A").build(),
        new CustomerData.Builder().sOBPTaxCategory("VAT 3%+CHARGE 0.5%").build()

        } });
  }

  /**
   * Test the setup client and organization flow.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void businessPartnersShouldBeLoaded() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [ADMhc010] Create Payment methods **");
    for (final PaymentMethodData paymentMethodData : paymentMethodsData) {
      PaymentMethod.PaymentMethodTab.create(mainPage, paymentMethodData);
    }
    logger.info("** End of test case [ADMhc010] Create Payment methods **");

    logger.info("** Start of test case [ADMhc020] Import Adv Financial Accounts **");
    Import.validateFile(mainPage, importFinancialAccountData);
    // TODO add the values to the parameters.
    Import.verifyValidationProcessCompletedSuccessfully(mainPage, 2, 2, 0);
    Import.processFile(mainPage, importFinancialAccountData);
    Import.verifyImportProcessCompletedSuccessfully(mainPage, 2, 2, 0);
    logger.info("** End of test case [ADMhc020] Import Adv Financial Accounts **");

    logger.info("** Start of test case [ADMhc030] Assign payment methods **");
    FinancialAccount.AccountTab.select(mainPage, financialAccountData);
    for (final PaymentMethodData paymentMethodData : paymentMethodsData) {
      FinancialAccount.PaymentMethod.create(mainPage,
          new com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData.Builder()
              .paymentMethod(paymentMethodData.getDataField("name").toString())
              .build());
    }
    // FinancialAccount.Accounting.edit(mainPage, accountingData);
    logger.info("** End of test case [ADMhc030] Assign payment methods **");

    logger.info("** Start of test case [ADMhc040] Import Business Partners **");
    Import.validateFile(mainPage, importBusinessPartnerData);
    // TODO add the values to the parameters.
    Import.verifyValidationProcessCompletedSuccessfully(mainPage, 9, 9, 0);
    Import.processFile(mainPage, importBusinessPartnerData);
    Import.verifyImportProcessCompletedSuccessfully(mainPage, 9, 9, 0);
    logger.info("** End of test case [ADMhc040] Import Business Partners **");

    // logger.info("** Start of test case [ADMhc050] Edit Business Partner Tax Category **");
    // BusinessPartner.BusinessPartnerTab.select(mainPage, businessPartnerData);
    // BusinessPartner.Customer.edit(mainPage, customerData);
    // logger.info("** End of test case [ADMhc050] Edit Business Partner Tax Category **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
    OpenbravoERPTest.seleniumStarted = false;
    SeleniumSingleton.INSTANCE.quit();
  }
}
