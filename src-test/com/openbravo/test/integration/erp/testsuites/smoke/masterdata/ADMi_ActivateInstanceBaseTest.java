/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.openbravo.test.integration.erp.testsuites.smoke.masterdata;

import static com.openbravo.test.integration.erp.data.generalsetup.application.instanceactivation.Purposes.PRODUCTION;
import static com.openbravo.test.integration.erp.data.generalsetup.application.instanceactivation.Purposes.TESTING;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.common.TestFiles;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.generalsetup.application.instanceactivation.ActivateOnlineData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.intanceactivation.InstanceActivation;

public abstract class ADMi_ActivateInstanceBaseTest extends OpenbravoERPTest {

  private static Logger logger = LogManager.getLogger();

  protected void changeRoleToSystemAdmin() {
    mainPage.getNavigationBar()
        .changeProfile(new ProfileData.Builder().role("System Administrator - System")
            .client("System")
            .organization("*")
            .isDefault(true)
            .build());
    mainPage.assertLogin(logInData.getUserName());
  }

  protected void setupInstanceAsProduction() {
    ActivateOnlineData activateOnlineData = new ActivateOnlineData.Builder()
        .requiredFields(PRODUCTION.purpose(), readLicenseKeyFile())
        .build();

    if (!InstanceActivation.activateInstanceOnline(mainPage, activateOnlineData)) {
      InstanceActivation.reactivateInstanceOnline(mainPage, activateOnlineData);
    }
  }

  protected void setupInstanceAsTesting() {
    ActivateOnlineData activateOnlineData = new ActivateOnlineData.Builder()
        .requiredFields(TESTING.purpose(), readLicenseKeyFile())
        .build();

    if (!InstanceActivation.activateInstanceOnline(mainPage, activateOnlineData)) {
      InstanceActivation.reactivateInstanceOnline(mainPage, activateOnlineData);
    }
  }

  private String readLicenseKeyFile() {
    try {
      return FileUtils.readFileToString(new File(TestFiles.getRelativeFilePath("SystemKey.txt")),
          (String) null);
    } catch (IOException exception) {
      logger.error("Cannot open license file", exception);
      return "";
    }
  }
}
