/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.procurement;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestWithSeleniumClosure;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Test Logging in and Logging out processes using the Navigation Bar module.
 *
 * @author Lorenzo Fidalgo
 */
@RunWith(Parameterized.class)
public class CLO_LoggingInAndOutClosure extends OpenbravoERPTestWithSeleniumClosure {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param logInData
   *          Data to log in.
   */
  public CLO_LoggingInAndOutClosure(LogInData logInData) {
    this.logInData = logInData;
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<LogInData[]> loggingValues() {
    return Arrays.asList(new LogInData[][] { { new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build() } });
  }

  /**
   * Test the log in and log out.
   */
  @Test
  public void userShouldLogInAndLogOut() {
    logger.info("** Start of test case [CLO] Logging in and out, with Selenium closure. **");
    // TODO Close the instance purpose pop-up.
    // TODO If Heartbeat pop-up is opened close it.
    mainPage.getNavigationBar()
        .changeProfile(new ProfileData.Builder().role("System Administrator - System")
            .client("System")
            .organization("*")
            .isDefault(true)
            .build());
    // Workaround to close the popup.
    mainPage.assertLogin(logInData.getUserName());
    logger.info("** End of test case [CLO] Logging in and out, with Selenium closure. **");
  }
}
