/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Pablo Lujan <pablo.lujan@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.procurement;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.generalsetup.processscheduling.processrequest.ProcessRequestData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.LocatorSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseinvoice.PurchaseInvoiceWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.sessionpreferences.SessionPreferences;
import com.openbravo.test.integration.erp.testscripts.generalsetup.processscheduling.processrequest.ProcessRequest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Order to invoice.
 *
 * @author Pablo Luján
 */
@RunWith(Parameterized.class)
public class PROa_OrderToInvoice extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /* Data for [PROa010] Create purchase order. */
  /** The purchase order header data. */
  PurchaseOrderHeaderData purchaseOrderHeaderData;
  /** The data to verify the purchase order header creation. */
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  /** The purchase order line data. */
  PurchaseOrderLinesData purchaseOrderLinesData;
  /** The data to verify the purchase order line creation. */
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  /** The booked purchase order verification data. */
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;
  /** The data to verify the payment out plan data. */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData purchaseOrderPaymentOutPlanData;

  /* Data for [PROa020] Create Goods Receipt. */
  /** The goods receipt header data. */
  GoodsReceiptHeaderData goodsReceiptHeaderData;
  /** The data to verify the goods receipt header creation. */
  GoodsReceiptHeaderData goodsReceiptHeaderVerificationData;
  /** The data to verify the goods receipt lines creation. */
  GoodsReceiptLinesData goodsReceiptLinesVerificationData;
  /** The data to verify the goods receipt header after completion. */
  GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData;

  /* Data for [PROa030] Create Purchase Invoice. */
  /** The purchase invoice header data. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  /** The data to verify the purchase invoice header creation. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  /** The data to verify the purchase invoice header after completion. */
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  /** The data to verify the payment out plan data. */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData purchaseInvoicePaymentOutPlanData;
  /**
   * Expected journal entry lines. Is an array of arrays. Each line has account number, name, debit
   * and credit.
   */
  private String[][] journalEntryLines;

  /**
   * Class constructor.
   *
   * @param purchaseOrderHeaderData
   *          The purchase order header data.
   * @param purchaseOrderHeaderVerficationData
   *          The data to verify the purchase order header creation.
   * @param purchaseOrderLinesData
   *          The purchase order line data.
   * @param purchaseOrderLinesVerificationData
   *          The data to verify the purchase order line creation.
   * @param bookedPurchaseOrderHeaderVerificationData
   *          The booked purchase order verification data.
   * @param purchaseOrderPaymentOutPlanData
   *          The data to verify the payment out plan.
   * @param goodsReceiptHeaderData
   *          The goods receipt header data.
   * @param goodsReceiptHeaderVerificationData
   *          The data to verify the goods receipt header lines creation.
   * @param goodsReceiptLinesVerificationData
   *          The data to verify the goods receipt lines creation.
   * @param completedGoodsReceiptHeaderVerificationData
   *          The data to verify the goods receipt header after verification.
   * @param purchaseInvoiceHeaderData
   *          The purchase invoice header data.
   * @param purchaseInvoiceHeaderVerificationData
   *          The data to verify the purchase invoice header creation.
   * @param completedPurchaseInvoiceHeaderVerificationData
   *          The data to verify the purchase invoice header after completion.
   * @param purchaseInvoicePaymentOutPlanData
   *          The data to verify the payment out plan data.
   * @param journalEntryLines
   *          Expected journal entry lines. Is an array of arrays. Each line has account number,
   *          name, debit and credit.
   */
  public PROa_OrderToInvoice(PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData purchaseOrderPaymentOutPlanData,
      GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptHeaderData goodsReceiptHeaderVerificationData,
      GoodsReceiptLinesData goodsReceiptLinesVerificationData,
      GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData purchaseInvoicePaymentOutPlanData,
      String[][] journalEntryLines) {
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.purchaseOrderPaymentOutPlanData = purchaseOrderPaymentOutPlanData;
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptHeaderVerificationData = goodsReceiptHeaderVerificationData;
    this.goodsReceiptLinesVerificationData = goodsReceiptLinesVerificationData;
    this.completedGoodsReceiptHeaderVerificationData = completedGoodsReceiptHeaderVerificationData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.purchaseInvoicePaymentOutPlanData = purchaseInvoicePaymentOutPlanData;
    this.journalEntryLines = journalEntryLines;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToInvoiceValues() {

    return Arrays.asList(new Object[][] { {
        // Parameters for [PROa010] Create purchase order.
        new PurchaseOrderHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("Spain warehouse")
            .priceList("Purchase")
            .paymentMethod("1 (Spain)")
            .paymentTerms("90 days")
            .documentStatus("Draft")
            .currency("EUR")
            .build(),
        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMA").priceListVersion("Purchase").build())
            .orderedQuantity("11.2")
            .build(),
        new PurchaseOrderLinesData.Builder().unitPrice("2.00")
            .listPrice("2.00")
            .uOM("Bag")
            .tax("VAT 10%")
            .lineNetAmount("22.40")
            .build(),
        new PurchaseOrderHeaderData.Builder().summedLineAmount("22.40")
            .grandTotalAmount("24.64")
            .documentStatus("Booked")
            .build(),
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .paymentMethod("1 (Spain)")
            .expected("24.64")
            .paid("0.00")
            .outstanding("24.64")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),
        // Parameters for [PROa020] Create Goods Receipt.
        new GoodsReceiptHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new GoodsReceiptHeaderData.Builder().documentStatus("Draft").build(),
        new GoodsReceiptLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Raw material A").build())
            .attributeSetValue("#123")
            .storageBin(new LocatorSelectorData.Builder().alias("spain111").build())
            .movementQuantity("11.2")
            .uOM("Bag")
            .build(),
        new GoodsReceiptHeaderData.Builder().documentStatus("Completed").build(),
        // Parameters for [PROa030] Create Purchase Invoice.
        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .invoiceDate(OBDate.CURRENT_DATE)
            .accountingDate(OBDate.CURRENT_DATE)
            .priceList("Purchase")
            .paymentMethod("1 (Spain)")
            .paymentTerms("90 days")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("24.64")
            .daysTillDue("90")
            .dueAmount("0.00")
            .summedLineAmount("22.40")
            .grandTotalAmount("24.64")
            .documentStatus("Completed")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("1 (Spain)")
            .expected("24.64")
            .paid("0.00")
            .outstanding("24.64")
            .lastPaymentDate("")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),
        new String[][] { { "60000", "Compras de mercaderías", "22.40", "" },
            { "47200", "Hacienda Pública IVA soportado", "2.24", "" },
            { "40000", "Proveedores (euros)", "", "24.64" } } } });
  }

  /**
   * Test the creation of a purchase order and an invoice.
   */
  @Test
  public void PROa_createPurchaseOrderAndInvoice() {
    logger.info("** Start of test case [PROa010] Create Purchase Order. **");
    final PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerficationData);
    final PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);
    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);
    DataObject bookedPurchaseOrderHeaderData = purchaseOrder.getData();
    PurchaseOrder.PaymentOutPlan purchaseOrderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(
        mainPage);
    purchaseOrderPaymentOutPlan.assertCount(1);
    purchaseOrderPaymentOutPlan.assertData(purchaseOrderPaymentOutPlanData);
    logger.info("** End of test case [PROa010] Create Purchase Order. **");

    logger.info("** Start of test case [PROa020] Create Goods Receipt. **");
    final GoodsReceipt goodsReceipt = new GoodsReceipt(mainPage).open();
    goodsReceipt.create(goodsReceiptHeaderData);
    goodsReceipt.assertSaved();
    // TODO when the fields in status bar are automated, we can take the grand total amount from the
    // same object.
    String purchaseOrderIdentifier = String.format("%s - %s - %s",
        bookedPurchaseOrderHeaderData.getDataField("documentNo"),
        bookedPurchaseOrderHeaderData.getDataField("orderDate"),
        bookedPurchaseOrderHeaderVerificationData.getDataField("grandTotalAmount"));
    goodsReceipt.createLinesFrom("spain111", purchaseOrderIdentifier, "123");
    goodsReceipt.assertProcessCompletedSuccessfully();
    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.assertCount(1);
    goodsReceiptLines.assertData(goodsReceiptLinesVerificationData);
    goodsReceipt.complete();
    // Workaround for issue 20118
    for (int i = 0; i < 20 && !(goodsReceipt.getData("documentStatus")).equals("Completed"); i++) {
      Sleep.trySleep(200);
    }

    goodsReceipt.assertProcessCompletedSuccessfully2();
    goodsReceipt.assertData(completedGoodsReceiptHeaderVerificationData);
    logger.info("** End of test case [PROa020] Create Goods Receipt. **");

    logger.info("** Start of test case [PROa030] Create Purchase Invoice. **");
    Sleep.trySleep(1000);
    ProcessRequest processRequest = new ProcessRequest(mainPage).open();
    processRequest.select(
        new ProcessRequestData.Builder().process("Acct Server Process").organization("*").build());
    if (processRequest.unscheduleProcess()) {
      processRequest.assertProcessUnscheduleSucceeded();
    }
    Sleep.trySleep();
    SessionPreferences sessionPreferences = new SessionPreferences(mainPage).open();
    sessionPreferences.checkShowAccountingTabs();
    Sleep.trySleep();
    final PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    purchaseInvoice.createLinesFrom(purchaseOrderIdentifier);
    purchaseInvoice.assertProcessCompletedSuccessfully();
    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.assertCount(1);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);
    PurchaseInvoice.PaymentOutPlan purchaseInvoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(
        mainPage);
    purchaseInvoicePaymentOutPlan.assertCount(1);
    purchaseInvoicePaymentOutPlan.assertData(purchaseInvoicePaymentOutPlanData);
    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails purchaseInvoicePaymentOutDetails = purchaseInvoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    purchaseInvoicePaymentOutDetails.assertCount(0);
    // FIXME: Returns to Payment Plan to try to avoid the deleted accounting date issue in SCv10
    purchaseInvoicePaymentOutPlan.assertCount(1);
    purchaseInvoicePaymentOutPlan.assertData(purchaseInvoicePaymentOutPlanData);
    // // FIXME: Add here a click in the Cancel button to avoid the clearing of the Accounting Date
    // // field in SCv10
    // purchaseInvoice.cancelOnGrid();
    purchaseInvoice.post();
    // XXX This is required because currently we can use only one tab at a time.
    mainPage.closeView(PurchaseInvoiceWindow.TITLE);
    mainPage.loadView(new PostWindow());
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines.length);
    for (int i = 0; i < journalEntryLines.length; i++) {
      post.assertJournalLine(i + 1, journalEntryLines[i][0], journalEntryLines[i][1],
          journalEntryLines[i][2], journalEntryLines[i][3]);
    }
    logger.info("** End of test case [PROa030] Create Purchase Invoice. **");
  }
}
