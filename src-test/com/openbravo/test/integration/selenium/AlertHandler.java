/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 * 	Lorenzo Fidalgo<lorenzo.fidalgo@openbravo.com>.
 *
 ************************************************************************
 */

package com.openbravo.test.integration.selenium;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;

/**
 * Class used to manipulate alerts with Selenium
 *
 * @author lorenzo.fidalgo
 *
 */
public class AlertHandler {

  /**
   * This is identical to the selenium.isAlertPresent() method for WebDriverBackedSelenium, in that
   * it checks to see if an alert message is present, except this one works. It uses WebDriver
   * methods to check to see if a JavaScript alert() message is on the screen.
   *
   * @return This will return true if an alert message is on the screen.
   */
  public static boolean isAlertPresent() {
    try {
      SeleniumSingleton.INSTANCE.switchTo().alert();
      return true;
    } catch (NoAlertPresentException noAlertPresentException) {
      return false;
    }
  }

  /**
   * This is identical to the selenium.getAlert() method for WebDriverBackedSelenium, in that it
   * gets the text of the alert message and it clicks the OK button to make it go away, except this
   * one works.
   *
   * @return String with the text of the alert message in it.
   */
  public static String getAlertAndCloseIt() {
    Alert alert = SeleniumSingleton.INSTANCE.switchTo().alert();
    String str = alert.getText();
    alert.accept();
    return str;
  }

  /**
   * This is similar to the selenium.getAlert() method for WebDriverBackedSelenium, in that it gets
   * the text of the alert message. This method does NOT close the alert, and this one works.
   *
   * @return String with the text of the alert message in it.
   */
  public static String getAlert() {
    Alert alert = SeleniumSingleton.INSTANCE.switchTo().alert();
    String str = alert.getText();
    return str;
  }

  /**
   * Selenium accepts the alert window
   */
  public static void acceptAlert() {
    SeleniumSingleton.INSTANCE.switchTo().alert().accept();
  }
}
