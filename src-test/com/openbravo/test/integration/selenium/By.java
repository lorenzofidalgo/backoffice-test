/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.selenium;

import java.util.List;

import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebElement;

/**
 * Provides locator strategies for a web driver.
 *
 * Extends the default implementation to add a smart client locator.
 *
 * @author elopio
 *
 */
public class By extends org.openqa.selenium.By {

  /**
   * @param scLocator
   *          The smart client locator" of the element to search for
   * @return a By which locates elements by smart client locator.
   */
  public static org.openqa.selenium.By scLocator(final String scLocator) {
    if (scLocator == null) {
      throw new IllegalArgumentException("Cannot find elements with a null id attribute.");
    }
    return new By() {
      @Override
      public WebElement findElement(SearchContext context) {
        return ((FindsByScLocator) context).findElementByScLocator(scLocator);
      }

      @Override
      public String toString() {
        return "By.scLocator: " + scLocator;
      }
    };
  }

  @Override
  public List<WebElement> findElements(SearchContext arg0) {
    // TODO Auto-generated method stub
    return null;
  }

}
