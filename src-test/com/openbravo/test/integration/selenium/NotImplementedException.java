package com.openbravo.test.integration.selenium;

public class NotImplementedException extends UnsupportedOperationException {

  /**
   *
   */
  private static final long serialVersionUID = 1701195101591122923L;

  public NotImplementedException() {
    super("This function is not implemented");
  }

  public NotImplementedException(String message) {
    super(message);
  }
}
