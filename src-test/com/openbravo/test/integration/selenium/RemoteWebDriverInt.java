/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.selenium;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Interface to provide the method names and Smart Client locator strategy for different web
 * drivers.
 *
 * Check RemoteWebDriver class from Selenium's API if new methods are to be added
 *
 * @author inaki_garcia
 *
 */
public interface RemoteWebDriverInt extends WebDriver {

  /**
   * Find an element using the Smart Client locator.
   *
   * @param scLocator
   *          The Smart Client locator of the element.
   *
   * @return the web element.
   */
  WebElement findElementByScLocator(String scLocator);

  WebElement findElementByScLocator(String smartClientLocator, boolean ignoreSleep);

  Object executeScript(String action, Object... args);

  WebElement findElementById(String comboId);

  WebElement findElementById(String comboId, int timeoutInMs);

  Object executeScriptWithReturn(String script, Object... args);

  WebElement findElementByCssSelector(String string);

  WebElement findElementByLinkText(String linkRebuildNow);

  WebElement findElementByLinkTextWithFWait(String linkRebuildNow);

  WebElement findElementByName(String textFieldFirstLine);

  WebElement findElementByXPath(String format);

  List<WebElement> findElementsById(String string);

  List<WebElement> findElementsByName(String checkBoxLines);

  List<WebElement> findElementsByXPath(String locatorJournalLines);
}
