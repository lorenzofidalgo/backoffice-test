/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>,
 *  Iñaki Garcia <inaki.garcia@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Helper enum singleton to handle configuration properties.
 *
 * @author elopio
 *
 */
public enum ConfigurationProperties {

  /** The singleton instance. */
  INSTANCE;

  /** Path to the configuration properties file. */
  private final String FILE_PATH = "config" + File.separator + "OpenbravoERPTest.properties";

  /* Selenium properties. */
  /** Port used by selenium server to listen to client connections. */
  private final int seleniumPort;
  /**
   * Speed to run tests, in milliseconds. This will be the time between selenium commands.
   */
  private final String seleniumSpeed;
  /** Browser that will be used to execute the test. */
  private final String browser;
  /**
   * Path to the binaries of the browser If it's empty, selenium will execute the default command
   * that corresponds to selected browser.
   */
  private final String browserPath;

  /* Openbravo server properties. */
  /** URL of Openbravo server. */
  private final String openbravoBaseURL;
  /** Context of Openbravo ERP. */
  private final String openbravoContext;

  /* Test properties. */
  /** Name of the System Administrator user. */
  private final String systemAdministratorUser;
  /** Password of the System Administrator user. */
  private final String systemAdministratorPassword;
  /** Name of the Client Administrator user. */
  private final String clientAdministratorUser;
  /** Password of the Client Administrator password. */
  private final String clientAdministratorPassword;
  /** Name of the user that will be used for tests. */
  private final String userName;
  /** Password of the user that will be used for tests. */
  private final String password;
  /** Format of the quantities displayed. */
  private final String numberOutputFormat;
  /** Decimal separator. */
  private final char decimalSeparator;
  /** Grouping separator. */
  private final char groupingSeparator;
  /** Date format. (Spanish - dd-MM-yyyy, USA - MM-dd-yyyy, etc.) */
  private final String dateFormat;
  /** Interface (Classic or New) */
  private final String interfaceType;
  /** Test video recording is activated or not ('t' or 'f') */
  private char videorecord;
  /** Stop the suite if failure or continue executing all the test suite ('f' to stop). */
  private char stopSuiteWhenFailure;
  /** Autosave feature is set to autosave routine or save by closing form ('1' or '2') */
  private char autosave;
  /** Selector input method preference ('t(EXTBOX)', 'c(OMBO)' or 'p(OPUP)') */
  private char selectorInputPref;
  /** Database name */
  private String dbName;
  /** Module maturity status level */
  private String moduleMaturityLevel;

  /**
   * Enum constructor.
   */
  private ConfigurationProperties() {
    final Properties properties = new Properties();
    try {
      properties.load(new FileInputStream(FILE_PATH));
    } catch (FileNotFoundException exception) {
      try {
        // Looking up for OpenbravoERPTest.properties in the project
        System.out.println("Using alternate path method to reach OpenbravoERPTest.properties");
        ClassLoader classLoader = getClass().getClassLoader();
        File fi = new File(classLoader.getResource("OpenbravoERPTest.properties").getFile());
        properties.load(new FileInputStream(fi));
      } catch (FileNotFoundException fnfe) {
        System.out.println("OpenbravoERPTest.properties has not been found");
        fnfe.printStackTrace();
      } catch (IOException ioe) {
        System.out.println("OpenbravoERPTest.properties has not been found");
        ioe.printStackTrace();
      }
    } catch (IOException exception) {
      exception.printStackTrace();
    }
    seleniumPort = Integer.parseInt(properties.getProperty("selenium.port"));
    seleniumSpeed = properties.getProperty("selenium.speed");
    browser = properties.getProperty("selenium.browser");
    browserPath = properties.getProperty("selenium.browserPath");
    openbravoBaseURL = properties.getProperty("openbravo.url");
    openbravoContext = properties.getProperty("openbravo.context");
    systemAdministratorUser = properties.getProperty("test.systemAdministratorUser");
    systemAdministratorPassword = properties.getProperty("test.systemAdministratorPassword");
    clientAdministratorUser = properties.getProperty("test.clientAdministratorUser");
    clientAdministratorPassword = properties.getProperty("test.clientAdministratorPassword");
    userName = properties.getProperty("test.user");
    password = properties.getProperty("test.password");
    numberOutputFormat = properties.getProperty("test.format.number.output");
    decimalSeparator = properties.getProperty("test.format.number.decimal").charAt(0);
    groupingSeparator = properties.getProperty("test.format.number.grouping").charAt(0);
    interfaceType = properties.getProperty("openbravo.interface");
    dateFormat = properties.getProperty("dateFormat.java");
    try {
      dbName = properties.getProperty("bbdd.rdbms");
    } catch (NullPointerException databaseNameNotSet) {
      // In case bbdd.rdbms property isn't properly set in OpenbravoERPTest.properties file
      System.out.println("Database name hasn't been propertly set by the Jenkins job");
      databaseNameNotSet.printStackTrace();
    }
    try {
      videorecord = properties.getProperty("test.videorecord").charAt(0);
    } catch (NullPointerException | StringIndexOutOfBoundsException videorecordSettingNotCreated) {
      // In case videorecord property is not created in OpenbravoERPTest.properties file
      System.out.println("videorecord setting has not been set");
      videorecord = 'f';
    }
    try {
      stopSuiteWhenFailure = properties.getProperty("test.stopSuiteWhenFailure").charAt(0);
    } catch (NullPointerException stopSuiteWhenFailureSettingNotCreated) {
      // In case stopSuiteWhenFailure property is not created in OpenbravoERPTest.properties file
      System.out.println("stopSuiteWhenFailure setting has not been set");
      stopSuiteWhenFailure = 't';
    }
    try {
      autosave = properties.getProperty("test.autosave").charAt(0);
    } catch (NullPointerException | StringIndexOutOfBoundsException autosaveSettingNotCreated) {
      // In case autosave property is not created in OpenbravoERPTest.properties file
      System.out.println("Autosave setting has not been set, using default method");
      autosave = '3';
    }
    try {
      selectorInputPref = properties.getProperty("test.selectorInputPref").charAt(0);
    } catch (NullPointerException
        | StringIndexOutOfBoundsException selectorInputPrefSettingNotCreated) {
      // In case selectorInputPref property is not created in OpenbravoERPTest.properties file
      System.out
          .println("Selector input preference setting has not been set, using default method");
      selectorInputPref = 'p';
    }
    try {
      moduleMaturityLevel = properties.getProperty("maturity");
    } catch (NullPointerException moduleMaturityLevelNotSet) {
      // In case the property has not been created in OpenbravoERPTest.properties file
      System.out.println("Maturity level setting has not been set, using default method");
      moduleMaturityLevel = "QA_APPROVED";
    }
  }

  /**
   * Get the selenium port.
   *
   * @return the selenium port.
   */
  public int getSeleniumPort() {
    return seleniumPort;
  }

  /**
   * Get the selenium speed.
   *
   * @return the selenium speed.
   */
  public String getSeleniumSpeed() {
    return seleniumSpeed;
  }

  /**
   * Get the browser that will be used to execute the test.
   *
   * @return the browser that will be used to execute the test.
   */
  public String getBrowser() {
    return browser;
  }

  /**
   * Get the path to the browser binary.
   *
   * @return the path to the browser binary.
   */
  public String getBrowserPath() {
    return browserPath;
  }

  /**
   * Get the base URL of the Openbravo server.
   *
   * @return the base URL of the Openbravo server.
   */
  public String getOpenbravoBaseURL() {
    return openbravoBaseURL;
  }

  /**
   * Get the name of the Openbravo context.
   *
   * @return the name of the Openbravo context.
   */
  public String getOpenbravoContext() {
    return openbravoContext;
  }

  /**
   * Get the user name of the system administrator.
   *
   * @return the user name of the system administrator
   */
  public String getSystemAdministratorUser() {
    return systemAdministratorUser;
  }

  /**
   * Get the password of the system administrator.
   *
   * @return the password of the system administrator.
   */
  public String getSystemAdministratorPassword() {
    return systemAdministratorPassword;
  }

  /**
   * Get the user name of the client administrator.
   *
   * @return the user name of the client administrator.
   */
  public String getClientAdministratorUser() {
    return clientAdministratorUser;
  }

  /**
   * Get the password of the client administrator.
   *
   * @return the password of the client administrator.
   */
  public String getClientAdministratorPassword() {
    return clientAdministratorPassword;
  }

  /**
   * Get the user name of the client user.
   *
   * @return the user name of the client user.
   */
  public String getUserName() {
    return userName;
  }

  /**
   * Get the password of the client user.
   *
   * @return the password of the client user.
   */
  public String getPassword() {
    return password;
  }

  /**
   * Get the output format for numbers.
   *
   * @return the output format for numbers.
   */
  public String getNumberOutputFormat() {
    return numberOutputFormat;
  }

  /**
   * Get the decimal separator character.
   *
   * @return the decimal separator character.
   */
  public char getDecimalSeparator() {
    return decimalSeparator;
  }

  /**
   * Get the grouping separator character.
   *
   * @return the grouping separator character.
   */
  public char getGroupingSeparator() {
    return groupingSeparator;
  }

  /**
   * Get the date format setting
   *
   * @return the dateFormat
   */
  public String getDateFormat() {
    return dateFormat;
  }

  /**
   * Get the interface type.
   *
   * @return the interface type.
   */
  public String getInterfaceType() {
    return interfaceType;
  }

  /**
   * Get the database name
   *
   * @return the database name
   */
  public String getDbName() {
    return dbName;
  }

  /**
   * Get the video record setting.
   *
   * @return the video record setting.
   */
  public char getVideoRecord() {
    return videorecord;
  }

  /**
   * Get the stopSuiteWhenFailure record setting.
   *
   * @return the stopSuiteWhenFailure record setting.
   */
  public char getStopSuiteWhenFailure() {
    return stopSuiteWhenFailure;
  }

  /**
   * Get the autosave setting.
   *
   * @return the autosave setting.
   */
  public int getAutosave() {
    return autosave;
  }

  /**
   * Get the selector field input preference setting.
   *
   * @return the selector field input preference setting.
   */
  public char getSelectorInputPref() {
    return selectorInputPref;
  }

  /**
   * Get the selector field input preference setting.
   *
   * @return the selector field input preference setting.
   *
   *         FIXME: May be this is not the place for this method
   */
  public String getMaturityStatus() {
    switch (moduleMaturityLevel) {
      case "CONFIRMED_STABLE":
        return "Confirmed Stable";
      case "QA_APPROVED":
        return "QA Approved";
      case "QA":
        return "QA";
      case "TEST":
        return "Test";
      default:
        return "QA Approved";
    }
  }

  /**
   * Overrides in the OpenbravoERPTest.properties file a specific property with another value
   *
   * @param propertyToChange
   *          The property key that is going to be searched for
   * @param newValueOfProperty
   *          The property value from now onwards
   */
  public void setProperty(String propertyToChange, String newValueOfProperty) {
    // Convert String FILE_PATH into a Path element
    Path pathOfFILE_PATH = Paths.get(FILE_PATH);
    List<String> fileContent = null;
    // Introduce all lines of the file in fileContent
    try {
      fileContent = new ArrayList<>(Files.readAllLines(pathOfFILE_PATH, StandardCharsets.UTF_8));
    } catch (IOException e1) {
      // TODO Auto-generated catch block
      e1.printStackTrace();
    }
    // Edit in fileContent what it is required. Finds the required line and stops looking for (Only
    // 1 ocurrence)
    for (int i = 0; i < fileContent.size(); i++) {
      if (fileContent.get(i).startsWith(propertyToChange + "=")) {
        fileContent.set(i, propertyToChange + "=" + newValueOfProperty);
        break;
      }
    }
    // Write the edited file. (Override the original file with the edited one)
    try {
      Files.write(pathOfFILE_PATH, fileContent, StandardCharsets.UTF_8);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
}
