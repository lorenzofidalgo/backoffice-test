/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */
package com.openbravo.test.integration.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * @author plujan
 *
 */
public class OBDate {
  /** DATE_FORMAT is dynamically got from OpenbravoERPTest.properties */
  static String DATE_FORMAT = ConfigurationProperties.INSTANCE.getDateFormat();
  static String YEAR_FORMAT = "yyyy";
  static String PERIOD_FORMAT = "MMM-yy";

  /* The following CONSTANTS are used when they are going to be asserted */

  /**
   * OBDate.CURRENT_DATE assigns the correct system date at that moment OBDate.CURRENT_DATE is
   * invoked
   */
  public static String CURRENT_DATE = "#currentDate#";

  /**
   * ADD_DAYS_TO_SYSTEM_DATE_90 adds 90 days to the correct system date at that moment
   * ADD_DAYS_TO_SYSTEM_DATE_90 is invoked
   */
  public static String ADD_DAYS_TO_SYSTEM_DATE_90 = "#addDaysToSystemDate90#";
  /**
   * GET_30_DAYS_5_FROM_SYSTEM_DATE get the correct system date at that moment
   * GET_30_DAYS_5_FROM_SYSTEM_DATE is invoked
   */
  public static String GET_30_DAYS_5_FROM_SYSTEM_DATE = "#get30Days5FromSystemDate#";
  /**
   * DEDUCT_1_MONTH_TO_SYSTEM_DATE deducts one month to the system date at that moment
   * DEDUCT_1_MONTH_TO_SYSTEM_DATE is invoked
   */
  public static String DEDUCT_1_MONTH_TO_SYSTEM_DATE = "#deduct1MonthToSystemDate#";
  /**
   * GET_YEAR_BEFORE deducts one year to the system date at that moment GET_YEAR_BEFORE is invoked
   */
  public static String GET_YEAR_BEFORE = "#getYearBefore#";
  /**
   * GET_CURRENT_YEAR get the current year from system date at that moment GET_CURRENT_YEAR is
   * invoked
   */
  public static String GET_CURRENT_YEAR = "#getCurrentYear#";

  /* END CONSTANTS */

  /**
   * Returns a formatted given date plus a fixed number of days.
   *
   * @param date
   *          The date to which it is wanted to add days
   * @param days
   *          The number of days that are going to be added to the date
   *
   * @return The formatted target date.
   */
  public static String addDaysToDate(String date, Integer days) throws ParseException {
    String dateAfterTag = (String) getDateFromTag(date);
    final Calendar calendar = Calendar.getInstance();
    calendar.setTime(new SimpleDateFormat(DATE_FORMAT, Locale.US).parse(dateAfterTag));
    calendar.add(Calendar.DATE, days);
    final String result = new SimpleDateFormat(DATE_FORMAT, Locale.US).format(calendar.getTime());
    return result;
  }

  /**
   * Returns a formatted system date.
   *
   * @return The formatted current system date.
   */
  public static String getSystemDate() {
    final Calendar calendar = Calendar.getInstance();
    final String result = new SimpleDateFormat(DATE_FORMAT, Locale.US).format(calendar.getTime());
    return result;
  }

  /**
   * Returns a formatted system time.
   *
   * @return The formatted current system time.
   */
  public static String getSystemTime() {
    final Calendar calendar = Calendar.getInstance();
    final String result = new SimpleDateFormat("HH:mm:ss").format(calendar.getTime());
    return result;
  }

  /**
   * Check if certain time is after the current system time.
   *
   * @param hourOfDay
   *          The current hour of the day
   * @param minute
   *          The current minute of the day
   * @param second
   *          The current second of the day
   *
   * @return true if the current system time is after HHMMSS specified in the parameters. Otherwise
   *         false.
   */
  public static boolean currentTimeIsAfterHHMMSS(int hourOfDay, int minute, int second) {
    final Calendar calendarNow = Calendar.getInstance();
    Calendar dangerousTime = Calendar.getInstance();
    dangerousTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
    dangerousTime.set(Calendar.MINUTE, minute);
    dangerousTime.set(Calendar.SECOND, second);
    return calendarNow.after(dangerousTime);
  }

  /**
   * Returns a formatted system date plus a fixed number of months.
   *
   * @param months
   *          The number of months that it is required to be added to the current date
   *
   * @return The formatted target date.
   */
  public static String addMonthsToSystemDate(Integer months) {
    return addMonthsToSystemDate(months, false);
  }

  /**
   * Returns a formatted system date plus a fixed number of months in a working date.
   *
   * @param months
   *          The number of months that it is required to be added to the current date
   * @param workingDate
   *          If true the returned date belongs to a working date
   *
   * @return The formatted target date.
   */
  public static String addMonthsToSystemDate(Integer months, boolean workingDate) {
    final Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.MONTH, months);
    if (workingDate) {
      if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
        calendar.add(Calendar.DATE, 2);
      } else if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
        calendar.add(Calendar.DATE, 1);
      }
    }
    final String result = new SimpleDateFormat(DATE_FORMAT, Locale.US).format(calendar.getTime());
    return result;
  }

  /**
   * Returns a formatted system date plus a fixed number of days.
   *
   * @param days
   *          The number of days that it is required to be added to the current date
   *
   * @return The formatted target date.
   */
  public static String addDaysToSystemDate(Integer days) {
    final Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, days);
    final String result = new SimpleDateFormat(DATE_FORMAT, Locale.US).format(calendar.getTime());
    return result;
  }

  /**
   * Returns a formatted system date.
   *
   * @return The formatted current system date.
   */
  public static String getLastDayOfCurrentMonth() {
    final Calendar calendar = Calendar.getInstance();
    int lastDate = calendar.getActualMaximum(Calendar.DATE);
    calendar.set(Calendar.DATE, lastDate);
    final String result = new SimpleDateFormat(DATE_FORMAT, Locale.US).format(calendar.getTime());
    return result;
  }

  /**
   * Get 30 days, 5 from current system date.
   *
   * @return 30 days, 5 from current system date.
   */
  public static String get30Days5FromSystemDate() {
    final Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DAY_OF_MONTH, 30);
    if (calendar.get(Calendar.DAY_OF_MONTH) > 5) {
      calendar.add(Calendar.MONTH, 1);
    }
    calendar.set(Calendar.DAY_OF_MONTH, 5);
    final String result = new SimpleDateFormat(DATE_FORMAT, Locale.US).format(calendar.getTime());
    return result;
  }

  /**
   * Returns current year.
   *
   * @return The formatted year.
   */
  public static String getCurrentYear() {
    final Calendar calendar = Calendar.getInstance();
    // calendar.add(Calendar.MONTH, months);
    final String result = new SimpleDateFormat(YEAR_FORMAT, Locale.US).format(calendar.getTime());
    return result;
  }

  /**
   * Returns previous year. In 2011 returns 2010
   *
   * @return The formatted year.
   */
  public static String getYearBefore() {
    final Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.YEAR, -1);
    final String result = new SimpleDateFormat(YEAR_FORMAT, Locale.US).format(calendar.getTime());
    return result;
  }

  /**
   * Returns a given period
   *
   * @param month
   *          The number of the month
   * @param year
   *          The number of the day
   *
   * @return The formatted period.
   */
  public static String getPeriod(Integer month, Integer year) {
    final Calendar calendar = Calendar.getInstance();
    calendar.set(year, month - 1, 1);
    final String result = new SimpleDateFormat(PERIOD_FORMAT, Locale.US).format(calendar.getTime());
    return result;
  }

  /**
   *
   * Returns a given formatted date
   *
   * @param Year
   *          The number of the year
   * @param Month
   *          The number of the month
   * @param Day
   *          The number of the day
   *
   * @return the formatted given date
   */
  public static String getGivenDate(int Year, int Month, int Day) {
    final Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.YEAR, Year);
    calendar.set(Calendar.MONTH, Month);
    calendar.set(Calendar.DAY_OF_MONTH, Day);
    final String result = new SimpleDateFormat(DATE_FORMAT, Locale.US).format(calendar.getTime());
    return result;
  }

  // This function manipulate certain CONSTANTS to get the Date in the correct moment
  public static Object getDateFromTag(Object expectedDisplayValue) {
    String result = null;
    if (expectedDisplayValue.toString().contains("#currentDate#")) {
      result = expectedDisplayValue.toString().replace("#currentDate#", OBDate.getSystemDate());
      return result;
    }
    // Manipulate #addDaysToSystemDate to get the correct OBDate.addDaysToSystemDate(days)
    // avoiding false positives
    else if (expectedDisplayValue.toString().contains("#addDaysToSystemDate")) {
      String strDays = expectedDisplayValue.toString().replaceAll("#addDaysToSystemDate", "");
      strDays = strDays.substring(0, strDays.indexOf('#'));
      int days = Integer.parseInt(strDays);
      result = OBDate.addDaysToSystemDate(days);
      return result;
    } else if (expectedDisplayValue.toString().contains("#get30Days5FromSystemDate#")) {
      result = expectedDisplayValue.toString()
          .replace("#get30Days5FromSystemDate#", OBDate.get30Days5FromSystemDate());
      return result;
    } else if (expectedDisplayValue.toString().contains("#deduct1MonthToSystemDate#")) {
      result = expectedDisplayValue.toString()
          .replace("#deduct1MonthToSystemDate#", OBDate.addMonthsToSystemDate(-1));
      return result;
    } else if (expectedDisplayValue.toString().contains("#getYearBefore#")) {
      result = expectedDisplayValue.toString().replace("#getYearBefore#", OBDate.getYearBefore());
      return result;
    } else if (expectedDisplayValue.toString().contains("#getCurrentYear#")) {
      result = expectedDisplayValue.toString().replace("#getCurrentYear#", OBDate.getCurrentYear());
      return result;
    } else {
      return expectedDisplayValue;
    }
  }
}
