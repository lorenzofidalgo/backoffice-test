import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class XMLGeneratorRunner {
  static String WORK_DIRECTORY = "c:\\temp";

  /**
   * @param args
   */
  public static void main(String[] args) {

    Connection connection = null;

    try {

      connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/openbravo", "tad",
          "tad");

    } catch (final SQLException e) {

      System.out.println("Connection Failed! Check output console");
      e.printStackTrace();
      return;

    }

    if (connection != null) {
      System.out.println("You made it, take control your database now!");
    } else {
      System.out.println("Failed to make connection!");
    }
    try {
      final Statement stWindow = connection.createStatement();
      final Statement stMainTab = connection.createStatement();
      final Statement stField = connection.createStatement();
      ResultSet rsWindow;
      ResultSet rsMainTab;
      ResultSet rsField;
      String result = "";
      String fileName = "";
      rsWindow = stWindow.executeQuery("SELECT ad_window_id as window, name  " + " from ad_window "
          + " where ad_window_id in (" + " select ad_window_id " + " from ad_tab "
          + " where isactive='Y');");
      while (rsWindow.next()) {
        rsMainTab = stMainTab.executeQuery("select ad_tab_id as tab, name" + " from ad_tab T0"
            + " where T0.isactive='Y' " + " and T0.ad_window_id='" + rsWindow.getString("window")
            + "'");
        while (rsMainTab.next()) {
          result = "";
          result = result + "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
          result = result + "<generator>\n";
          final String packageName = getPackagePath(rsWindow.getString("window"), connection);
          result = result + "<packageName>" + packageName + "</packageName>\n";
          result = result + "<fields>\n";

          if (rsMainTab.getString("name").equals("Header")
              || rsMainTab.getString("name").equals("Lines")) {
            fileName = transformToClassName(rsWindow.getString("name"))
                + transformToClassName(rsMainTab.getString("name"));
          } else {
            fileName = transformToClassName(rsMainTab.getString("name"));
          }
          rsField = stField
              .executeQuery("select F.description, "
                  + " R.name as type, "
                  + "(case when lower(C.name)='default' then 'defaultValue' else C.name end) as name, "
                  + "(case when lower(C.name)='default' then 'defaultValue' else C.name end) as locator, "
                  + " RV.name as selector "
                  + " from ad_field F "
                  + " inner join ad_column C on F.ad_column_id=C.ad_Column_id "
                  + "       inner join ad_reference R on C.ad_reference_id=R.ad_reference_id "
                  + "       left join ad_reference RV on C.ad_reference_value_id=RV.ad_reference_id "
                  + " where F.ad_tab_id='" + rsMainTab.getString("tab") + "' "
                  + " and R.name not in ('Button', 'Image', 'Image BLOB', 'Binary') "
                  + " and F.isdisplayed='Y' " + "  and F.isactive='Y'" + ";");
          while (rsField.next()) {
            result = result + "   <field>\n";
            String description = rsField.getString("description");
            if (rsField.wasNull()) {
              description = rsField.getString("locator");
            }
            result = result + "      <description>" + removeAnds(description) + "</description>\n";
            String selector = rsField.getString("selector");
            if (rsField.wasNull()) {
              selector = "String";
            }
            result = result + "      <type>" + getDataType(rsField.getString("type"), selector)
                + "</type>\n";
            result = result + "      <name>" + transformToField(rsField.getString("locator"))
                + "</name>\n";
            result = result + "      <locator>" + transformToField(rsField.getString("locator"))
                + "</locator>\n";
            result = result + "   </field>\n";

          }
          result = result + "</fields>\n";
          result = result + "</generator>\n";
          generateOutput(result, WORK_DIRECTORY, getTreeOf(packageName)/*
                                                                        * , transformToPackageName(
                                                                        * rsWindow
                                                                        * .getString("name"))
                                                                        */, fileName + "Data");
          // System.out.println(result);
        }
      }
    } catch (final SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  private static String standarizeName(String string) {
    String result = string;
    for (int i = 0; i < result.length() - 1; i++) {
      if (result.charAt(i) == ' ' || result.charAt(i) == '.' || result.charAt(i) == '_'
          || result.charAt(i) == '/' || result.charAt(i) == '(' || result.charAt(i) == ')'
          || result.charAt(i) == '%' || result.charAt(i) == '-' || result.charAt(i) == '\''
          || result.charAt(i) == '&' || result.charAt(i) == ',') {
        result = result.substring(0, i) + result.substring(i, i + 2).toUpperCase()
            + result.substring(i + 2);
      }
    }
    result = result.replace(" ", "").replace(".", "").replace("_", "").replace("/", "").replace(
        "(", "").replace(")", "").replace("%", "").replace("-", "").replace("'", "").replace("&",
        "").replace(",", "");

    return result;
  }

  private static String removeAnds(String string) {
    String result = string;
    result = result.replace("&", "").replace(",", "");
    return result;
  }

  private static String transformToClassName(String string) {
    String result;
    result = standarizeName(string);
    result = result.substring(0, 1).toUpperCase() + result.substring(1);
    return result;
  }

  private static String getDataType(String string, String selector) {
    String result;
    if (string.equals("YesNo")) {
      result = "Boolean";
    } else if (string.equals("Search")) {
      if (selector.equals("String")) {
        result = "String";
      } else if (selector.equals("Location")) {
        result = "LocationSelectorData";
      } else if (selector.equals("Account")) {
        result = "AccountSelectorData";
      } else if (selector.equals("Locator")) {
        result = "LocatorSelectorData";
      } else if (selector.equals("Product Complete")) {
        result = "ProductCompleteSelectorData";
      } else if (selector.equals("Business Partner")) {
        result = "BusinessPartnerSelectorData";
      } else if (selector.equals("Debt/Payment")) {
        result = "DebtPaymentSelectorData";
      } else if (selector.equals("Invoice")) {
        result = "InvoiceSelectorData";
      } else if (selector.equals("Product")) {
        result = "ProductSelectorData";
      } else if (selector.equals("Project")) {
        result = "ProjectSelectorData";
      } else if (selector.equals("Order")) {
        result = "OrderSelectorData";
      } else if (selector.equals("Order Line")) {
        result = "OrderLineSelectorData";
      } else if (selector.equals("Shipment/Receipt Line")) {
        result = "ShipmentReceiptLineSelectorData";
      } else if (selector.equals("Locator search")) {
        result = "LocatorSearchSelectorData";
      } else if (selector.equals("Shipment/Receipt")) {
        result = "ShipmentReceiptSelectorData";
      } else if (selector.equals("Invoice Line")) {
        result = "InvoiceLineSelectorData";
      } else if (selector.equals("Invoice Payment Plan")) {
        result = "InvoicePaymentPlanSelectorData";
      } else if (selector.equals("Account Element Value")) {
        result = "AccountElementValueSelectorData";
      } else if (selector.equals("Order Payment Plan")) {
        result = "OrderPaymentPlanSelectorData";
      } else {
        result = "String";
      }
    } else {
      result = "String";
    }
    return result;
  }

  private static String transformToPackageName(String string) {
    String result;
    result = standarizeName(string).toLowerCase();
    return result;
  }

  private static String transformToField(String string) {
    String result;
    result = standarizeName(string);
    result = result.substring(0, 1).toLowerCase() + result.substring(1);
    return result;
  }

  private static String getTreeOf(String packageName) {
    return packageName.replace(".", "\\");
  }

  private static void generateOutput(String file, String path, String tree, String outputFile) {
    BufferedOutputStream bo = null;
    try {
      final String fullPath = path + "\\" + tree;
      final Boolean result = new File(fullPath).mkdirs();

      System.out.println(fullPath);
      bo = new BufferedOutputStream(new FileOutputStream(fullPath + "\\" + outputFile + ".xml"));
      bo.write(file.getBytes());
    } catch (final FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (final IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } finally {
      // Close the BufferedOutputStream
      try {
        if (bo != null) {
          bo.flush();
          bo.close();
        }
      } catch (final IOException ex) {
        ex.printStackTrace();
      }
    }

  }

  private static String getPackagePath(String ADWindowID, Connection con) throws SQLException {
    String result = "";
    final Statement stMenu = con.createStatement();
    final ResultSet rsMenu = stMenu
        .executeQuery("select ( "
            + " select M.name from ad_treenode TN inner join ad_menu M on TN.node_id=M.ad_menu_id where "
            + " TN.ad_tree_id='10' and M.ad_menu_id=( "
            + " select TN.parent_id from ad_treenode TN inner join ad_menu M on TN.node_id=M.ad_menu_id where "
            + "  TN.ad_tree_id='10' and M.ad_menu_id=( "
            + " select TN.parent_id from ad_treenode TN inner join ad_menu M on TN.node_id=M.ad_menu_id where "
            + " TN.ad_tree_id='10' and M.ad_menu_id=( "
            + " select TN.parent_id from ad_treenode TN inner join ad_menu M on TN.node_id=M.ad_menu_id where "
            + " M.ad_window_id='"
            + ADWindowID
            + "' and TN.ad_tree_id='10') "
            + " ) "
            + " ) "
            + " ) as menu1,( "
            + " select M.name from ad_treenode TN inner join ad_menu M on TN.node_id=M.ad_menu_id where "
            + " TN.ad_tree_id='10' and M.ad_menu_id=( "
            + " select TN.parent_id from ad_treenode TN inner join ad_menu M on TN.node_id=M.ad_menu_id where "
            + " TN.ad_tree_id='10' and M.ad_menu_id=( "
            + " select TN.parent_id from ad_treenode TN inner join ad_menu M on TN.node_id=M.ad_menu_id where "
            + " M.ad_window_id='"
            + ADWindowID
            + "' and TN.ad_tree_id='10') "
            + " ) "
            + " ) as menu2, ( "
            + " select M.name from ad_treenode TN inner join ad_menu M on TN.node_id=M.ad_menu_id where "
            + " TN.ad_tree_id='10' and M.ad_menu_id=( "
            + " select TN.parent_id from ad_treenode TN inner join ad_menu M on TN.node_id=M.ad_menu_id where "
            + " M.ad_window_id='"
            + ADWindowID
            + "' and TN.ad_tree_id='10') "
            + " ) as menu3, ( "
            + " select M.name from ad_treenode TN inner join ad_menu M on TN.node_id=M.ad_menu_id where "
            + " M.ad_window_id='" + ADWindowID + "' and TN.ad_tree_id='10' " + "  ) as menu4 ");
    while (rsMenu.next()) {
      result = "com.openbravo.test.integration.erp.data.";
      String menuValue = rsMenu.getString("menu1");
      if (!rsMenu.wasNull()) {
        result = result + removeManagement(transformToPackageName(menuValue)) + ".";
      }
      menuValue = rsMenu.getString("menu2");
      if (!rsMenu.wasNull()) {
        result = result + removeManagement(transformToPackageName(menuValue)) + ".";
      }
      menuValue = rsMenu.getString("menu3");
      if (!rsMenu.wasNull()) {
        result = result + removeManagement(transformToPackageName(menuValue)) + ".";
      }
      menuValue = rsMenu.getString("menu4");
      if (!rsMenu.wasNull()) {
        result = result + removeManagement(transformToPackageName(menuValue));
      }

    }
    return result;
  }

  private static String removeManagement(String name) {
    String result = name;
    if (result.endsWith("management") && result.length() > "management".length()) {
      result = result.substring(0, result.length() - "management".length());
    }
    return result;
  }
}
